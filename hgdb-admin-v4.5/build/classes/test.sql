
/**
 * Author:  liuyuanyuan
 * Created: 2020年2月4日
 */

-- event trigger --
CREATE EVENT TRIGGER rrr ON DDL_COMMAND_END
  WHEN TAG IN('DROP FUNCTION', 'CREATE FUNCTION')
  EXECUTE PROCEDURE public.abort_any_command();

CREATE EVENT TRIGGER rrr ON DDL_COMMAND_END
  WHEN TAG IN('DROP FUNCTION', 'CREATE FUNCTION')
  EXECUTE PROCEDURE public.abort_any_command();

--
CREATE OR REPLACE FUNCTION snitch() RETURNS event_trigger AS $$
BEGIN
    RAISE NOTICE 'snitch: % %', tg_event, tg_tag;
END;
$$ LANGUAGE plpgsql;

CREATE EVENT TRIGGER snitch ON ddl_command_start EXECUTE FUNCTION snitch();

----
CREATE FUNCTION noddl() RETURNS event_trigger
    AS 'noddl' LANGUAGE C;

CREATE EVENT TRIGGER noddl ON ddl_command_start
    EXECUTE FUNCTION noddl();

----
CREATE FUNCTION test_event_trigger_for_drops()
        RETURNS event_trigger LANGUAGE plpgsql AS $$
DECLARE
    obj record;
BEGIN
    FOR obj IN SELECT * FROM pg_event_trigger_dropped_objects()
    LOOP
        RAISE NOTICE '% dropped object: % %.% %',
                     tg_tag,
                     obj.object_type,
                     obj.schema_name,
                     obj.object_name,
                     obj.object_identity;
    END LOOP;
END
$$;

CREATE EVENT TRIGGER test_event_trigger_for_drops
   ON sql_drop
   EXECUTE FUNCTION test_event_trigger_for_drops();


