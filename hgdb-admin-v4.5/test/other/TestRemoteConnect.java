/* ------------------------------------------------
 *
 * File: TestRemoteConnect.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/TestRemoteConnect.java
 *
 *-------------------------------------------------
 */

package other;

import java.io.IOException;
 
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.log4j.LogManager;
/**
 *
 * @author Yuanyuan
 */
public class TestRemoteConnect {

    private org.apache.log4j.Logger logger = LogManager.getLogger(getClass());

    public static void main(String[] args)
    {
//        Process proc;
//        try
//        {
//            proc = Runtime.getRuntime().exec("net start postgresql-x64-9.2");            
//             try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream())))//, "GBK"
//            {
//                String line;
//                while ((line = bufferedReader.readLine()) != null)
//                {
//                    System.out.println(line);
//                }
//            }
//            proc.waitFor();
//            System.out.println("exit value = "+proc.exitValue());
//        } catch (IOException | InterruptedException ex)
//        {
//            Logger.getLogger(TestRemoteConnect.class.getName()).log(Level.SEVERE, null, ex);
//        }
         
        String hostname = "192.168.100.234";//192.168.100.232";
        String username = "Administrator";
        String password = "highgo";
        //指明连接主机的IP地址  
        Connection conn = new Connection(hostname);
        Session ssh = null;
        try
        {
            //连接到主机  
            conn.connect();
            //使用用户名和密码校验  
            boolean isconn = conn.authenticateWithPassword(username, password);
            if (!isconn)
            {
                System.out.println("用户名称或者是密码不正确");
            } else
            {
                System.out.println("已经连接OK");
                ssh = conn.openSession();
                ssh.execCommand("cmd.exe /c \"net stop hgdb-se1.3\"");//hgdb-se1.3
                System.out.println("execute cmd");
                //ssh.execCommand("sh /root/hello.sh");
                //ssh.execCommand("perl /root/hello.pl");  
                //只允许使用一行命令，即ssh对象只能使用一次execCommand这个方法，
                //多次使用则会出现异常       
                //使用多个命令用分号隔开  
                //ssh.execCommand("cd /root; sh hello.sh"); 
                //将Terminal屏幕上的文字全部打印出来                  
                InputStream is = new StreamGobbler(ssh.getStdout());
                BufferedReader brs = new BufferedReader(new InputStreamReader(is));
                while (true)
                {
                    String line = brs.readLine();
                    if (line == null)
                    {
                        break;
                    }
                    System.out.println(line);
                }
                System.out.println("exit value = "+ssh.getExitStatus());
            }
        } catch (IOException e)
        {
            e.printStackTrace();
            System.out.println(e.getMessage());
        } finally
        {
            //连接的Session和Connection对象都需要关闭  
            ssh.close();
            conn.close();
        }
    } 
}
