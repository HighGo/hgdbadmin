/* ------------------------------------------------
 *
 * File: TestPattern.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/TestPattern.java
 *
 *-------------------------------------------------
 */

package other;

/**
 *
 * @author Yuanyuan
 */
public class TestPattern
{
    public static void main(String[] args)
    {
        String  fullname = "timestamp(2) with time zone";
        String name;
        int length = 0, scale = 0;
        if (fullname.indexOf("(") > 0 && fullname.indexOf(")")>0)
        {
            System.out.println("***********"+(fullname.startsWith("time")));
            if (fullname.startsWith("time") && fullname.endsWith(" time zone"))
            {
                length = Integer.valueOf(fullname.substring(fullname.indexOf("(") + 1, fullname.indexOf(")")));
                name = fullname.substring(0, fullname.indexOf("("))
                        + fullname.substring(fullname.indexOf(")")+1, fullname.length());
                System.out.println("name="+name);
            } else
            {
                name = fullname.substring(0, fullname.indexOf("("));
                String arg = fullname.substring(fullname.indexOf("(") + 1, fullname.indexOf(")"));
                System.out.println("arg=" + arg);
                if (arg.indexOf(",") <= 0)
                {
                    length = Integer.valueOf(arg);
                } else
                {
                    length = Integer.valueOf(arg.split(",")[0]);
                    scale = Integer.valueOf(arg.split(",")[1]);
                }
            }
        } else
        {
            name = fullname;
        }
        System.out.println("" + name);
        System.out.println("" + length);
        System.out.println("" + scale);
        
    }
    
}
