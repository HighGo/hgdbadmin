/* ------------------------------------------------
 *
 * File: TestLog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/TestLog.java
 *
 *-------------------------------------------------
 */

package other;

import java.util.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class TestLog
{
    private Logger logger = LogManager.getLogger(getClass());
     
    private Properties logProp = null;
    private String log4jPath = "./src/log4j.properties";

    public void execute()
    {       
        logger.error("log level " + LogManager.getRootLogger().getLevel());
        LogManager.getRootLogger().setLevel(org.apache.log4j.Level.ERROR);
        // LogManager.resetConfiguration();
        logger.debug("debug......");
        logger.info("info......");
        logger.warn("warn......");
        logger.error("error......");
        try
        {
//             OptionController oc = OptionController.getInstance();
//             logProp = oc.getPropertiesFromFile(log4jPath);
//             logProp.setProperty("log4j.appender.FILE.File", "logs/highgo.log");
//             oc.savePropertiesToFile(logProp, log4jPath);
           
            //logger = LoggerFactory.getLogger(getClass());
            LogManager.getLoggerRepository().resetConfiguration();
            logger.error("log level " + LogManager.getRootLogger().getLevel());
            logger.info("inf0------");
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        }
         
     }
     
     
    public static void main(String[] args)
    {
        TestLog tl= new TestLog();
        tl.execute();
      
    }
    
}
