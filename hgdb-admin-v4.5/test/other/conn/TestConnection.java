/* ------------------------------------------------
 *
 * File: TestConnection.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/conn/TestConnection.java
 *
 *-------------------------------------------------
 */
package other.conn;

/**
 *
 * @author Yuanyuan
 */
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//C:\\HighGo\\Database\\1.3\\bin\\pg_ctl start -D C:\\HighGo\\Database\\1.3\\data";//"pwd;ls;chmod 777 -R server_start.sh;";
//                SCPClient clt = conn.createSCPClient();
//                clt.put("conf/server_start.sh", "/opt/pg944/bin");    
public class TestConnection
{
    private final Logger logger = LogManager.getLogger(getClass());

    public void conn()
    {
        String hostName = "192.168.102.37";
        int port = 22;
        String userName = "root";
        String passWord = "911023";
        Connection conn = null;
        Session sess = null;
        try
        {
            conn = new Connection(hostName, port);
            conn.connect();
            conn.authenticateWithPassword(userName, passWord);
            logger.info("登录成功啦");
            if (conn != null)
            {            
                sess = conn.openSession();
                logger.info(sess.toString());
                String command ="cd /home/highgo/hgdb/data;sh server_start.sh";//"su - highgo;cd /home/highgo/hgdb/bin/;pwd;./pg_ctl status -D ../data;";;
                sess.execCommand(command);
                logger.info("execute cmd=" + command);
                InputStream stdout = new StreamGobbler(sess.getStdout());
                BufferedReader br = new BufferedReader(new InputStreamReader(stdout));
                while (true)
                {
                    String line = br.readLine();
                    if (line == null)
                    {
                        break;
                    }
                    logger.info("kkkkkk="+line);
                }
                Integer execResult = sess.getExitStatus();
                logger.info(execResult);
                logger.info(execResult == null ? "执行结果：Null" : (execResult.intValue() == 0 ? "执行结果：成功" : "执行结果：失败"));
            }
        } catch (IOException e)
        {
            logger.error(e);
            e.printStackTrace();
        } finally
        {
            if (sess != null)
            {
                sess.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }
    }

    public static void main(String[] args)
    {
        TestConnection tc = new TestConnection();
        tc.conn();        
    }
    
}
