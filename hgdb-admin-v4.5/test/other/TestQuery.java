/* ------------------------------------------------
 *
 * File: TestQuery.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/TestQuery.java
 *
 *-------------------------------------------------
 */

package other;

import com.highgo.hgdbadmin.util.JdbcHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class TestQuery
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());

    public void query()
    {
        String driver = "com.highgo.jdbc.Driver";
        String url = "jdbc:highgo://192.168.90.64:5866/highgo";//?preferQueryMode=simple
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(url,"sysdba", "Highgo@123", false);
           // conn.setSchema("admin");
            //conn.setAutoCommit(false)
            
             String sql = "delete from t1 where id=1";
            //stmt = conn.createStatement();
            // stmt.execute(sql);
             
             //conn.setSchema("dba");
            //conn.setAutoCommit(false);
             sql = "select * from  t1";
             stmt = conn.createStatement();
             rs = stmt.executeQuery(sql);
             while(rs.next()){
                System.out.println(rs.getInt("id"));
             }
            
             /*
             stmt = conn.prepareStatement(sql);
             stmt.setFetchSize(1000);
             rs = stmt.executeQuery();
             while(rs.next()){
                logger.debug(rs.getObject(1));
             }
             */
            /*
            for (int i = 6; i <= 2300; i++)
            {
                System.out.println(i);
                stmt.setInt(1, i);
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);*/
            /*stmt.setFetchSize(2000);
            boolean a = stmt.execute(s);
            if(a)
            {
                rs = stmt.getResultSet();
                while(rs.next())
                {
                    logger.info("rs = "+rs.getObject(1));
                }
            }else
            {
                logger.info("update="+stmt.getUpdateCount());
            }
            */
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace();
        } finally
        {
           
            //JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
     }
     
    public static void main(String[] args)
    {
      TestQuery tq = new TestQuery();
      tq.query();
    }
}
