/* ------------------------------------------------
 *
 * File: Test.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/Test.java
 *
 *-------------------------------------------------
 */

package other;



import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class Test
{
    private Logger logger = LogManager.getLogger(getClass());
    
    private void connect()
    {
        Process proc = null;
        try
        {
            String bin = "D:\\Program Files (x86)\\PostgreSQL\\9.4\\bin";
            String data = "D:\\Program Files (x86)\\PostgreSQL\\9.4\\data";
            String cmd="\"D:\\Program Files (x86)\\PostgreSQL\\9.4\\bin\\pg_ctl\" start -D \"D:\\Program Files (x86)\\PostgreSQL\\9.4\\data\"";
            logger.info(cmd.toString());
            proc = Runtime.getRuntime().exec(cmd.toString());
//                List<String> cmds = new ArrayList<>();
//                StringBuilder cmdStr = new StringBuilder();
//                cmds.add( bin + File.separator +"pg_ctl");//
//                cmdStr.append("\"").append(bin).append(File.separator).append("pg_ctl.exe\" ");
//                if (isStart)
//                {
//                    cmds.add("start");
//                    cmdStr.append("start ");
//                } else
//                {
//                    cmds.add("stop");
//                    cmdStr.append("stop ");
//                }
//                cmds.add("-t");
//                cmds.add("5");
//                cmds.add("-D");
//                cmds.add(data);
//                cmds.add("-w");               
//                cmdStr.append("-D \"").append(data).append("\" -w");//.append(" -m fast");
//                docs.insertString(docs.getLength(), cmdStr.toString() + System.lineSeparator(), attr);
//                logger.info(cmdStr.toString());
//                ProcessBuilder pb = new ProcessBuilder(cmds);
//                //Map<String,String> map=pb.environment();
//                //map.put("PGPASSWORD", helperInfo.getPwd());
//                //File log = new File("loglyy");
//                pb.redirectErrorStream(true);//将错误流与标准输出流合并到一起
//                //pb.redirectOutput(ProcessBuilder.Redirect.to(log));
//                proc = pb.start();
            InputStream is = proc.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "GBK"));//"GBK"
            String line;
            while ((line = bufferedReader.readLine()) != null)
            {
                logger.info(line);
//                if (isStart && (line.equals("server starting") || line.equals(constBundle.getString("serverStarted"))))
//                {
//                    break;
//                } else if (!isStart && (line.equals("server stopped") || line.equals(constBundle.getString("serverStopped"))))
//                {
//                    break;
//                }
            }
            logger.info("wait for:" + proc.waitFor());
            Integer exitValue = proc.exitValue();
            logger.info("exit value=" + exitValue);//the value null or 0 indicates normal termination.     
        } catch (IOException | InterruptedException ex)
        {
            if (proc != null)
            {
                proc.destroy();
            }
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }

    public static void main(String[] args)
    {
        Test t = new Test();
        t.connect();
        
//        VariableInfoDTO a = new VariableInfoDTO();
//        VariableInfoDTO b = new VariableInfoDTO();
//        b.setDb(" ");
//        System.out.println(Compare.getInstance().equal(a, b));

//        String s="java.sql.SQLException: com.highgo.jdbc.HGDBException: Connection to 127.0.0.1:5433 refused. Check that the hostname and port are correct and that the postmaster is accepting TCP/IP connections.";
//        String ss= s.replaceAll("[\\,\\ S;.!?\"] +",System.lineSeparator());
//        System.out.println(""+ss);
//        char[] a1={'a'};
//        char[] a2 = {'a'};
//        System.out.println(""+Arrays.equals(a1, a2));
//        
//        
//        
//        List<String> a = new ArrayList();
//        a.add("a");
//        a.add("b");
//        
//        List<String> b = new ArrayList();;
//        b.add("b");
//        b.add("a");
//        
//        List<String> aa= a;
//        System.out.println("===="+aa.size()+","+a.size());
//        aa.remove(1);
//        System.out.println("===="+aa.size()+","+a.size());

//        String f = "D:\\Program Files (x86)\\HighGo\\Database\\1.3\\bin";
//        f=f+File.separator+"runpsql.bat";
//        File ff= new File(f);
//        System.out.println("========="+ff.exists());
        
//      HelperInfoDTO a = new HelperInfoDTO();
//      a.setRelationOid(3);
//       HelperInfoDTO b = new HelperInfoDTO();
//       b.setRelationOid(3);
//        System.out.println(""+a.getRelationOid()+","+b.getRelationOid()+","+(a.getRelationOid()==b.getRelationOid()));
//        String s = "{random_page_cost=1,seq_page_cost=2}";
//        System.out.println(""+s.substring(1, s.length()-1));
//        LinkedHashMap hm = new LinkedHashMap<>();
//        System.out.println("----------containt id="+hm.containsKey("id"));
//        hm.put("id","public.a1");
//        System.out.println("----------containt id="+hm.containsKey("id"));
//        hm.put("age", "public.a0");
//        System.out.println("-----------has age="+hm.get("age"));
//        hm.put("id","public.a2");
//        System.out.println("-----------has id="+hm.get("id2"));
//        String s="selectfrominto";
//        System.out.println(""+s.matches("select.*into.*from.*"));
        
//        String s= "AFTER INSERT OR UPDATE ";
//        System.out.println("==="+s.split("OF").length);
//        String col =s.split("OF")[1];
//        System.out.println("----------"+col);
//        String[] cols = col.trim().split(",");
//        for(String c : cols)
//        {
//            System.out.println("============"+c);            
//        }
//        BASE64Encoder base64en ;
//        {
//            md5 = MessageDigest.getInstance("MD5");
//            base64en = new BASE64Encoder();
//            //加密后的字符串
//            String newstr = base64en.encode(md5.digest(str.getBytes("utf-8")));
//            System.out.println("" + newstr);
//        } catch (NoSuchAlgorithmException ex)
//        {
//            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (UnsupportedEncodingException ex)
//         {
//             Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
//         }
//        List<Object[]> list=new ArrayList();
//        list.add(new Object[]{1,2,3});
//        Object[] a = new Object[list.get(0).length];
//        for(int i=0;i<list.get(0).length;i++)
//        {
//            a[i] = list.get(0)[i];
//        }
//        System.out.println("================="+a[0]);
//        list.get(0)[0]=5;
//        System.out.println("================="+a[0]);
//        Object[] b= new Object[]{1,2,3};
//        System.out.println(""+Arrays.equals(a, b) );
        /*
        Object o=null;
        System.out.println((String)o);
        
        String sss= " HighGo Database 2.0.1 Linux 64-bit";
        System.out.println(""+sss.split(",")[0]);
        
        String s = ";select 11122;";
        System.out.println(s.lastIndexOf(";")+"---"+s.length());
        System.out.println(""+s.substring(0, s.length()));
        
        File f= new File("");
        System.out.println(f.getAbsolutePath());
        JFileChooser fc= new JFileChooser(new File(f.getAbsolutePath()));
        System.out.println("fc="+fc.toString());
        */
    }    
}
