/* ------------------------------------------------
 *
 * File: MyTableCellEditor.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/other/MyTableCellEditor.java
 *
 *-------------------------------------------------
 */

package other;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellEditor;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class MyTableCellEditor extends AbstractCellEditor
        implements TableCellEditor,  KeyListener,MouseListener
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private JTextField button;
    private String old;
    public MyTableCellEditor()
    {
        //Set up the editor (from the table's point of view),
        //which is a button.
        //This button brings up the color chooser dialog,
        //which is the editor from the user's point of view.
        button = new JTextField("lyy");
        old=button.getText();
        button.addKeyListener(this);
    }
 
    //Implement the one CellEditor method that AbstractCellEditor doesn't.
    @Override
    public Object getCellEditorValue()
    {
        return button.getText();
    }

    //Implement the one method defined by TableCellEditor.
    @Override
    public Component getTableCellEditorComponent(JTable table,
            Object value,
            boolean isSelected,
            int row,
            int column)
    {
        return button;
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
       logger.info("key typed============="+e.getKeyChar());
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        logger.info("KEY RELEASED=================" + old + "," + button.getText());
        if (old.equals(button.getText()))
        {
            button.setBackground(Color.WHITE);
        } else
        {
            button.setBackground(Color.red);
        }
    }
    
    
    public boolean isChanged()
    {
        return !old.equals(button.getText());
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        
    }

    @Override
    public void mousePressed(MouseEvent e)
    {
       
    }

    @Override
    public void mouseReleased(MouseEvent e)
    {
        
    }

    @Override
    public void mouseEntered(MouseEvent e)
    {
        
    }

    @Override
    public void mouseExited(MouseEvent e)
    {
        
    }
    
    
}
