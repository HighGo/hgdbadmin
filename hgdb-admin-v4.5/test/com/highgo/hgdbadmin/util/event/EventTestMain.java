/* ------------------------------------------------
 *
 * File: EventTestMain.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/util/event/EventTestMain.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.util.event;

import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;

/**
 *
 * @author Fanyi
 */
public class EventTestMain
{
    private SimpleEventBus eventBus;

    public EventTestMain(SimpleEventBus eventBus)
    {
        this.eventBus = eventBus;
    }

    public void fireEvent()
    {
        eventBus.fireEvent(new TreeNodeChangeEvent(null));
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        SimpleEventBus eventBus = new SimpleEventBus();
        EventTestMain main = new EventTestMain(eventBus);
        EventTest1 test1 = new EventTest1(eventBus);
        EventTest2 test2 = new EventTest2(eventBus);

        main.fireEvent();
    }
}
