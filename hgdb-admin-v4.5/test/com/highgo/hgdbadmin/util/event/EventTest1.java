/* ------------------------------------------------
 *
 * File: EventTest1.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/util/event/EventTest1.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.util.event;

import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;
import com.highgo.hgdbadmin.event.TreeNodeChangeEventHandler;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 *
 * @author Fanyi
 */
public class EventTest1
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private SimpleEventBus eventBus;

    public EventTest1(SimpleEventBus eventBus)
    {
        this.eventBus = eventBus;
        addHandlder();
    }

    private void addHandlder()
    {
        eventBus.addHandler(TreeNodeChangeEvent.TYPE, new TreeNodeChangeEventHandler()
        {
            @Override
            public void onChanged(TreeNodeChangeEvent event)
            {
                logger.info("Handler 1...." + event.getNodeObject());
            }
        });
    }

}
