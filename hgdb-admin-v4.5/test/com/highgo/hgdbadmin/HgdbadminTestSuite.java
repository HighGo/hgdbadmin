/* ------------------------------------------------
 *
 * File: HgdbadminTestSuite.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/HgdbadminTestSuite.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin;

import com.highgo.hgdbadmin.controller.TreeControllerTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Liu Yuanyuan
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
{
    TreeControllerTest.class

})
public class HgdbadminTestSuite
{

    @BeforeClass
    public static void setUpClass() throws Exception
    {
    }

    @AfterClass
    public static void tearDownClass() throws Exception
    {
    }

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

}
