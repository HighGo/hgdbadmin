/* ------------------------------------------------
 *
 * File: MigrationControllerTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/migrationassistant/controller/MigrationControllerTest.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DataTypeCastDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ObjInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ProgressInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jasmine
 */
public class MigrationControllerTest
{

    public MigrationControllerTest()
    {
    }

    @BeforeClass
    public static void setUpClass()
    {
    }

    @AfterClass
    public static void tearDownClass()
    {
    }

    @Before
    public void setUp()
    {
    }

    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInstance method, of class MigrationController.
     */
    @Test
    public void testGetInstance()
    {
        System.out.println("getInstance");
        MigrationController expResult = null;
        MigrationController result = MigrationController.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getDefaultDataTypeCastList method, of class MigrationController.
     */
    @Test
    public void testGetDefaultDataTypeCastList()
    {
        System.out.println("getDefaultDataTypeCastList");
        ObjEnum.DBType dbType = null;
        MigrationController instance = null;
        List expResult = null;
        List result = instance.getDefaultDataTypeCastList(dbType);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getObjListDTO method, of class MigrationController.
     */
    @Test
    public void testGetObjectInfoList() throws Exception
    {
        System.out.println("getObjectInfoList");
        DBConnInfoDTO dbInfo = null;
        MigrationController instance = null;
        Object expResult = null;
        Object result = instance.getObjListDTO(dbInfo);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getProgressInfo method, of class MigrationController.
     */
    @Test
    public void testGetProgressInfo()
    {
        System.out.println("getProgressInfo");
        MigrationController instance = null;
        ProgressInfoDTO expResult = null;
        ProgressInfoDTO result = instance.getProgressInfo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of startMigrateThread method, of class MigrationController.
     */
    @Test
    public void testStartMigrateThread()
    {
        System.out.println("startMigrateThread");
        String logPath = "";
        DBConnInfoDTO sourceDBInfo = null;
        DBConnInfoDTO hgDBInfo = null;
        List<ObjInfoDTO> choosedObjInfoList = null;
        List<DataTypeCastDTO> dataTypeCastList = null;
        String migrateModel = "";
        int copyBatch = 0;
        int insertBatch = 0;
        MigrationController instance = null;
        //instance.startMigrateThread(logPath, sourceDBInfo, hgDBInfo, choosedObjInfoList, dataTypeCastList,
        //        migrateModel, copyBatch, insertBatch);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of stopMigrateThread method, of class MigrationController.
     */
    @Test
    public void testStopMigrateThread()
    {
        System.out.println("stopMigrateThread");
        MigrationController instance = null;
        instance.stopMigrateThread();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
    
}