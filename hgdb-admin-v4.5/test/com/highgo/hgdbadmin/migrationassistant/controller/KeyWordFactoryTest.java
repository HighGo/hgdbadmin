/* ------------------------------------------------
 *
 * File: KeyWordFactoryTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/migrationassistant/controller/KeyWordFactoryTest.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yuanyuan
 */
public class KeyWordFactoryTest
{

    public KeyWordFactoryTest()
    {        
    }
    @BeforeClass
    public static void setUpClass()
    {
    }
    @AfterClass
    public static void tearDownClass()
    {
    }
    @Before
    public void setUp()
    {
    }
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInstance method, of class KeyWordFactory.
     */
    @Test
    public void testGetInstance()
    {
        System.out.println("getInstance");
        KeyWordFactory expResult = KeyWordFactory.getInstance();
        KeyWordFactory result = KeyWordFactory.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getColNameKeyWords method, of class KeyWordFactory.
     */
    @Test
    public void testGetColNameKeyWords()
    {
        System.out.println("getColNameKeyWords");
        ObjEnum.DBType dbType = ObjEnum.DBType.HGDB;
        KeyWordFactory instance = KeyWordFactory.getInstance();
        int expResult = 123;
        int result = instance.getColNameKeyWords(dbType).size();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getKeyWords method, of class KeyWordFactory.
     */
    @Test
    public void testGetKeyWords()
    {
        System.out.println("getKeyWords");
        ObjEnum.DBType dbType = ObjEnum.DBType.HGDB;
        KeyWordFactory instance = KeyWordFactory.getInstance();
        int expResult = 123;
        int result = instance.getKeyWords(dbType).size();
        assertEquals(expResult, result);
    }
}
