/* ------------------------------------------------
 *
 * File: SQLFactoryTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/migrationassistant/controller/SQLFactoryTest.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yuanyuan
 */
public class SQLFactoryTest
{
    
    public SQLFactoryTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInstance method, of class SQLFactory.
     */
    @Test
    public void testGetInstance()
    {
        System.out.println("getInstance");
        SQLFactory expResult = SQLFactory.getInstance();
        SQLFactory result = SQLFactory.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getObjSQL method, of class SQLFactory.
     */
    @Test
    public void testGetObjSQL()
    {
        System.out.println("getObjSQL");
        ObjEnum.DBType db = ObjEnum.DBType.ORACLE;
        ObjEnum.DBObj obj = ObjEnum.DBObj.SCHEMA;
        SQLFactory instance = SQLFactory.getInstance();
        String expResult = "SELECT DISTINCT user,user FROM user_objects";
        String result = instance.getObjSQL(db, obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getSelectSQL method, of class SQLFactory.
     */
    @Test
    public void testGetSelectSQL()
    {
        System.out.println("getSelectSQL");
        ObjEnum.DBType db = ObjEnum.DBType.ORACLE;
        ObjEnum.ObjType obj = ObjEnum.ObjType.SCHEMA;
        SQLFactory instance = SQLFactory.getInstance();
        String expResult = "SELECT DISTINCT user,user FROM user_objects";
        String result = instance.getSelectSQL(db, obj);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
