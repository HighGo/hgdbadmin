/* ------------------------------------------------
 *
 * File: DataTypeFactoryTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/migrationassistant/controller/DataTypeFactoryTest.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yuanyuan
 */
public class DataTypeFactoryTest
{
    
    public DataTypeFactoryTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInstance method, of class DataTypeFactory.
     */
    @Test
    public void testGetInstance()
    {
        System.out.println("getInstance");
        DataTypeFactory expResult = null;
        DataTypeFactory result = DataTypeFactory.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTypeCastList method, of class DataTypeFactory.
     */
    @Test
    public void testGetTypeCastList()
    {
        System.out.println("getTypeCastList");
        ObjEnum.DBType sourceDB = null;
        DataTypeFactory instance = null;
        List<List<String>> expResult = null;
        List<List<String>> result = instance.getTypeCastList(sourceDB);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
