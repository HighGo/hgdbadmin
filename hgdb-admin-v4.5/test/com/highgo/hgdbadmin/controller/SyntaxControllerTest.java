/* ------------------------------------------------
 *
 * File: SyntaxControllerTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/controller/SyntaxControllerTest.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yuanyuan
 */
public class SyntaxControllerTest
{
    
    public SyntaxControllerTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInstance method, of class SyntaxController.
     */
    @Test
    public void testGetInstance()
    {
        System.out.println("getInstance");
        SyntaxController expResult = SyntaxController.getInstance();
        SyntaxController result = SyntaxController.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getName method, of class SyntaxController.
     */
    @Test
    public void testGetName()
    {
        System.out.println("getName");
        String unquotedName = "ABC";
        SyntaxController instance = new SyntaxController();
        String expResult = "\"ABC\"";
        String result = instance.getName(unquotedName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of isReservedKeyWord method, of class SyntaxController.
     */
    @Test
    public void testIsReservedKeyWord()
    {
        System.out.println("isReservedKeyWord");
        String word = "FROM";
        SyntaxController instance = SyntaxController.getInstance();
        boolean expResult = true;
        boolean result = instance.isReservedKeyWord(word);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of isNonReservedKeyWord method, of class SyntaxController.
     */
    @Test
    public void testIsNonReservedKeyWord()
    {
        System.out.println("isNonReservedKeyWord");
        String word = "ADD";
        SyntaxController instance = new SyntaxController();
        boolean expResult = true;
        boolean result;// = instance.isNonReservedKeyWord(word);
        //assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of insert method, of class SyntaxController.
     */
    @Test
    public void testInsert()
    {
        System.out.println("insert");
//        Document docs = null;
//        String str = "";
//        AttributeSet attrset = null;
//        SyntaxController instance = new SyntaxController();
//        instance.insert(docs, str, attrset);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setComment method, of class SyntaxController.
     */
    @Test
    public void testSetComment()
    {
        System.out.println("setComment");
//        Document docs = null;
//        String str = "";
//        SyntaxController instance = new SyntaxController();
//        instance.setComment(docs, str);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of setDefine method, of class SyntaxController.
     */
    @Test
    public void testSetDefine()
    {
        System.out.println("setDefine");
//        Document docs = null;
//        String define = "";
//        SyntaxController instance = new SyntaxController();
//        instance.setDefine(docs, define);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
