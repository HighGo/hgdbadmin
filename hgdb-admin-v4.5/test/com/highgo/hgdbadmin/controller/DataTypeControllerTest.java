/* ------------------------------------------------
 *
 * File: DataTypeControllerTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/controller/DataTypeControllerTest.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.controller;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yuanyuan
 */
public class DataTypeControllerTest
{
    
    public DataTypeControllerTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getInstance method, of class DataTypeController.
     */
    @Test
    public void testGetInstance()
    {
        System.out.println("getInstance");
        DataTypeController expResult = DataTypeController.getInstance();
        DataTypeController result = DataTypeController.getInstance();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getDiaplayTypeFromInternalType method, of class DataTypeController.
     */
    @Test
    public void testGetDisplayTypeFromInternalType()
    {
        System.out.println("getDiaplayTypeFromInternalType");
        String typeName = "int4";
        String defaultValue = "nextval:";
        String expResult = "serial";
        String result = DataTypeController.getInstance().getDisplayTypeFromInternalType(typeName, defaultValue);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getTypeClassFromInternalType method, of class DataTypeController.
     */
    @Test
    public void testGetTypeClassFromInternalType()
    {
        System.out.println("getTypeClassFromInternalType");
        String typeName = "int2";
        Class expResult = Integer.class;
        Class result = DataTypeController.getInstance().getTypeClassFromInternalType(typeName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
