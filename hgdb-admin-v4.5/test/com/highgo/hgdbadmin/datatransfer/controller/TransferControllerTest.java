/* ------------------------------------------------
 *
 * File: TransferControllerTest.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       test/com/highgo/hgdbadmin/datatransfer/controller/TransferControllerTest.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datatransfer.controller;

import com.highgo.hgdbadmin.datatransfer.model.ColumnDTO;
import com.highgo.hgdbadmin.datatransfer.model.FormatInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import java.util.List;
import javax.swing.text.Document;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Yuanyuan
 */
public class TransferControllerTest
{
    
    public TransferControllerTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of getState method, of class TransferController.
     */
    @Test
    public void testGetState()
    {
        System.out.println("getState");
        TransferController instance = new TransferController();
        int expResult = 0;
        int result = instance.getState();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of pause method, of class TransferController.
     */
    @Test
    public void testPause() throws Exception
    {
        System.out.println("pause");
        TransferController instance = new TransferController();
        instance.pause();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of continues method, of class TransferController.
     */
    @Test
    public void testContinues() throws Exception
    {
        System.out.println("continues");
        TransferController instance = new TransferController();
        instance.continues();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of stop method, of class TransferController.
     */
    @Test
    public void testStop() throws Exception
    {
        System.out.println("stop");
        TransferController instance = new TransferController();
        instance.stop();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of exportDataToFile method, of class TransferController.
     */
    @Test
    public void testExportDataToFile()
    {
        System.out.println("exportDataToFile");
        HelperInfoDTO helperInfo = null;
        String relationName = "";
        List<ColumnDTO> columnList = null;
        FormatInfoDTO format = null;
        Document docs = null;
        TransferController instance = new TransferController();
        instance.exportDataToFile(helperInfo, relationName, columnList, format, docs);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getHeaderArrayFromFile method, of class TransferController.
     */
    @Test
    public void testGetHeaderArrayFromFile() throws Exception
    {
        System.out.println("getHeaderArrayFromFile");
        FormatInfoDTO format = null;
        TransferController instance = new TransferController();
        Object[] expResult = null;
        Object[] result = instance.getHeaderArrayFromFile(format);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of importDataFromFile method, of class TransferController.
     */
    @Test
    public void testImportDataFromFile()
    {
        System.out.println("importDataFromFile");
        HelperInfoDTO helperInfo = null;
        String relationName = "";
        List<ColumnDTO> columnList = null;
        FormatInfoDTO format = null;
        Document docs = null;
        TransferController instance = new TransferController();
        instance.importDataFromFile(helperInfo, relationName, columnList, format, docs);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
