/* ------------------------------------------------
 *
 * File: SaveDataTypeCast2XML.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/migrationassistant/util/SaveDataTypeCast2XML.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.util;

import java.io.FileWriter;
import java.io.IOException;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author Liu Yuanyuan
 */
public class SaveDataTypeCast2XML
{

    public static void main(String[] args)
    {
        Document document = DocumentHelper.createDocument();

        Element eleDatatype = document.addElement("datatype");
        Element eleOther2HG;
        Element eleCast;

        eleOther2HG = eleDatatype.addElement("Oracle2HighGo");
        eleCast = eleOther2HG.addElement("o1");
        eleCast.addAttribute("HighGo", "TIMESTAMP WITH TIME ZONE");
        eleCast.addAttribute("Oracle", "TIMESTAMP(6) WITH TIME ZONE");
        eleCast = eleOther2HG.addElement("o2");
        eleCast.addAttribute("HighGo", "TIMESTAMP WITH TIME ZONE");
        eleCast.addAttribute("Oracle", "TIMESTAMP(6) WITH LOCAL TIME ZONE");
        eleOther2HG.setText("Oracle2HighGo(" + eleOther2HG.nodeCount() + ")");
        System.out.println(eleOther2HG.getText());

        eleOther2HG = eleDatatype.addElement("SQLServer2HighGo");
        eleCast = eleOther2HG.addElement("s1");
        eleCast.addAttribute("HighGo", "bingint");
        eleCast.addAttribute("SQLServer", "bigint");
        eleCast = eleOther2HG.addElement("s2");
        eleCast.addAttribute("HighGo", "bytea");
        eleCast.addAttribute("SQLServer", "binary");
        eleOther2HG.setText("SQLServer2HighGo(" + eleOther2HG.nodeCount() + ")");
        System.out.println(eleOther2HG.getText());

        eleOther2HG = eleDatatype.addElement("MySQL2HighGo");
        eleOther2HG.setText("MySQL2HighGo(" + eleOther2HG.nodeCount() + ")");
        System.out.println(eleOther2HG.getText());

        try
        {
            //System.out.println(System.getProperty("user.dir"));
            XMLWriter output = new XMLWriter(new FileWriter(System.getProperty("user.dir") + "/datatype.xml"));
            output.write(document);
            output.close();
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
