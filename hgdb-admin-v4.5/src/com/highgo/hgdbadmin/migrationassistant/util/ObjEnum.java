/* ------------------------------------------------
 *
 * File: TypeCastDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/migrationassistant/util/TypeCastDialog.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.util;

/**
 *
 * @author Liu Yuanyuan
 */
public class ObjEnum
{
    public enum DBType
    {
        HGDB, ORACLE, SQLSERVER
    }

    public enum DBObj
    {
        SCHEMA, TABLE, SEQUENCE, VIEW, INDEX, PROCEDURE, FUNCTION, TRIGGER
    }

    //this order is design for object list sort,so care of changing this order
    public enum ObjType
    {
        SCHEMA, TABLE,
        COLUMN, CONSTRAINT_MAIN, CONSTRAINT_COLUMN, CONSTRAINT_CHECKCONDITION, CONSTRAINT_RCOLUMN,
        SEQUENCE, VIEW, INDEX, PROCEDURE, FUNCTION, TRIGGER,
        SCRIPT_OBJ
    }

}
