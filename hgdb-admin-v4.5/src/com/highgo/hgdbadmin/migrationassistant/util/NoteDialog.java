/* ------------------------------------------------
 *
 * File: ObjEnum.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/migrationassistant/util/ObjEnum.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.util;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author Liu Yuanyuan
 */
public class NoteDialog extends JDialog
{

    public NoteDialog(JFrame parent)
    {
        super(parent);
        setTitle("analyzing data...");
        setSize(450, 70);
        JProgressBar pbar = new JProgressBar();
        pbar.setSize(new Dimension(400, 25));
        pbar.setIndeterminate(true);
        add(pbar);
        pbar.setVisible(true);
    }
}
