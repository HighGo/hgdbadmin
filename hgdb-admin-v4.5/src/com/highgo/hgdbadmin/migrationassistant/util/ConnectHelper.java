/* ------------------------------------------------
 *
 * File: ConnectHelper.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/migrationassistant/util/ConnectHelper.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.util;

import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author Liu Yuanyuan
 */
public class ConnectHelper
{

    public static Connection getConnection(DBConnInfoDTO dbInfo) throws Exception
    {
        String driver = null;
        String url = null;        
        Connection conn = null;
        switch (dbInfo.getDBType())
        {
            case HGDB:
               Properties props = new Properties();
               props.setProperty("user",dbInfo.getUser());
               props.setProperty("password",dbInfo.getPwd());               
               System.out.println("onSSL=" + dbInfo.isOnSSL());
               if(dbInfo.isOnSSL())//2016.7 add  the follwing two paramenters to conn hgdb on ssl
               {
                    props.setProperty("ssl", "true");
                    props.setProperty("sslfactory", "com.highgo.jdbc.ssl.NonValidatingFactory");
               }
               driver = "com.highgo.jdbc.Driver";//"org.postgresql.Driver";
               url = "jdbc:highgo://" + dbInfo.getHost() + ":" + dbInfo.getPort() + "/" + dbInfo.getDb();
               
               Class.forName(driver);
               conn = DriverManager.getConnection(url, props);
               return conn;
            case ORACLE:
                driver = "oracle.jdbc.driver.OracleDriver";
                url = "jdbc:oracle:thin:@" + dbInfo.getHost() + ":" + dbInfo.getPort() + ":" + dbInfo.getDb();
               
                Class.forName(driver);
                conn = DriverManager.getConnection(url, dbInfo.getUser(), dbInfo.getPwd());
                return conn;
            case SQLSERVER:
                driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                url = "jdbc:sqlserver://" + dbInfo.getHost() + ":" + dbInfo.getPort() + ";DatabaseName = " + dbInfo.getDb();
                
                Class.forName(driver);
                conn = DriverManager.getConnection(url, dbInfo.getUser(), dbInfo.getPwd());
                return conn;
            default:
                return null;
        }
    }
}
