/* ------------------------------------------------
 *
 * File: TypeCastDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/migrationassistant/util/TypeCastDialog.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.util;

import com.highgo.hgdbadmin.migrationassistant.model.DataTypeCastDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DataTypeDTO;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yaunyuan
 */
public class TypeCastDialog extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private DataTypeCastDTO dataTypeCastDTO;
    private boolean isClickUse;
    private JTextField tfdHGPrecision;
    private JTextField tfdHGScale;
    private JTextField tfdHGType;
    private JTextField tfdSourcePrecision;
    private JTextField tfdSourceScale;
    private JTextField tfdSourceType;

    public TypeCastDialog(JFrame parent, Icon icon, String title,
            DataTypeCastDTO dataTypeCastDTO)
    {
        super(parent, true);
        this.dataTypeCastDTO = dataTypeCastDTO;
        JButton btnCancle;
        JButton btnUse;
        JLabel lblHGPrecision;
        JLabel lblHGScale;
        JLabel lblHGType;
        JLabel lblSourcePrecision;
        JLabel lblSourceScale;
        JLabel lblSourceType;

        isClickUse = false;
        btnUse = new JButton(constBundle.getString("use"));
        btnCancle = new JButton(constBundle.getString("cancle"));
        lblSourceType = new JLabel(constBundle.getString("sourceType"));
        lblSourcePrecision = new JLabel(constBundle.getString("precision"));
        lblSourceScale = new JLabel(constBundle.getString("scale"));
        lblHGType = new JLabel(constBundle.getString("hgType"));
        lblHGPrecision = new JLabel(constBundle.getString("precision"));
        lblHGScale = new JLabel(constBundle.getString("scale"));

        Font font = new Font("宋体", 0, 13);
        btnUse.setFont(font);
        btnCancle.setFont(font);
        lblSourceType.setFont(font);
        lblSourcePrecision.setFont(font);
        lblSourceScale.setFont(font);
        lblHGType.setFont(font);
        lblHGPrecision.setFont(font);
        lblHGScale.setFont(font);

        tfdSourceType = new JTextField(dataTypeCastDTO.getSourceDataType().getDataType());
        tfdSourcePrecision = new JTextField(dataTypeCastDTO.getSourceDataType().getDataPrecision());
        tfdSourceScale = new JTextField(dataTypeCastDTO.getSourceDataType().getDataScale());
        tfdHGType = new JTextField(dataTypeCastDTO.getHgDataType().getDataType());
        tfdHGPrecision = new JTextField(dataTypeCastDTO.getHgDataType().getDataPrecision());
        tfdHGScale = new JTextField(dataTypeCastDTO.getHgDataType().getDataScale());
        tfdSourceType.setEnabled(!(dataTypeCastDTO.isIsSystem()));

        lblSourcePrecision.setEnabled(false);
        lblSourceScale.setEnabled(false);
        lblHGPrecision.setEnabled(false);
        lblHGScale.setEnabled(false);
        tfdSourcePrecision.setEnabled(false);
        tfdSourceScale.setEnabled(false);
        tfdHGPrecision.setEnabled(false);
        tfdHGScale.setEnabled(false);

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setSize(415, 165);
        setTitle(title);
        getContentPane().setLayout(new GridBagLayout());

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(lblSourceScale, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSourcePrecision, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSourceType, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(tfdSourceType, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfdSourcePrecision, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfdSourceScale, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(lblHGScale, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblHGPrecision, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblHGType, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(tfdHGScale, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfdHGPrecision, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfdHGType, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createSequentialGroup()
                .addComponent(btnUse, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancle, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23)));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(tfdSourceType, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSourceType, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(tfdSourcePrecision, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSourcePrecision, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(tfdSourceScale, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSourceScale, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(tfdHGType, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblHGType, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(tfdHGPrecision, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblHGPrecision, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(tfdHGScale, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(lblHGScale, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 15, GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(btnUse, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)));

        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                logger.info(evt.getActionCommand());
                dispose();
            }
        });

        btnUse.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                logger.info(evt.getActionCommand());
                dispose();
                isClickUse = true;
            }
        });
    }

    public boolean isClickUse()
    {
        return isClickUse;
    }

    public DataTypeCastDTO getDataTypeCastDTO()
    {
        DataTypeDTO sourceTypeDTO = new DataTypeDTO();
        sourceTypeDTO.setDataType(tfdSourceType.getText());
        if (!"".equals(tfdSourcePrecision.getText()))
        {
            sourceTypeDTO.setDataPrecision(Integer.valueOf(tfdSourcePrecision.getText()));
        }
        if (!"".equals(tfdSourceScale.getText()))
        {
            sourceTypeDTO.setDataScale(Integer.valueOf(tfdSourceScale.getText()));
        }

        DataTypeDTO hgTypeDTO = new DataTypeDTO();
        hgTypeDTO.setDataType(tfdHGType.getText());
        if (!"".equals(tfdHGPrecision.getText()))
        {
            hgTypeDTO.setDataPrecision(Integer.valueOf(tfdHGPrecision.getText()));
        }
        if (!"".equals(tfdHGScale.getText()))
        {
            hgTypeDTO.setDataScale(Integer.valueOf(tfdHGScale.getText()));
        }

        DataTypeCastDTO returnDataTypeCastDTO = new DataTypeCastDTO();
        returnDataTypeCastDTO.setSourceDB(dataTypeCastDTO.getSourceDB());
        returnDataTypeCastDTO.setSourceDataType(sourceTypeDTO);
        returnDataTypeCastDTO.setHgDataType(hgTypeDTO);
        returnDataTypeCastDTO.setIsSystem(dataTypeCastDTO.isIsSystem());

        return returnDataTypeCastDTO;
    }
}
