/* ------------------------------------------------
 *
 * File: SourceDBPanel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\SourceDBPanel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class SourceDBPanel extends JPanel
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    
    private JComboBox cbDBType;
    private JTextField tfHost;
    private JTextField tfPort;
    private JTextField tfDB;
    private JTextField tfUser;
    private JPasswordField pfPwd;

    public SourceDBPanel()
    {
        initCompont();
        tfPort.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                //can only enter digit (>=0)
                txfdIntegerValueKeyTyped(evt);
            }
        });
    }

    private void initCompont()
    {
        JLabel lblDBType;
        JLabel lblHost;
        JLabel lblPort;
        JLabel lblDB;
        JLabel lblUser;
        JLabel lblPwd;

        lblDBType = new JLabel();
        lblHost = new JLabel();
        lblPort = new JLabel();
        lblDB = new JLabel();
        lblUser = new JLabel();
        lblPwd = new JLabel();
        cbDBType = new JComboBox();
        tfHost = new JTextField(20);
        tfPort = new JTextField(4);
        tfDB = new JTextField(20);
        tfUser = new JTextField(20);
        pfPwd = new JPasswordField(20);

        Font font = new Font(constBundle.getString("fontSongTi"), 0, 14);
        lblDBType.setFont(font);
        lblHost.setFont(font);
        lblPort.setFont(font);
        lblDB.setFont(font);
        lblUser.setFont(font);
        lblPwd.setFont(font);

        lblDBType.setText(constBundle.getString("dbType"));
        lblHost.setText(constBundle.getString("host"));
        lblPort.setText(constBundle.getString("port"));
        lblDB.setText(constBundle.getString("db"));
        lblUser.setText(constBundle.getString("user"));
        lblPwd.setText(constBundle.getString("pwd"));

        cbDBType.addItem(ObjEnum.DBType.ORACLE);
        //cbDBType.addItem(ObjEnum.DBType.SQLSERVER);//add sql server later
        //tfHost.setText("192.168.136.128");
        tfPort.setText("1521");
        tfDB.setText("orcl");
        //tfUser.setText("lyy");
        //pfPwd.setText("lyy");

        setPreferredSize(new Dimension(550, 390));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGap(70, 70, 70)// .addContainerGap(75, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(lblPwd, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(lblUser, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(lblDB, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(lblPort, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(lblHost, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                .addComponent(lblDBType, GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(cbDBType, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)//GroupLayout.PREFERRED_SIZE
                .addComponent(tfHost, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)
                .addComponent(tfPort, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)
                .addComponent(tfDB, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)
                .addComponent(tfUser, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE)
                .addComponent(pfPwd, GroupLayout.PREFERRED_SIZE, 300, Short.MAX_VALUE))
                .addGap(75, 75, 75)) // .addGroup(layout.createSequentialGroup()
                //.addContainerGap()
                //.addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                // .addContainerGap())
                );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                //.addContainerGap()
                //.addComponent(lblTitle)
                .addGap(50, 50, 50)//  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblDBType, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                .addComponent(cbDBType, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblHost, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfHost, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblPort, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfPort, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblDB, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfDB, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblUser, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                .addComponent(tfUser, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblPwd, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                .addComponent(pfPwd, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(140, Short.MAX_VALUE)));
    }

    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could only be entered integer(>=0).
        char keyCh = evt.getKeyChar();
        Character.isDigit(keyCh);
        //logger.debug("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh == '-')
            {
                evt.setKeyChar('\0');
            } else if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }       
    }
    
    public DBConnInfoDTO getSourceDBConnInfoDTO()
    {
        logger.info("Enter");
        DBConnInfoDTO hgdbInfo = new DBConnInfoDTO();
        hgdbInfo.setDBType((ObjEnum.DBType) cbDBType.getSelectedItem());
        hgdbInfo.setHost(tfHost.getText());
        hgdbInfo.setPort(tfPort.getText());
        hgdbInfo.setDb(tfDB.getText());
        hgdbInfo.setUser(tfUser.getText());
        hgdbInfo.setPwd(new String(pfPwd.getPassword()));
        logger.info("Return");
        return hgdbInfo;
    }
}
