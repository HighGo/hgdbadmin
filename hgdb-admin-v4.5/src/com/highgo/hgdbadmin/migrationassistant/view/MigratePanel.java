/* ------------------------------------------------
 *
 * File: MigratePanel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\MigratePanel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.migrationassistant.controller.MigrationController;
import com.highgo.hgdbadmin.migrationassistant.model.ProgressInfoDTO;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class MigratePanel extends JPanel
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private JLabel lblState;
    private JButton btnDetail;
    private JScrollPane spTxa;
    private JTextArea txaDetail;
    private JProgressBar progressBar;
    private Timer timer;
    private boolean isFirst = true;
    private boolean isFirstDiffer = true;

    public MigratePanel()
    {
        initCompont();
    }

    private void initCompont()
    {
        progressBar = new JProgressBar();
        lblState = new JLabel();
        btnDetail = new JButton();
        txaDetail = new JTextArea();
        spTxa = new JScrollPane();
        spTxa.setViewportView(txaDetail);

        progressBar.setMinimum(0);
//        progressBar.setMaximum(100);
//        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        progressBar.setBorderPainted(true);
        progressBar.setBackground(Color.WHITE);
//        progressBar.setForeground(new Color(222,231,209));
        lblState.setHorizontalAlignment(SwingConstants.CENTER);
        btnDetail.setText(constBundle.getString("expandDetails"));//"∨"
        txaDetail.setText("Overall Progress of this migration \n\r");
        btnDetail.addActionListener(new java.awt.event.ActionListener()
        {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnDetailActionPerformed(evt);
            }
        });
        txaDetail.setEditable(false);
        spTxa.setVisible(false);


        ActionListener taskPerformer = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                timerActionPerformed(evt);
            }
        };
        timer = new Timer(50, taskPerformer);

        setPreferredSize(new Dimension(550, 380));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);

        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(spTxa, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addComponent(progressBar, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                .addComponent(lblState, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDetail)))
                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblState, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnDetail))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spTxa, GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addContainerGap()));

    }

    private void btnDetailActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        if (btnDetail.getText().equals(constBundle.getString("expandDetails")))//"∨"
        {
            spTxa.setVisible(true);
            btnDetail.setText(constBundle.getString("collapseDetails"));//"∧"
        }
        else
        {
            spTxa.setVisible(false);
            btnDetail.setText(constBundle.getString("expandDetails"));//"∨"
        }
    }

    private void timerActionPerformed(ActionEvent evt)
    {
//        logger.info("value = " + progressBar.getValue());
        MigrationController mc = MigrationController.getInstance();
        ProgressInfoDTO progressInfo = mc.getProgressInfo();
        lblState.setText(progressInfo.getState());
        if (progressInfo.getMaxValue() > 0)
        {
            if (isFirst)
            {
                int objMax = progressInfo.getMaxValue();
                logger.info("objMax = " + objMax);
                progressBar.setMaximum(objMax);
                isFirst = false;
            }

            if (progressInfo.getMaxFKValue() > 0 && isFirstDiffer)
            {
                int fkMax = progressInfo.getMaxFKValue();
                logger.info("fkMax = " + fkMax);
                progressBar.setMaximum(fkMax);
                isFirstDiffer = false;
            }

            progressBar.setValue(progressInfo.getValue());
            Dimension d = progressBar.getSize();
            Rectangle rect = new Rectangle(0, 0, d.width, d.height);
            progressBar.paintImmediately(rect);

            if (progressInfo.getDetail().length() > 0)
            {
                txaDetail.setText(txaDetail.getText() + progressInfo.getDetail());
                progressInfo.setDetail(new StringBuilder());
            }
        }
    }

//    public void startProgress()
//    {
//        isFirst = true;
//        isFirstDiffer = true;
//        progressBar.setValue(0);
//    }

    public void startTimer()
    {
        logger.info("Start Timer");
        timer.start();
    }
}
