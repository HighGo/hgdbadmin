/* ------------------------------------------------
 *
 * File: TypePanel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\TypePanel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.migrationassistant.controller.MigrationController;
import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DataTypeCastDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DataTypeDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import com.highgo.hgdbadmin.migrationassistant.util.TypeCastDialog;
import com.highgo.hgdbadmin.util.MsgDialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class TypePanel extends JPanel
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private ResourceBundle msgBundle = ResourceBundle.getBundle("messages");
    private JTable typeCastTable;
    private JFrame frame;

    public TypePanel(JFrame frame)
    {
        this.frame = frame;
        initCompont();
    }

    private void initCompont()
    {
        JButton btnAdd;
        JButton btnEdit;
        JButton btnDel;
        JButton btnSystem;
        JScrollPane scrollPaneTable;

        scrollPaneTable = new JScrollPane();
        typeCastTable = new JTable();
        btnAdd = new JButton();
        btnEdit = new JButton();
        btnDel = new JButton();
        btnSystem = new JButton();

        typeCastTable.setModel(new DefaultTableModel(
                new Object[][]
        {
        },
                new String[]
        {
            constBundle.getString("sourceDBType"),
            constBundle.getString("sourceDatatype"),
            constBundle.getString("HGDBDatatype"),
            constBundle.getString("isSystem")
        })
        {
            Class[] types = new Class[]
            {
                String.class, String.class, String.class, Boolean.class
            };
            boolean[] canEdit = new boolean[]
            {
                false, false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        });
        scrollPaneTable.setViewportView(typeCastTable);
        //set select mode to single selection one time
        typeCastTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        btnAdd.setText(constBundle.getString("addRule"));
        btnEdit.setText(constBundle.getString("editRule"));
        btnDel.setText(constBundle.getString("delRule"));
        btnSystem.setText(constBundle.getString("systemRule"));
        //because the datatype file couldn't open in linux System
        //so hide this button temporary;
        btnSystem.setVisible(false);

        btnAdd.setToolTipText(constBundle.getString("addTypeRule"));
        btnEdit.setToolTipText(constBundle.getString("editTypeRule"));
        btnDel.setToolTipText(constBundle.getString("delTypeRule"));

        setPreferredSize(new Dimension(550, 390));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(
                GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(
                layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(scrollPaneTable, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(
                layout.createSequentialGroup()
                .addComponent(btnSystem)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAdd)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEdit)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnDel)))
                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(
                layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(scrollPaneTable, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addGroup(
                layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(btnDel)
                .addComponent(btnEdit)
                .addComponent(btnAdd)
                .addComponent(btnSystem))
                .addContainerGap()));

        //this button maybe hiden
        btnSystem.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnSystemActionPerformed(evt);
            }
        });

        btnDel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnDelActionPerformed(evt);
            }
        });

        btnEdit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnEditActionPerformed(evt);
            }
        });
        btnAdd.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnAddActionPerformed(evt);
            }
        });
    }

    private void btnSystemActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        try
        {
            File file = new File("datatype.txt");
            logger.info("path" + file.getAbsolutePath());
            Runtime.getRuntime().exec("cmd  /c  start " + file.getAbsolutePath());
        }
        catch (IOException ex)
        {
            logger.error("Error: " + ex.getMessage());
        }
    }

    private void btnAddActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        String title;
        ObjEnum.DBType sourceDB;
        DataTypeDTO sourceTypeDTO;
        DataTypeDTO hgTypeDTO;
        DataTypeCastDTO dataTypeCastDTO;

        if (typeCastTable.getSelectedRow() != -1)
        {
            int[] selection = typeCastTable.getSelectedRows();
            DefaultTableModel tbModel = (DefaultTableModel) typeCastTable.getModel();
            sourceDB = (ObjEnum.DBType) tbModel.getValueAt(selection[0], 0);
            sourceTypeDTO = (DataTypeDTO) tbModel.getValueAt(selection[0], 1);
            hgTypeDTO = (DataTypeDTO) tbModel.getValueAt(selection[0], 2);

            dataTypeCastDTO = new DataTypeCastDTO();
            dataTypeCastDTO.setSourceDB(sourceDB);
            dataTypeCastDTO.setSourceDataType(sourceTypeDTO);
            dataTypeCastDTO.setHgDataType(hgTypeDTO);
            dataTypeCastDTO.setIsSystem(false);
            title = constBundle.getString("addRule") + sourceDB + "2HGDB";
        }
        else
        {
            sourceDB = (ObjEnum.DBType) typeCastTable.getValueAt(0, 0);

            sourceTypeDTO = new DataTypeDTO();
//            sourceTypeDTO.setDbType(sourceDB);
            hgTypeDTO = new DataTypeDTO();
//            sourceTypeDTO.setDbType( ObjEnum.DBType.HGDB);

            dataTypeCastDTO = new DataTypeCastDTO();
            dataTypeCastDTO.setSourceDB(sourceDB);
            dataTypeCastDTO.setSourceDataType(sourceTypeDTO);
            dataTypeCastDTO.setHgDataType(hgTypeDTO);
            dataTypeCastDTO.setIsSystem(false);

            title = constBundle.getString("addRule") + sourceDB.toString() + "2HGDB";
        }

        TypeCastDialog dialog;
        dialog = new TypeCastDialog(this.frame, null, title, dataTypeCastDTO);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);

        if (dialog.isClickUse())
        {
            DefaultTableModel tbModel = (DefaultTableModel) typeCastTable.getModel();
            DataTypeCastDTO newDataTypeCastDTO = dialog.getDataTypeCastDTO();
            Object[] rowData = new Object[4];
            rowData[0] = newDataTypeCastDTO.getSourceDB();
            rowData[1] = newDataTypeCastDTO.getSourceDataType();
            rowData[2] = newDataTypeCastDTO.getHgDataType();
            rowData[3] = newDataTypeCastDTO.isIsSystem();
            tbModel.addRow(rowData);
        }
    }

    private void btnEditActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        logger.info("Selected Row = " + typeCastTable.getSelectedRow());
        //-1 means no row selected
        if (typeCastTable.getSelectedRow() != -1)
        {
            int[] selection = typeCastTable.getSelectedRows();
            DefaultTableModel tbModel = (DefaultTableModel) typeCastTable.getModel();
            ObjEnum.DBType sourceDB = (ObjEnum.DBType) tbModel.getValueAt(selection[0], 0);
            DataTypeDTO sourceTypeDTO = (DataTypeDTO) tbModel.getValueAt(selection[0], 1);
            DataTypeDTO hgTypeDTO = (DataTypeDTO) tbModel.getValueAt(selection[0], 2);
            boolean isSystem = false;
            if (tbModel.getValueAt(selection[0], 3).equals(true))
            {
                isSystem = true;
            }
            DataTypeCastDTO dataTypeCastDTO = new DataTypeCastDTO();
            dataTypeCastDTO.setSourceDB(sourceDB);
            dataTypeCastDTO.setSourceDataType(sourceTypeDTO);
            dataTypeCastDTO.setHgDataType(hgTypeDTO);
            dataTypeCastDTO.setIsSystem(isSystem);
            String title = constBundle.getString("editRule") + sourceDB.toString() + "2HGDB";
            TypeCastDialog dialog = new TypeCastDialog(this.frame, null, title, dataTypeCastDTO);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if (dialog.isClickUse())
            {
                DataTypeCastDTO newDataTypeCastDTO = dialog.getDataTypeCastDTO();
                tbModel.setValueAt(newDataTypeCastDTO.getHgDataType(), selection[0], 2);
            }
        }
    }

    private void btnDelActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        if (typeCastTable.getSelectedRow() != -1)
        {
            int[] selection = typeCastTable.getSelectedRows();
            DefaultTableModel tbModel = (DefaultTableModel) typeCastTable.getModel();
            if (tbModel.getValueAt(selection[0], 3).equals(false))
            {
                tbModel.removeRow(typeCastTable.getSelectedRow());
            }
            else
            {
                String stitle = constBundle.getString("warning");
                String sMsg = msgBundle.getString("errorDelSystemTypeCast");
                MsgDialog mDialog = new MsgDialog();
                mDialog.showDialog(this, JOptionPane.WARNING_MESSAGE, null, stitle, sMsg);
            }
        }
    }

    public void fillTypeTable(DBConnInfoDTO sourceDBInfoDTO)
    {
        logger.info("Enter");
        DefaultTableModel typeCastModel = (DefaultTableModel) typeCastTable.getModel();
        if (typeCastModel.getRowCount() > 0)
        {
            logger.info("Clean TypeCastTable");
            for (int i = typeCastModel.getRowCount() - 1; i >= 0; i--)
            {
                typeCastModel.removeRow(i);
            }
        }

        MigrationController mc = MigrationController.getInstance();
        List<DataTypeCastDTO> dataTypeCastList = mc.getDefaultDataTypeCastList(sourceDBInfoDTO.getDBType());
        logger.info("dataTypeCastList = " + dataTypeCastList.size());
        for (DataTypeCastDTO dataTypeCastDTO : dataTypeCastList)
        {
            Object[] rowData = new Object[4];
            rowData[0] = dataTypeCastDTO.getSourceDB();
            rowData[1] = dataTypeCastDTO.getSourceDataType();
            rowData[2] = dataTypeCastDTO.getHgDataType();
            rowData[3] = dataTypeCastDTO.isIsSystem();
            typeCastModel.addRow(rowData);
        }
    }

    public List<DataTypeCastDTO> getTypeCastList()
    {
        DefaultTableModel model = (DefaultTableModel) typeCastTable.getModel();
        int rowCount = model.getRowCount();
        List<DataTypeCastDTO> dataTypeCastList = new ArrayList();
        for (int i = 0; i < rowCount; i++)
        {
            DataTypeCastDTO dataTypeCast = new DataTypeCastDTO();
            dataTypeCast.setSourceDB((ObjEnum.DBType) model.getValueAt(i, 0));
            dataTypeCast.setSourceDataType((DataTypeDTO) model.getValueAt(i, 1));
            dataTypeCast.setHgDataType((DataTypeDTO) model.getValueAt(i, 2));
            if (model.getValueAt(i, 3).toString().equals("true"))
            {
                dataTypeCast.setIsSystem(true);
            }
            else
            {
                dataTypeCast.setIsSystem(false);
            }

            dataTypeCastList.add(dataTypeCast);
        }
        logger.info("Return:ObjSize=" + dataTypeCastList.size());
        return dataTypeCastList;
    }
}
