/* ------------------------------------------------
 *
 * File: ObjectsPanel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\ObjectsPanel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.migrationassistant.controller.MigrationController;
import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ObjInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ObjListDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 *
 * @author Liu Yuanyuan
 */
public class ObjectsPanel extends JPanel
{

    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private JComboBox cbTabMigrateModel;
    private JTextField tfdCopyBatch;
    private JTextField tfdInsertBatch;
    private int choosedTab;//like 0,1,2,3
    private JTable tabSchame;
    private JTable tabTable;
    private JTable tabSequence;
    private JTable tabView;
    private JTable tabIndex;
    private JTable tabProcedure;
    private JTable tabFunction;
    private JTable tabTrigger;
    private DefaultTableModel modelSchema;
    private DefaultTableModel modelTable;
    private DefaultTableModel modelSequence;
    private DefaultTableModel modelView;
    private DefaultTableModel modelIndex;
    private DefaultTableModel modelProcedure;
    private DefaultTableModel modelFunction;
    private DefaultTableModel modelTrigger;

    private JLabel lblDelimiter;// = new JLabel(constBundle.getString("columnSeparator"));
    private JTextField txfdDelimiter;// = new JTextField(",");
    private JLabel lblQuoteChar;// = new JLabel(constBundle.getString("quoteChar"));
    private JTextField txfdQuoteChar;// = new JTextField("\"");

    public ObjectsPanel()
    {
        initCompont();
    }

    public String getDelimiter()
    {
        return txfdDelimiter.getText();
    }
    
    public String getQuoteChar()
    {
        return txfdQuoteChar.getText();
    }
    
    public String getMigrateModel()
    {
        return (String) cbTabMigrateModel.getSelectedItem();
    }

    public int getCopyBatch()
    {
        return Integer.valueOf(tfdCopyBatch.getText());
    }

    public int getInsertBatch()
    {
        return Integer.valueOf(tfdInsertBatch.getText());
    }

    public List<ObjInfoDTO> getChoosedObjInfoList()
    {
        List<ObjInfoDTO> objInfoList = new ArrayList();
        getChoosedObj(objInfoList, modelSchema, ObjEnum.DBObj.SCHEMA);
        getChoosedObj(objInfoList, modelTable, ObjEnum.DBObj.TABLE);
        getChoosedObj(objInfoList, modelSequence, ObjEnum.DBObj.SEQUENCE);
        getChoosedObj(objInfoList, modelView, ObjEnum.DBObj.VIEW);
        getChoosedObj(objInfoList, modelIndex, ObjEnum.DBObj.INDEX);
        getChoosedObj(objInfoList, modelProcedure, ObjEnum.DBObj.PROCEDURE);
        getChoosedObj(objInfoList, modelFunction, ObjEnum.DBObj.FUNCTION);
        getChoosedObj(objInfoList, modelTrigger, ObjEnum.DBObj.TRIGGER);
        //needn't sort type name as ObjEnum's order
        //Collections.sort(objInfoList);
        logger.info("Return:ObjList = " + objInfoList.size());
        return objInfoList;
    }

    public void fillSourceTable(DBConnInfoDTO sourceDBInfoDTO) throws Exception
    {
        logger.info("Enter");

        removeFormerData(modelSchema);
        removeFormerData(modelTable);
        removeFormerData(modelSequence);
        removeFormerData(modelView);
        removeFormerData(modelIndex);
        removeFormerData(modelProcedure);
        removeFormerData(modelFunction);
        removeFormerData(modelTrigger);

        MigrationController mc = MigrationController.getInstance();
        ObjListDTO objListDTO = mc.getObjListDTO(sourceDBInfoDTO);
        fillTab(objListDTO.getSchemaList(), modelSchema);
        fillTab(objListDTO.getTableList(), modelTable);
        fillTab(objListDTO.getSequenceList(), modelSequence);
        fillTab(objListDTO.getViewList(), modelView);
        fillTab(objListDTO.getIndexList(), modelIndex);
        fillTab(objListDTO.getProcedureList(), modelProcedure);
        fillTab(objListDTO.getFunctionList(), modelFunction);
        fillTab(objListDTO.getTriggerList(), modelTrigger);

        logger.info("Return:");
    }

    private void getChoosedObj(List<ObjInfoDTO> objInfoList, DefaultTableModel model, ObjEnum.DBObj type)
    {
        for (int i = 0; i < model.getRowCount(); i++)
        {
            if (model.getValueAt(i, 2).equals(true))
            {
                ObjInfoDTO objInfo = new ObjInfoDTO();
                objInfo.setSchema(model.getValueAt(i, 0).toString());
                objInfo.setName(model.getValueAt(i, 1).toString());
                objInfo.setType(type);
                objInfoList.add(objInfo);
            }
        }
    }

    private void fillTab(List<ObjInfoDTO> objInfoList, DefaultTableModel model)
    {
        logger.info("ObjectInfoList = " + objInfoList.size());
        for (int i = 0; i < objInfoList.size(); i++)
        {
            Object[] rowData = new Object[3];
            rowData[0] = objInfoList.get(i).getSchema();
            rowData[1] = objInfoList.get(i).getName();
            rowData[2] = objInfoList.get(i).isSelected();
            model.addRow(rowData);
        }
    }

    private void removeFormerData(DefaultTableModel model)
    {
        if (model.getRowCount() > 0)
        {
            logger.info("Clean SourceTable");
            for (int i = model.getRowCount() - 1; i >= 0; i--)
            {
                model.removeRow(i);
            }
        }
    }

    private void tpaneStateChanged(ChangeEvent evt)
    {
        JTabbedPane sourceTabbedPane = (JTabbedPane) evt.getSource();
        logger.info("choosedTab = " + choosedTab);
        choosedTab = sourceTabbedPane.getSelectedIndex();
    }

    private void rbtnTabActionPerformed(ActionEvent evt)
    {
        switch (choosedTab)
        {
            case 0:
                select(true, modelSchema);
                break;
            case 1:
                select(true, modelTable);
                break;
            case 2:
                select(true, modelSequence);
                break;
            case 3:
                select(true, modelView);
                break;
            case 4:
                select(true, modelIndex);
                break;
            case 5:
                select(true, modelProcedure);
                break;
            case 6:
                select(true, modelFunction);
                break;
            case 7:
                select(true, modelTrigger);
                break;
        }
    }

    private void rbtnAllActionPerformed(ActionEvent evt)
    {
        select(true, modelSchema);
        select(true, modelTable);
        select(true, modelSequence);
        select(true, modelView);
        select(true, modelIndex);
        select(true, modelProcedure);
        select(true, modelFunction);
        select(true, modelTrigger);
    }

    private void rbtnNoneActionPerformed(ActionEvent evt)
    {
        select(false, modelSchema);
        select(false, modelTable);
        select(false, modelSequence);
        select(false, modelView);
        select(false, modelIndex);
        select(false, modelProcedure);
        select(false, modelFunction);
        select(false, modelTrigger);
    }

    private void rbtnInverseActionPerformed(ActionEvent evt)
    {
        inverse(modelSchema);
        inverse(modelTable);
        inverse(modelSequence);
        inverse(modelView);
        inverse(modelIndex);
        inverse(modelProcedure);
        inverse(modelFunction);
        inverse(modelTrigger);
    }

    private void inverse(DefaultTableModel model)
    {
        for (int i = 0; i < model.getRowCount(); i++)
        {

            if (model.getValueAt(i, 2).equals(false))
            {
                model.setValueAt(true, i, 2);
            }
            else
            {
                model.setValueAt(false, i, 2);
            }
        }
    }

    private void select(boolean isSelect, DefaultTableModel model)
    {
        for (int i = 0; i < model.getRowCount(); i++)
        {
            model.setValueAt(isSelect, i, 2);
        }
    }

    private void initCompont()
    {
        ButtonGroup buttonGroup;
        JRadioButton rbtnTab;
        JRadioButton rbtnAll;
        JRadioButton rbtnInverse;
        JRadioButton rbtnNone;
        JTabbedPane tpane;
        JLabel lblTabMigrateModel;
        JLabel lblCopy;
        JLabel lblInsert;
        JScrollPane spSchema;
        JScrollPane spTable;
        JScrollPane spSequence;
        JScrollPane spView;
        JScrollPane spIndex;
        JScrollPane spProcedure;
        JScrollPane spFunction;
        JScrollPane spTrigger;

        buttonGroup = new ButtonGroup();
        tpane = new JTabbedPane();
        spSchema = new JScrollPane();
        spTable = new JScrollPane();
        spSequence = new JScrollPane();
        spView = new JScrollPane();
        spIndex = new JScrollPane();
        spProcedure = new JScrollPane();
        spFunction = new JScrollPane();
        spTrigger = new JScrollPane();

        tabSchame = new JTable();
        tabTable = new JTable();
        tabSequence = new JTable();
        tabView = new JTable();
        tabIndex = new JTable();
        tabProcedure = new JTable();
        tabFunction = new JTable();
        tabTrigger = new JTable();

        rbtnInverse = new JRadioButton();
        rbtnNone = new JRadioButton();
        rbtnAll = new JRadioButton();
        rbtnTab = new JRadioButton();
        cbTabMigrateModel = new JComboBox();
        lblTabMigrateModel = new JLabel();
        lblCopy = new JLabel();
        lblInsert = new JLabel();
        tfdCopyBatch = new JTextField();
        tfdInsertBatch = new JTextField();

        cbTabMigrateModel.setEditable(false);
        cbTabMigrateModel.addItem(constBundle.getString("normalMigrate"));
        cbTabMigrateModel.addItem(constBundle.getString("onlyInsert"));
        cbTabMigrateModel.addItem(constBundle.getString("onlyStru"));
        cbTabMigrateModel.addItem(constBundle.getString("onlyData"));
        lblTabMigrateModel.setText(constBundle.getString("tabMigrateModel"));
        lblCopy.setText(constBundle.getString("copyBatch"));
        lblInsert.setText(constBundle.getString("insertBatch"));
//        lblTabMigrateModel.setToolTipText(constBundle.getString("chooseTableMigrateModel"));
//        lblCopy.setToolTipText(constBundle.getString("inputCopyBatch"));
//        lblInsert.setToolTipText(constBundle.getString("inputInsertBatch"));
        tfdCopyBatch.setText("500");
        tfdInsertBatch.setText("50");
        cbTabMigrateModel.setToolTipText(constBundle.getString("chooseTableMigrateModel"));
        tfdCopyBatch.setToolTipText(constBundle.getString("inputCopyBatch"));
        tfdInsertBatch.setToolTipText(constBundle.getString("inputInsertBatch"));

        buttonGroup.add(rbtnTab);
        rbtnTab.setText(constBundle.getString("chooseTab"));
        buttonGroup.add(rbtnAll);
        rbtnAll.setText(constBundle.getString("chooseAll"));
        buttonGroup.add(rbtnNone);
        rbtnNone.setText(constBundle.getString("chooseNone"));
        buttonGroup.add(rbtnInverse);
        rbtnInverse.setText(constBundle.getString("chooseInverse"));
        rbtnTab.setToolTipText(constBundle.getString("chooseTabObj"));
        rbtnAll.setToolTipText(constBundle.getString("chooseAllObj"));
        rbtnNone.setToolTipText(constBundle.getString("chooseNoneObj"));
        rbtnInverse.setToolTipText(constBundle.getString("chooseInverseObj"));

        
        lblDelimiter = new JLabel(constBundle.getString("columnSeparator"));
        txfdDelimiter = new JTextField(",");
        lblQuoteChar = new JLabel(constBundle.getString("quoteChar"));
        txfdQuoteChar = new JTextField("\"");
        
        tpane.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                tpaneStateChanged(evt);
            }
        });

        rbtnTab.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                rbtnTabActionPerformed(evt);
            }
        });

        rbtnAll.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                rbtnAllActionPerformed(evt);
            }
        });

        rbtnNone.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                rbtnNoneActionPerformed(evt);
            }
        });

        rbtnInverse.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                rbtnInverseActionPerformed(evt);
            }
        });


        Object[][] dm = new Object[][]
        {
        };

        String[] cm = new String[]
        {
            constBundle.getString("schemaName"),
            constBundle.getString("objName"),
            constBundle.getString("choose")
        };

        final Class[] type = new Class[]
        {
            String.class,
            String.class,
            Boolean.class
        };

        final boolean[] edit = new boolean[]
        {
            false,
            false,
            true
        };

        modelSchema = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };
        modelTable = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };
        modelSequence = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };

        modelView = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };

        modelIndex = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };

        modelProcedure = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };

        modelFunction = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };

        modelTrigger = new DefaultTableModel(dm, cm)
        {
            Class[] types = type;
            boolean[] canEdit = edit;
            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };

        tabSchame.setModel(modelSchema);
        tabTable.setModel(modelTable);
        tabSequence.setModel(modelSequence);
        tabView.setModel(modelView);
        tabIndex.setModel(modelIndex);
        tabProcedure.setModel(modelProcedure);
        tabFunction.setModel(modelFunction);
        tabTrigger.setModel(modelTrigger);

        spSchema.setViewportView(tabSchame);
        spTable.setViewportView(tabTable);
        spSequence.setViewportView(tabSequence);
        spView.setViewportView(tabView);
        spIndex.setViewportView(tabIndex);
        spProcedure.setViewportView(tabProcedure);
        spFunction.setViewportView(tabFunction);
        spTrigger.setViewportView(tabTrigger);

        tpane.addTab("schema", spSchema);
        tpane.addTab("table", spTable);
        tpane.addTab("sequence", spSequence);
        tpane.addTab("view", spView);
        tpane.addTab("index", spIndex);
        tpane.addTab("procedure", spProcedure);
        tpane.addTab("function", spFunction);
        tpane.addTab("trigger", spTrigger);

        setPreferredSize(new Dimension(550, 380));

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(rbtnTab, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(rbtnAll, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(rbtnNone, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(rbtnInverse, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTabMigrateModel,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(5,5,5)
                                .addComponent(cbTabMigrateModel,GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 282, Short.MAX_VALUE)
                                  
                                .addComponent(lblDelimiter,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(5,5,5)
                                .addComponent(txfdDelimiter,GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                .addGap(5,5,5)
                                .addComponent(lblQuoteChar,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(5,5,5)
                                .addComponent(txfdQuoteChar,GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                .addGap(10,10,10)
                                    
                                .addComponent(lblCopy,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(5,5,5)
                                .addComponent(tfdCopyBatch,GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                .addGap(10,10,10)
                                .addComponent(lblInsert,GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addGap(5,5,5)
                                .addComponent(tfdInsertBatch,GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                            .addGap(5,5,5)
                            .addComponent(tpane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(10, 10, 10))
        );

        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(2, 2, 2)//add(10, 10, 10)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTabMigrateModel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbTabMigrateModel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblDelimiter, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txfdDelimiter, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblQuoteChar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txfdQuoteChar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblCopy, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfdCopyBatch, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblInsert, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tfdInsertBatch, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(3, 3, 3)
                .addComponent(tpane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(rbtnTab)
                    .addComponent(rbtnAll)
                    .addComponent(rbtnNone)
                    .addComponent(rbtnInverse))
                .addGap(2, 2, 2))
);

    }

}
