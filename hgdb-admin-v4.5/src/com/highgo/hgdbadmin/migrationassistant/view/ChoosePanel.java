/* ------------------------------------------------
 *
 * File: ChoosePanel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\ChoosePanel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.migrationassistant.controller.MigrationController;
import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ObjInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 2013.9.25 deprecated this class
 * @author Liu Yuanyuan
 */
public class ChoosePanel extends JPanel
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private JTable tbSource;
    private JTable tbResult;
    private DefaultTableModel modelSource;

    public ChoosePanel()
    {
        initCompont();
    }

    private void initCompont()
    {
        JButton btnMove;
        JButton btnMoveAll;
        JButton btnDel;
        JButton btnDelAll;
        JScrollPane spLeft;
        JScrollPane spRight;

        btnMove = new JButton();
        btnMoveAll = new JButton();
        btnDel = new JButton();
        btnDelAll = new JButton();
        spLeft = new JScrollPane();
        spRight = new JScrollPane();
        tbSource = new JTable();
        tbResult = new JTable();

        btnMove.setText(">");
        btnMoveAll.setText(">>");
        btnDel.setText("<");
        btnDelAll.setText("<<");

        Object[][] dm = new Object[][]
        {
        };

        String[] cm = new String[]
        {
            constBundle.getString("schemaName"),
            constBundle.getString("objName"),
            constBundle.getString("objType"),
            constBundle.getString("choose")
        };

        modelSource = new DefaultTableModel(dm, cm)
        {
            Class[] types = new Class[]
            {
                String.class, String.class, ObjEnum.ObjType.class, Boolean.class
            };
            boolean[] canEdit = new boolean[]
            {
                false, false, false, true
            };

            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };
        DefaultTableModel modelResult = new DefaultTableModel(dm, cm)
        {
            Class[] types = new Class[]
            {
                String.class, String.class, ObjEnum.ObjType.class, Boolean.class
            };
            boolean[] canEdit = new boolean[]
            {
                false, false, false, true
            };

            @Override
            public Class getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit[columnIndex];
            }
        };
        tbSource.setModel(modelSource);
        spLeft.setViewportView(tbSource);
        tbResult.setModel(modelResult);
        spRight.setViewportView(tbResult);

        setPreferredSize(new Dimension(550, 380));
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                //.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                //.addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                .addComponent(spLeft, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(btnDelAll, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnDel, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnMove, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnMoveAll, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(spRight, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))//)
                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(
                layout.createSequentialGroup()
                .addContainerGap()
                //.addComponent(lblTitle)
                //.addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addComponent(btnMove)
                .addGap(10, 10, 10)
                .addComponent(btnMoveAll)
                .addGap(30, 30, 30)
                .addComponent(btnDel)
                .addGap(10, 10, 10)
                .addComponent(btnDelAll))
                .addGroup(
                layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(spLeft, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(spRight, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap()));

        btnMove.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnMoveActionPerformed(evt);
            }
        });
        btnMoveAll.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnMoveAllActionPerformed(evt);
            }
        });
        btnDel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnDelActionPerformed(evt);
            }
        });
        btnDelAll.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnDelAllActionPerformed(evt);
            }
        });
    }

    private void btnMoveAllActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        tbResult.updateUI();
        DefaultTableModel modelResult = (DefaultTableModel) tbResult.getModel();
        //remove the previous choosed objects from the result table
        for (int i = tbResult.getRowCount() - 1; i >= 0; i--)
        {
            modelResult.removeRow(i);
        }
        //add all source objects in result table
        for (int i = 0; i < tbSource.getRowCount(); i++)
        {
            Object[] obj = new Object[4];
            obj[0] = tbSource.getValueAt(i, 0);
            obj[1] = tbSource.getValueAt(i, 1);
            obj[2] = tbSource.getValueAt(i, 2);
            obj[3] = false;
            modelResult.addRow(obj);
        }
        tbResult.updateUI();
    }

    private void btnMoveActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        DefaultTableModel modelResult = (DefaultTableModel) tbResult.getModel();
        for (int i = 0; i < tbSource.getRowCount(); i++)
        {
            //logger.info(tbSource.getValueAt(i, 2).toString() +":isSelect="+ tbSource.getValueAt(i, 3).toString());
            if (tbSource.getValueAt(i, 3) != null)
            {
                if (tbSource.getValueAt(i, 3).equals(true))
                {
                    Object[] obj = new Object[4];
                    obj[0] = tbSource.getValueAt(i, 0);
                    obj[1] = tbSource.getValueAt(i, 1);
                    obj[2] = tbSource.getValueAt(i, 2);
                    obj[3] = false;
                    modelResult.addRow(obj);
                }
            }
        }
    }

    private void btnDelAllActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        tbResult.updateUI();
        DefaultTableModel modelResult = (DefaultTableModel) tbResult.getModel();
        logger.info("Result Table Row Count = " + tbResult.getRowCount());
        for (int i = tbResult.getRowCount() - 1; i >= 0; i--)
        {
            modelResult.removeRow(i);
        }
        tbResult.updateUI();
    }

    private void btnDelActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        tbResult.updateUI();
        DefaultTableModel modelResult = (DefaultTableModel) tbResult.getModel();
        logger.info("Result Table Row Count = " + tbResult.getRowCount());
        for (int i = tbResult.getRowCount() - 1; i >= 0; i--)
        {
            if (tbResult.getValueAt(i, 3).equals(true))
            {
                logger.info("Remove Object = " + modelResult.getValueAt(i, 0) + "." + modelResult.getValueAt(i, 1));
                modelResult.removeRow(i);
            }
        }
        tbResult.updateUI();
    }

    public void fillSourceTable(DBConnInfoDTO sourceDBInfoDTO) throws Exception
    {
        logger.info("Enter");
        DefaultTableModel modelResult = (DefaultTableModel) tbResult.getModel();
        if (modelResult.getRowCount() > 0)
        {
            logger.info("Clean ResultTable");
            for (int i = modelResult.getRowCount() - 1; i >= 0; i--)
            {
                modelResult.removeRow(i);
            }
        }

        DefaultTableModel modelSource = (DefaultTableModel) tbSource.getModel();
        if (modelSource.getRowCount() > 0)
        {
            logger.info("Clean SourceTable");
            for (int i = modelSource.getRowCount() - 1; i >= 0; i--)
            {
                modelSource.removeRow(i);
            }
        }
        MigrationController mc = MigrationController.getInstance();
        /*
        List<ObjInfoDTO> objInfoList = mc.getObjListDTO(sourceDBInfoDTO);
        logger.info("ObjectInfoList = " + objInfoList.size());
        for (int i = 0; i < objInfoList.size(); i++)
        {
            Object[] rowData = new Object[4];
            rowData[0] = objInfoList.get(i).getSchema();
            rowData[1] = objInfoList.get(i).getName();
            rowData[2] = objInfoList.get(i).getType();
            rowData[3] = objInfoList.get(i).isSelected();
            modelSource.addRow(rowData);
        }
        */
        logger.info("Return:");
    }

    public List<ObjInfoDTO> getChoosedObjInfoList()
    {
        DefaultTableModel model = (DefaultTableModel) tbResult.getModel();
        int rowCount = model.getRowCount();
        logger.info("ChoosedObjCount = " + rowCount);
        List<ObjInfoDTO> objInfoList = new ArrayList();
        for (int i = 0; i < rowCount; i++)
        {
            ObjInfoDTO objInfo = new ObjInfoDTO();
            objInfo.setSchema(model.getValueAt(i, 0).toString());
            objInfo.setName(model.getValueAt(i, 1).toString());
            objInfo.setType((ObjEnum.DBObj) model.getValueAt(i, 2));
            objInfoList.add(objInfo);
        }
        //sort type name as ObjEnum's order
        //Collections.sort(objInfoList);
        logger.info("Return:ObjListSize=" + objInfoList.size());
        return objInfoList;
    }
}
