/* ------------------------------------------------
 *
 * File: IntroducePanel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\IntroducePanel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.util.PathChooserDialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class IntroducePanel extends JPanel
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private ResourceBundle mesgBundle = ResourceBundle.getBundle("messages");
    private JTextField txtfldPath;

    public IntroducePanel()
    {
        initComponet();
    }

    private void initComponet()
    {
        JLabel lblIntroduce;
        JLabel lblChoosePath;
        JButton btnChooser;
        lblIntroduce = new JLabel();
        lblChoosePath = new JLabel();
        txtfldPath = new JTextField();
        btnChooser = new JButton();

        Font font = new Font(constBundle.getString("fontSongTi"), 0, 14);
        String sIntroduce = "<html><span style=font-family:" + constBundle.getString("fontSongTi") + "; font-size:12px; >"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph1") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph2") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph3") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph4") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph5") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph6") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph7") + "</p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph8") + "</p>"
                + "<p style = margin:2px ></p>"
                + "<p style = margin:4px >" + mesgBundle.getString("migrationIntroduceParagraph9") + "</p>"
                + "</span></html>";
        lblIntroduce.setFont(font);
        lblIntroduce.setText(sIntroduce);

        lblChoosePath.setFont(font);
        lblChoosePath.setText(constBundle.getString("logPath"));
        lblChoosePath.setHorizontalAlignment(SwingConstants.LEFT);
        lblChoosePath.setHorizontalTextPosition(SwingConstants.CENTER);
        lblChoosePath.setVerticalAlignment(SwingConstants.BOTTOM);

        txtfldPath.setHorizontalAlignment(JTextField.LEFT);
        File absolutedirectory = new File("");
        txtfldPath.setText(absolutedirectory.getAbsolutePath()
                + File.separator + "scripts" + File.separator);

        btnChooser.setText("...");
        btnChooser.setToolTipText(constBundle.getString("choosePath"));
        btnChooser.setHorizontalAlignment(SwingConstants.LEFT);
        btnChooser.setHorizontalTextPosition(SwingConstants.CENTER);
        btnChooser.setVerticalTextPosition(SwingConstants.BOTTOM);

        setPreferredSize(new Dimension(550, 380));
        GroupLayout layout = new GroupLayout(this);
        setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(
                layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                //.addComponent(lblTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,Short.MAX_VALUE)
                .addComponent(lblIntroduce)
                .addGroup(
                layout.createSequentialGroup()
                .addComponent(lblChoosePath)
                .addGap(10, 10, 10)
                .addComponent(txtfldPath)
                .addGap(10, 10, 10)
                .addComponent(btnChooser)))
                .addContainerGap()));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(
                layout.createSequentialGroup()
                .addContainerGap()
                //.addComponent(lblTitle)
                //.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblIntroduce, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lblChoosePath, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtfldPath, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addComponent(btnChooser))
                .addContainerGap()));

        btnChooser.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnChooserActionPerformed(evt);
            }
        });
    }

    private void btnChooserActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        PathChooserDialog fcDialog = new PathChooserDialog();
        int response = fcDialog.showPathChooser(this, txtfldPath.getText(),
                MessageFormat.format(constBundle.getString("chooseWhatPath"), constBundle.getString("logFiles")));
        if (response == JFileChooser.APPROVE_OPTION)
        {
            txtfldPath.setText(fcDialog.getChoosedPath() + File.separator);
            logger.info("Choose Path:" + fcDialog.getChoosedPath());
        }
    }

    public String getLogPath()
    {
        return txtfldPath.getText();
    }
}
