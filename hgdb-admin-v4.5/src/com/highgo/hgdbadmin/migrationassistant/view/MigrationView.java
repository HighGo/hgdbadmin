/* ------------------------------------------------
 *
 * File: MigrationView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\view\MigrationView.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.view;

import com.highgo.hgdbadmin.migrationassistant.controller.MigrationController;
import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ConnectHelper;
import com.highgo.hgdbadmin.util.MsgDialog;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ResourceBundle;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class MigrationView extends JFrame
{

    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private ResourceBundle mesgBundle = ResourceBundle.getBundle("messages");
    private int plSequen = 1;
    private JButton btnBack;
    private JButton btnNext;
    private JButton btnMigrate;
    private JButton btnCancle;
    private JButton btnHelp;
    private CardLayout card;
    private JPanel panelCard;
    private JLabel lblTitle;
    private JLabel lblGuidIntroduce;
    private JLabel lblGuidHGDB;
    private JLabel lblGuidSourceDB;
    private JLabel lblGuidChoose;
    private JLabel lblGuidType;
    private JLabel lblGuidMigrate;
    private Font fontBold;
    private Font fontNormal;
    private IntroducePanel plIntroduce;
    private HGDBPanel plHGDB;
    private SourceDBPanel plSourceDB;
//    private ChoosePanel plChoose;
    private ObjectsPanel plObject;
    private TypePanel plType;
    private MigratePanel plMigrate;

    public MigrationView()
    {
        initComponents();
    }

    private void initComponents()
    {
        //setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/migrateAssistant.png")));
        setTitle("Oracle2HGDB "+constBundle.getString("hgdbMigrationAssistant"));
        setSize(800, 520);

        //cardlayout main panel
        card = new CardLayout();
        panelCard = new JPanel(card);
        plIntroduce = new IntroducePanel();
        panelCard.add("", plIntroduce);
        plHGDB = new HGDBPanel();
        panelCard.add("", plHGDB);
        plSourceDB = new SourceDBPanel();
        panelCard.add("", plSourceDB);
//        plChoose = new ChoosePanel();
//        panelCard.add("", plChoose);
        plObject = new ObjectsPanel();
        panelCard.add("", plObject);
        plType = new TypePanel(this);
        panelCard.add("", plType);
        plMigrate = new MigratePanel();
        panelCard.add("", plMigrate);

        //overall layout
        mainLayOut();

        //action
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent evt)
            {
                windowClosingAction(evt);
            }
        });
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnCancleActionPerformed(evt);
            }
        });

        btnMigrate.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnMigrateActionPerformed(evt);
            }
        });

        btnNext.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnNextActionPerformed(evt);
            }
        });

        btnBack.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnBackActionPerformed(evt);
            }
        });

        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnHelpActionPerformed(evt);
            }
        });
    }
    
    private void windowClosingAction(WindowEvent evt)
    {
        logger.info("window closing");
        this.dispose();
        //System.exit(0);
    }
    
    private void btnHelpActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        ImageIcon iconHelp = new ImageIcon(getClass().getResource(
                "/com/highgo/hgdbadmin/image/help.png"));
        String titleHelp = constBundle.getString("help");
        String msg = mesgBundle.getString("migrationHelp");
        MsgDialog helpDailog = new MsgDialog();
        helpDailog.showDialog(this, JOptionPane.PLAIN_MESSAGE, iconHelp, titleHelp, msg);
    }

    private void btnNextActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand() + plSequen);
        if (plSequen == 1)
        {
            card.next(panelCard);
            lblTitle.setText(constBundle.getString("hgDB"));
            btnBack.setEnabled(true);
            lblGuidHGDB.setEnabled(true);
            lblGuidHGDB.setFont(fontBold);
            lblGuidIntroduce.setEnabled(false);
            lblGuidIntroduce.setFont(fontNormal);
            plSequen++;
        }
        else if (plSequen == 2)
        {
            try
            {
                DBConnInfoDTO hgdbInfo = plHGDB.getHGDBConnInfoDTO();
                logger.info("Connect HGDB:url="+"jdbc:highgo://" + hgdbInfo.getHost() + ":" + hgdbInfo.getPort() + "/" + hgdbInfo.getDb());
                ConnectHelper.getConnection(hgdbInfo);

                card.next(panelCard);
                lblTitle.setText(constBundle.getString("sourceDB"));
                lblGuidSourceDB.setEnabled(true);
                lblGuidSourceDB.setFont(fontBold);
                lblGuidHGDB.setEnabled(false);
                lblGuidHGDB.setFont(fontNormal);
                plSequen++;
            }
            catch (Exception ex)
            {
                logger.error("Error:" + ex.getMessage());
                MsgDialog errorDialog = new MsgDialog();
                String title = constBundle.getString("connFailed");
                String msgError = constBundle.getString("checkConnInfo");
                errorDialog.showDialog(this, JOptionPane.ERROR_MESSAGE, null, title,
                        msgError + "\n" + ex.getMessage());
            }
        }
        else if (plSequen == 3)
        {
//            NoteDialog ndialog = new NoteDialog(this);
//            ndialog.setLocationRelativeTo(plSourceDB);
//            ndialog.setVisible(true);
            try
            {
//                plChoose.fillSourceTable(plSourceDB.getSourceDBConnInfoDTO());
                plObject.fillSourceTable(plSourceDB.getSourceDBConnInfoDTO());
//                ndialog.setVisible(false);
                card.next(panelCard);
                lblTitle.setText(constBundle.getString("chooseObject"));
                lblGuidChoose.setEnabled(true);
                lblGuidChoose.setFont(fontBold);
                lblGuidSourceDB.setEnabled(false);
                lblGuidSourceDB.setFont(fontNormal);
                plSequen++;
            }
            catch (Exception ex)
            {
                MsgDialog errorDialog = new MsgDialog();
                String title = constBundle.getString("connFailed");
                String msgError = constBundle.getString("checkConnInfo");
                errorDialog.showDialog(this, JOptionPane.ERROR_MESSAGE, null, title,
                        msgError + "\n" + ex.getMessage());
            }
        }
        else if (plSequen == 4)
        {
            plType.fillTypeTable(plSourceDB.getSourceDBConnInfoDTO());

            card.next(panelCard);
            lblTitle.setText(constBundle.getString("dataType"));
            lblGuidType.setEnabled(true);
            lblGuidType.setFont(fontBold);
            lblGuidChoose.setEnabled(false);
            lblGuidChoose.setFont(fontNormal);
            plSequen++;
        }
        else if (plSequen == 5)
        {
            card.next(panelCard);
            lblTitle.setText(constBundle.getString("migrateData"));
            btnBack.setEnabled(false);//problem
            btnNext.setEnabled(false);
            btnMigrate.setEnabled(true);
            lblGuidMigrate.setEnabled(true);
            lblGuidMigrate.setFont(fontBold);
            lblGuidType.setEnabled(false);
            lblGuidType.setFont(fontNormal);

            plSequen++;
        }
    }

    private void btnCancleActionPerformed(ActionEvent evt)
    {
        String command = evt.getActionCommand();
        logger.info("Command = " + command);
        if (command.equals(constBundle.getString("stop")))
        {
            MigrationController mc = MigrationController.getInstance();
            mc.stopMigrateThread();
        }
        if (command.equals(constBundle.getString("quit"))
                || command.equals(constBundle.getString("cancle")))
        {
            this.dispose();
            //System.exit(0);
        }
    }

    private void btnMigrateActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        if (btnMigrate.getText().equals(constBundle.getString("start")))
//                && mc.getProgressInfo().getMaxValue()!= 0)
        {
            String logPath = plIntroduce.getLogPath();
            logger.info("logPath = " + logPath);
            logger.info("CopyBatch=" + plObject.getCopyBatch() + ", InsertBatch=" + plObject.getInsertBatch());
            MigrationController mc = MigrationController.getInstance();
            mc.startMigrateThread(logPath, plSourceDB.getSourceDBConnInfoDTO(),
                    plHGDB.getHGDBConnInfoDTO(), plObject.getChoosedObjInfoList(),//plChoose.getChoosedObjInfoList()
                    plType.getTypeCastList(),
                    plObject.getMigrateModel(), plObject.getCopyBatch(), plObject.getInsertBatch(),
                    plObject.getDelimiter(), plObject.getQuoteChar());
            logger.info("Start Migration Thread");
            plMigrate.startTimer();
            logger.info("Start Timer");
            btnMigrate.setText(constBundle.getString("stop"));
        }
        else if (btnMigrate.getText().equals(constBundle.getString("stop")))
        {
            MigrationController mc = MigrationController.getInstance();
            mc.stopMigrateThread();
            btnMigrate.setText(constBundle.getString("quit"));
        }
        else if (btnMigrate.getText().equals(constBundle.getString("quit")))
        {
            this.dispose();
        }
    }

    private void btnBackActionPerformed(ActionEvent evt)
    {
        logger.info("Command = " + evt.getActionCommand());
        card.previous(panelCard);
        if (plSequen == 2)
        {
            lblTitle.setText(constBundle.getString("introduce"));
            btnBack.setEnabled(false);
            lblGuidHGDB.setEnabled(false);
            lblGuidHGDB.setFont(fontNormal);
            lblGuidIntroduce.setEnabled(true);
            lblGuidIntroduce.setFont(fontBold);
        }
        else if (plSequen == 3)
        {
            lblTitle.setText(constBundle.getString("hgDB"));
            btnBack.setEnabled(true);
            lblGuidSourceDB.setEnabled(false);
            lblGuidSourceDB.setFont(fontNormal);
            lblGuidHGDB.setEnabled(true);
            lblGuidHGDB.setFont(fontBold);
        }
        else if (plSequen == 4)
        {
            lblTitle.setText(constBundle.getString("sourceDB"));
            lblGuidChoose.setEnabled(false);
            lblGuidChoose.setFont(fontNormal);
            lblGuidSourceDB.setEnabled(true);
            lblGuidSourceDB.setFont(fontBold);
        }
        else if (plSequen == 5)
        {
            lblTitle.setText(constBundle.getString("chooseObject"));
            lblGuidType.setEnabled(false);
            lblGuidType.setFont(fontNormal);
            lblGuidChoose.setEnabled(true);
            lblGuidChoose.setFont(fontBold);
        }
        else if (plSequen == 6)
        {
            lblTitle.setText(constBundle.getString("dataType"));
            btnNext.setEnabled(true);
            btnMigrate.setText(constBundle.getString("start"));
            btnMigrate.setEnabled(false);
            lblGuidMigrate.setEnabled(false);
            lblGuidMigrate.setFont(fontNormal);
            lblGuidType.setEnabled(true);
            lblGuidType.setFont(fontBold);
        }
        plSequen--;
    }

    private void mainLayOut()
    {
        //button group panel
        JPanel panelButtonGroup = getPanelButtonGroup();
        //navigation panel
        JPanel panelGuid = getPanelGuid();
        //title label
        JPanel panelTitle = getPanelTitle();

//        panelGuid.setBorder(BorderFactory.createTitledBorder(null, "HighGo Tech", TitledBorder.LEFT,
//                TitledBorder.BOTTOM, new Font(constBundle.getString("fontSongTi"), 2, 12), new Color(204, 204, 204)));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addComponent(panelButtonGroup, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                .addComponent(panelGuid, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(panelCard, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelTitle, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(0, 0, 0)));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                .addComponent(panelTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelCard, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addComponent(panelGuid, GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelButtonGroup, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap()));
    }

    private JPanel getPanelTitle()
    {
        JPanel panelTitle = new JPanel();
        lblTitle = new JLabel();
        Font fontTitle = new Font("宋体", 0, 14);
        Color colorBackground = new Color(254, 190, 0);
        lblTitle.setFont(fontTitle);
        lblTitle.setHorizontalAlignment(SwingConstants.RIGHT);
        lblTitle.setText(constBundle.getString("introduce"));
        panelTitle.setBackground(colorBackground);
        panelTitle.setBorder(BorderFactory.createEtchedBorder());
        GroupLayout plTitleLayout = new GroupLayout(panelTitle);
        panelTitle.setLayout(plTitleLayout);
        plTitleLayout.setHorizontalGroup(
                plTitleLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(plTitleLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                .addGap(50, 50, 50)));
        plTitleLayout.setVerticalGroup(
                plTitleLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(plTitleLayout.createSequentialGroup()
                .addComponent(lblTitle, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)));
        return panelTitle;
    }

    private JPanel getPanelButtonGroup()
    {
        JPanel panelButtonGroup = new JPanel();
        btnHelp = new JButton(constBundle.getString("help"));
        btnBack = new JButton(constBundle.getString("back"));
        btnNext = new JButton(constBundle.getString("next"));
        btnMigrate = new JButton(constBundle.getString("start"));
        btnCancle = new JButton(constBundle.getString("cancle"));
        btnBack.setEnabled(false);
        btnMigrate.setEnabled(false);
        //btnFinish.setEnabled(false);
        Font fontBtn = new Font("宋体", 0, 12);
        btnHelp.setFont(fontBtn);
        btnNext.setFont(fontBtn);
        btnBack.setFont(fontBtn);
        btnMigrate.setFont(fontBtn);
        btnCancle.setFont(fontBtn);
        GroupLayout btnPanelLayout = new GroupLayout(panelButtonGroup);
        panelButtonGroup.setLayout(btnPanelLayout);
        btnPanelLayout.setHorizontalGroup(
                btnPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(btnPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnHelp, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBack, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnNext, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnMigrate, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                //.addComponent(btnFinish, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                //.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancle, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addContainerGap()));
        btnPanelLayout.setVerticalGroup(
                btnPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, btnPanelLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(btnPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(btnBack)
                .addComponent(btnNext)
                .addComponent(btnMigrate)
                //.addComponent(btnFinish)
                .addComponent(btnCancle)
                .addComponent(btnHelp))
                .addContainerGap()));
        return panelButtonGroup;
    }

    private JPanel getPanelGuid()
    {
        JPanel panelGuid = new JPanel();
        lblGuidIntroduce = new JLabel();
        lblGuidHGDB = new JLabel();
        lblGuidSourceDB = new JLabel();
        lblGuidChoose = new JLabel();
        lblGuidType = new JLabel();
        lblGuidMigrate = new JLabel();

        fontNormal = new Font(constBundle.getString("fontSongTi"), 0, 13);
        fontBold = new Font(constBundle.getString("fontSongTi"), 1, 13);
        ImageIcon enableIcon = new ImageIcon(getClass().getResource(
                "/com/highgo/hgdbadmin/image/migrateNow.png"));
        ImageIcon disableIcon = new ImageIcon(getClass().getResource(
                "/com/highgo/hgdbadmin/image/migrateNot.png"));
        panelGuid.setPreferredSize(new Dimension(150, 430));
        panelGuid.setBorder(BorderFactory.createEtchedBorder());
        //panelGuid.setBorder(BorderFactory.createTitledBorder(null, constBundle.getString("navigation"),
        //        TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, fontSmall, colorBlue));

        lblGuidIntroduce.setText(constBundle.getString("introduce"));
        lblGuidIntroduce.setFont(fontBold);
        lblGuidIntroduce.setIcon(enableIcon);
        lblGuidIntroduce.setDisabledIcon(disableIcon);
        lblGuidIntroduce.setEnabled(true);
        lblGuidHGDB.setText(constBundle.getString("hgDB"));
        lblGuidHGDB.setFont(fontNormal);
        lblGuidHGDB.setIcon(enableIcon);
        lblGuidHGDB.setDisabledIcon(disableIcon);
        lblGuidHGDB.setEnabled(false);
        lblGuidSourceDB.setText(constBundle.getString("sourceDB"));
        lblGuidSourceDB.setFont(fontNormal);
        lblGuidSourceDB.setIcon(enableIcon);
        lblGuidSourceDB.setDisabledIcon(disableIcon);
        lblGuidSourceDB.setEnabled(false);
        lblGuidChoose.setText(constBundle.getString("chooseObject"));
        lblGuidChoose.setFont(fontNormal);
        lblGuidChoose.setIcon(enableIcon);
        lblGuidChoose.setDisabledIcon(disableIcon);
        lblGuidChoose.setEnabled(false);
        lblGuidType.setText(constBundle.getString("dataType"));
        lblGuidType.setFont(fontNormal);
        lblGuidType.setIcon(enableIcon);
        lblGuidType.setDisabledIcon(disableIcon);
        lblGuidType.setEnabled(false);
        lblGuidMigrate.setText(constBundle.getString("migrateData"));
        lblGuidMigrate.setFont(fontNormal);
        lblGuidMigrate.setFont(fontNormal);
        lblGuidMigrate.setIcon(enableIcon);
        lblGuidMigrate.setDisabledIcon(disableIcon);
        lblGuidMigrate.setEnabled(false);

        GroupLayout plGuidLayout = new GroupLayout(panelGuid);
        panelGuid.setLayout(plGuidLayout);
        plGuidLayout.setHorizontalGroup(
                plGuidLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, plGuidLayout.createSequentialGroup()
                .addGap(15, 15, 15)//.addContainerGap()
                .addGroup(plGuidLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(lblGuidMigrate, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)//GroupLayout.DEFAULT_SIZE
                .addComponent(lblGuidType, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addComponent(lblGuidChoose, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addComponent(lblGuidSourceDB, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addComponent(lblGuidHGDB, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                .addComponent(lblGuidIntroduce, GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))));
        plGuidLayout.setVerticalGroup(
                plGuidLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(plGuidLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblGuidIntroduce)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGuidHGDB)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGuidSourceDB)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGuidChoose)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGuidType)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblGuidMigrate)
                .addGap(0, 223, Short.MAX_VALUE)));
        return panelGuid;
    }
}
