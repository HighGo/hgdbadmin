/* ------------------------------------------------
 *
 * File: DataTypeFactory.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\controller\DataTypeFactory.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class DataTypeFactory
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());

    private DataTypeFactory()
    {
    }
    private static DataTypeFactory dataTypeController = null;

    public static DataTypeFactory getInstance()
    {
        if (dataTypeController == null)
        {
            dataTypeController = new DataTypeFactory();
        }
        return dataTypeController;
    }

    public List<List<String>> getTypeCastList(ObjEnum.DBType sourceDB)
    {
        logger.info("Enter:sourceDB = " + sourceDB);
        List<List<String>> hg4SourceTypeList = new ArrayList();

        switch (sourceDB)
        {
            case ORACLE:
                List<String> oracletype = new ArrayList();
                List<String> hg4OracleType = new ArrayList();
                //all have parameter - scale
                oracletype.add("INTERVAL DAY\\(\\d{1,9}\\) TO SECOND\\([0-9]\\)");
                hg4OracleType.add("INTERVAL");//INTERVAL(scale)
                oracletype.add("INTERVAL YEAR\\(\\d{1,9}\\) TO MONTH");
                hg4OracleType.add("INTERVAL");//INTERVAL(scale),scale=0

                //all have parameter - scale
                oracletype.add("TIMESTAMP\\([0-9]\\) WITH.*");//TIMESTAMP(scale) WITH TIME ZONE / WITH LOCAL TIME ZONE
                hg4OracleType.add("TIMESTAMPTZ");//TIMESTAMP(scale) WITH TIME ZONE
                oracletype.add("TIMESTAMP\\([0-9]\\)");//has no time zone
                hg4OracleType.add("TIMESTAMP");//TIMESTAMP(scale)

                oracletype.add("DATE");//second has no fractional seconds and time zone
                hg4OracleType.add("TIMESTAMP(0)");//

                //all have the parmeter -- length
                oracletype.add("CHAR");
                hg4OracleType.add("CHAR");
                oracletype.add("NCHAR");
                hg4OracleType.add("CHAR");
                //creating VARCHAR convert to VARCHAR2 in oracle
                //oracletype.add("VARCHAR");
                //hg4OracleType.add("VARCHAR");
                oracletype.add("VARCHAR2");
                hg4OracleType.add("VARCHAR");
                oracletype.add("NVARCHAR2");
                hg4OracleType.add("VARCHAR");

                //no parameter
                oracletype.add("LONG");
                hg4OracleType.add("TEXT");//TEXT, BYTEA, OID
                oracletype.add("CLOB");
                hg4OracleType.add("TEXT");//TEXT, OID
                oracletype.add("NCLOB");
                hg4OracleType.add("TEXT");//TEXT, OID

                //no parameter
                oracletype.add("BFILE");
                hg4OracleType.add("BYTEA");//BYTEA, OID
                oracletype.add("BLOB");
                hg4OracleType.add("BYTEA");//BYTEA, OID
                oracletype.add("LONG RAW");
                hg4OracleType.add("BYTEA");//BYTEA, OID
                oracletype.add("RAW");// <=2000 byte ,omit its parameter(<=2000) and directly convert to bytea
                hg4OracleType.add("BYTEA");//BYTEA, OID

                //all has two parameter- precision and scale
                //ambiguous -- oracle type and PG type has different parameter data range
                oracletype.add("NUMBER");
                hg4OracleType.add("NUMERIC");
                //creating NUMERIC,DEC,DECIMAL convert to NUMBER[P,S] or NUMBER in oracle
                //oracletype.add("NUMERIC");
                //hg4OracleType.add("NUMERIC");
                //oracletype.add("EDC");
                //hg4OracleType.add("NUMERIC");
                //oracletype.add("DECIMAL");
                //hg4OracleType.add("NUMERIC");
                //creating INT,INTEGER,SMALLINT convert to NUMBER in oracle
                //oracletype.add("INT");
                //hg4OracleType.add("NUMERIC");
                //oracletype.add("INTEGER");
                //hg4OracleType.add("NUMERIC");
                //oracletype.add("SMALLINT");
                //hg4OracleType.add("NUMERIC");

                //maybe no parameter
                //ambiguous -- in PG float8 equals double precision;float4 equals real;
                //but those two has different float point
                oracletype.add("BINARY_DOUBLE");
                hg4OracleType.add("DOUBLE PRECISION");
                oracletype.add("BINARY_FLOAT");
                hg4OracleType.add("DOUBLE PRECISION");
                oracletype.add("FLOAT");
                hg4OracleType.add("DOUBLE PRECISION");
                //oracletype.add("REAL");
                //hg4OracleType.add("DOUBLE PRECISION");
                //oracletype.add("DOUBLE PRECISION");
                //hg4OracleType.add("DOUBLE PRECISION");

                //ambiguous,now just test
                oracletype.add("ROWID");
                hg4OracleType.add("OID");
                //oracletype.add("UROWID");//has size--urowid[（size）]
                //hg4OracleType.add("OID");

                //no parameter
                oracletype.add("XMLTYPE");//new add
                hg4OracleType.add("XML");

                hg4SourceTypeList.add(oracletype);
                hg4SourceTypeList.add(hg4OracleType);
                break;
            case SQLSERVER:
                List<String> sqlServerType = new ArrayList();
                List<String> hg4SQLServerType = new ArrayList();

                sqlServerType.add("bigint");
                hg4SQLServerType.add("bigint");
                sqlServerType.add("binary");
                hg4SQLServerType.add("bytea");
                sqlServerType.add("bit");
                hg4SQLServerType.add("boolean");
                sqlServerType.add("char");
                hg4SQLServerType.add("char");
                sqlServerType.add("date");
                hg4SQLServerType.add("date");
                sqlServerType.add("datetime");
                hg4SQLServerType.add("timestamp");
                sqlServerType.add("datetime2");
                hg4SQLServerType.add("timestamp");
                sqlServerType.add("datetimeoffset");
                hg4SQLServerType.add("timestamp");
                sqlServerType.add("decimal");
                hg4SQLServerType.add("decimal");
                sqlServerType.add("float");
                hg4SQLServerType.add("double precision");
                sqlServerType.add("geography");
                hg4SQLServerType.add("geography");
                sqlServerType.add("geometry");
                hg4SQLServerType.add("geometry");
                sqlServerType.add("hierarchyid");
                hg4SQLServerType.add("hierarchyid");
                sqlServerType.add("image");
                hg4SQLServerType.add("bytea");
                sqlServerType.add("int");
                hg4SQLServerType.add("int");
                sqlServerType.add("money");
                hg4SQLServerType.add("numeric");
                sqlServerType.add("nchar");
                hg4SQLServerType.add("char");
                sqlServerType.add("ntext");
                hg4SQLServerType.add("text");
                sqlServerType.add("numeric");
                hg4SQLServerType.add("numeric");
                sqlServerType.add("nvarchar");
                hg4SQLServerType.add("varchar");
                sqlServerType.add("nvarchar");
                hg4SQLServerType.add("varchar");
                sqlServerType.add("real");
                hg4SQLServerType.add("real");
                sqlServerType.add("smalldatetime");
                hg4SQLServerType.add("timestamp");
                sqlServerType.add("smallint");
                hg4SQLServerType.add("smallint");
                sqlServerType.add("smallmoney");
                hg4SQLServerType.add("numeric");
                sqlServerType.add("sql_variant");
                hg4SQLServerType.add("text");
                sqlServerType.add("text");
                hg4SQLServerType.add("text");
                sqlServerType.add("time");
                hg4SQLServerType.add("time");
                sqlServerType.add("timestamp");
                hg4SQLServerType.add("timestamp");
                sqlServerType.add("tinyint");
                hg4SQLServerType.add("smallint");
                sqlServerType.add("uniqueidentifier");
                hg4SQLServerType.add("varchar");
                sqlServerType.add("varbinary");
                hg4SQLServerType.add("bytea");
                sqlServerType.add("varbinary");
                hg4SQLServerType.add("bytea");
                sqlServerType.add("varchar");
                hg4SQLServerType.add("varchar");
                sqlServerType.add("varchar");
                hg4SQLServerType.add("varchar");
                sqlServerType.add("xml");
                hg4SQLServerType.add("bytea");

                hg4SourceTypeList.add(sqlServerType);
                hg4SourceTypeList.add(hg4SQLServerType);
                break;
        }
        logger.info("Return: hg4SourceTypeList = " + hg4SourceTypeList.size());
        return hg4SourceTypeList;
    }
}
