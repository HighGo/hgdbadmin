/* ------------------------------------------------
 *
 * File: SQLFactory.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\controller\SQLFactory.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import static com.highgo.hgdbadmin.migrationassistant.util.ObjEnum.ObjType.SCRIPT_OBJ;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 *
 * property to make sql statement: table:schema,table,column list for table
 * create sql and table select sql; index:schema,table,index name, is
 * unique,using method(likes btree...),column list for index create sql;
 * sequences:schema,sequence,increment,minimum value,maximum value,start,cache
 * for sequences create sql; view:schema,view,query for view create sql; Script
 * object(procedure,function,trigger):type,schema,name,text for object create
 * sql script;
 *
 */
public class SQLFactory
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private SQLFactory()
    {
    }
    private static SQLFactory sqlController = null;
    public static SQLFactory getInstance()
    {
        if (sqlController == null)
        {
            sqlController = new SQLFactory();
        }
        return sqlController;
    }

    public String getObjSQL(ObjEnum.DBType db, ObjEnum.DBObj obj)
    {
        logger.info("Enter:dbType = " + db + ",objType = " + obj.toString());
        StringBuilder sql = new StringBuilder();
        switch (db)
        {
            case ORACLE:
                switch (obj)
                {
                    case SCHEMA:
                        sql.append("SELECT DISTINCT user,user FROM user_objects");
                        break;
                    case TABLE:
                        //user_tab_column include tables and views
                        sql.append("SELECT DISTINCT user,table_name FROM user_objects,user_tables ORDER BY table_name");
                        break;
                    case SEQUENCE:
                        sql.append("SELECT DISTINCT user,sequence_name");
                        sql.append(" FROM user_sequences");
                        break;
                    case INDEX:
                        sql.append("SELECT DISTINCT user,ind.index_name FROM user_indexes ind ");
                        sql.append(" WHERE ind.index_name NOT IN(SELECT constraint_name FROM user_constraints)");
                        sql.append(" AND ind.index_type  IN ('NORMAL')");
                        break;
                    case VIEW:
                        sql.append("SELECT DISTINCT user,view_name FROM user_views");
                        break;
                    case PROCEDURE:
                        sql.append("SELECT DISTINCT user,name,type FROM user_source WHERE type = 'PROCEDURE'");
                        break;
                    case TRIGGER:
                        sql.append("SELECT DISTINCT user,name,type FROM user_source WHERE type = 'TRIGGER'");
                        break;
                    case FUNCTION:
                        sql.append("SELECT DISTINCT user,name,type FROM user_source WHERE type = 'FUNCTION'");
                        break;
//                    case SCRIPT_OBJ:
//                        sql.append("SELECT DISTINCT user,name,type FROM user_source ");
//                        sql.append(" WHERE type IN('PROCEDURE','TRIGGER','FUNCTION') ");
//                        sql.append(" ORDER BY type,name");
//                        break;
                }
                break;
            case SQLSERVER:
                switch (obj)
                {
                    case SCHEMA:
                        break;
                    case TABLE:
                        break;
                }
                break;
        }
        //logger.info("Return :sql = " + sql.toString());
        return sql.toString();
    }

    public String getSelectSQL(ObjEnum.DBType db, ObjEnum.ObjType obj)
    {
        logger.info("Enter:dbType = " + db + ",objType = " + obj.toString());
        StringBuilder sql = new StringBuilder();
        switch (db)
        {
            case ORACLE:
                switch (obj)
                {
                    case SCHEMA:
                        sql.append("SELECT DISTINCT user,user FROM user_objects");
                        break;
                    case TABLE:
                        //user_tab_column include tables and views
                        sql.append("SELECT DISTINCT user,table_name FROM user_objects,user_tables ORDER BY table_name");
                        break;
                    case COLUMN:
                        sql.append("SELECT user AS schema,user_tables.table_name,user_tab_columns.column_name,");
                        sql.append("user_tab_columns.data_type,user_tab_columns.data_default,");
                        sql.append("user_tab_columns.data_precision,");
                        sql.append("user_tab_columns.data_scale, user_tab_columns.data_length, user_tab_columns.nullable");
                        sql.append(" FROM user_tab_columns, user_tables ");
                        sql.append(" WHERE user_tab_columns.table_name = user_tables.table_name  ");
                        sql.append(" AND user = ?");
                        sql.append(" AND user_tables.table_name = ?");
                        sql.append(" ORDER BY user_tables.table_name ASC , user_tab_columns.column_id ASC");
                        break;
                    case CONSTRAINT_MAIN:
                        sql.append("SELECT DISTINCT user_constraints.constraint_type,user, user_cons_columns.table_name,");
                        sql.append("user_constraints.constraint_name,user_constraints.r_constraint_name");
                        sql.append(" FROM user_constraints,user_cons_columns");
                        sql.append(" WHERE user_constraints.constraint_name = user_cons_columns.constraint_name");
//                        sql.append(" AND user_constraints.constraint_type = ?");//type
                        sql.append(" AND user = ? AND user_cons_columns.table_name = ?");
                        sql.append(" ORDER BY user_constraints.constraint_type, user_constraints.constraint_name");
                        break;
                    case CONSTRAINT_COLUMN:
                        sql.append("SELECT user_cons_columns.column_name,user_constraints.constraint_type,");
                        sql.append("user, user_cons_columns.table_name, user_constraints.constraint_name");
                        sql.append(" FROM user_constraints,user_cons_columns ");
                        sql.append(" WHERE user_constraints.constraint_name = user_cons_columns.constraint_name ");
                        sql.append(" AND user_constraints.constraint_type = ? ");
                        sql.append(" AND user = ? ");
                        sql.append(" AND user_cons_columns.table_name = ? ");
                        sql.append(" AND user_constraints.constraint_name = ? ");
                        sql.append(" ORDER BY user_constraints.constraint_type, user_constraints.constraint_name");
                        break;
                    case CONSTRAINT_CHECKCONDITION:
                        sql.append("SELECT user_constraints.search_condition,user_constraints.constraint_type,");
                        sql.append("user, user_cons_columns.table_name, user_constraints.constraint_name");
                        sql.append(" FROM user_constraints,user_cons_columns ");
                        sql.append(" WHERE user_constraints.constraint_name = user_cons_columns.constraint_name ");
                        sql.append(" AND user_constraints.constraint_type = 'C' ");
                        sql.append(" AND user = ? ");
                        sql.append(" AND user_cons_columns.table_name = ? ");
                        sql.append(" AND  user_constraints.constraint_name = ? ");
                        sql.append(" ORDER BY user_constraints.constraint_type, user_constraints.constraint_name");
                        break;
                    case CONSTRAINT_RCOLUMN:
                        sql.append("SELECT DISTINCT user, user_cons_columns.table_name, user_cons_columns.column_name, ");
                        sql.append("user_constraints.constraint_name,user_constraints.constraint_type");
                        sql.append(" FROM user_constraints,user_cons_columns");
                        sql.append(" WHERE user_constraints.constraint_name = user_cons_columns.constraint_name");
                        sql.append(" AND user = ? ");
                        sql.append(" AND user_constraints.constraint_name = ?");//reference constraint name
                        sql.append(" ORDER BY user_constraints.constraint_type, user_constraints.constraint_name,user_cons_columns.column_name");
                        break;
                    case SEQUENCE:
                        sql.append("SELECT user,sequence_name, last_number,min_value, max_value,  increment_by,");
                        sql.append("cache_size, null, null FROM user_sequences");
                        sql.append(" WHERE user = ? AND  sequence_name = ?");
                        break;
                    case INDEX:
                        sql.append("SELECT DISTINCT ind.uniqueness,user,ind.index_name,ind.index_type,ind.table_name,");
                        sql.append("inc.COLUMN_NAME,inc.DESCEND,inc.COLUMN_POSITION");
                        sql.append(" FROM user_indexes ind,user_ind_columns inc");
                        sql.append(" WHERE ind.index_name = inc.INDEX_NAME");
                        sql.append(" AND ind.index_name NOT IN(SELECT constraint_name FROM user_constraints)");
                        sql.append(" AND ind.index_type  IN ('NORMAL')");
                        sql.append(" AND user = ? ");
                        sql.append(" AND ind.index_name = ? ");
                        sql.append(" ORDER BY ind.table_name,inc.COLUMN_POSITION");
                        break;
                    case VIEW:
                        sql.append("SELECT user,view_name,text FROM user_views");
                        sql.append(" WHERE user = ? AND  view_name = ?");
                        break;
                    case SCRIPT_OBJ:
                        sql.append("SELECT text from user_source");
                        sql.append(" WHERE type = ? ");
                        sql.append(" AND name = ? ");
                        sql.append(" AND user = ? ");
                        sql.append(" ORDER BY line");//only generate script
                        break;
//                    case PROCEDURE:
//                        sql.append("SELECT text from user_source");
//                        sql.append(" WHERE user = ? ");
//                        sql.append(" AND name = ? ");
//                        sql.append(" AND type = 'PROCEDURE'");
//                        sql.append(" ORDER BY line");//only generate script
//                        break;
//                    case TRIGGER:
//                        sql.append("SELECT text from user_source");
//                        sql.append(" WHERE user = ? ");
//                        sql.append(" AND name = ? ");
//                        sql.append(" AND type = 'TRIGGER'");
//                        sql.append(" ORDER BY line");//only generate script
//                        break;
//                    case FUNCTION:
//                        sql.append("SELECT text from user_source");
//                        sql.append(" WHERE user = ? ");
//                        sql.append(" AND name = ? ");
//                        sql.append(" AND type = 'FUNCTION'");
//                        sql.append(" ORDER BY line");//only generate script
//                        break;
                }
                break;
            case SQLSERVER:
                switch (obj)
                {
                    case SCHEMA:
                        break;
                    case TABLE:
                        break;
                    case COLUMN:
                        break;
                }
                break;
        }
        //logger.info("Return: sql = " + sql.toString());
        return sql.toString();
    }
}
