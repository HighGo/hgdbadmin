/* ------------------------------------------------
 *
 * File: MigrationController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\controller\MigrationController.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.model.ConstrInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DBConnInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DataTypeCastDTO;
import com.highgo.hgdbadmin.migrationassistant.model.DataTypeDTO;
import com.highgo.hgdbadmin.migrationassistant.model.FKConstrInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ObjInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ObjListDTO;
import com.highgo.hgdbadmin.migrationassistant.model.ProgressInfoDTO;
import com.highgo.hgdbadmin.migrationassistant.util.ConnectHelper;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import static com.highgo.hgdbadmin.migrationassistant.util.ObjEnum.DBType.ORACLE;
import com.highgo.hgdbadmin.util.CloseStream;
import com.highgo.hgdbadmin.util.JdbcHelper;
//ygq v5 update start
//import com.highgo.jdbc.HGConnection;
import com.highgo.jdbc.PGConnection;
//ygq v5 update start
import com.highgo.jdbc.copy.CopyManager;
import com.highgo.jdbc.core.BaseConnection;
import com.highgo.jdbc.largeobject.LargeObject;
import com.highgo.jdbc.largeobject.LargeObjectManager;
import com.highgo.jdbc.util.PGInterval;
//import org.postgresql.PGConnection;
//import org.postgresql.copy.CopyManager;
//import org.postgresql.core.BaseConnection;
//import org.postgresql.largeobject.LargeObject;
//import org.postgresql.largeobject.LargeObjectManager;
//import org.postgresql.util.PGInterval;
import java.awt.Desktop;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import oracle.sql.BFILE;
import java.sql.BatchUpdateException;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class MigrationController
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");    
    private SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    private ProgressInfoDTO progressInfo = new ProgressInfoDTO();
    private boolean isStop = false;

    private MigrationController()
    {        
    }
    private static MigrationController migrateController = null;

    public static MigrationController getInstance()
    {
        if (migrateController == null)
        {
            migrateController = new MigrationController();
        }
        return migrateController;
    }

    //need to improve - get default datatype casting list
    public List<DataTypeCastDTO> getDefaultDataTypeCastList(ObjEnum.DBType dbType)
    {
        logger.info("Enter:dbType = " + dbType.toString());
        List<DataTypeCastDTO> dataTypeList = new ArrayList();
        DataTypeFactory dtf = DataTypeFactory.getInstance();
        List<List<String>> typeCastList = dtf.getTypeCastList(dbType);
        int size = typeCastList.get(0).size();
        logger.info("DataTypeList = " + size);
        for (int i = 0; i < size; i++)
        {
            DataTypeCastDTO dataTypeCastType = new DataTypeCastDTO();

            DataTypeDTO sourceDataType = new DataTypeDTO();
            sourceDataType.setDataType(typeCastList.get(0).get(i));
//            SourceDataTypeDTO.setDataPrecision();
//            SourceDataTypeDTO.setDataScale();
            DataTypeDTO hgDataType = new DataTypeDTO();
            hgDataType.setDataType(typeCastList.get(1).get(i));
//            hgDataTypeDTO.setDataPrecision(null);
//            hgDataTypeDTO.setDataScale(null);
            dataTypeCastType.setSourceDB(dbType);
            dataTypeCastType.setSourceDataType(sourceDataType);
            dataTypeCastType.setHgDataType(hgDataType);
            dataTypeCastType.setIsSystem(true);

            dataTypeList.add(dataTypeCastType);
        }
        logger.info("Return: DataTypeList = " + dataTypeList.size());
        return dataTypeList;
    }

    //test whether source DB information is right, and get objects for choose
    public ObjListDTO getObjListDTO(DBConnInfoDTO sourceDBInfo) throws Exception
    {
        logger.info("Enter:" + sourceDBInfo.getDBType());
        ObjListDTO objListDTO = new ObjListDTO();
        Connection conn = null;
        Statement stmt = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            stmt = conn.createStatement();

            SQLFactory sqlf = SQLFactory.getInstance();
            for (ObjEnum.DBObj obj : ObjEnum.DBObj.values())
            {
                List<ObjInfoDTO> objInfoList = new ArrayList();
                String sql = sqlf.getObjSQL(sourceDBInfo.getDBType(), obj);
                ResultSet rtset = null;
                try
                {
                    rtset = stmt.executeQuery(sql);
                    while (rtset.next())
                    {
                        ObjInfoDTO objInfo = new ObjInfoDTO();
                        objInfo.setType(obj);
                        objInfo.setSchema(rtset.getString(1));
                        objInfo.setName(rtset.getString(2));
                        objInfo.setSelected(false);
                        objInfoList.add(objInfo);
                    }
                    switch (obj)
                    {
                        case SCHEMA:
                            objListDTO.setSchemaList(objInfoList);
                            break;
                        case TABLE:
                            objListDTO.setTableList(objInfoList);
                            break;
                        case SEQUENCE:
                            objListDTO.setSequenceList(objInfoList);
                            break;
                        case VIEW:
                            objListDTO.setViewList(objInfoList);
                            break;
                        case INDEX:
                            objListDTO.setIndexList(objInfoList);
                            break;
                        case PROCEDURE:
                            objListDTO.setProcedureList(objInfoList);
                            break;
                        case FUNCTION:
                            objListDTO.setFunctionList(objInfoList);
                            break;
                        case TRIGGER:
                            objListDTO.setTriggerList(objInfoList);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    logger.error("Error: " + ex.getMessage());
                    ex.printStackTrace(System.out);
                }
                finally
                {
                    JdbcHelper.close(rtset);
                    logger.info("Close ResultSet");
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex.getMessage());
        }
        finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }

        return objListDTO;
    }

    private void writeLog(PrintWriter pw, String content)
    {
        pw.write(content);
        pw.println();
        pw.flush();
    }

    public ProgressInfoDTO getProgressInfo()
    {
        return progressInfo;
    }

    // enforce the cancle button could not only dispose the view
    // but also stop this thread.
    public void stopMigrateThread()
    {
        isStop = true;    
    }

    public void startMigrateThread(String logPath, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            List<ObjInfoDTO> choosedObjInfoList, List<DataTypeCastDTO> dataTypeCastList,
            String migrateModel, int copyBatch, int insertBatch, String delimiter, String quoteChar)
    {
        progressInfo = new ProgressInfoDTO();
        isStop = false;
        
        MigrateRunnable mRunnable = new MigrateRunnable(logPath, sourceDBInfo,
                hgDBInfo, choosedObjInfoList, dataTypeCastList,
                migrateModel, copyBatch, insertBatch, delimiter, quoteChar);
        Thread thread = new Thread(mRunnable);
        thread.start();
    }

    private class MigrateRunnable implements Runnable
    {
        private String logPath;
        private DBConnInfoDTO sourceDBInfo;
        private DBConnInfoDTO hgDBInfo;
        private List<ObjInfoDTO> choosedObjInfoList;
        private List<DataTypeCastDTO> dataTypeCastList;
        private int copyBatch;
        private int insertBatch;
        private String migrateModel;
        private String delimiter;
        private String quoteChar;

        public MigrateRunnable(String logPath, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
                List<ObjInfoDTO> choosedObjInfoList, List<DataTypeCastDTO> dataTypeCastList,
                String migrateModel, int copyBatch, int insertBatch, String delimiter, String quoteChar)
        {
            this.logPath = logPath;
            this.sourceDBInfo = sourceDBInfo;
            this.hgDBInfo = hgDBInfo;
            this.choosedObjInfoList = choosedObjInfoList;
            this.dataTypeCastList = dataTypeCastList;
            this.copyBatch = copyBatch;
            this.insertBatch = insertBatch;
            this.migrateModel = migrateModel;
            this.delimiter = delimiter;
            this.quoteChar = quoteChar;
        }

        @Override
        public void run()
        {
            migrate(logPath, sourceDBInfo, hgDBInfo, choosedObjInfoList, dataTypeCastList,
                    migrateModel, copyBatch, insertBatch, delimiter, quoteChar);
        }
    }

    public void migrate(String logPath, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            List<ObjInfoDTO> choosedObjInfoList, List<DataTypeCastDTO> dataTypeCastList,
            String migrateModel, int copyBatch, int insertBatch, String delimiter, String quoteChar)
    {
        File file = new File(logPath);
        if (!file.exists())
        {
            file.mkdirs();
            logger.info("logPath doesn't exist, create this directory.");
        }

        int str = time.format(new Date()).hashCode();
        if (str < 0)
        {
            str = - str;
        }
        String pathScript = logPath + File.separator + str
                + "Scripts.sql";
        logger.info("pathScript：" + pathScript);
        String pathError = logPath + File.separator + str
                +"Errors.sql";
        logger.info("pathError：" + pathError);
        String pathResult = logPath + File.separator + str
                + "Result.html";

        PrintWriter pwScript = null;
        PrintWriter pwError = null;
        PrintWriter pwResult = null;

        List<FKConstrInfoDTO> fkSqlList = new ArrayList();
        int objSize = choosedObjInfoList.size();

        logger.info("objSize = " + objSize);
        if (objSize == 0)
        {
            logger.info("No Object Need To Migrate, Do Nothing, And Return.");
            progressInfo.setState("No Object Need To Migrate.");
            progressInfo.setDetail(progressInfo.getDetail().append(progressInfo.getState()));
            try
            {
                pwResult = new PrintWriter(pathResult);
                writeLog(pwResult, " -- Result -- <br/> migrated nothing at all.");
                File fileResult = new File(pathResult);
                Desktop desktop = Desktop.getDesktop();
                desktop.open(fileResult);
            }
            catch (Exception ex)
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
            }
            return;
        }

        progressInfo.setMaxValue(objSize);
        progressInfo.setState(time.format(new Date()) + " Start Migrating Data:" + objSize);
        progressInfo.setDetail(progressInfo.getDetail().append(progressInfo.getState()).append("\n\r"));
        progressInfo.setValue(0);
        int count = 1;
        try
        {
            pwScript = new PrintWriter(pathScript);
            pwError = new PrintWriter(pathError);
            pwResult = new PrintWriter(pathResult);
            writeLog(pwScript, " -- Scripts of Procedure, Function and Trigger -- ");
            writeLog(pwError, " -- Errors -- ");
            writeLog(pwResult, " -- Result -- <br/> "+ time.format(new Date())+"<br/> Total Objects:" + objSize +"<br/>" );
            for (ObjInfoDTO objInfo : choosedObjInfoList)
            {
                if (isStop)
                {
                    break;
                }
                logger.debug(objInfo.getType() + " " + objInfo.getName());
                String isSuccess = "Successed";
                switch (objInfo.getType())
                {
                    case SCHEMA:
                        progressInfo.setState(count + "/" + objSize + " Create Schema:" + objInfo.getName());
                        isSuccess = createSchema(pwResult, pwError, hgDBInfo, objInfo.getName());
                        break;
                    case TABLE:
                        progressInfo.setState(count + "/" + objSize + " Migrate Table:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = migrateTable(pwResult, pwError, sourceDBInfo, hgDBInfo, dataTypeCastList,
                                fkSqlList, objInfo.getSchema(), objInfo.getName(),migrateModel, copyBatch, insertBatch,delimiter, quoteChar);
                        break;
                    case SEQUENCE:
                        progressInfo.setState(count + "/" + objSize + " Create Sequence:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = createSequence(pwResult, pwError, sourceDBInfo, hgDBInfo, objInfo.getSchema(),
                                objInfo.getName());
                        break;
                    case VIEW:
                        progressInfo.setState(count + "/" + objSize + " Create View:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = createView(pwResult, pwError, sourceDBInfo, hgDBInfo, objInfo.getSchema(), objInfo.getName());
                        break;
                    case INDEX:
                        progressInfo.setState(count + "/" + objSize + " Create Index:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = createIndex(pwResult, pwError, sourceDBInfo, hgDBInfo, objInfo.getSchema(), objInfo.getName());
                        break;
                    case PROCEDURE:
                        progressInfo.setState(count + "/" + objSize + " Generate Procedure Script:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = writeObjScript(pwResult, pwError, sourceDBInfo, pwScript, objInfo);
                        break;
                    case FUNCTION:
                        progressInfo.setState(count + "/" + objSize + " Generate Function Script:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = writeObjScript(pwResult, pwError, sourceDBInfo, pwScript, objInfo);
                        break;
                    case TRIGGER:
                        progressInfo.setState(count + "/" + objSize + " Generate Trigger Script:" + objInfo.getSchema()
                                + "." + objInfo.getName());
                        isSuccess = writeObjScript(pwResult, pwError, sourceDBInfo, pwScript, objInfo);
                        break;
                }
                progressInfo.setValue(count);
                progressInfo.setDetail(progressInfo.getDetail().
                        append(progressInfo.getState()).append(" ").append(isSuccess).append("\n\r"));
                count++;
            }
        }
        catch (Exception ex)
        {
            logger.error("Error:" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            CloseStream.close(pwScript);
            logger.info("Close Resource");
        }
        writeLog(pwResult, "<br/>"+time.format(new Date()) + "<br/>"); 
        progressInfo.setState(time.format(new Date()) + " Finished Migrating Data");
        progressInfo.setDetail(progressInfo.getDetail().
                append(progressInfo.getState()).append("\n\r"));

        if (!migrateModel.equals(constBundle.getString("onlyData")))
        {
            int fkcSize = fkSqlList.size();
            logger.info("FKsize = " + fkcSize);
            writeLog(pwResult, " Total FKey:" + fkcSize +"<br/>");
            if (fkcSize == 0)
            {
                logger.info("No FKey Constraint Need To Migrate, Do Nothing, And Return.");
                writeLog(pwResult, time.format(new Date()) + "<br/>");
                progressInfo.setState(time.format(new Date()) + " No FKey Constraint Need To Migrate");
                progressInfo.setDetail(progressInfo.getDetail().append(progressInfo.getState()));
            }
            else
            {
                progressInfo.setMaxFKValue(fkcSize);
                progressInfo.setValue(0);
                progressInfo.setState(time.format(new Date()) + " Start Migrating FK Constraints:" + fkcSize);
                progressInfo.setDetail(progressInfo.getDetail().
                        append(progressInfo.getState()).append("\n\r"));
                
                addFKConstrList(pwResult, pwError, hgDBInfo, fkSqlList);
           
                writeLog(pwResult, "<br/>" + time.format(new Date()) + "<br/>");
                progressInfo.setState(time.format(new Date()) + " Finished Migrating FK Constraints");
                progressInfo.setDetail(progressInfo.getDetail().
                        append(progressInfo.getState()).append("\n\r"));
            }
        }

        File fileResult = new File(pathResult);
        Desktop desktop = Desktop.getDesktop();
        try
        {
            desktop.open(fileResult);
        }
        catch (IOException ex)
        {
            logger.error("Error: " + ex.getMessage());
        }
    }

    //ocheck table name and column name-- will use for checking column name
    private String checkQuoteObjName(String objName, ObjEnum.DBType dbType)
    {
        //check whether the object name is key word
        KeyWordFactory kwf = KeyWordFactory.getInstance();
        List<String> keyWordList = kwf.getKeyWords(dbType);
        int size = keyWordList.size();
        for (int i = 0; i < size; i++)
        {
            String patternString = keyWordList.get(i).toUpperCase();
            if (objName.toUpperCase().equals(patternString))//or matches()
            {
                objName = "\"" + objName + "\"";
                logger.info(objName + " isReservedKeyWord ");
                return objName;
            }
        }
        //check whether object name need to be quoted
        objName = checkQuoteIdentifer(objName, dbType);
        return objName;
    }

    //check whether object name need to be quoted
    private String checkQuoteIdentifer(String objName, ObjEnum.DBType dbType)
    {
        switch (dbType)
        {
            case HGDB:
                if (String.valueOf(objName.charAt(0)).matches("[^A-Z]"))
                {
                    objName = "\"" + objName + "\"";
                    logger.info(objName
                            + " not start with upper letter, which HG not allowed "
                            + "(include starting with underline_ ,which Oracle quoted) so quote it.");
                }
                else if (objName.contains("#"))
                {
                    objName = "\"" + objName + "\"";
                    logger.info(objName
                            + " contains #(Oracle allowed), HG not allowed, so quote it.");
                }
                else if (objName.matches(".*[^$_0-9A-Z].*"))//lower letter get from oracle must quoted
                {
                    objName = "\"" + objName + "\"";
                    logger.info(objName
                            + " contains character expect A-Z, 0-9, _ and $,"
                            + " HG not allowed, so quote it.");
                }
                break;
            case ORACLE:
                if (String.valueOf(objName.charAt(0)).matches("[^A-Z]"))
                {
                    objName = "\"" + objName + "\"";
                    logger.info(objName + " not start with upper letter, "
                            + "which Oracle not allowed, so quote it.");
                }
                else if (objName.matches(".*[^$_#0-9A-Z].*"))//lower letter must quoted
                {
                    objName = "\"" + objName + "\"";
                    logger.info(objName
                            + " contains character expect A-Z, 0-9, _ and $,"
                            + " which Oracle not allowed, so quote it.");
                }
                break;
            case SQLSERVER:
                break;
        }
        return objName;
    }

    //ocheck column name
    private String checkQuoteColName(String objName, ObjEnum.DBType dbType)
    {
        //check whether the column name is key word
        KeyWordFactory kwf = KeyWordFactory.getInstance();
        List<String> colNameKeyWordList = kwf.getColNameKeyWords(dbType);
        int size = colNameKeyWordList.size();
        for (int i = 0; i < size; i++)
        {
            String patternString = colNameKeyWordList.get(i).toUpperCase();
            if (objName.toUpperCase().equals(patternString))//or matches()
            {
                objName = "\"" + objName + "\"";
                logger.info(objName + " isColumnNameKeyWord ");
                return objName;
            }
        }
        //check whether identifier need to be  quoted
        objName = checkQuoteIdentifer(objName, dbType);
        return objName;
    }

    private void executeUpdate(PrintWriter pwError, DBConnInfoDTO hgDBInfo, String updateSQL) throws Exception
    {
        //logger.info("Enter:SQL = " + updateSQL);
        Connection conn = null;
        PreparedStatement preparedStmt = null;
        try
        {
            conn = ConnectHelper.getConnection(hgDBInfo);
            logger.info("Connected");
            preparedStmt = conn.prepareStatement(updateSQL);
            preparedStmt.executeUpdate();
        }
        catch (Exception ex)
        {
            //writeLog(pwError, new Date() + " Error: " + updateSQL + "\n\r" + ex.getMessage());
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
        finally
        {
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            //logger.info("Close Resource");
        }
    }

    //for procedure, function and trigger
    private String writeObjScript(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO sourceDBInfo, PrintWriter pw,
            ObjInfoDTO scriptObjInfo)
    {
        logger.info("Enter:Type=" + scriptObjInfo.getType() + ",schema="
                + scriptObjInfo.getSchema() + ",objName=" + scriptObjInfo.getName());
        String isSuccess = "Successed";
        Connection conn = null;
        PreparedStatement preparedStmt = null;
        ResultSet rtset = null;
        SQLFactory sqlf = SQLFactory.getInstance();
        String sql = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.SCRIPT_OBJ);
        StringBuilder sbCreateSql = new StringBuilder();
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, scriptObjInfo.getType().toString());
            preparedStmt.setString(2, scriptObjInfo.getName());
            preparedStmt.setString(3, scriptObjInfo.getSchema());
            rtset = preparedStmt.executeQuery();
            sbCreateSql.append("CREATE OR REPLACE ");
            while (rtset.next())
            {
                sbCreateSql.append(rtset.getString(1));
            }
            String title = time.format(new Date()) + "--" + scriptObjInfo.getType().toString() + "--"
                    + scriptObjInfo.getSchema() + "." + scriptObjInfo.getName();
            writeLog(pw, title);
            writeLog(pw, sbCreateSql.toString());

            preparedStmt.clearParameters();
            preparedStmt.clearBatch();
        }
        catch (Exception ex)
        {
            isSuccess = "Failed";
            writeLog(pwError, time.format(new Date()) + " Error Write Script:"
                    + sbCreateSql.toString() + "\n" + ex.getMessage());
            writeLog(pwResult, "<br/> Failed write " + scriptObjInfo.getType() + " script: "
                    + scriptObjInfo.getSchema() + "." + scriptObjInfo.getName());
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        logger.info(isSuccess);
        return isSuccess;
    }

    //create schema before create any other object
    private String createSchema(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO hgDBInfo, String schema)
    {
        String isSuccess = "Successed";
        Connection conn = null;
        String sql = null;
        PreparedStatement preparedStmt = null;
        try
        {
            conn = ConnectHelper.getConnection(hgDBInfo);
            logger.info("Connected");
            schema = checkQuoteObjName(schema, ObjEnum.DBType.HGDB);
            sql = "CREATE SCHEMA " + schema;
            preparedStmt = conn.prepareStatement(sql);
            preparedStmt.executeUpdate();
            //logger.info("Created Schema = " + schema);
        }
        catch (Exception ex)
        {
            isSuccess = "Failed";
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
            writeLog(pwError, time.format(new Date()) + " Error execute: " + sql + "\n" + ex.getMessage());
            writeLog(pwResult, "<br/> Failed create schmea: " + schema);
        }
        finally
        {
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return isSuccess;
    }

    private String createIndex(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            String schema, String index)
    {
        logger.info("GetIndexInfo = " + index);
        String isSuccess = "Successed";
        Connection conn = null;
        PreparedStatement preparedStmt = null;
        ResultSet rtset = null;
        String indexSQL = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            SQLFactory sqlf = SQLFactory.getInstance();
            String sql = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.INDEX);
            preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, schema);
            preparedStmt.setString(2, index);
            rtset = preparedStmt.executeQuery();

            boolean isFirst = true;
            while (rtset.next())
            {
                if (isFirst == true)
                {
                    indexSQL = "CREATE ";
                    if (rtset.getString(1).toUpperCase().equals("UNIQUE"))
                    {
                        indexSQL = indexSQL + "UNIQUE ";
                    }
                    schema = checkQuoteObjName(rtset.getString(2), ObjEnum.DBType.HGDB);
                    index = checkQuoteObjName(rtset.getString(3), ObjEnum.DBType.HGDB);

                    //this is a problem
                    String usingMethod = null;
                    if ("NORMAL".equals(rtset.getString(4).toUpperCase()))
                    {
                        usingMethod = "BTREE";//index_type decide index using method
                    }
                    String table = rtset.getString(5);
                    indexSQL = indexSQL + "INDEX " + index + " ON "
                            + schema + "." + table
                            + " USING " + usingMethod + " (";

                    String colname = rtset.getString(6);
                    colname = checkQuoteColName(colname, ObjEnum.DBType.HGDB);

                    String colandType = colname + " " + rtset.getString(7);
                    indexSQL = indexSQL + colandType;

                    isFirst = false;
                }
                String colname = rtset.getString(6);
                colname = checkQuoteColName(colname, ObjEnum.DBType.HGDB);
                String colandType = colname + " " + rtset.getString(7);

                indexSQL = indexSQL + "," + colandType;
            }
            indexSQL = indexSQL + ")";
            preparedStmt.clearBatch();
            preparedStmt.clearParameters();

            executeUpdate(pwError, hgDBInfo, indexSQL);
        }
        catch (Exception ex)
        {
            isSuccess = "Failed";
            writeLog(pwError, time.format(new Date()) + " Error Create Index:" + indexSQL + "\n" + ex.getMessage());
            writeLog(pwResult, "<br/> Failed create index: " + schema + "." + index);
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return isSuccess;
    }

    private String createSequence(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            String schema, String sequence)
    {
        logger.info("GetSequenceInfo = " + sequence);
        String isSuccess = "Successed";
        Connection conn = null;
        PreparedStatement preparedStmt = null;
        ResultSet rtset = null;
        SQLFactory sqlf = SQLFactory.getInstance();
        String sql = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.SEQUENCE);
        BigDecimal limitValue = BigDecimal.valueOf(2).pow(63).subtract(BigDecimal.valueOf(1));
        String seqSQL = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, schema);
            preparedStmt.setString(2, sequence);
            rtset = preparedStmt.executeQuery();
            //only one line at most
            while (rtset.next())
            {
                String schemaName = rtset.getString(1);
                schemaName = checkQuoteObjName(schemaName, ObjEnum.DBType.HGDB);
                String seqName = rtset.getString(2);
                seqName = checkQuoteObjName(seqName, ObjEnum.DBType.HGDB);

                String start = rtset.getString(3);
                String minValue = rtset.getString(4);

                BigDecimal maxValue = rtset.getBigDecimal(5);
                if (maxValue.compareTo(limitValue) == 1)
                {
                    maxValue = limitValue;
                }
                logger.info("maxValue = " + maxValue.toString());

                String increment = rtset.getString(6);
                String cache = rtset.getString(7);
                if (cache.equals("0"))
                {
                    cache = "1";
                }
                seqSQL = "CREATE SEQUENCE " + schemaName + "." + seqName
                        + " START " + start
                        + " MINVALUE " + minValue
                        + " MAXVALUE " + maxValue
                        + " INCREMENT " + increment
                        + " CACHE " + cache
                        + " ;";
            }
            preparedStmt.clearBatch();
            preparedStmt.clearParameters();

            executeUpdate(pwError, hgDBInfo, seqSQL);
        }
        catch (Exception ex)
        {
            isSuccess = "Failed";
            writeLog(pwError, time.format(new Date()) + " Error Create Sequence:"
                    + seqSQL + "\n" + ex.getMessage());
            writeLog(pwResult, "<br/> Failed create sequence: " + schema + "." + sequence);
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }

        return isSuccess;
    }

    private String createView(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            String schema, String view)
    {
        logger.info("GetViewInfo = " + view);
        String isSuccess = "Successed";
        String viewSQL = null;
        Connection conn = null;
        PreparedStatement preparedStmt = null;
        ResultSet rtset = null;
        SQLFactory sqlf = SQLFactory.getInstance();
        String sql = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.VIEW);
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            preparedStmt = conn.prepareStatement(sql);
            preparedStmt.setString(1, schema);
            preparedStmt.setString(2, view);
            rtset = preparedStmt.executeQuery();
            while (rtset.next())
            {
                String schemaName = rtset.getString(1);
                schemaName = checkQuoteObjName(schemaName, ObjEnum.DBType.HGDB);
                String viewName = rtset.getString(2);
                viewName = checkQuoteObjName(viewName, ObjEnum.DBType.HGDB);

                String query = rtset.getString(3).toLowerCase();
                viewSQL = "CREATE OR REPLACE VIEW "
                        + schemaName + "." + viewName
                        + " AS " + query + " ;";
            }
            preparedStmt.clearBatch();
            preparedStmt.clearParameters();

            executeUpdate(pwError, hgDBInfo, viewSQL);
        }
        catch (Exception ex)
        {
            isSuccess = "Failed";
            writeLog(pwError, time.format(new Date()) + " Error Create View:"
                    + viewSQL + "\n" + ex.getMessage());
            writeLog(pwResult, "<br/> Failed create view: " + schema + "." + view);
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }

        return isSuccess;
    }

    private String migrateTable(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            List<DataTypeCastDTO> dataTypeCastList, List<FKConstrInfoDTO> fkSqlList,
            String schema, String table,String migrateModel, int copyBatch, int insertBatch
            , String delimiter, String quoteChar)
    {
        logger.info("Enter: table=" + schema + "." + table + ",copyBatch=" + copyBatch + ",insertBatch=" + insertBatch);
        String isSuccess = "Successed";
        StringBuilder tabSelectSQL = new StringBuilder();
        StringBuilder tabCreateSQL = new StringBuilder();
        Connection conn = null;
        PreparedStatement ppstt = null;
        ResultSet rtset = null;
        SQLFactory sqlf = SQLFactory.getInstance();
        String sql = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.COLUMN);
        String schema4create = schema;
        String table4create = table;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");

            ppstt = conn.prepareStatement(sql);
            ppstt.setString(1, schema);
            ppstt.setString(2, table);
            rtset = ppstt.executeQuery();
            //make table create and select sql.
            schema4create = checkQuoteObjName(schema, ObjEnum.DBType.HGDB);
            table4create = checkQuoteObjName(table, ObjEnum.DBType.HGDB);
            makeTabSQL(rtset, tabCreateSQL, tabSelectSQL, sourceDBInfo, dataTypeCastList,
                    schema4create, table4create, schema, table);
            
            ppstt.clearBatch();
            ppstt.clearParameters();

            //create table
            if (!migrateModel.equals(constBundle.getString("onlyData")))
            {               
                if (!isStop)
                {                
                    logger.info("TabCreateSQL="+tabCreateSQL.toString());
                    executeUpdate(pwError, hgDBInfo, tabCreateSQL.toString());
                }
            }
        }
        catch (Exception ex)
        {
            isSuccess = "Failed";
            writeLog(pwError, time.format(new Date()) + " Error Create Table:"
                    + tabCreateSQL.toString() + "\n" + ex.getMessage());
            writeLog(pwResult, "<br/> Failed create table: " + schema + "." + table);
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(ppstt);
            JdbcHelper.close(conn);
            logger.info("Close Source");
        }

        //migrate data
        if (!migrateModel.equals(constBundle.getString("onlyStru")))
        {
            //try
            //{
            if (migrateModel.equals(constBundle.getString("onlyData")))
            {
                //disable Fkey, sql - ALTER TABLE schema.table DISABLE TRIGGER ALL, how about system trigger ?
                String disableFK = " ALTER TABLE " + schema4create + "." + table4create + " DISABLE TRIGGER ALL";
                if (!isStop)
                {
                    try
                    {
                        executeUpdate(pwError, hgDBInfo, disableFK);
                    } 
                    catch (Exception ex)
                    {
                        writeLog(pwError, time.format(new Date()) + " Error Disable Constrainst:"
                                + disableFK + "\n" + ex.getMessage());
                        //logger.error("Error:" + ex.getMessage());
                        //ex.printStackTrace(System.out);
                    }
                }
            }
            
            if (!isStop)
            {
                //copy switch insert
                if (!copyData2HG(copyBatch, insertBatch, pwError, sourceDBInfo, hgDBInfo, tabSelectSQL.toString(), schema, table,
                        dataTypeCastList,delimiter, quoteChar, migrateModel))
                {
                    isSuccess = "Failed";
                    writeLog(pwResult, "<br/> Table data migration got some problem(check Error.sql for detail) of table: " + schema + "." + table);
                    //writeLog(pwError, "\n" + time.format(new Date()) + ex.getMessage());
                }
            }
            if (migrateModel.equals(constBundle.getString("onlyData")))
            {
                //how about system trigger ?
                String disableFK = " ALTER TABLE " + schema4create + "." + table4create + " ENABLE TRIGGER ALL";
                try
                {
                    executeUpdate(pwError, hgDBInfo, disableFK);
                } catch (Exception ex)
                {
                    writeLog(pwError, time.format(new Date()) + " Error Disable Constrainst:"
                            + disableFK + "\n" + ex.getMessage());
                    //logger.error("Error:" + ex.getMessage());
                    //ex.printStackTrace(System.out);
                }
            }
        }

        //add constraint which not fk,and get fk constraint sql
        if (!migrateModel.equals(constBundle.getString("onlyData")))
        {
            try
            {
                migrateConstraint(pwError, pwResult, sourceDBInfo, hgDBInfo, fkSqlList, schema, table);
            }
            catch (Exception ex)
            {
                isSuccess = "Failed";
                writeLog(pwResult, "<br/> Failed add unFKey Constraints: " + schema + "." + table);
            }
        }
        return isSuccess;
    }
    
    //make table create and select sql.
    private void makeTabSQL(ResultSet rtset, StringBuilder tabCreateSQL, StringBuilder tabSelectSQL, DBConnInfoDTO sourceDBInfo, List<DataTypeCastDTO> dataTypeCastList,
            String schema4create, String table4create, String schema, String table) throws Exception
    {    
        tabCreateSQL.append("CREATE TABLE ").append(schema4create).append(".")
                .append(table4create).append("(");
        tabSelectSQL.append("SELECT ");

        boolean isFirst = true;
        while (rtset.next())
        {
            //String schemaName = rtset.getString(1);
            //String tabName = rtset.getString(2);
            String cloName = rtset.getString(3);
            String dataType = rtset.getString(4);
            String dataDefault = rtset.getString(5);
            String dataPrecision = rtset.getString(6);
            String dataScale = rtset.getString(7);
            int dataLength = rtset.getInt(8);
            boolean nullable = true;
            if (rtset.getString(9).equals("N"))
            {
                nullable = false;
            }

            if (isFirst)
            {
                isFirst = false;
            } else
            {
                tabCreateSQL.append(",");
                tabSelectSQL.append(",");
            }

            //for tabCreateSQL
            String colName4Create = checkQuoteColName(cloName, ObjEnum.DBType.HGDB);
            dataType = getHgTypeWithParameter(sourceDBInfo.getDBType(), dataTypeCastList,
                    schema, table, cloName,
                    dataType, dataPrecision, dataScale, dataLength);
            tabCreateSQL.append(colName4Create).append(" ").append(dataType);
            if (dataDefault != null)
            {
                tabCreateSQL.append(" DEFAULT ").append(dataDefault);
            }
            if (!nullable)
            {
                tabCreateSQL.append(" NOT NULL");
            }

            //for tabSelectSQL
            String colName4Select = checkQuoteColName(cloName, sourceDBInfo.getDBType());
            tabSelectSQL.append(colName4Select);
        }
        //for tabCreateSQL
        tabCreateSQL.append(");");

        //for tabSelectSQL
        String schema4select = schema;
        schema4select = checkQuoteObjName(schema4select, sourceDBInfo.getDBType());
        String table4select = table;
        table4select = checkQuoteObjName(table4select, sourceDBInfo.getDBType());
        tabSelectSQL.append(" FROM ").append(schema4select).append(".").append(table4select);//oracle select statement couldn't have ';' 
        // + " ORDER BY ROWID";
        //omit OREDER BY ROWID,because Lu Jian said oracle select default order by rowid,
        // add adding ORDER BY ROWID will lower select speed  
    }   
    //INTERVAL YEAR TO MONTH
    private String checkIntervalYM(ObjEnum.DBType sourceDBType, String initalValue)
    {
        logger.info("Enter：" + initalValue);
        String correctValue = initalValue;
        switch (sourceDBType)
        {
            case ORACLE:
                if (initalValue.startsWith("-"))
                {
                    String[] s = initalValue.split("[-]", 3);
                    correctValue = "-" + s[1] + " year -" + s[2] + " month";
                }
                else //for those with + or without any symbol
                {
                    String[] s = initalValue.split("[-]", 2);
                    correctValue = s[0] + " year +" + s[1] + " month";
                }
                break;
            case SQLSERVER:
                break;
        }
        logger.info("Return:" + correctValue);
        return correctValue;
    }
    //INTERVAL DAY TO SECOND
    private String checkIntervalDS(ObjEnum.DBType sourceDBType, String initalValue)
    {
        logger.info("Enter：" + initalValue);
        String correctValue = initalValue;
        switch (sourceDBType)
        {
            case ORACLE:
                String symbol;
                if (initalValue.startsWith("-"))
                {
                    symbol = "-";
                }
                else //for those with + or without any symbol
                {
                    symbol = "+";
                }
                String[] s = initalValue.split("\\s", 2);
                String[] ss = s[1].split(":", 3);
                correctValue = s[0] + " days "
                        + symbol + ss[0] + " hours "
                        + symbol + ss[1] + " mins "
                        + symbol + ss[2] + " seconds ";
                break;
            case SQLSERVER:
                break;
        }
        logger.info("Return:" + correctValue);
        return correctValue;
    }

    private boolean copyData2HG(int copyBatch, int insertBatch, PrintWriter pwError, DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            String tabSelectSQL, String schema, String table, List<DataTypeCastDTO> dataTypeCastList
            , String delimiter, String quoteChar, String migrateModel)
    {
        logger.info("Enter:" + schema + "." + table);
        int errorFlag = 0;
        Connection conn4Select = null;
        Statement stmt = null;
        ResultSet rtset = null;
        Connection connForInsert = null;
        String insertSQL = null;
        boolean hasWarn = false;
        
        int colCount = 0;
        try
        {
            conn4Select = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected Oracle");
            stmt = conn4Select.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            logger.info("CurrentFetchSize=" + stmt.getFetchSize());
            
            logger.info("tabSelectSQL = " + tabSelectSQL);
            rtset = stmt.executeQuery(tabSelectSQL);
            logger.info("Got Data");

            connForInsert = ConnectHelper.getConnection(hgDBInfo);
            logger.info("Connected HG");
            if(rtset==null)
            {
                return true;
            }
            ResultSetMetaData metaData = rtset.getMetaData();
            colCount = metaData.getColumnCount();
            logger.info(migrateModel);
            if (hasBinaryType(metaData, colCount, dataTypeCastList)
                    || migrateModel.equals(constBundle.getString("onlyInsert"))) 
            {
                logger.info("hasBinaryType");
                insertSQL = appendTabInsertSQL(rtset, colCount, schema, table);
                //rtset never change here
                if(migrateAllBeginWithInsertBatchData(pwError, hgDBInfo,
                        insertSQL, rtset, colCount, dataTypeCastList, metaData,
                        insertBatch, conn4Select, tabSelectSQL))
                {
                    return true;
                }else
                {
                    return false;
                }
            }
            CopyManager copyManager = new CopyManager((BaseConnection) connForInsert);
            String tabCopySQL = "COPY " + checkQuoteObjName(schema, ObjEnum.DBType.HGDB) + "."
                    + checkQuoteObjName(table, ObjEnum.DBType.HGDB) + " FROM STDIN WITH  delimiter as '"+delimiter
                    +"' quote as '"+quoteChar+"' CSV";
            logger.info("CopyCommand: " + tabCopySQL);
            StringBuilder strb = new StringBuilder();
            int row = 0;   
            StringReader strReader = null;
            BufferedReader reader = null;
            while (rtset.next())
            {
                if (isStop)
                {
                    logger.info("Migration Assistant is stoped");
                    return false;
                }
                row++;
                
                if (appandDataStrb(strb, colCount, rtset, metaData, delimiter, quoteChar))
                {
                    hasWarn = true;                    
                }
                
                if (row % copyBatch == 0)
                {
                    if(!copyIn(strb, strReader, reader, copyManager, tabCopySQL, pwError, hasWarn, schema, table))
                    {
                        if (insertSQL == null)
                        {
                            insertSQL = appendTabInsertSQL(rtset, colCount, schema, table);
                            logger.info("LastInsertRow=" + row + " ,NormalBatch=" + copyBatch);
                        }
                        if (!getBatchDataToBatchInsert(conn4Select, tabSelectSQL, row, copyBatch,
                                insertBatch, pwError, hgDBInfo, insertSQL, colCount, dataTypeCastList, metaData))
                        {
                            errorFlag++;
                        }
                    }
                }
            }
            
            if (!copyIn(strb, strReader, reader, copyManager, tabCopySQL, pwError, hasWarn, schema, table))
            {
                if (insertSQL == null)
                {
                    insertSQL = appendTabInsertSQL(rtset, colCount, schema, table);
                    logger.info("LastInsertRow=" + row + " ,NormalBatch=" + copyBatch);
                }
                int lastBatch = row % copyBatch;
                logger.info("LastRow=" + row + ", LastBatch=" + lastBatch);
                if (!getBatchDataToBatchInsert(conn4Select, tabSelectSQL, row, lastBatch,
                        insertBatch, pwError, hgDBInfo, insertSQL, colCount, dataTypeCastList, metaData))
                {
                    errorFlag++;
                }
            }
        } catch (Exception ex)
        {
            errorFlag++;
            writeLog(pwError, time.format(new Date()) + ex.getMessage());
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            //throw new Exception(ex);
        }finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn4Select);
            JdbcHelper.close(connForInsert);
            logger.info("Close Resource");
        }
        
        logger.info("errorFlag=" + errorFlag);
        if (errorFlag == 0)
        {
            return true;
        } else
        {
            return false;
        }
    }
     
    private boolean copyIn(StringBuilder strb, StringReader strReader, BufferedReader reader, CopyManager copyManager,
            String tabCopySQL, PrintWriter pwError, boolean hasWarn, String schema, String table)
    {
        boolean isSuccess = true;
        try
        {
            strReader = new StringReader(strb.toString());
            reader = new BufferedReader(strReader);
            copyManager.copyIn(tabCopySQL, reader);
        } catch (Exception ex)
        {
            strb.delete(0, strb.length());
            isSuccess = false;

            writeLog(pwError, "\n" + time.format(new Date()) + "Warn: Couldn't use Copy to migrate this copyBatch of table data"
                    + ", then use Batch Insert to migrate data of " + schema + "." + table + ". \n Copy Warning:" + ex.getMessage());
            logger.warn("Copy Data Warning:" + ex.getMessage());
            ex.printStackTrace(System.out);
            if (hasWarn)
            {
                StringBuilder strbWarn = new StringBuilder();
                strbWarn.append("Warn: table [").append(schema).append(".").append(table)
                        .append("], with CLOB/NCLOB/LONG datatype field value more than 1023byte.")
                        .append(", if Failed migrate Data, maybe you need to change datatype cast to [CLOB/NCLOB/LONG to BYTEA/OID] and migrate this table and data again.");
                writeLog(pwError, "\n" + time.format(new Date()) + strbWarn.toString());
                logger.warn(strbWarn.toString());
            }
        } finally
        {
            strb.delete(0, strb.length());
            CloseStream.close(reader);
            CloseStream.close(strReader);
            //logger.info("Close Stream");
        }
        return isSuccess;
    }

    //maybe use hgType better
    private boolean hasBinaryType(ResultSetMetaData  metaData,int colCount, List<DataTypeCastDTO> dataTypeCastList) throws SQLException
    {
        for (int i = 1; i <= colCount; i++)
        {
            String type = metaData.getColumnTypeName(i);
            switch (type)
            {
                case "BFILE":
                case "BLOB":
                case "LONG RAW":
                case "RAW":
                    logger.warn("Table has " + type + " column, directly use insert.");
                    return true;
                case "LONG":
                case "CLOB":
                case "NCLOB":
                    String hgType = getHgTypeWithoutParameter(ObjEnum.DBType.ORACLE, type, dataTypeCastList);
                    switch (hgType)
                    {
                        case "BYTEA":
                        case "OID":
                            logger.warn("Table has " + type + " cast to " + hgType + ", directly use insert.");
                            return true;
                        default:
                            break;
                    }
                default:
                    break;
            }
        }        
        return false;
    }
    
    private boolean appandDataStrb(StringBuilder strb, int colCount, ResultSet rtset,ResultSetMetaData metaData
       , String delimiter, String quoteChar) throws Exception
    {
        boolean hasWarn = false;
        for (int k = 1; k <= colCount; k++)
        {
            String type = metaData.getColumnTypeName(k);
            //logger.info("type = " + type);
            switch(type)
            {
                case "CLOB":
                    Clob clob = rtset.getClob(k);
                    if (clob == null)
                    {
                        strb.append("");
                    } 
                    else if (clob.length() < 1024)
                    {                        
                        strb.append(quoteChar).append(clob.getSubString(1L, (int) clob.length())).append(quoteChar);
                    } else
                    {
                        //logger.warn("colName=" + metaData.getColumnName(k) + ",colType=" + type + ",valueLength=" + clob.length());
                        strb.append(quoteChar).append(clob.getSubString(1L, (int) clob.length())).append(quoteChar);
                        hasWarn = true;
                    }                   
                    break;
                case "NCLOB":
                    NClob nClob = rtset.getNClob(k);
                    if (nClob == null)
                    {
                        strb.append("");
                    }
                    else if (nClob.length() < 1024)
                    {
                        strb.append(quoteChar).append(nClob.getSubString(1L, (int) nClob.length())).append(quoteChar);
                    } else
                    {         
                       //logger.warn("colName="+metaData.getColumnName(k)+",colType="+type+",valueLength="+nClob.length());
                       strb.append(quoteChar).append(nClob.getSubString(1L, (int) nClob.length())).append(quoteChar);
                       hasWarn = true;
                    }                    
                    break;
                 case "LONG":
                    String strl = rtset.getString(k);
                    if (strl == null)
                    {
                        strb.append("");
                    } else if (strl.length() < 1024)
                    {
                        strb.append(quoteChar).append(strl).append(quoteChar);
                    } else
                    {
                        //logger.warn("colName="+metaData.getColumnName(k)+",colType="+type+",valueLength="+nClob.length());
                        strb.append(quoteChar).append(strl).append(quoteChar);
                        hasWarn = true;
                    }
                    break;
                default://other type could get correct values by getString()
                    String str = rtset.getString(k);
                    if (str == null)
                    {
                        strb.append("");
                    } 
                    else
                    {
                        switch(type)
                        {
                            case "INTERVALYM":
                                str = checkIntervalYM(ObjEnum.DBType.ORACLE, str);
                                break;
                            case "INTERVALDS":
                                str = checkIntervalDS(ObjEnum.DBType.ORACLE, str);
                                break;
                            case "TIMESTAMP":
                                if(str.startsWith("-"))
                                {
                                    str = str.substring(1) + " BC";
                                }  
                                break;      
                            default:
                                break;
                        }
                        strb.append(quoteChar).append(str).append(quoteChar);
                    }
                    break;                    
            }
            if (k < colCount)
            {
                strb.append(delimiter);
            }
        }
        strb.append("\r\n");
        
        return hasWarn;
    }
    
    private String appendTabInsertSQL(ResultSet rtset, int colCount, String schema, String table) throws SQLException
    {
        StringBuilder insertSQL = new StringBuilder();
        insertSQL.append("INSERT INTO ").append(schema + ".").append(table)
                .append("(").append(checkQuoteColName(rtset.getMetaData().getColumnName(1), ObjEnum.DBType.HGDB));
        for (int k = 2; k <= colCount; k++)
        {
            insertSQL.append(",").append(checkQuoteColName(rtset.getMetaData().getColumnName(k), ObjEnum.DBType.HGDB));
        }
        insertSQL.append(") values(?");
        for (int k = 2; k <= colCount; k++)
        {
            insertSQL.append(",?");
        }
        insertSQL.append(");");
        
        logger.info("insertSQL = "+insertSQL.toString());
        return insertSQL.toString();
    }
      
    //migrate data for table has binary
    private boolean migrateAllBeginWithInsertBatchData(PrintWriter pwError, DBConnInfoDTO hgDBInfo,
            String insertSQL, ResultSet rtset, int colCount,
            List<DataTypeCastDTO> dataTypeCastList,ResultSetMetaData metaData,
            int insertBatch,Connection conn4Select,String tabSelectSQL)
    {
        logger.info("Enter");
        int errorFlag = 0;
        Connection conn = null;
        PreparedStatement ppstmt = null;
        try
        {
            conn = ConnectHelper.getConnection(hgDBInfo);
            logger.info("Connected HG");
            conn.setAutoCommit(false);
            ppstmt = conn.prepareStatement(insertSQL);
            int row = 0;
            while (rtset.next())
            {
                if (isStop)
                {
                    break;
                }
                for (int k = 1; k <= colCount; k++)
                {                    
                    setParameter(k,rtset, ppstmt,metaData, conn, dataTypeCastList);
                }
                ppstmt.addBatch();
                row++;
                
                if (row % insertBatch == 0)
                {
                    if(!executeBatch(ppstmt, conn, pwError))
                    {
                        if(!getBatchDataToPerRowInsert(conn4Select,tabSelectSQL,  row, insertBatch,
                                pwError, colCount, conn, ppstmt, metaData, dataTypeCastList))
                        {
                            errorFlag++;
                        }
                        conn.setAutoCommit(false);
                    }
                }
            }
            if (!executeBatch(ppstmt, conn, pwError))
            {
                int lastBatch = row % insertBatch;
                if (!getBatchDataToPerRowInsert(conn4Select, tabSelectSQL, row, lastBatch,
                        pwError, colCount, conn, ppstmt, metaData, dataTypeCastList))
                {
                    errorFlag++;
                }
            }
            conn.setAutoCommit(true);
        } 
        catch (Exception ex)
        {
            errorFlag++;
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                writeLog(pwError, time.format(new Date()) + " Error Batch Insert Data To Table:"
                        + bex.getMessage());
                logger.error(bex.getMessage());
                bex.getNextException().printStackTrace(System.out);
            } else
            {
                writeLog(pwError, time.format(new Date()) + " Error Batch Insert Data To Table:"
                        + ex.getMessage());
                logger.error("Error:" + ex.getMessage());
                ex.printStackTrace(System.out);
            }
        } finally
        {
            JdbcHelper.close(ppstmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        
        logger.info("errorFlag = "+errorFlag);                
        if(errorFlag==0)
        {
            return true;
        }else
        {
            return false;
        }
    }    
    
    private boolean executeBatch(PreparedStatement ppstmt,Connection conn,PrintWriter pwError) throws SQLException
    {
        boolean isSuccess = true;
        try
        {
            ppstmt.executeBatch();                        
        } catch (Exception ex)
        {
            isSuccess = false;

            writeLog(pwError, time.format(new Date()) + " Couldn't Batch Insert this copyBatch of  Data, then insert one by one:"
                    + ex.getMessage());
            logger.warn("Batch Insert Warning:" + ex.getMessage());
            ex.printStackTrace(System.out);
        } finally
        {
            conn.commit();
            ppstmt.clearBatch();
        }
        return isSuccess;
    }
    
   //below is migrate data for normal table
    private boolean getBatchDataToBatchInsert(Connection conn4Select, String tabSelectSQL, int endRow, int thisBatch,
            int insertBatch, PrintWriter pwError, DBConnInfoDTO hgDBInfo,
            String insertSQL, int colCount, List<DataTypeCastDTO> dataTypeCastList, ResultSetMetaData metaData)
    {
        logger.info("Enter:SelectBatch =(" + (endRow - thisBatch) + "," + endRow + "]");
        boolean isSuccess = true;
        PreparedStatement ppstmt = null;
        ResultSet rtset = null;
        try
        {
            ppstmt = conn4Select.prepareStatement(tabSelectSQL+" WHERE ROWNUM <="+endRow);
            logger.info("CurrentFetchSize=" + ppstmt.getFetchSize());
            rtset = ppstmt.executeQuery();
            int absoluteRow = endRow - thisBatch;
            if (absoluteRow > 0)
            {
               while(rtset.next())
               {
                   if(rtset.getRow()==absoluteRow)
                   {
                       break;
                   }
               }
               logger.info("absoluteRowTo:"+rtset.getRow());
            }

            isSuccess = insertBatchData(pwError, hgDBInfo,endRow,
                    insertSQL, rtset, colCount, dataTypeCastList, metaData,
                    insertBatch, conn4Select, tabSelectSQL, endRow - thisBatch);
        } catch (Exception ex)
        {
            isSuccess = false;
            writeLog(pwError, time.format(new Date()) + " Error getBatchDataToBatchInsert:" + "\n" + ex.getMessage());
            logger.error("Error getBatchDataToBatchInsert:" + "\n Caused by:" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(ppstmt);
            logger.info("Close Resource");
        }
        return isSuccess;
    }
    
    private boolean insertBatchData(PrintWriter pwError, DBConnInfoDTO hgDBInfo, int endRow,
            String insertSQL, ResultSet rtset, int colCount,
            List<DataTypeCastDTO> dataTypeCastList,ResultSetMetaData metaData,
            int insertBatch,Connection conn4Select,String tabSelectSQL,int startRow)
    {
        logger.info("Enter");
        int errorFlag = 0;
        Connection conn = null;
        PreparedStatement ppstmt = null;
        try
        {
            conn = ConnectHelper.getConnection(hgDBInfo);
            logger.info("Connected HG");
            conn.setAutoCommit(false);
            ppstmt = conn.prepareStatement(insertSQL);
            int start = rtset.getRow();
            int row = 0;
            while (rtset.next())
            {
                if (isStop)
                {
                    break;
                }
                for (int k = 1; k <= colCount; k++)
                {                    
                    setParameter(k,rtset, ppstmt,metaData, conn, dataTypeCastList);
                }
                ppstmt.addBatch();
                row++;
                
                if (row % insertBatch == 0)
                {
                    if (!executeBatch(ppstmt, conn, pwError))
                    {
                        if (!getBatchDataToPerRowInsert(conn4Select, tabSelectSQL, startRow + row, insertBatch,
                                pwError, colCount, conn, ppstmt, metaData, dataTypeCastList))
                        {
                            errorFlag++;
                        }
                        conn.setAutoCommit(false);
                    }
                }
            }

            if (!executeBatch(ppstmt, conn, pwError))
            {
                int lastBatch = row % insertBatch;
                if (!getBatchDataToPerRowInsert(conn4Select, tabSelectSQL, startRow + row, lastBatch,
                        pwError, colCount, conn, ppstmt, metaData, dataTypeCastList))
                {
                    errorFlag++;
                }                
            }
            logger.info("EndedRow=" + (start + row));
           
            conn.setAutoCommit(true);
        } catch (Exception ex)
        {
            errorFlag++;
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                writeLog(pwError, time.format(new Date()) + " Error Batch Insert Data To Table:"
                        + bex.getMessage());
                logger.error(bex.getMessage());
                bex.getNextException().printStackTrace(System.out);
            } else
            {
                writeLog(pwError, time.format(new Date()) + " Error Batch Insert Data To Table:"
                        + ex.getMessage());
                logger.error("Error:" + ex.getMessage());
                ex.printStackTrace(System.out);
            }
        } finally
        {
            JdbcHelper.close(ppstmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        logger.info("errorFlag=" + errorFlag);
        if (errorFlag == 0)
        {
            return true;
        } else
        {
            return false;
        }
    }    

    private boolean getBatchDataToPerRowInsert(Connection conn4Select,String tabSelectSQL, int endRow, int thisBatch,
            PrintWriter pwError, int colCount, Connection conn4Insert, PreparedStatement ppstmt4Insert,
            ResultSetMetaData metaData, List<DataTypeCastDTO> dataTypeCastList)
    {
        logger.info("Enter:batch =(" + (endRow - thisBatch) + "," + endRow + "]");
        boolean isSuccess = true;
        PreparedStatement ppstmt =null;
        ResultSet rtset = null;
        try
        {                      
            ppstmt = conn4Select.prepareStatement(tabSelectSQL+" WHERE ROWNUM <="+endRow);
            logger.info("CurrentFetchSize=" + ppstmt.getFetchSize());
            rtset = ppstmt.executeQuery();
            int absoluteRow = endRow - thisBatch;
            if (absoluteRow > 0)
            {
                while (rtset.next())
                {
                    if (rtset.getRow() == absoluteRow)
                    {
                        break;
                    }
                }
                logger.info("absoluteRowTo:" + rtset.getRow());
            }  
            
            conn4Insert.setAutoCommit(false);  //OID type need to set autoCommit to false
            isSuccess = insertData(pwError, rtset,endRow, conn4Insert, ppstmt4Insert, metaData,
                    colCount, dataTypeCastList);
            //conn4Insert.setAutoCommit(true); 
        }
        catch (Exception ex)
        {
            isSuccess = false;
            writeLog(pwError, time.format(new Date()) + " Error getBatchDataToInsert:" + "\n" + ex.getMessage());
            logger.error("Error getBatchDataToInsert:" + "\n Caused by:" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {            
            JdbcHelper.close(rtset);
            JdbcHelper.close(ppstmt);
            logger.info("Close Resource");
        }
        logger.info("isSuccess = " + isSuccess);
        return isSuccess;
    }
    
     private boolean insertData(PrintWriter pwError, ResultSet rtset, int endRow, Connection conn4Insert,
             PreparedStatement ppstmt4Insert, ResultSetMetaData metaData, int colCount,
             List<DataTypeCastDTO> dataTypeCastList)throws Exception
    {
        logger.info("Enter");
        int errorFlag = 0;
        int row = rtset.getRow();
        while (rtset.next())
        {
            if (isStop)
            {
                break;
            }   
            row++;
            try
            {
                for (int k = 1; k <= colCount; k++)
                {
                    setParameter(k, rtset, ppstmt4Insert, metaData, conn4Insert, dataTypeCastList);
                }
                ppstmt4Insert.executeUpdate();                   
            }
            catch (Exception ex)
            {
                errorFlag++;
                logger.info("ErrorRowNum = " + rtset.getRow());
                //StringBuilder rowData = new StringBuilder();
                //appandDataStrb(rowData, colCount, rtset, metaData);
                if (ex instanceof BatchUpdateException)
                {
                    BatchUpdateException bex = (BatchUpdateException) ex;
                    writeLog(pwError, time.format(new Date()) + " Error Insert This One Row:ErrorRowNum =" + rtset.getRow() //+ rowData.toString()
                            + "\n This maybe caused by Null terminator (nonvisible terminator) in SQL Server.\n getNextException:" 
                            + bex.getNextException());
                    logger.error("Error Insert One Row:" //+ rowData.toString()
                            + " \n This maybe caused by Null terminator (nonvisible terminator) in SQL Server.\n getNextException:"
                            + bex.getNextException());
                    bex.getNextException().printStackTrace(System.out);
                } 
                else
                {
                    writeLog(pwError, time.format(new Date()) + " Error Insert This One Row:ErrorRowNum =" + rtset.getRow()// + rowData.toString()
                            + "\n" + ex.getMessage());
                    logger.error("Error Insert This One Row:ErrorRowNum =" + rtset.getRow()//+ rowData.toString() 
                            + "\n Caused by:" + ex.getMessage());
                    ex.printStackTrace(System.out);
                }
            }
            finally
            {
                conn4Insert.commit();
                ppstmt4Insert.clearParameters();
            }
        }
        logger.info("EndedRow="+ row);
        
        logger.info("errorFlag = " + errorFlag);
        if(errorFlag==0)
        {
            return true;
        }else
        {
            return false;
        }
    }
      
    private void setParameter(int k,ResultSet rtset,PreparedStatement ppstmt,ResultSetMetaData metaData,
            Connection conn, List<DataTypeCastDTO> dataTypeCastList)throws Exception
    {
        String colType = metaData.getColumnTypeName(k).toUpperCase();
        String hgType = getHgTypeWithoutParameter(ObjEnum.DBType.ORACLE, colType, dataTypeCastList);
        switch(colType)
        {            
            case "BFILE":
                BFILE bfile = (BFILE) rtset.getObject(k);
                switch (hgType)
                {
                    case "BYTEA":
                        if (bfile == null)
                        {
                            ppstmt.setNull(k, Types.BINARY);
                        } else
                        {
                            File file = getFile(bfile, k);
                            logger.info("BFile Length = " + file.length());
                            ppstmt.setBinaryStream(k, new FileInputStream(file), file.length());//for bytea
                        }
                        break;
                    case "OID": //test 4G and pass.
                        if (bfile == null)
                        {
                            ppstmt.setNull(k, Types.BIGINT);
                        } else
                        {
                            File file = getFile(bfile, k);
                            logger.info("BFile Length = " + file.length());
                            conn.setAutoCommit(false);
                            Long oid = getOid(conn, file);
                            ppstmt.setLong(k, oid);
                        }
                        break;
                }
                break;
            case "BLOB":
            case "LONG RAW":
            case "RAW":
                switch (hgType)
                {
                    case "BYTEA":
                        ppstmt.setBytes(k, rtset.getBytes(k));
                        break;
                    case "OID":
                        InputStream ips = rtset.getBinaryStream(k);
                        if (ips == null)
                        {
                            ppstmt.setNull(k, Types.BIGINT);
                        } else
                        {
                            File file = this.getFile(ips, k);
                            //conn2.setAutoCommit(false);
                            Long oid = getOid(conn, file);
                            ppstmt.setLong(k, oid);
                        }
                        break;
                }
                break;
            case "CLOB":
            case "NCLOB":
                switch (hgType)
                {
                    case "TEXT":
                        ppstmt.setString(k, rtset.getString(k));
                        break;
                    case "OID":
                        Reader reader = rtset.getCharacterStream(k);
                        if (reader == null)
                        {
                            ppstmt.setNull(k, Types.BIGINT);
                        } else
                        {
                            File file = this.getFile(reader, k);
                            //conn2.setAutoCommit(false);
                            Long oid = getOid(conn, file);
                            ppstmt.setLong(k, oid);
                        }
                        break;
                }
                break;
            case "LONG":
                switch (hgType)
                {
                    case "TEXT":
                        ppstmt.setString(k, rtset.getString(k));
                        break;
                    case "BYTEA":
                        ppstmt.setBytes(k, rtset.getBytes(k));//hgdb not support setAsciiStream()
                        break;
                    case "OID":
                        Reader reader = rtset.getCharacterStream(k);
                        if (reader == null)
                        {
                            ppstmt.setNull(k, Types.BIGINT);
                        } else
                        {
                            File file = this.getFile(reader, k);
                            //conn2.setAutoCommit(false);
                            Long oid = getOid(conn, file);
                            ppstmt.setLong(k, oid);
                        }
                        break;
                }
                break;
            case "INTERVALDS":
                String intervalds = checkIntervalDS(ObjEnum.DBType.ORACLE, rtset.getObject(k).toString());
                ppstmt.setObject(k, new PGInterval(intervalds));
                break;
            case "INTERVALYM":
                String intervalym = checkIntervalYM(ObjEnum.DBType.ORACLE, rtset.getObject(k).toString());
                ppstmt.setObject(k, new PGInterval(intervalym));
                break;
            case "ROWID":
                ppstmt.setLong(k, rtset.getLong(k));
                break;
            case "XMLTYPE":
                ppstmt.setSQLXML(k, rtset.getSQLXML(k));
                break;
            case "CHAR":
            case "NCHAR":
            case "VARCHAR2":
            case "NVARCHAR2":
                ppstmt.setString(k, rtset.getString(k));
                break;
            case "DATE":
                ppstmt.setTimestamp(k, rtset.getTimestamp(k));//PRO
                break;
            case "TIMESTAMP":
            case "TIMESTAMP WITH TIME ZONE":
            case "TIMESTAMP WITH LOCAL TIME ZONE":
                ppstmt.setTimestamp(k, rtset.getTimestamp(k));
                break;
            case "NUMBER":
                ppstmt.setBigDecimal(k, rtset.getBigDecimal(k));
                break;
            case "FLOAT":
            case "BINARY_FLOAT":
            case "BINARY_DOUBLE":
                ppstmt.setDouble(k, rtset.getDouble(k));
                break;
            default:
                logger.warn("undefined oracle datatype convertion: oracleType=" + colType
                        + ",hgType=" + hgType);
                ppstmt.setObject(k, rtset.getObject(k));
                break;
        }
    }
       
    //for oracle BFILE
    private File getFile(BFILE bfile, int column) throws Exception
    {
        OutputStream ops = null;
        BufferedInputStream bips = null;
        File file = null;
        try
        {
            File f = new File("tabData");
            if (!f.exists())
            {
                f.mkdirs();
                logger.info("Folder TabData doesn't exists,make this directory.");
            }

            file = new File("tabData"
                    + File.separator + column);
            logger.info("BFile Path = " + file.getAbsolutePath());
            bfile.openFile();
            bips = new BufferedInputStream(bfile.getBinaryStream());
            ops = new FileOutputStream(file);
            byte[] buffer = new byte[1024];//bfile.getBytes().length
            for (int i; (i = bips.read(buffer)) > 0;)
            {
                ops.write(buffer, 0, i);
                ops.flush();
            }
            bfile.closeFile();
        }
        catch (Exception ex)
        {
            logger.error("Error:" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error get file for BFile");
        }
        finally
        {
            CloseStream.close(bips);
            CloseStream.close(ops);
            logger.info("Close Resource");
        }
        return file;
    }

    //for oracle blob
    private File getFile(InputStream ips, int column) throws Exception
    {
        OutputStream ops = null;
        BufferedInputStream bips = null;
        File file = null;
        try
        {
            File f = new File("tabData");
            if (!f.exists())
            {
                f.mkdirs();
                logger.info("Folder TabData doesn't exists,make this directory.");
            }
            file = new File("tabData" + File.separator + column);
            logger.info("Binary File Path = " + file.getAbsolutePath());
            ops = new FileOutputStream(file);
            bips = new BufferedInputStream(ips);
            byte[] buffer = new byte[1024];//bfile.getBytes().length
            for (int i; (i = bips.read(buffer)) > 0;)
            {
                ops.write(buffer, 0, i);
                ops.flush();
            }
        }
        catch (Exception ex)
        {
            logger.error("Error:" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error get file for BLOB");
        }
        finally
        {
            CloseStream.close(ips);
            CloseStream.close(bips);
            CloseStream.close(ops);
            logger.info("Close Resource");
        }
        return file;
    }

    //for oracle clob
    private File getFile(Reader reader, int column) throws Exception
    {
        BufferedWriter outstream = null;
        BufferedReader instream = null;
        File file = null;
        try
        {
            File f = new File("tabData");
            if (!f.exists())
            {
                f.mkdirs();
                logger.info("Folder TabData doesn't exists,make this directory.");
            }
            file = new File("tabData" + File.separator + column);
            logger.info("Binary File Path = " + file.getAbsolutePath());
            outstream = new BufferedWriter(new FileWriter(file));
            instream = new BufferedReader(reader);


            char[] data = new char[1024];
            for (int i; (i = instream.read(data)) > 0;)
            {
                outstream.write(data, 0, i);
                outstream.flush();
            }
        }
        catch (Exception ex)
        {
            logger.error("Error:" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error get file for CLOB");
        }
        finally
        {
            CloseStream.close(instream);
            CloseStream.close(outstream);
            logger.info("Close Resource");
        }
        return file;
    }

    //maybe for oracle binary type
    private Long getOid(Connection conn, File file) throws Exception
    {
        long oid = 0;
        FileInputStream fis = null;
        try
        {
            // Get the Large Object Manager to perform operations with
            //ygq v5 update start
            //LargeObjectManager lobj = ((HGConnection) conn).getLargeObjectAPI();
            LargeObjectManager lobj = ((PGConnection) conn).getLargeObjectAPI();
            //ygq v5 update end
            // Create a new large object
            oid = lobj.createLO(LargeObjectManager.READ);
            // Open the large object for writing
            LargeObject obj = lobj.open(oid, LargeObjectManager.WRITE);
            // Now open the file
            fis = new FileInputStream(file);
            // Copy the data from the file to the large object
            byte buf[] = new byte[2048];
            int s, tl = 0;
            while ((s = fis.read(buf, 0, 2048)) > 0)
            {
                obj.write(buf, 0, s);
                tl += s;
            }
            obj.close();
        }
        catch (Exception ex)
        {
            logger.error("Error:" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception("Error get long value for OID");
        }
        finally
        {
            CloseStream.close(fis);
        }
        return oid;
    }

    private String getHgTypeWithoutParameter(ObjEnum.DBType sourceDB, String sourceDataType, List<DataTypeCastDTO> dataTypeCastList)
    {
        String hgType = null;        
        int size = dataTypeCastList.size();
        int j = 0;
        for (; j < size ; j++)
        {
            String patternString;
            Pattern pattern;
            Matcher matcher;
            patternString = dataTypeCastList.get(j).getSourceDataType().getDataType();
            pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(sourceDataType);
            if (matcher.matches())
            {
                hgType = dataTypeCastList.get(j).getHgDataType().getDataType();
                break;
            }
        }
        //logger.info("j=" + j);
        if (j == dataTypeCastList.size())
        {
            hgType = "TEXT";
            logger.warn("Warn:Found no cast for source datatype = " + sourceDataType
                    + ",and set it's HG datatype to text");
        }
        return hgType.toUpperCase();
    }

    private String getHgTypeWithParameter(ObjEnum.DBType sourceDBType, List<DataTypeCastDTO> dataTypeCastList,
            String schema, String table, String column,
            String sourceDataType, String dataPrecision, String dataScale, int dataLength)
    {
//        logger.info("Enter");
//        int j;
//        for (j = 0; j < dataTypeCastList.size(); j++)
//        {
//            String patternString;
//            Pattern pattern;
//            Matcher matcher;
//            patternString = dataTypeCastList.get(j).getSourceDataType().getDataType();
//            pattern = Pattern.compile(patternString, Pattern.CASE_INSENSITIVE);
//            matcher = pattern.matcher(sourceDataType);
//            if (matcher.matches())
//            {
//                sourceDataType = dataTypeCastList.get(j).getHgDataType().getDataType();
//                sourceDataType = castDataType(sourceDBType, schema, table, column,
//                        sourceDataType, dataPrecision, dataScale, dataLength);
//                break;
//            }
//        }
//        //logger.info("j=" + j);
//        if (j == dataTypeCastList.size())
//        {
//            String warn = "Warn:Found no cast for datatype = " + sourceDataType
//                    + ",and set this datatype to text";
//            logger.warn(warn);
//            //remain to deal with
//            // writeLog(pwTotal, "DataType Cast Warn = " + warn);
//            sourceDataType = "TEXT";
//            //use the text datetype to make this column
//        }
//        logger.info("Return");
        //logger.info("sourceDataType=" + sourceDataType);
        String hgType = getHgTypeWithoutParameter(sourceDBType, sourceDataType, dataTypeCastList);
        //logger.info("HgTypeWithoutParameter=" + hgType);
        hgType = castDataType(sourceDBType, schema, table, column,
                hgType, dataPrecision, dataScale, dataLength);
        //logger.info("FinalHgType=" + hgType);
        return hgType;
    }

    private String castDataType(ObjEnum.DBType sourceDBType, String schema, String table, String column,
            String dataType, String dataPrecision, String dataScale, int dataLength)
    {
        String hgType = null;
        //never need to switch db type, because this is only for oracle
        switch (dataType)
        {
            case "INTERVAL":
            case "TIMESTAMP":
            case "TIMESTAMPTZ":
                hgType = dataType + "(" + dataScale + ")";
                break;
            case "CHAR":
            case "VARCHAR":
                hgType = dataType + "(" + dataLength + ")";
                break;
            case "NUMERIC":
                //the following two special situations arise from
                //oracle number datatype and highgo numric datatype 's parameter having
                //different defination method and  different range of value
                //1. number(*) or number--dataprecision=null,datascale=null,this has all scale and all precision
                if (dataPrecision == null && dataScale == null)
                {
                    hgType = dataType;
                } //2.number(*,s) ---dataPrecision=null,dataScale=s(0,1,2...),this has specified scale and all precision
                else if (dataPrecision == null && dataScale != null)
                {
                    if (Integer.parseInt(dataScale) < 0)
                    {
                        logger.warn("Warn:There's an oracle number data scale < 0,"
                                + "its schema=" + schema
                                + " ,table=" + table
                                + " ,column=" + column
                                + " ,datatype=" + dataType
                                + " ,dataPrecision=" + dataPrecision
                                + " ,dataScale=" + dataPrecision
                                + ";here we convert this scale to 0 in HighGo DB.");
                        dataScale = "0";
                    }
                    hgType = dataType + "(126," + dataScale + ")";
                } 
                //3.number(p,s) (include number(38) stands for number(38,0))
                else if (dataPrecision != null && dataScale != null)
                {
                    //HGDB never suppert a negivate scale;
                    if (Integer.parseInt(dataScale) < 0)
                    {
                        logger.warn("Warn:There's an oracle number data scale < 0,"
                                + "its schema=" + schema
                                + " ,table=" + table
                                + " ,column=" + column
                                + " ,datatype=" + dataType
                                + " ,dataPrecision=" + dataPrecision
                                + " ,dataScale=" + dataPrecision
                                + ";here we convert this scale to 0 in HighGo DB.");
                        dataScale = "0";
                    }
                    //this beacause oracle suppoert type like number(3,6)(can insert values <0.001,like 0.0009,0.000888 etc)
                    //but hgdb always support precision >= scale.
                    //in order to support oracle number values ,then use 
                    else if (Integer.parseInt(dataPrecision) < Integer.parseInt(dataScale))
                    {
                        dataPrecision = dataScale;
                    }

                    hgType = dataType + "(" + dataPrecision + "," + dataScale + ")";
                }
                break;
            default://others data type needn't parameter                
                hgType = dataType;
                break;           
        }
        return hgType;
    }

    //one table has more than one constraint of the same type
    private List<FKConstrInfoDTO> migrateConstraint(PrintWriter pwError, PrintWriter pwResult,
            DBConnInfoDTO sourceDBInfo, DBConnInfoDTO hgDBInfo,
            List<FKConstrInfoDTO> fkSQLList, String schema, String table) throws Exception
    {
        logger.info("Enter");

        SQLFactory sqlf = SQLFactory.getInstance();
        String mainSql = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.CONSTRAINT_MAIN);

        Connection conn = null;
        PreparedStatement preparedStmt = null;
        ResultSet rtset = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            preparedStmt = conn.prepareStatement(mainSql);
            preparedStmt.setString(1, schema);
            preparedStmt.setString(2, table);
            rtset = preparedStmt.executeQuery();
            while (rtset.next())
            {
                ConstrInfoDTO tabConstrInfo = new ConstrInfoDTO();
                String constrType = rtset.getString(1);
                tabConstrInfo.setConstrType(constrType);
                tabConstrInfo.setSchema(rtset.getString(2));
                tabConstrInfo.setTabName(rtset.getString(3));
                tabConstrInfo.setConstrName(rtset.getString(4));
                tabConstrInfo.setRConstrName(rtset.getString(5));
                if (!constrType.equals("C"))
                {
                    String colSQL = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.CONSTRAINT_COLUMN);
                    tabConstrInfo.setColsSQL(getConstrColSQL(tabConstrInfo, sourceDBInfo, colSQL));
                }
                if (constrType.equals("C"))
                {
                    String checkSQL = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.CONSTRAINT_CHECKCONDITION);
                    tabConstrInfo.setSearchCondition(getConstrCheckCondition(tabConstrInfo, sourceDBInfo, checkSQL));
                }
                if (constrType.equals("R"))
                {
                    String rColumnSQL = sqlf.getSelectSQL(sourceDBInfo.getDBType(), ObjEnum.ObjType.CONSTRAINT_RCOLUMN);
                    tabConstrInfo.setRTabColsSQL(getConstrRTabColSQL(tabConstrInfo, sourceDBInfo, rColumnSQL));
                }
                String constrSQL = getConstrAddSQL(tabConstrInfo);

                if (constrType.equals("R"))
                {
                    FKConstrInfoDTO fkConstr = new FKConstrInfoDTO();
                    fkConstr.setSchema(schema);
                    fkConstr.setTable(table);
                    fkConstr.setConstraint(tabConstrInfo.getConstrName());
                    fkConstr.setFkSQL(constrSQL);
                    fkSQLList.add(fkConstr);
                }
                else
                {
                    logger.info("constrSQL=" + constrSQL);
                    executeUpdate(pwError, hgDBInfo, constrSQL);
                }
            }
            preparedStmt.clearBatch();
            preparedStmt.clearParameters();
        }
        catch (Exception ex)
        {
            writeLog(pwError, time.format(new Date()) + " Error Get Constrait Main Information:"
                    + mainSql + "\n" + ex.getMessage());
//            writeLog(pwResult, "<br/> Failed add Constraints(exexpt FKey): " + schema + "." + table);
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        logger.info("Return: ");
        return fkSQLList;
    }

    //get constrain columns for PK,FK,UK;like (col1,col2...).
    private String getConstrColSQL(ConstrInfoDTO constrInfo,
            DBConnInfoDTO sourceDBInfo, String colSQL) throws Exception
    {
        StringBuilder constrColSQL = new StringBuilder();
        Connection conn = null;
        PreparedStatement preparedStmt = null;
        ResultSet rtset = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            preparedStmt = conn.prepareStatement(colSQL);
            preparedStmt.setString(1, constrInfo.getConstrType());
            preparedStmt.setString(2, constrInfo.getSchema());
            preparedStmt.setString(3, constrInfo.getTabName());
            preparedStmt.setString(4, constrInfo.getConstrName());
            rtset = preparedStmt.executeQuery();
            boolean isFirst = true;
            constrColSQL.append("(");
            while (rtset.next())
            {
                String colname = rtset.getString(1);
                colname = checkQuoteColName(colname, ObjEnum.DBType.HGDB);
                if (isFirst)
                {
                    isFirst = false;
                    constrColSQL.append(colname);
                } else
                {
                    constrColSQL.append(",").append(colname);
                }
            }
            constrColSQL.append(")");

            preparedStmt.clearBatch();
            preparedStmt.clearParameters();
        }
        catch (Exception ex)
        {
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(preparedStmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return constrColSQL.toString();
    }

    //get check condition for check constraint.
    private String getConstrCheckCondition(ConstrInfoDTO constrInfoDTO, DBConnInfoDTO sourceDBInfo, String checkSQL)
    {
        String sbSCondition = null;
        Connection conn = null;
        PreparedStatement ppstmt = null;
        ResultSet rtset = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            ppstmt = conn.prepareStatement(checkSQL);
            ppstmt.setString(1, constrInfoDTO.getSchema());
            ppstmt.setString(2, constrInfoDTO.getTabName());
            ppstmt.setString(3, constrInfoDTO.getConstrName());
            rtset = ppstmt.executeQuery();
            boolean isFirst = true;
            while (rtset.next())//only one line
            {
                if (isFirst)
                {
                    sbSCondition = rtset.getString(1);
                }
            }
            //logger.info("sbSCondition = " + sbSCondition);
            ppstmt.clearBatch();
            ppstmt.clearParameters();
        }
        catch (Exception ex)
        {
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(ppstmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }

        return sbSCondition;
    }

    //get references table and columns for FK,likes schema.table(col1,col2...).
    private String getConstrRTabColSQL(ConstrInfoDTO constrInfo, DBConnInfoDTO sourceDBInfo, String rColumnSQL)
    {
        logger.info("Enter");
        String rColSQL = null;

        Connection conn = null;
        PreparedStatement ppstmt = null;
        ResultSet rtset = null;
        try
        {
            conn = ConnectHelper.getConnection(sourceDBInfo);
            logger.info("Connected");
            ppstmt = conn.prepareStatement(rColumnSQL);
//            preparedStmt.setString(1, constrType);
            ppstmt.setString(1, constrInfo.getSchema());
            ppstmt.setString(2, constrInfo.getRConstrName());
            rtset = ppstmt.executeQuery();
            boolean isFirst = true;
            while (rtset.next())
            {
                if (isFirst)
                {
                    String user = rtset.getString(1);
                    user = checkQuoteObjName(user, ObjEnum.DBType.HGDB);
                    String table = rtset.getString(2);
                    table = checkQuoteObjName(table, ObjEnum.DBType.HGDB);
                    String colname = rtset.getString(3);
                    colname = checkQuoteColName(colname, ObjEnum.DBType.HGDB);

                    rColSQL = " " + user + "." + table + "(" + colname;
                    isFirst = false;
                }
                else
                {
                    String colname = rtset.getString(3);
                    colname = checkQuoteColName(colname, ObjEnum.DBType.HGDB);
                    rColSQL = rColSQL + "," + colname;
                }
            }
            rColSQL = rColSQL + ")";

            ppstmt.clearParameters();
            ppstmt.clearBatch();
        }
        catch (Exception ex)
        {
            logger.error("Error:" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(rtset);
            JdbcHelper.close(ppstmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        logger.info("Return:rColSQL=" + rColSQL);
        return rColSQL;
    }

    //for all kind of constraint
    private String getConstrAddSQL(ConstrInfoDTO constrInfo)
    {
        logger.info("Enter");
        StringBuilder constrSQL = new StringBuilder();
        constrSQL.append("ALTER TABLE ")
                .append(checkQuoteObjName(constrInfo.getSchema(), ObjEnum.DBType.HGDB)).append(".")
                .append(checkQuoteObjName(constrInfo.getTabName(), ObjEnum.DBType.HGDB))
                .append(" ADD CONSTRAINT ").append(constrInfo.getConstrName());
        if (constrInfo.getConstrType().equals("R"))
        {
            constrSQL.append(" FOREIGN KEY").append(constrInfo.getColsSQL())
                    .append(" REFERENCES ").append(constrInfo.getrTabColsSQL()).append(";");
        }
        if (constrInfo.getConstrType().equals("P"))
        {
            constrSQL.append(" PRIMARY KEY").append(constrInfo.getColsSQL()).append(";");
        }
        if (constrInfo.getConstrType().equals("U"))
        {
            constrSQL.append(" UNIQUE").append(constrInfo.getColsSQL()).append(";");
        }
        if (constrInfo.getConstrType().equals("C"))
        {
            constrSQL.append(" CHECK(").append(constrInfo.getSearchCondition().toLowerCase()).append(");");
        }
        //logger.info("Return:constrSQL=" + constrSQL.toString());
        return constrSQL.toString();
    }

    //add fk
    private void addFKConstrList(PrintWriter pwResult, PrintWriter pwError, DBConnInfoDTO hgDBInfo, List<FKConstrInfoDTO> fkSqlList)
    {
        int fkSize = fkSqlList.size();
        logger.info("Enter:FKSize = " + fkSize);
        Connection conn = null;
        try
        {
            conn = ConnectHelper.getConnection(hgDBInfo);
            logger.info("Connected");
            PreparedStatement preparedStmt = null;
            int count = 1;
            for (FKConstrInfoDTO fkConstr : fkSqlList)
            {
                if (isStop)
                {
                    break;
                }
                String isSuccess = "Successed";
                progressInfo.setState((count) + "/" + fkSize + " Migrating FKey:" + fkConstr.getConstraint()
                        + "(" + fkConstr.getSchema() + "." + fkConstr.getTable() + ")");
                String fkSQL = fkConstr.getFkSQL();
                logger.info("fkSQL = " + fkSQL);
                try
                {
                    preparedStmt = conn.prepareStatement(fkSQL);
                    preparedStmt.executeUpdate();
                }
                catch (Exception ex)
                {
                    isSuccess = "Failed";
                    writeLog(pwError, time.format(new Date()) + " Error Add FKey Constraint:"
                            + fkSQL + "\n" + ex.getMessage());
                    writeLog(pwResult, "<br/> Failed add FKey to table: "
                            + fkConstr.getSchema() + "." + fkConstr.getTable());
                    logger.error(fkSQL + "\n Error:" + ex.getMessage());
                    ex.printStackTrace(System.out);
                }
                finally
                {
                    JdbcHelper.close(preparedStmt);
                    count++;
                    progressInfo.setValue(count);
                    progressInfo.setDetail(progressInfo.getDetail().
                            append(progressInfo.getState()).append(" ").append(isSuccess).append("\n\r"));
                }
            }
        }
        catch (Exception ex)
        {
            writeLog(pwError, time.format(new Date()) + " Error Add FKey Constraint List:"
                    + ex.getMessage());
            logger.error("Error: " + ex.getMessage());
            ex.printStackTrace(System.out);
        }
        finally
        {
            JdbcHelper.close(conn);
            logger.info("Close Source");
        }
        logger.info("Return");
    }
    
}
