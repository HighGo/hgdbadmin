/* ------------------------------------------------
 *
 * File: DataTypeFactory.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\controller\DataTypeFactory.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.controller;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class KeyWordFactory
{
    //private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private KeyWordFactory()
    {
    }
    private static KeyWordFactory keyWordController = null;
    public static KeyWordFactory getInstance()
    {
        if (keyWordController == null)
        {
            keyWordController = new KeyWordFactory();
        }
        return keyWordController;
    }

    //column name key word of different database
    public List<String> getColNameKeyWords(ObjEnum.DBType dbType)
    {
        //logger.info("Enter getKeyWords:dbType = " + dbType);
        List<String> keywords = new ArrayList();
        switch (dbType)
        {
            case HGDB:
                //column name key word
//                keywords.add("BETWEEN");
//                keywords.add("BIGINT");
//                keywords.add("BIT");
//                keywords.add("BLOB");
//                keywords.add("BOOLEAN");
//                keywords.add("CHAR");
//                keywords.add("CHARACTER");
//                keywords.add("CLOB");
//                keywords.add("COALESCE");
//                keywords.add("DEC");
//                keywords.add("DECIMAL");
//                keywords.add("EXISTS");
//                keywords.add("EXTRACT");
//                keywords.add("FLOAT");
//                keywords.add("GREATEST");
//                keywords.add("INOUT");
//                keywords.add("INT");
//                keywords.add("INTEGER");
//                keywords.add("INTERVAL");
//                keywords.add("LEAST");
//                keywords.add("NATIONAL");
//                keywords.add("NCHAR");
//                keywords.add("NONE");
//                keywords.add("NULLIF");
//                keywords.add("NUMERIC");
//                keywords.add("OUT");
//                keywords.add("OVERLAY");
//                keywords.add("POSITION");
//                keywords.add("PRECISION");
//                keywords.add("REAL");
//                keywords.add("ROW");
//                keywords.add("SETOF");
//                keywords.add("SMALLINT");
//                keywords.add("SUBSTRING");
//                keywords.add("SYSDATE");
//                keywords.add("TIME");
//                keywords.add("TIMESTAMP");
//                keywords.add("TREAT");
//                keywords.add("TRIM");
//                keywords.add("VALUES");
//                keywords.add("VARCHAR");
//                keywords.add("XMLATTRIBUTES");
//                keywords.add("XMLCONCAT");
//                keywords.add("XMLELEMENT");
//                keywords.add("XMLEXISTS");
//                keywords.add("XMLFOREST");
//                keywords.add("XMLPARSE");
//                keywords.add("XMLPI");
//                keywords.add("XMLROOT");
//                keywords.add("XMLSERIALIZE");

                //reserved keywords
                keywords.add("ALL");
                keywords.add("ANALYSE");
                keywords.add("ANALYZE");
                keywords.add("AND");
                keywords.add("ANY");
                keywords.add("ARRAY");
                keywords.add("AS");
                keywords.add("ASC");
                keywords.add("ASYMMETRIC");
                keywords.add("BINARY");
                keywords.add("BOTH");
                keywords.add("CASE");
                keywords.add("CAST");
                keywords.add("CHECK");
                keywords.add("COLLATE");
                keywords.add("COLUMN");
                keywords.add("CONCURRENTLY");
                keywords.add("CONSTRAINT");
                keywords.add("CREATE");
                keywords.add("CROSS");
                keywords.add("CURRENT_CATALOG");
                keywords.add("CURRENT_DATE");
                keywords.add("CURRENT_ROLE");
                keywords.add("CURRENT_SCHEMA");
                keywords.add("CURRENT_TIME");
                keywords.add("CURRENT_TIMESTAMP");
                keywords.add("CURRENT_USER");
                keywords.add("DEFAULT");
                keywords.add("DEFERABLE");
                keywords.add("DESC");
                keywords.add("DISTINCT");
                keywords.add("DO");
                keywords.add("ELSE");
                keywords.add("END");
                keywords.add("EXCEPT");
                keywords.add("FALSE");
                keywords.add("FETCH");               
                keywords.add("FOR");
                keywords.add("FOREIGN");
                keywords.add("FREEZE");
                keywords.add("FROM");
                keywords.add("FULL");
                keywords.add("FUNCTION");//
                keywords.add("GRANT");
                keywords.add("GROUP");
                keywords.add("HAVING");
                keywords.add("ILIKE");
                keywords.add("IN");
                keywords.add("INITIALLY");
                keywords.add("INNER");
                keywords.add("INTERSECT");
                keywords.add("INTO");
                keywords.add("IS");
                keywords.add("ISNULL");
                keywords.add("JOIN");
                keywords.add("LEADING");
                keywords.add("LEFT");
                keywords.add("LIKE");
                keywords.add("LIMIT");
                keywords.add("LOCALTIME");
                keywords.add("LOCALTIMESTAMP");
                keywords.add("NOT");
                keywords.add("NOTNULL");
                keywords.add("NULL");
                keywords.add("OFFSET");
                keywords.add("ON");
                keywords.add("ONLY");
                keywords.add("OR");
                keywords.add("ORDER");
                keywords.add("OUTER");
                keywords.add("OVER");
                keywords.add("OVERLAPS");
                keywords.add("PLACING");
                keywords.add("PRIMARY");
                keywords.add("PROCEDURE");//
                keywords.add("REFERENCES");
                keywords.add("RETURN");//
                keywords.add("RETURNING");
                keywords.add("RIGHT");
                keywords.add("SELECT");
                keywords.add("SESSION_USER");
                keywords.add("SIMILAR");
                keywords.add("SOME");
                keywords.add("SYMMETRIC");
                keywords.add("TABLE");
                keywords.add("THEN");
                keywords.add("TO");
                keywords.add("TOP");//
                keywords.add("TRAILING");
                keywords.add("TRUE");
                keywords.add("UNION");
                keywords.add("UNIQUE");
                keywords.add("USER");
                keywords.add("USING");
                keywords.add("VARIADIC");
                keywords.add("VERBOSE");
                keywords.add("WHEN");
                keywords.add("WHERE");
                keywords.add("WINDOW");
                keywords.add("WITH");                
                //reserved keywords(can be function or type)
                keywords.add("AUTHORIZATION");
                keywords.add("BINARY");
                keywords.add("COLLATION");//is key word in pg9.2,not key word in hg1.3
                keywords.add("CONCURRENTLY");
                keywords.add("CROSS");
                keywords.add("CURRENT_SCHEMA");
                keywords.add("FREEZE");
                keywords.add("FULL");
                keywords.add("ILIKE");
                keywords.add("INNER");
                keywords.add("IS");
                keywords.add("ISNULL");
                keywords.add("JOIN");
                keywords.add("LEFT");
                keywords.add("LIKE");
                keywords.add("NATURAL");
                keywords.add("NOTNULL");
                keywords.add("OUTER");
                keywords.add("OVER");
                keywords.add("OVERLAPS");
                keywords.add("RIGHT");
                keywords.add("SIMILAR");
                keywords.add("VERBOSE");
                break;
            case ORACLE:
                //column name key word
                keywords.add("ACCESS");
                keywords.add("ADD");
                keywords.add("AUDIT");
                keywords.add("COLUMN");
                keywords.add("COMMENT");
                keywords.add("CURRENT");
                keywords.add("FILE");
                keywords.add("IMMEDIATE");
                keywords.add("INCREMENT");
                keywords.add("INITIAL");
                keywords.add("LEVEL");
                keywords.add("MAXEXTENTS");
                keywords.add("MLSLABEL");
                keywords.add("MODIFY");
                keywords.add("NOAUDIT");
                keywords.add("OFFLINE");
                keywords.add("ONLINE");
                keywords.add("PRIVILLEGES");
                keywords.add("ROW");
                keywords.add("ROWID");
                keywords.add("ROWNUM");
                keywords.add("ROWS");
                keywords.add("SESSION");
                keywords.add("SUCCESSFUL");
                keywords.add("SYSDATE");
                keywords.add("UID");
                keywords.add("USER");
                keywords.add("VALIDATE");
                keywords.add("WHENEVER");

                //reserved keyword
                keywords.add("&");
                keywords.add("ALL");
                keywords.add("ALTER");
                keywords.add("AND");
                keywords.add("ANY");
                keywords.add("AS");
                keywords.add("ASC");
                keywords.add("BETWEEN");
                keywords.add("BY");
                keywords.add("CHAR");
                keywords.add("CHECK");
                keywords.add("CLUSTER");
                keywords.add("COMPRESS");
                keywords.add("CONNECT");
                keywords.add("CREATE");
                keywords.add("DATE");
                keywords.add("DECIMAL");
                keywords.add("DEFAULT");
                keywords.add("DELETE");
                keywords.add("DESC");
                keywords.add("DISTINCT");
                keywords.add("DROP");
                keywords.add("ELSE");
                keywords.add("EXCLUSIVE");
                keywords.add("EXISTS");
                keywords.add("FLOAT");
                keywords.add("FOR");
                keywords.add("FROM");
                keywords.add("GRANT");
                keywords.add("GROUP");
                keywords.add("HAVING");
                keywords.add("IDENTIFIED");
                keywords.add("IN");
                keywords.add("INDEX");
                keywords.add("INSERT");
                keywords.add("INTEGER");
                keywords.add("INTERSECT");
                keywords.add("INTO");
                keywords.add("IS");
                keywords.add("LIKE");
                keywords.add("LOCK");
                keywords.add("LONG");
                keywords.add("MINUS");
                keywords.add("MODE");
                keywords.add("NOCOMPRESS");
                keywords.add("NOT");
                keywords.add("NOWAIT");
                keywords.add("NULL");
                keywords.add("BUMBER");
                keywords.add("OF");
                keywords.add("ON");
                keywords.add("OPTION");
                keywords.add("OR");
                keywords.add("ORDER");
                keywords.add("PCTFREE");
                keywords.add("PRIOR");
                keywords.add("PUBLIC");
                keywords.add("RAW");
                keywords.add("RENAME");
                keywords.add("RESOURCE");
                keywords.add("REVOKE");
                keywords.add("SELECT");
                keywords.add("SET");
                keywords.add("SHARE");
                keywords.add("SIZE");
                keywords.add("SMALLINT");
                keywords.add("START");
                keywords.add("SYNONYM");
                keywords.add("TABLE");
                keywords.add("THEN");
                keywords.add("TO");
                keywords.add("TRIGGER");
                keywords.add("UNION");
                keywords.add("UNIQUE");
                keywords.add("UPDATE");
                keywords.add("VALUES");
                keywords.add("VARCHAR");
                keywords.add("VARCHAR2");
                keywords.add("VIEW");
                keywords.add("WHERE");
                keywords.add("WITH");
                //inital keyword
                //those
//                keywords.add("/");
//                keywords.add("\\");
//                keywords.add(" ");
//                keywords.add("#");
//                keywords.add("&");
                break;
            case SQLSERVER:
                //need to deal
                break;
        }
        //logger.info("Return getKeyWords:size  = " + keywords.size());
        return keywords;
    }

    //reserved key word of different database
    public List<String> getKeyWords(ObjEnum.DBType dbType)
    {
        //logger.info("Enter getKeyWords:dbType = " + dbType);
        List<String> keywords = new ArrayList();
        switch (dbType)
        {
            case HGDB:
                //hg1.3(pg9.0) http://www.postgresql.org/docs/9.0/interactive/sql-keywords-appendix.html
                //reserved keywords
                keywords.add("ALL");
                keywords.add("ANALYSE");
                keywords.add("ANALYZE");
                keywords.add("AND");
                keywords.add("ANY");
                keywords.add("ARRAY");
                keywords.add("AS");
                keywords.add("ASC");
                keywords.add("ASYMMETRIC");
                keywords.add("BINARY");
                keywords.add("BOTH");
                keywords.add("CASE");
                keywords.add("CAST");
                keywords.add("CHECK");
                keywords.add("COLLATE");
                keywords.add("COLUMN");
                keywords.add("CONCURRENTLY");
                keywords.add("CONSTRAINT");
                keywords.add("CREATE");
                keywords.add("CROSS");
                keywords.add("CURRENT_CATALOG");
                keywords.add("CURRENT_DATE");
                keywords.add("CURRENT_ROLE");
                keywords.add("CURRENT_SCHEMA");
                keywords.add("CURRENT_TIME");
                keywords.add("CURRENT_TIMESTAMP");
                keywords.add("CURRENT_USER");
                keywords.add("DEFAULT");
                keywords.add("DEFERABLE");
                keywords.add("DESC");
                keywords.add("DISTINCT");
                keywords.add("DO");
                keywords.add("ELSE");
                keywords.add("END");
                keywords.add("EXCEPT");
                keywords.add("FALSE");
                keywords.add("FETCH");               
                keywords.add("FOR");
                keywords.add("FOREIGN");
                keywords.add("FREEZE");
                keywords.add("FROM");
                keywords.add("FULL");
                keywords.add("FUNCTION");//
                keywords.add("GRANT");
                keywords.add("GROUP");
                keywords.add("HAVING");
                keywords.add("ILIKE");
                keywords.add("IN");
                keywords.add("INITIALLY");
                keywords.add("INNER");
                keywords.add("INTERSECT");
                keywords.add("INTO");
                keywords.add("IS");
                keywords.add("ISNULL");
                keywords.add("JOIN");
                keywords.add("LEADING");
                keywords.add("LEFT");
                keywords.add("LIKE");
                keywords.add("LIMIT");
                keywords.add("LOCALTIME");
                keywords.add("LOCALTIMESTAMP");
                keywords.add("NOT");
                keywords.add("NOTNULL");
                keywords.add("NULL");
                keywords.add("OFFSET");
                keywords.add("ON");
                keywords.add("ONLY");
                keywords.add("OR");
                keywords.add("ORDER");
                keywords.add("OUTER");
                keywords.add("OVER");
                keywords.add("OVERLAPS");
                keywords.add("PLACING");
                keywords.add("PRIMARY");
                keywords.add("PROCEDURE");//
                keywords.add("REFERENCES");
                keywords.add("RETURN");//
                keywords.add("RETURNING");
                keywords.add("RIGHT");
                keywords.add("SELECT");
                keywords.add("SESSION_USER");
                keywords.add("SIMILAR");
                keywords.add("SOME");
                keywords.add("SYMMETRIC");
                keywords.add("TABLE");
                keywords.add("THEN");
                keywords.add("TO");
                keywords.add("TOP");//
                keywords.add("TRAILING");
                keywords.add("TRUE");
                keywords.add("UNION");
                keywords.add("UNIQUE");
                keywords.add("USER");
                keywords.add("USING");
                keywords.add("VARIADIC");
                keywords.add("VERBOSE");
                keywords.add("WHEN");
                keywords.add("WHERE");
                keywords.add("WINDOW");
                keywords.add("WITH");                
                //reserved keywords(can be function or type)
                keywords.add("AUTHORIZATION");
                keywords.add("BINARY");
                keywords.add("COLLATION");//is key word in pg9.2,not key word in hg1.3
                keywords.add("CONCURRENTLY");
                keywords.add("CROSS");
                keywords.add("CURRENT_SCHEMA");
                keywords.add("FREEZE");
                keywords.add("FULL");
                keywords.add("ILIKE");
                keywords.add("INNER");
                keywords.add("IS");
                keywords.add("ISNULL");
                keywords.add("JOIN");
                keywords.add("LEFT");
                keywords.add("LIKE");
                keywords.add("NATURAL");
                keywords.add("NOTNULL");
                keywords.add("OUTER");
                keywords.add("OVER");
                keywords.add("OVERLAPS");
                keywords.add("RIGHT");
                keywords.add("SIMILAR");
                keywords.add("VERBOSE");
                
//                //resvered key words
//                keywords.add("ALL");
//                keywords.add("ANALYSE");
//                keywords.add("ANALYZE");
//                keywords.add("AND");
//                keywords.add("ANY");
//                keywords.add("ARRAY");
//                keywords.add("AS");
//                keywords.add("ASC");
//                keywords.add("ASYMMETRIC");
//                keywords.add("BOTH");
//                keywords.add("CASE");
//                keywords.add("CAST");
//                keywords.add("CHECK");
//                keywords.add("COLLATE");
//                keywords.add("COLUMN");
//                keywords.add("CONSTRAINT");
//                keywords.add("CREATE");
//                keywords.add("CURRENT_CATALOG");
//                keywords.add("CURRENT_DATE");
//                keywords.add("CURRENT_ROLE");
//                keywords.add("CURRENT_TIME");
//                keywords.add("CURRENT_TIMESTAMP");
//                keywords.add("CURRENT_USER");
//                keywords.add("DEFAULT");
//                keywords.add("DEFERABLE");
//                keywords.add("DESC");
//                keywords.add("DISTINCT");
//                keywords.add("DO");
//                keywords.add("ELSE");
//                keywords.add("END");
//                keywords.add("EXCEPT");
//                keywords.add("FALSE");
//                keywords.add("FETCH");
//                keywords.add("FOR");
//                keywords.add("FOREIGN");
//                keywords.add("FROM");
//                keywords.add("FUNCTION");
//                keywords.add("GRANT");
//                keywords.add("GROUP");
//                keywords.add("HAVING");
//                keywords.add("IN");
//                keywords.add("INITIALLY");
//                keywords.add("INTERSECT");
//                keywords.add("INTO");
//                keywords.add("LEADING");
//                keywords.add("LIMIT");
//                keywords.add("LOCALTIME");
//                keywords.add("LOCALTIMESTAMP");
//                keywords.add("NOT");
//                keywords.add("NULL");
//                keywords.add("OFFSET");
//                keywords.add("ON");
//                keywords.add("ONLY");
//                keywords.add("OR");
//                keywords.add("ORDER");
//                keywords.add("PLACING");
//                keywords.add("PRIMARY");
//                keywords.add("PROCEDURE");
//                keywords.add("REFERENCES");
//                keywords.add("RETURN");
//                keywords.add("RETURNING");
//                keywords.add("SELECT");
//                keywords.add("SESSION_USER");
//                keywords.add("SOME");
//                keywords.add("SYMMETRIC");
//                keywords.add("TABLE");
//                keywords.add("THEN");
//                keywords.add("TO");
//                keywords.add("TOP");
//                keywords.add("TRAILING");
//                keywords.add("TRUE");
//                keywords.add("UNION");
//                keywords.add("UNIQUE");
//                keywords.add("USER");
//                keywords.add("USING");
//                keywords.add("VARIADIC");
//                keywords.add("WHEN");
//                keywords.add("WHERE");
//                keywords.add("WINDOW");
//                keywords.add("WITH");
//                //inital keyword
//                keywords.add("CURRENT_SCHEMA");
//                keywords.add("ANALYZE");
//                keywords.add("IS");
//                keywords.add("FULL");
//                keywords.add("LEFT");
//                keywords.add("LIKE");
//                keywords.add("CROSS");
//                keywords.add("GRANT");
//                keywords.add("OUTER");
//                keywords.add("RIGHT");
//                keywords.add("TABLE");
//                keywords.add("CONCURRENTLY");
//                keywords.add("VERBOSE");
//                keywords.add("OVERLAPS");
//                keywords.add("AUTHORIZATION");
//                keywords.add("BINARY");
//                keywords.add("FREEZE");
                //inital keyword
//                keywords.add("/");
//                keywords.add("\\");
//                keywords.add(" ");
//                keywords.add("#");
//                keywords.add("&");
                break;
            case ORACLE:
                //oracle 10g doc: http://docs.oracle.com/cd/B19306_01/server.102/b14200/ap_keywd.htm
                //reserved keyword
                keywords.add("&");
                keywords.add("ACCESS");
                keywords.add("ADD");
                keywords.add("ALL");
                keywords.add("ALTER");
                keywords.add("AND");
                keywords.add("ANY");
                keywords.add("AS");
                keywords.add("ASC");
                keywords.add("AUDIT");
                keywords.add("BETWEEN");
                keywords.add("BY");
                keywords.add("CHAR");
                keywords.add("CHECK");
                keywords.add("CLUSTER");
                keywords.add("COLUMN");
                keywords.add("COMMENT");
                keywords.add("COMPRESS");
                keywords.add("CONNECT");
                keywords.add("CREATE");
                keywords.add("DATE");
                keywords.add("DECIMAL");
                keywords.add("DEFAULT");
                keywords.add("DELETE");
                keywords.add("DESC");
                keywords.add("DISTINCT");
                keywords.add("DROP");
                keywords.add("ELSE");
                keywords.add("EXCLUSIVE");
                keywords.add("EXISTS");
                keywords.add("FILE");
                keywords.add("FLOAT");
                keywords.add("FOR");
                keywords.add("FROM");
                keywords.add("GRANT");
                keywords.add("GROUP");
                keywords.add("HAVING");
                keywords.add("IDENTIFIED");
                keywords.add("IN");
                keywords.add("INCREMENT");
                keywords.add("INDEX");
                keywords.add("INITIAL");
                keywords.add("INSERT");
                keywords.add("INTEGER");
                keywords.add("INTERSECT");
                keywords.add("INTO");
                keywords.add("IS");
                keywords.add("LEVEL");
                keywords.add("LIKE");
                keywords.add("LOCK");
                keywords.add("LONG");
                keywords.add("MAXEXTENTS");
                keywords.add("MINUS");
                keywords.add("MLSLABEL");
                keywords.add("MODE");
                keywords.add("MODIFY");
                keywords.add("NOAUDIT");
                keywords.add("NOCOMPRESS");
                keywords.add("NOT");
                keywords.add("NOWAIT");
                keywords.add("NULL");
                keywords.add("BUMBER");
                keywords.add("OF");
                keywords.add("OFFLINE");
                keywords.add("ON");
                keywords.add("ONLINE");
                keywords.add("OPTION");
                keywords.add("OR");
                keywords.add("ORDER");
                keywords.add("PCTFREE");
                keywords.add("PRIOR");
                keywords.add("PRIVILEGES");
                keywords.add("PUBLIC");
                keywords.add("RAW");
                keywords.add("RENAME");
                keywords.add("RESOURCE");
                keywords.add("REVOKE");
                keywords.add("ROW");
                keywords.add("ROWID");
                keywords.add("ROWNUM");
                keywords.add("ROWS");
                keywords.add("SELECT");
                keywords.add("SESSION");
                keywords.add("SET");
                keywords.add("SHARE");
                keywords.add("SIZE");
                keywords.add("SMALLINT");
                keywords.add("START");
                keywords.add("SUCCESSFUL");
                keywords.add("SYNONYM");
                keywords.add("SYSDATE");
                keywords.add("TABLE");
                keywords.add("THEN");
                keywords.add("TO");
                keywords.add("TRIGGER");
                keywords.add("UID");
                keywords.add("UNION");
                keywords.add("UNIQUE");
                keywords.add("UPDATE");
                keywords.add("USER");
                keywords.add("VALIDATE");
                keywords.add("VALUES");
                keywords.add("VARCHAR");
                keywords.add("VARCHAR2");
                keywords.add("VIEW");
                keywords.add("WHENEVER");
                keywords.add("WHERE");
                keywords.add("WITH");
                //inital keyword
//                keywords.add("/");
//                keywords.add("\\");
//                keywords.add(" ");
//                keywords.add("#");
//                keywords.add("&");
                break;
            case SQLSERVER:
                //reserved keyword
                keywords.add("ADD");
                keywords.add("ALL");
                keywords.add("ALTER");
                keywords.add("AND");
                keywords.add("ANY");
                keywords.add("AS");
                keywords.add("ASC");
                keywords.add("AUTHORIZATION");
                keywords.add("BACKUP");
                keywords.add("BEGIN");
                keywords.add("BETWEEN");
                keywords.add("BREAK");
                keywords.add("BROWSE");
                keywords.add("BULK");
                keywords.add("BY");
                keywords.add("CASCADE");
                keywords.add("CASE");
                keywords.add("CHECK");
                keywords.add("CHECKPOINT");
                keywords.add("CLOSE");
                keywords.add("CLUSTERED");
                keywords.add("COALESCE");
                keywords.add("COLLATE");
                keywords.add("COLUMN");
                keywords.add("COMMIT");
                keywords.add("COMPUTE");
                keywords.add("CONSTRAINT");
                keywords.add("CONTAINS");
                keywords.add("CONTAINSTABLE");
                keywords.add("CONTINUE");
                keywords.add("CONVERT");
                keywords.add("CREATE");
                keywords.add("CROSS");
                keywords.add("CURRENT");
                keywords.add("CURRENT_DATE");
                keywords.add("CURRENT_TIME");
                keywords.add("CURRENT_TIMESTAMP");
                keywords.add("CURRENT_USER");
                keywords.add("CURSOR");
                keywords.add("DATABASE");
                keywords.add("DBCC");
                keywords.add("DEALLOCATE");
                keywords.add("DECLARE");
                keywords.add("DEFAULT");
                keywords.add("DELETE");
                keywords.add("DENY");
                keywords.add("DESC");
                keywords.add("DISK");
                keywords.add("DISTINCT");
                keywords.add("DISTRIBUTED");
                keywords.add("DOUBLE");
                keywords.add("DROP");
                keywords.add("DUMMY");
                keywords.add("DUMP");
                keywords.add("ELSE");
                keywords.add("END");
                keywords.add("ERRLVL");
                keywords.add("ESCAPE");
                keywords.add("EXCEPT");
                keywords.add("EXEC");
                keywords.add("EXECUTE");
                keywords.add("EXISTS");
                keywords.add("EXIT");
                keywords.add("FETCH");
                keywords.add("FILE");
                keywords.add("FILLFACTOR");
                keywords.add("FOR");
                keywords.add("FOREIGN");
                keywords.add("FREETEXT");
                keywords.add("FREETEXTTABLE");
                keywords.add("FROM");
                keywords.add("FULL");
                keywords.add("FUNCTION");
                keywords.add("GOTO");
                keywords.add("GRANT");
                keywords.add("GROUP");
                keywords.add("HAVING");
                keywords.add("HOLDLOCK");
                keywords.add("IDENTITY");
                keywords.add("IDENTITY_INSERT");
                keywords.add("IDENTITYCOL");
                keywords.add("IF");
                keywords.add("IN");
                keywords.add("INDEX");
                keywords.add("INNER");
                keywords.add("INSERT");
                keywords.add("INTERSECT");
                keywords.add("INTO");
                keywords.add("IS");
                keywords.add("JOIN");
                keywords.add("KEY");
                keywords.add("KILL");
                keywords.add("LEFT");
                keywords.add("LIKE");
                keywords.add("LINENO");
                keywords.add("LOAD");
                keywords.add("NATIONAL");
                keywords.add("NOCHECK");
                keywords.add("NONCLUSTERED");
                keywords.add("NOT");
                keywords.add("NULL");
                keywords.add("NULLIF");
                keywords.add("OF");
                keywords.add("OFF");
                keywords.add("OFFSETS");
                keywords.add("ON");
                keywords.add("OPEN");
                keywords.add("OPENDATASOURCE");
                keywords.add("OPENQUERY");
                keywords.add("OPENROWSET");
                keywords.add("OPENXML");
                keywords.add("OPTION");
                keywords.add("OR");
                keywords.add("ORDER");
                keywords.add("OUTER");
                keywords.add("OVER");
                keywords.add("PERCENT");
                keywords.add("PLAN");
                keywords.add("PRECISION");
                keywords.add("PRIMARY");
                keywords.add("PRINT");
                keywords.add("PROC");
                keywords.add("PROCEDURE");
                keywords.add("PUBLIC");
                keywords.add("RAISERROR");
                keywords.add("READ");
                keywords.add("READTEXT");
                keywords.add("RECONFIGURE");
                keywords.add("REFERENCES");
                keywords.add("REPLICATION");
                keywords.add("RESTORE");
                keywords.add("RESTRICT");
                keywords.add("RETURN");
                keywords.add("REVOKE");
                keywords.add("RIGHT");
                keywords.add("ROLLBACK");
                keywords.add("ROWCOUNT");
                keywords.add("ROWGUIDCOL");
                keywords.add("RULE");
                keywords.add("SAVE");
                keywords.add("SCHEMA");
                keywords.add("SELECT");
                keywords.add("SESSION_USER");
                keywords.add("SET");
                keywords.add("SETUSER");
                keywords.add("SHUTDOWN");
                keywords.add("SOME");
                keywords.add("STATISTICS");
                keywords.add("SYSTEM_USER");
                keywords.add("TABLE");
                keywords.add("TEXTSIZE");
                keywords.add("THEN");
                keywords.add("TO");
                keywords.add("TOP");
                keywords.add("TRAN");
                keywords.add("TRANSACTION");
                keywords.add("TRIGGER");
                keywords.add("TRUNCATE");
                keywords.add("TSEQUAL");
                keywords.add("UNION");
                keywords.add("UNIQUE");
                keywords.add("UPDATE");
                keywords.add("UPDATETEXT");
                keywords.add("USE");
                keywords.add("USER");
                keywords.add("VALUES");
                keywords.add("VARYING");
                keywords.add("VIEW");
                keywords.add("WAITFOR");
                keywords.add("WHEN");
                keywords.add("WHERE");
                keywords.add("WHILE");
                keywords.add("WITH");
                keywords.add("WRITETEXT");
                break;
        }
        //logger.info("Return getKeyWords:size  = " + keywords.size());
        return keywords;
    }
    
}
