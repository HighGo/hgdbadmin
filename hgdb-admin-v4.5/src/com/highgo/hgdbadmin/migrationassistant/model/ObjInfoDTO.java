/* ------------------------------------------------
 *
 * File: ObjInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\ObjInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum.DBObj;


/**
 *
 * @author Liu Yuanyuan
 */
public class ObjInfoDTO //implements Comparable
{
    private DBObj type;
    private String schema;
    private String name;
    private boolean selected;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public DBObj getType()
    {
        return type;
    }

    public void setType(DBObj type)
    {
        this.type = type;
    }

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

//    @Override
//    public int compareTo(Object o)
//    {
//        ObjInfoDTO obj = (ObjInfoDTO) o;
//        DBObj otherType = obj.getType();
//        //enum-type's comparation depend on types' list order of enum method
//        //so this compare follow ObjEnum.objType order
//        return this.type.compareTo(otherType);
//    }

}
