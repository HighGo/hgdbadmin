/* ------------------------------------------------
 *
 * File: ObjListDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\ObjListDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class ObjListDTO
{
//    private String migrateModel;
//    private int copyBatch;
//    private int insertBatch;
    private List<ObjInfoDTO> schemaList;
    private List<ObjInfoDTO> tableList;
    private List<ObjInfoDTO> sequenceList;
    private List<ObjInfoDTO> viewList;
    private List<ObjInfoDTO> indexList;
    private List<ObjInfoDTO> procedureList;
    private List<ObjInfoDTO> functionList;
    private List<ObjInfoDTO> triggerList;

//    public String getMigrateModel()
//    {
//        return migrateModel;
//    }
//
//    public void setMigrateModel(String migrateModel)
//    {
//        this.migrateModel = migrateModel;
//    }
//
//    public int getCopyBatch()
//    {
//        return copyBatch;
//    }
//
//    public void setCopyBatch(int copyBatch)
//    {
//        this.copyBatch = copyBatch;
//    }
//
//    public int getInsertBatch()
//    {
//        return insertBatch;
//    }
//
//    public void setInsertBatch(int insertBatch)
//    {
//        this.insertBatch = insertBatch;
//    }

    public List<ObjInfoDTO> getSchemaList()
    {
        return schemaList;
    }

    public void setSchemaList(List<ObjInfoDTO> schemaList)
    {
        this.schemaList = schemaList;
    }

    public List<ObjInfoDTO> getTableList()
    {
        return tableList;
    }

    public void setTableList(List<ObjInfoDTO> tableList)
    {
        this.tableList = tableList;
    }

    public List<ObjInfoDTO> getSequenceList()
    {
        return sequenceList;
    }

    public void setSequenceList(List<ObjInfoDTO> sequenceList)
    {
        this.sequenceList = sequenceList;
    }

    public List<ObjInfoDTO> getViewList()
    {
        return viewList;
    }

    public void setViewList(List<ObjInfoDTO> viewList)
    {
        this.viewList = viewList;
    }

    public List<ObjInfoDTO> getIndexList()
    {
        return indexList;
    }

    public void setIndexList(List<ObjInfoDTO> indexList)
    {
        this.indexList = indexList;
    }

    public List<ObjInfoDTO> getProcedureList()
    {
        return procedureList;
    }

    public void setProcedureList(List<ObjInfoDTO> procedureList)
    {
        this.procedureList = procedureList;
    }

    public List<ObjInfoDTO> getFunctionList()
    {
        return functionList;
    }

    public void setFunctionList(List<ObjInfoDTO> functionList)
    {
        this.functionList = functionList;
    }

    public List<ObjInfoDTO> getTriggerList()
    {
        return triggerList;
    }

    public void setTriggerList(List<ObjInfoDTO> triggerList)
    {
        this.triggerList = triggerList;
    }

}
