/* ------------------------------------------------
 *
 * File: FKConstrInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\FKConstrInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class FKConstrInfoDTO
{
    String schema;
    String table;
    String constraint;
    String fkSQL;

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public String getTable()
    {
        return table;
    }

    public void setTable(String table)
    {
        this.table = table;
    }

    public String getConstraint()
    {
        return constraint;
    }

    public void setConstraint(String constraint)
    {
        this.constraint = constraint;
    }

    public String getFkSQL()
    {
        return fkSQL;
    }

    public void setFkSQL(String fkSQL)
    {
        this.fkSQL = fkSQL;
    }

}
