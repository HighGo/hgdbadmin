/* ------------------------------------------------
 *
 * File: ProgressInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\ProgressInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class ProgressInfoDTO
{
    private int maxValue;
    private int maxFKValue;
    private int value;//for progressBar
    private String state;//for lblState
    private StringBuilder detail = new StringBuilder();//for txaArea

    public int getMaxValue()
    {
        return maxValue;
    }

    public void setMaxValue(int maxValue)
    {
        this.maxValue = maxValue;
    }

    public int getMaxFKValue()
    {
        return maxFKValue;
    }

    public void setMaxFKValue(int maxFKValue)
    {
        this.maxFKValue = maxFKValue;
    }

    public int getValue()
    {
        return value;
    }

    public void setValue(int value)
    {
        this.value = value;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public StringBuilder getDetail()
    {
        return detail;
    }

    public void setDetail(StringBuilder detail)
    {
        this.detail = detail;
    }
}
