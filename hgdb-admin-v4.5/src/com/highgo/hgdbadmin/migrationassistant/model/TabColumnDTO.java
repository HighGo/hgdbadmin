/* ------------------------------------------------
 *
 * File: TabColumnDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\TabColumnDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class TabColumnDTO
{
    private String schema;
    private String tabName;
    private String cloName;
    private String dataType;
    private int dataLength;
    private String dataPrecision;
    private String dataScale;
    private String dataDefault;
    private boolean nullable;

    private String hgDataType;

    public String getHgDataType()
    {
        return hgDataType;
    }

    public void setHgDataType(String hgDataType)
    {
        this.hgDataType = hgDataType;
    }

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public String getTabName()
    {
        return tabName;
    }

    public void setTabName(String tabName)
    {
        this.tabName = tabName;
    }

    public String getCloName()
    {
        return cloName;
    }

    public void setCloName(String cloName)
    {
        this.cloName = cloName;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public int getDataLength()
    {
        return dataLength;
    }

    public void setDataLength(int dataLength)
    {
        this.dataLength = dataLength;
    }

    public String getDataPrecision()
    {
        return dataPrecision;
    }

    public void setDataPrecision(String dataPrecision)
    {
        this.dataPrecision = dataPrecision;
    }

    public String getDataScale()
    {
        return dataScale;
    }

    public void setDataScale(String dataScale)
    {
        this.dataScale = dataScale;
    }

    public String getDataDefault()
    {
        return dataDefault;
    }

    public void setDataDefault(String dataDefault)
    {
        this.dataDefault = dataDefault;
    }

    public boolean isNullable()
    {
        return nullable;
    }

    public void setNullable(boolean nullable)
    {
        this.nullable = nullable;
    }

    @Override
    public String toString()
    {
        return this.cloName;
    }
}
