/* ------------------------------------------------
 *
 * File: DataTypeCastDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\DataTypeCastDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum;
import com.highgo.hgdbadmin.migrationassistant.util.ObjEnum.DBType;

/**
 *
 * @author Liu Yuanyuan
 */
public class DataTypeCastDTO
{
    ObjEnum.DBType sourceDB;
    DataTypeDTO sourceDataType;
    DataTypeDTO hgDataType;
    boolean isSystem;

    public DBType getSourceDB()
    {
        return sourceDB;
    }

    public void setSourceDB(DBType sourceDB)
    {
        this.sourceDB = sourceDB;
    }

    public DataTypeDTO getSourceDataType()
    {
        return sourceDataType;
    }

    public void setSourceDataType(DataTypeDTO sourceDataType)
    {
        this.sourceDataType = sourceDataType;
    }

    public DataTypeDTO getHgDataType()
    {
        return hgDataType;
    }

    public void setHgDataType(DataTypeDTO hgDataType)
    {
        this.hgDataType = hgDataType;
    }

    public boolean isIsSystem()
    {
        return isSystem;
    }

    public void setIsSystem(boolean isSystem)
    {
        this.isSystem = isSystem;
    }
}
