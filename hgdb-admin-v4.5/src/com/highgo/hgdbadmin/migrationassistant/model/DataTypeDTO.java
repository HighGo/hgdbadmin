/* ------------------------------------------------
 *
 * File: DataTypeDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\migrationassistant\model\DataTypeDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.migrationassistant.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class DataTypeDTO
{
//    private ObjEnum.DBType dbType;
    private String dataType;
    private int dataPrecision;
    private int dataScale;

//    public ObjEnum.DBType getDbType()
//    {
//        return dbType;
//    }
//
//    public void setDbType(ObjEnum.DBType dbType)
//    {
//        this.dbType = dbType;
//    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public int getDataPrecision()
    {
        return dataPrecision;
    }

    public void setDataPrecision(int dataPrecision)
    {
        this.dataPrecision = dataPrecision;
    }

    public int getDataScale()
    {
        return dataScale;
    }

    public void setDataScale(int dataScale)
    {
        this.dataScale = dataScale;
    }

    @Override
    public String toString()
    {
        return this.dataType;
    }
}
