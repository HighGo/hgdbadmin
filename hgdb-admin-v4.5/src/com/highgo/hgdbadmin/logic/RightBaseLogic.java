/* ------------------------------------------------
 *
 * File: RightBaseLogic.java
 *
 * Abstract:
 *       .
 *
 * Authored by sunqiankun@highgo.com，2019
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\logic\RightBaseLogic.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.logic;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * the feature of this class isn't needed for this version
 * @author sunqk at 2019.06.03
 */
@Deprecated
public class RightBaseLogic {
    private final Logger logger;
    private final AbstractObject obj;
    public RightBaseLogic(AbstractObject inObj)
    {
        this.logger = LogManager.getLogger(getClass());
        this.obj = inObj;
    }
    
    /*
     get information_schema.table_privileges
    */
    private boolean getGrantInfo(String strTableName, String strTableOwner, String strLoginUser) throws Exception
    {
        logger.info("invoke begin rightBaseLogic::getGrantInfo");
        HelperInfoDTO helperInfo = obj.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("rightBaseLogic::getGrantInfo invoke end helperInfo is null, do nothing, return.");
            throw new Exception("connectInfo is null, do nothing, return.");
        }
        
        //get dbnames
        String strDB;
        if (obj instanceof DBInfoDTO)
        {
            strDB = helperInfo.getMaintainDB();
        } else
        {
            strDB = helperInfo.getDbName();
        }
        logger.info("dbname:" + strDB);
        
        //build sql select info
        StringBuilder strBuildSql = new StringBuilder();
        strBuildSql.append("SELECT GRANTEE, PRIVILEGE_TYPE FROM INFORMATION_SCHEMA.TABLE_PRIVILEGES T WHERE T.TABLE_NAME = '");
        strBuildSql.append(strTableName);
        strBuildSql.append("' AND T.GRANTEE <> '");
        strBuildSql.append(strTableOwner);
        strBuildSql.append("'");
        logger.info("dump select information_schema.table_privileges:" + strBuildSql.toString());
        
        //connect info
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try
        {
            //connect and get data
            String strConnURL = helperInfo.getPartURL() + strDB + "?preferQueryMode=simple";
            logger.info("connect database urlinfo:" + strConnURL);
            conn = JdbcHelper.getConnection(strConnURL, helperInfo);
            stmt = conn.createStatement();
            stmt.setFetchSize(100);
            logger.info("fetchSize=" + stmt.getFetchSize());
            rs = stmt.executeQuery(strBuildSql.toString());
            while (rs.next())
            {
                //check
                logger.info("GRANTEE:" + rs.getString(1));
                logger.info("grant type:" + rs.getString(2));
                if(strLoginUser.equals(rs.getString(1)) && rs.getString(2).equals("SELECT"))
                {
                    logger.info("invoke end rightBaseLogic::getGrantInfo return true");
                    JdbcHelper.close(stmt);
                    JdbcHelper.close(conn);
                    return true;
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.debug("close stream");
        }

        logger.info("invoke end rightBaseLogic::getGrantInfo return false");
        return false;
    }
    
    /*
     check login user have right to do
    */
    public boolean chkLoginUserViewTabelData() throws Exception
    {
        logger.info("invoke begin rightBaseLogic::chkLoginUserViewTabelData");
        String strOwnerUser = obj.getOwner();
        HelperInfoDTO helperInfo = obj.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("rightBaseLogic::getGrantInfo invoke end helperInfo is null, do nothing, return.");
            throw new Exception("connectInfo is null, do nothing, return.");
        }
        
        logger.info("tablename:" + obj.getName());
        String strLoginUser = helperInfo.getUser();
        logger.info("login user:" + strLoginUser + "table:" + obj.getName() + "'s owner:" + strOwnerUser);
        
        //same user return true
        if(strLoginUser.equals(strOwnerUser))
        {
            logger.info("invoke end rightBaseLogic::chkLoginUserViewTabelData same user return true");
            return true;
        }
        
        //chk if have grant
        return getGrantInfo(obj.getName(), strOwnerUser, strLoginUser);
    }
}
