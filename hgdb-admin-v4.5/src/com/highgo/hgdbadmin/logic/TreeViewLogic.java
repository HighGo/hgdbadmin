/* ------------------------------------------------
 *
 * File: TreeViewLogic.java
 *
 * Abstract:
 *       .
 *
 * Authored by sunqiankun@highgo.com，2019
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\logic\TreeViewLogic.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.logic;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.Date;

import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import com.highgo.hgdbadmin.view.TreeView;
import com.highgo.hgdbadmin.view.dialog.PwdRolvalidUntil;
import javax.swing.JFrame;

/**
 * the feature of this class isn't needed for this version
 * @author sunqiankun
 */
@Deprecated
public class TreeViewLogic {
    private final Logger logger;
    private final ResourceBundle constBundle;
    private final JFrame treeLogicParent;
    private final TreeView treeLogicObject;
    private final ResourceBundle msgBundle;
    public TreeViewLogic(TreeView logicObject, JFrame logicObjectParent)
    {
        this.treeLogicParent = logicObjectParent;
        this.msgBundle = ResourceBundle.getBundle("messages");
        this.constBundle = ResourceBundle.getBundle("constants");
        this.logger = LogManager.getLogger(getClass());       
        this.treeLogicObject = logicObject;
        logger.info("init treeViewLogice");
    }
    
    //修改密码
    public void DoChangePassWD(XMLServerInfoDTO serverInfo, Connection connect, Timestamp outValidData)
    {
        //获取当前时间, 这个地方其实是有bug的。 应该获服务器的时间
        String  message1 = null;
        boolean is_success = true;
        while(is_success)
        {
            if(!serverInfo.isConnected())
            {
                is_success = false;
                 continue;
            }
            message1 = MessageFormat.format(msgBundle.getString("userrolvaliduntilOut"), serverInfo.getUser(), outValidData);
            logger.info(message1);
            PwdRolvalidUntil rolvaliduntil =  new PwdRolvalidUntil(treeLogicParent, true, message1, true);
            rolvaliduntil.setLocationRelativeTo(treeLogicParent);
            rolvaliduntil.setVisible(true);
            if(rolvaliduntil.isSuccess())
            {
                logger.info(serverInfo.getUser()+"-----------------------------" + rolvaliduntil.getPassword() + "++++++" + rolvaliduntil.getConPassword());
                if(rolvaliduntil.getPassword().length() >= 8 && rolvaliduntil.getConPassword().length() >= 8 && rolvaliduntil.getConPassword().equals(rolvaliduntil.getPassword()))
                {

                    StringBuilder sql_pw = new StringBuilder();
                    PreparedStatement stmt_pw = null;
                   // ResultSet rs_pw = null;

                    sql_pw.append("ALTER user ").append(serverInfo.getUser()).append(" with password '").append(rolvaliduntil.getPassword()).append("'");
                    try {
                        stmt_pw = connect.prepareStatement(sql_pw.toString());
                        logger.info("------SQL: " + sql_pw.toString());
                        int t_sql = stmt_pw.executeUpdate();
                        logger.info(t_sql + "------------------------------0004" + serverInfo.getUser() );
                        String warn_pw = null;
                        if(t_sql == 0)
                        {
                            warn_pw = "密码修改成功！";
                            is_success = false;
                             logger.warn(warn_pw);
                        }

                        JOptionPane.showMessageDialog(treeLogicParent, warn_pw,
                        constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);
                        } catch (Exception e) {
                           logger.info("--------------try  catch  ---357");
                           String warn_pw = "密码修改失败！"
                                   + "(1) 密码必须同时含有字母和数字且至少为8位有效字符；"
                                   + "(2) 修改密码不能与上次密码相同；";
                            JOptionPane.showMessageDialog(treeLogicParent, warn_pw,
                        constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);
                    }
                }
                else
                {

                    String str = "密码修改失败！"
                            + "(1) 密码必须同时含有字母和数字且至少为8位有效字符；"
                            + "(2) 修改密码不能与上次密码相同；";
                    JOptionPane.showMessageDialog(treeLogicParent, str,
                    constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);

                }
            }
        }
    }
    
    //获取密码
    private void GetPassWDFromPG(XMLServerInfoDTO serverInfo, Connection connect)
    {
        //1、连接数据库
        //2、获取系统时间
        try
        {
            //获取系统当前时间
            StringBuilder strSqlNow = new StringBuilder();
            Timestamp tsDBNow = null;
            strSqlNow.append("SELECT now();");
            PreparedStatement psExecSql = null;
            ResultSet rsInfo = null;
            psExecSql = connect.prepareStatement(strSqlNow.toString());
            rsInfo = psExecSql.executeQuery();
            try
            {
                //密码过期时间          
                if(rsInfo.next())
                {
                    tsDBNow = rsInfo.getTimestamp("now");
                }
            }
            catch (SQLException ex)
            {
                this.DealException(ex);
            }
            
            //获取过期时间
            StringBuilder strSqlOutDate = new StringBuilder();
            strSqlOutDate.append("SELECT rolvaliduntil FROM pg_roles where rolname = '").append(serverInfo.getUser()).append("';");
            Timestamp tsUntil = null;
            psExecSql = connect.prepareStatement(strSqlOutDate.toString());
            rsInfo = psExecSql.executeQuery();
            try
            {
                //密码过期时间          
                if(rsInfo.next())
                {
                       tsUntil = rsInfo.getTimestamp("rolvaliduntil");
                }
                else
                {
                    Date date = new Date(); 
                    tsUntil = new Timestamp(date.getTime());
                }
            }
            catch (Exception ex) 
            {
                this.DealException(ex);
            }
            //here's a null poniter exception when tsUNtil is null()
            logger.info(" until_time = " + tsUntil);
            if(tsUntil!=null && tsUntil.compareTo(tsDBNow) < 0)
            {
                DoChangePassWD(serverInfo, connect, tsUntil);
            }
            //JdbcHelper.close(connect);//move this to finally
        }
        catch (SQLException ex)
        {
            this.DealException(ex);
        }finally
        {
            JdbcHelper.close(connect);
        }
    }    
    
    //校验是否需要修改密码
    public void ChangePassWD(XMLServerInfoDTO serverInfo)
    {
        try
        {
            Connection connect = null;
            String url = serverInfo.getPartURL() + serverInfo.getMaintainDB();
            connect = JdbcHelper.getConnection(url, serverInfo);
            GetPassWDFromPG(serverInfo, connect);
        }
        catch(SQLException e)
        {
            DealSQLException(e);
        }
        catch(ClassNotFoundException e)
        {
            logger.info("close connection");
        }
    }
    
    //异常处理
    private void DealException(Exception ex)
    {
        String msg = ex.getMessage();
        logger.info(msg);
        ex.printStackTrace(System.out);
        if (msg != null && !msg.isEmpty())
        {
            msg = msg.replaceAll("[\\,\\ S;.!?\"] +", "." + System.lineSeparator());
        }
        JOptionPane.showMessageDialog(treeLogicObject, msg, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
    }

    //never come here
    private void DealSQLException(SQLException ex) {
        this.DealException(ex);
    }
    
}
