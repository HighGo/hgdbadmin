/* ------------------------------------------------ 
* 
* File: OptionController.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/OptionController.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.optioneditor.controller;

import com.highgo.hgdbadmin.util.CloseStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Liu Yuanyuan
 */
public class OptionController
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());

    private final static String DocBasePath = "doc" + File.separator;//(new File("")).getAbsolutePath() + File.separator + "doc" + File.separator
    public final static String HgdbHelpPath = DocBasePath + "hgdb_user_manual.pdf";
    public final static String HgdbAdminPath = DocBasePath + "hgdb_admin_manual.pdf";
    
    private final String OPTION_PATH = "conf" + File.separator + "option.properties";
    private final String DEFAULT_FAVOURITE = "conf" + File.separator + "highgo-favourites.xml";
    private final String DEFAULT_HISQUERY = "conf" + File.separator + "highgo-histoqueries.xml";
    private final String DEFAULT_MACRO = "conf" + File.separator + "highgo-macros.xml";
    private final String DEFAULT_HISQUERY_COUNT = "10";
    private final String DEFAULT_HISQUERY_SIZE = "50";
    
    private final String DEFAULT_INACTIVITY_TIMEOUT = "30";
    
    private static OptionController oc = null;
    public static OptionController getInstance()
    {
        if (oc == null)
        {
            oc = new OptionController();
        }
        return oc;
    }

     
    public Properties getOptionProperties() throws Exception
    {
        return this.getPropertiesFromFile(OPTION_PATH);
    }    
    public Properties getPropertiesFromFile(String filePath) throws Exception
    {
        logger.info("filePath=" + filePath);
        Properties prop = null;
        File file = new File(filePath);
        logger.info("file exist=" + file.exists());
        InputStream is = null;
        try
        {
            if (!file.exists())
            {
                file.createNewFile();
            }
            is = new FileInputStream(file);
            prop = new Properties();
            prop.load(is);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(is);
            logger.debug("close stream");
        }
        return prop;
    }
        
    
    public void savePropertiesToOption(Properties prop) throws Exception
    {
        this.savePropertiesToFile(prop, OPTION_PATH);
    }
    public void savePropertiesToFile(Properties prop, String filePath) throws Exception
    {
        logger.info(filePath);
        if (prop == null)
        {
            logger.error("properties is null, do nothing and return.");
            return;
        }        
        OutputStream fos = null;
        try
        {
            fos = new FileOutputStream(filePath);
            prop.store(fos, "Update " + filePath);//update file            
            PropertyConfigurator.configure(prop);//update configuration            
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(fos);
            logger.debug("close stream");
        }
    }

    
    public String getPropertyValue(Properties prop, String key)
    {
        String value = prop.getProperty(key);
        switch (key)
        {
            case "favourite_file":
                return value == null ? DEFAULT_FAVOURITE : value;
            case "macro_file":
                return value == null ? DEFAULT_MACRO : value;
            case "history_query_file":
                return value == null ? DEFAULT_HISQUERY : value;
            case "query_max_count":
                return value == null ? DEFAULT_HISQUERY_COUNT : value;
            case "query_max_size":
                return value == null ? DEFAULT_HISQUERY_SIZE : value;
            case "inactivity_timeout":
                return value == null ? DEFAULT_INACTIVITY_TIMEOUT : value;
            case "view_data":
                return value == null ? "false" : value;
            case "set_schema":
                return value == null ? "false" : value;
            case "with_oracle_prefix":
                 return value == null ? "true" : value;
            case "compatable_money":
                 return value == null ? "false" : value;
            case "compatable_percent":
                return value == null ? "false" : value;
            default:
                return value == null? null : value.trim();
        }  
    }
    
}
