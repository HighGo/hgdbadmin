/* ------------------------------------------------ 
* 
* File: OptionModel.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\optioneditor\model\OptionModel.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.optioneditor.model;

/**
 *
 * @author Administrator
 */
public class OptionModel
{
    private String name ;
    private Object value;
    private boolean change;
    private String text;
    private int lineNum;

    public int getLineNum()
    {
        return lineNum;
    }

    public void setLineNum(int lineNum)
    {
        this.lineNum = lineNum;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }
    public boolean isChange()
    {
        return change;
    }

    public void setChange(boolean change)
    {
        this.change = change;
    }
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }
    
}
