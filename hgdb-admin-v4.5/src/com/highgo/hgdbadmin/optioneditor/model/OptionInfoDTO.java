/* ------------------------------------------------ 
* 
* File: OptionInfoDTO.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\optioneditor\model\OptionInfoDTO.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.optioneditor.model;

/**
 *
 * @author Liu Yuanyuan
 * 
 */
@Deprecated
public class OptionInfoDTO
{    
   private String dataDir;
   private String binDir;
   private String favouriteDir;
   private String macroDir;
   private String historyQueryDir;
   private String logDir;
   private String hgHelpUrl;
   private String pgHelpUrl; 
   
    public String getDataDir()
    {
        return dataDir;
    }

    public void setDataDir(String dataDir)
    {
        this.dataDir = dataDir;
    }

    public String getBinDir()
    {
        return binDir;
    }

    public void setBinDir(String binDir)
    {
        this.binDir = binDir;
    }

    public String getFavouriteDir()
    {
        return favouriteDir;
    }

    public void setFavouriteDir(String favouriteDir)
    {
        this.favouriteDir = favouriteDir;
    }

    public String getMacroDir()
    {
        return macroDir;
    }

    public void setMacroDir(String macroDir)
    {
        this.macroDir = macroDir;
    }

    public String getHistoryQueryDir()
    {
        return historyQueryDir;
    }

    public void setHistoryQueryDir(String historyQueryDir)
    {
        this.historyQueryDir = historyQueryDir;
    }
   
    public String getLogDir()
    {
        return logDir;
    }

    public void setLogDir(String logDir)
    {
        this.logDir = logDir;
    }
    
    public String getHgHelpUrl()
    {
        return hgHelpUrl;
    }

    public void setHgHelpUrl(String hgHelpUrl)
    {
        this.hgHelpUrl = hgHelpUrl;
    }

    public String getPgHelpUrl()
    {
        return pgHelpUrl;
    }

    public void setPgHelpUrl(String pgHelpUrl)
    {
        this.pgHelpUrl = pgHelpUrl;
    }
}
