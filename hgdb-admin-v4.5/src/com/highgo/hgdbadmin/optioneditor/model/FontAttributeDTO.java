/* ------------------------------------------------ 
* 
* File: FontAttributeDTO.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\optioneditor\model\FontAttributeDTO.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.optioneditor.model;

import java.awt.Color;
import java.awt.Font;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Yuanyuan
 */
public class FontAttributeDTO extends SimpleAttributeSet
{

    public void setFontFamily(String fontFamily)
    {
        StyleConstants.setFontFamily(this, fontFamily);
    }
    public String getFontFamily()
    {
        return StyleConstants.getFontFamily(this);
    }
    
    public void setBold(boolean isBold)
    {
        StyleConstants.setBold(this, isBold);
    }
    public boolean isBold()
    {
        return StyleConstants.isBold(this);
    }
      
    public void setItalic(boolean isItalic)
    {
        StyleConstants.setItalic(this, isItalic);
    }
    public boolean isItalic()
    {
        return StyleConstants.isItalic(this);
    }

    public int getStyle()
    {
        if (this.isBold() && this.isItalic())
        {
            return Font.BOLD + Font.ITALIC;
        } else if (this.isBold())
        {
            return Font.BOLD;
        } else if (this.isItalic())
        {
            return Font.ITALIC;
        } else
        {
            return Font.PLAIN;
        }
    }
    
    public void setFontSize(int size)
    {
         StyleConstants.setFontSize(this,size);
    }
    public int getFontSize()
    {
        return StyleConstants.getFontSize(this);
    }
    
    
    public void setStrikeThrough(boolean isStrikeThrough)
    {
        StyleConstants.setStrikeThrough(this, isStrikeThrough);
    }
    public boolean isStrikeThrough()
    {
        return StyleConstants.isStrikeThrough(this);
    }
    
    public void setUnderline(boolean isUnderLine)
    {
        StyleConstants.setUnderline(this, isUnderLine);
    }
    public boolean isUnderLine()
    {
        return StyleConstants.isUnderline(this);
    }
    
    public void setForeground(Color color)
    {
        StyleConstants.setForeground(this,color);
    }
    public Color getForeground()
    {
        return StyleConstants.getForeground(this);
    }
    
    public void setBackground(Color color)
    {
        StyleConstants.setBackground(this, color);
    }
    public Color getBackground()
    {
        return StyleConstants.getForeground(this);
    }
    
    public String getText()
    {
        return this.getFontFamily()+","+this.getFontSize();
    }
    
}
