/* ------------------------------------------------ 
* 
* File: OptionEditView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\optioneditor\view\OptionEditView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.optioneditor.view;

import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.optioneditor.model.FontAttributeDTO;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import com.highgo.hgdbadmin.util.PathChooserDialog;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.MessageFormat;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 *
 * @author Liu Yuanyuan
 */
public class OptionEditView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(this.getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
      
    private CardLayout card;    
    private Properties prop = null;
    private FontAttributeDTO queryEditorFontAttribute = null;
    
    private Level logLevel = null;
    //private Properties logProp = null;
    //private String log4jPath = "./src/log4j.properties";
        
    /**
     * Creates new form ServerAddView
     * @param parent
     * @param modal
     */
    public OptionEditView(JFrame parent, boolean modal)
    {
        super(parent, modal);
        card = new CardLayout();
        initComponents();
        customization();  
        //color
        MouseAdapter colorMouseAdapter = new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent evt)
            {
                setColor(evt);
            }
        };
        txfdBackground.addMouseListener(colorMouseAdapter);
        txfdForeground.addMouseListener(colorMouseAdapter);
        txfdMarginBackground.addMouseListener(colorMouseAdapter);
        txfdCaretColor.addMouseListener(colorMouseAdapter);
        txfdMultilineComment.addMouseListener(colorMouseAdapter);
        txfdSingleLineComment.addMouseListener(colorMouseAdapter);
        txfdSqlDoc.addMouseListener(colorMouseAdapter);
        txfdNumber.addMouseListener(colorMouseAdapter);
        txfdCustomBackground.addMouseListener(colorMouseAdapter);
        txfdDoubleQuotedString.addMouseListener(colorMouseAdapter);
        txfdSingleQuotedString.addMouseListener(colorMouseAdapter);
        txfdOperator.addMouseListener(colorMouseAdapter);
        txfdIdentifer.addMouseListener(colorMouseAdapter);
        txfdIdleProcessColor.addMouseListener(colorMouseAdapter);
        txfdActiveProcessColor.addMouseListener(colorMouseAdapter);
        txfdSlowProcessColor.addMouseListener(colorMouseAdapter);
        txfdBlockedProcessColor.addMouseListener(colorMouseAdapter);
        //display obj
        btnDefaultObjDisplay.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                initObjectDisplayStatus();
            }
        });
        /**********************************************************/        
        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnHelpActionPerformed(e);
            }
        });
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancleActionPerformed(e);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformed(e);
            }
        });

        initSetting();        
        
        //content changed by key type or paste
        txfdHGDataPath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdContentChanged((JTextField) e.getSource(), "data_path");
            }      
        });  
        txfdHGBinPath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdContentChanged((JTextField) e.getSource(), "bin_path");
            }      
        });
        txfdScriptPath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdContentChanged((JTextField) e.getSource(), "script_path");
            }
        });
        txfdHistoryFilePath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdContentChanged((JTextField) e.getSource(), "history_query_file");
            }
        });
        txfdFavouriteFilePath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdContentChanged((JTextField) e.getSource(), "favourite_file");
            }
        });
        txfdMacroFilePath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdContentChanged((JTextField) e.getSource(), "macro_file");
            }
        });
        
        //file
        btnBrowseHistoryQuery.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseHistoryQueryFileAction(e);
            }
        });
        btnBrowseMacrosFile.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseMacrosFileAction(e);
            }
        });
        btnBrowseFavouriteFile.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFavouriteFileAction(e);
            }
        });
        //data path      
        btnHGDataPath.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseDataPath(e);
            }
        });
        //bin path
        btnHGBinPath.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseBinPath(e);
            }
        });
        //script path
        btnScriptPath.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseScriptPath(e);
            }
        });
        //help url
        txfdPgHelpPath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdDBHelpUrlKeyReleased();
            }
        });
        txfdPgAdminHelpPath.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                txfdAdminHelpUrlKeyReleased();
            }
        });
        
        //Font
        ActionListener fontAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnFontForUIActionPerformed(evt);
            }
        };
        btnUIFont.addActionListener(fontAction);
        btnDBDesignerFont.addActionListener(fontAction);  
        btnQueryEditorFont.addActionListener(fontAction);
   
        //log action
        ActionListener logLevelAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                logLevelAction(e);
            }
        };
        rbNoLog.addActionListener(logLevelAction);
        rbErrorOnly.addActionListener(logLevelAction);
        rbErrorWarn.addActionListener(logLevelAction);
        rbErrorWarnInfo.addActionListener(logLevelAction);
        rbDebugNotForNormal.addActionListener(logLevelAction);
//        btnBrowseLogFile.addActionListener(new ActionListener()
//        {
//            @Override
//            public void actionPerformed(ActionEvent e)
//            {
//                chooseLogFileAction(e);
//            }
//        });
        
        spinInActivityTimeout.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                spinnerValueChanged(spinInActivityTimeout, "inactivity_timeout");
            }
        });
        
    }

    private void initSetting()
    {
        //path
        OptionController oc = OptionController.getInstance();
        try
        {
            prop = oc.getOptionProperties();
            //db binary path
            txfdHGDataPath.setText(prop.getProperty("data_path"));
            txfdHGBinPath.setText(prop.getProperty("bin_path"));
            txfdScriptPath.setText(prop.getProperty("script_path"));//for console

            //query tool
            /*
            //favourite
            txfdFavouriteFilePath.setText(oc.getPropertyValue(prop, "favourite_file"));
            //macro
            txfdMacroFilePath.setText(oc.getPropertyValue(prop, "macro_file"));
            //query history
            txfdHistoryFilePath.setText(oc.getPropertyValue(prop, "history_query_file"));
            */
            spMaxQueryCount.setValue(Integer.valueOf(oc.getPropertyValue(prop, "query_max_count")));
            spMaxQuerySize.setValue(Integer.valueOf(oc.getPropertyValue(prop, "query_max_size")));       
            
            //help path
            txfdHgdbManual.setText(OptionController.HgdbHelpPath);
            txfdHgdbManual.setEditable(false);
            txfdHgdbAdminManual.setText(OptionController.HgdbAdminPath);
            txfdHgdbAdminManual.setEditable(false);

            txfdPgHelpPath.setText(oc.getPropertyValue(prop, "db_help_url"));
            txfdPgHelpPath.setEditable(false);
            txfdPgAdminHelpPath.setText(oc.getPropertyValue(prop, "admin_help_url"));
            txfdPgAdminHelpPath.setEditable(false);
            
            //query editor
            String fontSetting = oc.getPropertyValue(prop,"query_editor_font");
            if (fontSetting != null && !fontSetting.isEmpty())
            {
                String[] f = fontSetting.split(",");
                queryEditorFontAttribute = new FontAttributeDTO();
                queryEditorFontAttribute.setFontFamily(f[0].trim());
                int style = Integer.valueOf(f[1].trim());
                queryEditorFontAttribute.setBold(style == Font.BOLD || style == (Font.BOLD + Font.ITALIC));
                queryEditorFontAttribute.setItalic(style == Font.ITALIC || style == (Font.BOLD + Font.ITALIC));
                queryEditorFontAttribute.setFontSize(Integer.valueOf(f[2].trim()));
                queryEditorFontAttribute.setForeground(new Color(Integer.valueOf(f[3].trim())));
                btnQueryEditorFont.setText(queryEditorFontAttribute.getText());
            }

            spinInActivityTimeout.setValue(Integer.valueOf(oc.getPropertyValue(prop, "inactivity_timeout")));
            
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), 
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }        
        
        //log level        
        this.setLogLevel();   
        //log file path
        /*try
        {
            logProp = oc.getPropertiesFromFile(log4jPath);
            txfdLogPath.setText(logProp.getProperty("log4j.appender.FILE.File"));   
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), 
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }*/
    }
    private void setLogLevel()
    {
        String loglevel =  LogManager.getRootLogger().getLevel().toString().trim();
        logger.debug("loglevel=" + loglevel);
        switch (loglevel)
        {
            case "OFF":
                rbNoLog.setSelected(true);
                break;
            case "ERROR":
                rbErrorOnly.setSelected(true);
                break;
            case "WARN":
                rbErrorWarn.setSelected(true);
                break;
            case "INFO":
                rbErrorWarnInfo.setSelected(true);
                break;
            case "DEBUG":
                rbDebugNotForNormal.setSelected(true);
                break;
            default:
                logger.info(loglevel + " is an Exception log level.");
                break;
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pnlDisplay = new javax.swing.JPanel();
        lblDisplayObjects = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblObjects = new javax.swing.JTable();
        btnDefaultObjDisplay = new javax.swing.JButton();
        pnlProperty = new javax.swing.JPanel();
        lblRowCountLimit1 = new javax.swing.JLabel();
        txfdRowCountLimit1 = new javax.swing.JTextField();
        pnlBinPath = new javax.swing.JPanel();
        lblSlonyIPath = new javax.swing.JLabel();
        txfdSlonyIPath = new javax.swing.JTextField();
        btnSlonyIPath = new javax.swing.JButton();
        btnHGBinPath = new javax.swing.JButton();
        lblHGBinPath = new javax.swing.JLabel();
        txfdHGBinPath = new javax.swing.JTextField();
        lblGPBinPath = new javax.swing.JLabel();
        btnGPBinPath = new javax.swing.JButton();
        txfdGPBinPath = new javax.swing.JTextField();
        lblDataPath = new javax.swing.JLabel();
        txfdHGDataPath = new javax.swing.JTextField();
        btnHGDataPath = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        btnScriptPath = new javax.swing.JButton();
        lblScript = new javax.swing.JLabel();
        txfdScriptPath = new javax.swing.JTextField();
        pnlUIMiscellaneous = new javax.swing.JPanel();
        lblFont1 = new javax.swing.JLabel();
        btnUIFont = new javax.swing.JButton();
        lblRefRefreshOnClick = new javax.swing.JLabel();
        cbShowUsersForPrivilege = new javax.swing.JCheckBox();
        cbShowPropertyOnDoubleClick = new javax.swing.JCheckBox();
        cbShowNotice = new javax.swing.JCheckBox();
        cbConfirmObjectDeletion = new javax.swing.JCheckBox();
        cbShowSysObjInTreeView = new javax.swing.JCheckBox();
        jComboBox1 = new javax.swing.JComboBox();
        pnlQueryEditor = new javax.swing.JPanel();
        lblQueryEditorFont = new javax.swing.JLabel();
        txfdMaxCharPerColumn = new javax.swing.JTextField();
        lblMaxCharPerColumn = new javax.swing.JLabel();
        btnQueryEditorFont = new javax.swing.JButton();
        txfdIndentChar = new javax.swing.JTextField();
        lblIndentChar = new javax.swing.JLabel();
        lblUseNullStrInstaedOfNullResult = new javax.swing.JLabel();
        lblCopySQLFromMainToQueryTool = new javax.swing.JLabel();
        lblEnableAutoRollBack = new javax.swing.JLabel();
        lblKeyWordsInUppercase = new javax.swing.JLabel();
        cbUseNullInsteadOfNull = new javax.swing.JCheckBox();
        cbCopySQLFromMainToQueryTool = new javax.swing.JCheckBox();
        cbEnableAutoRollBack = new javax.swing.JCheckBox();
        cbKeyWordsInUppercase = new javax.swing.JCheckBox();
        pnlColor = new javax.swing.JPanel();
        pnlSqlSyntaxHighlight = new javax.swing.JPanel();
        cbUseSysBackground = new javax.swing.JCheckBox();
        cbUseSysForeground = new javax.swing.JCheckBox();
        lblCustomBackground = new javax.swing.JLabel();
        lblCustomForeground = new javax.swing.JLabel();
        lblMarginBackground = new javax.swing.JLabel();
        txfdBackground = new javax.swing.JTextField();
        txfdForeground = new javax.swing.JTextField();
        txfdMarginBackground = new javax.swing.JTextField();
        pnlCaret = new javax.swing.JPanel();
        lblCaret = new javax.swing.JLabel();
        txfdCaretColor = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        lblMultlineComment = new javax.swing.JLabel();
        txfdMultilineComment = new javax.swing.JTextField();
        lblDoubleQuotedString = new javax.swing.JLabel();
        txfdDoubleQuotedString = new javax.swing.JTextField();
        lblSinglelineComment = new javax.swing.JLabel();
        txfdSingleLineComment = new javax.swing.JTextField();
        lblSQLDoc = new javax.swing.JLabel();
        txfdSqlDoc = new javax.swing.JTextField();
        txfdNumber = new javax.swing.JTextField();
        lblNumber = new javax.swing.JLabel();
        txfdCustomBackground = new javax.swing.JTextField();
        lblKeyword = new javax.swing.JLabel();
        txfdSingleQuotedString = new javax.swing.JTextField();
        lblSingleQuotedString = new javax.swing.JLabel();
        txfdOperator = new javax.swing.JTextField();
        lblOperator = new javax.swing.JLabel();
        lblIdentifier = new javax.swing.JLabel();
        txfdIdentifer = new javax.swing.JTextField();
        pnlResultGrid = new javax.swing.JPanel();
        lblResultCopyQuoting = new javax.swing.JLabel();
        lblResultCopyQuotChar = new javax.swing.JLabel();
        lblResultCopyFieldSeparator = new javax.swing.JLabel();
        lblShowNullValues = new javax.swing.JLabel();
        lblThousandSeparator = new javax.swing.JLabel();
        lblDecimalMark = new javax.swing.JLabel();
        lblCopyColumnNames = new javax.swing.JLabel();
        cbShowNullValues = new javax.swing.JCheckBox();
        cbCopyColumnNames = new javax.swing.JCheckBox();
        txfdThousandSeparator = new javax.swing.JTextField();
        txfdDecimalMark = new javax.swing.JTextField();
        cbResultCopyQuoting = new javax.swing.JComboBox();
        cbResultCopyQuotChar = new javax.swing.JComboBox();
        cbResultCopyFieldSeparator = new javax.swing.JComboBox();
        pnlQueryFile = new javax.swing.JPanel();
        cbReadWriteFile = new javax.swing.JCheckBox();
        cbWriteBomForUtfFile = new javax.swing.JCheckBox();
        cbExitNotPromptSave = new javax.swing.JCheckBox();
        pnlFavourite = new javax.swing.JPanel();
        lblFavouriteFilePath = new javax.swing.JLabel();
        txfdFavouriteFilePath = new javax.swing.JTextField();
        btnBrowseFavouriteFile = new javax.swing.JButton();
        pnlMacros = new javax.swing.JPanel();
        lblMacroFilePath = new javax.swing.JLabel();
        txfdMacroFilePath = new javax.swing.JTextField();
        btnBrowseMacrosFile = new javax.swing.JButton();
        pnlHistoryFile = new javax.swing.JPanel();
        lblHistoryFilePath = new javax.swing.JLabel();
        txfdHistoryFilePath = new javax.swing.JTextField();
        btnBrowseHistoryQuery = new javax.swing.JButton();
        lblMaxQueryToStore = new javax.swing.JLabel();
        lblMaxSizeOfStoredQuery = new javax.swing.JLabel();
        spMaxQueryCount = new javax.swing.JSpinner();
        spMaxQuerySize = new javax.swing.JSpinner();
        pnlDBDesigner = new javax.swing.JPanel();
        lblDBDesignerFont = new javax.swing.JLabel();
        btnDBDesignerFont = new javax.swing.JButton();
        pnlServerStatus = new javax.swing.JPanel();
        lblIdleProcessColor = new javax.swing.JLabel();
        txfdIdleProcessColor = new javax.swing.JTextField();
        lblActiveProcessColor = new javax.swing.JLabel();
        txfdActiveProcessColor = new javax.swing.JTextField();
        txfdSlowProcessColor = new javax.swing.JTextField();
        lblSlowProcessColor = new javax.swing.JLabel();
        txfdBlockedProcessColor = new javax.swing.JTextField();
        lblBlockedProcessColor = new javax.swing.JLabel();
        pnlUserInterface = new javax.swing.JPanel();
        lblSysSchema = new javax.swing.JLabel();
        txfdSysSchema = new javax.swing.JTextField();
        lblUserLanguage = new javax.swing.JLabel();
        cbUserLanguage = new javax.swing.JComboBox();
        lblInActivityTimeout = new javax.swing.JLabel();
        spinInActivityTimeout = new javax.swing.JSpinner();
        pnlHelpPath = new javax.swing.JPanel();
        lblDBHelpPath = new javax.swing.JLabel();
        txfdHgdbManual = new javax.swing.JTextField();
        lblAdminHelpPath = new javax.swing.JLabel();
        txfdHgdbAdminManual = new javax.swing.JTextField();
        lblGgHelpPath = new javax.swing.JLabel();
        txfdGgHelpPath = new javax.swing.JTextField();
        txfdSlonyHelpPath = new javax.swing.JTextField();
        lblSlonygHelpPath = new javax.swing.JLabel();
        txfdPgHelpPath = new javax.swing.JTextField();
        txfdPgAdminHelpPath = new javax.swing.JTextField();
        lblHgdbManual = new javax.swing.JLabel();
        lblHgdbAdminManual = new javax.swing.JLabel();
        pnlGuruHint = new javax.swing.JPanel();
        cbNotShowGuruHints1 = new javax.swing.JCheckBox();
        cbResetGuruHints1 = new javax.swing.JCheckBox();
        pnlLogging = new javax.swing.JPanel();
        lblLogFile = new javax.swing.JLabel();
        txfdLogPath = new javax.swing.JTextField();
        btnBrowseLogFile = new javax.swing.JButton();
        pnlLogLevel = new javax.swing.JPanel();
        rbNoLog = new javax.swing.JRadioButton();
        rbErrorOnly = new javax.swing.JRadioButton();
        rbErrorWarn = new javax.swing.JRadioButton();
        rbErrorWarnInfo = new javax.swing.JRadioButton();
        rbDebugNotForNormal = new javax.swing.JRadioButton();
        tbnGroupLogLevel = new javax.swing.ButtonGroup();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        pnlButtons = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        pnlContent = new javax.swing.JPanel(card);
        spMenu = new javax.swing.JScrollPane();
        treeMenu = new javax.swing.JTree();

        pnlDisplay.setPreferredSize(new java.awt.Dimension(500, 461));

        lblDisplayObjects.setText(constBundle.getString("displayFollowingObjects")
        );

        Object[][] obj = new Object[][]
        {};
        String[] title = new String[]
        {"isShow","objName"};
        tblObjects.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
        tblObjects.setModel(new DefaultTableModel(obj, title)
            {
                Class[] types = new Class[]
                {Boolean.class, String.class};
                boolean[] canEdit = new boolean[]
                {true, false};
                @Override
                public Class getColumnClass(int columnIndex)
                {
                    return types[columnIndex];
                }
                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex)
                {
                    return canEdit[columnIndex];
                }
            });
            tblObjects.getTableHeader().setVisible(false);
            tblObjects.setColumnSelectionAllowed(true);
            TableColumn firsetColumn = tblObjects.getColumnModel().getColumn(0);
            firsetColumn.setPreferredWidth(30);
            firsetColumn.setMaxWidth(30);
            firsetColumn.setMinWidth(30);
            tblObjects.setEnabled(false);
            tblObjects.setGridColor(javax.swing.UIManager.getDefaults().getColor("Button.background"));
            jScrollPane1.setViewportView(tblObjects);
            tblObjects.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);

            btnDefaultObjDisplay.setText(constBundle.getString("default"));
            btnDefaultObjDisplay.setEnabled(false);

            javax.swing.GroupLayout pnlDisplayLayout = new javax.swing.GroupLayout(pnlDisplay);
            pnlDisplay.setLayout(pnlDisplayLayout);
            pnlDisplayLayout.setHorizontalGroup(
                pnlDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDisplayLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane1)
                        .addGroup(pnlDisplayLayout.createSequentialGroup()
                            .addComponent(lblDisplayObjects)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(btnDefaultObjDisplay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap())
            );
            pnlDisplayLayout.setVerticalGroup(
                pnlDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDisplayLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblDisplayObjects)
                    .addGap(10, 10, 10)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 383, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnDefaultObjDisplay)
                    .addContainerGap())
            );

            lblRowCountLimit1.setText(constBundle.getString("rowCountLimit")
            );

            javax.swing.GroupLayout pnlPropertyLayout = new javax.swing.GroupLayout(pnlProperty);
            pnlProperty.setLayout(pnlPropertyLayout);
            pnlPropertyLayout.setHorizontalGroup(
                pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertyLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblRowCountLimit1)
                    .addGap(10, 10, 10)
                    .addComponent(txfdRowCountLimit1, javax.swing.GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)
                    .addContainerGap())
            );
            pnlPropertyLayout.setVerticalGroup(
                pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertyLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblRowCountLimit1)
                        .addComponent(txfdRowCountLimit1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(300, Short.MAX_VALUE))
            );

            pnlBinPath.setPreferredSize(new java.awt.Dimension(300, 200));

            lblSlonyIPath.setText(constBundle.getString("solnyIPath"));
            lblSlonyIPath.setEnabled(false);

            txfdSlonyIPath.setEnabled(false);

            btnSlonyIPath.setText(constBundle.getString("browse"));
            btnSlonyIPath.setEnabled(false);

            btnHGBinPath.setText(constBundle.getString("browse"));

            lblHGBinPath.setText(constBundle.getString("hgBinPath"));

            lblGPBinPath.setText(constBundle.getString("gpBinPath"));
            lblGPBinPath.setEnabled(false);

            btnGPBinPath.setText(constBundle.getString("browse"));
            btnGPBinPath.setEnabled(false);

            txfdGPBinPath.setEnabled(false);

            lblDataPath.setText(constBundle.getString("hgDataPath"));
            lblDataPath.setEnabled(false);

            txfdHGDataPath.setEnabled(false);

            btnHGDataPath.setText(constBundle.getString("browse"));
            btnHGDataPath.setEnabled(false);

            jTextArea2.setEditable(false);
            jTextArea2.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
            jTextArea2.setColumns(20);
            jTextArea2.setLineWrap(true);
            jTextArea2.setRows(3);
            jTextArea2.setText(constBundle.getString("versionMismatchWarning"));
            jTextArea2.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
            jScrollPane3.setViewportView(jTextArea2);

            btnScriptPath.setText(constBundle.getString("browse"));
            btnScriptPath.setEnabled(false);

            lblScript.setText(constBundle.getString("scriptPath"));
            lblScript.setEnabled(false);

            txfdScriptPath.setEnabled(false);

            javax.swing.GroupLayout pnlBinPathLayout = new javax.swing.GroupLayout(pnlBinPath);
            pnlBinPath.setLayout(pnlBinPathLayout);
            pnlBinPathLayout.setHorizontalGroup(
                pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlBinPathLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlBinPathLayout.createSequentialGroup()
                            .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblHGBinPath, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                .addComponent(lblDataPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(pnlBinPathLayout.createSequentialGroup()
                                    .addGap(2, 2, 2)
                                    .addComponent(lblScript, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGap(10, 10, 10)
                            .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlBinPathLayout.createSequentialGroup()
                                    .addComponent(txfdHGDataPath)
                                    .addGap(10, 10, 10)
                                    .addComponent(btnHGDataPath, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addContainerGap())
                                .addGroup(pnlBinPathLayout.createSequentialGroup()
                                    .addComponent(txfdHGBinPath)
                                    .addGap(10, 10, 10)
                                    .addComponent(btnHGBinPath, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(10, 10, 10))
                                .addGroup(pnlBinPathLayout.createSequentialGroup()
                                    .addComponent(txfdScriptPath)
                                    .addGap(10, 10, 10)
                                    .addComponent(btnScriptPath, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addContainerGap())))
                        .addGroup(pnlBinPathLayout.createSequentialGroup()
                            .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblGPBinPath, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                                .addComponent(lblSlonyIPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(10, 10, 10)
                            .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlBinPathLayout.createSequentialGroup()
                                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txfdGPBinPath)
                                        .addComponent(txfdSlonyIPath))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnSlonyIPath, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnGPBinPath, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 277, Short.MAX_VALUE))
                            .addContainerGap())))
            );
            pnlBinPathLayout.setVerticalGroup(
                pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlBinPathLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblHGBinPath)
                        .addComponent(txfdHGBinPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnHGBinPath))
                    .addGap(10, 10, 10)
                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDataPath)
                        .addComponent(txfdHGDataPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnHGDataPath))
                    .addGap(10, 10, 10)
                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdScriptPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblScript)
                        .addComponent(btnScriptPath))
                    .addGap(10, 10, 10)
                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSlonyIPath)
                            .addComponent(txfdSlonyIPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(btnSlonyIPath))
                    .addGap(10, 10, 10)
                    .addGroup(pnlBinPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblGPBinPath)
                        .addComponent(txfdGPBinPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnGPBinPath))
                    .addGap(10, 10, 10)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(49, Short.MAX_VALUE))
            );

            lblFont1.setText(constBundle.getString("font"));

            btnUIFont.setText("Calibri,12");

            lblRefRefreshOnClick.setText(constBundle.getString("refRefreshOnClick"));

            cbShowUsersForPrivilege.setSelected(true);
            cbShowUsersForPrivilege.setText(constBundle.getString("showUsersForPrivilege"));

            cbShowPropertyOnDoubleClick.setSelected(true);
            cbShowPropertyOnDoubleClick.setText(constBundle.getString("showPropertyOnDoubleClick"));

            cbShowNotice.setSelected(true);
            cbShowNotice.setText(constBundle.getString("enableAutoRollBack"));

            cbConfirmObjectDeletion.setSelected(true);
            cbConfirmObjectDeletion.setText(constBundle.getString("confirmObjectDeletion"));

            cbShowSysObjInTreeView.setSelected(true);
            cbShowSysObjInTreeView.setText(constBundle.getString("showSysObjInTreeView"));

            jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { constBundle.getString("none"),constBundle.getString("refreshObjOnClick"),constBundle.getString("refreshObjAndChildrenOnClick") }));

            javax.swing.GroupLayout pnlUIMiscellaneousLayout = new javax.swing.GroupLayout(pnlUIMiscellaneous);
            pnlUIMiscellaneous.setLayout(pnlUIMiscellaneousLayout);
            pnlUIMiscellaneousLayout.setHorizontalGroup(
                pnlUIMiscellaneousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlUIMiscellaneousLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlUIMiscellaneousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlUIMiscellaneousLayout.createSequentialGroup()
                            .addComponent(lblRefRefreshOnClick, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlUIMiscellaneousLayout.createSequentialGroup()
                            .addComponent(lblFont1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnUIFont, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(cbConfirmObjectDeletion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbShowSysObjInTreeView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbShowUsersForPrivilege, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbShowPropertyOnDoubleClick, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbShowNotice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap())
            );
            pnlUIMiscellaneousLayout.setVerticalGroup(
                pnlUIMiscellaneousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlUIMiscellaneousLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlUIMiscellaneousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnUIFont)
                        .addComponent(lblFont1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addComponent(cbConfirmObjectDeletion)
                    .addGap(10, 10, 10)
                    .addComponent(cbShowSysObjInTreeView)
                    .addGap(10, 10, 10)
                    .addComponent(cbShowUsersForPrivilege)
                    .addGap(10, 10, 10)
                    .addComponent(cbShowPropertyOnDoubleClick)
                    .addGap(10, 10, 10)
                    .addComponent(cbShowNotice)
                    .addGap(10, 10, 10)
                    .addGroup(pnlUIMiscellaneousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblRefRefreshOnClick, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(98, Short.MAX_VALUE))
            );

            lblQueryEditorFont.setText(constBundle.getString("font"));

            txfdMaxCharPerColumn.setEnabled(false);

            lblMaxCharPerColumn.setText(constBundle.getString("maxCharPerColumn")
            );

            btnQueryEditorFont.setText("Calibri,12");
            btnQueryEditorFont.setEnabled(false);

            txfdIndentChar.setEditable(false);
            txfdIndentChar.setText("0");

            lblIndentChar.setText(constBundle.getString("indentChar")
            );

            lblUseNullStrInstaedOfNullResult.setText(constBundle.getString("useNullStrInstaedOfNullResult"));

            lblCopySQLFromMainToQueryTool.setText(constBundle.getString("copySQLFromMainToQueryTool"));

            lblEnableAutoRollBack.setText(constBundle.getString("enableAutoRollBack"));

            lblKeyWordsInUppercase.setText(constBundle.getString("keyWordsInUppercase"));

            cbUseNullInsteadOfNull.setSelected(true);
            cbUseNullInsteadOfNull.setEnabled(false);

            cbCopySQLFromMainToQueryTool.setSelected(true);
            cbCopySQLFromMainToQueryTool.setEnabled(false);

            cbEnableAutoRollBack.setSelected(true);
            cbEnableAutoRollBack.setEnabled(false);

            cbKeyWordsInUppercase.setEnabled(false);

            javax.swing.GroupLayout pnlQueryEditorLayout = new javax.swing.GroupLayout(pnlQueryEditor);
            pnlQueryEditor.setLayout(pnlQueryEditorLayout);
            pnlQueryEditorLayout.setHorizontalGroup(
                pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlQueryEditorLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblEnableAutoRollBack, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                        .addComponent(lblCopySQLFromMainToQueryTool, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblUseNullStrInstaedOfNullResult, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblIndentChar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMaxCharPerColumn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblQueryEditorFont, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblKeyWordsInUppercase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbUseNullInsteadOfNull)
                        .addComponent(cbCopySQLFromMainToQueryTool)
                        .addComponent(cbEnableAutoRollBack)
                        .addComponent(cbKeyWordsInUppercase)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlQueryEditorLayout.createSequentialGroup()
                            .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txfdIndentChar, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txfdMaxCharPerColumn, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(btnQueryEditorFont, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))
                            .addContainerGap())))
            );
            pnlQueryEditorLayout.setVerticalGroup(
                pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlQueryEditorLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblQueryEditorFont)
                        .addComponent(btnQueryEditorFont, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMaxCharPerColumn)
                        .addComponent(txfdMaxCharPerColumn))
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblIndentChar)
                        .addComponent(txfdIndentChar))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbUseNullInsteadOfNull)
                        .addComponent(lblUseNullStrInstaedOfNullResult, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(cbCopySQLFromMainToQueryTool, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCopySQLFromMainToQueryTool))
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(cbEnableAutoRollBack, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblEnableAutoRollBack))
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryEditorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(cbKeyWordsInUppercase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblKeyWordsInUppercase))
                    .addContainerGap(28, Short.MAX_VALUE))
            );

            pnlSqlSyntaxHighlight.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("foreBackground")
            ));

            cbUseSysBackground.setText(constBundle.getString("useSysBackground")
            );

            cbUseSysForeground.setText(constBundle.getString("useSysForeground")
            );

            lblCustomBackground.setText(constBundle.getString("customBackground"));

            lblCustomForeground.setText(constBundle.getString("customForeground"));

            lblMarginBackground.setText(constBundle.getString("customForeground"));

            txfdBackground.setEditable(false);
            txfdBackground.setBackground(new java.awt.Color(255, 255, 255));
            txfdBackground.setBorder(new javax.swing.border.MatteBorder(null));

            txfdForeground.setEditable(false);
            txfdForeground.setBackground(new java.awt.Color(0, 0, 0));
            txfdForeground.setBorder(new javax.swing.border.MatteBorder(null));

            txfdMarginBackground.setEditable(false);
            txfdMarginBackground.setBackground(new java.awt.Color(255, 255, 255));
            txfdMarginBackground.setBorder(new javax.swing.border.MatteBorder(null));

            javax.swing.GroupLayout pnlSqlSyntaxHighlightLayout = new javax.swing.GroupLayout(pnlSqlSyntaxHighlight);
            pnlSqlSyntaxHighlight.setLayout(pnlSqlSyntaxHighlightLayout);
            pnlSqlSyntaxHighlightLayout.setHorizontalGroup(
                pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSqlSyntaxHighlightLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(cbUseSysBackground, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                        .addComponent(cbUseSysForeground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblMarginBackground, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                        .addComponent(lblCustomForeground, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCustomBackground, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdBackground, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txfdForeground, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txfdMarginBackground))
                    .addContainerGap())
            );
            pnlSqlSyntaxHighlightLayout.setVerticalGroup(
                pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSqlSyntaxHighlightLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbUseSysBackground, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblCustomBackground))
                        .addComponent(txfdBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cbUseSysForeground, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblCustomForeground)
                        .addComponent(txfdForeground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlSqlSyntaxHighlightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lblMarginBackground)
                        .addComponent(txfdMarginBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
            );

            pnlCaret.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("caret")));

            lblCaret.setText(constBundle.getString("customBackground"));

            txfdCaretColor.setEditable(false);
            txfdCaretColor.setBackground(new java.awt.Color(0, 0, 0));
            txfdCaretColor.setBorder(new javax.swing.border.MatteBorder(null));

            javax.swing.GroupLayout pnlCaretLayout = new javax.swing.GroupLayout(pnlCaret);
            pnlCaret.setLayout(pnlCaretLayout);
            pnlCaretLayout.setHorizontalGroup(
                pnlCaretLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlCaretLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblCaret)
                    .addGap(18, 18, 18)
                    .addComponent(txfdCaretColor)
                    .addContainerGap())
            );
            pnlCaretLayout.setVerticalGroup(
                pnlCaretLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlCaretLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlCaretLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblCaret)
                        .addComponent(txfdCaretColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap())
            );

            jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("sqlSyntaxHighlight")
            ));

            lblMultlineComment.setText(constBundle.getString("multilineComment"));

            txfdMultilineComment.setEditable(false);
            txfdMultilineComment.setBackground(new java.awt.Color(3, 131, 3));
            txfdMultilineComment.setBorder(new javax.swing.border.MatteBorder(null));

            lblDoubleQuotedString.setText(constBundle.getString("doubleQuotedString"));

            txfdDoubleQuotedString.setEditable(false);
            txfdDoubleQuotedString.setBackground(new java.awt.Color(126, 1, 126));
            txfdDoubleQuotedString.setBorder(new javax.swing.border.MatteBorder(null));

            lblSinglelineComment.setText(constBundle.getString("singlelineComment"));

            txfdSingleLineComment.setEditable(false);
            txfdSingleLineComment.setBackground(new java.awt.Color(3, 131, 3));
            txfdSingleLineComment.setBorder(new javax.swing.border.MatteBorder(null));

            lblSQLDoc.setText(constBundle.getString("sqlDoc"));

            txfdSqlDoc.setEditable(false);
            txfdSqlDoc.setBackground(new java.awt.Color(127, 127, 127));
            txfdSqlDoc.setBorder(new javax.swing.border.MatteBorder(null));

            txfdNumber.setEditable(false);
            txfdNumber.setBackground(new java.awt.Color(0, 128, 126));
            txfdNumber.setBorder(new javax.swing.border.MatteBorder(null));

            lblNumber.setText(constBundle.getString("number"));

            txfdCustomBackground.setEditable(false);
            txfdCustomBackground.setBackground(new java.awt.Color(0, 1, 125));
            txfdCustomBackground.setBorder(new javax.swing.border.MatteBorder(null));

            lblKeyword.setText(constBundle.getString("customBackground"));

            txfdSingleQuotedString.setEditable(false);
            txfdSingleQuotedString.setBackground(new java.awt.Color(126, 1, 126));
            txfdSingleQuotedString.setBorder(new javax.swing.border.MatteBorder(null));

            lblSingleQuotedString.setText(constBundle.getString("singleQuotedString"));

            txfdOperator.setEditable(false);
            txfdOperator.setBackground(new java.awt.Color(125, 125, 125));
            txfdOperator.setBorder(new javax.swing.border.MatteBorder(null));

            lblOperator.setText(constBundle.getString("operator"));

            lblIdentifier.setText(constBundle.getString("identifier"));

            txfdIdentifer.setEditable(false);
            txfdIdentifer.setBackground(new java.awt.Color(0, 0, 0));
            txfdIdentifer.setBorder(new javax.swing.border.MatteBorder(null));

            javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
            jPanel3.setLayout(jPanel3Layout);
            jPanel3Layout.setHorizontalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lblNumber, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 67, Short.MAX_VALUE)
                        .addComponent(lblSQLDoc, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSinglelineComment, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMultlineComment, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblKeyword, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdMultilineComment)
                        .addComponent(txfdSingleLineComment)
                        .addComponent(txfdSqlDoc)
                        .addComponent(txfdNumber)
                        .addComponent(txfdCustomBackground, javax.swing.GroupLayout.DEFAULT_SIZE, 155, Short.MAX_VALUE))
                    .addGap(13, 13, 13)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lblOperator, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblSingleQuotedString, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblIdentifier, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(10, 10, 10)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txfdSingleQuotedString)
                                .addComponent(txfdOperator)
                                .addComponent(txfdIdentifer)))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(lblDoubleQuotedString, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(txfdDoubleQuotedString, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)))
                    .addGap(10, 10, 10))
            );
            jPanel3Layout.setVerticalGroup(
                jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(txfdMultilineComment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txfdSingleLineComment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblSinglelineComment))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txfdSqlDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblSQLDoc))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txfdNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblNumber))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txfdCustomBackground, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblKeyword)))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblMultlineComment)
                                .addComponent(lblDoubleQuotedString)
                                .addComponent(txfdDoubleQuotedString, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblSingleQuotedString)
                                .addComponent(txfdSingleQuotedString, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblOperator)
                                .addComponent(txfdOperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblIdentifier)
                                .addComponent(txfdIdentifer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addContainerGap())
            );

            javax.swing.GroupLayout pnlColorLayout = new javax.swing.GroupLayout(pnlColor);
            pnlColor.setLayout(pnlColorLayout);
            pnlColorLayout.setHorizontalGroup(
                pnlColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColorLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlCaret, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlSqlSyntaxHighlight, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap())
            );
            pnlColorLayout.setVerticalGroup(
                pnlColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlColorLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(pnlSqlSyntaxHighlight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(pnlCaret, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );

            lblResultCopyQuoting.setText(constBundle.getString("resultCopyQuoting"));

            lblResultCopyQuotChar.setText(constBundle.getString("resultCopyQuotChar")
            );

            lblResultCopyFieldSeparator.setText(constBundle.getString("resultCopyFieldSeparator")
            );

            lblShowNullValues.setText(constBundle.getString("showNullValues")
            );

            lblThousandSeparator.setText(constBundle.getString("thousandSeparator")
            );

            lblDecimalMark.setText(constBundle.getString("decimalMark")
            );

            lblCopyColumnNames.setText(constBundle.getString("copyColumnNames")
            );

            txfdThousandSeparator.setEnabled(false);

            txfdDecimalMark.setEnabled(false);

            cbResultCopyQuoting.setModel(new javax.swing.DefaultComboBoxModel(new String[] { constBundle.getString("none"), constBundle.getString("string"), constBundle.getString("all") }));

            cbResultCopyQuotChar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "'", "\"" }));

            cbResultCopyFieldSeparator.setModel(new javax.swing.DefaultComboBoxModel(new String[] { ";", "。", "|", "Tab", " " }));

            javax.swing.GroupLayout pnlResultGridLayout = new javax.swing.GroupLayout(pnlResultGrid);
            pnlResultGrid.setLayout(pnlResultGridLayout);
            pnlResultGridLayout.setHorizontalGroup(
                pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlResultGridLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlResultGridLayout.createSequentialGroup()
                            .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblDecimalMark, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                                .addComponent(lblCopyColumnNames, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblThousandSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblShowNullValues, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(10, 10, 10)
                            .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txfdDecimalMark, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txfdThousandSeparator, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(pnlResultGridLayout.createSequentialGroup()
                                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(cbCopyColumnNames)
                                        .addComponent(cbShowNullValues))
                                    .addGap(0, 0, Short.MAX_VALUE))))
                        .addGroup(pnlResultGridLayout.createSequentialGroup()
                            .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblResultCopyFieldSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblResultCopyQuotChar, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblResultCopyQuoting, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(10, 10, 10)
                            .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cbResultCopyFieldSeparator, javax.swing.GroupLayout.Alignment.TRAILING, 0, 238, Short.MAX_VALUE)
                                .addComponent(cbResultCopyQuotChar, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbResultCopyQuoting, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addContainerGap())
            );
            pnlResultGridLayout.setVerticalGroup(
                pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlResultGridLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblResultCopyQuoting)
                        .addComponent(cbResultCopyQuoting, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblResultCopyQuotChar)
                        .addComponent(cbResultCopyQuotChar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblResultCopyFieldSeparator)
                        .addComponent(cbResultCopyFieldSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbShowNullValues)
                        .addComponent(lblShowNullValues, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblThousandSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txfdThousandSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblDecimalMark, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txfdDecimalMark, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlResultGridLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(cbCopyColumnNames, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblCopyColumnNames, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap(146, Short.MAX_VALUE))
            );

            cbReadWriteFile.setText(constBundle.getString("readWriteUtf8File")
            );

            cbWriteBomForUtfFile.setText(constBundle.getString("writeBomForUtfFile")
            );

            cbExitNotPromptSave.setText(constBundle.getString("exitNotPromptSave")
            );

            javax.swing.GroupLayout pnlQueryFileLayout = new javax.swing.GroupLayout(pnlQueryFile);
            pnlQueryFile.setLayout(pnlQueryFileLayout);
            pnlQueryFileLayout.setHorizontalGroup(
                pnlQueryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlQueryFileLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlQueryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbWriteBomForUtfFile, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                        .addComponent(cbReadWriteFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbExitNotPromptSave, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE))
                    .addGap(10, 10, 10))
            );
            pnlQueryFileLayout.setVerticalGroup(
                pnlQueryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlQueryFileLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(cbReadWriteFile)
                    .addGap(5, 5, 5)
                    .addComponent(cbWriteBomForUtfFile)
                    .addGap(5, 5, 5)
                    .addComponent(cbExitNotPromptSave)
                    .addContainerGap(213, Short.MAX_VALUE))
            );

            lblFavouriteFilePath.setText(constBundle.getString("favouriteFilePath")
            );

            btnBrowseFavouriteFile.setText(constBundle.getString("browse"));

            javax.swing.GroupLayout pnlFavouriteLayout = new javax.swing.GroupLayout(pnlFavourite);
            pnlFavourite.setLayout(pnlFavouriteLayout);
            pnlFavouriteLayout.setHorizontalGroup(
                pnlFavouriteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlFavouriteLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblFavouriteFilePath)
                    .addGap(10, 10, 10)
                    .addComponent(txfdFavouriteFilePath, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
                    .addGap(10, 10, 10)
                    .addComponent(btnBrowseFavouriteFile, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
            );
            pnlFavouriteLayout.setVerticalGroup(
                pnlFavouriteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlFavouriteLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlFavouriteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlFavouriteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblFavouriteFilePath)
                            .addComponent(txfdFavouriteFilePath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(btnBrowseFavouriteFile))
                    .addContainerGap(292, Short.MAX_VALUE))
            );

            lblMacroFilePath.setText(constBundle.getString("macrosFilePath")
            );

            btnBrowseMacrosFile.setText(constBundle.getString("browse"));

            javax.swing.GroupLayout pnlMacrosLayout = new javax.swing.GroupLayout(pnlMacros);
            pnlMacros.setLayout(pnlMacrosLayout);
            pnlMacrosLayout.setHorizontalGroup(
                pnlMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlMacrosLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblMacroFilePath)
                    .addGap(10, 10, 10)
                    .addComponent(txfdMacroFilePath, javax.swing.GroupLayout.DEFAULT_SIZE, 302, Short.MAX_VALUE)
                    .addGap(10, 10, 10)
                    .addComponent(btnBrowseMacrosFile, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
            );
            pnlMacrosLayout.setVerticalGroup(
                pnlMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlMacrosLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMacroFilePath)
                        .addComponent(txfdMacroFilePath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBrowseMacrosFile))
                    .addContainerGap(298, Short.MAX_VALUE))
            );

            lblHistoryFilePath.setText(constBundle.getString("historyFilePath")
            );

            btnBrowseHistoryQuery.setText(constBundle.getString("browse"));

            lblMaxQueryToStore.setText(constBundle.getString("maxQueryToStore")
            );

            lblMaxSizeOfStoredQuery.setText(constBundle.getString("maxSizeOfStoredQuery")
            );

            javax.swing.GroupLayout pnlHistoryFileLayout = new javax.swing.GroupLayout(pnlHistoryFile);
            pnlHistoryFile.setLayout(pnlHistoryFileLayout);
            pnlHistoryFileLayout.setHorizontalGroup(
                pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlHistoryFileLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblMaxQueryToStore, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                        .addComponent(lblHistoryFilePath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblMaxSizeOfStoredQuery, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txfdHistoryFilePath, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlHistoryFileLayout.createSequentialGroup()
                            .addGroup(pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(spMaxQuerySize, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                                .addComponent(spMaxQueryCount, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnBrowseHistoryQuery, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10))
            );
            pnlHistoryFileLayout.setVerticalGroup(
                pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlHistoryFileLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblHistoryFilePath)
                        .addComponent(txfdHistoryFilePath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBrowseHistoryQuery))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMaxQueryToStore)
                        .addComponent(spMaxQueryCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(9, 9, 9)
                    .addGroup(pnlHistoryFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblMaxSizeOfStoredQuery)
                        .addComponent(spMaxQuerySize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(241, Short.MAX_VALUE))
            );

            lblDBDesignerFont.setText(constBundle.getString("font"));

            btnDBDesignerFont.setText("Calibri,12");

            javax.swing.GroupLayout pnlDBDesignerLayout = new javax.swing.GroupLayout(pnlDBDesigner);
            pnlDBDesigner.setLayout(pnlDBDesignerLayout);
            pnlDBDesignerLayout.setHorizontalGroup(
                pnlDBDesignerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDBDesignerLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblDBDesignerFont)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnDBDesignerFont, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                    .addContainerGap())
            );
            pnlDBDesignerLayout.setVerticalGroup(
                pnlDBDesignerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDBDesignerLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlDBDesignerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDBDesignerFont)
                        .addComponent(btnDBDesignerFont))
                    .addContainerGap(330, Short.MAX_VALUE))
            );

            lblIdleProcessColor.setText(constBundle.getString("idleProcessColor"));

            txfdIdleProcessColor.setEditable(false);
            txfdIdleProcessColor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));

            lblActiveProcessColor.setText(constBundle.getString("activeProcessColor"));

            txfdActiveProcessColor.setEditable(false);
            txfdActiveProcessColor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));

            txfdSlowProcessColor.setEditable(false);
            txfdSlowProcessColor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));

            lblSlowProcessColor.setText(constBundle.getString("slowProcessColor"));

            txfdBlockedProcessColor.setEditable(false);
            txfdBlockedProcessColor.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.highlight"));

            lblBlockedProcessColor.setText(constBundle.getString("blockedProcessColor"));

            javax.swing.GroupLayout pnlServerStatusLayout = new javax.swing.GroupLayout(pnlServerStatus);
            pnlServerStatus.setLayout(pnlServerStatusLayout);
            pnlServerStatusLayout.setHorizontalGroup(
                pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlServerStatusLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblIdleProcessColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblActiveProcessColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSlowProcessColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblBlockedProcessColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdBlockedProcessColor, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                        .addComponent(txfdSlowProcessColor)
                        .addComponent(txfdActiveProcessColor)
                        .addComponent(txfdIdleProcessColor))
                    .addContainerGap())
            );
            pnlServerStatusLayout.setVerticalGroup(
                pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlServerStatusLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblIdleProcessColor)
                        .addComponent(txfdIdleProcessColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblActiveProcessColor)
                        .addComponent(txfdActiveProcessColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblSlowProcessColor)
                        .addComponent(txfdSlowProcessColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlServerStatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblBlockedProcessColor)
                        .addComponent(txfdBlockedProcessColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(212, Short.MAX_VALUE))
            );

            lblSysSchema.setText(constBundle.getString("sysSchema")
            );

            txfdSysSchema.setEditable(false);
            txfdSysSchema.setText("information_schema");

            lblUserLanguage.setText(constBundle.getString("userLanguage")
            );

            cbUserLanguage.setModel(new javax.swing.DefaultComboBoxModel(new String[] { constBundle.getString("fontChinese") }));

            lblInActivityTimeout.setText(constBundle.getString("inActivityTimeout")
            );

            spinInActivityTimeout.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), Integer.valueOf(0), null, Integer.valueOf(1)));

            javax.swing.GroupLayout pnlUserInterfaceLayout = new javax.swing.GroupLayout(pnlUserInterface);
            pnlUserInterface.setLayout(pnlUserInterfaceLayout);
            pnlUserInterfaceLayout.setHorizontalGroup(
                pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlUserInterfaceLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlUserInterfaceLayout.createSequentialGroup()
                            .addGroup(pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblSysSchema, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                                .addComponent(lblUserLanguage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cbUserLanguage, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txfdSysSchema, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)))
                        .addGroup(pnlUserInterfaceLayout.createSequentialGroup()
                            .addComponent(lblInActivityTimeout, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(spinInActivityTimeout)))
                    .addContainerGap())
            );
            pnlUserInterfaceLayout.setVerticalGroup(
                pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlUserInterfaceLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblUserLanguage)
                        .addComponent(cbUserLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblSysSchema)
                        .addComponent(txfdSysSchema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlUserInterfaceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblInActivityTimeout)
                        .addComponent(spinInActivityTimeout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(274, Short.MAX_VALUE))
            );

            lblDBHelpPath.setText(constBundle.getString("dbHelpPath")
            );

            lblAdminHelpPath.setText(constBundle.getString("adminHelpPath")
            );

            lblGgHelpPath.setText(constBundle.getString("gpHelpPath")
            );
            lblGgHelpPath.setEnabled(false);

            txfdGgHelpPath.setText("http://gpdb.docs.pivotal.io/4360/common/welcome.html");
            txfdGgHelpPath.setEnabled(false);

            txfdSlonyHelpPath.setText("http://slony.info/documentation/preface.html");
            txfdSlonyHelpPath.setEnabled(false);

            lblSlonygHelpPath.setText(constBundle.getString("slonyIHelpPath")
            );
            lblSlonygHelpPath.setEnabled(false);

            lblHgdbManual.setText(constBundle.getString("hgdbManual"));

            lblHgdbAdminManual.setText(constBundle.getString("hgdbAdminManual"));

            javax.swing.GroupLayout pnlHelpPathLayout = new javax.swing.GroupLayout(pnlHelpPath);
            pnlHelpPath.setLayout(pnlHelpPathLayout);
            pnlHelpPathLayout.setHorizontalGroup(
                pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlHelpPathLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblDBHelpPath, javax.swing.GroupLayout.DEFAULT_SIZE, 76, Short.MAX_VALUE)
                        .addComponent(lblAdminHelpPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblGgHelpPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSlonygHelpPath, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblHgdbManual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblHgdbAdminManual, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdSlonyHelpPath, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                        .addComponent(txfdGgHelpPath)
                        .addComponent(txfdHgdbAdminManual)
                        .addComponent(txfdHgdbManual)
                        .addComponent(txfdPgHelpPath)
                        .addComponent(txfdPgAdminHelpPath))
                    .addContainerGap())
            );
            pnlHelpPathLayout.setVerticalGroup(
                pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlHelpPathLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdHgdbManual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblHgdbManual))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdHgdbAdminManual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblHgdbAdminManual))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdPgHelpPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblDBHelpPath))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdPgAdminHelpPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblAdminHelpPath))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblGgHelpPath)
                        .addComponent(txfdGgHelpPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlHelpPathLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblSlonygHelpPath)
                        .addComponent(txfdSlonyHelpPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(145, Short.MAX_VALUE))
            );

            cbNotShowGuruHints1.setText(constBundle.getString("notShowGuruHints")
            );
            cbNotShowGuruHints1.setEnabled(false);

            cbResetGuruHints1.setText(constBundle.getString("resetGuruHints")
            );
            cbResetGuruHints1.setEnabled(false);

            javax.swing.GroupLayout pnlGuruHintLayout = new javax.swing.GroupLayout(pnlGuruHint);
            pnlGuruHint.setLayout(pnlGuruHintLayout);
            pnlGuruHintLayout.setHorizontalGroup(
                pnlGuruHintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlGuruHintLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlGuruHintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbResetGuruHints1, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
                        .addComponent(cbNotShowGuruHints1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10))
            );
            pnlGuruHintLayout.setVerticalGroup(
                pnlGuruHintLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlGuruHintLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(cbNotShowGuruHints1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(cbResetGuruHints1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(252, Short.MAX_VALUE))
            );

            lblLogFile.setText(constBundle.getString("logfile")
            );

            txfdLogPath.setEditable(false);
            txfdLogPath.setText("logs/highgo.log");

            btnBrowseLogFile.setText(constBundle.getString("browse"));
            btnBrowseLogFile.setEnabled(false);

            pnlLogLevel.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("logLevel")
            ));

            tbnGroupLogLevel.add(rbNoLog);
            rbNoLog.setText(constBundle.getString("noLogging"));

            tbnGroupLogLevel.add(rbErrorOnly);
            rbErrorOnly.setText(constBundle.getString("errorOnly"));

            tbnGroupLogLevel.add(rbErrorWarn);
            rbErrorWarn.setText(constBundle.getString("errorWarn"));

            tbnGroupLogLevel.add(rbErrorWarnInfo);
            rbErrorWarnInfo.setSelected(true);
            rbErrorWarnInfo.setText(constBundle.getString("errorWarnInfo"));

            tbnGroupLogLevel.add(rbDebugNotForNormal);
            rbDebugNotForNormal.setText(constBundle.getString("debugNotNormal"));

            javax.swing.GroupLayout pnlLogLevelLayout = new javax.swing.GroupLayout(pnlLogLevel);
            pnlLogLevel.setLayout(pnlLogLevelLayout);
            pnlLogLevelLayout.setHorizontalGroup(
                pnlLogLevelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlLogLevelLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlLogLevelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(rbNoLog)
                        .addComponent(rbErrorOnly)
                        .addComponent(rbErrorWarn)
                        .addComponent(rbErrorWarnInfo)
                        .addComponent(rbDebugNotForNormal))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
            pnlLogLevelLayout.setVerticalGroup(
                pnlLogLevelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlLogLevelLayout.createSequentialGroup()
                    .addComponent(rbNoLog)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(rbErrorOnly)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(rbErrorWarn)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(rbErrorWarnInfo)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(rbDebugNotForNormal))
            );

            javax.swing.GroupLayout pnlLoggingLayout = new javax.swing.GroupLayout(pnlLogging);
            pnlLogging.setLayout(pnlLoggingLayout);
            pnlLoggingLayout.setHorizontalGroup(
                pnlLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlLoggingLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlLoggingLayout.createSequentialGroup()
                            .addComponent(txfdLogPath, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnBrowseLogFile, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pnlLoggingLayout.createSequentialGroup()
                            .addComponent(lblLogFile)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(pnlLogLevel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap())
            );
            pnlLoggingLayout.setVerticalGroup(
                pnlLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlLoggingLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblLogFile)
                    .addGap(10, 10, 10)
                    .addGroup(pnlLoggingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdLogPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnBrowseLogFile))
                    .addGap(10, 10, 10)
                    .addComponent(pnlLogLevel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(126, Short.MAX_VALUE))
            );

            jTextArea1.setColumns(20);
            jTextArea1.setRows(5);
            jScrollPane2.setViewportView(jTextArea1);

            setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
            setTitle(constBundle.getString("addSchema"));
            setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/namespace.png")));
    setModal(true);
    setName("dlgServerAdd"); // NOI18N

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));

    javax.swing.GroupLayout pnlButtonsLayout = new javax.swing.GroupLayout(pnlButtons);
    pnlButtons.setLayout(pnlButtonsLayout);
    pnlButtonsLayout.setHorizontalGroup(
        pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonsLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 370, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonsLayout.setVerticalGroup(
        pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButtons, java.awt.BorderLayout.PAGE_END);

    pnlContent.setPreferredSize(new java.awt.Dimension(500, 461));

    javax.swing.GroupLayout pnlContentLayout = new javax.swing.GroupLayout(pnlContent);
    pnlContent.setLayout(pnlContentLayout);
    pnlContentLayout.setHorizontalGroup(
        pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 473, Short.MAX_VALUE)
    );
    pnlContentLayout.setVerticalGroup(
        pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 407, Short.MAX_VALUE)
    );

    getContentPane().add(pnlContent, java.awt.BorderLayout.CENTER);

    treeMenu.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("navigation")));
    treeMenu.setModel(null);
    treeMenu.setPreferredSize(new java.awt.Dimension(150, 50));
    spMenu.setViewportView(treeMenu);

    getContentPane().add(spMenu, java.awt.BorderLayout.WEST);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void customization()
    {
        this.setTitle(constBundle.getString("options"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/property.png")));
        //treeMenu.setRootVisible(false);
        treeMenu.setEditable(false);
        treeMenu.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) treeMenu.getCellRenderer();
        renderer.setLeafIcon(null);
        renderer.setClosedIcon(null);
        renderer.setOpenIcon(null);
        treeMenu.setCellRenderer(renderer);
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(constBundle.getString("options"));
        DefaultMutableTreeNode groupNode = null;
        DefaultMutableTreeNode itemNode = null;
        groupNode = new DefaultMutableTreeNode(constBundle.getString("browser"));
        root.add(groupNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("display"));
        groupNode.add(itemNode);
//        itemNode = new DefaultMutableTreeNode(constBundle.getString("properties"));
//        groupNode.add(itemNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("binaryPaths"));
        groupNode.add(itemNode);
//        itemNode = new DefaultMutableTreeNode(constBundle.getString("UIMiscellaneous"));
//        groupNode.add(itemNode);
        groupNode = new DefaultMutableTreeNode(constBundle.getString("queryTool"));
        root.add(groupNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("queryEditor"));
        groupNode.add(itemNode);
//        itemNode = new DefaultMutableTreeNode(constBundle.getString("colors"));
//        groupNode.add(itemNode);
//        itemNode = new DefaultMutableTreeNode(constBundle.getString("resultsGrid"));
//        groupNode.add(itemNode);
//        itemNode = new DefaultMutableTreeNode(constBundle.getString("queryFiles"));
//        groupNode.add(itemNode);
        /*
        itemNode = new DefaultMutableTreeNode(constBundle.getString("historyQuery"));
        groupNode.add(itemNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("favourites"));
        groupNode.add(itemNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("macros"));
        groupNode.add(itemNode);
        */
//        groupNode = new DefaultMutableTreeNode(constBundle.getString("dbDesigner"));
//        root.add(groupNode);
//        groupNode = new DefaultMutableTreeNode(constBundle.getString("serverStatus"));
//        root.add(groupNode);
        groupNode = new DefaultMutableTreeNode(constBundle.getString("miscellaneous"));
        root.add(groupNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("userInterface"));
        groupNode.add(itemNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("helpPaths"));
        groupNode.add(itemNode);
//        itemNode = new DefaultMutableTreeNode(constBundle.getString("guruHints"));
//        groupNode.add(itemNode);
        itemNode = new DefaultMutableTreeNode(constBundle.getString("logging"));
        groupNode.add(itemNode);
        treeMenu.setModel(new DefaultTreeModel(root));
        for (int i = 0; i < 19; i++)
        {
            treeMenu.expandRow(i);
        }
        initObjectDisplayStatus();
        addPanel();
        treeMenu.addTreeSelectionListener(new TreeSelectionListener()
        {
            @Override
            public void valueChanged(TreeSelectionEvent evt)
            {
                treeMenuValueChanged(evt);
            }
        });
        
        //for query hiostory
        spMaxQueryCount.setModel(new SpinnerNumberModel(10, 1, 100, 1));
        this.forbitInvalidInputToSpinner(spMaxQueryCount);        
        spMaxQueryCount.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                maxQueryCountValueChanged();
            }
        });
        spMaxQuerySize.setModel(new SpinnerNumberModel(512, 1, 1024, 1));
        this.forbitInvalidInputToSpinner(spMaxQuerySize);
        spMaxQuerySize.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                maxQuerySizeValueChanged();
            }
        });

        Level oldLevel = LogManager.getRootLogger().getLevel();
        logger.info(oldLevel.toString());
        if (oldLevel == Level.OFF)
        {
            rbNoLog.setSelected(true);
        } else if (oldLevel == Level.ERROR)
        {
            rbErrorOnly.setSelected(true);
        } else if (oldLevel == Level.WARN)
        {
            rbErrorWarn.setSelected(true);
        } else if (oldLevel == Level.INFO)
        {
            rbErrorWarnInfo.setSelected(true);
        } else if (oldLevel == Level.DEBUG)
        {
            rbDebugNotForNormal.setSelected(true);
        }
                
         this.forbitInvalidInputToSpinner(spinInActivityTimeout);
    }
    private void addPanel()
    {
        pnlContent.setLayout(card);
        pnlContent.add(constBundle.getString("browser"), pnlDisplay);
        pnlContent.add(constBundle.getString("display"), pnlDisplay);
        pnlContent.add(constBundle.getString("properties"), pnlProperty);
        pnlContent.add(constBundle.getString("binaryPaths"), pnlBinPath);
        pnlContent.add(constBundle.getString("UIMiscellaneous"), pnlUIMiscellaneous);

        pnlContent.add(constBundle.getString("queryTool"), pnlQueryEditor);
        pnlContent.add(constBundle.getString("queryEditor"), pnlQueryEditor);
        pnlContent.add(constBundle.getString("colors"), pnlColor);
        pnlContent.add(constBundle.getString("resultsGrid"), pnlResultGrid);
        pnlContent.add(constBundle.getString("queryFiles"), pnlQueryFile);
        pnlContent.add(constBundle.getString("favourites"), pnlFavourite);
        pnlContent.add(constBundle.getString("macros"), pnlMacros);
        pnlContent.add(constBundle.getString("historyQuery"), pnlHistoryFile);

        pnlContent.add(constBundle.getString("dbDesigner"), pnlDBDesigner);

        pnlContent.add(constBundle.getString("serverStatus"), pnlServerStatus);

        pnlContent.add(constBundle.getString("miscellaneous"), pnlUserInterface);
        pnlContent.add(constBundle.getString("userInterface"), pnlUserInterface);
        pnlContent.add(constBundle.getString("helpPaths"), pnlHelpPath);
        pnlContent.add(constBundle.getString("guruHints"), pnlGuruHint);
        pnlContent.add(constBundle.getString("logging"), pnlLogging);
    }
    private void initObjectDisplayStatus()
    {
        tblObjects.setEnabled(false);
        DefaultTableModel objModel = (DefaultTableModel) tblObjects.getModel();
        int row = objModel.getRowCount();
        if (row > 0)
        {
            for (int i = row - 1; i >= 0; i--)
            {
                objModel.removeRow(i);
            }
        }
        Object[] rowData;
        rowData = new Object[]{true, constBundle.getString("databases")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("tablespaces")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("pgAgentJobs")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("groupRoles")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("loginRoles")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("resourceQueues")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("catalogs")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("casts")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("eventTriggers")};
        objModel.addRow(rowData);        
        rowData = new Object[]{false, constBundle.getString("extensions")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("foreignDataWrappers")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("foreignServers")};
        objModel.addRow(rowData);        
        rowData = new Object[]{false, constBundle.getString("usersMappings")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("languages")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("schemas")};
        objModel.addRow(rowData);        
        rowData = new Object[]{false, constBundle.getString("synonyms")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("slonyICluster")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("aggregates")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("collations")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("conversions")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("domains")};
        objModel.addRow(rowData);        
        rowData = new Object[]{false, constBundle.getString("externalTables")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("foreignTables")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("ftsConfiguration")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("ftsDictionaries")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("ftsParsers")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("ftsTemplates")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("functions")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("operators")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("operatorClasses")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("operatorFamilies")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("packages")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("procedures")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("sequences")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("tables")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("triggerFunctions")};
        objModel.addRow(rowData);
        rowData = new Object[]{false, constBundle.getString("types")};
        objModel.addRow(rowData);
        rowData = new Object[]{true, constBundle.getString("views")};
        objModel.addRow(rowData);
        
    }
    
    private void treeMenuValueChanged(TreeSelectionEvent evt)
    {
        String item = evt.getPath().getLastPathComponent().toString();
        pnlContent.setVisible(true);
        logger.info("current value :" + item);
        if (item != null && !item.isEmpty())
        {
            card.show(pnlContent, item);
        }
    }
    private void btnHelpActionPerformed(ActionEvent evt)                                        
    {                 
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showAdminHelpPage(this, "options.html");
    }                                       
    private void btnCancleActionPerformed(ActionEvent evt)                                          
    {                                              
        logger.info(evt.getActionCommand());
        this.dispose();
    }
    private void btnOKActionPerformed(ActionEvent evt)
    {                                          
        logger.info(evt.getActionCommand());
        
        OptionController oc = OptionController.getInstance();
        try
        {
            //validate                      
            validatePath("data_path", constBundle.getString("hgDataPath"));
            validatePath("bin_path", constBundle.getString("hgBinPath"));
            validatePath("script_path",  constBundle.getString("scriptPath"));
            //logger.debug("inactivity_timeout=" + prop.getProperty("inactivity_timeout"));  

            //for main option
            oc.savePropertiesToOption(prop);
            //for log
            //oc.savePropertiesToFile(logProp, log4jPath);   
            if (logLevel != null)
            {
                LogManager.getRootLogger().setLevel(logLevel);
                logger.warn("warn..............");
                logger.error("error.............");
                logger.info("info................");
                logger.debug("debug.............");
            }
            this.dispose();
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void validatePath(String key, String name) throws Exception
    {
        String path = prop.getProperty(key);
        logger.debug(key + "=" + path);
        if (path == null)
        {
            return;
        }
        File f = new File(path);
        if (!f.exists())
        {
            throw new Exception(MessageFormat.format(constBundle.getString("pathNotExist"), name, path));
        }
        if (!f.isDirectory())
        {
            throw new Exception(MessageFormat.format(constBundle.getString("pathNotValid"), name, path));
        }        
    }
    
    //log file
    private void chooseLogFileAction(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.chooseLogFile(pnlFavourite, txfdLogPath,
                MessageFormat.format(constBundle.getString("chooseWhatFile"), constBundle.getString("log")));
        //logProp.setProperty("log4j.appender.FILE.File", txfdLogPath.getText());
    }    
    private void logLevelAction(ActionEvent e)
    {
        JRadioButton rb = (JRadioButton) e.getSource();
        if(rb.isSelected())
        {
            if (rb == rbNoLog)
            {
                logLevel = Level.OFF;
                //logProp.setProperty("log4j.rootLogger", "OFF, STDOUT, FILE");
            } else if (rb == rbErrorOnly)
            {
                logLevel = Level.ERROR;
                //logProp.setProperty("log4j.rootLogger", "ERROR, STDOUT, FILE");
            } else if (rb == rbErrorWarn)
            {
                logLevel = Level.WARN;
                // logProp.setProperty("log4j.rootLogger", "WARN, STDOUT, FILE");
            } else if (rb == rbErrorWarnInfo)
            {
                logLevel = Level.INFO;
                //logProp.setProperty("log4j.rootLogger", "INFO, STDOUT, FILE");
            } else if (rb == rbDebugNotForNormal)
            {
                logLevel = Level.DEBUG;
                //logProp.setProperty("log4j.rootLogger", "DEBUG, STDOUT, FILE");
            }
        }
    }
    
    
    
    //content changed
    private void txfdContentChanged(JTextField txfd, String propname)
    {
        logger.info(txfd.getText());
        if (txfd.getText().isEmpty())
        {
            prop.remove(propname);
        } else
        {
            prop.setProperty(propname, txfd.getText());
        }
    }    
    //file
    private void chooseMacrosFileAction(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.chooseXMLFile(pnlFavourite, txfdMacroFilePath,
                MessageFormat.format(constBundle.getString("chooseWhatFile"), constBundle.getString("macros")));
        prop.setProperty("macro_file", txfdMacroFilePath.getText());
    }
    private void chooseFavouriteFileAction(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.chooseXMLFile(pnlFavourite, txfdFavouriteFilePath,
                MessageFormat.format(constBundle.getString("chooseWhatFile"), constBundle.getString("favourites")));
        prop.setProperty("favourite_file", txfdFavouriteFilePath.getText());
    }
    private void chooseHistoryQueryFileAction(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.chooseXMLFile(pnlFavourite, txfdHistoryFilePath,
                MessageFormat.format(constBundle.getString("chooseWhatFile"), constBundle.getString("historyQuery")));
        prop.setProperty("history_query_file", txfdHistoryFilePath.getText());
    }    
    
    
    private void chooseXMLFile(Component parent, JTextField txtfd, String title)
    {
        FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter("xml files", "xml");
        this.chooseFile(parent, txtfd, title, xmlFilter);
    }
    private void chooseLogFile(Component parent, JTextField txtfd, String title)
    {
        FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter("log files", "log");
        this.chooseFile(parent, txtfd, title, xmlFilter);
    }
    private void chooseFile(Component parent, JTextField txtfd, String title, FileNameExtensionFilter filter)
    {
        FileChooserDialog fileChooser = new FileChooserDialog();
        int response = fileChooser.showFileChooser(parent, title, txtfd.getText(), filter);
        logger.info("response = " + response);
        if (response == 0)
        {
            String choosedFile = fileChooser.getChoosedFileDir();
            logger.info("choosedFile=" + choosedFile);
            txtfd.setText(choosedFile);
        }
    } 
    
    
    //path    
    private void chooseDataPath(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.choosePath(pnlBinPath, txfdHGDataPath,MessageFormat.format(constBundle.getString("chooseWhatPath"), "data"));
        prop.setProperty("data_path", txfdHGDataPath.getText());
    }
    private void chooseBinPath(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.choosePath(pnlBinPath, txfdHGBinPath,MessageFormat.format(constBundle.getString("chooseWhatPath"), "bin"));
        prop.setProperty("bin_path", txfdHGBinPath.getText());
    }    
    private void chooseScriptPath(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.choosePath(pnlBinPath, txfdScriptPath,MessageFormat.format(constBundle.getString("chooseWhatPath"), "script"));
        prop.setProperty("script_path", txfdScriptPath.getText());
    } 
    private void choosePath(Component parent, JTextField txtfd,String title)
    {
        PathChooserDialog fileChooser = new PathChooserDialog();
        int response = fileChooser.showPathChooser(parent, txtfd.getText(),title);
        logger.info("response = " + response);
        if (response == 0)
        {
            String choosedFile = fileChooser.getChoosedPath();
            logger.info("choosedFile=" + choosedFile);
            txtfd.setText(choosedFile);
        }
    }
    //help url
    private void txfdDBHelpUrlKeyReleased()
    {
        this.enterUrl(txfdPgHelpPath, "db_help_url");
    }
    private void txfdAdminHelpUrlKeyReleased()
    {
        this.enterUrl(txfdPgAdminHelpPath, "admin_help_url");
    }    
    private void enterUrl(JTextField txfd, String propName)
    {
        String oldUrl = prop.getProperty(propName);
        String newUrl = txfd.getText();
        //logger.info("newUrl=" + newUrl + ",oldUrl=" + oldUrl);
        if (oldUrl == null)
        {
            if (!newUrl.isEmpty())
            {
                prop.setProperty(propName, newUrl);
            }
        } else
        {
            if (!oldUrl.equals(newUrl))
            {
                prop.setProperty(propName, newUrl);
            }
        }
    }  
    //query 
    private void forbitInvalidInputToSpinner(JSpinner sp)
    {
        JFormattedTextField tf = ((JSpinner.NumberEditor) sp.getEditor()).getTextField();
        DefaultFormatterFactory factory = (DefaultFormatterFactory) tf.getFormatterFactory();
        NumberFormatter formatter = (NumberFormatter) factory.getDefaultFormatter();
        formatter.setAllowsInvalid(false);
    }
    private void maxQueryCountValueChanged()
    {
        this.spinnerValueChanged(spMaxQueryCount, "query_max_count");
    }
    private void maxQuerySizeValueChanged()
    {
        this.spinnerValueChanged(spMaxQuerySize, "query_max_size");
    }
    private void spinnerValueChanged(JSpinner sp, String propName)
    {
        int newValue = (int) sp.getValue();
        String oldValue = prop.getProperty(propName);
        logger.info(propName + ":oldValeu=" + oldValue + ",newValue=" + newValue);
        if (oldValue == null)
        {
            prop.setProperty(propName, String.valueOf(newValue));
        } else if (newValue != Integer.valueOf(oldValue))
        {
            prop.setProperty(propName, String.valueOf(newValue));
        }
    }
 

    /*********************************************************/
    private void btnFontForUIActionPerformed(ActionEvent evt)
    {
        FontChooserDialog fontChooser = new FontChooserDialog(this, true, queryEditorFontAttribute);
        fontChooser.setLocationRelativeTo(this);
        fontChooser.setVisible(true);
        if (fontChooser.isFinish())
        {
            JButton btn = (JButton) evt.getSource();
            FontAttributeDTO fontAttribute = fontChooser.getFontAttributeDTO();
            btn.setText(fontAttribute.getText());
            logger.info(fontAttribute.toString());
            if (btn == btnQueryEditorFont)
            {
                prop.setProperty("query_editor_font", fontAttribute.getFontFamily() + "," + fontAttribute.getStyle() + "," + fontAttribute.getFontSize()+","+fontAttribute.getForeground().getRGB());
            }
        }
    }
    private void setColor(MouseEvent evt)
    {
        Color newColor = JColorChooser.showDialog(
                this, constBundle.getString("chooseFontColor"),
                txfdBackground.getBackground());
        if (newColor != null)
        {
            JTextField txfd = (JTextField) evt.getSource();
            txfd.setBackground(newColor);
        }
    }

    /*
    private OptionInfoDTO getOptionInfo()
    {
        OptionInfoDTO option = new OptionInfoDTO();
        option.setDataDir(txfdHGDataPath.getText());
        option.setBinDir(txfdHGBinPath.getText());
        option.setLogDir(txfdLogPath.getText());
        option.setFavouriteDir(txfdFavouriteFilePath.getText());
        option.setMacroDir(txfdMacroFilePath.getText());
        option.setHistoryQueryDir(txfdHistoryFilePath.getText());
        option.setHgHelpUrl(txfdHgHelpPath.getText());
        option.setPgHelpUrl(txfdPgHelpPath.getText());
        return option;
    }
    */
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBrowseFavouriteFile;
    private javax.swing.JButton btnBrowseHistoryQuery;
    private javax.swing.JButton btnBrowseLogFile;
    private javax.swing.JButton btnBrowseMacrosFile;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnDBDesignerFont;
    private javax.swing.JButton btnDefaultObjDisplay;
    private javax.swing.JButton btnGPBinPath;
    private javax.swing.JButton btnHGBinPath;
    private javax.swing.JButton btnHGDataPath;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnQueryEditorFont;
    private javax.swing.JButton btnScriptPath;
    private javax.swing.JButton btnSlonyIPath;
    private javax.swing.JButton btnUIFont;
    private javax.swing.JCheckBox cbConfirmObjectDeletion;
    private javax.swing.JCheckBox cbCopyColumnNames;
    private javax.swing.JCheckBox cbCopySQLFromMainToQueryTool;
    private javax.swing.JCheckBox cbEnableAutoRollBack;
    private javax.swing.JCheckBox cbExitNotPromptSave;
    private javax.swing.JCheckBox cbKeyWordsInUppercase;
    private javax.swing.JCheckBox cbNotShowGuruHints1;
    private javax.swing.JCheckBox cbReadWriteFile;
    private javax.swing.JCheckBox cbResetGuruHints1;
    private javax.swing.JComboBox cbResultCopyFieldSeparator;
    private javax.swing.JComboBox cbResultCopyQuotChar;
    private javax.swing.JComboBox cbResultCopyQuoting;
    private javax.swing.JCheckBox cbShowNotice;
    private javax.swing.JCheckBox cbShowNullValues;
    private javax.swing.JCheckBox cbShowPropertyOnDoubleClick;
    private javax.swing.JCheckBox cbShowSysObjInTreeView;
    private javax.swing.JCheckBox cbShowUsersForPrivilege;
    private javax.swing.JCheckBox cbUseNullInsteadOfNull;
    private javax.swing.JCheckBox cbUseSysBackground;
    private javax.swing.JCheckBox cbUseSysForeground;
    private javax.swing.JComboBox cbUserLanguage;
    private javax.swing.JCheckBox cbWriteBomForUtfFile;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JLabel lblActiveProcessColor;
    private javax.swing.JLabel lblAdminHelpPath;
    private javax.swing.JLabel lblBlockedProcessColor;
    private javax.swing.JLabel lblCaret;
    private javax.swing.JLabel lblCopyColumnNames;
    private javax.swing.JLabel lblCopySQLFromMainToQueryTool;
    private javax.swing.JLabel lblCustomBackground;
    private javax.swing.JLabel lblCustomForeground;
    private javax.swing.JLabel lblDBDesignerFont;
    private javax.swing.JLabel lblDBHelpPath;
    private javax.swing.JLabel lblDataPath;
    private javax.swing.JLabel lblDecimalMark;
    private javax.swing.JLabel lblDisplayObjects;
    private javax.swing.JLabel lblDoubleQuotedString;
    private javax.swing.JLabel lblEnableAutoRollBack;
    private javax.swing.JLabel lblFavouriteFilePath;
    private javax.swing.JLabel lblFont1;
    private javax.swing.JLabel lblGPBinPath;
    private javax.swing.JLabel lblGgHelpPath;
    private javax.swing.JLabel lblHGBinPath;
    private javax.swing.JLabel lblHgdbAdminManual;
    private javax.swing.JLabel lblHgdbManual;
    private javax.swing.JLabel lblHistoryFilePath;
    private javax.swing.JLabel lblIdentifier;
    private javax.swing.JLabel lblIdleProcessColor;
    private javax.swing.JLabel lblInActivityTimeout;
    private javax.swing.JLabel lblIndentChar;
    private javax.swing.JLabel lblKeyWordsInUppercase;
    private javax.swing.JLabel lblKeyword;
    private javax.swing.JLabel lblLogFile;
    private javax.swing.JLabel lblMacroFilePath;
    private javax.swing.JLabel lblMarginBackground;
    private javax.swing.JLabel lblMaxCharPerColumn;
    private javax.swing.JLabel lblMaxQueryToStore;
    private javax.swing.JLabel lblMaxSizeOfStoredQuery;
    private javax.swing.JLabel lblMultlineComment;
    private javax.swing.JLabel lblNumber;
    private javax.swing.JLabel lblOperator;
    private javax.swing.JLabel lblQueryEditorFont;
    private javax.swing.JLabel lblRefRefreshOnClick;
    private javax.swing.JLabel lblResultCopyFieldSeparator;
    private javax.swing.JLabel lblResultCopyQuotChar;
    private javax.swing.JLabel lblResultCopyQuoting;
    private javax.swing.JLabel lblRowCountLimit1;
    private javax.swing.JLabel lblSQLDoc;
    private javax.swing.JLabel lblScript;
    private javax.swing.JLabel lblShowNullValues;
    private javax.swing.JLabel lblSingleQuotedString;
    private javax.swing.JLabel lblSinglelineComment;
    private javax.swing.JLabel lblSlonyIPath;
    private javax.swing.JLabel lblSlonygHelpPath;
    private javax.swing.JLabel lblSlowProcessColor;
    private javax.swing.JLabel lblSysSchema;
    private javax.swing.JLabel lblThousandSeparator;
    private javax.swing.JLabel lblUseNullStrInstaedOfNullResult;
    private javax.swing.JLabel lblUserLanguage;
    private javax.swing.JPanel pnlBinPath;
    private javax.swing.JPanel pnlButtons;
    private javax.swing.JPanel pnlCaret;
    private javax.swing.JPanel pnlColor;
    private javax.swing.JPanel pnlContent;
    private javax.swing.JPanel pnlDBDesigner;
    private javax.swing.JPanel pnlDisplay;
    private javax.swing.JPanel pnlFavourite;
    private javax.swing.JPanel pnlGuruHint;
    private javax.swing.JPanel pnlHelpPath;
    private javax.swing.JPanel pnlHistoryFile;
    private javax.swing.JPanel pnlLogLevel;
    private javax.swing.JPanel pnlLogging;
    private javax.swing.JPanel pnlMacros;
    private javax.swing.JPanel pnlProperty;
    private javax.swing.JPanel pnlQueryEditor;
    private javax.swing.JPanel pnlQueryFile;
    private javax.swing.JPanel pnlResultGrid;
    private javax.swing.JPanel pnlServerStatus;
    private javax.swing.JPanel pnlSqlSyntaxHighlight;
    private javax.swing.JPanel pnlUIMiscellaneous;
    private javax.swing.JPanel pnlUserInterface;
    private javax.swing.JRadioButton rbDebugNotForNormal;
    private javax.swing.JRadioButton rbErrorOnly;
    private javax.swing.JRadioButton rbErrorWarn;
    private javax.swing.JRadioButton rbErrorWarnInfo;
    private javax.swing.JRadioButton rbNoLog;
    private javax.swing.JSpinner spMaxQueryCount;
    private javax.swing.JSpinner spMaxQuerySize;
    private javax.swing.JScrollPane spMenu;
    private javax.swing.JSpinner spinInActivityTimeout;
    private javax.swing.JTable tblObjects;
    private javax.swing.ButtonGroup tbnGroupLogLevel;
    private javax.swing.JTree treeMenu;
    private javax.swing.JTextField txfdActiveProcessColor;
    private javax.swing.JTextField txfdBackground;
    private javax.swing.JTextField txfdBlockedProcessColor;
    private javax.swing.JTextField txfdCaretColor;
    private javax.swing.JTextField txfdCustomBackground;
    private javax.swing.JTextField txfdDecimalMark;
    private javax.swing.JTextField txfdDoubleQuotedString;
    private javax.swing.JTextField txfdFavouriteFilePath;
    private javax.swing.JTextField txfdForeground;
    private javax.swing.JTextField txfdGPBinPath;
    private javax.swing.JTextField txfdGgHelpPath;
    private javax.swing.JTextField txfdHGBinPath;
    private javax.swing.JTextField txfdHGDataPath;
    private javax.swing.JTextField txfdHgdbAdminManual;
    private javax.swing.JTextField txfdHgdbManual;
    private javax.swing.JTextField txfdHistoryFilePath;
    private javax.swing.JTextField txfdIdentifer;
    private javax.swing.JTextField txfdIdleProcessColor;
    private javax.swing.JTextField txfdIndentChar;
    private javax.swing.JTextField txfdLogPath;
    private javax.swing.JTextField txfdMacroFilePath;
    private javax.swing.JTextField txfdMarginBackground;
    private javax.swing.JTextField txfdMaxCharPerColumn;
    private javax.swing.JTextField txfdMultilineComment;
    private javax.swing.JTextField txfdNumber;
    private javax.swing.JTextField txfdOperator;
    private javax.swing.JTextField txfdPgAdminHelpPath;
    private javax.swing.JTextField txfdPgHelpPath;
    private javax.swing.JTextField txfdRowCountLimit1;
    private javax.swing.JTextField txfdScriptPath;
    private javax.swing.JTextField txfdSingleLineComment;
    private javax.swing.JTextField txfdSingleQuotedString;
    private javax.swing.JTextField txfdSlonyHelpPath;
    private javax.swing.JTextField txfdSlonyIPath;
    private javax.swing.JTextField txfdSlowProcessColor;
    private javax.swing.JTextField txfdSqlDoc;
    private javax.swing.JTextField txfdSysSchema;
    private javax.swing.JTextField txfdThousandSeparator;
    // End of variables declaration//GEN-END:variables
}
