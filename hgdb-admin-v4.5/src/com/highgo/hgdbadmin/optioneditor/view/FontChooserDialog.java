/* ------------------------------------------------ 
* 
* File: FontChooseDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\optioneditor\view\FontChooseDialog.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.optioneditor.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.optioneditor.model.FontAttributeDTO;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class FontChooserDialog extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private String EXAMPLE = constBundle.getString("fontSample");
    
    private FontAttributeDTO attrset;   
    private boolean isFinish;
    
    /**
     * Creates new form FontChooserDialog
     *
     * @param parent
     * @param modal
     * @param attrset
     */
    public FontChooserDialog(JDialog parent, boolean modal, FontAttributeDTO attrset)
    {
        super(parent, modal);
        this.attrset = attrset;
        isFinish = false;
        this.initComponents();
        
        String[] fontFamily = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        logger.debug("fontFamily=" + fontFamily.length);
        cbbFontFamily.setModel(new DefaultComboBoxModel(fontFamily));
        
        String[] patternArray = new String[]
        {
            constBundle.getString("normal"), constBundle.getString("bold"), 
            constBundle.getString("italic"), constBundle.getString("boldItalic")
        };
        cbbFontStyle.setModel(new DefaultComboBoxModel(patternArray));
        cbbFontStyle.setSelectedItem(constBundle.getString("normal"));

        String[] sizeArray = new String[]
        {
            "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24", "26", "28", "36", "48"            
        };
        cbbFontSize.setModel(new DefaultComboBoxModel(sizeArray));
        
        String[] scriptArray = new String[]
        {
            constBundle.getString("chinese"),
            constBundle.getString("westernEurope")                                
//            ,constBundle.getString("hebrew"), constBundle.getString("arabic"),
//            constBundle.getString("greek"), constBundle.getString("turkic"), constBundle.getString("poro"),
//            constBundle.getString("centralCharacter"), constBundle.getString("cyrilVin"), constBundle.getString("vietnamese"),
        };
        cbScript.setModel(new DefaultComboBoxModel(scriptArray));       
        cbScript.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbScriptStateChanged(e);                
            }
        });
        
        if (attrset != null)
        {
            cbbFontFamily.setSelectedItem(attrset.getFontFamily());
            if (attrset.getStyle() == Font.PLAIN)
            {
                cbbFontStyle.setSelectedItem(constBundle.getString("normal"));
            } else if (attrset.getStyle() == Font.BOLD)
            {
                cbbFontStyle.setSelectedItem(constBundle.getString("bold"));
            } else if (attrset.getStyle() == Font.ITALIC)
            {
                cbbFontStyle.setSelectedItem(constBundle.getString("italic"));
            } else if (attrset.getStyle() == (Font.BOLD + Font.ITALIC))
            {
                cbbFontStyle.setSelectedItem(constBundle.getString("boldItalic"));
            }
            cbbFontSize.setSelectedItem(String.valueOf(attrset.getFontSize()));
            txfdColor.setBackground(attrset.getForeground());
            tpExample.setText("");
            SyntaxController.getInstance().insert(tpExample.getDocument(), EXAMPLE, attrset);
        } else
        {
            attrset = new FontAttributeDTO();
            this.setStyle();
        }
        
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancleActionPerformed(e);
            }
        });
        btnOk.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkActionPerformed(e);
            }
        });
     
        txfdColor.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent evt)
            {
                txfdColorMousePressed(evt);
            }
        });

        cbbFontFamily.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                setStyle();
            }
        });
        cbbFontStyle.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                setStyle();
            }
        });
        cbbFontSize.addItemListener(new ItemListener()
        {

            @Override
            public void itemStateChanged(ItemEvent e)
            {
                setStyle();
            }
        });
        cbUnderLine.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                setStyle();
            }
        });
        cbDelLine.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                setStyle();
            }
        });
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pnlContent = new javax.swing.JPanel();
        cbbFontSize = new javax.swing.JComboBox();
        cbbFontStyle = new javax.swing.JComboBox();
        cbbFontFamily = new javax.swing.JComboBox();
        lblFontStyle = new javax.swing.JLabel();
        lblFontPattern = new javax.swing.JLabel();
        lblFontSize = new javax.swing.JLabel();
        pnlEffect = new javax.swing.JPanel();
        cbDelLine = new javax.swing.JCheckBox();
        cbUnderLine = new javax.swing.JCheckBox();
        lblColor = new javax.swing.JLabel();
        txfdColor = new javax.swing.JTextField();
        pnlExample = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tpExample = new javax.swing.JTextPane();
        lblScript = new javax.swing.JLabel();
        cbScript = new javax.swing.JComboBox();
        pnlButton = new javax.swing.JPanel();
        btnOk = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("font"));
        setIconImage(null);
        setIconImages(null);
        setResizable(false);

        lblFontStyle.setText(constBundle.getString("fontStyle"));

        lblFontPattern.setText(constBundle.getString("fontPattern"));

        lblFontSize.setText(constBundle.getString("fontSize"));

        pnlEffect.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("effect")));

        cbDelLine.setText(constBundle.getString("delLine"));
        cbDelLine.setEnabled(false);

        cbUnderLine.setText(constBundle.getString("underLine"));
        cbUnderLine.setEnabled(false);

        lblColor.setText(constBundle.getString("colors"));

        txfdColor.setEditable(false);
        txfdColor.setBackground(new java.awt.Color(0, 0, 0));
        txfdColor.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 255, 255)));

        javax.swing.GroupLayout pnlEffectLayout = new javax.swing.GroupLayout(pnlEffect);
        pnlEffect.setLayout(pnlEffectLayout);
        pnlEffectLayout.setHorizontalGroup(
            pnlEffectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEffectLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlEffectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdColor)
                    .addGroup(pnlEffectLayout.createSequentialGroup()
                        .addGroup(pnlEffectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbDelLine)
                            .addComponent(lblColor)
                            .addComponent(cbUnderLine))
                        .addGap(0, 12, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlEffectLayout.setVerticalGroup(
            pnlEffectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlEffectLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(cbDelLine)
                .addGap(10, 10, 10)
                .addComponent(cbUnderLine)
                .addGap(10, 10, 10)
                .addComponent(lblColor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txfdColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pnlExample.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("example")));

        tpExample.setEditable(false);
        tpExample.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        tpExample.setBorder(null);
        tpExample.setText(EXAMPLE);
        jScrollPane1.setViewportView(tpExample);

        javax.swing.GroupLayout pnlExampleLayout = new javax.swing.GroupLayout(pnlExample);
        pnlExample.setLayout(pnlExampleLayout);
        pnlExampleLayout.setHorizontalGroup(
            pnlExampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        pnlExampleLayout.setVerticalGroup(
            pnlExampleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlExampleLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblScript.setText(constBundle.getString("script")
        );

        javax.swing.GroupLayout pnlContentLayout = new javax.swing.GroupLayout(pnlContent);
        pnlContent.setLayout(pnlContentLayout);
        pnlContentLayout.setHorizontalGroup(
            pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbbFontFamily, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlEffect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFontStyle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlContentLayout.createSequentialGroup()
                        .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbbFontStyle, 0, 133, Short.MAX_VALUE)
                            .addComponent(lblFontPattern, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbbFontSize, 0, 128, Short.MAX_VALUE)
                            .addComponent(lblFontSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(cbScript, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(pnlContentLayout.createSequentialGroup()
                        .addComponent(lblScript)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(pnlExample, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlContentLayout.setVerticalGroup(
            pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlContentLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFontStyle)
                    .addComponent(lblFontPattern)
                    .addComponent(lblFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbbFontFamily, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbFontStyle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbbFontSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(pnlContentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(pnlContentLayout.createSequentialGroup()
                        .addComponent(pnlExample, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblScript)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbScript, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnlEffect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10))
        );

        getContentPane().add(pnlContent, java.awt.BorderLayout.CENTER);

        btnOk.setText(constBundle.getString("ok"));

        btnCancle.setText(constBundle.getString("cancle"));

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
                .addGap(248, 248, 248)
                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk)
                    .addComponent(btnCancle))
                .addGap(10, 10, 10))
        );

        getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbScriptStateChanged(ItemEvent evt)
    {
        Object o = cbScript.getSelectedItem();
        if (o == null)
        {
            return;
        }
        if (o.equals(constBundle.getString("westernEurope")))
        {
            EXAMPLE = "AaBbYyZz";
        } else
        {
            EXAMPLE = constBundle.getString("fontSample");
        }
        this.setStyle();
    }
    private void btnCancleActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        this.dispose();
    }
    private void btnOkActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        isFinish = true;
        this.dispose();
    }
    private void txfdColorMousePressed(MouseEvent evt)
    {
        logger.info("press");
        Color newColor = JColorChooser.showDialog(
                this,
                constBundle.getString("chooseFontColor"),
                txfdColor.getBackground());
        if (newColor != null)
        {
            txfdColor.setBackground(newColor);
            this.setStyle();
        }
    }    

    private void setStyle()
    {
        this.setAttributeSet();
        tpExample.setText("");
        SyntaxController.getInstance().insert(tpExample.getDocument(), EXAMPLE, attrset);
    }

    private void setAttributeSet()
    {
        attrset.setFontFamily(cbbFontFamily.getSelectedItem().toString());
        String fontStyle = cbbFontStyle.getSelectedItem().toString();
        if (fontStyle.equals(constBundle.getString("boldItalic")))
        {
            attrset.setBold(true);
            attrset.setItalic(true);
        } else
        {
            attrset.setBold(fontStyle.equals(constBundle.getString("bold")));
            attrset.setItalic(fontStyle.equals(constBundle.getString("italic")));
        }
        attrset.setFontSize(Integer.valueOf(cbbFontSize.getSelectedItem().toString()));
//        attrset.setStrikeThrough(cbDelLine.isSelected());
//        attrset.setUnderline(cbUnderLine.isSelected());
        attrset.setForeground(txfdColor.getBackground());
        attrset.setBackground(UIManager.getDefaults().getColor("Label.background"));
    }

    public boolean isFinish()
    {
        return isFinish;
    }
    
    public FontAttributeDTO getFontAttributeDTO()
    {
        return attrset;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnOk;
    private javax.swing.JCheckBox cbDelLine;
    private javax.swing.JComboBox cbScript;
    private javax.swing.JCheckBox cbUnderLine;
    private javax.swing.JComboBox cbbFontFamily;
    private javax.swing.JComboBox cbbFontSize;
    private javax.swing.JComboBox cbbFontStyle;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblColor;
    private javax.swing.JLabel lblFontPattern;
    private javax.swing.JLabel lblFontSize;
    private javax.swing.JLabel lblFontStyle;
    private javax.swing.JLabel lblScript;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlContent;
    private javax.swing.JPanel pnlEffect;
    private javax.swing.JPanel pnlExample;
    private javax.swing.JTextPane tpExample;
    private javax.swing.JTextField txfdColor;
    // End of variables declaration//GEN-END:variables
}
