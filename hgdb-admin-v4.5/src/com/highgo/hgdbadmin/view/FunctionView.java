/* ------------------------------------------------ 
* 
* File: FunctionView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\FunctionView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.Compare;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.FunctionArgDTO;
import com.highgo.hgdbadmin.model.FunctionInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.SysVariableInfoDTO;
import com.highgo.hgdbadmin.model.DatatypeDTO;
import com.highgo.hgdbadmin.model.VariableInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 * @description for function and procedure
 * 
 * @author Liu Yuanyuan
 */
public class FunctionView extends JDialog
{
    final Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private final ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private TreeEnum.TreeNode type;
    private HelperInfoDTO helperInfo;
    private FunctionInfoDTO fun;    
    private boolean isSuccess=false;
    
    private SysVariableInfoDTO currentVariable;
    
    //add
    //function must specify Name, return type, language(default is sql), and it's source code.(arguments is optional)
    public FunctionView(JFrame parent, boolean modal, TreeEnum.TreeNode type, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.type = type;
        this.helperInfo = helperInfo;
        this.fun = null;
        initComponents();
        jtp.setEnabledAt(6, false);
        this.setTitle(TreeEnum.TreeNode.FUNCTION == type
                ? constBundle.getString("addFunction") : constBundle.getString("addProcedure"));
        this.setNormalInfo(false);
        this.disableForProcrdure();
        this.limitUserPrivilege();
        
        //define action      
        KeyAdapter btnOkEnable = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnabledForAdd();
            }
        };
        txfdName.addKeyListener(btnOkEnable);
        cbbReturnType.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnabledForAdd();
            }
        });
        cbbLanguage.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnabledForAdd();
            }
        });
        txfdObjectFile.addKeyListener(btnOkEnable);
        ttarSrc.addKeyListener(btnOkEnable);
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });
    }
    //property
    public FunctionView(JFrame parent, boolean modal, FunctionInfoDTO fun)
    {
        super(parent, modal);
        this.fun = fun;
        this.helperInfo = fun.getHelperInfo();
        this.type = fun.getType();
        initComponents();
        jtp.setEnabledAt(6, false);   
        this.setTitle((TreeEnum.TreeNode.FUNCTION == fun.getType()
                ? constBundle.getString("functions") : constBundle.getString("procedures"))
                + " " + fun.getName());//fun.getSimpleName()
        this.setNormalInfo(true);
        this.setProperty();
        this.disableForProcrdure();

        //define action
        KeyAdapter btnOkEnableKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnabledForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnableKeyAdapter);
        ItemListener btnOkItemListener = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnabledForChange();
            }
        };
        cbbOwner.addItemListener(btnOkItemListener);
        txarComment.addKeyListener(btnOkEnableKeyAdapter);
        cbbLanguage.addItemListener(btnOkItemListener);
        txfdObjectFile.addKeyListener(btnOkEnableKeyAdapter);
        ttarSrc.addKeyListener(btnOkEnableKeyAdapter);
        cbbVolatile.addItemListener(btnOkItemListener);
        cbStrict.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        cbSecurityDefiner.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        txfdCost.addKeyListener(btnOkEnableKeyAdapter);        
        DefaultTableModel aclModel = (DefaultTableModel) tblPrivileges.getModel();
        aclModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        DefaultTableModel varModel = (DefaultTableModel) tblVariables.getModel();
        varModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        DefaultTableModel argModel = (DefaultTableModel) tblArgs.getModel();
        argModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });
    }

    private  void limitUserPrivilege(){
        ((DefaultTableModel) tblPrivileges.getModel()).removeRow(0);
    }
    
    private void setProperty()
    {
        logger.debug("Enter");
        cbbReturnType.setEnabled(false);//change return type need to drop function then create or replace function.
        cbReturnSet.setEnabled(false);
        cbWindow.setEnabled(false);
        cbbDataType.setEnabled(false);
        rbIn.setEnabled(false);
        rbOut.setEnabled(false);
        rbInOut.setEnabled(false);
        rbVariadic.setEnabled(false);
        btnRemoveArg.setEnabled(false);
        btnAddArg.setEnabled(false);
        txfdName.setText(fun.getSimpleName());
        txfdOID.setText(fun.getOid().toString());
        cbbOwner.setSelectedItem(fun.getOwner());
        txarComment.setText(fun.getComment());
        txfdArg.setText(fun.getAllArgDefine()); //txfdArg.setText(fun.getInArgTypes());
        
        logger.debug("ReturnType=" + fun.getReturnType());
        if (fun.getReturnType() == null && fun.getReturnType().isEmpty())
        {
            cbbReturnType.setSelectedIndex(-1);
        } else
        {
            for (int i = 0; i < cbbReturnType.getItemCount(); i++)
            {
                if (fun.getReturnType().equalsIgnoreCase(cbbReturnType.getItemAt(i).toString()))
                {
                    cbbReturnType.setSelectedIndex(i);
                    break;
                }
            }
        }
        
        cbbLanguage.setSelectedItem(fun.getLanguage());        
        if (fun.getLanguage().equals("c"))
        {
            txfdObjectFile.setEditable(true);
            txfdLinkSymbol.setEditable(true);
            ttarSrc.setEditable(false);
            
            txfdObjectFile.setText(fun.getBin());
            txfdLinkSymbol.setText(fun.getSrc());
        } else
        {
            txfdObjectFile.setEditable(false);
            txfdLinkSymbol.setEditable(false);
            ttarSrc.setEditable(true);
            
            ttarSrc.setText(fun.getSrc());
        }
        cbbVolatile.setSelectedItem(fun.getVolatiles());
        cbReturnSet.setSelected(fun.isReturnSet());
        cbStrict.setSelected(fun.isStrict());
        cbSecurityDefiner.setSelected(fun.isSecurityDefiner());
        cbWindow.setSelected(fun.isWindow());
        cbLeakProof.setSelected(fun.isLeafProof());
        cbbParallel.setSelectedItem(fun.getParallel());
        txfdCost.setText(String.valueOf(fun.getCost()));
        //txfdEstimateRow.setText(String.valueOf(fun.get));
        //cbLeakProof       
        List<FunctionArgDTO> arglist = fun.getArgList();
        if (arglist != null)
        {            
            tblArgs.setEnabled(false);
            DefaultTableModel argModel = (DefaultTableModel) tblArgs.getModel();
            for (FunctionArgDTO arg : arglist)
            {
                argModel.addRow(new String[]
                {
                    arg.getType(),
                    arg.getMode(),
                    arg.getName(),
                    arg.getDefaultValue()
                });
            }
        }  
        
        String acl = fun.getAcl();
        DefaultTableModel privilegeModel = (DefaultTableModel) tblPrivileges.getModel();
        //clear table privilege
        privilegeModel.removeRow(0);
        if (acl != null && !acl.isEmpty() && !acl.equals("{}"))
        {            
            acl = acl.substring(1, acl.length() - 1);
            String[] privilegeArray = acl.split(",");
            for (String privilege : privilegeArray)
            {
                logger.info("privilege:" + privilege);
                String[] pArray = privilege.split("=");
                String[] p = new String[2];
                if (pArray[0] == null || pArray[0].isEmpty())
                {
                    p[0] = "public";
                } else
                {
                    p[0] = pArray[0];
                }
                p[1] = pArray[1].split("/")[0];
                privilegeModel.addRow(p);
            }
        }
        List<VariableInfoDTO> vlist = fun.getVariableList();
        if (vlist != null && !vlist.isEmpty())
        {
            DefaultTableModel vModel = (DefaultTableModel) tblVariables.getModel();
            for (VariableInfoDTO v : vlist)
            {
                String[] sl = new String[2];
                sl[0] = v.getName();
                sl[1] = v.getValue();
                vModel.addRow(sl);
            }
        }
        List<SecurityLabelInfoDTO> slist = fun.getSecurityLabelList();
        if (slist != null)
        {
            DefaultTableModel sModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[2];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                sModel.addRow(sl);
            }
        } 
    }
    private void setNormalInfo(boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
//        cbbSchema.setModel(new DefaultComboBoxModel(tc.getOptionArrary(helperInfo,"schema",null)));
        cbbSchema.setModel(new DefaultComboBoxModel(new String[]{helperInfo.getSchema()}));
        cbbSchema.setSelectedItem(helperInfo.getSchema());
        try
        {
            cbbOwner.setModel(new DefaultComboBoxModel((String[]) tc.getItemsArrary(helperInfo, "owner")));
            if(isChange)
            {
                cbbOwner.removeItemAt(0);
            }else
            {
               cbbOwner.setSelectedIndex(0);
            }                    
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            DatatypeDTO[] types = tc.getDataType4Function(helperInfo);
            cbbReturnType.setModel(new DefaultComboBoxModel(types));
            cbbReturnType.setSelectedIndex(-1);
            cbbDataType.setModel(new DefaultComboBoxModel(types));
            cbbDataType.setSelectedIndex(-1);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbLanguage.setModel(new DefaultComboBoxModel(tc.getLanguageList(helperInfo).toArray()));
            cbbLanguage.setSelectedItem("sql");
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }   

        //must after cbbReturnType and cbbLanguage
        cbbReturnType.addActionListener(new ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbbReturnTypeActionPerformed(evt);
            }
        });
        
        try
        {
            cbbRole.setModel(new DefaultComboBoxModel(tc.getRoles(helperInfo)));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbVariableNames.setModel(new DefaultComboBoxModel(tc.getVariables(helperInfo, "database")));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        //normal action
        txfdCost.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent e)
            {
                txfdIntegerValueKeyTyped(e);
            }
        });
        //for parameters
        tblArgs.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent e)
            {
                tblArgsMousePressAction(e);
            }
        });
         ActionListener modeSelect = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                txfdDefaultValueEnable(e);
            }
        };
        rbIn.addActionListener(modeSelect);
        rbOut.addActionListener(modeSelect);
        rbInOut.addActionListener(modeSelect);
        rbVariadic.addActionListener(modeSelect);      
        btnRemoveArg.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnRemoveArgAction(e);
            }
        });
        btnAddArg.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnAddArgAction(e);
            }
        });
        btnChangeArg.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnChangeArgAction(e);
            }
        });
        
    }
    
    private void disableForProcrdure()
    {
        logger.debug(type);
        if (TreeEnum.TreeNode.FUNCTION == type) {
            return;
        }
        lbReturnType.setEnabled(false);
        cbbReturnType.setEnabled(false);
        cbbReturnType.setSelectedIndex(-1);
        
        lbVolatile.setEnabled(false);
        cbbVolatile.setEnabled(false);
        
        lbReturnSet.setEnabled(false);
        cbReturnSet.setEnabled(false);
        
        lbStrict.setEnabled(false);
        cbStrict.setEnabled(false);
        
        lbWindow.setEnabled(false);
        cbWindow.setEnabled(false);
        
        lbParallel.setEnabled(false);
        cbbParallel.setEnabled(false);
        cbbParallel.setSelectedIndex(-1);
        
        lbCost.setEnabled(false);
        txfdCost.setEnabled(false);
        txfdCost.setText("");
        
        lbEstimateRow.setEnabled(false);
        txfdEstimateRow.setEnabled(false);
        txfdEstimateRow.setText("");
        
        lbLeakProof.setEnabled(false);
        cbLeakProof.setEnabled(false); 
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        bgMode = new javax.swing.ButtonGroup();
        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        lblUseSlony = new javax.swing.JLabel();
        txfdUseSlony = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        cbbSchema = new javax.swing.JComboBox();
        lblSchema = new javax.swing.JLabel();
        pnlDefine = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txfdArg = new javax.swing.JTextField();
        lbReturnType = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbbLanguage = new javax.swing.JComboBox();
        jScrollPane5 = new javax.swing.JScrollPane();
        ttarSrc = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        cbbReturnType = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txfdObjectFile = new javax.swing.JTextField();
        txfdLinkSymbol = new javax.swing.JTextField();
        pnlOption = new javax.swing.JPanel();
        lbVolatile = new javax.swing.JLabel();
        cbbVolatile = new javax.swing.JComboBox();
        cbReturnSet = new javax.swing.JCheckBox();
        cbStrict = new javax.swing.JCheckBox();
        cbSecurityDefiner = new javax.swing.JCheckBox();
        cbWindow = new javax.swing.JCheckBox();
        txfdCost = new javax.swing.JTextField();
        txfdEstimateRow = new javax.swing.JTextField();
        cbLeakProof = new javax.swing.JCheckBox();
        lbReturnSet = new javax.swing.JLabel();
        lbStrict = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbWindow = new javax.swing.JLabel();
        lbCost = new javax.swing.JLabel();
        lbEstimateRow = new javax.swing.JLabel();
        lbLeakProof = new javax.swing.JLabel();
        cbbParallel = new javax.swing.JComboBox();
        lbParallel = new javax.swing.JLabel();
        pnlArg = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblArgs = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        cbbDataType = new javax.swing.JComboBox();
        rbIn = new javax.swing.JRadioButton();
        rbOut = new javax.swing.JRadioButton();
        rbInOut = new javax.swing.JRadioButton();
        rbVariadic = new javax.swing.JRadioButton();
        txfdArgName = new javax.swing.JTextField();
        txfdDefaultValue = new javax.swing.JTextField();
        btnAddArg = new javax.swing.JButton();
        btnRemoveArg = new javax.swing.JButton();
        btnChangeArg = new javax.swing.JButton();
        pnlVariables = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblVariables = new javax.swing.JTable();
        lblVariableValue = new javax.swing.JLabel();
        btnAddChangeVariable = new javax.swing.JButton();
        btnRemoveVariable = new javax.swing.JButton();
        lblVariableName = new javax.swing.JLabel();
        txfdVariableValue = new javax.swing.JTextField();
        cbbVariableNames = new javax.swing.JComboBox();
        cbValue = new javax.swing.JCheckBox();
        pnlPrivileges = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPrivileges = new javax.swing.JTable();
        btnAddChangePrivilege = new javax.swing.JButton();
        btnRemovePrivilege = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRole = new javax.swing.JLabel();
        cbbRole = new javax.swing.JComboBox();
        cbExecute = new javax.swing.JCheckBox();
        cbOptionExecute = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addSchema"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/function.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

lblUseSlony.setText(constBundle.getString("useSlony"));

txfdUseSlony.setEditable(false);

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane1.setViewportView(txarComment);

cbbSchema.setEnabled(false);

lblSchema.setText(constBundle.getString("schema"));

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txfdUseSlony, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblComment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSchema, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbbSchema, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txfdOID)
                    .addComponent(txfdName)
                    .addComponent(cbbOwner, 0, 407, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE))))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblSchema)
                .addComponent(cbbSchema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblComment))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblUseSlony)
                .addComponent(txfdUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(34, Short.MAX_VALUE))
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    jLabel1.setText(constBundle.getString("arguments")
    );

    txfdArg.setEditable(false);

    lbReturnType.setText(constBundle.getString("returnType")
    );

    jLabel3.setText(constBundle.getString("languages")
    );

    cbbLanguage.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbLanguageItemStateChanged(evt);
        }
    });

    ttarSrc.setColumns(20);
    ttarSrc.setRows(5);
    jScrollPane5.setViewportView(ttarSrc);

    jLabel12.setText(constBundle.getString("src"));

    cbbReturnType.setMaximumRowCount(12);

    jLabel17.setText(constBundle.getString("objectFile")
    );

    jLabel18.setText(constBundle.getString("linkSymbol")
    );

    txfdObjectFile.setEditable(false);

    txfdLinkSymbol.setEditable(false);

    javax.swing.GroupLayout pnlDefineLayout = new javax.swing.GroupLayout(pnlDefine);
    pnlDefine.setLayout(pnlDefineLayout);
    pnlDefineLayout.setHorizontalGroup(
        pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefineLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbReturnType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                .addComponent(txfdArg)
                .addComponent(cbbLanguage, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbbReturnType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txfdObjectFile)
                .addComponent(txfdLinkSymbol))
            .addContainerGap())
    );
    pnlDefineLayout.setVerticalGroup(
        pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefineLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel1)
                .addComponent(txfdArg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lbReturnType)
                .addComponent(cbbReturnType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel3)
                .addComponent(cbbLanguage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel17)
                .addComponent(txfdObjectFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel18)
                .addComponent(txfdLinkSymbol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlDefineLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel12))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("defination"), pnlDefine);

    lbVolatile.setText(constBundle.getString("volatile"));

    cbbVolatile.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "VOLATILE", "STABLE", "IMMUTABLE" }));

    txfdEstimateRow.setEnabled(false);

    lbReturnSet.setText(constBundle.getString("returnSet"));

    lbStrict.setText(constBundle.getString("strict")
    );

    jLabel7.setText(constBundle.getString("securityDefiner"));

    lbWindow.setText(constBundle.getString("windowFunction"));

    lbCost.setText(constBundle.getString("cost"));

    lbEstimateRow.setText(constBundle.getString("row"));

    lbLeakProof.setText(constBundle.getString("leakProof"));

    cbbParallel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "UNSAFE", "RESTRICTED", "SAFE" }));

    lbParallel.setText(constBundle.getString("funcParallel"));

    javax.swing.GroupLayout pnlOptionLayout = new javax.swing.GroupLayout(pnlOption);
    pnlOption.setLayout(pnlOptionLayout);
    pnlOptionLayout.setHorizontalGroup(
        pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlOptionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lbParallel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbWindow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbCost, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbEstimateRow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbLeakProof, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbVolatile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbReturnSet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbStrict, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbbVolatile, 0, 405, Short.MAX_VALUE)
                .addComponent(txfdCost)
                .addComponent(txfdEstimateRow)
                .addComponent(cbbParallel, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlOptionLayout.createSequentialGroup()
                    .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbLeakProof)
                        .addComponent(cbWindow)
                        .addComponent(cbSecurityDefiner)
                        .addComponent(cbStrict)
                        .addComponent(cbReturnSet))
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlOptionLayout.setVerticalGroup(
        pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlOptionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbbVolatile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlOptionLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(lbVolatile, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(10, 10, 10)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbReturnSet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbReturnSet, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(10, 10, 10)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbStrict, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbStrict, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(10, 10, 10)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbSecurityDefiner)
                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbWindow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbWindow, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(10, 10, 10)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lbParallel, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbbParallel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(txfdCost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlOptionLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(lbCost, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(txfdEstimateRow, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lbEstimateRow, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbLeakProof, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbLeakProof, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(144, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("options")
        , pnlOption);

    tblArgs.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", "", "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false, true, true
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    jScrollPane2.setViewportView(tblArgs);
    if (tblArgs.getColumnModel().getColumnCount() > 0)
    {
        tblArgs.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("type"));
        tblArgs.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("schema"));
        tblArgs.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("name")
        );
        tblArgs.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("defaultValues")
        );
    }

    jLabel13.setText(constBundle.getString("dataType"));

    jLabel14.setText(constBundle.getString("schema"));

    jLabel15.setText(constBundle.getString("argName")
    );

    jLabel16.setText(constBundle.getString("defaultValues"));

    bgMode.add(rbIn);
    rbIn.setSelected(true);
    rbIn.setText("IN");

    bgMode.add(rbOut);
    rbOut.setText("OUT");

    bgMode.add(rbInOut);
    rbInOut.setText("IN OUT");

    bgMode.add(rbVariadic);
    rbVariadic.setText("VARIADIC");

    btnAddArg.setText(constBundle.getString("add"));

    btnRemoveArg.setText(constBundle.getString("remove"));

    btnChangeArg.setText(constBundle.getString("change"));

    javax.swing.GroupLayout pnlArgLayout = new javax.swing.GroupLayout(pnlArg);
    pnlArg.setLayout(pnlArgLayout);
    pnlArgLayout.setHorizontalGroup(
        pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlArgLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
                .addGroup(pnlArgLayout.createSequentialGroup()
                    .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbbDataType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txfdArgName)
                        .addGroup(pnlArgLayout.createSequentialGroup()
                            .addComponent(rbIn)
                            .addGap(10, 10, 10)
                            .addComponent(rbOut)
                            .addGap(10, 10, 10)
                            .addComponent(rbInOut)
                            .addGap(10, 10, 10)
                            .addComponent(rbVariadic)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(txfdDefaultValue)))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlArgLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(btnAddArg)
                    .addGap(10, 10, 10)
                    .addComponent(btnChangeArg)
                    .addGap(10, 10, 10)
                    .addComponent(btnRemoveArg)))
            .addContainerGap())
    );
    pnlArgLayout.setVerticalGroup(
        pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlArgLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
            .addGap(10, 10, 10)
            .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnRemoveArg)
                .addComponent(btnChangeArg)
                .addComponent(btnAddArg))
            .addGap(10, 10, 10)
            .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel13)
                .addComponent(cbbDataType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlArgLayout.createSequentialGroup()
                    .addGap(40, 40, 40)
                    .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(txfdArgName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16)
                        .addComponent(txfdDefaultValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(pnlArgLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlArgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbOut)
                        .addComponent(rbInOut)
                        .addComponent(rbVariadic)
                        .addComponent(rbIn)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGap(10, 10, 10))
    );

    jtp.addTab(constBundle.getString("arguments")
        , pnlArg);

    pnlVariables.setBackground(new java.awt.Color(255, 255, 255));
    pnlVariables.setPreferredSize(new java.awt.Dimension(520, 500));

    tblVariables.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblVariables.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblVariables.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblVariablesMousePressed(evt);
        }
    });
    jScrollPane6.setViewportView(tblVariables);
    if (tblVariables.getColumnModel().getColumnCount() > 0)
    {
        tblVariables.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("name"));
        tblVariables.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("value"));
    }

    lblVariableValue.setText(constBundle.getString("variableValue"));

    btnAddChangeVariable.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeVariable.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeVariableActionPerformed(evt);
        }
    });

    btnRemoveVariable.setText(constBundle.getString("remove"));
    btnRemoveVariable.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveVariableActionPerformed(evt);
        }
    });

    lblVariableName.setText(constBundle.getString("variableName"));

    cbbVariableNames.setEditable(true);
    cbbVariableNames.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbVariableNamesItemStateChanged(evt);
        }
    });

    cbValue.setBackground(new java.awt.Color(255, 255, 255));
    cbValue.setEnabled(false);

    javax.swing.GroupLayout pnlVariablesLayout = new javax.swing.GroupLayout(pnlVariables);
    pnlVariables.setLayout(pnlVariablesLayout);
    pnlVariablesLayout.setHorizontalGroup(
        pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlVariablesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblVariableName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(18, 18, 18)
                    .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbbVariableNames, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlVariablesLayout.createSequentialGroup()
                            .addComponent(cbValue)
                            .addGap(10, 10, 10)
                            .addComponent(txfdVariableValue))))
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlVariablesLayout.setVerticalGroup(
        pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlVariablesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVariableName)
                .addComponent(cbbVariableNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblVariableValue)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txfdVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbValue)))
            .addGap(10, 10, 10))
    );

    jtp.addTab(constBundle.getString("variables"), pnlVariables);

    pnlPrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlPrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

    tblPrivileges.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {
            {"public", "X"}
        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblPrivileges.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblPrivileges.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblPrivilegesMousePressed(evt);
        }
    });
    jScrollPane4.setViewportView(tblPrivileges);
    if (tblPrivileges.getColumnModel().getColumnCount() > 0)
    {
        tblPrivileges.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("user")+"/"+constBundle.getString("group"));
        tblPrivileges.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("privileges"));
    }

    btnAddChangePrivilege.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivilege.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangePrivilegeActionPerformed(evt);
        }
    });

    btnRemovePrivilege.setText(constBundle.getString("remove"));
    btnRemovePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemovePrivilegeActionPerformed(evt);
        }
    });

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("privileges"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("宋体", 0, 12), new java.awt.Color(204, 204, 204))); // NOI18N

    lblRole.setText(constBundle.getString("role"));

    cbbRole.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbRoleItemStateChanged(evt);
        }
    });

    cbExecute.setText("EXECUTE");
    cbExecute.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbExecuteActionPerformed(evt);
        }
    });

    cbOptionExecute.setText("WITH GRANT OPTION");
    cbOptionExecute.setEnabled(false);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbRole, 0, 384, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(cbExecute)
                    .addGap(171, 171, 171)
                    .addComponent(cbOptionExecute)))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblRole))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbOptionExecute)
                .addComponent(cbExecute))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlPrivilegesLayout = new javax.swing.GroupLayout(pnlPrivileges);
    pnlPrivileges.setLayout(pnlPrivilegesLayout);
    pnlPrivilegesLayout.setHorizontalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE))
            .addContainerGap())
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(btnAddChangePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnlPrivilegesLayout.setVerticalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("privileges"), pnlPrivileges);
    pnlPrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    lblProvider.setText(constBundle.getString("provider"));

    tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    ));
    jScrollPane3.setViewportView(tblSecurityLabels);
    if (tblSecurityLabels.getColumnModel().getColumnCount() > 0)
    {
        tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveSecurityLabelActionPerformed(evt);
        }
    });

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeSecurityLabelActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                    .addGap(10, 10, 10))))
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider)
                .addComponent(txfdSecurityLabel))
            .addContainerGap())
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addGap(10, 10, 10))
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSQLLayout.createSequentialGroup()
                    .addComponent(cbReadOnly)
                    .addGap(0, 425, Short.MAX_VALUE))
                .addGroup(pnlSQLLayout.createSequentialGroup()
                    .addComponent(spDefinationSQL)
                    .addContainerGap())))
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(cbReadOnly)
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("SQL");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 251, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could only be entered integer(>=0).
        char keyCh = evt.getKeyChar();
        Character.isDigit(keyCh);
        logger.info("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }
    }
    private void txfdDefaultValueEnable(ActionEvent e)
    {
        txfdDefaultValue.setEditable(rbIn.isSelected());
        txfdDefaultValue.setText("");
    }
    
    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.FUNCTION);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnRemovePrivilegeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemovePrivilegeActionPerformed
    {//GEN-HEADEREND:event_btnRemovePrivilegeActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int selectedRow = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblPrivileges.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemovePrivilegeActionPerformed

    private void btnAddChangePrivilegeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangePrivilegeActionPerformed
    {//GEN-HEADEREND:event_btnAddChangePrivilegeActionPerformed
        // TODO add your handling code here:
        StringBuilder privileges = new StringBuilder();
            if (cbExecute.isSelected())
        {
            privileges.append("X");
        }

        String usergroup = cbbRole.getSelectedItem().toString();
        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String usergroupOld = tableModel.getValueAt(i, 0).toString();
            logger.info("usergroupOld=" + usergroupOld + usergroupOld.equals(usergroup));
            if (usergroupOld.equals(usergroup))
            {
                isExist = true;
                tableModel.setValueAt(privileges.toString(), i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] p = new String[2];
            p[0] = usergroup;
            p[1] = privileges.toString();
            tableModel.addRow(p);
        }
    }//GEN-LAST:event_btnAddChangePrivilegeActionPerformed

    private void cbbRoleItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbRoleItemStateChanged
    {//GEN-HEADEREND:event_cbbRoleItemStateChanged
        // TODO add your handling code here:
        String role = cbbRole.getSelectedItem().toString();
        logger.info("role:" + role );
        if(role.equals("public"))
        {
            cbOptionExecute.setEnabled(false);
            cbOptionExecute.setSelected(false);
        }
        else
        {
           if(cbExecute.isSelected())
           {
               cbOptionExecute.setEnabled(true);
           }
           else
           {
                cbOptionExecute.setSelected(false);                
                cbOptionExecute.setEnabled(false); 
           }
        }
    }//GEN-LAST:event_cbbRoleItemStateChanged

    private void tblPrivilegesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblPrivilegesMousePressed
    {//GEN-HEADEREND:event_tblPrivilegesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int row = tblPrivileges.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblPrivileges.getValueAt(row, 0).toString();
        cbbRole.setSelectedItem(user);
        String p = tblPrivileges.getValueAt(row, 1).toString();
        cbExecute.setSelected(false);
        cbExecute.setEnabled(true);
        cbOptionExecute.setSelected(false);
        if(!user.equals("public"))
        {
            cbOptionExecute.setEnabled(true);
        }
        if (p.contains("X"))
        {
            cbExecute.setSelected(true);
            if(p.contains("X*"))
                cbOptionExecute.setSelected(true);
            else
                cbOptionExecute.setSelected(false);
        } else
        {
            cbExecute.setSelected(false);
            cbOptionExecute.setSelected(false);
        }
    
    }//GEN-LAST:event_tblPrivilegesMousePressed

    private void tblVariablesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblVariablesMousePressed
    {//GEN-HEADEREND:event_tblVariablesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int row = tblVariables.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
     
        String v = tblVariables.getValueAt(row, 0).toString();
        cbbVariableNames.setSelectedItem(v);
        String value = tblVariables.getValueAt(row, 1).toString();        
        if (value.equals("true") || value.equals("false"))
        {
            cbValue.setEnabled(true);
            cbValue.setSelected(Boolean.valueOf(value));
            txfdVariableValue.setEnabled(false);
            txfdVariableValue.setText("");
        } else
        {
            cbValue.setEnabled(false);
            cbValue.setSelected(false);
            txfdVariableValue.setEnabled(true);
            txfdVariableValue.setText(value);
        }
    }//GEN-LAST:event_tblVariablesMousePressed

    private void btnAddChangeVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeVariableActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeVariableActionPerformed
        // TODO add your handling code here:
        String name = cbbVariableNames.getSelectedItem().toString();
        String value;
        if (txfdVariableValue.isEnabled())
        {
            value = txfdVariableValue.getText();
        } else
        {
            value = String.valueOf(cbValue.isSelected());
        }
        if(value==null || value.equals(""))
        {
            value = "DEFAULT";
        }
        if(name!=null && !name.equals(""))
        {
            DefaultTableModel tableModel =  (DefaultTableModel) tblVariables.getModel();
            int row = tableModel.getRowCount();
            boolean isExist = false;
            for(int i=0;i<row;i++)
            {
                logger.info("row="+row);
                String nameOld = tableModel.getValueAt(i, 0).toString();
                logger.info("nameOld="+nameOld+nameOld.equals(name));
                if(nameOld.equals(name))
                {
                    isExist = true;
                    tableModel.setValueAt(value, i, 1);
                }
                if(isExist)
                {
                    break;
                }
            }
            if(!isExist)
            {
                String[] v = new String[2];
                v[0] = name;
                v[1] = value;
                tableModel.addRow(v);
            }
        }
    }//GEN-LAST:event_btnAddChangeVariableActionPerformed

    private void btnRemoveVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveVariableActionPerformed
    {//GEN-HEADEREND:event_btnRemoveVariableActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        int row = tblVariables.getSelectedRow();
        logger.info("selectedRow = " + row);
        if (row < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblVariables.getModel();
        tableModel.removeRow(row);
    }//GEN-LAST:event_btnRemoveVariableActionPerformed

    private void cbbVariableNamesItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbVariableNamesItemStateChanged
    {//GEN-HEADEREND:event_cbbVariableNamesItemStateChanged
        // TODO add your handling code here:
        int index = cbbVariableNames.getSelectedIndex();
        if (index < 0)
        {
            return;
        }
        SysVariableInfoDTO variable = (SysVariableInfoDTO) cbbVariableNames.getSelectedItem();
        this.currentVariable = variable;
        boolean isEnableCkb = variable.getValueType().equals("bool");
        cbValue.setEnabled(isEnableCkb);
        cbValue.setSelected(false);
        txfdVariableValue.setEnabled(!isEnableCkb);
        txfdVariableValue.setText("");
        txfdVariableValue.addKeyListener(new KeyAdapter()//variable value must >=0
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                //can only enter digit (>=0)
                variableValueForAddKeyTyped(evt);
            }
        });
    }//GEN-LAST:event_cbbVariableNamesItemStateChanged

    private void cbExecuteActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbExecuteActionPerformed
    {//GEN-HEADEREND:event_cbExecuteActionPerformed
        // TODO add your handling code here:
        if (!cbbRole.getSelectedItem().equals("public"))
        {
            if (cbExecute.isSelected())
            {
                cbOptionExecute.setEnabled(true);
                cbOptionExecute.setSelected(false);
            } else
            {
                cbOptionExecute.setEnabled(false);
                cbOptionExecute.setSelected(false);
            }
        }
    }//GEN-LAST:event_cbExecuteActionPerformed

    private void cbbLanguageItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbLanguageItemStateChanged
    {//GEN-HEADEREND:event_cbbLanguageItemStateChanged
        // TODO add your handling code here:
        logger.info("Current Language: " + cbbLanguage.getSelectedItem());
        if (cbbLanguage.getSelectedItem() != null
                && cbbLanguage.getSelectedItem().toString().equals("c"))
        {
            txfdObjectFile.setEditable(true);
            txfdLinkSymbol.setEditable(true);
            ttarSrc.setEditable(false);
            ttarSrc.setText("");
        } else
        {
            txfdObjectFile.setEditable(false);
            txfdObjectFile.setText("");
            txfdLinkSymbol.setEditable(false);
            txfdLinkSymbol.setText("");
            ttarSrc.setEditable(true);
        }
    }//GEN-LAST:event_cbbLanguageItemStateChanged

    private void cbbReturnTypeActionPerformed(java.awt.event.ActionEvent evt)                                              
    {                                                  
        // TODO add your handling code here:
        if(cbbReturnType.getSelectedItem()!=null)
        {
            String rtype = cbbReturnType.getSelectedItem().toString();
            if(rtype.equals("trigger") || rtype.equals("event_trigger")){
            cbbLanguage.setSelectedItem("plpgsql");
            }
        }
    } 
    private void btnRemoveArgAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        int row = tblArgs.getSelectedRow();
        logger.info("Select Row = "+row);
        if(row<0)
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblArgs.getModel();
        tableModel.removeRow(row);
    }
    private void btnAddArgAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        DatatypeDTO datatype = (DatatypeDTO) cbbDataType.getSelectedItem();
        String mode = this.getMode();
        String name = txfdArgName.getText();
        String defaultValue = txfdDefaultValue.getText();

        DefaultTableModel argModel = (DefaultTableModel) tblArgs.getModel();
        Object[] row = new Object[4];
        row[0] = datatype;
        row[1] = mode;
        row[2] = name;
        row[3] = defaultValue;
        argModel.addRow(row);
    }
    private void btnChangeArgAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        int row = tblArgs.getSelectedRow();
        if(row<0)
        {
            return;
        }
        tblArgs.setValueAt(cbbDataType.getSelectedItem(), row, 0);
        tblArgs.setValueAt(this.getMode(), row, 1);
        tblArgs.setValueAt(txfdArgName.getText(), row, 2);
        tblArgs.setValueAt(txfdDefaultValue.getText(), row, 3);
    }
    private void tblArgsMousePressAction(MouseEvent evt)
    {
        int row = tblArgs.getSelectedRow();
        logger.info("row=" + row);
        if (row < 0)
        {
            return;
        }
        logger.info("datatype="+tblArgs.getValueAt(row, 0));
        for(int i=0;i<cbbDataType.getItemCount();i++)
        {
            if(cbbDataType.getItemAt(i).toString().equals(tblArgs.getValueAt(row, 0).toString()))
            {
                cbbDataType.setSelectedIndex(i);
                break;
            }
        }        
        switch (tblArgs.getValueAt(row, 1).toString())
        {
            case "IN":
                rbIn.setSelected(true);
                break;
            case "OUT":
                rbOut.setSelected(true);
                break;
            case "INOUT":
                rbInOut.setSelected(true);
                break;
            case "VARIADIC":
                rbVariadic.setSelected(true);
                break;
        }
        if (tblArgs.getValueAt(row, 2) != null)
        {
            txfdArgName.setText(tblArgs.getValueAt(row, 2).toString());
        }
        if (tblArgs.getValueAt(row, 3) != null)
        {
            txfdDefaultValue.setText(tblArgs.getValueAt(row, 3).toString());
        }
    }
    private String getMode()
    {
        if (rbIn.isSelected())
        {
            return "IN";
        } else if (rbOut.isSelected())
        {
            return "OUT";
        } else if (rbInOut.isSelected())
        {
            return "INOUT";
        } else if (rbVariadic.isSelected())
        {
            return "VARIADIC";
        } else
        {
            logger.error("no mode was selected, do nothing and return null.");
            return null;
        }
    }
    private void variableValueForAddKeyTyped(KeyEvent evt)
    {
        String type = currentVariable.getValueType();
        char keyCh = evt.getKeyChar();
        //logger.info("keych=" + keyCh+",isDigit="+ Character.isDigit(keyCh));
        logger.info("Variable Type = " + type);
        switch (type)
        {
            case "real":
                if ((keyCh < '0') || (keyCh > '9'))
                {
                    if (keyCh != '' && keyCh != '.' && keyCh!='-')
                    {
                        evt.setKeyChar('\0');
                    }
                    if(!currentVariable.getMinValue().startsWith("-")
                            && keyCh=='-')
                    {
                         evt.setKeyChar('\0');
                    }
                }
                break;
            case "integer":
                if ((keyCh < '0') || (keyCh > '9'))
                {
                    if (keyCh != ''  && keyCh!='-')
                    {
                        evt.setKeyChar('\0');
                    }
                    if (!currentVariable.getMinValue().startsWith("-")
                            && keyCh == '-')
                    {
                        evt.setKeyChar('\0');
                    }
                }
                break;
        }
    }

    
    //add
    private void btnOkEnabledForAdd()
    {
        //logger.info(txfdName.getText());
        if (txfdName.getText().isEmpty())
        {
            btnOK.setEnabled(false);
            return;
        }
        //function
        if (TreeEnum.TreeNode.FUNCTION == type
                && cbbReturnType.getSelectedItem() != null
                && !cbbReturnType.getSelectedItem().toString().isEmpty()) 
        {
            if (cbbLanguage.getSelectedItem().equals("c")) {
                btnOK.setEnabled(!txfdObjectFile.getText().trim().isEmpty());
            } else {
                btnOK.setEnabled(!ttarSrc.getText().trim().isEmpty());
            }
        } else//procedure 
        {
            if (cbbLanguage.getSelectedItem().equals("c")) {
                btnOK.setEnabled(!txfdObjectFile.getText().trim().isEmpty());
            } else {
                btnOK.setEnabled(!ttarSrc.getText().trim().isEmpty());
            }
        }
    }
    private void jtpForAddStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
        if (index == 7)
        {
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
            } else
            {
                tpDefinitionSQL.setText("");
                sc.setDefine(tpDefinitionSQL.getDocument(), TreeController.getInstance().getDefinitionSQL(this.getFunctionInfo()));
            }
        }
    } 
    private void btnOKForAddActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        try
        {
            TreeController.getInstance().createObj(helperInfo, this.getFunctionInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    //change
    private void btnOkEnabledForChange()
    {
        logger.debug("Enter");
        String name = txfdName.getText();
        logger.debug("name=" + name
        + ",cbbReturnType=" + cbbReturnType.getSelectedItem()
        + ",cbbLanguage=" + cbbLanguage.getSelectedItem()
        + ",txfdObjectFile=" + txfdObjectFile.getText()
        + ",ttarSrc=" +  ttarSrc.getText()
        + ",funcSrc=" + fun.getSrc());
        if (name == null || name.isEmpty()
                //PROCEDURE HAS NO RETURN TYPE
                || (fun.getType() == TreeEnum.TreeNode.FUNCTION && (cbbReturnType.getSelectedItem() == null || cbbReturnType.getSelectedItem().toString().isEmpty()))
                || cbbLanguage.getSelectedItem() == null || cbbLanguage.getSelectedItem().toString().isEmpty()
                || (cbbLanguage.getSelectedItem().equals("c") && txfdObjectFile.getText().trim().isEmpty())
                || (!cbbLanguage.getSelectedItem().equals("c") && ttarSrc.getText().trim().isEmpty()))
        {
            logger.debug("Disable btnOk");
            btnOK.setEnabled(false);
        } else
        {
            String acl = this.getACL();
            List<FunctionArgDTO> argList = this.getParameters();
            List<VariableInfoDTO> varList = getVariables();
            if (!name.equals(fun.getSimpleName()))
            {
                logger.info("name changed");
                btnOK.setEnabled(true);
            } else if (cbbOwner.getSelectedItem() != null
                    && !cbbOwner.getSelectedItem().equals(fun.getOwner()))
            {
                logger.info("owner changed");
                btnOK.setEnabled(true);
            } else if (!txarComment.getText().equals(fun.getComment())
                    && !(txarComment.getText().isEmpty() && fun.getComment() == null))
            {
                logger.info("comment changed");
                btnOK.setEnabled(true);
            } else if ((!cbbLanguage.getSelectedItem().equals("c") &&  !ttarSrc.getText().equals(fun.getSrc()))
                    || (cbbLanguage.getSelectedItem().equals("c") && !txfdObjectFile.getText().equals(fun.getBin())))
            {
                logger.info("define changed");
                btnOK.setEnabled(true);
            } else if(!fun.getReturnType().equals(cbbReturnType.getSelectedItem().toString())){
                logger.info("ReturnType change");
                btnOK.setEnabled(true);
            }
            else if (!cbbVolatile.getSelectedItem().equals(fun.getVolatiles()))
            {
                logger.info("volatile changed");
                btnOK.setEnabled(true);
            } else if(!cbbParallel.getSelectedItem().equals(fun.getParallel()))
            {
                logger.info("Parallel changed");
                btnOK.setEnabled(true);
             }else if (cbStrict.isSelected() != fun.isStrict()
                    || cbSecurityDefiner.isSelected() != fun.isSecurityDefiner()
                    || cbLeakProof.isSelected() != fun.isLeafProof())
            {
                logger.info("option changed");
                btnOK.setEnabled(true);
            } else if (!txfdCost.getText().trim().equals(String.valueOf(fun.getCost())))
            {
                logger.info("cost changed");
                btnOK.setEnabled(true);
            } else if (!acl.equals(fun.getAcl())
                    && !(acl.isEmpty() && fun.getAcl() == null))
            {
                logger.info("ACL changed");
                btnOK.setEnabled(true);
            } else if (!Compare.getInstance().equalVariableList(varList, fun.getVariableList()))
            {
                logger.info("variable changed");
                btnOK.setEnabled(true);
            } else if (!Compare.getInstance().equalFuncArgList(argList, fun.getArgList()))
            {
                logger.info("argument changed");
                btnOK.setEnabled(true);
            } else
            {
                btnOK.setEnabled(false);
            }
        } 
    }
    private List<FunctionArgDTO> getParameters()
    {
        List<FunctionArgDTO> argList = new ArrayList();
        int argRow = tblArgs.getRowCount();
        if (argRow > 0)
        {
            for (int i = 0; i < argRow; i++)
            {
                FunctionArgDTO arg = new FunctionArgDTO();
                //TypeDTO type = (TypeDTO) tblArgs.getValueAt(i, 0);
                arg.setType(tblArgs.getValueAt(i, 0).toString());
                arg.setMode(tblArgs.getValueAt(i, 1).toString());
                if (tblArgs.getValueAt(i, 2) != null && !tblArgs.getValueAt(i, 2).toString().isEmpty())
                {
                    arg.setName(tblArgs.getValueAt(i, 2).toString());
                }
                if (tblArgs.getValueAt(i, 3) != null && !tblArgs.getValueAt(i, 3).toString().isEmpty())
                {
                    arg.setDefaultValue("'" + tblArgs.getValueAt(i, 3).toString() + "'::" + arg.getType());
                }
                argList.add(arg);
            }
        }
        return argList;
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
        if (index == 7)
        {
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            }else
            {
                tpDefinitionSQL.setText("");
                sc.setDefine(tpDefinitionSQL.getDocument(), TreeController.getInstance().getFuncProcChangeSQL(this.fun, this.getFunctionInfo()));
            }
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getDbName(),
                    tc.getFuncProcChangeSQL(this.fun, this.getFunctionInfo()));
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    
    private String getACL()
    {
        return AclUtil.getACL(cbbOwner.getSelectedItem().toString(), helperInfo, fun, (DefaultTableModel) tblPrivileges.getModel());
    }
    private  List<VariableInfoDTO> getVariables()
    {
        List<VariableInfoDTO> varList = new ArrayList();
        int varRow = tblVariables.getRowCount();
        if (varRow > 0)
        {
            VariableInfoDTO v;
            for (int i = 0; i < varRow; i++)
            {
                v = new VariableInfoDTO();
                v.setName(tblVariables.getValueAt(i, 0).toString());
                v.setValue(tblVariables.getValueAt(i, 1).toString());
                varList.add(v);
            }
        }
        return varList;
    }
    public FunctionInfoDTO getFunctionInfo()
    {
        FunctionInfoDTO newFun = new FunctionInfoDTO();
        if (fun != null) {
            newFun.setType(fun.getType());
            newFun.setOid(fun.getOid());
        } else {
            newFun.setType(type);
        }
        newFun.setHelperInfo(helperInfo);
        newFun.setSimpleName(txfdName.getText());
        newFun.setOwner(cbbOwner.getSelectedItem() == null ? "" : cbbOwner.getSelectedItem().toString());
        newFun.setComment(txarComment.getText());
        newFun.setReturnType(cbbReturnType.getSelectedItem() == null ? null : cbbReturnType.getSelectedItem().toString());
        newFun.setLanguage(cbbLanguage.getSelectedItem() == null ? "" : cbbLanguage.getSelectedItem().toString());
        if (newFun.getLanguage().equals("c"))
        {
            newFun.setBin(txfdObjectFile.getText());
            newFun.setSrc(txfdLinkSymbol.getText());
        } else
        {
            newFun.setSrc(ttarSrc.getText());
        }
        newFun.setVolatiles(cbbVolatile.getSelectedItem().toString());
        newFun.setReturnSet(cbReturnSet.isSelected());
        newFun.setStrict(cbStrict.isSelected());
        newFun.setSecurityDefiner(cbSecurityDefiner.isSelected());
        newFun.setWindow(cbWindow.isSelected());
        newFun.setLeafProof(cbLeakProof.isSelected());
        newFun.setParallel(cbbParallel.getSelectedItem() == null ? null : cbbParallel.getSelectedItem().toString());
        if (!txfdCost.getText().isEmpty())
        {
            newFun.setCost(Integer.valueOf(txfdCost.getText()));
        }
        //arguments
        int argRow = tblArgs.getRowCount();
        List<FunctionArgDTO> argList = new ArrayList();
        StringBuilder intypesOid = new StringBuilder();
        StringBuilder intypes = new StringBuilder();
        if(argRow>0)
        {           
            for (int i = 0; i < argRow; i++)
            {
                FunctionArgDTO arg = new FunctionArgDTO();
                DatatypeDTO type = null;
                if(fun == null)
                {
                    type = (DatatypeDTO) tblArgs.getValueAt(i, 0);
                    arg.setTypeOid(type.getOid());
                }
                arg.setType(tblArgs.getValueAt(i, 0).toString());
                arg.setMode(tblArgs.getValueAt(i, 1).toString());
                arg.setName(tblArgs.getValueAt(i, 2).toString());
                if (tblArgs.getValueAt(i, 3) != null && !tblArgs.getValueAt(i, 3).toString().isEmpty())
                {
                    arg.setDefaultValue(arg.getType().equalsIgnoreCase("integer") 
                            || arg.getType().equalsIgnoreCase("bigint") 
                            || arg.getType().equalsIgnoreCase("smallint") 
                            || arg.getType().equalsIgnoreCase("numeric") 
                            || arg.getType().equalsIgnoreCase("decimal") 
                            || arg.getType().equalsIgnoreCase("real") 
                            || arg.getType().equalsIgnoreCase("double precision") 
                            || arg.getType().equalsIgnoreCase("serial") 
                            || arg.getType().equalsIgnoreCase("bigserial") 
                            || arg.getType().equalsIgnoreCase("smallserial") 
                            || arg.getType().equalsIgnoreCase("boolean") 
                            || tblArgs.getValueAt(i, 3).toString().contains( "'::" + arg.getType()) ?
                            tblArgs.getValueAt(i, 3).toString() : "'" + tblArgs.getValueAt(i, 3).toString() + "'::" + arg.getType());
                }
                argList.add(arg);
                if (arg.getMode().equals("IN") || arg.getMode().equals("INOUT")
                        || arg.equals("VARIADIC"))
                {
                    intypes.append(arg.getType()).append(",");
                    if(fun == null)
                    {
                        intypesOid.append(type.getOid()).append(" ");
                    }
                }
            }
            logger.debug("intypes=" + intypes);
            intypes.deleteCharAt(intypes.length()-1); 
            newFun.setInArgTypes(intypes.toString());
            if(fun == null)
            {
                intypesOid.deleteCharAt(intypesOid.length()-1);
                newFun.setInArgTypeOids(intypesOid.toString());
            }
            newFun.setInArgCount(intypes.length());            
        }
        newFun.setArgList(argList);       
        //variable       
        newFun.setVariableList(this.getVariables());
        //privilege
        newFun.setAcl(this.getACL());
        //security label
        /*
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.info("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        newFun.setSecurityLabelList(securityList);
        */
        return newFun;
    }
    public boolean isSuccess()
    {
        return this.isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgMode;
    private javax.swing.JButton btnAddArg;
    private javax.swing.JButton btnAddChangePrivilege;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnAddChangeVariable;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnChangeArg;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemoveArg;
    private javax.swing.JButton btnRemovePrivilege;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JButton btnRemoveVariable;
    private javax.swing.JCheckBox cbExecute;
    private javax.swing.JCheckBox cbLeakProof;
    private javax.swing.JCheckBox cbOptionExecute;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbReturnSet;
    private javax.swing.JCheckBox cbSecurityDefiner;
    private javax.swing.JCheckBox cbStrict;
    private javax.swing.JCheckBox cbValue;
    private javax.swing.JCheckBox cbWindow;
    private javax.swing.JComboBox cbbDataType;
    private javax.swing.JComboBox cbbLanguage;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.JComboBox cbbParallel;
    private javax.swing.JComboBox cbbReturnType;
    private javax.swing.JComboBox cbbRole;
    private javax.swing.JComboBox cbbSchema;
    private javax.swing.JComboBox cbbVariableNames;
    private javax.swing.JComboBox cbbVolatile;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lbCost;
    private javax.swing.JLabel lbEstimateRow;
    private javax.swing.JLabel lbLeakProof;
    private javax.swing.JLabel lbParallel;
    private javax.swing.JLabel lbReturnSet;
    private javax.swing.JLabel lbReturnType;
    private javax.swing.JLabel lbStrict;
    private javax.swing.JLabel lbVolatile;
    private javax.swing.JLabel lbWindow;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSchema;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JLabel lblVariableName;
    private javax.swing.JLabel lblVariableValue;
    private javax.swing.JPanel pnlArg;
    private javax.swing.JPanel pnlDefine;
    private javax.swing.JPanel pnlOption;
    private javax.swing.JPanel pnlPrivileges;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JPanel pnlVariables;
    private javax.swing.JRadioButton rbIn;
    private javax.swing.JRadioButton rbInOut;
    private javax.swing.JRadioButton rbOut;
    private javax.swing.JRadioButton rbVariadic;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblArgs;
    private javax.swing.JTable tblPrivileges;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTable tblVariables;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarSrc;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdArg;
    private javax.swing.JTextField txfdArgName;
    private javax.swing.JTextField txfdCost;
    private javax.swing.JTextField txfdDefaultValue;
    private javax.swing.JTextField txfdEstimateRow;
    private javax.swing.JTextField txfdLinkSymbol;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdObjectFile;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdUseSlony;
    private javax.swing.JTextField txfdVariableValue;
    // End of variables declaration//GEN-END:variables
}
