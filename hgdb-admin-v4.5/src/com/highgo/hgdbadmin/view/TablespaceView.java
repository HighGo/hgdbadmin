/* ------------------------------------------------ 
* 
* File: TablespaceView.java
*
* Abstract: 
* 		表空间操作窗体.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\TablespaceView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.Compare;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.TablespaceInfoDTO;
import com.highgo.hgdbadmin.model.VariableInfoDTO;
import com.highgo.hgdbadmin.util.PathChooserDialog;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class TablespaceView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
 
    private boolean isSuccess=false;
    private HelperInfoDTO helperInfo;
    private TablespaceInfoDTO tablespaceInfo;
    /**
     * add  
     * @param parent
     * @param modal
     * @param helperInfo
     */
    public TablespaceView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.tablespaceInfo = null;
        initComponents();
        jtp.setEnabledAt(4, false);
        this.setTitle(constBundle.getString("addTablespace"));
        this.setNormalInfo(false);
        
        //action
        KeyAdapter keyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAdd(evt);
            }
        };
        txfdName.addKeyListener(keyAdapter);
        txfdLocation.addKeyListener(keyAdapter);           
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnOKForAddActionPerformed(evt);
            }
        });

    }
     //property
    public TablespaceView(JFrame parent, boolean modal, TablespaceInfoDTO tablespaceInfo)
    {
        super(parent, modal);        
        this.tablespaceInfo = tablespaceInfo;
        this.helperInfo = tablespaceInfo.getHelperInfo();
        initComponents();    
        jtp.setEnabledAt(4, false);
        this.setTitle(constBundle.getString("tablespaces") + " " + tablespaceInfo.getName());
        this.setNormalInfo(true);        
        //property        
        this.setProperty();     

        //action
        KeyAdapter btnOkEnable = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnable);
        ttarComment.addKeyListener(btnOkEnable);
        cbbOwner.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        DefaultTableModel aclModel = (DefaultTableModel) tblPrivileges.getModel();
        aclModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnableForChange();
            }
        });
        DefaultTableModel vModel = (DefaultTableModel) tblVariables.getModel();
        vModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnableForChange();
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnOKForChangeActionPerformed(evt);
            }
        });        
    }
    
    private void setProperty()
    {
        txfdName.setText(tablespaceInfo.getName());
        txfdOID.setText(tablespaceInfo.getOid().toString());
        cbbOwner.setSelectedItem(tablespaceInfo.getOwner());
        ttarComment.setText(tablespaceInfo.getComment());
        txfdLocation.setText(tablespaceInfo.getLocation());
        txfdLocation.setEnabled(false);
        List<VariableInfoDTO> vlist = tablespaceInfo.getVariableList();
        if (vlist != null)
        {
            DefaultTableModel vModel = (DefaultTableModel) tblVariables.getModel();
            for (VariableInfoDTO variable : vlist)
            {
                String[] v = new String[2];
                v[0] = variable.getName();
                v[1] = variable.getValue();
                vModel.addRow(v);
            }
        }
        String acl = tablespaceInfo.getAcl();
        if (acl != null && !acl.isEmpty() && !acl.equals("{}"))
        {
            DefaultTableModel sModel = (DefaultTableModel) tblPrivileges.getModel();
            acl = acl.substring(1, acl.length() - 1);
            String[] privilegeArray = acl.split(",");
            for (String privilege : privilegeArray)
            {
                logger.info("privilege:" + privilege);
                String[] pArray = privilege.split("=");
                String[] p = new String[2];
                if (pArray[0] == null || pArray[0].isEmpty())
                {
                    p[0] = "public";
                } else
                {
                    p[0] = pArray[0];
                }
                p[1] = pArray[1].split("/")[0];
                sModel.addRow(p);
            }
        }
        List<SecurityLabelInfoDTO> slist = tablespaceInfo.getSecurityLabelList();
        if (slist != null)
        {
            DefaultTableModel sModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[3];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                sModel.addRow(sl);
            }
        }
    }
    
    private void setNormalInfo(boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
        try
        {
            //change for sm privilege limit
            cbbOwner.setModel(new DefaultComboBoxModel(new String[]{"sysdba"})); 
            cbbOwner.setSelectedIndex(0); 
            cbbOwner.setEditable(false);
            /*String[] owners = tc.getItemsArrary(helperInfo, "owner");  
            cbbOwner.setModel(new DefaultComboBoxModel(owners)); 
            if(isChange)
            {
                cbbOwner.removeItemAt(0);
            }else
            {
                cbbOwner.setSelectedIndex(0);
            }*/
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbRole.setModel(new DefaultComboBoxModel(tc.getRoles(helperInfo)));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbVariableNames.setModel(new DefaultComboBoxModel(tc.getVariables(helperInfo, "tablespace")));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }

        //normal action
        txfdLocation.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                txfdLocationForAddMouseClicked(evt);
            }
        });
        txfdVariableValue.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                variableRealValueKeyTyped(evt);//real,>=0
            }
        });
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        ttarComment = new javax.swing.JTextArea();
        pnlDefinition = new javax.swing.JPanel();
        lblLocation = new javax.swing.JLabel();
        txfdLocation = new javax.swing.JTextField();
        pnlVariables = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblVariables = new javax.swing.JTable();
        btnAddChangeVariable = new javax.swing.JButton();
        btnRemoveVariable = new javax.swing.JButton();
        lblVariableName = new javax.swing.JLabel();
        cbbVariableNames = new javax.swing.JComboBox();
        lblVariableValue = new javax.swing.JLabel();
        txfdVariableValue = new javax.swing.JTextField();
        pnlPrivileges = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPrivileges = new javax.swing.JTable();
        btnAddChangePrivileges = new javax.swing.JButton();
        btnRemovePrivilege = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRole = new javax.swing.JLabel();
        cbbRole = new javax.swing.JComboBox();
        cbCreate = new javax.swing.JCheckBox();
        cbOptionCreate = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        lblSecurityLabel = new javax.swing.JLabel();
        txfdSecurityLabel = new javax.swing.JTextField();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addTablespace"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/tablespace.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

ttarComment.setColumns(20);
ttarComment.setRows(5);
jScrollPane1.setViewportView(ttarComment);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
        .addGap(10, 10, 10)
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txfdOID)
            .addComponent(txfdName)
            .addComponent(cbbOwner, 0, 413, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE))
            .addContainerGap())
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(520, 470));

    lblLocation.setText(constBundle.getString("location"));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(lblLocation, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(txfdLocation, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblLocation)
                .addComponent(txfdLocation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(434, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlVariables.setBackground(new java.awt.Color(255, 255, 255));
    pnlVariables.setPreferredSize(new java.awt.Dimension(520, 500));

    jScrollPane5.setBackground(new java.awt.Color(255, 255, 255));

    tblVariables.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblVariables.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblVariables.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblVariablesMousePressed(evt);
        }
    });
    jScrollPane5.setViewportView(tblVariables);
    if (tblVariables.getColumnModel().getColumnCount() > 0)
    {
        tblVariables.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("variableName"));
        tblVariables.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("variableValue"));
    }

    btnAddChangeVariable.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeVariable.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeVariableActionPerformed(evt);
        }
    });

    btnRemoveVariable.setText(constBundle.getString("remove"));
    btnRemoveVariable.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveVariableActionPerformed(evt);
        }
    });

    lblVariableName.setText(constBundle.getString("variableName"));

    cbbVariableNames.setEditable(true);

    lblVariableValue.setText(constBundle.getString("variableValue"));

    javax.swing.GroupLayout pnlVariablesLayout = new javax.swing.GroupLayout(pnlVariables);
    pnlVariables.setLayout(pnlVariablesLayout);
    pnlVariablesLayout.setHorizontalGroup(
        pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlVariablesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblVariableName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdVariableValue)
                        .addComponent(cbbVariableNames, 0, 412, Short.MAX_VALUE))))
            .addContainerGap())
    );
    pnlVariablesLayout.setVerticalGroup(
        pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlVariablesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVariableName)
                .addComponent(cbbVariableNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVariableValue)
                .addComponent(txfdVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("variables"), pnlVariables);
    pnlVariables.getAccessibleContext().setAccessibleName("");

    pnlPrivileges.setBackground(new java.awt.Color(255, 255, 255));
    pnlPrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlPrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

    jScrollPane4.setBackground(new java.awt.Color(255, 255, 255));

    tblPrivileges.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblPrivileges.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblPrivileges.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblPrivilegesMousePressed(evt);
        }
    });
    jScrollPane4.setViewportView(tblPrivileges);
    if (tblPrivileges.getColumnModel().getColumnCount() > 0)
    {
        tblPrivileges.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("user")+"/"+constBundle.getString("group"));
        tblPrivileges.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("privileges"));
    }

    btnAddChangePrivileges.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangePrivileges.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangePrivilegesActionPerformed(evt);
        }
    });

    btnRemovePrivilege.setText(constBundle.getString("remove"));
    btnRemovePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemovePrivilegeActionPerformed(evt);
        }
    });

    jPanel1.setBackground(new java.awt.Color(255, 255, 255));
    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("privileges"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("宋体", 0, 12), new java.awt.Color(204, 204, 204))); // NOI18N

    lblRole.setText(constBundle.getString("role"));

    cbbRole.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbRoleItemStateChanged(evt);
        }
    });

    cbCreate.setBackground(new java.awt.Color(255, 255, 255));
    cbCreate.setText("CREATE");
    cbCreate.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbCreateActionPerformed(evt);
        }
    });

    cbOptionCreate.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionCreate.setText("WITH GRANT OPTION");
    cbOptionCreate.setEnabled(false);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbRole, 0, 453, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(cbCreate)
                    .addGap(189, 189, 189)
                    .addComponent(cbOptionCreate)))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblRole))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbOptionCreate)
                .addComponent(cbCreate))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlPrivilegesLayout = new javax.swing.GroupLayout(pnlPrivileges);
    pnlPrivileges.setLayout(pnlPrivilegesLayout);
    pnlPrivilegesLayout.setHorizontalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlPrivilegesLayout.createSequentialGroup()
                    .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlPrivilegesLayout.setVerticalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 326, Short.MAX_VALUE)
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10))
    );

    jtp.addTab(constBundle.getString("privileges"), pnlPrivileges);
    pnlPrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    jScrollPane3.setBackground(new java.awt.Color(255, 255, 255));

    tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblSecurityLabels.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblSecurityLabels.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblSecurityLabelsMousePressed(evt);
        }
    });
    jScrollPane3.setViewportView(tblSecurityLabels);
    if (tblSecurityLabels.getColumnModel().getColumnCount() > 0)
    {
        tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeSecurityLabelActionPerformed(evt);
        }
    });

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveSecurityLabelActionPerformed(evt);
        }
    });

    lblProvider.setText(constBundle.getString("provider"));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane3)
            .addGap(10, 10, 10))
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(375, 375, 375))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSecurityLabelLayout.createSequentialGroup()
                    .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdSecurityLabel)
                        .addComponent(txfdProvider))
                    .addContainerGap())))
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void variableRealValueKeyTyped(KeyEvent evt)
    {
        char keyCh = evt.getKeyChar();
        //logger.info("keych=" + keyCh+",isDigit="+ Character.isDigit(keyCh));
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '' && keyCh != '.')
            {
                evt.setKeyChar('\0');
            }
        }
    }
    
    private void txfdLocationForAddMouseClicked(MouseEvent evt)
    {
        if(!txfdLocation.isEnabled())
        {
            return;
        }
        PathChooserDialog chooser = new PathChooserDialog();
        int response = chooser.showPathChooser(this, null,
                MessageFormat.format(constBundle.getString("chooseWhatPath"), constBundle.getString("tablespaces")));
        logger.info("response = " + response);
        if (response == JFileChooser.APPROVE_OPTION)
        {
            txfdLocation.setText(chooser.getChoosedPath().replace("\\", "/"));
            if (!txfdName.getText().isEmpty())
            {
                btnOK.setEnabled(true);
            }
        }
    }
  
    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);    
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnAddChangePrivilegesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangePrivilegesActionPerformed
    {//GEN-HEADEREND:event_btnAddChangePrivilegesActionPerformed
        // TODO add your handling code here:
        boolean create = cbCreate.isSelected();
        boolean optionCreate = cbOptionCreate.isSelected();
        String usergroup = cbbRole.getSelectedItem().toString();
        StringBuilder privileges = new StringBuilder();
        if (create)
        {
            privileges.append("C");
        }
        if (optionCreate)
        {
            privileges.append("*");
        }

        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String usergroupOld = tableModel.getValueAt(i, 0).toString();
            logger.info("usergroupOld=" + usergroupOld + usergroupOld.equals(usergroup));
            if (usergroupOld.equals(usergroup))
            {
                isExist = true;
                tableModel.setValueAt(privileges.toString(), i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] p = new String[2];
            p[0] = usergroup;
            p[1] = privileges.toString();
            tableModel.addRow(p);
        }
    }//GEN-LAST:event_btnAddChangePrivilegesActionPerformed

    private void btnRemovePrivilegeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemovePrivilegeActionPerformed
    {//GEN-HEADEREND:event_btnRemovePrivilegeActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int selectedRow = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblPrivileges.getModel();
        tableModel.removeRow(selectedRow);     
    }//GEN-LAST:event_btnRemovePrivilegeActionPerformed

    private void btnAddChangeVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeVariableActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeVariableActionPerformed
        // TODO add your handling code here:
        String name = cbbVariableNames.getSelectedItem().toString();
        String value = txfdVariableValue.getText();
        if(value==null || value.equals(""))
        {
            value = "DEFAULT";
        }
        String db = txfdName.getText();        
        if(name!=null && !name.equals(""))
        {
            DefaultTableModel tableModel =  (DefaultTableModel) tblVariables.getModel();            
            int row = tableModel.getRowCount();
            boolean isExist = false;
            for(int i=0;i<row;i++)
            {
                logger.info("row="+row);
                String nameOld = tableModel.getValueAt(i, 0).toString();                
                logger.info("nameOld="+nameOld+nameOld.equals(name));
                if( nameOld.equals(name))
                {
                    isExist = true;
                    tableModel.setValueAt(value, i, 1);
                }
                if(isExist)
                {
                    break;
                }
            }
            if(!isExist)
            {
                String[] v = new String[3];
                v[0] = name;
                v[1] = value;
                tableModel.addRow(v);
            }
        }  
    }//GEN-LAST:event_btnAddChangeVariableActionPerformed

    private void btnRemoveVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveVariableActionPerformed
    {//GEN-HEADEREND:event_btnRemoveVariableActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int selectedRow = tblVariables.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblVariables.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveVariableActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.TABLESPACE);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void tblVariablesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblVariablesMousePressed
    {//GEN-HEADEREND:event_tblVariablesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int row = tblVariables.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String v = tblVariables.getValueAt(row, 0).toString();
        cbbVariableNames.setSelectedItem(v);
        String value = tblVariables.getValueAt(row, 1).toString();
        txfdVariableValue.setText(value);        
    }//GEN-LAST:event_tblVariablesMousePressed

    private void cbCreateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbCreateActionPerformed
    {//GEN-HEADEREND:event_cbCreateActionPerformed
        // TODO add your handling code here:
        if(cbCreate.isSelected())
        {
            if (cbbRole.getSelectedItem().toString().equals("public"))
            {
                cbOptionCreate.setEnabled(false);
                cbOptionCreate.setSelected(false);
            } else
            {
                cbOptionCreate.setEnabled(true);
                cbOptionCreate.setSelected(false);
            }
        } else
        {
            cbOptionCreate.setSelected(false);
            cbOptionCreate.setEnabled(false);
        }       
    }//GEN-LAST:event_cbCreateActionPerformed

    private void cbbRoleItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbRoleItemStateChanged
    {//GEN-HEADEREND:event_cbbRoleItemStateChanged
        // TODO add your handling code here:
        String role = cbbRole.getSelectedItem().toString();
        logger.info("role:" + role );
       if(role.equals("public"))
        {
            cbOptionCreate.setEnabled(false);
            cbOptionCreate.setSelected(false);
        }
        else
        {
           if(cbCreate.isSelected())
           {
               cbOptionCreate.setEnabled(true);
           }
           else
           {
                cbOptionCreate.setEnabled(false);
                cbOptionCreate.setSelected(false);  
           }
        }
    }//GEN-LAST:event_cbbRoleItemStateChanged

    private void tblPrivilegesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblPrivilegesMousePressed
    {//GEN-HEADEREND:event_tblPrivilegesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int row = tblPrivileges.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblPrivileges.getValueAt(row, 0).toString();
        cbbRole.setSelectedItem(user);
        String p = tblPrivileges.getValueAt(row, 1).toString();
        cbCreate.setSelected(false);
        if(p.contains("C"))
        {
            cbCreate.setSelected(true);
            if(p.contains("C*"))
                cbOptionCreate.setSelected(true);
            else
                cbOptionCreate.setSelected(false);
        }
        else
        {
            cbCreate.setSelected(false);
            cbOptionCreate.setSelected(false);
        }
    }//GEN-LAST:event_tblPrivilegesMousePressed

    private void tblSecurityLabelsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblSecurityLabelsMousePressed
    {//GEN-HEADEREND:event_tblSecurityLabelsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press Security Label table");
        int row = tblSecurityLabels.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblSecurityLabels.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabels.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }//GEN-LAST:event_tblSecurityLabelsMousePressed
           
    // for add
    private void btnOkEnableForAdd(KeyEvent evt)
    {
        String name = txfdName.getText();
        String location = txfdLocation.getText();
        if (name == null || name.isEmpty()
                || location == null || location.isEmpty())
        {
            btnOK.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
        }
    }
    private void jtpForAddStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
        if (index == 5)
        {
            tpDefinitionSQL.setText("");//clear
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNotComplete"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getTablespaceInfo()));
            }
        }
    }
    private void btnOKForAddActionPerformed(ActionEvent evt)                                      
    {
        logger.info(txfdName.getText() + "," + cbbOwner.getSelectedItem().toString());       
        try
        {
             TreeController tc = TreeController.getInstance();
            tc.createObj(helperInfo, this.getTablespaceInfo());  
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }                                     

    //for change
    private void btnOkEnableForChange()
    {
        String name = txfdName.getText();        
        String acl = this.getACL();
        if (name == null || name.isEmpty())
        {
            btnOK.setEnabled(false);
        } else if (!name.equals(tablespaceInfo.getName()))
        {
            btnOK.setEnabled(true);
        } else if (cbbOwner.getSelectedItem() != null
                && !cbbOwner.getSelectedItem().equals(tablespaceInfo.getOwner()))
        {
            btnOK.setEnabled(true);
        } else if (!ttarComment.getText().equals(tablespaceInfo.getComment())
                && !(ttarComment.getText().isEmpty() && tablespaceInfo.getComment() == null))
        {
            btnOK.setEnabled(true);
        } else if (!acl.equals(tablespaceInfo.getAcl())
                && !(acl.isEmpty() && tablespaceInfo.getAcl() == null))
        {
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalVariableList(this.getVariableList(), tablespaceInfo.getVariableList()))
        {
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
        if (index == 5)
        {
            tpDefinitionSQL.setText("");
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getTablespaceChangeSQL(this.tablespaceInfo, this.getTablespaceInfo()));
            }
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)                                      
    {
        logger.info( txfdName.getText() + "," + cbbOwner.getSelectedItem().toString());      
        TreeController tc = TreeController.getInstance();
        try
        {
            tc.executeSql(helperInfo, helperInfo.getDbName(), tc.getTablespaceChangeSQL(this.tablespaceInfo, this.getTablespaceInfo()));  
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }       

    
    //***************
    private String getACL()
    {
        return AclUtil.getACL(cbbOwner.getSelectedItem().toString(), helperInfo, tablespaceInfo, (DefaultTableModel) tblPrivileges.getModel());
    }    
    private List<VariableInfoDTO> getVariableList()
    {
        List<VariableInfoDTO> variableList = new ArrayList();
        DefaultTableModel variableModel = (DefaultTableModel) tblVariables.getModel();
        int variableRow = variableModel.getRowCount();
        logger.info("variableRow:" + variableRow);
        if (variableRow > 0)
        {
            for (int i = 0; i < variableRow; i++)
            {
                VariableInfoDTO v = new VariableInfoDTO();
                v.setName(variableModel.getValueAt(i, 0).toString());
                v.setValue(variableModel.getValueAt(i, 1).toString());
                variableList.add(v);
            }
        }
        return variableList;
    }
    public TablespaceInfoDTO getTablespaceInfo()
    {
        TablespaceInfoDTO newTablespace = new TablespaceInfoDTO();
        newTablespace.setHelperInfo(helperInfo);
        if(this.tablespaceInfo!=null)
        {
            newTablespace.setOid(this.tablespaceInfo.getOid());
        }
        newTablespace.setName(txfdName.getText());
        if (cbbOwner.getSelectedItem() == null || cbbOwner.getSelectedItem().toString().isEmpty())
        {
            newTablespace.setOwner(helperInfo.getUser());
        } else
        {
            newTablespace.setOwner(cbbOwner.getSelectedItem().toString());
        }
        newTablespace.setComment(ttarComment.getText());
        newTablespace.setLocation(txfdLocation.getText());
        //for privilege       
        newTablespace.setAcl(this.getACL());
        //for variable       
        newTablespace.setVariableList(this.getVariableList());
        /*
        //for security label
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.info("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        tablespaceInfo.setSecurityLabelList(securityList);
        */
        return newTablespace;
    }
    public boolean isSucccess()
    {
        return this.isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangePrivileges;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnAddChangeVariable;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemovePrivilege;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JButton btnRemoveVariable;
    private javax.swing.JCheckBox cbCreate;
    private javax.swing.JCheckBox cbOptionCreate;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.JComboBox cbbRole;
    private javax.swing.JComboBox cbbVariableNames;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblLocation;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblVariableName;
    private javax.swing.JLabel lblVariableValue;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlPrivileges;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JPanel pnlVariables;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblPrivileges;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTable tblVariables;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarComment;
    private javax.swing.JTextField txfdLocation;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdVariableValue;
    // End of variables declaration//GEN-END:variables
}
