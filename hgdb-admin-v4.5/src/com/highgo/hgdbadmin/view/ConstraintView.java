/* ------------------------------------------------ 
* 
* File: ConstraintView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\ConstraintView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.ColInfoDTO;
import com.highgo.hgdbadmin.model.ColItemInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.ObjItemInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.AbstractButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * 
 * 
 */
public class ConstraintView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private TreeEnum.TreeNode constrType;
    private HelperInfoDTO helperInfo;
    private boolean inTabCreate;
    private boolean isSuccess;
   
    
    //add
    public ConstraintView(JFrame parent, boolean modal,
            TreeEnum.TreeNode constrType, ColItemInfoDTO[] columns, HelperInfoDTO helperInfo, boolean inTableCreation)
    {
        super(parent, modal);
        this.constrType = constrType;
        this.helperInfo = helperInfo;
        this.inTabCreate = inTableCreation;
        initComponents();
        isSuccess = false;          
        this.setBasicInfo(constrType, false);
        this.setNormalInfo(columns, false);     

        //define action
        this.setBtnOkEnable4Add();
        if (!inTableCreation)
        {
            jtp.addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent evt)
                {
                    jtpStateChanged4Add(evt);
                }
            });
        }
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformed4Add(e);
            }
        });
    }

    //property
    private ConstraintInfoDTO constrInfo;
    public ConstraintView(JFrame parent, boolean modal, 
            TreeEnum.TreeNode constrType, HelperInfoDTO helperInfo, ConstraintInfoDTO constrInfo)
    {
        super(parent, modal);
        this.constrType = constrType;
        this.helperInfo = helperInfo;
        this.constrInfo = constrInfo;
        this.inTabCreate = false;
        initComponents();
        isSuccess = false;
        this.setBasicInfo(constrType, true);
        ColItemInfoDTO[] localColumns = null;       
        try
        {
            TreeController tc= TreeController.getInstance();
            localColumns = tc.getColumnOfRelation(helperInfo, helperInfo.getRelationOid());
        } catch (ClassNotFoundException | SQLException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        this.setNormalInfo(localColumns, true);

        //property
        this.setProperty();
        
        //define action
        this.setBtnOkEnable4Change();
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                jtpStateChanged4Change(e);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformed4Change(e);
            }
        });
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        rbGroupNulls = new javax.swing.ButtonGroup();
        rbGroupUpdate = new javax.swing.ButtonGroup();
        rbGroupDelete = new javax.swing.ButtonGroup();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        cbbUseSlony = new javax.swing.JComboBox();
        lblUseSlony = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        ttarComment = new javax.swing.JTextArea();
        pnlDefinitionPK = new javax.swing.JPanel();
        lblTablespace = new javax.swing.JLabel();
        lblFillFactor = new javax.swing.JLabel();
        lblConstraint = new javax.swing.JLabel();
        lblDeferrable = new javax.swing.JLabel();
        txfdFillFactor = new javax.swing.JTextField();
        cbbTablespace = new javax.swing.JComboBox();
        cbDeferrable = new javax.swing.JCheckBox();
        lblIndex = new javax.swing.JLabel();
        cbbUsingIndex = new javax.swing.JComboBox();
        cbbAccessMethod = new javax.swing.JComboBox();
        lblAccessMethod = new javax.swing.JLabel();
        lblDeferred = new javax.swing.JLabel();
        cbDeferred = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        ttarConstraint = new javax.swing.JTextArea();
        pnlDefinitionFK = new javax.swing.JPanel();
        lblMatchFull1 = new javax.swing.JLabel();
        lblCoveringIndexFK = new javax.swing.JLabel();
        lblDeferrableFK = new javax.swing.JLabel();
        txfdlCoveringIndexFK = new javax.swing.JTextField();
        cbDeferrableFK = new javax.swing.JCheckBox();
        lblDoNotValidateFK = new javax.swing.JLabel();
        lblAutoFKIndexFK = new javax.swing.JLabel();
        lblDeferredFK = new javax.swing.JLabel();
        cbDeferredFK = new javax.swing.JCheckBox();
        cbMatchFullFK = new javax.swing.JCheckBox();
        cbNotValidateFK = new javax.swing.JCheckBox();
        cbAutoFKIndexFK = new javax.swing.JCheckBox();
        pnlDefinitionCheck = new javax.swing.JPanel();
        lblNoInherit = new javax.swing.JLabel();
        lblCheck = new javax.swing.JLabel();
        lblDoNotValidateCheck = new javax.swing.JLabel();
        cbNoInheritCheck = new javax.swing.JCheckBox();
        cbDoNotValidateCheck = new javax.swing.JCheckBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tarrCheck = new javax.swing.JTextArea();
        pnlColumns = new javax.swing.JPanel();
        spColumns = new javax.swing.JScrollPane();
        tblColumns = new javax.swing.JTable();
        btnAddChangeColumn = new javax.swing.JButton();
        btnRemoveColumn = new javax.swing.JButton();
        lblOperator = new javax.swing.JLabel();
        cbbOperator = new javax.swing.JComboBox();
        cbbColumn = new javax.swing.JComboBox();
        lblColumn = new javax.swing.JLabel();
        cbbOperatorClass = new javax.swing.JComboBox();
        lblOperatorClass = new javax.swing.JLabel();
        lblDesc = new javax.swing.JLabel();
        cbDesc = new javax.swing.JCheckBox();
        rbFirst = new javax.swing.JRadioButton();
        rbLast = new javax.swing.JRadioButton();
        lblNulls = new javax.swing.JLabel();
        pnlColumnsFK = new javax.swing.JPanel();
        spColumns1 = new javax.swing.JScrollPane();
        tblColumnsFK = new javax.swing.JTable();
        btnAddChangeColumnFK = new javax.swing.JButton();
        btnRemoveColumnFK = new javax.swing.JButton();
        lblReferencing = new javax.swing.JLabel();
        cbbReferencingColumn = new javax.swing.JComboBox();
        cbbReferencesTable = new javax.swing.JComboBox();
        lblReferences = new javax.swing.JLabel();
        cbbLocalColumn = new javax.swing.JComboBox();
        lblLocalColumn = new javax.swing.JLabel();
        pnlAction = new javax.swing.JPanel();
        pnlDelete = new javax.swing.JPanel();
        rbSetDefaultDelete = new javax.swing.JRadioButton();
        rbSetNullDelete = new javax.swing.JRadioButton();
        rbCascadeDelete = new javax.swing.JRadioButton();
        rbRestrictDelete = new javax.swing.JRadioButton();
        rbNoActionDelete = new javax.swing.JRadioButton();
        pnlUpdate = new javax.swing.JPanel();
        rbNoAction = new javax.swing.JRadioButton();
        rbRestrict = new javax.swing.JRadioButton();
        rbCascade = new javax.swing.JRadioButton();
        rbSetNull = new javax.swing.JRadioButton();
        rbSetDefault = new javax.swing.JRadioButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlColumnsExclude = new javax.swing.JPanel();
        spColumns2 = new javax.swing.JScrollPane();
        tblColumnsExclude = new javax.swing.JTable();
        btnAddChangeColumnExclude = new javax.swing.JButton();
        btnRemoveColumnExclude = new javax.swing.JButton();
        lblOperator1 = new javax.swing.JLabel();
        cbbOperatorExclude = new javax.swing.JComboBox();
        cbbColumnExclude = new javax.swing.JComboBox();
        lblColumn1 = new javax.swing.JLabel();
        cbbOperatorClassExclude = new javax.swing.JComboBox();
        lblOperatorClass1 = new javax.swing.JLabel();
        lblDesc1 = new javax.swing.JLabel();
        cbDescExclude = new javax.swing.JCheckBox();
        rbFirstExclude = new javax.swing.JRadioButton();
        rbLastExclude = new javax.swing.JRadioButton();
        lblNulls1 = new javax.swing.JLabel();
        jtp = new javax.swing.JTabbedPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
        pnlProperties.setMinimumSize(new java.awt.Dimension(500, 440));
        pnlProperties.setPreferredSize(new java.awt.Dimension(500, 440));

        lblName.setText(constBundle.getString("name"));

        lblComment.setText(constBundle.getString("comment"));

        txfdName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));

        cbbUseSlony.setEditable(true);
        cbbUseSlony.setEnabled(false);

        lblUseSlony.setText(constBundle.getString("useSlony"));

        ttarComment.setColumns(20);
        ttarComment.setRows(5);
        ttarComment.setEnabled(false);
        jScrollPane4.setViewportView(ttarComment);

        javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
        pnlProperties.setLayout(pnlPropertiesLayout);
        pnlPropertiesLayout.setHorizontalGroup(
            pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblComment, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblName, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 404, Short.MAX_VALUE)
                            .addComponent(txfdName)))
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbbUseSlony, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlPropertiesLayout.setVerticalGroup(
            pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName)
                    .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addComponent(lblComment)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUseSlony)
                    .addComponent(cbbUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlDefinitionPK.setBackground(new java.awt.Color(255, 255, 255));
        pnlDefinitionPK.setMinimumSize(new java.awt.Dimension(520, 470));
        pnlDefinitionPK.setPreferredSize(new java.awt.Dimension(520, 470));

        lblTablespace.setText(constBundle.getString("tablespace"));

        lblFillFactor.setText(constBundle.getString("fillFactor"));

        lblConstraint.setText(constBundle.getString("constraints"));

        lblDeferrable.setText(constBundle.getString("deferrable"));

        cbDeferrable.setBackground(new java.awt.Color(255, 255, 255));

        lblIndex.setText(constBundle.getString("index"));

        cbbUsingIndex.setEnabled(false);

        cbbAccessMethod.setEnabled(false);
        cbbAccessMethod.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                cbbAccessMethodItemStateChanged(evt);
            }
        });

        lblAccessMethod.setText(constBundle.getString("accessMethod"));

        lblDeferred.setText(constBundle.getString("deferred"));

        cbDeferred.setBackground(new java.awt.Color(255, 255, 255));

        ttarConstraint.setColumns(20);
        ttarConstraint.setRows(5);
        ttarConstraint.setEnabled(false);
        jScrollPane2.setViewportView(ttarConstraint);

        javax.swing.GroupLayout pnlDefinitionPKLayout = new javax.swing.GroupLayout(pnlDefinitionPK);
        pnlDefinitionPK.setLayout(pnlDefinitionPKLayout);
        pnlDefinitionPKLayout.setHorizontalGroup(
            pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionPKLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDefinitionPKLayout.createSequentialGroup()
                        .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblFillFactor, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                            .addComponent(lblAccessMethod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblIndex, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTablespace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbbTablespace, 0, 430, Short.MAX_VALUE)
                            .addComponent(cbbUsingIndex, 0, 430, Short.MAX_VALUE)
                            .addComponent(cbbAccessMethod, 0, 430, Short.MAX_VALUE)
                            .addComponent(txfdFillFactor, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)))
                    .addGroup(pnlDefinitionPKLayout.createSequentialGroup()
                        .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblDeferred, javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE)
                            .addComponent(lblDeferrable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblConstraint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDefinitionPKLayout.createSequentialGroup()
                                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbDeferred, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbDeferrable))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlDefinitionPKLayout.setVerticalGroup(
            pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionPKLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTablespace)
                    .addComponent(cbbTablespace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblIndex)
                    .addComponent(cbbUsingIndex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccessMethod)
                    .addComponent(cbbAccessMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDeferrable, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDeferrable))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDeferred, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDeferred, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlDefinitionPKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
                    .addGroup(pnlDefinitionPKLayout.createSequentialGroup()
                        .addComponent(lblConstraint)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pnlDefinitionFK.setBackground(new java.awt.Color(255, 255, 255));
        pnlDefinitionFK.setMinimumSize(new java.awt.Dimension(520, 470));

        lblMatchFull1.setText(constBundle.getString("matchFull"));

        lblCoveringIndexFK.setText(constBundle.getString("coveringIndex"));
        lblCoveringIndexFK.setEnabled(false);

        lblDeferrableFK.setText(constBundle.getString("deferrable"));

        txfdlCoveringIndexFK.setEnabled(false);

        cbDeferrableFK.setBackground(new java.awt.Color(255, 255, 255));
        cbDeferrableFK.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbDeferrableFKActionPerformed(evt);
            }
        });

        lblDoNotValidateFK.setText(constBundle.getString("doNotValidate"));

        lblAutoFKIndexFK.setText(constBundle.getString("autoFKIndex"));
        lblAutoFKIndexFK.setEnabled(false);

        lblDeferredFK.setText(constBundle.getString("deferred"));

        cbDeferredFK.setBackground(new java.awt.Color(255, 255, 255));
        cbDeferredFK.setEnabled(false);

        cbMatchFullFK.setBackground(new java.awt.Color(255, 255, 255));

        cbNotValidateFK.setBackground(new java.awt.Color(255, 255, 255));

        cbAutoFKIndexFK.setBackground(new java.awt.Color(255, 255, 255));
        cbAutoFKIndexFK.setEnabled(false);
        cbAutoFKIndexFK.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbAutoFKIndexFKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDefinitionFKLayout = new javax.swing.GroupLayout(pnlDefinitionFK);
        pnlDefinitionFK.setLayout(pnlDefinitionFKLayout);
        pnlDefinitionFKLayout.setHorizontalGroup(
            pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionFKLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblCoveringIndexFK, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                    .addComponent(lblAutoFKIndexFK, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                    .addComponent(lblDoNotValidateFK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblMatchFull1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDeferrableFK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblDeferredFK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDefinitionFKLayout.createSequentialGroup()
                        .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbDeferredFK, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbDeferrableFK)
                                    .addComponent(cbMatchFullFK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(cbNotValidateFK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cbAutoFKIndexFK, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 417, Short.MAX_VALUE))
                    .addComponent(txfdlCoveringIndexFK)))
        );
        pnlDefinitionFKLayout.setVerticalGroup(
            pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionFKLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDeferrableFK, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDeferrableFK))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbDeferredFK, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDeferredFK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cbMatchFullFK, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblMatchFull1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(cbNotValidateFK, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDoNotValidateFK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbAutoFKIndexFK, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblAutoFKIndexFK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCoveringIndexFK)
                    .addComponent(txfdlCoveringIndexFK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(312, Short.MAX_VALUE))
        );

        pnlDefinitionCheck.setBackground(new java.awt.Color(255, 255, 255));
        pnlDefinitionCheck.setMinimumSize(new java.awt.Dimension(520, 470));

        lblNoInherit.setText(constBundle.getString("noInherit"));
        lblNoInherit.setMaximumSize(new java.awt.Dimension(63, 15));
        lblNoInherit.setMinimumSize(new java.awt.Dimension(63, 15));
        lblNoInherit.setPreferredSize(new java.awt.Dimension(63, 15));

        lblCheck.setText(constBundle.getString("check"));
        lblCheck.setMaximumSize(new java.awt.Dimension(63, 15));
        lblCheck.setMinimumSize(new java.awt.Dimension(63, 15));
        lblCheck.setPreferredSize(new java.awt.Dimension(63, 15));

        lblDoNotValidateCheck.setText(constBundle.getString("doNotValidate"));

        cbNoInheritCheck.setBackground(new java.awt.Color(255, 255, 255));

        cbDoNotValidateCheck.setBackground(new java.awt.Color(255, 255, 255));

        tarrCheck.setColumns(20);
        tarrCheck.setRows(5);
        jScrollPane1.setViewportView(tarrCheck);

        javax.swing.GroupLayout pnlDefinitionCheckLayout = new javax.swing.GroupLayout(pnlDefinitionCheck);
        pnlDefinitionCheck.setLayout(pnlDefinitionCheckLayout);
        pnlDefinitionCheckLayout.setHorizontalGroup(
            pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionCheckLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDefinitionCheckLayout.createSequentialGroup()
                        .addComponent(lblCheck, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 420, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pnlDefinitionCheckLayout.createSequentialGroup()
                        .addGroup(pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblDoNotValidateCheck, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblNoInherit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbNoInheritCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbDoNotValidateCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        pnlDefinitionCheckLayout.setVerticalGroup(
            pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionCheckLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlDefinitionCheckLayout.createSequentialGroup()
                        .addComponent(lblCheck, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblNoInherit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbNoInheritCheck, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionCheckLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDoNotValidateCheck, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDoNotValidateCheck, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlColumns.setBackground(new java.awt.Color(255, 255, 255));
        pnlColumns.setPreferredSize(new java.awt.Dimension(520, 500));

        tblColumns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", "", "", "", ""
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        tblColumns.setGridColor(new java.awt.Color(255, 255, 255));
        tblColumns.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
                tblColumnsMousePressed(evt);
            }
        });
        spColumns.setViewportView(tblColumns);
        if (tblColumns.getColumnModel().getColumnCount() > 0)
        {
            tblColumns.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("columnName"));
            tblColumns.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("order"));
            tblColumns.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("nullsOrder"));
            tblColumns.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("operatorClass"));
            tblColumns.getColumnModel().getColumn(4).setHeaderValue(constBundle.getString("operator"));
        }

        btnAddChangeColumn.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
        btnAddChangeColumn.setMaximumSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumn.setMinimumSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumn.setPreferredSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumn.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddChangeColumnActionPerformed(evt);
            }
        });

        btnRemoveColumn.setText(constBundle.getString("remove"));
        btnRemoveColumn.setMaximumSize(new java.awt.Dimension(80, 23));
        btnRemoveColumn.setMinimumSize(new java.awt.Dimension(80, 23));
        btnRemoveColumn.setPreferredSize(new java.awt.Dimension(80, 23));
        btnRemoveColumn.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRemoveColumnActionPerformed(evt);
            }
        });

        lblOperator.setText(constBundle.getString("operator"));

        cbbOperator.setEnabled(false);

        lblColumn.setText(constBundle.getString("column"));

        cbbOperatorClass.setEnabled(false);

        lblOperatorClass.setText(constBundle.getString("operatorClass"));

        lblDesc.setText("DESC");

        cbDesc.setBackground(new java.awt.Color(255, 255, 255));
        cbDesc.setEnabled(false);

        rbFirst.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupNulls.add(rbFirst);
        rbFirst.setText("FIRST");
        rbFirst.setEnabled(false);

        rbLast.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupNulls.add(rbLast);
        rbLast.setText("LAST");
        rbLast.setEnabled(false);

        lblNulls.setText("NULLs");

        javax.swing.GroupLayout pnlColumnsLayout = new javax.swing.GroupLayout(pnlColumns);
        pnlColumns.setLayout(pnlColumnsLayout);
        pnlColumnsLayout.setHorizontalGroup(
            pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(spColumns, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlColumnsLayout.createSequentialGroup()
                        .addComponent(btnAddChangeColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlColumnsLayout.createSequentialGroup()
                        .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblOperator, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(lblDesc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblOperatorClass, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblColumn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbbColumn, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbbOperatorClass, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlColumnsLayout.createSequentialGroup()
                                .addComponent(cbDesc)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblNulls)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbFirst)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbLast))
                            .addComponent(cbbOperator, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlColumnsLayout.setVerticalGroup(
            pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlColumnsLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spColumns, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddChangeColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblColumn)
                    .addComponent(cbbColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOperatorClass)
                    .addComponent(cbbOperatorClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDesc)
                    .addComponent(cbDesc)
                    .addComponent(rbFirst)
                    .addComponent(rbLast)
                    .addComponent(lblNulls))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOperator)
                    .addComponent(cbbOperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlColumns.getAccessibleContext().setAccessibleName("");

        pnlColumnsFK.setBackground(new java.awt.Color(255, 255, 255));
        pnlColumnsFK.setPreferredSize(new java.awt.Dimension(520, 500));

        tblColumnsFK.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", "", ""
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        spColumns1.setViewportView(tblColumnsFK);
        if (tblColumnsFK.getColumnModel().getColumnCount() > 0)
        {
            tblColumnsFK.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("localColumn"));
            tblColumnsFK.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("referencingColumn"));
            tblColumnsFK.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("referencedTable"));
        }

        btnAddChangeColumnFK.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
        btnAddChangeColumnFK.setMaximumSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumnFK.setMinimumSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumnFK.setPreferredSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumnFK.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddChangeColumnFKActionPerformed(evt);
            }
        });

        btnRemoveColumnFK.setText(constBundle.getString("remove"));
        btnRemoveColumnFK.setMaximumSize(new java.awt.Dimension(80, 23));
        btnRemoveColumnFK.setMinimumSize(new java.awt.Dimension(80, 23));
        btnRemoveColumnFK.setPreferredSize(new java.awt.Dimension(80, 23));
        btnRemoveColumnFK.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRemoveColumnFKActionPerformed(evt);
            }
        });

        lblReferencing.setText(constBundle.getString("referencingColumn"));

        cbbReferencesTable.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                cbbReferencesTableItemStateChanged(evt);
            }
        });

        lblReferences.setText(constBundle.getString("referencedTable"));

        lblLocalColumn.setText(constBundle.getString("localColumn"));

        javax.swing.GroupLayout pnlColumnsFKLayout = new javax.swing.GroupLayout(pnlColumnsFK);
        pnlColumnsFK.setLayout(pnlColumnsFKLayout);
        pnlColumnsFKLayout.setHorizontalGroup(
            pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsFKLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(spColumns1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlColumnsFKLayout.createSequentialGroup()
                        .addComponent(btnAddChangeColumnFK, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoveColumnFK, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlColumnsFKLayout.createSequentialGroup()
                        .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblReferencing, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(lblLocalColumn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblReferences, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbbReferencesTable, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbbLocalColumn, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbbReferencingColumn, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlColumnsFKLayout.setVerticalGroup(
            pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlColumnsFKLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spColumns1, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddChangeColumnFK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveColumnFK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblReferences)
                    .addComponent(cbbReferencesTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblLocalColumn)
                    .addComponent(cbbLocalColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsFKLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblReferencing)
                    .addComponent(cbbReferencingColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pnlAction.setBackground(new java.awt.Color(255, 255, 255));
        pnlAction.setMinimumSize(new java.awt.Dimension(520, 470));

        pnlDelete.setBackground(new java.awt.Color(255, 255, 255));
        pnlDelete.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true), constBundle.getString("onDelete")));

        rbSetDefaultDelete.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupDelete.add(rbSetDefaultDelete);
        rbSetDefaultDelete.setText("SET DEFAULT");

        rbSetNullDelete.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupDelete.add(rbSetNullDelete);
        rbSetNullDelete.setText("SET NULL");

        rbCascadeDelete.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupDelete.add(rbCascadeDelete);
        rbCascadeDelete.setText("CASCADE");

        rbRestrictDelete.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupDelete.add(rbRestrictDelete);
        rbRestrictDelete.setText("RESTRICT");

        rbNoActionDelete.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupDelete.add(rbNoActionDelete);
        rbNoActionDelete.setSelected(true);
        rbNoActionDelete.setText("NO ACTION");

        javax.swing.GroupLayout pnlDeleteLayout = new javax.swing.GroupLayout(pnlDelete);
        pnlDelete.setLayout(pnlDeleteLayout);
        pnlDeleteLayout.setHorizontalGroup(
            pnlDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDeleteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbRestrictDelete)
                    .addComponent(rbCascadeDelete)
                    .addComponent(rbSetNullDelete)
                    .addComponent(rbSetDefaultDelete)
                    .addComponent(rbNoActionDelete))
                .addContainerGap(138, Short.MAX_VALUE))
        );
        pnlDeleteLayout.setVerticalGroup(
            pnlDeleteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDeleteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rbNoActionDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbRestrictDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbCascadeDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbSetNullDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbSetDefaultDelete)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pnlUpdate.setBackground(new java.awt.Color(255, 255, 255));
        pnlUpdate.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true), constBundle.getString("onUpdate")));

        rbNoAction.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupUpdate.add(rbNoAction);
        rbNoAction.setSelected(true);
        rbNoAction.setText("NO ACTION");

        rbRestrict.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupUpdate.add(rbRestrict);
        rbRestrict.setText("RESTRICT");

        rbCascade.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupUpdate.add(rbCascade);
        rbCascade.setText("CASCADE");

        rbSetNull.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupUpdate.add(rbSetNull);
        rbSetNull.setText("SET NULL");

        rbSetDefault.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupUpdate.add(rbSetDefault);
        rbSetDefault.setText("SET DEFAULT");

        javax.swing.GroupLayout pnlUpdateLayout = new javax.swing.GroupLayout(pnlUpdate);
        pnlUpdate.setLayout(pnlUpdateLayout);
        pnlUpdateLayout.setHorizontalGroup(
            pnlUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlUpdateLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbNoAction)
                    .addComponent(rbRestrict)
                    .addComponent(rbCascade)
                    .addComponent(rbSetNull)
                    .addComponent(rbSetDefault))
                .addContainerGap(138, Short.MAX_VALUE))
        );
        pnlUpdateLayout.setVerticalGroup(
            pnlUpdateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlUpdateLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rbNoAction)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbRestrict)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbCascade)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbSetNull)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rbSetDefault)
                .addContainerGap(298, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlActionLayout = new javax.swing.GroupLayout(pnlAction);
        pnlAction.setLayout(pnlActionLayout);
        pnlActionLayout.setHorizontalGroup(
            pnlActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlActionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pnlDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlActionLayout.setVerticalGroup(
            pnlActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlActionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pnlUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
        pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));

        cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
        cbReadOnly.setSelected(true);
        cbReadOnly.setText(constBundle.getString("readOnly"));
        cbReadOnly.setActionCommand("");
        cbReadOnly.setEnabled(false);
        cbReadOnly.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbReadOnlyActionPerformed(evt);
            }
        });

        tpDefinitionSQL.setEditable(false);
        spDefinationSQL.setViewportView(tpDefinitionSQL);

        javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
        pnlSQL.setLayout(pnlSQLLayout);
        pnlSQLLayout.setHorizontalGroup(
            pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSQLLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbReadOnly)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlSQLLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlSQLLayout.setVerticalGroup(
            pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSQLLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbReadOnly)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnlColumnsExclude.setBackground(new java.awt.Color(255, 255, 255));
        pnlColumnsExclude.setPreferredSize(new java.awt.Dimension(520, 500));

        tblColumnsExclude.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", "", "", "", ""
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        tblColumnsExclude.setGridColor(new java.awt.Color(255, 255, 255));
        tblColumnsExclude.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
                tblColumnsExcludeMousePressed(evt);
            }
        });
        spColumns2.setViewportView(tblColumnsExclude);
        if (tblColumnsExclude.getColumnModel().getColumnCount() > 0)
        {
            tblColumnsExclude.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("columnName"));
            tblColumnsExclude.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("order"));
            tblColumnsExclude.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("nullsOrder"));
            tblColumnsExclude.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("operatorClass"));
            tblColumnsExclude.getColumnModel().getColumn(4).setHeaderValue(constBundle.getString("operator"));
        }

        btnAddChangeColumnExclude.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
        btnAddChangeColumnExclude.setMaximumSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumnExclude.setMinimumSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumnExclude.setPreferredSize(new java.awt.Dimension(80, 23));
        btnAddChangeColumnExclude.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddChangeColumnExcludeActionPerformed(evt);
            }
        });

        btnRemoveColumnExclude.setText(constBundle.getString("remove"));
        btnRemoveColumnExclude.setMaximumSize(new java.awt.Dimension(80, 23));
        btnRemoveColumnExclude.setMinimumSize(new java.awt.Dimension(80, 23));
        btnRemoveColumnExclude.setPreferredSize(new java.awt.Dimension(80, 23));
        btnRemoveColumnExclude.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRemoveColumnExcludeActionPerformed(evt);
            }
        });

        lblOperator1.setText(constBundle.getString("operator"));

        cbbOperatorExclude.setEnabled(false);
        cbbOperatorExclude.setModel(new DefaultComboBoxModel(new String[] { "","<", "<=", "<>", "=", ">", ">=", "~<=~", "~<~", "~>=~", "~>~" }));

        lblColumn1.setText(constBundle.getString("column"));

        cbbOperatorClassExclude.setEnabled(false);
        cbbOperatorClassExclude.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "bpchar_pattern_ops", "cidr_ops", "text_pattern_ops", "varchar_ops", "varchar_pattern_ops" }));

        lblOperatorClass1.setText(constBundle.getString("operatorClass"));

        lblDesc1.setText("DESC");

        cbDescExclude.setBackground(new java.awt.Color(255, 255, 255));
        cbDescExclude.setEnabled(false);

        rbFirstExclude.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupNulls.add(rbFirstExclude);
        rbFirstExclude.setText("FIRST");
        rbFirstExclude.setEnabled(false);

        rbLastExclude.setBackground(new java.awt.Color(255, 255, 255));
        rbGroupNulls.add(rbLastExclude);
        rbLastExclude.setSelected(true);
        rbLastExclude.setText("LAST");
        rbLastExclude.setEnabled(false);

        lblNulls1.setText("NULLs");

        javax.swing.GroupLayout pnlColumnsExcludeLayout = new javax.swing.GroupLayout(pnlColumnsExclude);
        pnlColumnsExclude.setLayout(pnlColumnsExcludeLayout);
        pnlColumnsExcludeLayout.setHorizontalGroup(
            pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsExcludeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(spColumns2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlColumnsExcludeLayout.createSequentialGroup()
                        .addComponent(btnAddChangeColumnExclude, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoveColumnExclude, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlColumnsExcludeLayout.createSequentialGroup()
                        .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblOperator1, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                            .addComponent(lblDesc1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblOperatorClass1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblColumn1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbbColumnExclude, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbbOperatorClassExclude, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlColumnsExcludeLayout.createSequentialGroup()
                                .addComponent(cbDescExclude)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblNulls1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbFirstExclude)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(rbLastExclude))
                            .addComponent(cbbOperatorExclude, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlColumnsExcludeLayout.setVerticalGroup(
            pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlColumnsExcludeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spColumns2, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddChangeColumnExclude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveColumnExclude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblColumn1)
                    .addComponent(cbbColumnExclude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOperatorClass1)
                    .addComponent(cbbOperatorClassExclude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDesc1)
                    .addComponent(cbDescExclude)
                    .addComponent(rbFirstExclude)
                    .addComponent(rbLastExclude)
                    .addComponent(lblNulls1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlColumnsExcludeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOperator1)
                    .addComponent(cbbOperatorExclude, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(450, 450));
        setModal(true);
        setName("dlgServerAdd"); // NOI18N

        jtp.setBackground(new java.awt.Color(255, 255, 255));
        jtp.setMinimumSize(new java.awt.Dimension(520, 470));
        jtp.setPreferredSize(new java.awt.Dimension(520, 470));
        getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
        jtp.getAccessibleContext().setAccessibleName("");

        btnHelp.setText(constBundle.getString("help"));
        btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
        btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
        btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
        btnHelp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnHelpActionPerformed(evt);
            }
        });

        btnOK.setText(constBundle.getString("ok"));
        btnOK.setEnabled(false);
        btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
        btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
        btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

        btnCancle.setText(constBundle.getString("cancle"));
        btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
        btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
        btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCancle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnCancleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 329, Short.MAX_VALUE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    //normal info for both add and change
    private void setBasicInfo(TreeEnum.TreeNode constrType, boolean isChange)
    {
        switch (constrType)
        {
            case PRIMARY_KEY:
                this.setIconImage(Toolkit.getDefaultToolkit().getImage(
                        getClass().getResource("/com/highgo/hgdbadmin/image/primarykey.png")));
                if (isChange)
                {
                    this.setTitle(constBundle.getString("primaryKey") + " " + constrInfo.getName());
                } else
                {
                    this.setTitle(constBundle.getString("addPKey"));
                }

                jtp.add(constBundle.getString("property"), pnlProperties);
                jtp.add(constBundle.getString("definition"), pnlDefinitionPK);
                jtp.add(constBundle.getString("columns"), pnlColumns);
                cbbUsingIndex.setEnabled(true);
                //cbbOperatorClass.setEnabled(false);
                //cbbOperator.setEnabled(false);
                //cbDesc.setEnabled(false);
                //rbFirst.setEnabled(false);
                //rbLast.setEnabled(false);
                break;
            case UNIQUE:
                this.setIconImage(Toolkit.getDefaultToolkit().getImage(
                        getClass().getResource("/com/highgo/hgdbadmin/image/unique.png")));
                if (isChange)
                {
                    this.setTitle(constBundle.getString("unique") + " " + constrInfo.getName());
                } else
                {
                    this.setTitle(constBundle.getString("addUnique"));
                }

                jtp.add(constBundle.getString("property"), pnlProperties);
                jtp.add(constBundle.getString("definition"), pnlDefinitionPK);
                jtp.add(constBundle.getString("columns"), pnlColumns);
                cbbUsingIndex.setEnabled(true);
                //cbbOperatorClass.setEnabled(false);
                //cbbOperator.setEnabled(false);
                //cbDesc.setEnabled(false);
                //rbFirst.setEnabled(false);
                //rbLast.setEnabled(false);
                break;
            case FOREIGN_KEY:
                this.setIconImage(Toolkit.getDefaultToolkit().getImage(
                        getClass().getResource("/com/highgo/hgdbadmin/image/foreignkey.png")));
                if (isChange)
                {
                    this.setTitle(constBundle.getString("foreignKey") + " " + constrInfo.getName());
                } else
                {
                    this.setTitle(constBundle.getString("addFKey"));
                }

                jtp.add(constBundle.getString("property"), pnlProperties);
                jtp.add(constBundle.getString("definition"), pnlDefinitionFK);
                jtp.add(constBundle.getString("columns"), pnlColumnsFK);
                jtp.add(constBundle.getString("action"), pnlAction);
                /*tblColumnsFK.getModel().addTableModelListener(new TableModelListener()
                {
                    @Override
                    public void tableChanged(TableModelEvent evt)
                    {
                        btnAddChangeColumnFK.setEnabled((tblColumnsFK.getRowCount() <= 0));
                    }
                });
                */
                break;         
            case EXCLUDE:
                this.setIconImage(Toolkit.getDefaultToolkit().getImage(
                        getClass().getResource("/com/highgo/hgdbadmin/image/exclude.png")));
                this.setTitle(isChange ? constBundle.getString("exclude") + " " + constrInfo.getName()
                        : constBundle.getString("addExclude"));

                jtp.add(constBundle.getString("property"), pnlProperties);
                jtp.add(constBundle.getString("definition"), pnlDefinitionPK);
                cbDeferred.setEnabled(false);
                ttarConstraint.setEnabled(!isChange);
                jtp.add(constBundle.getString("columns"), pnlColumnsExclude);                
                cbbAccessMethod.setModel(new DefaultComboBoxModel(new String[]
                {
                    "", "btree", "gist", "hash", "spgist"
                }));
                cbbAccessMethod.setEnabled(true);
                cbbAccessMethod.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e)
                    {
                        changeOperatorClassWhenAccessMethodChanged();
                    }
                });
                cbbColumnExclude.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e)
                    {
                        changeOperatorWhenColumnChanged();
                    }
                });
                try
                {
                    cbbOperatorClassExclude.setModel(new DefaultComboBoxModel(
                            TreeController.getInstance().getOperatorClassOfAccessMethod(helperInfo, "btree")));
                    cbbOperatorClassExclude.setEnabled(true);
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
                cbbOperatorExclude.setModel(new DefaultComboBoxModel(new String[]
                {
                }));
                cbbOperatorExclude.setEnabled(true);
                cbDescExclude.setEnabled(true);
                rbFirstExclude.setEnabled(true);
                rbLastExclude.setEnabled(true);
                break;
            case CHECK:
                this.setIconImage(Toolkit.getDefaultToolkit().getImage(
                        getClass().getResource("/com/highgo/hgdbadmin/image/check.png")));
                if (isChange)
                {
                    this.setTitle(constBundle.getString("check") + " " + constrInfo.getName());
                } else
                {
                    this.setTitle(constBundle.getString("addCheck"));
                }
                jtp.add(constBundle.getString("property"), pnlProperties);
                jtp.add(constBundle.getString("definition"), pnlDefinitionCheck);
                break;
        }

        if (!inTabCreate)
        {
            ttarComment.setEnabled(true);
            pnlSQL.getAccessibleContext().setAccessibleName("SQL");
            jtp.add("SQL", pnlSQL);
        }
    }
    private void setNormalInfo(ColItemInfoDTO[] columns, boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
        switch (constrType)
        {
            case CHECK:
                cbDoNotValidateCheck.setEnabled(tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0));
                break;
            case FOREIGN_KEY:
                cbNotValidateFK.setEnabled(tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0));
                cbbLocalColumn.setModel(new DefaultComboBoxModel(columns));
                cbbLocalColumn.setSelectedIndex(-1);
                try
                {
                    cbbReferencesTable.setModel(new DefaultComboBoxModel(tc.getParentTables(helperInfo)));
                    cbbReferencesTable.setSelectedIndex(-1);
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
                break;
            case EXCLUDE:
                cbbColumnExclude.setModel(new DefaultComboBoxModel(columns));
                cbbColumnExclude.setSelectedIndex(-1);
                try
                {
                    cbbTablespace.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "tablespace")));
                    if(isChange)
                    {
                        cbbTablespace.removeItemAt(0);
                    }else
                    {
                        cbbTablespace.setSelectedIndex(0);
                    }
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
                if (inTabCreate)
                {
                    cbbOperatorExclude.setModel(new DefaultComboBoxModel(new String[]{}));
                }
                break;
            case PRIMARY_KEY:
            case UNIQUE:
                cbbColumn.setModel(new DefaultComboBoxModel(columns));
                cbbColumn.setSelectedIndex(-1);
                try
                {
                    cbbTablespace.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "tablespace")));
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
                if (!inTabCreate)
                {
                    try
                    {
                        cbbUsingIndex.setModel(new DefaultComboBoxModel(tc.getIndexOfTable(helperInfo, helperInfo.getRelationOid())));
                        cbbUsingIndex.setSelectedIndex(-1);
                        cbbUsingIndex.setEnabled(true);                       
                    } catch (Exception ex)
                    {
                        JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
        }
        switch (constrType)
        {
            case PRIMARY_KEY:
            case UNIQUE:
            case EXCLUDE:
                txfdFillFactor.addKeyListener(new KeyAdapter()
                {
                    @Override
                    public void keyTyped(KeyEvent evt)
                    {
                        valueKeyTypedAction(evt);
                    }
                });
                break;
        } 
    }
    //property for change
    private void setProperty()
    {
        btnOK.setEnabled(false);
        txfdName.setText(constrInfo.getName());
        ttarComment.setText(constrInfo.getComment());
        TreeEnum.TreeNode enumType = constrInfo.getType();
        switch (enumType)
        {
            case PRIMARY_KEY:
            case UNIQUE:
            case EXCLUDE:
                //in fact we cannot get using index, this is only for create 
                cbbUsingIndex.setSelectedItem(constrInfo.getUsingIndex());
                cbbUsingIndex.setEnabled(false);

                cbbTablespace.setSelectedItem(constrInfo.getTablespace());
                
                cbbAccessMethod.addItem(constrInfo.getAccessMethod());
                cbbAccessMethod.setSelectedItem(constrInfo.getAccessMethod());
                cbbAccessMethod.setEnabled(false);
                
                txfdFillFactor.setText(constrInfo.getFillFactor());
                
                cbDeferrable.setSelected(constrInfo.isDeferrable());
                cbDeferrable.setEnabled(false);
                
                cbDeferred.setSelected(constrInfo.isDeferred());
                cbDeferred.setEnabled(false);
                
                ttarConstraint.setText(constrInfo.getConstraint());
                ttarConstraint.setEnabled(false);//enumType == TreeEnum.TreeNode.EXCLUDE
                if (enumType.equals(TreeEnum.TreeNode.EXCLUDE))
                {
                    btnAddChangeColumnExclude.setEnabled(false);
                    btnRemoveColumnExclude.setEnabled(false);
                    cbbColumnExclude.setEnabled(false);
                } else
                {
                    btnAddChangeColumn.setEnabled(false);
                    btnRemoveColumn.setEnabled(false);
                    cbbColumn.setEnabled(false);
                }               
                break;
            case FOREIGN_KEY:
                cbDeferrableFK.setSelected(constrInfo.isDeferrable());
                cbDeferrableFK.setEnabled(false);
                cbDeferredFK.setSelected(constrInfo.isDeferred());
                cbDeferredFK.setEnabled(false);
                cbMatchFullFK.setSelected(constrInfo.getMatchType() != null && constrInfo.getMatchType().equals("FULL"));
                cbMatchFullFK.setEnabled(false);
                cbNotValidateFK.setSelected(!constrInfo.isValidate());
                cbNotValidateFK.setEnabled(!constrInfo.isValidate());

                //in fact we cannot get this covering index,this only for create
                cbAutoFKIndexFK.setSelected(constrInfo.getCoveringIndex() != null && !constrInfo.getCoveringIndex().isEmpty());
                cbAutoFKIndexFK.setEnabled(false);
                txfdlCoveringIndexFK.setText(constrInfo.getCoveringIndex());
                txfdlCoveringIndexFK.setEnabled(false);
                
                btnRemoveColumnFK.setEnabled(false);
                cbbReferencesTable.setEnabled(false);
                cbbLocalColumn.setEnabled(false);
                cbbReferencingColumn.setEnabled(false);
                
                String updateAction = constrInfo.getUpdateActions();
                switch (updateAction)
                {
                    case "NO ACTION":
                        rbNoAction.setSelected(true);
                        break;
                    case "RESTRICT":
                        rbRestrict.setSelected(true);
                        break;
                    case "CASCADE":
                        rbCascade.setSelected(true);
                        break;
                    case "SET NULL":
                        rbSetNull.setSelected(true);
                        break;
                    case "SET DEFAULT":
                        rbSetDefault.setSelected(true);
                        break;
                }
                rbNoAction.setEnabled(false);
                rbRestrict.setEnabled(false);
                rbCascade.setEnabled(false);
                rbSetNull.setEnabled(false);
                rbSetDefault.setEnabled(false);
                String deleteAction = constrInfo.getDeleteAction();
                switch(deleteAction)
                {
                     case "NO ACTION":
                        rbNoActionDelete.setSelected(true);
                        break;
                    case "RESTRICT":
                        rbRestrictDelete.setSelected(true);
                        break;
                    case "CASCADE":
                        rbCascadeDelete.setSelected(true);
                        break;
                    case "SET NULL":
                        rbSetNullDelete.setSelected(true);
                        break;
                    case "SET DEFAULT":
                        rbSetDefaultDelete.setSelected(true);
                        break;
                }
                rbNoActionDelete.setEnabled(false);
                rbRestrictDelete.setEnabled(false);
                rbCascadeDelete.setEnabled(false);
                rbSetNullDelete.setEnabled(false);
                rbSetDefaultDelete.setEnabled(false);
                break;
            case CHECK:
                tarrCheck.setText(constrInfo.getCheckConditonQuoted());
                tarrCheck.setEnabled(false);
                cbNoInheritCheck.setSelected(constrInfo.isNoInherit());
                cbNoInheritCheck.setEnabled(false);
                cbDoNotValidateCheck.setSelected(!constrInfo.isValidate());
                cbDoNotValidateCheck.setEnabled(!constrInfo.isValidate());
                break;
        }
        List<ColInfoDTO> columnList = constrInfo.getColInfoList();
        switch (enumType)
        {
            case PRIMARY_KEY:
            case UNIQUE:
                DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
                for (ColInfoDTO columnInfo : columnList)
                {
                    String[] v = new String[5];
                    v[0] = columnInfo.getName();
                    tableModel.addRow(v);
                }
                break;
            case FOREIGN_KEY:
                DefaultTableModel fkModel = (DefaultTableModel) tblColumnsFK.getModel();
                List<ColInfoDTO> refColumnList = constrInfo.getRefColumnList();
                int size = columnList.size();
                for (int i = 0; i < size; i++)
                {
                    String[] v = new String[3];
                    v[0] = columnList.get(i).getName();
                    v[1] = refColumnList.get(i).getName();
                    v[2] = constrInfo.getRefTable();
                    fkModel.addRow(v);
                }
                cbbReferencesTable.setSelectedItem(constrInfo.getRefTable());
                break;
            case CHECK:
                //nothing to do
                break;
            case EXCLUDE:
                DefaultTableModel excludeModel = (DefaultTableModel) tblColumnsExclude.getModel();
                for (ColInfoDTO columnInfo : columnList)
                {
                    String[] v = new String[5];
                    v[0] = columnInfo.getName();
                    v[1] = columnInfo.getOrder();
                    v[2] = columnInfo.getNullsWhen();
                    if (columnInfo.isDefaultOpClass())
                    {
                        v[3] = "";
                    } else
                    {
                        v[3] = columnInfo.getOperatorClass();
                    }
                    v[4] = columnInfo.getOperator();
                    excludeModel.addRow(v);
                }
                break;
        }
    }
    
    
    
    //format
    private void usingIndexChanged4PKandUK(ItemEvent e)
    {
        Object item = cbbUsingIndex.getSelectedItem();
        Boolean isEnable = (item==null || item.toString().isEmpty());
        cbbTablespace.setEnabled(isEnable);
        txfdFillFactor.setEnabled(isEnable);
        cbDeferrable.setEnabled(isEnable);
        cbDeferred.setEnabled(isEnable);
        cbbColumn.setEnabled(isEnable);
        tblColumns.setEnabled(isEnable);
    }
    private void valueKeyTypedAction(KeyEvent evt)
    {
        //those textfields could only be enter number.
        char keyCh = evt.getKeyChar();
        logger.info("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }
    }
    //for exclude columns
    private void changeOperatorClassWhenAccessMethodChanged()
    {
        Object am = cbbAccessMethod.getSelectedItem();
        if (am == null || am.toString().isEmpty()
                || am.toString().equals("btree"))
        {
            cbbOperatorClass.setEnabled(true);
        } else
        {
            //cbbOperatorClass.setModel(new DefaultComboBoxModel(new String[]{}));
            cbbOperatorClass.setSelectedIndex(-1);
            cbbOperatorClass.setEnabled(false);
        }            
    }
    private void changeOperatorWhenColumnChanged()
    {
        logger.debug("Enter");
        if(cbbColumnExclude.getSelectedItem()==null)
        {
            cbbOperatorExclude.setModel(new DefaultComboBoxModel(new String[]{}));
            return;
        }
        ColItemInfoDTO col = (ColItemInfoDTO) cbbColumnExclude.getSelectedItem();
        if (col.getTypeOid() == null || col.getTypeOid() == 0)
        {
             cbbOperatorExclude.setModel(new DefaultComboBoxModel(new String[]{}));
             return;
        }
        try
        {
            cbbOperatorExclude.setModel(new DefaultComboBoxModel(TreeController.getInstance().getOperatorOfType(helperInfo, col.getTypeOid())));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }        
    }
    
    private void btnAddChangeColumnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeColumnActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeColumnActionPerformed
        // TODO add your handling code here:       
        String columnName = cbbColumn.getSelectedItem().toString();
        if (columnName != null && !columnName.isEmpty())
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            int row = tableModel.getRowCount();
            boolean isExist = false;
            for (int i = 0; i < row; i++)
            {
                logger.info("row=" + row);
                String nameOld = tableModel.getValueAt(i, 0).toString();
                logger.info("nameOld=" + nameOld + nameOld.equals(columnName));
                if (nameOld.equals(columnName))
                {
                    isExist = true;
                    //tableModel.setValueAt(value, i, 1);
                }
                if (isExist)
                {
                    break;
                }
            }
            if (!isExist)
            {
                String[] v = new String[5];
                v[0] = columnName;
                tableModel.addRow(v);
            }
        }        
    }//GEN-LAST:event_btnAddChangeColumnActionPerformed

    private void btnRemoveColumnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveColumnActionPerformed
    {//GEN-HEADEREND:event_btnRemoveColumnActionPerformed
        // TODO add your handling code here:
        logger.info("remove selected row of variable table");
        int selectedRow = tblColumns.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveColumnActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.CONSTRAINT);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void tblColumnsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblColumnsMousePressed
    {//GEN-HEADEREND:event_tblColumnsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int row = tblColumns.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String columnName = tblColumns.getValueAt(row, 0).toString();
        cbbColumn.setSelectedItem(columnName); 
    }//GEN-LAST:event_tblColumnsMousePressed

    private void btnAddChangeColumnFKActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeColumnFKActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeColumnFKActionPerformed
        // TODO add your handling code here:       
        if (cbbLocalColumn.getSelectedItem() == null || cbbLocalColumn.getSelectedItem().toString().isEmpty())
        {
            return;
        }
        String columnName = cbbLocalColumn.getSelectedItem().toString();
        DefaultTableModel tableModel = (DefaultTableModel) tblColumnsFK.getModel();
        int row = tableModel.getRowCount();
        String referenceColumn = null;
        if (cbbReferencingColumn.getSelectedItem() != null)
        {
            referenceColumn = cbbReferencingColumn.getSelectedItem().toString();
        }
        String referenceTable = null;
        if (cbbReferencesTable.getSelectedItem() != null)
        {
            referenceTable = cbbReferencesTable.getSelectedItem().toString();
        }
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row=" + row);
            String nameOld = tableModel.getValueAt(i, 0).toString();
            logger.info("nameOld=" + nameOld + nameOld.equals(columnName));
            if (nameOld.equals(columnName))
            {
                isExist = true;
                tableModel.setValueAt(referenceColumn, i, 1);
                tableModel.setValueAt(referenceTable, i, 2);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] v = new String[3];
            v[0] = columnName;
            v[1] = referenceColumn;
            v[2] = referenceTable;
            tableModel.addRow(v);
        }        
    }//GEN-LAST:event_btnAddChangeColumnFKActionPerformed

    private void btnRemoveColumnFKActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveColumnFKActionPerformed
    {//GEN-HEADEREND:event_btnRemoveColumnFKActionPerformed
        // TODO add your handling code here:
        logger.info("remove selected row of FK constraint table");
        int selectedRow = tblColumnsFK.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblColumnsFK.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveColumnFKActionPerformed

    private void cbDeferrableFKActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbDeferrableFKActionPerformed
    {//GEN-HEADEREND:event_cbDeferrableFKActionPerformed
        // TODO add your handling code here:
        boolean enable = cbDeferrableFK.isSelected();
        logger.info("cbDeferrableFK:" + enable);
        cbDeferredFK.setEnabled(enable);
        if(!enable)
        {
            cbDeferredFK.setSelected(false);
        }
    }//GEN-LAST:event_cbDeferrableFKActionPerformed

    private void cbAutoFKIndexFKActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbAutoFKIndexFKActionPerformed
    {//GEN-HEADEREND:event_cbAutoFKIndexFKActionPerformed
        // TODO add your handling code here:
        boolean enable = cbAutoFKIndexFK.isSelected();
        logger.info("cbAutoFKIndexFK:"+enable);
        txfdlCoveringIndexFK.setEnabled(enable);
        if(!enable)
        {
            txfdlCoveringIndexFK.setText(null);
        }
    }//GEN-LAST:event_cbAutoFKIndexFKActionPerformed

    private void cbReadOnlyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbReadOnlyActionPerformed
    {//GEN-HEADEREND:event_cbReadOnlyActionPerformed
        // TODO add your handling code here:
        logger.info("ReadOnly select=" + cbReadOnly.isSelected());
//        tpDefinitionSQL.setEditable(!cbReadOnly.isSelected());
    }//GEN-LAST:event_cbReadOnlyActionPerformed

    private void tblColumnsExcludeMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblColumnsExcludeMousePressed
    {//GEN-HEADEREND:event_tblColumnsExcludeMousePressed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int row = tblColumnsExclude.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String columnName = tblColumnsExclude.getValueAt(row, 0).toString();
        cbbColumnExclude.setSelectedItem(columnName);         
    }//GEN-LAST:event_tblColumnsExcludeMousePressed

    private void btnRemoveColumnExcludeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveColumnExcludeActionPerformed
    {//GEN-HEADEREND:event_btnRemoveColumnExcludeActionPerformed
        // TODO add your handling code here:
        logger.info("remove selected row of FK constraint table");
        int selectedRow = tblColumnsExclude.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblColumnsExclude.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveColumnExcludeActionPerformed

    private void cbbAccessMethodItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbAccessMethodItemStateChanged
    {//GEN-HEADEREND:event_cbbAccessMethodItemStateChanged
        // TODO add your handling code here:
        logger.info("AccessMethod=" + cbbAccessMethod.getSelectedItem().toString());
        switch (cbbAccessMethod.getSelectedItem().toString())
        {
            case "":
            case "btree":
                cbbOperatorClassExclude.setEnabled(true);
                cbDescExclude.setEnabled(true);
                rbFirstExclude.setEnabled(true);
                rbLastExclude.setEnabled(true);
                cbbOperatorExclude.setEnabled(true);
                break;
            case "gist":
            case "hash":
            case "spgist":
                cbbOperatorClassExclude.setEnabled(false);
                cbDescExclude.setEnabled(false);
                rbFirstExclude.setEnabled(false);
                rbLastExclude.setEnabled(false);
                cbbOperatorExclude.setEnabled(false);
                break;
            default:
                logger.warn("default is:" + cbbAccessMethod.getSelectedItem().toString());
                break;
        }
    }//GEN-LAST:event_cbbAccessMethodItemStateChanged

    private void cbbReferencesTableItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbReferencesTableItemStateChanged
    {//GEN-HEADEREND:event_cbbReferencesTableItemStateChanged
        // TODO add your handling code here:
        ObjItemInfoDTO referencesTable  = (ObjItemInfoDTO) cbbReferencesTable.getSelectedItem();
        if(referencesTable==null)
        {
            logger.warn("Never choose an references table, do nothing and return.");
            return;
        }
        logger.info("referencesTable=" + referencesTable.toString() + ", oid=" + referencesTable.getOid());
        try
        {
            String[][] referenceColumnInfo = (String[][]) TreeController.getInstance().getInheritTableColumns(helperInfo, referencesTable.getOid());//reference
            String[] referenceColumnName = new String[referenceColumnInfo.length];
            for (int i = 0; i < referenceColumnInfo.length; i++)
            {
                referenceColumnName[i] = referenceColumnInfo[i][0];
                logger.info("reference column: " + referenceColumnName[i]);
            }
            cbbReferencingColumn.setModel(new DefaultComboBoxModel(referenceColumnName));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_cbbReferencesTableItemStateChanged

    private void btnAddChangeColumnExcludeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeColumnExcludeActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeColumnExcludeActionPerformed
        // TODO add your handling code here:
         String columnName = cbbColumnExclude.getSelectedItem().toString();
        if (columnName != null && !columnName.isEmpty())
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumnsExclude.getModel();
            int row = tableModel.getRowCount();
            String order = "ASC";
            if(cbDescExclude.isSelected())
            {
                order = "DESC";
            }
            String nullsOrder = "FIRST";
            if(rbLastExclude.isSelected())
            {
                 nullsOrder = "LAST";
            }            
            boolean isExist = false;
            for (int i = 0; i < row; i++)
            {
                logger.info("row=" + row);
                String nameOld = tableModel.getValueAt(i, 0).toString();
                logger.info("nameOld=" + nameOld + nameOld.equals(columnName));
                if (nameOld.equals(columnName))
                {
                    isExist = true;
                    tableModel.setValueAt(order, i, 1);
                    tableModel.setValueAt(nullsOrder, i, 2);
                    tableModel.setValueAt(cbbOperatorClassExclude.getSelectedItem().toString(), i, 3);
                    if (cbbOperatorExclude.getSelectedItem() == null)
                    {
                        tableModel.setValueAt("", i, 4);
                    } else
                    {
                        tableModel.setValueAt(cbbOperatorExclude.getSelectedItem().toString(), i, 4);
                    }
                }
                if (isExist)
                {
                    break;
                }
            }
            if (!isExist)
            {
                String[] v = new String[5];
                v[0] = columnName;
                v[1] = order;
                v[2] = nullsOrder;
                v[3] = cbbOperatorClassExclude.getSelectedItem().toString();
                if (cbbOperatorExclude.getSelectedItem() == null)
                {
                    v[4] = "";
                } else
                {
                    v[4] = cbbOperatorExclude.getSelectedItem().toString();
                }
                tableModel.addRow(v);
            }
        }
    }//GEN-LAST:event_btnAddChangeColumnExcludeActionPerformed

    //add
    private void setBtnOkEnable4Add()
    {
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableAction4Add();
            }
        });
        switch (constrType)
        {
            case PRIMARY_KEY:
            case UNIQUE:
                tblColumns.getModel().addTableModelListener(new TableModelListener()
                {
                    @Override
                    public void tableChanged(TableModelEvent evt)
                    {
                        btnOkEnableAction4Add();
                    }
                });
                cbbUsingIndex.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e)
                    {
                        usingIndexChanged4PKandUK(e);
                        btnOkEnableAction4Add();
                    }
                });
                break;
            case EXCLUDE:
                tblColumnsExclude.getModel().addTableModelListener(new TableModelListener()
                {
                    @Override
                    public void tableChanged(TableModelEvent evt)
                    {
                        btnOkEnableAction4Add();
                    }
                });
                break;
            case FOREIGN_KEY:
                tblColumnsFK.getModel().addTableModelListener(new TableModelListener()
                {
                    @Override
                    public void tableChanged(TableModelEvent evt)
                    {
                        btnOkEnableAction4Add();
                    }
                });
                break;
            case CHECK:
                tarrCheck.addKeyListener(new KeyAdapter()
                {
                    @Override
                    public void keyReleased(KeyEvent evt)
                    {
                        btnOkEnableAction4Add();
                    }
                });
                break;
        }
    }
    private void btnOkEnableAction4Add()
    {
        String cName = txfdName.getText();
        if ((constrType == TreeEnum.TreeNode.PRIMARY_KEY || constrType == TreeEnum.TreeNode.UNIQUE)
                && cbbUsingIndex.getSelectedItem() != null && !cbbUsingIndex.getSelectedItem().toString().isEmpty())
        {
            btnOK.setEnabled(cName != null && !cName.isEmpty());
        } else
        {
            //for pk and uk
            int row = tblColumns.getRowCount();
            if (constrType.equals(TreeEnum.TreeNode.FOREIGN_KEY))
            {
                row = tblColumnsFK.getRowCount();
            } else if (constrType.equals(TreeEnum.TreeNode.EXCLUDE))
            {
                row = tblColumnsExclude.getRowCount();
            } else if (constrType.equals(TreeEnum.TreeNode.CHECK))
            {
                row = tarrCheck.getText().length();
            }
            logger.info("count = " + row);
            if (row <= 0 || cName == null || cName.isEmpty())
            {
                btnRemoveColumn.setEnabled(false);
                btnOK.setEnabled(false);
            } else
            {
                btnRemoveColumn.setEnabled(true);
                btnOK.setEnabled(true);
            }
        }
    }
    private void jtpStateChanged4Add(ChangeEvent evt)
    {
        if (jtp.getSelectedIndex() < 0)
        {
            return;
        }
        String name = jtp.getTitleAt(jtp.getSelectedIndex());
        logger.info("SelectTabName:" + name);
        SyntaxController sc = SyntaxController.getInstance();
        if (name != null && name.equals("SQL"))
        {
            tpDefinitionSQL.setText("");//clear
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
            } else
            {                
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getConstrInfo()));
            }
        }
    }
    private void btnOKActionPerformed4Add(ActionEvent evt)                                      
    {                                          
        logger.info(evt.getActionCommand());
        if (!inTabCreate)
        {
            try
            {
                TreeController.getInstance().createObj(helperInfo, this.getConstrInfo());
                isSuccess = true;
                this.dispose();
            } catch (Exception ex)
            {
                isSuccess = false;
                logger.error("Error " + ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            }
        } else
        {
            isSuccess = true;
            this.dispose();
        } 
    }  
    //change
    private void setBtnOkEnable4Change()
    {
        KeyAdapter btnOkKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnable4Change();
            }
        };
        txfdName.addKeyListener(btnOkKeyAdapter);
        ttarComment.addKeyListener(btnOkKeyAdapter);
        switch (constrType)
        {
            case PRIMARY_KEY:
            case UNIQUE:
                cbbTablespace.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e)
                    {
                        btnOkEnable4Change();
                    }
                });
                cbbUsingIndex.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e)
                    {
                         btnOkEnable4Change();
                    }
                });
                txfdFillFactor.addKeyListener(btnOkKeyAdapter);
                break;
            case EXCLUDE:
                cbbTablespace.addItemListener(new ItemListener()
                {
                    @Override
                    public void itemStateChanged(ItemEvent e)
                    {
                        btnOkEnable4Change();
                    }
                });
                txfdFillFactor.addKeyListener(btnOkKeyAdapter);
                ttarConstraint.addKeyListener(btnOkKeyAdapter);
                break;
            case FOREIGN_KEY:
                cbNotValidateFK.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        btnOkEnable4Change();
                    }
                });
                break;
            case CHECK:
                cbDoNotValidateCheck.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        btnOkEnable4Change();
                    }
                });
                break;
        }
    }
    private void btnOkEnable4Change()
    {
        if(txfdName.getText().isEmpty())
        {
            btnOK.setEnabled(false);
        } else if (!txfdName.getText().equals(constrInfo.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        }else if(!ttarComment.getText().equals(constrInfo.getComment())
                &&!(ttarComment.getText().isEmpty() && constrInfo.getComment()==null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else
        {
            switch (constrType)
            {
                case PRIMARY_KEY:
                case UNIQUE:
                    if (cbbTablespace.getSelectedItem() != null && !cbbTablespace.getSelectedItem().equals(constrInfo.getTablespace()))
                    {
                        logger.info("tablespace changed");
                        btnOK.setEnabled(true);
                    } else if (cbbUsingIndex.getSelectedItem() != null && !cbbUsingIndex.getSelectedItem().equals(constrInfo.getTablespace()))
                    {
                        logger.info("usingindex changed");
                        btnOK.setEnabled(true);
                    } else if (!txfdFillFactor.getText().trim().isEmpty() && !txfdFillFactor.getText().trim().equals(constrInfo.getFillFactor()))
                    {
                        logger.info("fillfactor changed");
                        btnOK.setEnabled(true);
                    } else
                    {
                        btnOK.setEnabled(false);
                    }
                    break;
                 case EXCLUDE:
                    if (cbbTablespace.getSelectedItem() != null && !cbbTablespace.getSelectedItem().equals(constrInfo.getTablespace()))
                    {
                        logger.info("tablespace changed");
                        btnOK.setEnabled(true);
                    }  else if (!txfdFillFactor.getText().trim().isEmpty() && !txfdFillFactor.getText().trim().equals(constrInfo.getFillFactor()))
                    {
                        logger.info("fillfactor changed");
                        btnOK.setEnabled(true);
                    } else if (!ttarConstraint.getText().trim().isEmpty()
                            && ttarConstraint.getText().trim().equals(constrInfo.getConstraint()))
                    {
                        logger.info("constraint changed");
                        btnOK.setEnabled(true);
                    } else
                    {
                        btnOK.setEnabled(false);
                    }                    
                    break;
                case FOREIGN_KEY:
                    if (cbNotValidateFK.isSelected() == constrInfo.isValidate())
                    {
                        logger.info("FK validate changed");
                        btnOK.setEnabled(true);
                    } else
                    {
                        btnOK.setEnabled(false);
                    }
                    break;
                case CHECK:
                    if (cbDoNotValidateCheck.isSelected() == constrInfo.isValidate())
                    {
                        logger.info("CHECK validate changed");
                        btnOK.setEnabled(true);
                    } else
                    {
                        btnOK.setEnabled(false);
                    }
                    break;
            }
        }
    }
    private void jtpStateChanged4Change(ChangeEvent evt)
    {
        if (jtp.getSelectedIndex() < 0)
        {
            return;
        }
        String name = jtp.getTitleAt(jtp.getSelectedIndex());
        logger.info("SelectTabName:" + name);
        SyntaxController sc = SyntaxController.getInstance();
        if (name != null && name.equals("SQL"))
        {
            tpDefinitionSQL.setText("");//clear
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(),tc.getConstraintChangeSQL(this.constrInfo, this.getConstrInfo()));
            }
        }
    }
    private void btnOKActionPerformed4Change(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (!inTabCreate)
        {
            try
            {
                TreeController tc = TreeController.getInstance();
                String sql = tc.getConstraintChangeSQL(this.constrInfo, this.getConstrInfo());                
                tc.executeSql(helperInfo, helperInfo.getDbName(), sql);
                isSuccess = true;
                this.dispose();
            } catch (Exception ex)
            {
                isSuccess = false;
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            }
        } else
        {
            isSuccess = true;
            this.dispose();
        } 
    }

    
    public ConstraintInfoDTO getConstrInfo()
    {
        ConstraintInfoDTO newconstr = new ConstraintInfoDTO();
        if (constrInfo != null)
        {
            newconstr.setOid(constrInfo.getOid());
            newconstr.setEnable(constrInfo.isEnable());
        }
        newconstr.setType(constrType);
        newconstr.setHelperInfo(helperInfo);
        newconstr.setName(txfdName.getText());
        newconstr.setComment(ttarComment.getText());
        List<ColInfoDTO> columnList = new ArrayList();
        //set definetion before constraint add or creation
        switch (constrType)
        {
            case PRIMARY_KEY:
            case UNIQUE:
                if (cbbUsingIndex.getSelectedItem() != null && !cbbUsingIndex.getSelectedItem().toString().isEmpty())
                {
                    newconstr.setUsingIndex(cbbUsingIndex.getSelectedItem().toString());
                } else
                {
                    newconstr.setTablespace(cbbTablespace.getSelectedItem().toString());
                    if (cbbAccessMethod.getSelectedItem() != null)
                    {
                        newconstr.setAccessMethod(cbbAccessMethod.getSelectedItem().toString());
                    }
                    newconstr.setFillFactor(txfdFillFactor.getText());
                    newconstr.setDeferrable(cbDeferrable.isSelected());
                    newconstr.setDeferred(cbDeferred.isSelected());
                    //for column list
                    DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
                    for (int row = 0; row < columnModel.getRowCount(); row++)
                    {
                        ColInfoDTO c = new ColInfoDTO();
                        c.setName(columnModel.getValueAt(row, 0).toString());
                        columnList.add(c);
                    }
                    newconstr.setColInfoList(columnList);
                }
                break;
            case FOREIGN_KEY:          
                newconstr.setDeferrable(cbDeferrableFK.isSelected());
                newconstr.setDeferred(cbDeferredFK.isSelected());
                if(cbMatchFullFK.isSelected())
                {
                    newconstr.setMatchType("FULL");
                }                
                newconstr.setValidate(!cbNotValidateFK.isSelected());
                newconstr.setCoveringIndex(txfdlCoveringIndexFK.getText());
                //a FK can only reference from one table!
                newconstr.setRefTable(tblColumnsFK.getValueAt(0, 2).toString());                
                List<ColInfoDTO> refColumnList = new ArrayList();
                DefaultTableModel columnFKModel = (DefaultTableModel) tblColumnsFK.getModel();
                for (int row = 0; row < columnFKModel.getRowCount(); row++)
                {
                    ColInfoDTO c = new ColInfoDTO();
                    c.setName(columnFKModel.getValueAt(row, 0).toString());
                    columnList.add(c);

                    ColInfoDTO rc = new ColInfoDTO();
                    rc.setName(columnFKModel.getValueAt(row, 1).toString());
                    refColumnList.add(rc);
                }
                newconstr.setColInfoList(columnList);
                newconstr.setRefColList(refColumnList);

                Enumeration<AbstractButton> updateBtns = rbGroupUpdate.getElements();
                while (updateBtns.hasMoreElements())
                {
                    AbstractButton btn = updateBtns.nextElement();
                    if (btn.isSelected())
                    {
                        logger.info("Update action:" + btn.getText());
                        newconstr.setUpdateActions(btn.getText());
                        break;
                    }
                }
                Enumeration<AbstractButton> deleteBtns = rbGroupDelete.getElements();
                while (deleteBtns.hasMoreElements())
                {
                    AbstractButton btn = deleteBtns.nextElement();
                    if (btn.isSelected())
                    {
                        logger.info("Delete Action:" + btn.getText());
                        newconstr.setDeleteAction(btn.getText());
                        break;
                    }
                }
                break;
            case EXCLUDE:          
                newconstr.setTablespace(cbbTablespace.getSelectedItem().toString());
                newconstr.setAccessMethod(cbbAccessMethod.getSelectedItem().toString());
                newconstr.setFillFactor(txfdFillFactor.getText().trim());
                newconstr.setDeferrable(cbDeferrable.isSelected());
                newconstr.setDeferred(cbDeferred.isSelected());
                newconstr.setConstraint(ttarConstraint.getText());
                //for column list
                DefaultTableModel excludeModel = (DefaultTableModel) tblColumnsExclude.getModel();
                for (int row = 0; row < excludeModel.getRowCount(); row++)
                {
                    ColInfoDTO c = new ColInfoDTO();
                    c.setName(excludeModel.getValueAt(row, 0).toString());
                    c.setOrder(excludeModel.getValueAt(row, 1).toString());
                    c.setNullsWhen(excludeModel.getValueAt(row, 2).toString());
                    c.setOperatorClass(excludeModel.getValueAt(row, 3).toString());
                    c.setOperator(excludeModel.getValueAt(row, 4).toString());
                    columnList.add(c);
                }
                newconstr.setColInfoList(columnList);
                break; 
            case CHECK:
                newconstr.setCheckConditonQuoted("("+tarrCheck.getText()+")");
                newconstr.setNoInherit(cbNoInheritCheck.isSelected());
                newconstr.setValidate(!cbDoNotValidateCheck.isSelected());
        }
        return newconstr;
    }
    public boolean isSuccess()
    {
        return isSuccess;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangeColumn;
    private javax.swing.JButton btnAddChangeColumnExclude;
    private javax.swing.JButton btnAddChangeColumnFK;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemoveColumn;
    private javax.swing.JButton btnRemoveColumnExclude;
    private javax.swing.JButton btnRemoveColumnFK;
    private javax.swing.JCheckBox cbAutoFKIndexFK;
    private javax.swing.JCheckBox cbDeferrable;
    private javax.swing.JCheckBox cbDeferrableFK;
    private javax.swing.JCheckBox cbDeferred;
    private javax.swing.JCheckBox cbDeferredFK;
    private javax.swing.JCheckBox cbDesc;
    private javax.swing.JCheckBox cbDescExclude;
    private javax.swing.JCheckBox cbDoNotValidateCheck;
    private javax.swing.JCheckBox cbMatchFullFK;
    private javax.swing.JCheckBox cbNoInheritCheck;
    private javax.swing.JCheckBox cbNotValidateFK;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JComboBox cbbAccessMethod;
    private javax.swing.JComboBox cbbColumn;
    private javax.swing.JComboBox cbbColumnExclude;
    private javax.swing.JComboBox cbbLocalColumn;
    private javax.swing.JComboBox cbbOperator;
    private javax.swing.JComboBox cbbOperatorClass;
    private javax.swing.JComboBox cbbOperatorClassExclude;
    private javax.swing.JComboBox cbbOperatorExclude;
    private javax.swing.JComboBox cbbReferencesTable;
    private javax.swing.JComboBox cbbReferencingColumn;
    private javax.swing.JComboBox cbbTablespace;
    private javax.swing.JComboBox cbbUseSlony;
    private javax.swing.JComboBox cbbUsingIndex;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblAccessMethod;
    private javax.swing.JLabel lblAutoFKIndexFK;
    private javax.swing.JLabel lblCheck;
    private javax.swing.JLabel lblColumn;
    private javax.swing.JLabel lblColumn1;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblConstraint;
    private javax.swing.JLabel lblCoveringIndexFK;
    private javax.swing.JLabel lblDeferrable;
    private javax.swing.JLabel lblDeferrableFK;
    private javax.swing.JLabel lblDeferred;
    private javax.swing.JLabel lblDeferredFK;
    private javax.swing.JLabel lblDesc;
    private javax.swing.JLabel lblDesc1;
    private javax.swing.JLabel lblDoNotValidateCheck;
    private javax.swing.JLabel lblDoNotValidateFK;
    private javax.swing.JLabel lblFillFactor;
    private javax.swing.JLabel lblIndex;
    private javax.swing.JLabel lblLocalColumn;
    private javax.swing.JLabel lblMatchFull1;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNoInherit;
    private javax.swing.JLabel lblNulls;
    private javax.swing.JLabel lblNulls1;
    private javax.swing.JLabel lblOperator;
    private javax.swing.JLabel lblOperator1;
    private javax.swing.JLabel lblOperatorClass;
    private javax.swing.JLabel lblOperatorClass1;
    private javax.swing.JLabel lblReferences;
    private javax.swing.JLabel lblReferencing;
    private javax.swing.JLabel lblTablespace;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JPanel pnlAction;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlColumns;
    private javax.swing.JPanel pnlColumnsExclude;
    private javax.swing.JPanel pnlColumnsFK;
    private javax.swing.JPanel pnlDefinitionCheck;
    private javax.swing.JPanel pnlDefinitionFK;
    private javax.swing.JPanel pnlDefinitionPK;
    private javax.swing.JPanel pnlDelete;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlUpdate;
    private javax.swing.JRadioButton rbCascade;
    private javax.swing.JRadioButton rbCascadeDelete;
    private javax.swing.JRadioButton rbFirst;
    private javax.swing.JRadioButton rbFirstExclude;
    private javax.swing.ButtonGroup rbGroupDelete;
    private javax.swing.ButtonGroup rbGroupNulls;
    private javax.swing.ButtonGroup rbGroupUpdate;
    private javax.swing.JRadioButton rbLast;
    private javax.swing.JRadioButton rbLastExclude;
    private javax.swing.JRadioButton rbNoAction;
    private javax.swing.JRadioButton rbNoActionDelete;
    private javax.swing.JRadioButton rbRestrict;
    private javax.swing.JRadioButton rbRestrictDelete;
    private javax.swing.JRadioButton rbSetDefault;
    private javax.swing.JRadioButton rbSetDefaultDelete;
    private javax.swing.JRadioButton rbSetNull;
    private javax.swing.JRadioButton rbSetNullDelete;
    private javax.swing.JScrollPane spColumns;
    private javax.swing.JScrollPane spColumns1;
    private javax.swing.JScrollPane spColumns2;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTextArea tarrCheck;
    private javax.swing.JTable tblColumns;
    private javax.swing.JTable tblColumnsExclude;
    private javax.swing.JTable tblColumnsFK;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarComment;
    private javax.swing.JTextArea ttarConstraint;
    private javax.swing.JTextField txfdFillFactor;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdlCoveringIndexFK;
    // End of variables declaration//GEN-END:variables

}
