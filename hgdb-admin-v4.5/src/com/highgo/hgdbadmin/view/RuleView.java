/* ------------------------------------------------ 
* 
* File: SchemaView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\SchemaView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.RuleInfoDTO;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 *
 *
 */
public class RuleView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private HelperInfoDTO helperInfo;
    private RuleInfoDTO ruleInfo;
    private boolean isSuccess;

    //add
     public RuleView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.ruleInfo = null;
        initComponents();
        isSuccess = false;        
        this.setTitle(constBundle.getString("addRule"));
        //define action
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAdd();
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformedForAdd(e);
            }
        });
    }
    
    //property
    public RuleView(JFrame parent, boolean modal, RuleInfoDTO ruleInfo)
    {
        super(parent, modal);
        this.ruleInfo = ruleInfo;
        this.helperInfo = ruleInfo.getHelperInfo();
        initComponents();
        isSuccess = false;
        this.setTitle(constBundle.getString("rules") + " " + ruleInfo.getName());  
        cbReadOnly.setEnabled(true);
        this.setProperty();
        //define action    
        KeyAdapter btnOkEnableAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnableAdapter);
        txarComment.addKeyListener(btnOkEnableAdapter);
        ttarCondition.addKeyListener(btnOkEnableAdapter);
        ppStatements.addKeyListener(btnOkEnableAdapter);
        ActionListener btnOkEnableAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnableForChange();
            }
        };
        rbSelect.addActionListener(btnOkEnableAction);
        rbInsert.addActionListener(btnOkEnableAction);
        rbUpdate.addActionListener(btnOkEnableAction);
        rbDelete.addActionListener(btnOkEnableAction);
        cbDoInstead.addActionListener(btnOkEnableAction);
        
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformedForChange(e);
            }
        });
    }

    private void setProperty()
    {
        txfdName.setText(ruleInfo.getName());
        txfdName.setEnabled(false);
        txfdOID.setText(ruleInfo.getOid().toString());
        txarComment.setText(ruleInfo.getComment());
        switch (ruleInfo.getEvent())
        {
            case "SELECT":
                rbSelect.setSelected(true);
                break;
            case "INSERT":
                rbInsert.setSelected(true);
                break;
            case "UPDATE":
                rbUpdate.setSelected(true);
                break;
            case "DELETE":
                rbDelete.setSelected(true);
                break;
        }
        cbDoInstead.setSelected(ruleInfo.isDoInstead());
        ttarCondition.setText(ruleInfo.getWhereCondition());
        ppStatements.setText(ruleInfo.getDoStatement());
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        rbGroupEvents = new javax.swing.ButtonGroup();
        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        cbbUseSlony = new javax.swing.JComboBox();
        lblUseSlony = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        lblOID = new javax.swing.JLabel();
        txfdOID = new javax.swing.JTextField();
        pnlDefinition = new javax.swing.JPanel();
        lblEvent = new javax.swing.JLabel();
        lblCondition = new javax.swing.JLabel();
        lblDoInstead = new javax.swing.JLabel();
        cbDoInstead = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        ttarCondition = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        rbSelect = new javax.swing.JRadioButton();
        rbInsert = new javax.swing.JRadioButton();
        rbUpdate = new javax.swing.JRadioButton();
        rbDelete = new javax.swing.JRadioButton();
        pnlStatements = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ppStatements = new javax.swing.JTextPane();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addRule"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/rule.png")));
setMinimumSize(new java.awt.Dimension(500, 500));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setBackground(new java.awt.Color(255, 255, 255));
jtp.setMinimumSize(new java.awt.Dimension(500, 440));
jtp.setPreferredSize(new java.awt.Dimension(500, 440));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(500, 420));
pnlProperties.setPreferredSize(new java.awt.Dimension(500, 420));

lblName.setText(constBundle.getString("name"));

lblComment.setText(constBundle.getString("comment"));

txfdName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));

cbbUseSlony.setEditable(true);
cbbUseSlony.setEnabled(false);

lblUseSlony.setText(constBundle.getString("useSlony"));

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane4.setViewportView(txarComment);

lblOID.setText("OID");

txfdOID.setEditable(false);
txfdOID.setEnabled(false);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblComment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblOID, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txfdOID, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
                    .addComponent(txfdName)))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbUseSlony, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblName)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblOID))))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblUseSlony)
                .addComponent(cbbUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("property"), pnlProperties);

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(500, 420));

    lblEvent.setText(constBundle.getString("event"));

    lblCondition.setText(constBundle.getString("condition"));

    lblDoInstead.setText(constBundle.getString("doInstead"));

    cbDoInstead.setBackground(new java.awt.Color(255, 255, 255));

    ttarCondition.setColumns(20);
    ttarCondition.setRows(5);
    jScrollPane2.setViewportView(ttarCondition);

    jPanel1.setBackground(new java.awt.Color(255, 255, 255));
    jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));

    rbSelect.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupEvents.add(rbSelect);
    rbSelect.setSelected(true);
    rbSelect.setText("SELECT");

    rbInsert.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupEvents.add(rbInsert);
    rbInsert.setText("INSERT");

    rbUpdate.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupEvents.add(rbUpdate);
    rbUpdate.setText("UPDATE");

    rbDelete.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupEvents.add(rbDelete);
    rbDelete.setText("DELETE");

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(rbSelect)
                .addComponent(rbInsert)
                .addComponent(rbUpdate)
                .addComponent(rbDelete))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(rbSelect)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(rbInsert)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(rbUpdate)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(rbDelete)
            .addContainerGap())
    );

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(lblEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblDoInstead, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(cbDoInstead, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 370, Short.MAX_VALUE))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblCondition, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)))))
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblEvent)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblDoInstead, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbDoInstead, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblCondition, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 219, Short.MAX_VALUE))
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlStatements.setBackground(new java.awt.Color(255, 255, 255));
    pnlStatements.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlStatements.setOpaque(false);
    pnlStatements.setPreferredSize(new java.awt.Dimension(500, 420));

    jScrollPane1.setViewportView(ppStatements);

    javax.swing.GroupLayout pnlStatementsLayout = new javax.swing.GroupLayout(pnlStatements);
    pnlStatements.setLayout(pnlStatementsLayout);
    pnlStatementsLayout.setHorizontalGroup(
        pnlStatementsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlStatementsLayout.createSequentialGroup()
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 485, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlStatementsLayout.setVerticalGroup(
        pnlStatementsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlStatementsLayout.createSequentialGroup()
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 401, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("statements"), pnlStatements);
    pnlStatements.getAccessibleContext().setAccessibleName("");

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlSQL.setPreferredSize(new java.awt.Dimension(500, 420));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);
    cbReadOnly.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbReadOnlyActionPerformed(evt);
        }
    });

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 475, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 245, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.RULE);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void cbReadOnlyActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbReadOnlyActionPerformed
    {//GEN-HEADEREND:event_cbReadOnlyActionPerformed
        // TODO add your handling code here:
        logger.info("cbReadOnly = " + cbReadOnly.isSelected());
        tpDefinitionSQL.setEditable(!cbReadOnly.isSelected());
        jtp.setEnabledAt(0, cbReadOnly.isSelected());
        jtp.setEnabledAt(1, cbReadOnly.isSelected());
        jtp.setEnabledAt(2, cbReadOnly.isSelected());
        if(!cbReadOnly.isSelected())
        {
            btnOK.setEnabled(true);
        }
    }//GEN-LAST:event_cbReadOnlyActionPerformed
    private String getSelectedEvent()
    {
        if (rbSelect.isSelected())
        {
            return "SELECT";
        } else if (rbInsert.isSelected())
        {
            return "INSERT";
        } else if (rbUpdate.isSelected())
        {
            return "UPDATE";
        } else if (rbDelete.isSelected())
        {
            return "DELETE";
        }
        return "";
    }
    
    //for add
    private void btnOkEnableForAdd()
    {
        String ruleName = txfdName.getText();
        if (ruleName == null || ruleName.isEmpty())
        {
            btnOK.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
        }
    }
    private void jtpForAddStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTab:" + index);
        SyntaxController sc = SyntaxController.getInstance();
        if (index == 3)
        {
            tpDefinitionSQL.setText("");//clear
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getRuleInfo()));
            }
        }
    }
    private void btnOKActionPerformedForAdd(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.createObj(helperInfo, this.getRuleInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        } 
    }    
    
    //for change
    private void btnOkEnableForChange()
    {
        if (txfdName.getText().isEmpty())
        {
            btnOK.setEnabled(false);
        } else if (!this.getSelectedEvent().equals(ruleInfo.getEvent()))
        {
            logger.info("event change");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(ruleInfo.getComment())
                && !(txarComment.getText().isEmpty() && ruleInfo.getComment() == null))
        {
            logger.info("commnet change");
            btnOK.setEnabled(true);
        } else if (cbDoInstead.isSelected() != ruleInfo.isDoInstead())
        {
            logger.info("doInstead change");
            btnOK.setEnabled(true);
        } else if (!ttarCondition.getText().equals(ruleInfo.getWhereCondition())
                &&!(ttarCondition.getText().isEmpty() && ruleInfo.getWhereCondition()==null))
        {
            logger.info("condition change");
            btnOK.setEnabled(true);
        } else if (!ppStatements.getText().equals(ruleInfo.getDoStatement())
                && !(ppStatements.getText().isEmpty() && ruleInfo.getDoStatement()==null))
        {
            logger.info("statement change");
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTab:" + index);
        SyntaxController sc = SyntaxController.getInstance();
        if (index == 3 && cbReadOnly.isSelected())
        {
            tpDefinitionSQL.setText("");//clear
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getRuleChangeSQL(ruleInfo, this.getRuleInfo()));
            }
        }
    }
    private void btnOKActionPerformedForChange(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        try
        {
            TreeController tc = TreeController.getInstance();
            if (cbReadOnly.isSelected())
            {
                tc.executeSql(helperInfo, helperInfo.getDbName(), tc.getRuleChangeSQL(ruleInfo, this.getRuleInfo()));
            } else
            {
                tc.executeSql(helperInfo, helperInfo.getDbName(), tpDefinitionSQL.getText());
            }
            isSuccess = true;
            this.dispose();
        } catch (ClassNotFoundException | SQLException ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    
    
    public RuleInfoDTO getRuleInfo()
    {
        RuleInfoDTO newRule = new RuleInfoDTO();
        if (ruleInfo != null)
        {
            newRule.setOid(ruleInfo.getOid());
        }
        newRule.setHelperInfo(helperInfo);
        switch (helperInfo.getType())
        {
            case TABLE:
                newRule.setParent("TABLE");
                break;
            case VIEW:
                newRule.setParent("VIEW");
                break;
        }
        newRule.setName(txfdName.getText());
        newRule.setComment(txarComment.getText());
        newRule.setEvent(this.getSelectedEvent());        
        newRule.setDoInstead(cbDoInstead.isSelected());
        newRule.setWhereCondition(ttarCondition.getText());
        newRule.setDoStatement(ppStatements.getText());
        return newRule;
    }

    public boolean isSuccess()
    {
        return isSuccess;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JCheckBox cbDoInstead;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JComboBox cbbUseSlony;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblCondition;
    private javax.swing.JLabel lblDoInstead;
    private javax.swing.JLabel lblEvent;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlStatements;
    private javax.swing.JTextPane ppStatements;
    private javax.swing.JRadioButton rbDelete;
    private javax.swing.ButtonGroup rbGroupEvents;
    private javax.swing.JRadioButton rbInsert;
    private javax.swing.JRadioButton rbSelect;
    private javax.swing.JRadioButton rbUpdate;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarCondition;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    // End of variables declaration//GEN-END:variables
}
