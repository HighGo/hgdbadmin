/* ------------------------------------------------ 
* 
* File: TreeView.java
*
* Abstract: 
* 		主窗口左侧树结构.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\TreeView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.BranchDTO;
import com.highgo.hgdbadmin.model.CatalogInfoDTO;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.EventTriggerInfoDTO;
import com.highgo.hgdbadmin.model.FunObjInfoDTO;
import com.highgo.hgdbadmin.model.ObjGroupDTO;
import com.highgo.hgdbadmin.model.ObjInfoDTO;
import com.highgo.hgdbadmin.model.SchemaInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerBranchInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.util.DropConfirmDialog;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.view.dialog.PwdInputDialog;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.event.SimpleEventBus;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.util.DBUtil;
import com.highgo.hgdbadmin.util.MessageUtil;

public class TreeView extends JTree
{
    private final Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private final ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private final ResourceBundle msgBundle = ResourceBundle.getBundle("messages");
    
    private SimpleEventBus eventBus;
    private JFrame parent;
    private DefaultMutableTreeNode rootNode;   
    protected DefaultMutableTreeNode serverGroupNode;
    
    public TreeView(SimpleEventBus eventBus, JFrame parent)
    {
        this.eventBus = eventBus;
        this.parent = parent;
        
        //initialize tree
        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);        
        rootNode = new DefaultMutableTreeNode(constBundle.getString("serverGroups"));
        this.setModel(new DefaultTreeModel(rootNode));
        //this.setExpandsSelectedPaths(true);
        this.setSelectionRow(0);         
        this.addRootBranch();  
        
        //custom node icon
        DefaultTreeCellRenderer render = new TreeCellRenderer();
        this.setCellRenderer(render);

        //right-click items
//        this.initTreeRightClickItems();

        //register mouse listener
        this.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent evt)
            {
                nodePressAction(evt);
            }
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                nodeClickedAction(evt);
            }
        });
      
        //regitser action listener
//        this.registerObjAction();
    } 
    //init tree
    private void addRootBranch()
    {
        logger.info("Init tree server branch.");       
        DefaultMutableTreeNode serverInfoNode;
        List<XMLServerBranchInfoDTO> serverBranchInfoList = new ArrayList();
        try
        {
            TreeController tc = TreeController.getInstance();
            serverBranchInfoList = tc.getServerFromXML();
        } catch (Exception ex)
        {
            MessageUtil.showErrorMsg(parent, ex);
//            JOptionPane.showMessageDialog(this, "Failed to get server from config file.\n" + ex.getMessage(),
//                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        logger.info("ServerGroup from XML Count = " + serverBranchInfoList.size());
        for (XMLServerBranchInfoDTO servGroupInfo : serverBranchInfoList)
        {
            ObjGroupDTO groupInfo = new ObjGroupDTO();
            groupInfo.setType(TreeEnum.TreeNode.SERVER_GROUP);
            groupInfo.setServerInfoList(servGroupInfo.getXmlServerInfoList());
            serverGroupNode = new DefaultMutableTreeNode(groupInfo, true);
            rootNode.add(serverGroupNode);
            for (XMLServerInfoDTO servInfo : servGroupInfo.getXmlServerInfoList())
            {
                serverInfoNode = new DefaultMutableTreeNode(servInfo, true);
                serverGroupNode.add(serverInfoNode);
            }
        }
        this.expandAll(new TreePath(rootNode));
    }
    private void expandAll(TreePath parent)
    {
        TreeNode node = (TreeNode) parent.getLastPathComponent();
        if (node.getChildCount() >= 0)
        {
            for (Enumeration e = node.children(); e.hasMoreElements();)
            {
                TreeNode n = (TreeNode) e.nextElement();
                TreePath path = parent.pathByAddingChild(n);
                this.expandPath(path);
            }
        }
        //if (expand)
        this.expandPath(parent);
        //this.collapsePath(parent);        
    }
    
    //mouse click action
    private void nodeClickedAction(MouseEvent e)
    { 
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) this.getLastSelectedPathComponent();
        if(node == null)
        {
            return;
        }
        if (!node.isLeaf())
        {
            logger.info(node.toString() + " isn't a leaf node, do nothing and return.");
            return;
        }
        Object nodeObj = node.getUserObject();
        logger.info("ClickNode = " + node.toString() + "," + nodeObj.getClass().toString());
        if (e.getClickCount() == 2)
        {
            if (nodeObj instanceof XMLServerInfoDTO)
            {
                this.serverClickedAction(node);
            } else if (nodeObj instanceof DBInfoDTO)
            {
                this.dbClickedAction(node);
            } else if (nodeObj instanceof SchemaInfoDTO)
            {
                this.schemaClickedAction(node);
            } else if (nodeObj instanceof CatalogInfoDTO)
            {
                if (!nodeObj.toString().equals("information_schema"))
                {
                    this.catalogClickedAction(node);
                }
            } else if (nodeObj instanceof TableInfoDTO)
            {
                this.tableClickedAction(node);
            } else if (nodeObj instanceof ViewInfoDTO)
            {
                this.viewClickedAction(node);
            }
            this.expandPath(this.getSelectionPath());
        }
    }
    //all default method is for class in the same package
    void serverClickedAction(DefaultMutableTreeNode clickNode)
    {
        XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) clickNode.getUserObject();
        String url = serverInfo.getPartURL() + serverInfo.getMaintainDB();
        logger.info("url=" + url + ",isConnected = " + serverInfo.isConnected() + ",isSavePwd = " + serverInfo.isSavePwd());
        //add begin by sunqk at 2019.4.29
        //TreeViewLogic logicForTree = new TreeViewLogic(this, parent);
        //add begin by sunqk at 2019.4.29
        if (serverInfo.isConnected())
        {
            this.addServerBranchNode(clickNode);
            logger.info(clickNode.toString() + " isConnected:" + serverInfo.isConnected());
            eventBus.fireEvent(new TreeNodeChangeEvent(clickNode));
            logger.info("------------ Fire Node = " + clickNode.toString() + " ------------");
            //add begin by sunqk at 2019.4.29
            //logicForTree.ChangePassWD(serverInfo);
            //add begin by sunqk at 2019.4.29
        } else
        {
            boolean isSuccess = true;
            if (serverInfo.isSavePwd())
            {
                Connection conn = null;
                try
                {
                    conn = JdbcHelper.getConnection(url, serverInfo);                    
                    serverInfo.setConnected(true);
                    TreeController.getInstance().getMoreServerInfo(serverInfo);//must get last sys oid before add server branch
                    this.addServerBranchNode(clickNode);
                    eventBus.fireEvent(new TreeNodeChangeEvent(clickNode));
                    logger.info("------------ Fire Node = " + clickNode.toString() + " ------------");
                } catch (SQLException e)
                {
                    isSuccess = false;
                    this.dealSQLException(e);
//                    logger.error(e.getMessage());
//                    e.printStackTrace(System.out);
//                    JOptionPane.showMessageDialog(this, e.getMessage(),
//                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                } catch (ClassNotFoundException e)
                {
                    isSuccess = false;
                    MessageUtil.showErrorMsg(parent, e);
                } finally
                {
                    JdbcHelper.close(conn);
                    logger.info("close connection");
                }
            }
            if (!serverInfo.isSavePwd() || isSuccess == false)
            {
                String message = MessageFormat.format(msgBundle.getString("pleaseEnterPasswordForUser"),
                        serverInfo.getUser()) + "\n" + MessageFormat.format(msgBundle.getString("onServer"),
                        serverInfo.getName());
                PwdInputDialog dialog = new PwdInputDialog(parent, true, message, false);
                dialog.setLocationRelativeTo(parent);
                dialog.setVisible(true);
                if (dialog.isSuccess())
                {
                    //cancle or closing
                    String pwd = dialog.getPassword();
                    Connection conn = null;
                    try
                    {
                        conn = JdbcHelper.getConnection(url, serverInfo.getUser(), pwd, serverInfo.isOnSSL());
                        SQLWarning warn = conn.getWarnings();
                        if (warn != null)
                        {
                            //to show login hint msg
                            logger.warn("Connection SQLWarning=" + warn);
                            JOptionPane.showMessageDialog(this, warn.getMessage(),
                                    constBundle.getString("hint"), JOptionPane.INFORMATION_MESSAGE);
                        }
                        JdbcHelper.close(conn);
                        logger.info("close connection");
                        
                        serverInfo.setPwd(pwd);                        
                        if(serverInfo.isSavePwd())//in fact not save pwd anymore
                        {                            
                            TreeController.getInstance().changePwdForServerFromXML(serverInfo);
                        }
                        serverInfo.setConnected(true);
                        TreeController.getInstance().getMoreServerInfo(serverInfo);//must get last sys oid before add server branch
                        this.addServerBranchNode(clickNode);
                        eventBus.fireEvent(new TreeNodeChangeEvent(clickNode));
                        logger.info("------------ Fire Node = " + clickNode.toString() + " ------------");
                        //add begin by sunqk at 2019.4.29
                        
                        //logicForTree.ChangePassWD(serverInfo);
                        //add begin by sunqk at 2019.4.29
                    } catch (SQLException ex)
                    {
                        this.dealSQLException(ex);
                    }catch (ClassNotFoundException |  DocumentException | IOException ex)
                    {
                        MessageUtil.showErrorMsg(parent, ex);
                    }finally
                    {
                        JdbcHelper.close(conn);
                        logger.debug("close connection");
                    }
                }
            }
        }
    }
    private void addServerBranchNode(DefaultMutableTreeNode clickNode)
    {
        //logger.info("Enter:" + clickNode.toString());
        XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) clickNode.getUserObject();  
        try
        {
            BranchDTO branch = TreeController.getInstance()
                    .getServerBranchInfo(serverInfo);
            
            DefaultMutableTreeNode objGroupNode;
            DefaultMutableTreeNode objNode;
            for (ObjGroupDTO objGroupInfo : branch.getAll())
            {
                objGroupNode = new DefaultMutableTreeNode(objGroupInfo, true);
                clickNode.add(objGroupNode);
                //logger.info("Group Node = " + objGroupNode.toString());
                for (ObjInfoDTO objInfo : objGroupInfo.getObjInfoList())
                {
                    objNode = new DefaultMutableTreeNode(objInfo, true);
                    objGroupNode.add(objNode);
                }
            }
            logger.info("Expand; " + this.getSelectionPath());
            this.expandPath(this.getSelectionPath());
        } catch (SQLException ex)
        { 
            this.dealSQLException(ex);
        }catch(Exception ex)
        {            
            MessageUtil.showErrorMsg(parent, ex);
        }
    }
    void dbClickedAction(DefaultMutableTreeNode clickNode)
    {
        DBInfoDTO dbInfo = (DBInfoDTO) clickNode.getUserObject();
        try
        {
            BranchDTO branch = TreeController.getInstance()
                    .getDBBranchInfo(dbInfo.getHelperInfo());
            
            DefaultMutableTreeNode objGroupNode;
            DefaultMutableTreeNode objNode;
            for (ObjGroupDTO objGroupInfo : branch.getAll())
            {
                objGroupNode = new DefaultMutableTreeNode(objGroupInfo, true);
                clickNode.add(objGroupNode);
                //logger.debug("Group Node = " + objGroupNode.toString());
                for (ObjInfoDTO objInfo : objGroupInfo.getObjInfoList())
                {
                    objNode = new DefaultMutableTreeNode(objInfo, true);
                    objGroupNode.add(objNode);
                }
            }
        } catch (SQLException ex)
        {
            this.dealSQLException(ex);
        }catch (Exception ex)
        {
            MessageUtil.showErrorMsg(parent, ex);
        }
    }
    private void catalogClickedAction(DefaultMutableTreeNode clickNode)
    {
        CatalogInfoDTO catalogInfo = (CatalogInfoDTO) clickNode.getUserObject();    
        try
        {
            BranchDTO branch = TreeController.getInstance()
                    .getCatalogBranchInfo(catalogInfo.getHelperInfo());
            DefaultMutableTreeNode objGroupNode;
            DefaultMutableTreeNode objNode;
            for (ObjGroupDTO objGroupInfo : branch.getAll())
            {
                objGroupNode = new DefaultMutableTreeNode(objGroupInfo, true);
                clickNode.add(objGroupNode);
                //logger.info("Group Node = " + objGroupNode.toString());
                for (ObjInfoDTO objInfo : objGroupInfo.getObjInfoList())
                {
                    objNode = new DefaultMutableTreeNode(objInfo, true);
                    objGroupNode.add(objNode);
                }
            }
        } catch (SQLException ex)
        {
            this.dealSQLException(ex);
        }catch (Exception ex)
        {
            MessageUtil.showErrorMsg(parent, ex);
        }
    }
    void schemaClickedAction(DefaultMutableTreeNode clickNode)
    {
        SchemaInfoDTO schemaInfo = (SchemaInfoDTO) clickNode.getUserObject();
        try
        {
            BranchDTO branch = TreeController.getInstance()
                    .getSchemaBranchInfo(schemaInfo.getHelperInfo());
            
            DefaultMutableTreeNode objGroupNode;
            DefaultMutableTreeNode objNode; 
            //TABLE_GROUP, VIEW_GROUP, SEQUENCE_GROUP,FUNCTION_GROUP
            for (ObjGroupDTO objGroupInfo : branch.getAll())
            {
                objGroupNode = new DefaultMutableTreeNode(objGroupInfo, true);
                clickNode.add(objGroupNode);
                logger.info("Group Node = " + objGroupNode.toString());
                for (ObjInfoDTO objInfo : objGroupInfo.getObjInfoList())
                {
                    objNode = new DefaultMutableTreeNode(objInfo, true);
                    objGroupNode.add(objNode);
                }
            }
        } catch (SQLException ex)
        {
            this.dealSQLException(ex);
        } catch (Exception ex)
        {
            MessageUtil.showErrorMsg(parent, ex);
        }
    }
    void viewClickedAction(DefaultMutableTreeNode clickNode)
    {
        ViewInfoDTO viewInfo = (ViewInfoDTO) clickNode.getUserObject();
        if("pg_catalog".equals(viewInfo.getHelperInfo().getSchema())){
            logger.debug("pg_catalog view return.");
            return;
        }
        try
        {
            BranchDTO branch = TreeController.getInstance()
                    .getViewBranchInfo(viewInfo.getHelperInfo());
            
            DefaultMutableTreeNode objGroupNode;
            DefaultMutableTreeNode objNode;  
            //COLUMN_GROUP,RULE_GROUP,TRIGGER_GROUP
            for (ObjGroupDTO objGroupInfo : branch.getAll())
            {
                objGroupNode = new DefaultMutableTreeNode(objGroupInfo, true);
                clickNode.add(objGroupNode);
                logger.info("Group Node = " + objGroupNode.toString());
                for (ObjInfoDTO objInfo : objGroupInfo.getObjInfoList())
                {
                    objNode = new DefaultMutableTreeNode(objInfo, true);
                    objGroupNode.add(objNode);
                }
            }
        } catch (Exception ex)
        {
            MessageUtil.showErrorMsg(parent, ex);
        }
    }
    void tableClickedAction(DefaultMutableTreeNode clickNode)
    {
        TableInfoDTO tabInfo = (TableInfoDTO) clickNode.getUserObject();
        if("pg_catalog".equals(tabInfo.getHelperInfo().getSchema())){
            logger.debug("pg_catalog table return.");
            return;
        }
        try
        {
            BranchDTO branch = TreeController.getInstance()
                    .getTableBranchInfo(tabInfo.getHelperInfo());
            DefaultMutableTreeNode objGroupNode;
            DefaultMutableTreeNode objNode;
            //COLUMN_GROUP,CONSTRAINT_GROUP,INDEXE_GROUP,RULE_GROUP,TRIGGER_GROUP
            for (ObjGroupDTO objGroupInfo : branch.getAll())
            {
                objGroupNode = new DefaultMutableTreeNode(objGroupInfo, true);
                clickNode.add(objGroupNode);
                //logger.info("Group Node = " + objGroupNode.toString());
                for (ObjInfoDTO objInfo : objGroupInfo.getObjInfoList())
                {
                    objNode = new DefaultMutableTreeNode(objInfo, true);
                    objGroupNode.add(objNode);
                }
            }
        } catch (SQLException ex)
        {
            this.dealSQLException(ex);
        } catch (Exception ex)
        {
            MessageUtil.showErrorMsg(parent, ex);
        }
    }
    
    
    //mouse press action
    private void nodePressAction(MouseEvent e)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) this.getLastSelectedPathComponent();
        if (node == null)
        {
            return;
        }
        Object nodeObj = node.getUserObject();
        logger.info("PressNode = " + node.toString() + ", Class = " + nodeObj.getClass().toString());
//        if (e.getButton() == 3)//popup right-click item
//        {
//            this.showRightClickMenu(e);
//        } else      
        if (e.getButton() == 1)//press to get full property
        {
            DefaultTreeModel model = (DefaultTreeModel) this.getModel();
            if (nodeObj instanceof ObjInfoDTO)
            {
                ObjInfoDTO objInfo = (ObjInfoDTO) nodeObj;
                DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
                ObjGroupDTO groupInfo = (ObjGroupDTO) groupNode.getUserObject();
                try
                {
                    logger.info("+++++++++++++++++++++++++++++++++get property");
                    Object obj = TreeController.getInstance().getObjPropertyInfo(groupInfo.getHelperInfo(), objInfo);
                    if (obj == null)
                    {                       
                        String hint = MessageFormat.format(constBundle.getString("removeNonexistentObjNode"), objInfo.getType(), objInfo.getName());
                        logger.warn(hint);
                        JOptionPane.showMessageDialog(parent, hint,
                                constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
                        this.removeObjNode(model, node, objInfo, groupNode, groupInfo);
                    } else
                    {
                        node.setUserObject(obj);
                        model.reload();
                        this.scrollPathToVisible(new TreePath(node.getPath()));
                    }
                } catch (SQLException ex)
                {
                    this.dealSQLException(ex);
                }
                catch (Exception ex)
                {
                    MessageUtil.showErrorMsg(parent, ex);
                }
            } 
            //all node press fired
            eventBus.fireEvent(new TreeNodeChangeEvent(node));
            logger.info("-----------------Fire Node = " + node.toString() + "-----------------");
        }
    }
    private void removeObjNode( DefaultTreeModel model, DefaultMutableTreeNode node, AbstractObject objInfo, 
            DefaultMutableTreeNode groupNode, ObjGroupDTO groupInfo)
    {
        //DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        //ObjGroupInfoDTO groupInfo = (ObjGroupDTO) groupNode.getUserObject();
        List<ObjInfoDTO> objInfoList = groupInfo.getObjInfoList();
        int size = objInfoList.size();
        if (TreeEnum.TreeNode.FUNCTION == objInfo.getType()
                || TreeEnum.TreeNode.PROCEDURE == objInfo.getType()) {
            for (int i = 0; i < size; i++)
            {
                FunObjInfoDTO fun = (FunObjInfoDTO) objInfoList.get(i);
                if (fun.toString().equals(objInfo.getName()))
                {
                    objInfoList.remove(i);
                    break;
                }
            }
        } else
        {
            for (int i = 0; i < size; i++)
            {
                ObjInfoDTO obj = objInfoList.get(i);
                if (obj.getName().equals(objInfo.getName()))
                {
                    objInfoList.remove(i);
                    break;
                }
            }
        }
        model.removeNodeFromParent(node);
        this.scrollPathToVisible(new TreePath(groupNode.getPath()));
        this.setSelectionPath(new TreePath(groupNode.getPath()));
    }
    
    
    //object fresh item action
    void refreshItemAction(DefaultMutableTreeNode node)
    {
        logger.info("-----------------Refresh Node: " + node);
        if (node.isRoot())
        {
            node.removeAllChildren();
            this.addRootBranch();
        }
        Object nodeObj = node.getUserObject();
        if (nodeObj instanceof XMLServerInfoDTO)
        {
            node.removeAllChildren();
            this.serverClickedAction(node);
        } 
        else if (nodeObj instanceof AbstractObject)
        {
            //always leaf: tablespace, grouprole,loginrole,language, function, sequence, rule,trigger,column,constraint
            //can be branch: database,catalog,schema,view,table 
            if (!node.isLeaf())
            {
                node.removeAllChildren();
            }
            AbstractObject obj = (AbstractObject) nodeObj;
            try
            {
                node.setUserObject(TreeController.getInstance().getObjPropertyInfo(obj.getHelperInfo(), obj));
            } catch (SQLException ex)
            {
                this.dealSQLException(ex);
            } catch (Exception cex)
            {
                MessageUtil.showErrorMsg(parent, cex);
            }
        }
        DefaultTreeModel model = (DefaultTreeModel) this.getModel();
        model.reload();
        TreePath tp = new TreePath(node.getPath());
        logger.info("Expand : " + tp);
        this.scrollPathToVisible(tp);
        this.setSelectionPath(tp);
        logger.info("-----------------Fire Node = " + node.toString() + "-----------------");
        eventBus.fireEvent(new TreeNodeChangeEvent(node)); 
       
    }
    //object drop item action
    void dropItemAction(DefaultMutableTreeNode node, boolean isCascade)
    {
        logger.info("drop " + node);
        Object nodeObj = node.getUserObject();
        DefaultTreeModel model = (DefaultTreeModel) this.getModel();
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        ObjGroupDTO groupInfo = (ObjGroupDTO) groupNode.getUserObject();
        
        if (nodeObj instanceof XMLServerInfoDTO)
        {
            XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) node.getUserObject();
            String type = constBundle.getString("servers");
            DropConfirmDialog dropConfirm = new DropConfirmDialog();
            int response = dropConfirm.showDialog(parent, type, serverInfo.toString(), false);
            if (response == JOptionPane.YES_OPTION)
            {
                try
                {
                    TreeController.getInstance().deleteServerFromXML(serverInfo);
                } catch (DocumentException | IOException ex)
                {
                    JOptionPane.showMessageDialog(this, "Failed to delete server from config file.\n" + ex.getMessage(),
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
                for (XMLServerInfoDTO server : groupInfo.getServerInfoList())
                {
                    if (server.equals(serverInfo))
                    {
                        groupInfo.getServerInfoList().remove(server);
                        break;
                    }
                }
                model.removeNodeFromParent(node);
                this.scrollPathToVisible(new TreePath(node.getPath()));
                this.setSelectionPath(new TreePath(groupNode.getPath()));
                eventBus.fireEvent(new TreeNodeChangeEvent(groupNode));
                logger.info("-----------------Fire Node = " + groupNode.toString());
            }
        } else if (nodeObj instanceof AbstractObject)
        {
            AbstractObject objInfo = (AbstractObject) nodeObj;
            String type;
            if (objInfo instanceof ConstraintInfoDTO)
            {
                ConstraintInfoDTO constraintInfo = (ConstraintInfoDTO) objInfo;
                TreeEnum.TreeNode enumType = constraintInfo.getType();
                type = this.getStringTypeFromEnum(enumType);
            } else
            {
                type = constBundle.getString(objInfo.getType().toString().toLowerCase());
            }
            DropConfirmDialog dropConfirm = new DropConfirmDialog();
            int response = dropConfirm.showDialog(parent, type, objInfo.getName(), isCascade);
            if (response == JOptionPane.YES_OPTION)
            {
                try
                {
                    TreeController.getInstance().dropObj(objInfo.getHelperInfo(), objInfo, isCascade);
                    if (nodeObj instanceof ConstraintInfoDTO
                            || nodeObj instanceof ColumnInfoDTO
                            || nodeObj instanceof PartitionInfoDTO)
                    {
                        DefaultMutableTreeNode tableNode = (DefaultMutableTreeNode) groupNode.getParent();
                        this.refreshItemAction(tableNode);
                    } else
                    {
                        this.removeObjNode(model, node, objInfo, groupNode, groupInfo);
                        eventBus.fireEvent(new TreeNodeChangeEvent(groupNode));
                        logger.debug("-----------------Fire Node = " + groupNode.toString());
                    }                    
                } catch (SQLException ex)
                {
                    this.dealSQLException(ex);
                } catch (ClassNotFoundException cex) 
                {
                    MessageUtil.showErrorMsg(parent, cex);
                } catch (Exception ex) 
                {
                    MessageUtil.showErrorMsg(parent, ex);
                }
            }
        }
   
    }  
    private String getStringTypeFromEnum(TreeEnum.TreeNode enumType)
    {
        switch (enumType)
        {
            case PRIMARY_KEY:
                return constBundle.getString("primaryKey");
            case FOREIGN_KEY:
                return constBundle.getString("foreignKey");
            case CHECK:
                return constBundle.getString("check");
            case UNIQUE:
                return constBundle.getString("unique");
            case EXCLUDE:
                return constBundle.getString("exclude");
            default:
                logger.warn("An Exception enum type, return null.");
                return null;
        }
    }

    
    //get servergroup name
    String[] getServerGroup()
    {
        int size = rootNode.getChildCount();
        String[] serverGroup = new String[size];
        for (int i = 0; i < size; i++)
        {
            serverGroup[i] = rootNode.getChildAt(i).toString().split("\\(", 0)[0].trim();
            //DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode)rootNode.getChildAt(i);            
        }
        logger.info("ServerGroup = " + serverGroup[0]);
        return serverGroup;
    }
    DefaultTreeModel getTreeModel()
    {
        return (DefaultTreeModel) this.getModel();
    }

    
    void dealSQLException(SQLException ex)
    {
        MessageUtil.showErrorMsg(parent, ex);
        
        int errorcode = ex.getErrorCode();
        logger.error("=sqlstate=" + ex.getSQLState());//08001
        logger.error("=errorcode=" + errorcode);
        logger.error("=msg=" + ex.getMessage());
        //ex.printStackTrace(System.err);
        if (errorcode == 0
                && ex.getMessage().contains("Connection") && ex.getMessage().contains("refused"))
        {
            String serverStr = this.getSelectionPath().getPathComponent(2).toString();
            logger.debug("=================serverStr=" + serverStr);
            int serverCount = serverGroupNode.getChildCount();
            for (int i = 0; i < serverCount; i++)
            {
                if (serverGroupNode.getChildAt(i).toString().equals(serverStr))
                {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) serverGroupNode.getChildAt(i);
                    XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) node.getUserObject();
                    //disconnect its server
                    serverInfo.setConnected(false);
                    node.removeAllChildren();
                    DefaultTreeModel model = (DefaultTreeModel) this.getModel();
                    model.reload();
                    TreePath tp = new TreePath(node.getPath());
                    this.scrollPathToVisible(tp);
                    this.setSelectionPath(tp);
                    break;
                }
            }
        }
    }

    
    
    HelperInfoDTO getCurrentConnInfo(boolean selectOpenedServer)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) this.getLastSelectedPathComponent();
        if (node == null) 
        {
            logger.debug("node is null, return null.");
            return null;
        } else if (node.isRoot()) 
        {
            if (node.isLeaf()) 
            {
                logger.debug("node is root and not opened, return null.");
                return null;  
            }else
            {
                DefaultMutableTreeNode sgroupNode
                        = (DefaultMutableTreeNode) node.getChildAt(0);
                HelperInfoDTO helper = new HelperInfoDTO();
                findOpendServer(sgroupNode, helper, selectOpenedServer);
                logger.debug("Return: " + (helper == null? "no connected server" : helper.getPartURL()));
                return helper;
            }
        } else 
        {
            HelperInfoDTO helper = new HelperInfoDTO();
            getParentServer(node, helper, selectOpenedServer);
            logger.debug("Return: " + (helper == null? "no connected server" : helper.getPartURL()));
            return helper;
        }
    }
    private HelperInfoDTO findOpendServer(DefaultMutableTreeNode sgroupNode, HelperInfoDTO helper, boolean selectOpenedServer)
    {
        if (sgroupNode.isLeaf()) 
        {
            logger.warn("server group node isn't opened or has no server, return null.");
            return null;
        }

        XMLServerInfoDTO server = null;
        for (int i = 0; i < sgroupNode.getChildCount(); i++) 
        {
            DefaultMutableTreeNode snode = (DefaultMutableTreeNode) sgroupNode.getChildAt(i);
            server = (XMLServerInfoDTO) snode.getUserObject();
            logger.debug(snode + " conntected=" + server.isConnected());
            if (server.isConnected()) 
            {
                logger.debug("Return: " + snode.toString());
                DBUtil.copyProperties(server, helper);
                if(selectOpenedServer)
                {
                    this.setSelectionPath(new TreePath(snode.getPath()));
                }
                return helper;
            } 
        }
        logger.debug("Return: " + "no connected server");
        return null;
    }
    private void getParentServer(DefaultMutableTreeNode node, HelperInfoDTO helper, boolean selectOpenedServer)
    {
        if (node == null) {
            helper = null;
            return;
        }
        Object nodeObj = node.getUserObject();
        if(nodeObj instanceof XMLServerInfoDTO)
        {
            if (((XMLServerInfoDTO) nodeObj).isConnected()) 
            {
                DBUtil.copyProperties((XMLServerInfoDTO) nodeObj, helper);
            } else 
            {
                DefaultMutableTreeNode sgroupNode 
                      = (DefaultMutableTreeNode) node.getParent();
                helper = findOpendServer(sgroupNode, helper, selectOpenedServer);
            }
        } else 
        {
            if (nodeObj instanceof DBInfoDTO) 
            {
                helper.setDbName(((DBInfoDTO)nodeObj).getName());
                helper.setDbTablespace(((DBInfoDTO)nodeObj).getTablespace());
            } else if (nodeObj instanceof EventTriggerInfoDTO) 
            {
                helper.setDbName(((EventTriggerInfoDTO) nodeObj).getName());
            } else if (nodeObj instanceof SchemaInfoDTO
                    || nodeObj instanceof CatalogInfoDTO) 
            {
                helper.setSchema(((AbstractObject)nodeObj).getName());
                helper.setSchemaOid(((AbstractObject)nodeObj).getOid());
            }else if(nodeObj instanceof TableInfoDTO)
            {
                helper.setType(TreeEnum.TreeNode.TABLE);
                helper.setRelation(((TableInfoDTO)nodeObj).getName());
                helper.setRelationOid(((TableInfoDTO)nodeObj).getOid());
            }else if(nodeObj instanceof ViewInfoDTO)
            {
                helper.setType(TreeEnum.TreeNode.VIEW);
                helper.setRelation(((ViewInfoDTO)nodeObj).getName());
                helper.setRelationOid(((ViewInfoDTO)nodeObj).getOid());
            } else if (nodeObj instanceof ObjInfoDTO) 
            {
                ObjInfoDTO obj = (ObjInfoDTO) nodeObj;
                switch (obj.getType()) 
                {
                    case DATABASE:
                    case EVENT_TRIGGER:
                        helper.setDbName(obj.getName());
                        break;
                    case SCHEMA:
                        helper.setSchema(obj.getName());
                        helper.setSchemaOid(obj.getOid());
                        break;
                    case TABLE:
                    case VIEW:
                        helper.setType(obj.getType());
                        helper.setRelation(obj.getName());
                        if(obj.getOid() != null)//for new add
                        {
                            helper.setRelationOid(obj.getOid());
                        }
                        break;
                }
            }
            getParentServer((DefaultMutableTreeNode)node.getParent(), helper, selectOpenedServer);
        }
    }
  
    
}
