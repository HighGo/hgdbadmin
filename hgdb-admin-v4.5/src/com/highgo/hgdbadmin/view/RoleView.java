/* ------------------------------------------------ 
* 
* File: RoleView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\RoleView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.Compare;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.RoleInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.SysVariableInfoDTO;
import com.highgo.hgdbadmin.model.VariableInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import com.highgo.hgdbadmin.util.MessageUtil;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * 
 * 
 */
public class RoleView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private RoleInfoDTO roleInfo;
    private HelperInfoDTO helperInfo;
    private boolean isSuccess=false;   
    private  boolean isCanLogin;
    
    private SysVariableInfoDTO currentVariable = null;
    private DefaultListModel modelNotMember;
    private DefaultListModel modelMember;
    
    private String dateFormatStr = "yyyy-MM-dd HH:mm:ss";
    private SimpleDateFormat dateFormat =  new SimpleDateFormat(dateFormatStr);

    //add
    public RoleView(JFrame parent, boolean modal, HelperInfoDTO helperInfo, boolean isCanLogin)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.isCanLogin = isCanLogin;
        this.roleInfo = null;
        initComponents();
        jtp.setEnabledAt(5, false);        
        if (isCanLogin)
        {
            this.setTitle(constBundle.getString("addLoginRole"));
            this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                    "/com/highgo/hgdbadmin/image/user.png")));           
        } else
        {
            this.setTitle(constBundle.getString("addGroupRole"));
            this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                    "/com/highgo/hgdbadmin/image/users.png")));
        }        
        this.setNormalInfo();
        this.limitPrivilege4Create();
        
        //define action
        KeyAdapter btnOkEnableKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOKEnableForAdd();
            }
        };
        txfdName.addKeyListener(btnOkEnableKeyAdapter);
        pwdfdPwd.addKeyListener(btnOkEnableKeyAdapter);
        pwdfdPwdAgain.addKeyListener(btnOkEnableKeyAdapter);
        //txfdConnectionLimit.addKeyListener(btnOkEnableKeyAdapter);
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });        
    }
    
    //property
    public RoleView(JFrame parent, boolean modal, RoleInfoDTO roleInfo)
    {
        super(parent, modal);
        this.helperInfo = roleInfo.getHelperInfo();
        this.isCanLogin = roleInfo.isCanLogin();
        this.roleInfo = roleInfo;
        initComponents();
        jtp.setEnabledAt(5, false);
        if (roleInfo.isCanLogin())
        {
            this.setTitle(constBundle.getString("loginRoles") + " " + roleInfo.getName());
            this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                    "/com/highgo/hgdbadmin/image/user.png")));
        } else
        {
            this.setTitle(constBundle.getString("groupRoles") + " " + roleInfo.getName());
            this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                    "/com/highgo/hgdbadmin/image/users.png")));
        }        
        this.setNormalInfo();
        this.setProperty();
        this.limitPrivilege4Property();
        
        //define action
        KeyAdapter btnOkEnableKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOKEnableForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnableKeyAdapter);
        txarComment.addKeyListener(btnOkEnableKeyAdapter);
        pwdfdPwd.addKeyListener(btnOkEnableKeyAdapter);
        pwdfdPwdAgain.addKeyListener(btnOkEnableKeyAdapter);      
        txfdConnectionLimit.addKeyListener(btnOkEnableKeyAdapter);
        spinTime.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                btnOKEnableForChange();
            }
        });
        ActionListener btnOkEnableAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKEnableForChange();
            }
        };
        cbInfinity.addActionListener(btnOkEnableAction);
        cbCanLogin.addActionListener(btnOkEnableAction);
        cbInheritFromParent.addActionListener(btnOkEnableAction);
        cbSuperUser.addActionListener(btnOkEnableAction);
        cbCreateDB.addActionListener(btnOkEnableAction);
        cbCreateRole.addActionListener(btnOkEnableAction);
        //ygq v5 del start
        //cbModifyCatalog.addActionListener(btnOkEnableAction);
        //ygq v5 del start
        cbInitReplicationAndBackup.addActionListener(btnOkEnableAction);
        modelMember.addListDataListener(new ListDataListener()
        {
            @Override
            public void contentsChanged(ListDataEvent e)
            {
                btnOKEnableForChange();
            }
            @Override
            public void intervalRemoved(ListDataEvent e)
            {  
                btnOKEnableForChange();
            }          
            @Override
            public void intervalAdded(ListDataEvent e)
            {
                btnOKEnableForChange();
            }           
        });                
        DefaultTableModel vModel = (DefaultTableModel) tblVariables.getModel();
        vModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOKEnableForChange();
            }
        });
        
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });  

    }
   
    //limit privilege for sm
    private void limitPrivilege4Create()
    {
        cbCreateDB.setEnabled(false);
        cbCreateRole.setEnabled(false);
        
        cbWithAdminOption.setEnabled(false);
    }
    private void limitPrivilege4Property()
    {
        String loginuser = roleInfo.getHelperInfo().getUser();
        String user = roleInfo.getName();
        boolean selflogin = loginuser.equals(user);
        boolean syslogin = "sysdba".equals(loginuser)|| "syssao".equals(loginuser)|| "syssso".equals(loginuser);
        boolean sysUser = "sysdba".equals(user) || "syssao".equals(user)|| "syssso".equals(user);
        
        if("sysdba".equals(loginuser) && !sysUser){
            pwdfdPwd.setEnabled(false);
            pwdfdPwdAgain.setEnabled(false);
            
            cbCreateDB.setEnabled(false);
            cbCreateRole.setEnabled(false);
            return;//sysdba can change alll of non-sys user
        }
                
        txfdName.setEditable((selflogin & !sysUser));
        txarComment.setEnabled("sysdba".equals(loginuser) || loginuser.equals(user));

        boolean changePwd = (syslogin  && selflogin)
                || ("syssso".equals(loginuser) && !sysUser)
                || (!syslogin && selflogin);
        pwdfdPwd.setEnabled(changePwd);
        pwdfdPwdAgain.setEnabled(changePwd);
        boolean valid = "syssso".equals(loginuser) && !sysUser;
        cbInfinity.setEnabled(valid);
        spinTime.setEnabled(valid);
        txfdConnectionLimit.setEditable("sysdba".equals(loginuser) || user.equals(loginuser));//sysdba can modify for all, other can modify self

        cbInheritFromParent.setEnabled(false);
        cbCreateDB.setEnabled(false);
        cbCreateRole.setEnabled(false);
        cbInitReplicationAndBackup.setEnabled(false);
        
        btnAdd.setEnabled("syssso".equals(loginuser)  && !sysUser);
        btnRemove.setEnabled("syssso".equals(loginuser)  && !sysUser);
        cbWithAdminOption.setEnabled(false);//always
        
        btnAddChangeVariable.setEnabled(false);
        btnRemoveVariable.setEnabled(false); 
    }
    
    
    class SpinnerDate implements Comparable{
         private long time;
         public SpinnerDate(long time){
             this.time = time;
         }

        @Override
        public int compareTo(Object o)
        {            
             return compareTo((Date)o);
        }
        
        private int compareTo(Date t)
        {
            return time < t.getTime() ? -1 : (time == t.getTime() ? 0 : 1);
        }
    }
    private SpinnerDateModel spinDateModel;
    private void initSpinTime()
    {
        /*
        Calendar min = Calendar.getInstance();        
        Calendar max = Calendar.getInstance();
        max.add(Calendar.DAY_OF_MONTH, 7);
        Date value = max.getTime();        
        SpinnerDate end =  new SpinnerDate(value.getTime());
        SpinnerDate start = new SpinnerDate(min.getTime().getTime());
        logger.debug("start=" + min.getTime() + " | value=" + value + " | " + (start.compareTo(value) <= 0));
        logger.debug("end=" + max.getTime() + " | value=" + value + " | " + (end.compareTo(value) >= 0));
        spinDateModel = new SpinnerDateModel(value, min.getTime(), max.getTime(), Calendar.DAY_OF_MONTH );
        */
        spinDateModel = new SpinnerDateModel();//start from now
        spinTime.setModel(spinDateModel);
        
        JSpinner.DateEditor startEditor = new JSpinner.DateEditor(spinTime, dateFormatStr);
        spinTime.setEditor(startEditor);
        JFormattedTextField tf = ((JSpinner.DateEditor) spinTime.getEditor()).getTextField();
        DefaultFormatterFactory factory = (DefaultFormatterFactory) tf.getFormatterFactory();
        DateFormatter formatter = (DateFormatter) factory.getDefaultFormatter();
        formatter.setAllowsInvalid(false);
    }
    
    private void setNormalInfo()
    {
        this.initSpinTime();
        cbCanLogin.setSelected(isCanLogin);        
        modelMember = new DefaultListModel();
        listMember.setModel(modelMember);
        TreeController tc = TreeController.getInstance();
        cbInitReplicationAndBackup.setEnabled(tc.isVersionHigherThan(helperInfo.getDbSys(),
                helperInfo.getVersionNumber(), 9, 0));
        try
        {
            Object[] groupRoles = tc.getMemberRoles(helperInfo, roleInfo == null ? null: roleInfo.getOid());
            modelNotMember = new DefaultListModel();
            for (Object groupRole : groupRoles)
            {
                modelNotMember.addElement(groupRole);
            }
            listNotMember.setModel(modelNotMember);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbDatabase.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "template")));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbVariableNames.setModel(new DefaultComboBoxModel(tc.getVariables(helperInfo,"role")));
            cbbVariableNames.setSelectedIndex(-1);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        //cbValue.setEnabled(false);
        //txfdVariableValue.setEnabled(true);
        currentVariable = (SysVariableInfoDTO) cbbVariableNames.getItemAt(0);
            
        //format action
        txfdConnectionLimit.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                //can only enter digit (>=0)
                txfdIntegerValueKeyTyped(evt);
            }
        });
        txfdVariableValue.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                variableValueForAddKeyTyped(evt);
            }
        });
    }
     
    private void setProperty()
    {
        txfdName.setText(roleInfo.getName());
        txfdOID.setText(roleInfo.getOid().toString());
        txarComment.setText(roleInfo.getComment());        
        try
        {
            logger.debug("roleInfo.getAccountExpires()=" + roleInfo.getAccountExpires());
            if (roleInfo.getAccountExpires() != null && !roleInfo.getAccountExpires().isEmpty())
            {
                Date newValue = dateFormat.parse(roleInfo.getAccountExpires());
                spinDateModel.setStart(new SpinnerDate(newValue.getTime()));
                spinTime.setValue(newValue);
                
                cbInfinity.setSelected(false);
                spinTime.setEnabled(true);
            }else
            {
                cbInfinity.setSelected(true);
                spinTime.setEnabled(false);
            }
        } catch (ParseException ex)
        {
            logger.error(ex.getMessage());
        }
        txfdConnectionLimit.setText(roleInfo.getConnectionLimit());
        cbCanLogin.setSelected(roleInfo.isCanLogin());
        cbInheritFromParent.setSelected(roleInfo.isCanInherits());
        cbSuperUser.setSelected(roleInfo.isSuperuser());
        cbCreateDB.setSelected(roleInfo.isCanCreateDB());
        cbCreateRole.setSelected(roleInfo.isCanCreateRoles());
        //ygq v5 admin del start
        //cbModifyCatalog.setSelected(roleInfo.isCanModifyCatalog());
        //ygq v5 admin del end
        cbInitReplicationAndBackup.setSelected(roleInfo.isCanInitStreamReplicationAndBackup());
        List<String> memberOf = roleInfo.getMemeberOf();
        if (memberOf != null)
        {
            for (String mof : memberOf)
            {
                modelMember.addElement(mof);
                int size = modelNotMember.getSize();
                for (int i = size - 1; i >= 0; i--)
                {
                    //logger.info(mof + "***********" + modelNotMember.getElementAt(i));
                    if (mof.equals(modelNotMember.getElementAt(i))
                            || mof.equals(modelNotMember.getElementAt(i) + "(*)"))
                    {
                        modelNotMember.remove(i);
                        break;
                    }
                }
            }
        }
        List<VariableInfoDTO> vlist = roleInfo.getVariableList();
        if (vlist != null)
        {
            DefaultTableModel vModel = (DefaultTableModel) tblVariables.getModel();
            for (VariableInfoDTO variable : vlist)
            {
                String[] v = new String[3];
                v[0] = variable.getDb();
                v[1] = variable.getName();
                v[2] = variable.getValue();
                vModel.addRow(v);
            }
        }
        List<SecurityLabelInfoDTO> slist = roleInfo.getSecurityLabelList();
        if (slist != null)
        {
            DefaultTableModel sModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[3];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                sModel.addRow(sl);
            }
        }
        
    }
        
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        cbSuperUser = new javax.swing.JCheckBox();
        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblUseSlony = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbUseSlony = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        pnlDefinition = new javax.swing.JPanel();
        lblPwd = new javax.swing.JLabel();
        lblPwdAgain = new javax.swing.JLabel();
        lblPwdValidDate = new javax.swing.JLabel();
        lblConnectionLimit = new javax.swing.JLabel();
        txfdConnectionLimit = new javax.swing.JTextField();
        pwdfdPwd = new javax.swing.JPasswordField();
        pwdfdPwdAgain = new javax.swing.JPasswordField();
        spinTime = new javax.swing.JSpinner();
        cbInfinity = new javax.swing.JCheckBox();
        pnlRolePrivileges = new javax.swing.JPanel();
        cbCanLogin = new javax.swing.JCheckBox();
        cbInheritFromParent = new javax.swing.JCheckBox();
        cbInitReplicationAndBackup = new javax.swing.JCheckBox();
        cbCreateDB = new javax.swing.JCheckBox();
        cbCreateRole = new javax.swing.JCheckBox();
        pnlRoleMember = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listNotMember = new javax.swing.JList();
        lblNotRember = new javax.swing.JLabel();
        lblRember = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        listMember = new javax.swing.JList();
        cbWithAdminOption = new javax.swing.JCheckBox();
        btnAdd = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        pnlVariables = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblVariables = new javax.swing.JTable();
        lblVariableValue = new javax.swing.JLabel();
        lblDatabase = new javax.swing.JLabel();
        btnAddChangeVariable = new javax.swing.JButton();
        btnRemoveVariable = new javax.swing.JButton();
        lblVariableName = new javax.swing.JLabel();
        cbbDatabase = new javax.swing.JComboBox();
        cbbVariableNames = new javax.swing.JComboBox();
        txfdVariableValue = new javax.swing.JTextField();
        cbValue = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        cbSuperUser.setBackground(new java.awt.Color(255, 255, 255));
        cbSuperUser.setText(constBundle.getString("superUser"));
        cbSuperUser.addChangeListener(new javax.swing.event.ChangeListener()
        {
            public void stateChanged(javax.swing.event.ChangeEvent evt)
            {
                cbSuperUserStateChanged(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setName("dlgServerAdd"); // NOI18N

        jtp.setMinimumSize(new java.awt.Dimension(520, 470));
        jtp.setPreferredSize(new java.awt.Dimension(520, 470));

        pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
        pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
        pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

        lblName.setText(constBundle.getString("roleName"));

        lblOID.setText("OID");

        lblUseSlony.setText(constBundle.getString("useSlony"));

        lblComment.setText(constBundle.getString("comment"));

        txfdOID.setEditable(false);

        cbbUseSlony.setEnabled(false);

        txarComment.setColumns(20);
        txarComment.setRows(5);
        jScrollPane1.setViewportView(txarComment);

        javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
        pnlProperties.setLayout(pnlPropertiesLayout);
        pnlPropertiesLayout.setHorizontalGroup(
            pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txfdOID)
                            .addComponent(txfdName)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)))
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbbUseSlony, 0, 418, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlPropertiesLayout.setVerticalGroup(
            pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblOID))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addComponent(lblComment)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUseSlony)
                    .addComponent(cbbUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lblOID.getAccessibleContext().setAccessibleName("");

        jtp.addTab(constBundle.getString("property"), pnlProperties);
        pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

        pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
        pnlDefinition.setMinimumSize(new java.awt.Dimension(520, 470));
        pnlDefinition.setPreferredSize(new java.awt.Dimension(520, 470));

        lblPwd.setText(constBundle.getString("pwd"));

        lblPwdAgain.setText(constBundle.getString("pwdAgain"));

        lblPwdValidDate.setText(constBundle.getString("pwdValidDate"));

        lblConnectionLimit.setText(constBundle.getString("connectionLimit"));

        txfdConnectionLimit.setText("-1");

        spinTime.setEnabled(false);

        cbInfinity.setBackground(new java.awt.Color(255, 255, 255));
        cbInfinity.setSelected(true);
        cbInfinity.setText(constBundle.getString("accountExpiryInfinity"));
        cbInfinity.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                cbInfinityActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
        pnlDefinition.setLayout(pnlDefinitionLayout);
        pnlDefinitionLayout.setHorizontalGroup(
            pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionLayout.createSequentialGroup()
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
                        .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlDefinitionLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(lblPwd, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlDefinitionLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(lblPwdAgain, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pwdfdPwd)
                            .addComponent(pwdfdPwdAgain)))
                    .addGroup(pnlDefinitionLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pnlDefinitionLayout.createSequentialGroup()
                                .addComponent(lblPwdValidDate, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(cbInfinity, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(spinTime, javax.swing.GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE))
                            .addGroup(pnlDefinitionLayout.createSequentialGroup()
                                .addComponent(lblConnectionLimit, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txfdConnectionLimit)))))
                .addContainerGap())
        );
        pnlDefinitionLayout.setVerticalGroup(
            pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlDefinitionLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPwd)
                    .addComponent(pwdfdPwd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPwdAgain)
                    .addComponent(pwdfdPwdAgain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPwdValidDate)
                    .addComponent(spinTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbInfinity))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblConnectionLimit)
                    .addComponent(txfdConnectionLimit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(348, Short.MAX_VALUE))
        );

        jtp.addTab(constBundle.getString("definition"), pnlDefinition);

        pnlRolePrivileges.setBackground(new java.awt.Color(255, 255, 255));
        pnlRolePrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
        pnlRolePrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

        cbCanLogin.setBackground(new java.awt.Color(255, 255, 255));
        cbCanLogin.setText(constBundle.getString("canLogin"));
        cbCanLogin.setEnabled(false);

        cbInheritFromParent.setBackground(new java.awt.Color(255, 255, 255));
        cbInheritFromParent.setSelected(true);
        cbInheritFromParent.setText(constBundle.getString("inheritFromParent")
        );

        cbInitReplicationAndBackup.setBackground(new java.awt.Color(255, 255, 255));
        cbInitReplicationAndBackup.setText(constBundle.getString("canInitiateStreamReplicationAndBackup"));

        cbCreateDB.setBackground(new java.awt.Color(255, 255, 255));
        cbCreateDB.setText(constBundle.getString("canCreateDB")
        );

        cbCreateRole.setBackground(new java.awt.Color(255, 255, 255));
        cbCreateRole.setText(constBundle.getString("canCreateRole"));

        javax.swing.GroupLayout pnlRolePrivilegesLayout = new javax.swing.GroupLayout(pnlRolePrivileges);
        pnlRolePrivileges.setLayout(pnlRolePrivilegesLayout);
        pnlRolePrivilegesLayout.setHorizontalGroup(
            pnlRolePrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRolePrivilegesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlRolePrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cbCanLogin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbInheritFromParent, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbCreateDB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbCreateRole, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbInitReplicationAndBackup, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(419, Short.MAX_VALUE))
        );
        pnlRolePrivilegesLayout.setVerticalGroup(
            pnlRolePrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRolePrivilegesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(cbCanLogin)
                .addGap(10, 10, 10)
                .addComponent(cbInheritFromParent)
                .addGap(10, 10, 10)
                .addComponent(cbCreateDB)
                .addGap(10, 10, 10)
                .addComponent(cbCreateRole)
                .addGap(10, 10, 10)
                .addComponent(cbInitReplicationAndBackup)
                .addContainerGap(305, Short.MAX_VALUE))
        );

        jtp.addTab(constBundle.getString("rolePrivileges"), pnlRolePrivileges);
        pnlRolePrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

        pnlRoleMember.setBackground(new java.awt.Color(255, 255, 255));
        pnlRoleMember.setMinimumSize(new java.awt.Dimension(520, 470));

        jScrollPane2.setViewportView(listNotMember);

        lblNotRember.setText(constBundle.getString("notMember"));

        lblRember.setText(constBundle.getString("member"));

        jScrollPane4.setViewportView(listMember);

        cbWithAdminOption.setBackground(new java.awt.Color(255, 255, 255));
        cbWithAdminOption.setText(" WITH ADMIN OPTION");

        btnAdd.setText(">>");
        btnAdd.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddActionPerformed(evt);
            }
        });

        btnRemove.setText("<<");
        btnRemove.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRemoveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlRoleMemberLayout = new javax.swing.GroupLayout(pnlRoleMember);
        pnlRoleMember.setLayout(pnlRoleMemberLayout);
        pnlRoleMemberLayout.setHorizontalGroup(
            pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                        .addGroup(pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNotRember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnRemove, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(btnAdd, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addGap(10, 10, 10)
                        .addGroup(pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
                            .addComponent(lblRember, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(cbWithAdminOption, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlRoleMemberLayout.setVerticalGroup(
            pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                        .addComponent(lblNotRember)
                        .addGap(10, 10, 10)
                        .addGroup(pnlRoleMemberLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                                .addComponent(btnAdd)
                                .addGap(10, 10, 10)
                                .addComponent(btnRemove))
                            .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                                .addGap(10, 10, 10)
                                .addComponent(cbWithAdminOption))))
                    .addGroup(pnlRoleMemberLayout.createSequentialGroup()
                        .addComponent(lblRember)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        jtp.addTab(constBundle.getString("roleMember"), pnlRoleMember);

        pnlVariables.setBackground(new java.awt.Color(255, 255, 255));
        pnlVariables.setPreferredSize(new java.awt.Dimension(520, 500));

        tblVariables.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", "", ""
            }
        )
        {
            boolean[] canEdit = new boolean []
            {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        tblVariables.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblVariables.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
                tblVariablesMousePressed(evt);
            }
        });
        jScrollPane5.setViewportView(tblVariables);
        if (tblVariables.getColumnModel().getColumnCount() > 0)
        {
            tblVariables.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("database"));
            tblVariables.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("variableName"));
            tblVariables.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("variableValue"));
        }

        lblVariableValue.setText(constBundle.getString("variableValue"));

        lblDatabase.setText(constBundle.getString("databases"));

        btnAddChangeVariable.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
        btnAddChangeVariable.setMaximumSize(new java.awt.Dimension(80, 23));
        btnAddChangeVariable.setMinimumSize(new java.awt.Dimension(80, 23));
        btnAddChangeVariable.setPreferredSize(new java.awt.Dimension(80, 23));
        btnAddChangeVariable.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddChangeVariableActionPerformed(evt);
            }
        });

        btnRemoveVariable.setText(constBundle.getString("remove"));
        btnRemoveVariable.setMaximumSize(new java.awt.Dimension(80, 23));
        btnRemoveVariable.setMinimumSize(new java.awt.Dimension(80, 23));
        btnRemoveVariable.setPreferredSize(new java.awt.Dimension(80, 23));
        btnRemoveVariable.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRemoveVariableActionPerformed(evt);
            }
        });

        lblVariableName.setText(constBundle.getString("variableName"));

        cbbDatabase.setEditable(true);

        cbbVariableNames.setEditable(true);
        cbbVariableNames.addItemListener(new java.awt.event.ItemListener()
        {
            public void itemStateChanged(java.awt.event.ItemEvent evt)
            {
                cbbVariableNamesItemStateChanged(evt);
            }
        });

        cbValue.setBackground(new java.awt.Color(255, 255, 255));
        cbValue.setEnabled(false);

        javax.swing.GroupLayout pnlVariablesLayout = new javax.swing.GroupLayout(pnlVariables);
        pnlVariables.setLayout(pnlVariablesLayout);
        pnlVariablesLayout.setHorizontalGroup(
            pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVariablesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                    .addGroup(pnlVariablesLayout.createSequentialGroup()
                        .addComponent(lblDatabase, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(cbbDatabase, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlVariablesLayout.createSequentialGroup()
                        .addComponent(lblVariableName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(cbbVariableNames, 0, 412, Short.MAX_VALUE))
                    .addGroup(pnlVariablesLayout.createSequentialGroup()
                        .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(pnlVariablesLayout.createSequentialGroup()
                        .addComponent(lblVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(cbValue)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txfdVariableValue)))
                .addContainerGap())
        );
        pnlVariablesLayout.setVerticalGroup(
            pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlVariablesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblVariableName)
                    .addComponent(cbbVariableNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblVariableValue)
                    .addComponent(txfdVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbValue))
                .addGap(10, 10, 10)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDatabase)
                    .addComponent(cbbDatabase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jtp.addTab(constBundle.getString("variables"), pnlVariables);
        pnlVariables.getAccessibleContext().setAccessibleName("");

        pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
        pnlSecurityLabel.setDoubleBuffered(false);
        pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
        pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

        lblSecurityLabel.setText(constBundle.getString("securityLabels"));

        lblProvider.setText(constBundle.getString("provider"));

        tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", ""
            }
        ));
        tblSecurityLabels.addMouseListener(new java.awt.event.MouseAdapter()
        {
            public void mousePressed(java.awt.event.MouseEvent evt)
            {
                tblSecurityLabelsMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(tblSecurityLabels);
        if (tblSecurityLabels.getColumnModel().getColumnCount() > 0)
        {
            tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
            tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
        }

        btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
        btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
        btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
        btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
        btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnRemoveSecurityLabelActionPerformed(evt);
            }
        });

        btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
        btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
        btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
        btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
        btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnAddChangeSecurityLabelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
        pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
        pnlSecurityLabelLayout.setHorizontalGroup(
            pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSecurityLabelLayout.createSequentialGroup()
                .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                        .addContainerGap(6, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)
                    .addComponent(txfdProvider))
                .addContainerGap())
            .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                        .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                        .addGap(10, 10, 10))))
        );
        pnlSecurityLabelLayout.setVerticalGroup(
            pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblProvider))
                .addGap(10, 10, 10)
                .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSecurityLabel))
                .addGap(10, 10, 10))
        );

        jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

        pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
        pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
        pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

        cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
        cbReadOnly.setSelected(true);
        cbReadOnly.setText(constBundle.getString("readOnly"));
        cbReadOnly.setActionCommand("");
        cbReadOnly.setEnabled(false);

        tpDefinitionSQL.setEditable(false);
        spDefinationSQL.setViewportView(tpDefinitionSQL);

        javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
        pnlSQL.setLayout(pnlSQLLayout);
        pnlSQLLayout.setHorizontalGroup(
            pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSQLLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cbReadOnly)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pnlSQLLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnlSQLLayout.setVerticalGroup(
            pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSQLLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(cbReadOnly)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
                .addContainerGap())
        );

        jtp.addTab("SQL", pnlSQL);
        pnlSQL.getAccessibleContext().setAccessibleName("SQL");

        getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
        jtp.getAccessibleContext().setAccessibleName("");

        btnHelp.setText(constBundle.getString("help"));
        btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
        btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
        btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
        btnHelp.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnHelpActionPerformed(evt);
            }
        });

        btnOK.setText(constBundle.getString("ok"));
        btnOK.setEnabled(false);
        btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
        btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
        btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

        btnCancle.setText(constBundle.getString("cancle"));
        btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
        btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
        btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
        btnCancle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnCancleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 269, Short.MAX_VALUE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could only be entered integer(>=0).
        char keyCh = evt.getKeyChar();
        Character.isDigit(keyCh);
        //logger.debug("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh == '-')
            {
                logger.debug("lastIndexOf=" + txfdConnectionLimit.getText().lastIndexOf("-"));
                if (txfdConnectionLimit.getText().lastIndexOf("-") >= 0)
                {
                    evt.setKeyChar('\0');
                }
            } else if (keyCh != '') //回车字符
             {
                 evt.setKeyChar('\0');
             }
        }        
    }
    
    private void variableValueForAddKeyTyped(KeyEvent evt)
    {
        String type = currentVariable.getValueType();
        char keyCh = evt.getKeyChar();
        logger.info("keychar = " + keyCh+",isDigit = "+ Character.isDigit(keyCh));
        logger.info("vType = " + type);
        switch (type)
        {
            case "real":
                if ((keyCh < '0') || (keyCh > '9'))
                {
                    if (keyCh != '' && keyCh != '.' && keyCh!='-')
                    {
                        evt.setKeyChar('\0');
                    }
                    if(!currentVariable.getMinValue().startsWith("-")
                            && keyCh=='-')
                    {
                         evt.setKeyChar('\0');
                    }
                }
                break;
            case "integer":
                if ((keyCh < '0') || (keyCh > '9'))
                {
                    if (keyCh != ''  && keyCh!='-')
                    {
                        evt.setKeyChar('\0');
                    }
                    if (!currentVariable.getMinValue().startsWith("-")
                            && keyCh == '-')
                    {
                        evt.setKeyChar('\0');
                    }
                }
                break;
        }
    }
      
    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);    
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnAddChangeVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeVariableActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeVariableActionPerformed
        // TODO add your handling code here:
        String value;
        if (txfdVariableValue.isEnabled())
        {
            value = txfdVariableValue.getText();
        } else
        {
            value = String.valueOf(cbValue.isSelected());
        }
        if(value==null || value.equals(""))
        {
            value = "DEFAULT";
        }        
        String db = cbbDatabase.getSelectedItem().toString();
        String name;
        boolean isExist = false;
        if (cbbVariableNames.getSelectedItem() != null && !cbbVariableNames.getSelectedItem().equals(""))
        {
            name = cbbVariableNames.getSelectedItem().toString();
            DefaultTableModel tableModel = (DefaultTableModel) tblVariables.getModel();
            int row = tableModel.getRowCount();
            
            for (int i = 0; i < row; i++)
            {
                logger.info("row=" + row);
                String olddb = null;
                if (tableModel.getValueAt(i, 0) != null)
                {
                    olddb = tableModel.getValueAt(i, 0).toString();
                }
                String oldname = tableModel.getValueAt(i, 1).toString();
                if (name.equals(oldname) && db.equals(olddb))
                {
                    isExist = true;
                    tableModel.setValueAt(value, i, 2);
                    break;
                }
            }
            if (!isExist)
            {
                String[] v = new String[3];
                v[0] = db;
                v[1] = name;
                v[2] = value;
                tableModel.addRow(v);
            }
        }  
    }//GEN-LAST:event_btnAddChangeVariableActionPerformed

    private void btnRemoveVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveVariableActionPerformed
    {//GEN-HEADEREND:event_btnRemoveVariableActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int selectedRow = tblVariables.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblVariables.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveVariableActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
       logger.info(evt.getActionCommand());
        if (this.isCanLogin)
        {
            HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.LOGINROLE);
        } else
        {
            HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.GROUPROLE);
        }
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void tblVariablesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblVariablesMousePressed
    {//GEN-HEADEREND:event_tblVariablesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int row = tblVariables.getSelectedRow();
        logger.info("row = " + row);
        if (row < 0)
        {
            return;
        }
        Object db = tblVariables.getValueAt(row, 0);
        cbbDatabase.setSelectedItem(db);
        Object v = tblVariables.getValueAt(row, 1);
        cbbVariableNames.setSelectedItem(v);
        String value = tblVariables.getValueAt(row, 2).toString();
        if (value.equals("true") || value.equals("false"))
        {
            cbValue.setEnabled(true);
            cbValue.setSelected(Boolean.valueOf(value));
            txfdVariableValue.setEnabled(false);
            txfdVariableValue.setText("");
        } else
        {
            cbValue.setEnabled(false);
            cbValue.setSelected(false);
            txfdVariableValue.setEnabled(true);
            txfdVariableValue.setText(value);
        }
    }//GEN-LAST:event_tblVariablesMousePressed

    private void tblSecurityLabelsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblSecurityLabelsMousePressed
    {//GEN-HEADEREND:event_tblSecurityLabelsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press Security Label table");
        int row = tblSecurityLabels.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblSecurityLabels.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabels.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }//GEN-LAST:event_tblSecurityLabelsMousePressed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddActionPerformed
    {//GEN-HEADEREND:event_btnAddActionPerformed
        // TODO add your handling code here:
        int selectedIndex = listNotMember.getSelectedIndex();
        logger.info("selectedIndex = " + selectedIndex);
        if (selectedIndex < 0) 
        {
            return;
        }
        logger.info("selectedItem = " + listNotMember.getModel().getElementAt(selectedIndex));
        String mem = (String) listNotMember.getModel().getElementAt(selectedIndex);
        if (cbWithAdminOption.isSelected())
        {
            mem = mem + "(*)";
        }
        modelMember.addElement(mem);
        modelNotMember.remove(selectedIndex);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveActionPerformed
    {//GEN-HEADEREND:event_btnRemoveActionPerformed
        // TODO add your handling code here:
        int selectedIndex = listMember.getSelectedIndex();
        logger.info("selectedIndex = " + selectedIndex);
        if (selectedIndex < 0)
        {
            return;
        }
        modelNotMember.addElement(listMember.getModel().getElementAt(selectedIndex).toString().replace("(*)", ""));
        modelMember.remove(selectedIndex);
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void cbSuperUserStateChanged(javax.swing.event.ChangeEvent evt)//GEN-FIRST:event_cbSuperUserStateChanged
    {//GEN-HEADEREND:event_cbSuperUserStateChanged
        // TODO add your handling code here:
        //ygq v5 admin delete start
        //Boolean enable = cbSuperUser.isSelected();
        //cbModifyCatalog.setEnabled(enable);
        //cbModifyCatalog.setSelected(enable);
        //ygq v5 admin delete end
    }//GEN-LAST:event_cbSuperUserStateChanged
    private void cbbVariableNamesItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbVariableNamesItemStateChanged
    {//GEN-HEADEREND:event_cbbVariableNamesItemStateChanged
        // TODO add your handling code here:
        int index = cbbVariableNames.getSelectedIndex();
        if (index < 0)
        {
            return;
        }
        SysVariableInfoDTO variable = (SysVariableInfoDTO) cbbVariableNames.getSelectedItem();
        this.currentVariable = variable;
        
        boolean isEnableCkb = variable.getValueType().equals("bool");
        cbValue.setEnabled(isEnableCkb);
        cbValue.setSelected(false);
        txfdVariableValue.setEnabled(!isEnableCkb);
        txfdVariableValue.setText("");
        
    }//GEN-LAST:event_cbbVariableNamesItemStateChanged

    private void cbInfinityActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbInfinityActionPerformed
    {//GEN-HEADEREND:event_cbInfinityActionPerformed
        spinTime.setEnabled(!cbInfinity.isSelected());
    }//GEN-LAST:event_cbInfinityActionPerformed

    //for add
    private void btnOKEnableForAdd()
    {
        String dbName = txfdName.getText();
        if (dbName != null && !dbName.isEmpty()
                //&& pwdfdPwd.getPassword() != null && pwdfdPwd.getPassword().length != 0
                //&& pwdfdPwdAgain.getPassword() != null && pwdfdPwdAgain.getPassword().length != 0
                && Arrays.equals(pwdfdPwd.getPassword(), pwdfdPwdAgain.getPassword()))//all null or all not null
               // && !txfdConnectionLimit.getText().equals("-"))
        {
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);            
        }
    }
    private void jtpForAddStateChanged(ChangeEvent evt)                                 
    {                                     
        int index = jtp.getSelectedIndex();
        logger.info("TabIndex:" + index);        
        if (index == 6)
        {
            tpDefinitionSQL.setText("");
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNotComplete"));
            } else
            {
                if (!validatePwd())
                {
                    return;
                }
                RoleInfoDTO role = this.getRoleInfo();
                role.setDisplaySQL(true);
                sc.setDefine(tpDefinitionSQL.getDocument(), TreeController.getInstance().getDefinitionSQL(role));
            }
        }
    }          
    private void btnOKForAddActionPerformed(ActionEvent evt)                                      
    {
        logger.debug(evt.getActionCommand());
        try
        {
            if(!validatePwd()){
                return;
            }
            RoleInfoDTO role = this.getRoleInfo();
            role.setDisplaySQL(false);
            TreeController.getInstance().createObj(helperInfo, role);  
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            MessageUtil.showErrorMsg(this, ex);
            //logger.error(ex.getMessage());
            //JOptionPane.showMessageDialog(this, ex.getMessage(),
            //       constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            //ex.printStackTrace(System.err);
        }
    } 
    
    //for change
    private boolean isExpireTimeChange()
    {
        logger.debug("originExpireTime:" +  roleInfo.getAccountExpires());
        logger.debug("currentExpireTime:isInfinity=" + cbInfinity.isSelected() + ", value=" + dateFormat.format(spinTime.getValue()));

        return (roleInfo.getAccountExpires() == null && !cbInfinity.isSelected())
                || (roleInfo.getAccountExpires() != null && cbInfinity.isSelected())
                || (!cbInfinity.isSelected() && spinTime.getValue() != null && roleInfo.getAccountExpires() != null && !roleInfo.getAccountExpires().startsWith(dateFormat.format(spinTime.getValue())));
    }
    private void btnOKEnableForChange()
    {
        String name = txfdName.getText();
        if (name == null || name.isEmpty())
        {
            logger.info("name or pwd is null");
            btnOK.setEnabled(false);
        } else if (!txfdName.getText().equals(roleInfo.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(roleInfo.getComment())
                && !(txarComment.getText().isEmpty() && roleInfo.getComment() == null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else if (Arrays.equals(pwdfdPwd.getPassword(), pwdfdPwdAgain.getPassword()))
        {
            logger.info("password changed");
            btnOK.setEnabled(true);
        } else if (!txfdConnectionLimit.getText().isEmpty()
                && !txfdConnectionLimit.getText().equals("-")
                && !txfdConnectionLimit.getText().trim().equals(roleInfo.getConnectionLimit()))
        {
            logger.info("connection limit changed");
            btnOK.setEnabled(true);
        } else if (isExpireTimeChange())
        {
            logger.info("Expires timestamp changed ");
            btnOK.setEnabled(true);
        } else if (cbInfinity.isSelected() 
                && roleInfo.getAccountExpires()!=null && !roleInfo.getAccountExpires().isEmpty())
        {
            logger.info("Expires timestamp changed: infinity");
            btnOK.setEnabled(true);
        } 
        else if (cbCanLogin.isSelected() != roleInfo.isCanLogin()
                || cbInheritFromParent.isSelected() != roleInfo.isCanInherits()
                || cbSuperUser.isSelected() != roleInfo.isSuperuser()
                || cbCreateDB.isSelected() != roleInfo.isCanCreateDB()
                || cbCreateRole.isSelected() != roleInfo.isCanCreateRoles()
                // ygq v5 admin delete start
                //|| cbModifyCatalog.isSelected() != roleInfo.isCanModifyCatalog()
                // ygq v5 admin delete end
                || cbInitReplicationAndBackup.isSelected() != roleInfo.isCanInitStreamReplicationAndBackup())
        {
            logger.info("role privilege changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalStrList(this.getMemberOf(), roleInfo.getMemeberOf()))
        {
            logger.info("memberof changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalVariableList(this.getVariableList(), roleInfo.getVariableList()))
        {
            logger.info("variable changed");
            btnOK.setEnabled(true);
        } else
        {
            logger.info("nothing changed");
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("TabIndex:" + index);       
        if (index == 6)
        {
            tpDefinitionSQL.setText("");
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            } else
            {
                if (!validatePwd())
                {
                    return;
                }
                RoleInfoDTO role = this.getRoleInfo();
                role.setDisplaySQL(true);
                sc.setDefine(tpDefinitionSQL.getDocument(), TreeController.getInstance().getRoleChangeSQL(this.roleInfo, role));
            }
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        try
        {
            if(!validatePwd()){
                return;
            }
            RoleInfoDTO newRole = this.getRoleInfo();
            newRole.setDisplaySQL(false);
            TreeController.getInstance().executeSql(helperInfo, helperInfo.getDbName(),
                    TreeController.getInstance().getRoleChangeSQL(this.roleInfo, newRole));
            changCurrentUserPwd = helperInfo.getUser().equals(newRole.getName())
                    && (newRole.getPwd() != null && !newRole.getPwd().isEmpty()
                    && !newRole.getPwd().equals(roleInfo.getPwd()));
            isSuccess = true;
            this.dispose();
        } catch (ClassNotFoundException | SQLException ex)
        {
            isSuccess = false;
            MessageUtil.showErrorMsg(this, ex);
            //logger.error(ex.getMessage());
            //JOptionPane.showMessageDialog(this, ex.getMessage(),
            //        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            //ex.printStackTrace(System.out);
        }
    } 
    
    
     private boolean validatePwd()
    {
        if ((pwdfdPwd.getPassword() == null || pwdfdPwd.getPassword().length == 0)
                && (pwdfdPwdAgain.getPassword() == null || pwdfdPwdAgain.getPassword().length == 0))
        {
            return true;//no password input
        } else if (String.valueOf(pwdfdPwd.getPassword()).equals(String.valueOf(pwdfdPwdAgain.getPassword())))
        {
            //input password same
            return true;
        } else
        {
            JOptionPane.showMessageDialog(this, constBundle.getString("passwordValidDiff"),
                    constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
            return false;
        }
    }
    
    private List<VariableInfoDTO> getVariableList()
    {
        List<VariableInfoDTO> variableList = new ArrayList();
        DefaultTableModel variableModel = (DefaultTableModel) tblVariables.getModel();
        int variableRow = variableModel.getRowCount();
        logger.info("variableRow:" + variableRow);
        if (variableRow > 0)
        {
            for (int i = 0; i < variableRow; i++)
            {
                VariableInfoDTO variable = new VariableInfoDTO();
                if (variableModel.getValueAt(i, 0) == null)
                {
                    variable.setDb(null);
                } else
                {
                    variable.setDb(variableModel.getValueAt(i, 0).toString());
                }
                variable.setName(variableModel.getValueAt(i, 1).toString());
                variable.setValue(variableModel.getValueAt(i, 2).toString());
                variableList.add(variable);
            }
        }
        return variableList;
    }
    private List<String> getMemberOf()
    {
        List<String> owners = new ArrayList();
        int size = modelMember.getSize();
        for (int i = 0; i < size; i++)
        {
            owners.add(modelMember.getElementAt(i).toString());
        }
        return owners;
    }    
    public RoleInfoDTO getRoleInfo()
    {
        RoleInfoDTO newRole = new RoleInfoDTO();
        if (this.roleInfo != null)
        {
           newRole.setOid(this.roleInfo.getOid());
        }
        newRole.setHelperInfo(helperInfo);
        newRole.setName(txfdName.getText());
        newRole.setComment(txarComment.getText());
        newRole.setEncrypted(false);
        if ((pwdfdPwd.getPassword() == null || pwdfdPwd.getPassword().length == 0)
                && (pwdfdPwdAgain.getPassword() == null || pwdfdPwdAgain.getPassword().length == 0))
        {
            //no password input
        } else if (String.valueOf(pwdfdPwd.getPassword()).equals(String.valueOf(pwdfdPwdAgain.getPassword())))
        {
            //input password same
            newRole.setPwd(String.valueOf(pwdfdPwd.getPassword()));
        } else
        {
            if(this.roleInfo != null){
                newRole.setPwd(this.roleInfo.getPwd());
            }
            //JOptionPane.showMessageDialog(this, constBundle.getString("passwordValidDiff"),
            //        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
            //return;
        }
        if (!txfdConnectionLimit.getText().equals("-"))
        {
            newRole.setConnectionLimit(txfdConnectionLimit.getText());
        }
        if(cbInfinity.isSelected())
        {
            newRole.setAccountExpires(null);
        }else
        {
            newRole.setAccountExpires(spinTime.getValue()==null? null:dateFormat.format(spinTime.getValue()));
        }
        newRole.setCanLogin(cbCanLogin.isSelected());
        newRole.setCanInherits(cbInheritFromParent.isSelected());
        newRole.setSuperuser(cbSuperUser.isSelected());
        newRole.setCanCreateDB(cbCreateDB.isSelected());
        newRole.setCanCreateRoles(cbCreateRole.isSelected());
        //ygq v5 admin delete start
        //newRole.setCanModifyCatalog(cbModifyCatalog.isSelected());
        //ygq v5 admin delete end
        newRole.setCanInitStreamReplicationAndBackup(cbInitReplicationAndBackup.isSelected());
        newRole.setWithAdminOption(cbWithAdminOption.isSelected());       
        //for member of
        newRole.setMemeberOf(this.getMemberOf());
        //for variable     
        newRole.setVariableList(this.getVariableList());
        //for security label
        /*
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.info("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        roleInfo.setSecurityLabelList(securityList);
        */
        return newRole;
    }    
    
    private boolean changCurrentUserPwd;
    public boolean changCurrentUserPwd()
    {
        logger.debug("changCurrentUserPwd=" + changCurrentUserPwd);
        return changCurrentUserPwd;
    }
    
    public boolean isSucccess()
    {
        return this.isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnAddChangeVariable;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JButton btnRemoveVariable;
    private javax.swing.JCheckBox cbCanLogin;
    private javax.swing.JCheckBox cbCreateDB;
    private javax.swing.JCheckBox cbCreateRole;
    private javax.swing.JCheckBox cbInfinity;
    private javax.swing.JCheckBox cbInheritFromParent;
    private javax.swing.JCheckBox cbInitReplicationAndBackup;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbSuperUser;
    private javax.swing.JCheckBox cbValue;
    private javax.swing.JCheckBox cbWithAdminOption;
    private javax.swing.JComboBox cbbDatabase;
    private javax.swing.JComboBox cbbUseSlony;
    private javax.swing.JComboBox cbbVariableNames;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblConnectionLimit;
    private javax.swing.JLabel lblDatabase;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNotRember;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblPwd;
    private javax.swing.JLabel lblPwdAgain;
    private javax.swing.JLabel lblPwdValidDate;
    private javax.swing.JLabel lblRember;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JLabel lblVariableName;
    private javax.swing.JLabel lblVariableValue;
    private javax.swing.JList listMember;
    private javax.swing.JList listNotMember;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlRoleMember;
    private javax.swing.JPanel pnlRolePrivileges;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JPanel pnlVariables;
    private javax.swing.JPasswordField pwdfdPwd;
    private javax.swing.JPasswordField pwdfdPwdAgain;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JSpinner spinTime;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTable tblVariables;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdConnectionLimit;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdVariableValue;
    // End of variables declaration//GEN-END:variables
}
