/* ------------------------------------------------ 
* 
* File: InactivityListener.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\InactivityListener.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/*
 *  @description monitors inactivity of an application.
 *  The inactivity interval and event mask can be changed at any time,
 *  but they will not become effective until you stop and start the listener.
 *  Some common event masks have be defined with the class:
 *  KEY_EVENTS - includes key type events
 *  MOUSE_EVENTS - includes mouse motion events
 *  USER_EVENTS - includes KEY_EVENTS and MOUSE_EVENT (this is the default)
 *
 *  @Author Liu Yuanyuan
 *
 */
class InactivityListener implements ActionListener, AWTEventListener
{
    private final Logger logger = LogManager.getLogger(getClass());

    public final static long KEY_EVENTS 
            = AWTEvent.KEY_EVENT_MASK;
    public final static long MOUSE_EVENTS
            = AWTEvent.MOUSE_MOTION_EVENT_MASK + AWTEvent.MOUSE_EVENT_MASK;
    public final static long USER_EVENTS = KEY_EVENTS + MOUSE_EVENTS;

    private Window window;
    private Action action;
    private int interval;
    private long eventMask;
    private Timer timer = new Timer(0, this);

    // Use a default inactivity interval of 1 minute and listen for
    // USER_EVENTS
    public InactivityListener(Window window, Action action)
    {
        this(window, action, 1);
    }

    // Specify the inactivity interval and listen for USER_EVENTS
    public InactivityListener(Window window, Action action, int interval)
    {
        this(window, action, interval, USER_EVENTS);
    }

    // Specify the inactivity interval and the events to listen for
    public InactivityListener(Window window, Action action, int minutes, long eventMask)
    {
        this.window = window;
        setAction(action);
        setInterval(minutes);
        setEventMask(eventMask);
    }

    // The Action to be invoked after the specified inactivity period
    public void setAction(Action action)
    {
        this.action = action;
    }

    // The interval before the Action is invoked specified in minutes
    public void setInterval(int minutes)
    {
        setIntervalInMillis(minutes * 60000);
    }

    // The interval before the Action is invoked specified in milliseconds
    public void setIntervalInMillis(int interval)
    {
        this.interval = interval;
        timer.setInitialDelay(interval);
    }

    // A mask specifying the events to be passed to the AWTEventListener
    public void setEventMask(long eventMask)
    {
        this.eventMask = eventMask;
    }

    // Start listening for events.
    public void start()
    {
        timer.setInitialDelay(interval);
        timer.setRepeats(false);//true
        timer.start();
        Toolkit.getDefaultToolkit().addAWTEventListener(this, eventMask);
    }

    // Stop listening for events
    public void stop()
    {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        timer.stop();
    }

    // Implement ActionListener for the Timer
    @Override
    public void actionPerformed(ActionEvent e)
    {
        ActionEvent ae = new ActionEvent(window, ActionEvent.ACTION_PERFORMED, "");
        action.actionPerformed(ae);
    }

    // Implement AWTEventListener
    @Override
    public void eventDispatched(AWTEvent e)
    {
        //logger.debug("Enter: " + timer.isRunning());
        if (timer.isRunning())
        {
            timer.restart();
        }else{
            timer.start();
        }            
    }
}
