/* ------------------------------------------------ 
* 
* File: AboutAdmin.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\dialog\AboutAdmin.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view.dialog;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

/**
 *
 * @author Yuanyuan
 */
public class AboutAdmin extends JDialog
{
    public AboutAdmin(Frame owner, boolean modal)
    {
        super(owner, modal);
        this.setUndecorated(true);
        JButton b = new JButton();
        b.setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/about.jpg")));
        b.requestFocus(false);
        b.setSize(499, 340);
        b.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnAction(e);
            }
        });
        this.add(b);
        this.setSize(503, 344);
    }
    private void btnAction(ActionEvent evt)
    {
        this.dispose();
    } 
}
