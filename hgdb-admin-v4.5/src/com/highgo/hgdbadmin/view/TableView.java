/* ------------------------------------------------ 
* 
* File: TableView.java
*
* Abstract: 
* 		表操作窗体.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\TableView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.Compare;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.AutoVacuumDTO;
import com.highgo.hgdbadmin.model.ColItemInfoDTO;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.ObjItemInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class TableView extends JDialog {

    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private boolean isChange;

    private HelperInfoDTO helperInfo;
    private boolean isSuccess = false;
    private boolean isPartitioned = false;
    private boolean isPartitionRange = true;
    private EditableTableModel partitionKeyTableModel = new EditableTableModel(new Object[][]{},
            new String[]{
                constBundle.getString("partition_key_type"), constBundle.getString("partition_column"), constBundle.getString("partition_expression")
            });
    private EditableTableModel partitionTableModel = new EditableTableModel(new Object[][]{},
            new String[]{constBundle.getString("partition_name"), "DEFAULT",  "FROM", "TO","IN", "MODULUS", "REMAINDER" },
            new Class[]{Object.class, Boolean.class, Object.class, Object.class, Object.class, Object.class, Object.class});

    private TableInfoDTO tableInfo;
    
    public TableView(JFrame parent, boolean modal, HelperInfoDTO helperInfo) {
        super(parent, modal);
        this.isChange = false;
        this.helperInfo = helperInfo;
        initComponents();
        jtp.setEnabledAt(8, false);
        jtp.setEnabledAt(9, false);
        this.setTitle(constBundle.getString("addTable"));
        this.setNormalInfo();

        //define action
        txfdName.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent evt) {
                btnOkEnableForAdd(evt);
            }
        });
        jtp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent evt) {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOKForAddActionPerformed(e);
            }
        });
        
        partitionTypeComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index =  partitionTypeComboBox.getSelectedIndex();
                isPartitionRange = (index == 0);
                logger.debug("is Partition Range: " + isPartitionRange);    
                partitionTableModel.setColumnEditable(2, index == 0);
                partitionTableModel.setColumnEditable(3, index == 0);
                partitionTableModel.setColumnEditable(4, index == 1);
                partitionTableModel.setColumnEditable(5, index == 2);
                partitionTableModel.setColumnEditable(6, index == 2);
                /*if (partitionTypeComboBox.getSelectedIndex() == 0) {                   
                    partitionTableModel.setColumnEditable(1, true);
                    partitionTableModel.setColumnEditable(2, true);
                    partitionTableModel.setColumnEditable(3, false);
                } else if(partitionTypeComboBox.getSelectedIndex() == 1){
                    partitionTableModel.setColumnEditable(1, false);
                    partitionTableModel.setColumnEditable(2, false);
                    partitionTableModel.setColumnEditable(3, true);
                }else if(partitionTypeComboBox.getSelectedIndex() == 2){
                    partitionTableModel.setColumnEditable(1, false);
                    partitionTableModel.setColumnEditable(2, false);
                    partitionTableModel.setColumnEditable(3, false);
                    partitionTableModel.setColumnEditable(4, true);
                    partitionTableModel.setColumnEditable(5, true);   
                }*/      
            }
        });
          
        btnAddPartition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               btnAddPartitionactionPerformed(e);
            }
        });
    }

    
    public TableView(JFrame parent, boolean modal, TableInfoDTO tableInfo) {
        super(parent, modal);
        this.isChange = true;
        this.tableInfo = tableInfo;
        this.helperInfo = tableInfo.getHelperInfo();
        initComponents();
        jtp.setEnabledAt(8, false);
        this.setTitle(constBundle.getString("tables") + " " + tableInfo.getName());
        this.setNormalInfo();
        this.setProperty();

        KeyAdapter btnOkEnableKeyAdapter = new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent evt) {
                btnOkEnableForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnableKeyAdapter);
        cbbOwner.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                btnOkEnableForChange();
            }
        });
        txarComment.addKeyListener(btnOkEnableKeyAdapter);
        cbbTablespace.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                btnOkEnableForChange();
            }
        });
        txfdFillFactor.addKeyListener(btnOkEnableKeyAdapter);
        cbHasOIDs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOkEnableForChange();
            }
        });

        TableModelListener btnOkEnableTableModelListener = new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                btnOkEnableForChange();
            }
        };
        DefaultTableModel inherits = (DefaultTableModel) tblTableInherits.getModel();
        inherits.addTableModelListener(btnOkEnableTableModelListener);
        DefaultTableModel acl = (DefaultTableModel) tblPrivileges.getModel();
        acl.addTableModelListener(btnOkEnableTableModelListener);
        DefaultTableModel constr = (DefaultTableModel) tblConstraints.getModel();
        constr.addTableModelListener(btnOkEnableTableModelListener);
        DefaultTableModel column = (DefaultTableModel) tblColumns.getModel();
        column.addTableModelListener(btnOkEnableTableModelListener);

        ActionListener btnOkEnableAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOkEnableForChange();
            }
        };
        //vacuum
        cbCustomAutoVacuum.addActionListener(btnOkEnableAction);
        cbEnable.addActionListener(btnOkEnableAction);
        txfdVacuumBaseThreshold.addKeyListener(btnOkEnableKeyAdapter);
        txfdAnalyzeBaseThreshold.addKeyListener(btnOkEnableKeyAdapter);
        txfdVacuumScaleFactor.addKeyListener(btnOkEnableKeyAdapter);
        txfdAnalyzeScaleFactor.addKeyListener(btnOkEnableKeyAdapter);
        txfdVacuumCostDelay.addKeyListener(btnOkEnableKeyAdapter);
        txfdVacuumCostLimit.addKeyListener(btnOkEnableKeyAdapter);
        txfdFreezeMinimumAge.addKeyListener(btnOkEnableKeyAdapter);
        txfdFreezeMaximumAge.addKeyListener(btnOkEnableKeyAdapter);
        txfdFreezeTableAge.addKeyListener(btnOkEnableKeyAdapter);
        //toast
        cbCustomAutoVacuumToast.addActionListener(btnOkEnableAction);
        cbEnableToast.addActionListener(btnOkEnableAction);
        txfdVacuumBaseThresholdToast.addKeyListener(btnOkEnableKeyAdapter);
        txfdVacuumScaleFactorToast.addKeyListener(btnOkEnableKeyAdapter);
        txfdVacuumCostDelayToast.addKeyListener(btnOkEnableKeyAdapter);
        txfdVacuumCostLimitToast.addKeyListener(btnOkEnableKeyAdapter);
        txfdFreezeMinimumAgeToast.addKeyListener(btnOkEnableKeyAdapter);
        txfdFreezeMaximumAgeToast.addKeyListener(btnOkEnableKeyAdapter);
        txfdFreezeTableAgeToast.addKeyListener(btnOkEnableKeyAdapter);

        jtp.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent evt) {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnOKForChangeActionPerformed(e);
            }
        });
        
        btnAddPartition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddPartitionactionPerformed(e);
                btnOkEnableForChange();
            }
        });
        

    }

    private void setNormalInfo() {
        TreeController tc = TreeController.getInstance();
        //cbbSchema.setModel(new DefaultComboBoxModel(tc.getOptionArrary(helperInfo, "schema", null)));
        cbbSchema.setModel(new DefaultComboBoxModel(new String[]{helperInfo.getSchema()}));
        cbbSchema.setSelectedItem(helperInfo.getSchema());
        tabPartitionKey.setModel(partitionKeyTableModel);
        tabPartitions.setModel(partitionTableModel);

        try {
            cbbOwner.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "owner")));
            if (isChange) {
                cbbOwner.removeItemAt(0);
            } else {
                cbbOwner.setSelectedIndex(0);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try {
            cbbTablespace.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "tablespace")));
            if (isChange) {
                cbbTablespace.removeItemAt(0);
            } else {
                cbbTablespace.setSelectedIndex(0);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try {
            cbbOfTypeTable.setModel(new DefaultComboBoxModel(tc.getOfTypeTabless(helperInfo)));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try {
            cbbLikeRelation.setModel(new DefaultComboBoxModel(tc.getLikeTables(helperInfo)));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try {
            cbbTableInherits.setModel(new DefaultComboBoxModel(tc.getParentTables(helperInfo)));
            cbbTableInherits.setSelectedIndex(-1);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try {
            cbbRole.setModel(new DefaultComboBoxModel(tc.getRoles(helperInfo)));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }

        KeyAdapter valueIntegerKeyAdapter = new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                txfdIntegerValueKeyTyped(evt);//can only enter digit (>=0)
            }
        };
        KeyAdapter valueFloatKeyAdapter = new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent evt) {
                txfdFloatValueKeyTyped(evt);//can only enter digit (>=0)
            }
        };
        txfdFillFactor.addKeyListener(valueIntegerKeyAdapter);
        // for autovacuum,document:http://www.postgresql.org/docs/9.3/static/runtime-config-autovacuum.html
        txfdVacuumBaseThreshold.addKeyListener(valueIntegerKeyAdapter);
        txfdAnalyzeBaseThreshold.addKeyListener(valueIntegerKeyAdapter);
        txfdVacuumScaleFactor.addKeyListener(valueFloatKeyAdapter);
        txfdAnalyzeScaleFactor.addKeyListener(valueFloatKeyAdapter);
        txfdVacuumCostDelay.addKeyListener(valueIntegerKeyAdapter);
        txfdVacuumCostLimit.addKeyListener(valueIntegerKeyAdapter);
        txfdFreezeMinimumAge.addKeyListener(valueIntegerKeyAdapter);
        txfdFreezeMaximumAge.addKeyListener(valueIntegerKeyAdapter);
        txfdFreezeTableAge.addKeyListener(valueIntegerKeyAdapter);
        //for toast autovacuum
        txfdVacuumBaseThresholdToast.addKeyListener(valueIntegerKeyAdapter);
        txfdVacuumScaleFactorToast.addKeyListener(valueIntegerKeyAdapter);
        txfdVacuumCostDelayToast.addKeyListener(valueIntegerKeyAdapter);
        txfdVacuumCostLimitToast.addKeyListener(valueIntegerKeyAdapter);
        txfdFreezeMinimumAgeToast.addKeyListener(valueIntegerKeyAdapter);
        txfdFreezeMaximumAgeToast.addKeyListener(valueIntegerKeyAdapter);
        txfdFreezeTableAgeToast.addKeyListener(valueIntegerKeyAdapter);

        tblColumns.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                tblColumnsMousePressed(isChange);
            }
        });
        btnChangeColumn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnChangeColumnActionPerformed(e);
            }
        });
        btnAddColumn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddColumnActionPerformed(e);
            }
        });
        btnRemoveColumn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRemoveColumnActionPerformed(e);
            }
        });

        tblConstraints.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                tblConstraintsMousePressed(evt);
            }
        });
        btnAddConstraint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddConstraintActionPerformed(e);
            }
        });
        btnRemoveConstraint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRemoveConstraintActionPerformed(e);
            }
        });

        tblPrivileges.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                tblPrivilegesMousePressed(evt);
            }
        });
        btnRemovePrivilege.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnRemovePrivilegeActionPerformed(e);
            }
        });
        btnAddChangePrivilege.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnAddChangePrivilegeActionPerformed(e);
            }
        });

        tblSecurityLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent evt) {
                tblSecurityLabelMousePressed(evt);
            }
        });
        btnRemoveSecurityLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnRemoveSecurityLabelActionPerformed(evt);
            }
        });
        btnAddChangeSecurityLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                btnAddChangeSecurityLabelActionPerformed(evt);
            }
        });

        cbPartitioned.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cbPartitioned.isSelected()) {
                    isPartitioned = true;
                    //partitioned table cannot inherit from other tables
                    logger.info("partitioned table selected " + isPartitioned);
                    cbbTableInherits.setEnabled(false);
                    jtp.setEnabledAt(9, true);
                } else {
                    isPartitioned = false;
                    logger.info("partition table is not selected " + isPartitioned);
                    cbbTableInherits.setEnabled(true);
                    jtp.setEnabledAt(9, false);
                }
            }

        });
        
//        partitionTypeComboBox.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (partitionTypeComboBox.getSelectedIndex() == 0) {
//                    isPartitionRange = true;
//                    partitionTableModel.setColumnEditable(3, false);
//                    partitionTableModel.setColumnEditable(1, true);
//                    partitionTableModel.setColumnEditable(2, true);
//                } else if(partitionTypeComboBox.getSelectedIndex() == 1){
//                    isPartitionRange = false;
//                    partitionTableModel.setColumnEditable(3, true);
//                    partitionTableModel.setColumnEditable(1, false);
//                    partitionTableModel.setColumnEditable(2, false);
//                }else{
//                    partitionTableModel.setColumnEditable(3, true);
//                    partitionTableModel.setColumnEditable(1, true);
//                    partitionTableModel.setColumnEditable(2, true);
//                }
//                logger.info("is partition range: " + isPartitionRange);          
//            }
//        });

        btnAddPartitionKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultTableModel tableModel = (DefaultTableModel) tabPartitionKey.getModel();
                setPartitionKeyTableColumn(getColumnList());
                Object[] row = new Object[3];
                row[0] = null;
                row[1] = null;
                row[2] = null;
                tableModel.addRow(row);
                tableModel.fireTableDataChanged();

            }
        });

        
        btnRemovePartitionKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = tabPartitionKey.getSelectedRow();
                logger.info("selectedRow = " + selectedRow);
                if (selectedRow >= 0) {

                    DefaultTableModel tableModel = (DefaultTableModel) tabPartitionKey.getModel();
                    tableModel.removeRow(selectedRow);
                }
            }
        });
        btnRemovePartition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 int selectedRow = tabPartitions.getSelectedRow();
                logger.debug("selectedRow = " + selectedRow);
                if (selectedRow >= 0) {
                    DefaultTableModel tableModel = (DefaultTableModel) tabPartitions.getModel();
                    tableModel.removeRow(selectedRow);
                    btnOkEnableForChange();
                }
            }
        });
        
      

    }

    private void setProperty() {
        txfdName.setText(tableInfo.getName());
        txfdOID.setText(tableInfo.getOid().toString());
        cbbOwner.setSelectedItem(tableInfo.getOwner());
        cbbSchema.setSelectedItem(tableInfo.getSchema());
        txarComment.setText(tableInfo.getComment());
        cbbTablespace.setSelectedItem(tableInfo.getTablespace());
        cbbOfTypeTable.setSelectedItem(tableInfo.getOfType());
        cbbOfTypeTable.setEnabled(false);
        txfdFillFactor.setText(tableInfo.getFillFactor());
        cbHasOIDs.setSelected(tableInfo.isHasOID());
        cbUnlogged.setSelected(tableInfo.isUnlogged());
        cbUnlogged.setEnabled(false);
        List<ObjItemInfoDTO> inheritTables = tableInfo.getInheritTableList();
        if (inheritTables != null) {
            DefaultTableModel inheritModel = (DefaultTableModel) tblTableInherits.getModel();
            for (ObjItemInfoDTO inherit : inheritTables) {
                ObjItemInfoDTO[] labels = new ObjItemInfoDTO[1];
                labels[0] = inherit;
                inheritModel.addRow(labels);
            }
        }
        cbbLikeRelation.setSelectedItem(tableInfo.getLikeRelation());
        cbbLikeRelation.setEnabled(false);
        cbDefaultValues.setSelected(tableInfo.isIncludeDefaultValue());
        cbDefaultValues.setEnabled(false);
        cbConstraints.setSelected(tableInfo.isIncludeConstraint());
        cbConstraints.setEnabled(false);
        cbIndexes.setSelected(tableInfo.isIncludeIndexes());
        cbIndexes.setEnabled(false);
        cbStorage.setSelected(tableInfo.isIncludeStorage());
        cbStorage.setEnabled(false);
        cbComments.setSelected(tableInfo.isIncludeComments());
        cbComments.setEnabled(false);

        TreeController tc = TreeController.getInstance();
        List<ColumnInfoDTO> columnList = tableInfo.getColumnInfoList();
        if (columnList != null) {
            DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
            for (ColumnInfoDTO columnInfo : columnList) {
                Object[] column = new Object[10];
                column[0] = columnInfo;
                column[1] = tc.getPartColumnDefine(columnInfo);
                if (columnInfo.isInherit()) {
                    column[2] = columnInfo.getInheritedTable();//because we cannot get column's inherit table
                }
                columnModel.addRow(column);
            }
        }

        List<ConstraintInfoDTO> constraintList = tableInfo.getConstraintInfoList();
        if (constraintList != null) {
            DefaultTableModel constrModel = (DefaultTableModel) tblConstraints.getModel();
            for (ConstraintInfoDTO constraintInfo : constraintList) {
                Object[] constraint = new Object[3];
                constraint[0] = constraintInfo.getType();
                constraint[1] = constraintInfo;
                constraint[2] = tc.getPartContraintDefine(constraintInfo);
                constrModel.addRow(constraint);
            }
        }

        AutoVacuumDTO autoVacuum = tableInfo.getAutoVacuumInfo();
        if (autoVacuum != null) {
            if (autoVacuum.isCustomAutoVacuum()) {
                cbCustomAutoVacuum.setSelected(true);
                cbCustomAutoVacuum.setEnabled(false);
                cbEnable.setSelected(autoVacuum.isEnable());
                cbEnable.setEnabled(false);
                txfdVacuumBaseThreshold.setText(autoVacuum.getVacuumBaseThreshold());
                txfdAnalyzeBaseThreshold.setText(autoVacuum.getAnalyzeBaseThreshold());
                txfdVacuumScaleFactor.setText(autoVacuum.getVacuumScaleFactor());
                txfdAnalyzeScaleFactor.setText(autoVacuum.getAnalyzeScaleFactor());
                txfdVacuumCostDelay.setText(autoVacuum.getVacuumCostDelay());
                txfdVacuumCostLimit.setText(autoVacuum.getVacuumCostLimit());
                txfdFreezeMinimumAge.setText(autoVacuum.getFreezeMinimumAge());
                txfdFreezeMaximumAge.setText(autoVacuum.getFreezeMaximumAge());
                txfdFreezeTableAge.setText(autoVacuum.getFreezeTableAge());
            }
            if (autoVacuum.isCustomAutoVacuumToast()) {
                cbCustomAutoVacuumToast.setSelected(true);
                cbCustomAutoVacuumToast.setEnabled(false);
                cbEnableToast.setSelected(autoVacuum.isEnable());
                cbEnableToast.setEnabled(false);
                txfdVacuumBaseThresholdToast.setText(autoVacuum.getVacuumBaseThresholdToast());
                txfdVacuumScaleFactorToast.setText(autoVacuum.getVacuumScaleFactorToast());
                txfdVacuumCostDelayToast.setText(autoVacuum.getVacuumCostDelayToast());
                txfdVacuumCostLimitToast.setText(autoVacuum.getVacuumCostLimitToast());
                txfdFreezeMinimumAgeToast.setText(autoVacuum.getFreezeMinimumAgeToast());
                txfdFreezeMaximumAgeToast.setText(autoVacuum.getFreezeMaximumAgeToast());
                txfdFreezeTableAgeToast.setText(autoVacuum.getFreezeTableAgeToast());
            }
        }

        String acl = tableInfo.getAcl();
        if (acl != null && !acl.isEmpty() && !acl.equals("{}")) {
            DefaultTableModel sModel = (DefaultTableModel) tblPrivileges.getModel();
            for (int i = sModel.getRowCount() - 1; i >= 0; i--) {
                sModel.removeRow(i);
            }
            acl = acl.substring(1, acl.length() - 1);
            String[] privilegeArray = acl.split(",");
            for (String privilege : privilegeArray) {
                logger.info("privilege:" + privilege);
                String[] pArray = privilege.split("=");
                String[] p = new String[2];
                if (pArray[0] == null || pArray[0].isEmpty()) {
                    p[0] = "public";
                } else {
                    p[0] = pArray[0];
                }
                p[1] = pArray[1].split("/")[0];
                sModel.addRow(p);
            }
        }
        
        //partition
        cbPartitioned.setSelected(tableInfo.isPartitioned());
        cbPartitioned.setEnabled(false);
        if (tableInfo.isPartitioned())
        {
            logger.debug("tableInfo.getPartitionSchema()=====" + tableInfo.getPartitionSchema());
            String pschema = tableInfo.getPartitionSchema().contains("RANGE")? "RANGE":
                    (tableInfo.getPartitionSchema().contains("LIST")? "LIST" : "HASH");
            partitionTypeComboBox.setSelectedItem(pschema);
            partitionTypeComboBox.setEnabled(false);
            btnAddPartitionKey.setEnabled(false);
            btnRemovePartitionKey.setEnabled(false);
            lblPartitionKeys.setText(tableInfo.getPartitionSchema());
          
            btnAddPartition.setEnabled(true);
            btnRemovePartition.setEnabled(true);
            partitionTableModel.setColumnEditable(0, true);
            partitionTableModel.setColumnEditable(1, true);//default
            partitionTableModel.setColumnEditable(2, true);//"RANGE".equals(tableInfo.getPartitionSchema())
            partitionTableModel.setColumnEditable(3, true);
            partitionTableModel.setColumnEditable(4, true);
            PartitionInfoDTO p;
            for (int i=0; i<tableInfo.getPartitionList().size(); i++)
            {   
                p = tableInfo.getPartitionList().get(i);
                partitionTableModel.addRow(new Object[]{p.getName(), p.isDefaultValue(), p.getFromValue(), p.getToValue()
                        , p.getInValue(), p.getModulusValue(), p.getRemainderValue() });
            }            
        }
       
        tpDefinitionSQL.setText("");
        SyntaxController sc = SyntaxController.getInstance();
        sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        lblSchema = new javax.swing.JLabel();
        cbbSchema = new javax.swing.JComboBox();
        lblUseSlony = new javax.swing.JLabel();
        cbbUseSlony = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        lblPartitionedTable = new javax.swing.JLabel();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(32767, 32767));
        cbPartitioned = new javax.swing.JCheckBox();
        pnlDefinition = new javax.swing.JPanel();
        lblTablesapce = new javax.swing.JLabel();
        lblOfType = new javax.swing.JLabel();
        lblFillFactor = new javax.swing.JLabel();
        lblHasOIDs = new javax.swing.JLabel();
        txfdFillFactor = new javax.swing.JTextField();
        cbbTablespace = new javax.swing.JComboBox();
        cbbOfTypeTable = new javax.swing.JComboBox();
        cbHasOIDs = new javax.swing.JCheckBox();
        lblUnlogged = new javax.swing.JLabel();
        cbUnlogged = new javax.swing.JCheckBox();
        pnlInherits = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblTableInherits = new javax.swing.JTable();
        btnAddInherits = new javax.swing.JButton();
        btnRemoveInherits = new javax.swing.JButton();
        cbbTableInherits = new javax.swing.JComboBox();
        pnlLike = new javax.swing.JPanel();
        lblRelation = new javax.swing.JLabel();
        lblIncluding = new javax.swing.JLabel();
        cbbLikeRelation = new javax.swing.JComboBox();
        cbDefaultValues = new javax.swing.JCheckBox();
        cbConstraints = new javax.swing.JCheckBox();
        cbIndexes = new javax.swing.JCheckBox();
        cbStorage = new javax.swing.JCheckBox();
        cbComments = new javax.swing.JCheckBox();
        pnlColumns = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblColumns = new javax.swing.JTable();
        btnAddColumn = new javax.swing.JButton();
        btnRemoveColumn = new javax.swing.JButton();
        btnChangeColumn = new javax.swing.JButton();
        pnlConstraints = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblConstraints = new javax.swing.JTable();
        btnAddConstraint = new javax.swing.JButton();
        btnRemoveConstraint = new javax.swing.JButton();
        cbbConstraint = new javax.swing.JComboBox();
        pnlAutoVacuum = new javax.swing.JPanel();
        jtpAutoVacuum = new javax.swing.JTabbedPane();
        pnlTable = new javax.swing.JPanel();
        cbCustomAutoVacuum = new javax.swing.JCheckBox();
        lblEnable = new javax.swing.JLabel();
        cbEnable = new javax.swing.JCheckBox();
        txfdVacuumBaseThreshold = new javax.swing.JTextField();
        lblVacuumBaseThreshold = new javax.swing.JLabel();
        lblCurrentValue = new javax.swing.JLabel();
        lblAnalyzeBaseThreshold = new javax.swing.JLabel();
        txfdAnalyzeBaseThreshold = new javax.swing.JTextField();
        lblVacuumScaleFactor = new javax.swing.JLabel();
        lblVacuumBaseThresholdValue = new javax.swing.JLabel();
        txfdVacuumScaleFactor = new javax.swing.JTextField();
        lblAnalyzeScaleFactor = new javax.swing.JLabel();
        txfdAnalyzeScaleFactor = new javax.swing.JTextField();
        lblAnalyzeScaleFactorValue = new javax.swing.JLabel();
        txfdVacuumCostDelay = new javax.swing.JTextField();
        lblVacuumCostDelayValue = new javax.swing.JLabel();
        lblVacuumCostDelay = new javax.swing.JLabel();
        lblVacuumCostLimitValue = new javax.swing.JLabel();
        txfdVacuumCostLimit = new javax.swing.JTextField();
        lblVacuumCostLimit = new javax.swing.JLabel();
        lblFreezeMinimumAge = new javax.swing.JLabel();
        txfdFreezeMinimumAge = new javax.swing.JTextField();
        lblFreezeMinimumAgeValue = new javax.swing.JLabel();
        txfdFreezeMaximumAge = new javax.swing.JTextField();
        lblFreezeMaximumAge = new javax.swing.JLabel();
        lblFreezeMaximumAgeValue = new javax.swing.JLabel();
        lblFreezeTableAge = new javax.swing.JLabel();
        txfdFreezeTableAge = new javax.swing.JTextField();
        lblFreezeTableAgeValue = new javax.swing.JLabel();
        lblAnalyzeBaseThresholdValue = new javax.swing.JLabel();
        lblVacuumScaleFactorValue = new javax.swing.JLabel();
        pnlTableToast = new javax.swing.JPanel();
        cbCustomAutoVacuumToast = new javax.swing.JCheckBox();
        lblEnableToast = new javax.swing.JLabel();
        cbEnableToast = new javax.swing.JCheckBox();
        txfdVacuumBaseThresholdToast = new javax.swing.JTextField();
        lblVacuumBaseThresholdToast = new javax.swing.JLabel();
        lblCurrentValue2 = new javax.swing.JLabel();
        lblVacuumScaleFactorToast = new javax.swing.JLabel();
        lblVacuumBaseThresholdValue1 = new javax.swing.JLabel();
        txfdVacuumScaleFactorToast = new javax.swing.JTextField();
        txfdVacuumCostDelayToast = new javax.swing.JTextField();
        lblVacuumCostDelayValue1 = new javax.swing.JLabel();
        lblVacuumCostDelayToast = new javax.swing.JLabel();
        lblVacuumCostLimitValue1 = new javax.swing.JLabel();
        txfdVacuumCostLimitToast = new javax.swing.JTextField();
        lblVacuumCostLimitToast = new javax.swing.JLabel();
        lblFreezeMinimumAgeToast = new javax.swing.JLabel();
        txfdFreezeMinimumAgeToast = new javax.swing.JTextField();
        lblFreezeMinimumAgeValue1 = new javax.swing.JLabel();
        txfdFreezeMaximumAgeToast = new javax.swing.JTextField();
        lblFreezeMaximumAgeToast = new javax.swing.JLabel();
        lblFreezeMaximumAgeValue1 = new javax.swing.JLabel();
        lblFreezeTableAgeToast = new javax.swing.JLabel();
        txfdFreezeTableAgeToast = new javax.swing.JTextField();
        lblFreezeTableAgeValue1 = new javax.swing.JLabel();
        lblVacuumScaleFactorValue1 = new javax.swing.JLabel();
        pnlPrivileges = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPrivileges = new javax.swing.JTable();
        btnAddChangePrivilege = new javax.swing.JButton();
        btnRemovePrivilege = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRole = new javax.swing.JLabel();
        cbbRole = new javax.swing.JComboBox();
        cbAll = new javax.swing.JCheckBox();
        cbUpdate = new javax.swing.JCheckBox();
        cbDelete = new javax.swing.JCheckBox();
        cbOptionAll = new javax.swing.JCheckBox();
        cbTruncate = new javax.swing.JCheckBox();
        cbOptionUpdate = new javax.swing.JCheckBox();
        cbOptionDelete = new javax.swing.JCheckBox();
        cbOptionTruncate = new javax.swing.JCheckBox();
        cbInsert = new javax.swing.JCheckBox();
        cbOptionInsert = new javax.swing.JCheckBox();
        cbSelect = new javax.swing.JCheckBox();
        cbOptionSelect = new javax.swing.JCheckBox();
        cbRule = new javax.swing.JCheckBox();
        cbOptionRule = new javax.swing.JCheckBox();
        cbOptionReferences = new javax.swing.JCheckBox();
        cbReferences = new javax.swing.JCheckBox();
        cbTrigger = new javax.swing.JCheckBox();
        cbOptionTrigger = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabel = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlPartition = new javax.swing.JPanel();
        partitionTypeLabel = new javax.swing.JLabel();
        partitionTypeComboBox = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabPartitionKey = new javax.swing.JTable();
        partitionKeysLabel = new javax.swing.JLabel();
        btnAddPartitionKey = new javax.swing.JButton();
        PatitionsLabel = new javax.swing.JLabel();
        btnAddPartition = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        tabPartitions = new javax.swing.JTable();
        btnRemovePartitionKey = new javax.swing.JButton();
        btnRemovePartition = new javax.swing.JButton();
        lblPartitionKeys = new javax.swing.JLabel();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addTable"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/table.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

lblSchema.setText(constBundle.getString("schema"));

cbbSchema.setEnabled(false);

lblUseSlony.setText(constBundle.getString("useSlony"));

cbbUseSlony.setEnabled(false);

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane1.setViewportView(txarComment);

lblPartitionedTable.setText(constBundle.getString("partition_table"));

cbPartitioned.setBackground(new java.awt.Color(255, 255, 255));

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdOID)
                    .addComponent(txfdName)))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSchema, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbbSchema, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbbOwner, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbbUseSlony, 0, 520, Short.MAX_VALUE)))
        .addContainerGap())
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addContainerGap()
        .addComponent(lblPartitionedTable, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(cbPartitioned)
        .addGap(48, 48, 48)
        .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblSchema)
                .addComponent(cbbSchema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblPartitionedTable)
                        .addComponent(cbPartitioned)))
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addGap(28, 28, 28)
                    .addComponent(filler1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(18, 18, 18)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 195, Short.MAX_VALUE))
                .addComponent(jScrollPane1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblUseSlony)
                .addComponent(cbbUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(520, 470));

    lblTablesapce.setText(constBundle.getString("tablespace"));

    lblOfType.setText(constBundle.getString("ofType"));

    lblFillFactor.setText(constBundle.getString("fillFactor"));

    lblHasOIDs.setText(constBundle.getString("hasOIDs"));

    cbbTablespace.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<"+constBundle.getString("defaultTablespace")+">", "pg_default" }));

    cbbOfTypeTable.setEditable(true);
    cbbOfTypeTable.setModel(new javax.swing.DefaultComboBoxModel(new String[] { ""}));
    cbbOfTypeTable.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbOfTypeTableItemStateChanged(evt);
        }
    });

    cbHasOIDs.setBackground(new java.awt.Color(255, 255, 255));

    lblUnlogged.setText(constBundle.getString("unlogged"));

    cbUnlogged.setBackground(new java.awt.Color(255, 255, 255));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblHasOIDs, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cbHasOIDs))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblUnlogged, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cbUnlogged)))
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblTablesapce, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cbbTablespace, 0, 520, Short.MAX_VALUE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblOfType, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cbbOfTypeTable, 0, 520, Short.MAX_VALUE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(txfdFillFactor, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblTablesapce)
                .addComponent(cbbTablespace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbbOfTypeTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOfType))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblFillFactor)
                .addComponent(txfdFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblHasOIDs)
                .addComponent(cbHasOIDs))
            .addGap(10, 10, 10)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbUnlogged)
                .addComponent(lblUnlogged))
            .addContainerGap(315, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlInherits.setBackground(new java.awt.Color(255, 255, 255));
    pnlInherits.setPreferredSize(new java.awt.Dimension(520, 500));

    tblTableInherits.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            ""
        }
    ));
    jScrollPane5.setViewportView(tblTableInherits);

    btnAddInherits.setText(constBundle.getString("add"));
    btnAddInherits.setEnabled(false);
    btnAddInherits.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddInherits.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddInherits.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddInherits.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddInheritsActionPerformed(evt);
        }
    });

    btnRemoveInherits.setText(constBundle.getString("remove"));
    btnRemoveInherits.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveInherits.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveInherits.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveInherits.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveInheritsActionPerformed(evt);
        }
    });

    cbbTableInherits.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbTableInheritsItemStateChanged(evt);
        }
    });

    javax.swing.GroupLayout pnlInheritsLayout = new javax.swing.GroupLayout(pnlInherits);
    pnlInherits.setLayout(pnlInheritsLayout);
    pnlInheritsLayout.setHorizontalGroup(
        pnlInheritsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlInheritsLayout.createSequentialGroup()
            .addGroup(pnlInheritsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInheritsLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(cbbTableInherits, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInheritsLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlInheritsLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(btnAddInherits, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnRemoveInherits, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );
    pnlInheritsLayout.setVerticalGroup(
        pnlInheritsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlInheritsLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbbTableInherits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlInheritsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddInherits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveInherits, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("inherits"), pnlInherits);
    pnlInherits.getAccessibleContext().setAccessibleName("");

    pnlLike.setBackground(new java.awt.Color(255, 255, 255));
    pnlLike.setToolTipText(constBundle.getString("constraints"));
    pnlLike.setMinimumSize(new java.awt.Dimension(520, 470));

    lblRelation.setText(constBundle.getString("relation"));

    lblIncluding.setText(constBundle.getString("including"));

    cbbTablespace.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<"+constBundle.getString("defaultTablespace")+">", "pg_default" }));

    cbDefaultValues.setBackground(new java.awt.Color(255, 255, 255));
    cbDefaultValues.setText(constBundle.getString("defaultValues"));

    cbConstraints.setBackground(new java.awt.Color(255, 255, 255));
    cbConstraints.setText(constBundle.getString("constraints"));

    cbIndexes.setBackground(new java.awt.Color(255, 255, 255));
    cbIndexes.setText(constBundle.getString("indexes"));

    cbStorage.setBackground(new java.awt.Color(255, 255, 255));
    cbStorage.setText(constBundle.getString("storage"));

    cbComments.setBackground(new java.awt.Color(255, 255, 255));
    cbComments.setText(constBundle.getString("comments"));

    javax.swing.GroupLayout pnlLikeLayout = new javax.swing.GroupLayout(pnlLike);
    pnlLike.setLayout(pnlLikeLayout);
    pnlLikeLayout.setHorizontalGroup(
        pnlLikeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlLikeLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlLikeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlLikeLayout.createSequentialGroup()
                    .addComponent(lblRelation, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cbbLikeRelation, 0, 520, Short.MAX_VALUE))
                .addGroup(pnlLikeLayout.createSequentialGroup()
                    .addComponent(lblIncluding, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlLikeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbConstraints)
                        .addComponent(cbDefaultValues)
                        .addComponent(cbIndexes)
                        .addComponent(cbStorage)
                        .addComponent(cbComments))
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlLikeLayout.setVerticalGroup(
        pnlLikeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlLikeLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlLikeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblRelation)
                .addComponent(cbbLikeRelation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlLikeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblIncluding)
                .addComponent(cbDefaultValues))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(cbConstraints)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(cbIndexes)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(cbStorage)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(cbComments)
            .addContainerGap(314, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("like"), pnlLike);

    pnlColumns.setBackground(new java.awt.Color(255, 255, 255));
    pnlColumns.setPreferredSize(new java.awt.Dimension(520, 500));

    tblColumns.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", "", "", "", "", "", "", "", "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false, false, true, false, false, false, false, false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    jScrollPane6.setViewportView(tblColumns);
    tblColumns.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("columnName"));
    tblColumns.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("definition"));
    tblColumns.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("inheritedFromTable"));
    tblColumns.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("columnDefination"));
    tblColumns.getColumnModel().getColumn(4).setHeaderValue(constBundle.getString("columnComment"));
    tblColumns.getColumnModel().getColumn(5).setHeaderValue(constBundle.getString("columnStatistics"));
    tblColumns.getColumnModel().getColumn(6).setHeaderValue(constBundle.getString("column"));
    tblColumns.getColumnModel().getColumn(7).setHeaderValue(constBundle.getString("columnTypeOid"));
    tblColumns.getColumnModel().getColumn(8).setHeaderValue(constBundle.getString("changedColumn"));
    tblColumns.getColumnModel().getColumn(9).setHeaderValue(null);

    btnAddColumn.setText(constBundle.getString("add"));
    btnAddColumn.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddColumn.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddColumn.setPreferredSize(new java.awt.Dimension(80, 23));

    btnRemoveColumn.setText(constBundle.getString("remove"));
    btnRemoveColumn.setEnabled(false);
    btnRemoveColumn.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.setPreferredSize(new java.awt.Dimension(80, 23));

    btnChangeColumn.setText(constBundle.getString("change"));
    btnChangeColumn.setEnabled(false);

    javax.swing.GroupLayout pnlColumnsLayout = new javax.swing.GroupLayout(pnlColumns);
    pnlColumns.setLayout(pnlColumnsLayout);
    pnlColumnsLayout.setHorizontalGroup(
        pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(btnChangeColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(btnAddColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );
    pnlColumnsLayout.setVerticalGroup(
        pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnChangeColumn))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("columns"), pnlColumns);

    pnlConstraints.setBackground(new java.awt.Color(255, 255, 255));
    pnlConstraints.setPreferredSize(new java.awt.Dimension(520, 500));

    tblConstraints.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", "", "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false, false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    jScrollPane7.setViewportView(tblConstraints);
    if (tblConstraints.getColumnModel().getColumnCount() > 0)
    {
        tblConstraints.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("type"));
        tblConstraints.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("constraintName"));
        tblConstraints.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("definition"));
        tblConstraints.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("comment"));
    }

    btnAddConstraint.setText(constBundle.getString("add"));
    btnAddConstraint.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddConstraint.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddConstraint.setPreferredSize(new java.awt.Dimension(80, 23));

    btnRemoveConstraint.setText(constBundle.getString("remove"));
    btnRemoveConstraint.setEnabled(false);
    btnRemoveConstraint.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveConstraint.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveConstraint.setPreferredSize(new java.awt.Dimension(80, 23));

    cbbConstraint.setModel(new javax.swing.DefaultComboBoxModel(new String[] {constBundle.getString("primaryKey"),constBundle.getString("foreignKey"),constBundle.getString("exclude"),constBundle.getString("unique"),constBundle.getString("check")}));

    javax.swing.GroupLayout pnlConstraintsLayout = new javax.swing.GroupLayout(pnlConstraints);
    pnlConstraints.setLayout(pnlConstraintsLayout);
    pnlConstraintsLayout.setHorizontalGroup(
        pnlConstraintsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlConstraintsLayout.createSequentialGroup()
            .addGroup(pnlConstraintsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlConstraintsLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlConstraintsLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(cbbConstraint, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnAddConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(btnRemoveConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );
    pnlConstraintsLayout.setVerticalGroup(
        pnlConstraintsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlConstraintsLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlConstraintsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbbConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("constraints"), pnlConstraints);

    pnlAutoVacuum.setBackground(new java.awt.Color(255, 255, 255));
    pnlAutoVacuum.setMinimumSize(new java.awt.Dimension(520, 470));

    pnlTable.setBackground(new java.awt.Color(255, 255, 255));

    cbCustomAutoVacuum.setBackground(new java.awt.Color(255, 255, 255));
    cbCustomAutoVacuum.setText(constBundle.getString("customAutoVacuum"));
    cbCustomAutoVacuum.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbCustomAutoVacuumActionPerformed(evt);
        }
    });

    lblEnable.setText(constBundle.getString("enable"));

    cbEnable.setBackground(new java.awt.Color(255, 255, 255));
    cbEnable.setSelected(true);
    cbEnable.setEnabled(false);

    txfdVacuumBaseThreshold.setEnabled(false);

    lblVacuumBaseThreshold.setText(constBundle.getString("vacuumBaseThreshold"));

    lblCurrentValue.setText(constBundle.getString("currentValue"));

    lblAnalyzeBaseThreshold.setText(constBundle.getString("analyzeBaseThreshold"));

    txfdAnalyzeBaseThreshold.setEnabled(false);

    lblVacuumScaleFactor.setText(constBundle.getString("vacuumScaleFactor"));

    lblVacuumBaseThresholdValue.setText("50");

    txfdVacuumScaleFactor.setEnabled(false);

    lblAnalyzeScaleFactor.setText(constBundle.getString("analyzeScaleFactor"));

    txfdAnalyzeScaleFactor.setEnabled(false);

    lblAnalyzeScaleFactorValue.setText("0.1");

    txfdVacuumCostDelay.setEnabled(false);

    lblVacuumCostDelayValue.setText("20");

    lblVacuumCostDelay.setText(constBundle.getString("vacuumCostDelay"));

    lblVacuumCostLimitValue.setText("200");

    txfdVacuumCostLimit.setEnabled(false);

    lblVacuumCostLimit.setText("OID");

    lblFreezeMinimumAge.setText(constBundle.getString("freezeMinimumAge"));

    txfdFreezeMinimumAge.setEnabled(false);

    lblFreezeMinimumAgeValue.setText("50000000");

    txfdFreezeMaximumAge.setEnabled(false);

    lblFreezeMaximumAge.setText(constBundle.getString("freezeMaximumAge"));

    lblFreezeMaximumAgeValue.setText("200000000");

    lblFreezeTableAge.setText(constBundle.getString("freezeTableAge"));

    txfdFreezeTableAge.setEnabled(false);

    lblFreezeTableAgeValue.setText("150000000");

    lblAnalyzeBaseThresholdValue.setText("50");

    lblVacuumScaleFactorValue.setText("0.2");

    javax.swing.GroupLayout pnlTableLayout = new javax.swing.GroupLayout(pnlTable);
    pnlTable.setLayout(pnlTableLayout);
    pnlTableLayout.setHorizontalGroup(
        pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTableLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                .addComponent(lblFreezeMaximumAge, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMinimumAge, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostLimit, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostDelay, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAnalyzeScaleFactor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumScaleFactor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAnalyzeBaseThreshold, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumBaseThreshold, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblEnable, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbCustomAutoVacuum, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addComponent(lblFreezeTableAge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbEnable)
                .addComponent(txfdVacuumBaseThreshold)
                .addComponent(txfdAnalyzeBaseThreshold)
                .addComponent(txfdVacuumScaleFactor)
                .addComponent(txfdAnalyzeScaleFactor)
                .addComponent(txfdVacuumCostDelay)
                .addComponent(txfdVacuumCostLimit)
                .addComponent(txfdFreezeMinimumAge)
                .addComponent(txfdFreezeMaximumAge)
                .addComponent(txfdFreezeTableAge, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(lblVacuumBaseThresholdValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAnalyzeBaseThresholdValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAnalyzeScaleFactorValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblVacuumCostDelayValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblVacuumCostLimitValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFreezeTableAgeValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFreezeMaximumAgeValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFreezeMinimumAgeValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblVacuumScaleFactorValue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addComponent(lblCurrentValue))
            .addContainerGap())
    );
    pnlTableLayout.setVerticalGroup(
        pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTableLayout.createSequentialGroup()
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlTableLayout.createSequentialGroup()
                    .addComponent(cbCustomAutoVacuum)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbEnable)
                        .addComponent(lblEnable)))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTableLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(lblCurrentValue)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumBaseThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumBaseThreshold)
                .addComponent(lblVacuumBaseThresholdValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdAnalyzeBaseThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblAnalyzeBaseThreshold)
                .addComponent(lblAnalyzeBaseThresholdValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumScaleFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumScaleFactor)
                .addComponent(lblVacuumScaleFactorValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdAnalyzeScaleFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblAnalyzeScaleFactor)
                .addComponent(lblAnalyzeScaleFactorValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostDelay)
                .addComponent(lblVacuumCostDelayValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostLimit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostLimit)
                .addComponent(lblVacuumCostLimitValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMinimumAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMinimumAge)
                .addComponent(lblFreezeMinimumAgeValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMaximumAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMaximumAge)
                .addComponent(lblFreezeMaximumAgeValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeTableAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeTableAge)
                .addComponent(lblFreezeTableAgeValue))
            .addContainerGap(52, Short.MAX_VALUE))
    );

    jtpAutoVacuum.addTab(constBundle.getString("dataTable"), pnlTable);

    pnlTableToast.setBackground(new java.awt.Color(255, 255, 255));

    cbCustomAutoVacuumToast.setBackground(new java.awt.Color(255, 255, 255));
    cbCustomAutoVacuumToast.setText(constBundle.getString("customAutoVacuum"));
    cbCustomAutoVacuumToast.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbCustomAutoVacuumToastActionPerformed(evt);
        }
    });

    lblEnableToast.setText(constBundle.getString("enable"));

    cbEnableToast.setBackground(new java.awt.Color(255, 255, 255));
    cbEnableToast.setSelected(true);
    cbEnableToast.setEnabled(false);

    txfdVacuumBaseThresholdToast.setEnabled(false);

    lblVacuumBaseThresholdToast.setText(constBundle.getString("vacuumBaseThreshold"));

    lblCurrentValue2.setText(constBundle.getString("currentValue"));

    lblVacuumScaleFactorToast.setText(constBundle.getString("vacuumScaleFactor"));

    lblVacuumBaseThresholdValue1.setText("50");

    txfdVacuumScaleFactorToast.setEnabled(false);

    txfdVacuumCostDelayToast.setEnabled(false);

    lblVacuumCostDelayValue1.setText("20");

    lblVacuumCostDelayToast.setText(constBundle.getString("vacuumCostDelay"));

    lblVacuumCostLimitValue1.setText("200");

    txfdVacuumCostLimitToast.setEnabled(false);

    lblVacuumCostLimitToast.setText("OID");

    lblFreezeMinimumAgeToast.setText(constBundle.getString("freezeMinimumAge"));

    txfdFreezeMinimumAgeToast.setEnabled(false);

    lblFreezeMinimumAgeValue1.setText("50000000");

    txfdFreezeMaximumAgeToast.setEnabled(false);

    lblFreezeMaximumAgeToast.setText(constBundle.getString("freezeMaximumAge"));

    lblFreezeMaximumAgeValue1.setText("200000000");

    lblFreezeTableAgeToast.setText(constBundle.getString("freezeTableAge"));

    txfdFreezeTableAgeToast.setEnabled(false);

    lblFreezeTableAgeValue1.setText("150000000");

    lblVacuumScaleFactorValue1.setText("0.2");

    javax.swing.GroupLayout pnlTableToastLayout = new javax.swing.GroupLayout(pnlTableToast);
    pnlTableToast.setLayout(pnlTableToastLayout);
    pnlTableToastLayout.setHorizontalGroup(
        pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTableToastLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lblFreezeTableAgeToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMaximumAgeToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMinimumAgeToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostLimitToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblEnableToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostDelayToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumScaleFactorToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumBaseThresholdToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbCustomAutoVacuumToast, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbEnableToast)
                .addComponent(txfdVacuumBaseThresholdToast)
                .addComponent(txfdVacuumScaleFactorToast)
                .addComponent(txfdVacuumCostDelayToast)
                .addComponent(txfdVacuumCostLimitToast)
                .addComponent(txfdFreezeMinimumAgeToast)
                .addComponent(txfdFreezeMaximumAgeToast)
                .addComponent(txfdFreezeTableAgeToast, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblVacuumBaseThresholdValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblCurrentValue2)
                .addComponent(lblVacuumScaleFactorValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostDelayValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostLimitValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMinimumAgeValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMaximumAgeValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeTableAgeValue1, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );
    pnlTableToastLayout.setVerticalGroup(
        pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTableToastLayout.createSequentialGroup()
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlTableToastLayout.createSequentialGroup()
                    .addComponent(cbCustomAutoVacuumToast)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbEnableToast)
                        .addComponent(lblEnableToast)))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTableToastLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(lblCurrentValue2)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumBaseThresholdToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumBaseThresholdToast)
                .addComponent(lblVacuumBaseThresholdValue1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVacuumScaleFactorToast)
                .addComponent(txfdVacuumScaleFactorToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumScaleFactorValue1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostDelayToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostDelayToast)
                .addComponent(lblVacuumCostDelayValue1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostLimitToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostLimitToast)
                .addComponent(lblVacuumCostLimitValue1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMinimumAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMinimumAgeToast)
                .addComponent(lblFreezeMinimumAgeValue1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMaximumAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMaximumAgeToast)
                .addComponent(lblFreezeMaximumAgeValue1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlTableToastLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeTableAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeTableAgeValue1)
                .addComponent(lblFreezeTableAgeToast))
            .addContainerGap())
    );

    jtpAutoVacuum.addTab(constBundle.getString("toastTable"), pnlTableToast);

    javax.swing.GroupLayout pnlAutoVacuumLayout = new javax.swing.GroupLayout(pnlAutoVacuum);
    pnlAutoVacuum.setLayout(pnlAutoVacuumLayout);
    pnlAutoVacuumLayout.setHorizontalGroup(
        pnlAutoVacuumLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jtpAutoVacuum)
    );
    pnlAutoVacuumLayout.setVerticalGroup(
        pnlAutoVacuumLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jtpAutoVacuum)
    );

    jtp.addTab(constBundle.getString("autoVacuum"), pnlAutoVacuum);

    pnlPrivileges.setBackground(new java.awt.Color(255, 255, 255));
    pnlPrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlPrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

    tblPrivileges.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblPrivileges.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    jScrollPane4.setViewportView(tblPrivileges);
    if (tblPrivileges.getColumnModel().getColumnCount() > 0)
    {
        tblPrivileges.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("user")+"/"+constBundle.getString("group"));
        tblPrivileges.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("privileges"));
    }

    btnAddChangePrivilege.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));

    btnRemovePrivilege.setText(constBundle.getString("remove"));
    btnRemovePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));

    jPanel1.setBackground(new java.awt.Color(255, 255, 255));
    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("privileges"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("宋体", 0, 12), new java.awt.Color(204, 204, 204))); // NOI18N

    lblRole.setText(constBundle.getString("role"));

    cbAll.setBackground(new java.awt.Color(255, 255, 255));
    cbAll.setText("ALL");
    cbAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbAllActionPerformed(evt);
        }
    });

    cbUpdate.setBackground(new java.awt.Color(255, 255, 255));
    cbUpdate.setText("UPDATE");

    cbDelete.setBackground(new java.awt.Color(255, 255, 255));
    cbDelete.setText("DELETE");

    cbOptionAll.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionAll.setText("WITH GRANT OPTION");
    cbOptionAll.setEnabled(false);
    cbOptionAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbOptionAllActionPerformed(evt);
        }
    });

    cbTruncate.setBackground(new java.awt.Color(255, 255, 255));
    cbTruncate.setText("TRUNCATE");

    cbOptionUpdate.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionUpdate.setText("WITH GRANT OPTION");
    cbOptionUpdate.setEnabled(false);

    cbOptionDelete.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionDelete.setText("WITH GRANT OPTION");
    cbOptionDelete.setEnabled(false);

    cbOptionTruncate.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionTruncate.setText("WITH GRANT OPTION");
    cbOptionTruncate.setEnabled(false);

    cbInsert.setBackground(new java.awt.Color(255, 255, 255));
    cbInsert.setText("INSERT");

    cbOptionInsert.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionInsert.setText("WITH GRANT OPTION");
    cbOptionInsert.setEnabled(false);

    cbSelect.setBackground(new java.awt.Color(255, 255, 255));
    cbSelect.setText("SELECT");

    cbOptionSelect.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionSelect.setText("WITH GRANT OPTION");
    cbOptionSelect.setEnabled(false);

    cbRule.setBackground(new java.awt.Color(255, 255, 255));
    cbRule.setText("RULE");
    cbRule.setEnabled(false);

    cbOptionRule.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionRule.setText("WITH GRANT OPTION");
    cbOptionRule.setEnabled(false);

    cbOptionReferences.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionReferences.setText("WITH GRANT OPTION");
    cbOptionReferences.setEnabled(false);

    cbReferences.setBackground(new java.awt.Color(255, 255, 255));
    cbReferences.setText("REFERENCES");

    cbTrigger.setBackground(new java.awt.Color(255, 255, 255));
    cbTrigger.setText("TRIGGER");

    cbOptionTrigger.setBackground(new java.awt.Color(255, 255, 255));
    cbOptionTrigger.setText("WITH GRANT OPTION");
    cbOptionTrigger.setEnabled(false);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbRole, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbDelete)
                        .addComponent(cbUpdate)
                        .addComponent(cbAll)
                        .addComponent(cbInsert)
                        .addComponent(cbSelect)
                        .addComponent(cbTruncate)
                        .addComponent(cbRule)
                        .addComponent(cbReferences)
                        .addComponent(cbTrigger))
                    .addGap(153, 153, 153)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbOptionTrigger)
                        .addComponent(cbOptionRule)
                        .addComponent(cbOptionSelect)
                        .addComponent(cbOptionInsert)
                        .addComponent(cbOptionUpdate)
                        .addComponent(cbOptionDelete)
                        .addComponent(cbOptionAll)
                        .addComponent(cbOptionTruncate)
                        .addComponent(cbOptionReferences))
                    .addGap(0, 215, Short.MAX_VALUE)))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblRole))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbOptionAll)
                .addComponent(cbAll))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbInsert)
                .addComponent(cbOptionInsert))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbSelect)
                .addComponent(cbOptionSelect))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbOptionUpdate)
                .addComponent(cbUpdate))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbDelete)
                .addComponent(cbOptionDelete))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbTruncate)
                .addComponent(cbOptionTruncate))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbRule)
                .addComponent(cbOptionRule))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbReferences)
                .addComponent(cbOptionReferences))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbTrigger)
                .addComponent(cbOptionTrigger))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlPrivilegesLayout = new javax.swing.GroupLayout(pnlPrivileges);
    pnlPrivileges.setLayout(pnlPrivilegesLayout);
    pnlPrivilegesLayout.setHorizontalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jScrollPane4)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlPrivilegesLayout.createSequentialGroup()
                    .addComponent(btnAddChangePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlPrivilegesLayout.setVerticalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(btnAddChangePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("privileges"), pnlPrivileges);
    pnlPrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    lblProvider.setText(constBundle.getString("provider"));

    tblSecurityLabel.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    ));
    jScrollPane3.setViewportView(tblSecurityLabel);
    if (tblSecurityLabel.getColumnModel().getColumnCount() > 0)
    {
        tblSecurityLabel.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabel.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                    .addGap(10, 10, 10))))
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE)
                .addComponent(txfdProvider)))
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    partitionTypeLabel.setText(constBundle.getString("partition_type"));

    partitionTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "RANGE", "LIST", "HASH" }));

    tabPartitionKey.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "Key Type", "Column", "Expression"
        }
    ));
    tabPartitionKey.setRowHeight(20);
    jScrollPane2.setViewportView(tabPartitionKey);

    partitionKeysLabel.setText(constBundle.getString("partition_key"));

    btnAddPartitionKey.setText(constBundle.getString("partition_add"));

    PatitionsLabel.setText(constBundle.getString("partition_partition"));

    btnAddPartition.setText(constBundle.getString("partition_add"));

    tabPartitions.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "分区表名", "FROM", "TO", "IN", "modulus", "remainder"
        }
    ));
    tabPartitions.setRowHeight(20);
    jScrollPane8.setViewportView(tabPartitions);

    btnRemovePartitionKey.setText(constBundle.getString("partition_remove"));

    btnRemovePartition.setText(constBundle.getString("partition_remove"));

    javax.swing.GroupLayout pnlPartitionLayout = new javax.swing.GroupLayout(pnlPartition);
    pnlPartition.setLayout(pnlPartitionLayout);
    pnlPartitionLayout.setHorizontalGroup(
        pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPartitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                .addGroup(pnlPartitionLayout.createSequentialGroup()
                    .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(partitionTypeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(partitionKeysLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlPartitionLayout.createSequentialGroup()
                            .addComponent(btnAddPartitionKey)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnRemovePartitionKey)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(lblPartitionKeys, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addComponent(partitionTypeComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane8)
                .addGroup(pnlPartitionLayout.createSequentialGroup()
                    .addComponent(PatitionsLabel)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnAddPartition)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemovePartition)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlPartitionLayout.setVerticalGroup(
        pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPartitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(partitionTypeLabel)
                .addComponent(partitionTypeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblPartitionKeys, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(partitionKeysLabel)
                    .addComponent(btnAddPartitionKey)
                    .addComponent(btnRemovePartitionKey)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(13, 13, 13)
            .addGroup(pnlPartitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddPartition)
                .addComponent(btnRemovePartition)
                .addComponent(PatitionsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("partition_table"), pnlPartition);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 602, Short.MAX_VALUE)
                .addGroup(pnlSQLLayout.createSequentialGroup()
                    .addComponent(cbReadOnly)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.PREFERRED_SIZE, 381, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.PAGE_START);
    jtp.getAccessibleContext().setAccessibleName("SQL");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 372, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txfdIntegerValueKeyTyped(KeyEvent evt) {
        //those textfields could only be entered integer(>=0).
        char keyCh = evt.getKeyChar();
        Character.isDigit(keyCh);
        logger.info("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9')) {
            if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }
    }

    private void txfdFloatValueKeyTyped(KeyEvent evt) {
        //those textfields could only be entered integer(>=0).
        char keyCh = evt.getKeyChar();
        Character.isDigit(keyCh);
        logger.info("keych:" + keyCh);
        if (((keyCh < '0') || (keyCh > '9')) && (keyCh != '.')) {
            if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }
    }

    private void btnAddInheritsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddInheritsActionPerformed
    {//GEN-HEADEREND:event_btnAddInheritsActionPerformed
        // TODO add your handling code here:
        ObjItemInfoDTO tableIherits = (ObjItemInfoDTO) cbbTableInherits.getSelectedItem();
        if (tableIherits == null) {
            return;
        }
        logger.info("cbbTableInherits:" + tableIherits.toString());
        DefaultTableModel tableModel = (DefaultTableModel) tblTableInherits.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++) {
            logger.info("row = " + row);
            String tableIheritsOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld = " + tableIheritsOld);
            if (tableIheritsOld.equals(tableIherits.toString())) {
                isExist = true;
            }
            if (isExist) {
                break;
            }
        }
        if (!isExist) {
            ObjItemInfoDTO[] labels = new ObjItemInfoDTO[1];
            labels[0] = tableIherits;
            tableModel.addRow(labels);

            TreeController tc = TreeController.getInstance();
            DefaultTableModel columnsModel = (DefaultTableModel) tblColumns.getModel();
            try {
                String[][] inheritColumns = (String[][]) tc.getInheritTableColumns(helperInfo, tableIherits.getOid());//inherit
                for (String[] inheritColumnInfo : inheritColumns) {
                    columnsModel.addRow(inheritColumnInfo);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_btnAddInheritsActionPerformed

    private void btnRemoveInheritsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveInheritsActionPerformed
    {//GEN-HEADEREND:event_btnRemoveInheritsActionPerformed
        // TODO add your handling code here:
        logger.info("Enter");
        int selectedRow = tblTableInherits.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0) {
            return; //nothing select
        } else {
            int response = JOptionPane.showConfirmDialog(this, constBundle.getString("inheritTableRemoveQuestionMessage"),
                    constBundle.getString("whetherRemoveTable"), JOptionPane.YES_NO_OPTION);
            if (response == JOptionPane.YES_OPTION) {
                DefaultTableModel tableModel = (DefaultTableModel) tblTableInherits.getModel();
                String inheritTable = tableModel.getValueAt(selectedRow, 0).toString();
                tableModel.removeRow(selectedRow);

                DefaultTableModel columnsModel = (DefaultTableModel) tblColumns.getModel();
                for (int row = columnsModel.getRowCount() - 1; row >= 0; row--) {
                    Object value = columnsModel.getValueAt(row, 2);
                    if (value != null && value.toString().equals(inheritTable)) {
                        columnsModel.removeRow(row);
                    }
                }
            }
        }
    }//GEN-LAST:event_btnRemoveInheritsActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.TABLE);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void cbbTableInheritsItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbTableInheritsItemStateChanged
    {//GEN-HEADEREND:event_cbbTableInheritsItemStateChanged
        // TODO add your handling code here:
        logger.info("cbbUserName Selected Item:" + cbbTableInherits.getSelectedItem());
        if (cbbTableInherits.getSelectedItem() != null) {
            btnAddInherits.setEnabled(true);
        } else {
            btnAddInherits.setEnabled(false);
        }
    }//GEN-LAST:event_cbbTableInheritsItemStateChanged

    private void cbCustomAutoVacuumActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbCustomAutoVacuumActionPerformed
    {//GEN-HEADEREND:event_cbCustomAutoVacuumActionPerformed
        // TODO add your handling code here:
        boolean enable = cbCustomAutoVacuum.isSelected();
        logger.info("cbCustomAutoVacuum:" + enable);
        cbEnable.setEnabled(enable);
        txfdVacuumBaseThreshold.setEnabled(enable);
        txfdAnalyzeBaseThreshold.setEnabled(enable);
        txfdVacuumScaleFactor.setEnabled(enable);
        txfdAnalyzeScaleFactor.setEnabled(enable);
        txfdVacuumCostDelay.setEnabled(enable);
        txfdVacuumCostLimit.setEnabled(enable);
        txfdFreezeMinimumAge.setEnabled(enable);
        txfdFreezeMaximumAge.setEnabled(enable);
        txfdFreezeTableAge.setEnabled(enable);
    }//GEN-LAST:event_cbCustomAutoVacuumActionPerformed

    private void cbCustomAutoVacuumToastActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbCustomAutoVacuumToastActionPerformed
    {//GEN-HEADEREND:event_cbCustomAutoVacuumToastActionPerformed
        // TODO add your handling code here:
        boolean enable = cbCustomAutoVacuumToast.isSelected();
        logger.info("cbCustomAutoVacuumToast:" + enable);
        cbEnableToast.setEnabled(enable);
        txfdVacuumBaseThresholdToast.setEnabled(enable);
        txfdVacuumScaleFactorToast.setEnabled(enable);
        txfdVacuumCostDelayToast.setEnabled(enable);
        txfdVacuumCostLimitToast.setEnabled(enable);
        txfdFreezeMinimumAgeToast.setEnabled(enable);
        txfdFreezeMaximumAgeToast.setEnabled(enable);
        txfdFreezeTableAgeToast.setEnabled(enable);
    }//GEN-LAST:event_cbCustomAutoVacuumToastActionPerformed

    private void cbbOfTypeTableItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbOfTypeTableItemStateChanged
    {//GEN-HEADEREND:event_cbbOfTypeTableItemStateChanged
        // TODO add your handling code here:
        logger.info("cbbOfTypeTable selected index=" + cbbOfTypeTable.getSelectedIndex());
        boolean enable;
        if (cbbOfTypeTable.getSelectedIndex() < 0 || cbbOfTypeTable.getSelectedItem() == null
                || cbbOfTypeTable.getSelectedItem().toString().isEmpty()) {
            enable = true;
            this.emptyTable(tblColumns);
        } else {
            ObjItemInfoDTO ofTypeTable = (ObjItemInfoDTO) cbbOfTypeTable.getSelectedItem();
            logger.info("selectdOfTypeTable=" + ofTypeTable);
            enable = false;
            this.emptyTable(tblTableInherits);
            this.emptyTable(tblConstraints);
            this.emptyTable(tblColumns);
            TreeController tc = TreeController.getInstance();
            try {
                String[][] ofTypeColumns = (String[][]) tc.getOfTypeTableColumn(helperInfo, ofTypeTable.getOid());
                DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
                for (String[] column : ofTypeColumns) {
                    columnModel.addRow(column);
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
        tblTableInherits.setEnabled(enable);
        cbbTableInherits.setEnabled(enable);
        btnAddInherits.setEnabled(enable);
        btnRemoveInherits.setEnabled(enable);
        cbbLikeRelation.setEnabled(enable);
        cbDefaultValues.setEnabled(enable);
        cbConstraints.setEnabled(enable);
        cbIndexes.setEnabled(enable);
        cbStorage.setEnabled(enable);
        cbComments.setEnabled(enable);
        tblColumns.setEnabled(enable);
        btnAddColumn.setEnabled(enable);
        btnRemoveColumn.setEnabled(enable);
    }//GEN-LAST:event_cbbOfTypeTableItemStateChanged

    private void emptyTable(JTable tbl) {
        DefaultTableModel model = (DefaultTableModel) tbl.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    private void cbAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbAllActionPerformed
    {//GEN-HEADEREND:event_cbAllActionPerformed
        // TODO add your handling code here:
        boolean enable = cbAll.isSelected();
        logger.info("cbAll:" + enable);
        cbInsert.setSelected(enable);
        cbSelect.setSelected(enable);
        cbUpdate.setSelected(enable);
        cbDelete.setSelected(enable);
        cbTruncate.setSelected(enable);
        cbReferences.setSelected(enable);
        cbTrigger.setSelected(enable);

        cbInsert.setEnabled(!enable);
        cbSelect.setEnabled(!enable);
        cbUpdate.setEnabled(!enable);
        cbDelete.setEnabled(!enable);
        cbTruncate.setEnabled(!enable);
        cbReferences.setEnabled(!enable);
        cbTrigger.setEnabled(!enable);

        String role = cbbRole.getSelectedItem().toString();
        if (!role.equals("public")) {
            cbOptionAll.setEnabled(enable);
            cbOptionInsert.setEnabled(!enable);
            cbOptionSelect.setEnabled(!enable);
            cbOptionUpdate.setEnabled(!enable);
            cbOptionDelete.setEnabled(!enable);
            cbOptionTruncate.setEnabled(!enable);
            cbOptionReferences.setEnabled(!enable);
            cbOptionTrigger.setEnabled(!enable);
        }
        if (!enable) {
            cbOptionAll.setSelected(false);
        }
    }//GEN-LAST:event_cbAllActionPerformed

    private void cbOptionAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbOptionAllActionPerformed
    {//GEN-HEADEREND:event_cbOptionAllActionPerformed
        // TODO add your handling code here:
        boolean enable = cbOptionAll.isSelected();
        logger.info("cbbOptionAll:" + enable);
        cbOptionInsert.setSelected(enable);
        cbOptionSelect.setSelected(enable);
        cbOptionUpdate.setSelected(enable);
        cbOptionDelete.setSelected(enable);
        cbOptionTruncate.setSelected(enable);
        cbOptionReferences.setSelected(enable);
        cbOptionTrigger.setSelected(enable);
    }//GEN-LAST:event_cbOptionAllActionPerformed
    //column
    private void tblColumnsMousePressed(boolean ischange) {
        logger.info("mouse press column table");
        int row = tblColumns.getSelectedRow();
        logger.info("row = " + row);
        if (row < 0) {
            return; //nothing select
        }
        Object tableInherits = tblColumns.getValueAt(row, 2);
        if (tableInherits == null || tableInherits.toString().isEmpty()) {
            btnRemoveColumn.setEnabled(true);
            btnChangeColumn.setEnabled(true);
        } else {
            btnRemoveColumn.setEnabled(false);
        }
    }

    private void btnChangeColumnActionPerformed(ActionEvent evt) {
        int row = tblColumns.getSelectedRow();
        if(row<0)
        {
            return;
        }
        ColumnInfoDTO col = (ColumnInfoDTO) tblColumns.getValueAt(row, 0);
        String owner;
        if (cbbOwner.getSelectedItem() != null) {
            owner = cbbOwner.getSelectedItem().toString();
        } else if (tableInfo != null && tableInfo.getOwner() != null) {
            owner = tableInfo.getOwner();
        } else {
            owner = helperInfo.getUser();
        }
        ColumnView cav = new ColumnView(this, true, col, owner);
        cav.setLocationRelativeTo(this);
        cav.setVisible(true);
        if (cav.isSuccess()) {
            ColumnInfoDTO columnInfo = cav.getColumnInfo();
            String define = TreeController.getInstance().getPartColumnDefine(columnInfo);
            tblColumns.setValueAt(columnInfo, row, 0);
            tblColumns.setValueAt(define, row, 1);
        }
    }

    private void btnAddColumnActionPerformed(ActionEvent evt) {
        String owner;
        if (cbbOwner.getSelectedItem() != null) {
            owner = cbbOwner.getSelectedItem().toString();
        } else {
            if (tableInfo != null && tableInfo.getOwner() != null) {
                owner = tableInfo.getOwner();
            } else {
                owner = helperInfo.getUser();
            }
        }
        ColumnView cav = new ColumnView(this, true, helperInfo, owner);
        cav.setLocationRelativeTo(this);
        cav.setVisible(true);
        if (cav.isSuccess()) {
            ColumnInfoDTO columnInfo = cav.getColumnInfo();
            TreeController tc = TreeController.getInstance();
            String define = tc.getPartColumnDefine(columnInfo);
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            int row = tableModel.getRowCount();
            boolean isExist = false;
            String columnName = columnInfo.getName();
            for (int i = 0; i < row; i++) {
                logger.info("row = " + row);
                String cloumnNameOld = tableModel.getValueAt(i, 0).toString();
                logger.info("cloumnNameOld = " + cloumnNameOld + cloumnNameOld.equals(columnName));
                if (cloumnNameOld.equals(columnName)) {
                    tableModel.setValueAt(columnInfo, i, 0);
                    tableModel.setValueAt(define, i, 1);
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                Object[] p = new Object[10];
                p[0] = columnInfo;
                p[1] = define;
                //other property needed to add
                tableModel.addRow(p);
            }
        }
    }

    private void btnRemoveColumnActionPerformed(ActionEvent evt) {
        int selectedRow = tblColumns.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow >= 0) {
            JOptionPane.showMessageDialog(this, constBundle.getString("columnRemoveQuestionMessage"),
                    constBundle.getString("whetherRemoveColumn"), JOptionPane.QUESTION_MESSAGE);

            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            tableModel.removeRow(selectedRow);
        }
    }

    //constraint   
    private void tblConstraintsMousePressed(MouseEvent evt) {
        int row = tblConstraints.getSelectedRow();
        logger.info("selectedRow = " + row);
        btnRemoveConstraint.setEnabled((row >= 0));
    }

    private void btnAddConstraintActionPerformed(ActionEvent evt) {
        logger.info("constraintType:" + cbbConstraint.getSelectedItem());
        if (cbbConstraint.getSelectedItem() == null || cbbConstraint.getSelectedItem().toString().isEmpty()) {
            return;
        }
        DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
        ColItemInfoDTO[] columns = new ColItemInfoDTO[columnModel.getRowCount()];
        for (int i = 0; i < columnModel.getRowCount(); i++) {
            columns[i] = new ColItemInfoDTO();
            ColumnInfoDTO c = (ColumnInfoDTO) columnModel.getValueAt(i, 0);
            columns[i].setName(c.getName());//columnModel.getValueAt(i, 0).toString()
            columns[i].setTypeOid(c.getDatatype().getOid());
        }
        String constraintType = cbbConstraint.getSelectedItem().toString();
        TreeEnum.TreeNode enumType = null;
        if (constraintType.equals(constBundle.getString("primaryKey"))) {
            enumType = TreeEnum.TreeNode.PRIMARY_KEY;
        } else if (constraintType.equals(constBundle.getString("foreignKey"))) {
            enumType = TreeEnum.TreeNode.FOREIGN_KEY;
        } else if (constraintType.equals(constBundle.getString("unique"))) {
            enumType = TreeEnum.TreeNode.UNIQUE;
        } else if (constraintType.equals(constBundle.getString("check"))) {
            enumType = TreeEnum.TreeNode.CHECK;
        } else if (constraintType.equals(constBundle.getString("exclude"))) {
            enumType = TreeEnum.TreeNode.EXCLUDE;
        }
        ConstraintView cav = new ConstraintView(null, true, enumType, columns, helperInfo, true);
        cav.setLocationRelativeTo(this);
        cav.setVisible(true);
        if (cav.isSuccess()) {
            ConstraintInfoDTO constrInfo = cav.getConstrInfo();
            TreeController tc = TreeController.getInstance();
            constrInfo.setDefineSQL(constrInfo.getType().toString().replace("_", " ") + " " + tc.getPartContraintDefine(constrInfo));
            DefaultTableModel tableModel = (DefaultTableModel) tblConstraints.getModel();
            int row = tableModel.getRowCount();
            boolean isExist = false;
            for (int i = 0; i < row; i++) {
                logger.info("row = " + row);
                String type = tableModel.getValueAt(i, 0).toString();
                String name = tableModel.getValueAt(i, 1).toString();
                if (type.equals(constrInfo.getType().toString())
                        && name.equals(constrInfo.getName())) {
                    tableModel.setValueAt(constrInfo, i, 1);
                    tableModel.setValueAt(constrInfo.getDefineSQL(), i, 2);
                    tableModel.setValueAt(constrInfo.getComment(), i, 3);
                    isExist = true;
                    break;
                }
            }
            if (!isExist) {
                Object[] p = new Object[4];
                p[0] = constrInfo.getType().toString();
                p[1] = constrInfo;
                p[2] = constrInfo.getDefineSQL();
                p[3] = constrInfo.getComment();
                tableModel.addRow(p);
            }
        }
    }

    private void btnRemoveConstraintActionPerformed(ActionEvent evt) {
        int selectedRow = tblConstraints.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow >= 0) {
            JOptionPane.showMessageDialog(this, constBundle.getString("constraintRemoveQuestionMessage"),
                    constBundle.getString("whetherRemoveConstraint"), JOptionPane.QUESTION_MESSAGE);
            DefaultTableModel tableModel = (DefaultTableModel) tblConstraints.getModel();
            tableModel.removeRow(selectedRow);
        }
    }

    //privilege
    private void tblPrivilegesMousePressed(MouseEvent evt) {
        int row = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + row);
        btnRemovePrivilege.setEnabled((row >= 0));
    }

    private void btnAddChangePrivilegeActionPerformed(ActionEvent evt) {
        // TODO add your handling code here:
        String usergroup = cbbRole.getSelectedItem().toString();
        StringBuilder privileges = new StringBuilder();
        if (cbInsert.isSelected()) {
            privileges.append("a");
        }
        if (cbOptionInsert.isSelected()) {
            privileges.append("*");
        }
        if (cbSelect.isSelected()) {
            privileges.append("r");
        }
        if (cbOptionSelect.isSelected()) {
            privileges.append("*");
        }
        if (cbUpdate.isSelected()) {
            privileges.append("w");
        }
        if (cbOptionUpdate.isSelected()) {
            privileges.append("*");
        }
        if (cbDelete.isSelected()) {
            privileges.append("d");
        }
        if (cbOptionDelete.isSelected()) {
            privileges.append("*");
        }
        if (cbTruncate.isSelected()) {
            privileges.append("D");
        }
        if (cbOptionTruncate.isSelected()) {
            privileges.append("*");
        }
        if (cbReferences.isSelected()) {
            privileges.append("x");
        }
        if (cbOptionReferences.isSelected()) {
            privileges.append("*");
        }
        if (cbTrigger.isSelected()) {
            privileges.append("t");
        }
        if (cbOptionTrigger.isSelected()) {
            privileges.append("*");
        }

        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++) {
            logger.info("row = " + row);
            String usergroupOld = tableModel.getValueAt(i, 0).toString();
            logger.info("usergroupOld=" + usergroupOld + usergroupOld.equals(usergroup));
            if (usergroupOld.equals(usergroup)) {
                isExist = true;
                tableModel.setValueAt(privileges.toString(), i, 1);
            }
            if (isExist) {
                break;
            }
        }
        if (!isExist) {
            String[] p = new String[2];
            p[0] = usergroup;
            p[1] = privileges.toString();
            tableModel.addRow(p);
        }
    }

    private void btnRemovePrivilegeActionPerformed(ActionEvent evt) {
        int selectedRow = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0) {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        tableModel.removeRow(selectedRow);
    }

    //security label
    private void tblSecurityLabelMousePressed(MouseEvent evt) {
        int row = tblSecurityLabel.getSelectedRow();
        logger.info("swelectedRow = " + row);
        if (row < 0) {
            return; //nothing select
        }
        String user = tblSecurityLabel.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabel.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }

    private void btnRemoveSecurityLabelActionPerformed(ActionEvent evt) {
        int selectedRow = tblSecurityLabel.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0) {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabel.getModel();
        tableModel.removeRow(selectedRow);
    }

    private void btnAddChangeSecurityLabelActionPerformed(ActionEvent evt) {
        String provider = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:" + provider + ",label:" + label);
        if (provider == null || provider.equals("")) {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabel.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++) {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider)) {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist) {
                break;
            }
        }
        if (!isExist) {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }

    //for add
    private void btnOkEnableForAdd(KeyEvent evt) {
        String tabName = txfdName.getText();
        btnOK.setEnabled(tabName != null && !tabName.isEmpty());
    }

    private void jtpForAddStateChanged(ChangeEvent evt) {
        int index = jtp.getSelectedIndex();
        SyntaxController sc = SyntaxController.getInstance();
        logger.info("SelectedTabIndex:" + index);
        if (index == 10) {
            if (txfdName.getText() == null || txfdName.getText().isEmpty()) {
                tpDefinitionSQL.setText("");//clear
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
                return;
            }
            tpDefinitionSQL.setText("");//clear
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getTableInfo()));
        }
        if (index == 9) {
            enablePartitionTabButtons();
        }
    }

    private void btnOKForAddActionPerformed(ActionEvent evt) {
        logger.info(evt.getActionCommand());
        TreeController tc = TreeController.getInstance();
        TableInfoDTO tabInfo = this.getTableInfo();
        try {
            tc.createObj(helperInfo, tabInfo);
            isSuccess = true;
            this.dispose();
        } catch (Exception ex) {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    ex instanceof SQLWarning ? constBundle.getString("warning") : constBundle.getString("errorWarning"),
                    ex instanceof SQLWarning ? JOptionPane.WARNING_MESSAGE : JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }

    //for change
    private void btnOkEnableForChange() {
        String name = txfdName.getText();
        String acl = this.getACL();
        if (name == null || name.isEmpty()) {
            logger.info("name cannnot be empty");
            btnOK.setEnabled(false);
        } else if (!name.equals(tableInfo.getName())) {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (!cbbOwner.getSelectedItem().equals(tableInfo.getOwner())) {
            logger.info("owner changed");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(tableInfo.getComment())
                && !(txarComment.getText().isEmpty() && tableInfo.getComment() == null)) {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else if (!cbbTablespace.getSelectedItem().equals(tableInfo.getTablespace())) {
            logger.info("tablespace changed");
            btnOK.setEnabled(true);
        } else if (!txfdFillFactor.getText().trim().isEmpty()
                && !txfdFillFactor.getText().trim().equals(tableInfo.getFillFactor())) {
            logger.info("fillfactor changed");
            btnOK.setEnabled(true);
        } else if (cbHasOIDs.isSelected() != tableInfo.isHasOID()) {
            logger.info("hasOid changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalObjItemList(this.getInheritTables(), tableInfo.getInheritTableList())) {
            logger.info("inherit table changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalColumnList(this.getColumnList(), tableInfo.getColumnInfoList())) {
            logger.info("column changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalConstraintList(this.getConstraintList(), tableInfo.getConstraintInfoList())) {
            logger.info("constraint changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equal(this.getAutoVacuumInfo(), tableInfo.getAutoVacuumInfo())) {
            logger.info("autovacuum changed");
            btnOK.setEnabled(true);
        } else if ( !acl.isEmpty() && tableInfo.getAcl() != null && !acl.equals(tableInfo.getAcl())) {
            logger.info("acl changed");
            btnOK.setEnabled(true);
        } else if (tableInfo.isPartitioned())
        {
            logger.debug("current partition size=" + this.partitionTableModel.getRowCount());
            logger.debug("original partition size=" + tableInfo.getPartitionList().size());
            if ((tableInfo.getPartitionList() == null && this.partitionTableModel.getRowCount() > 0)
                    || (tableInfo.getPartitionList().size() != this.partitionTableModel.getRowCount())
                    || !Compare.getInstance().equalPartitionList(tableInfo.getPartitionList(), this.getPartitions()))
            {
                logger.info("partition changed");
                btnOK.setEnabled(true);
            }
        } else
        {
            logger.info("nothing changed");
            btnOK.setEnabled(false);
        }
    }
    
    private void jtpForChangeStateChanged(ChangeEvent evt) {
        int index = jtp.getSelectedIndex();
        SyntaxController sc = SyntaxController.getInstance();
        logger.info("SelectedTabIndex:" + index);
        if (index == 10) {
            if (!btnOK.isEnabled()) {
                tpDefinitionSQL.setText("");//clear
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
                return;
            }
            tpDefinitionSQL.setText("");//clear
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getTableChangeSQL(this.tableInfo, this.getTableInfo()));
        }
        if(index == 9) {
            if(!cbPartitioned.isSelected()) {
                logger.info("partitioned isn't selected");
                disablePartitionTabButtons();
            }
        }
    }

    private void btnOKForChangeActionPerformed(ActionEvent evt) {
        logger.info(evt.getActionCommand());
        try {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getDbName(),
                    tc.getTableChangeSQL(this.tableInfo, this.getTableInfo()));
            isSuccess = true;
            this.dispose();
        } catch (ClassNotFoundException | SQLException ex) {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    ex instanceof SQLWarning ? constBundle.getString("warning") : constBundle.getString("errorWarning"),
                    ex instanceof SQLWarning ? JOptionPane.WARNING_MESSAGE : JOptionPane.ERROR_MESSAGE);
            //ex.printStackTrace(System.out);
        }
    }

    private List<ObjItemInfoDTO> getInheritTables() {
        List<ObjItemInfoDTO> inheritTableList = new ArrayList();
        DefaultTableModel inheritModel = (DefaultTableModel) tblTableInherits.getModel();
        for (int i = 0; i < inheritModel.getRowCount(); i++) {
            logger.info("inheritTable=" + inheritModel.getValueAt(i, 0).toString());
            inheritTableList.add((ObjItemInfoDTO) inheritModel.getValueAt(i, 0));
        }
        return inheritTableList;
    }

    private List<ColumnInfoDTO> getColumnList() {
        List<ColumnInfoDTO> columnList = new ArrayList();
        DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
        for (int row = 0; row < columnModel.getRowCount(); row++) {
            //inherit table'column couldn't be added
            if (columnModel.getValueAt(row, 2) == null || columnModel.getValueAt(row, 2).toString().isEmpty()) {
                columnList.add((ColumnInfoDTO) columnModel.getValueAt(row, 0));
            }
        }
        return columnList;
    }

    private List<ConstraintInfoDTO> getConstraintList() {
        List<ConstraintInfoDTO> constraintList = new ArrayList();
        DefaultTableModel constraintModel = (DefaultTableModel) tblConstraints.getModel();
        for (int rowc = 0; rowc < constraintModel.getRowCount(); rowc++) {
            constraintList.add((ConstraintInfoDTO) constraintModel.getValueAt(rowc, 1));
        }
        logger.info("setConstrSize=" + constraintList.size());
        return constraintList;
    }

    private AutoVacuumDTO getAutoVacuumInfo() {
        if (cbCustomAutoVacuum.isSelected() || cbCustomAutoVacuumToast.isSelected()) {
            AutoVacuumDTO autoVacuum = new AutoVacuumDTO();
            //for auto vacuum
            autoVacuum.setCustomAutoVacuum(cbCustomAutoVacuum.isSelected());
            autoVacuum.setEnable(cbEnable.isSelected());
            //-1 or null is not useful
            autoVacuum.setVacuumBaseThreshold(txfdVacuumBaseThreshold.getText());
            autoVacuum.setAnalyzeBaseThreshold(txfdAnalyzeBaseThreshold.getText());
            autoVacuum.setVacuumScaleFactor(txfdVacuumScaleFactor.getText());
            autoVacuum.setAnalyzeScaleFactor(txfdAnalyzeScaleFactor.getText());
            autoVacuum.setVacuumCostDelay(txfdVacuumCostDelay.getText());
            autoVacuum.setVacuumCostLimit(txfdVacuumCostLimit.getText());
            autoVacuum.setFreezeMinimumAge(txfdFreezeMinimumAge.getText());
            autoVacuum.setFreezeMaximumAge(txfdFreezeMaximumAge.getText());
            autoVacuum.setFreezeTableAge(txfdFreezeTableAge.getText());
            //for toast auto vacuum 
            autoVacuum.setCustomAutoVacuumToast(cbCustomAutoVacuumToast.isSelected());
            autoVacuum.setEnableToast(cbEnableToast.isSelected());
            //-1 or null is not useful
            autoVacuum.setVacuumBaseThresholdToast(txfdVacuumBaseThresholdToast.getText());
            autoVacuum.setVacuumScaleFactorToast(txfdVacuumScaleFactorToast.getText());
            autoVacuum.setVacuumCostDelayToast(txfdVacuumCostDelayToast.getText());
            autoVacuum.setVacuumCostLimitToast(txfdVacuumCostLimitToast.getText());
            autoVacuum.setFreezeMinimumAgeToast(txfdFreezeMinimumAgeToast.getText());
            autoVacuum.setFreezeMaximumAgeToast(txfdFreezeMaximumAgeToast.getText());
            autoVacuum.setFreezeTableAgeToast(txfdFreezeTableAgeToast.getText());

            return autoVacuum;
        } else {
            return null;
        }
    }

    private String getACL() {
        return AclUtil.getACL(cbbOwner.getSelectedItem().toString(), helperInfo, tableInfo, (DefaultTableModel) tblPrivileges.getModel());
    }

    private  List<PartitionInfoDTO> getPartitions()
    {
        List<PartitionInfoDTO> parttions = new ArrayList();
        
        TableModel partitionModel = tabPartitions.getModel();
        PartitionInfoDTO prtt;
        for (int pr = 0; pr < partitionModel.getRowCount(); pr++)
        {
            prtt = new PartitionInfoDTO();
            helperInfo.setRelation(txfdName.getText()); //(newtab.getName());
            prtt.setHelperInfo(helperInfo);

            prtt.setName(partitionModel.getValueAt(pr, 0).toString());
            prtt.setPartitionSchema(partitionTypeComboBox.getSelectedItem().toString().toUpperCase());
            
            Object defaultValue = partitionModel.getValueAt(pr, 1);
            if (defaultValue != null && (Boolean.TRUE).equals(defaultValue))
            {
                prtt.setDefaultValue(true);
            } else if ("RANGE".equalsIgnoreCase(prtt.getPartitionSchema()))
            {
                prtt.setFromValue(partitionModel.getValueAt(pr, 2).toString());
                prtt.setToValue(partitionModel.getValueAt(pr, 3).toString());
            } else if ("LIST".equalsIgnoreCase(prtt.getPartitionSchema()))
            {
                prtt.setInValue(partitionModel.getValueAt(pr, 4).toString());
            } else //HASH
            {
                prtt.setModulusValue(partitionModel.getValueAt(pr, 5).toString());
                prtt.setRemainderValue(partitionModel.getValueAt(pr, 6).toString());
            }
            parttions.add(prtt);
        }
        
        logger.debug("partition size=" + parttions.size());
        return parttions;
    }
    
    public TableInfoDTO getTableInfo() {
        TableInfoDTO newtab = new TableInfoDTO();
        if (tableInfo != null) {
            newtab.setOid(tableInfo.getOid());
        }
        newtab.setHelperInfo(helperInfo);
        if (cbbSchema.getSelectedItem() != null && !cbbSchema.getSelectedItem().toString().isEmpty()) {
            newtab.setSchema(cbbSchema.getSelectedItem().toString());
            logger.info(newtab.getSchema());
        } else {
            newtab.setSchema(helperInfo.getSchema());
            newtab.setSchemaOid(helperInfo.getSchemaOid());
        }
        newtab.setName(txfdName.getText());
        if (cbbOwner.getSelectedItem() == null || cbbOwner.getSelectedItem().toString().isEmpty()) {
            newtab.setOwner(helperInfo.getUser());
        } else {
            newtab.setOwner(cbbOwner.getSelectedItem().toString());
        }
        newtab.setComment(txarComment.getText());

        newtab.setTablespace(cbbTablespace.getSelectedItem().toString());
        newtab.setFillFactor(txfdFillFactor.getText().trim());
        newtab.setHasOID(cbHasOIDs.isSelected());
        newtab.setUnlogged(cbUnlogged.isSelected());
        if (cbbOfTypeTable.getSelectedIndex() >= 0) {
            newtab.setOfType(cbbOfTypeTable.getSelectedItem().toString());
        }
        if (newtab.getOfType() == null || newtab.getOfType().isEmpty()) {
            newtab.setInheritTableList(this.getInheritTables());

            if (cbbLikeRelation.getSelectedIndex() >= 0) {
                newtab.setLikeRelation(cbbLikeRelation.getSelectedItem().toString());
            }
            newtab.setIncludeDefaultValue(cbDefaultValues.isSelected());
            newtab.setIncludeConstraint(cbConstraints.isSelected());
            newtab.setIncludeIndexes(cbIndexes.isSelected());
            newtab.setIncludeStorage(cbStorage.isSelected());
            newtab.setIncludeComments(cbComments.isSelected());

            newtab.setColumnInfoList(this.getColumnList());
        }
        newtab.setConstraintInfoList(this.getConstraintList());
        //autovacuum
        newtab.setAutoVacuumInfo(this.getAutoVacuumInfo());
        //privilege   
        newtab.setAcl(this.getACL());
        //for security labels
        /*
        List<SecurityLabelInfoDTO> slList = new ArrayList();
        DefaultTableModel slModel = (DefaultTableModel) tblSecurityLabel.getModel();
        for (int rowp = 0; rowp < slModel.getRowCount(); rowp++)
        {
            SecurityLabelInfoDTO sl = new SecurityLabelInfoDTO();
            sl.setProvider(slModel.getValueAt(rowp, 0).toString());
            sl.setSecurityLabel(slModel.getValueAt(rowp, 1).toString());
            slList.add(sl);
        }
        newtab.setSecurityInfoList(slList);
         */

        newtab.setKind('r');
        if (cbPartitioned.isSelected()) {
            newtab.setKind('p');
            newtab.setPartitionSchema(partitionTypeComboBox.getSelectedItem().toString().toUpperCase());

            List<String> partitionKeys = new ArrayList();
            TableModel pkmodel = tabPartitionKey.getModel();
            for (int r = 0; r < pkmodel.getRowCount(); r++) {
                partitionKeys.add("COLUMN".equalsIgnoreCase(pkmodel.getValueAt(r, 0).toString())
                        ? pkmodel.getValueAt(r, 1).toString() : pkmodel.getValueAt(r, 2).toString());
            }
            newtab.setPartitionKey(partitionKeys);            
            newtab.setPartitionList(getPartitions());
        }

        return newtab;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    //partition
    private void btnAddPartitionactionPerformed(ActionEvent e) {
        DefaultTableModel tableModel = (DefaultTableModel) tabPartitions.getModel();
        Object[] row = new Object[6];
        for (Object r : row) {
            r = null;
        }
        tableModel.addRow(row);
    }
    // enable partition tab buttons
    private void enablePartitionTabButtons() {
        if (isPartitioned && !getColumnList().isEmpty()) {
            partitionTypeComboBox.setEnabled(true);
            btnAddPartitionKey.setEnabled(true);
            btnAddPartition.setEnabled(true);
            btnRemovePartitionKey.setEnabled(true);
            btnRemovePartition.setEnabled(true);
        } else {
            if (isPartitioned == false) {
                JOptionPane.showMessageDialog(this, constBundle.getString("partition_button_check_warning"), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            } else if (getColumnList().isEmpty()) {
                JOptionPane.showMessageDialog(this, constBundle.getString("partition_column_add_warning"), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
            partitionTypeComboBox.setEnabled(false);
            btnAddPartitionKey.setEnabled(false);
            btnAddPartition.setEnabled(false);
            btnRemovePartitionKey.setEnabled(false);
            btnRemovePartition.setEnabled(false);
        }
    }
    
    private void disablePartitionTabButtons() {
            partitionTypeComboBox.setEnabled(false);
            btnAddPartitionKey.setEnabled(false);
            btnAddPartition.setEnabled(false);
            btnRemovePartitionKey.setEnabled(false);
            btnRemovePartition.setEnabled(false);
    }
    

    private void setPartitionKeyTableColumn(List<ColumnInfoDTO> list) {

        TableColumn partitionKeyColumn = tabPartitionKey.getColumnModel().getColumn(0);
        final JComboBox cbKeyType = new JComboBox();
        cbKeyType.addItem("Column");
        cbKeyType.addItem("Expression");
        //comboBox1.setSelectedIndex(0);
        partitionKeyColumn.setCellEditor(new DefaultCellEditor(cbKeyType));

        TableColumn columnColumn = tabPartitionKey.getColumnModel().getColumn(1);
        JComboBox comboxBox2 = new JComboBox();
        if (list == null || list.isEmpty()) {
            comboxBox2.addItem(" ");
        } else {
            for (int i = 0; i < list.size(); i++) {
                comboxBox2.addItem(list.get(i).getName());
            }
        }
        //comboxBox2.setSelectedIndex(0);
        columnColumn.setCellEditor(new DefaultCellEditor(comboxBox2));

        cbKeyType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (cbKeyType.getSelectedIndex() == 0) {
                    logger.info("column selected + row " + tabPartitionKey.getSelectedRow());
                    partitionKeyTableModel.setColumnEditable(2, false);
                    partitionKeyTableModel.setColumnEditable(1, true);
                    tabPartitionKey.setValueAt(null, tabPartitionKey.getSelectedRow(), 2);
                } else if (cbKeyType.getSelectedIndex() == 1) {
                    partitionKeyTableModel.setColumnEditable(1, false);
                    partitionKeyTableModel.setColumnEditable(2, true);
                    tabPartitionKey.setValueAt(null, tabPartitionKey.getSelectedRow(), 1);
                } else {
                    logger.info("selected null" + cbKeyType.getSelectedIndex());
                    partitionKeyTableModel.setColumnEditable(1, true);
                    partitionKeyTableModel.setColumnEditable(2, true);
                }
            }
        });

    }

    class EditableTableModel extends DefaultTableModel {

        boolean[] columnEditable;
        Class[] types;

        public EditableTableModel(Object[][] data, String[] columnNames) {
            super(data, columnNames);
            columnEditable = new boolean[columnNames.length];
            Arrays.fill(columnEditable, true);
        }
        public EditableTableModel(Object[][] data, String[] columnNames, Class[] types) {
            this(data, columnNames);
            this.types = types;
        }

        @Override
        public Class getColumnClass(int columnIndex)
        {
            if (types == null)
            {
                return Object.class;
            }
            return types[columnIndex];
        }
        
        @Override
        public boolean isCellEditable(int row, int column) {
            if (!columnEditable[column]) {
                return false;
            } else {
                return super.isCellEditable(row, column);
            }
        }

        public void setColumnEditable(int column, boolean editable) {
            columnEditable[column] = editable;
        }

        public boolean getColumnEditable(int column) {
            return columnEditable[column];
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel PatitionsLabel;
    private javax.swing.JButton btnAddChangePrivilege;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnAddColumn;
    private javax.swing.JButton btnAddConstraint;
    private javax.swing.JButton btnAddInherits;
    private javax.swing.JButton btnAddPartition;
    private javax.swing.JButton btnAddPartitionKey;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnChangeColumn;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemoveColumn;
    private javax.swing.JButton btnRemoveConstraint;
    private javax.swing.JButton btnRemoveInherits;
    private javax.swing.JButton btnRemovePartition;
    private javax.swing.JButton btnRemovePartitionKey;
    private javax.swing.JButton btnRemovePrivilege;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JCheckBox cbAll;
    private javax.swing.JCheckBox cbComments;
    private javax.swing.JCheckBox cbConstraints;
    private javax.swing.JCheckBox cbCustomAutoVacuum;
    private javax.swing.JCheckBox cbCustomAutoVacuumToast;
    private javax.swing.JCheckBox cbDefaultValues;
    private javax.swing.JCheckBox cbDelete;
    private javax.swing.JCheckBox cbEnable;
    private javax.swing.JCheckBox cbEnableToast;
    private javax.swing.JCheckBox cbHasOIDs;
    private javax.swing.JCheckBox cbIndexes;
    private javax.swing.JCheckBox cbInsert;
    private javax.swing.JCheckBox cbOptionAll;
    private javax.swing.JCheckBox cbOptionDelete;
    private javax.swing.JCheckBox cbOptionInsert;
    private javax.swing.JCheckBox cbOptionReferences;
    private javax.swing.JCheckBox cbOptionRule;
    private javax.swing.JCheckBox cbOptionSelect;
    private javax.swing.JCheckBox cbOptionTrigger;
    private javax.swing.JCheckBox cbOptionTruncate;
    private javax.swing.JCheckBox cbOptionUpdate;
    private javax.swing.JCheckBox cbPartitioned;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbReferences;
    private javax.swing.JCheckBox cbRule;
    private javax.swing.JCheckBox cbSelect;
    private javax.swing.JCheckBox cbStorage;
    private javax.swing.JCheckBox cbTrigger;
    private javax.swing.JCheckBox cbTruncate;
    private javax.swing.JCheckBox cbUnlogged;
    private javax.swing.JCheckBox cbUpdate;
    private javax.swing.JComboBox cbbConstraint;
    private javax.swing.JComboBox cbbLikeRelation;
    private javax.swing.JComboBox cbbOfTypeTable;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.JComboBox cbbRole;
    private javax.swing.JComboBox cbbSchema;
    private javax.swing.JComboBox cbbTableInherits;
    private javax.swing.JComboBox cbbTablespace;
    private javax.swing.JComboBox cbbUseSlony;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JTabbedPane jtpAutoVacuum;
    private javax.swing.JLabel lblAnalyzeBaseThreshold;
    private javax.swing.JLabel lblAnalyzeBaseThresholdValue;
    private javax.swing.JLabel lblAnalyzeScaleFactor;
    private javax.swing.JLabel lblAnalyzeScaleFactorValue;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblCurrentValue;
    private javax.swing.JLabel lblCurrentValue2;
    private javax.swing.JLabel lblEnable;
    private javax.swing.JLabel lblEnableToast;
    private javax.swing.JLabel lblFillFactor;
    private javax.swing.JLabel lblFreezeMaximumAge;
    private javax.swing.JLabel lblFreezeMaximumAgeToast;
    private javax.swing.JLabel lblFreezeMaximumAgeValue;
    private javax.swing.JLabel lblFreezeMaximumAgeValue1;
    private javax.swing.JLabel lblFreezeMinimumAge;
    private javax.swing.JLabel lblFreezeMinimumAgeToast;
    private javax.swing.JLabel lblFreezeMinimumAgeValue;
    private javax.swing.JLabel lblFreezeMinimumAgeValue1;
    private javax.swing.JLabel lblFreezeTableAge;
    private javax.swing.JLabel lblFreezeTableAgeToast;
    private javax.swing.JLabel lblFreezeTableAgeValue;
    private javax.swing.JLabel lblFreezeTableAgeValue1;
    private javax.swing.JLabel lblHasOIDs;
    private javax.swing.JLabel lblIncluding;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOfType;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblPartitionKeys;
    private javax.swing.JLabel lblPartitionedTable;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblRelation;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSchema;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblTablesapce;
    private javax.swing.JLabel lblUnlogged;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JLabel lblVacuumBaseThreshold;
    private javax.swing.JLabel lblVacuumBaseThresholdToast;
    private javax.swing.JLabel lblVacuumBaseThresholdValue;
    private javax.swing.JLabel lblVacuumBaseThresholdValue1;
    private javax.swing.JLabel lblVacuumCostDelay;
    private javax.swing.JLabel lblVacuumCostDelayToast;
    private javax.swing.JLabel lblVacuumCostDelayValue;
    private javax.swing.JLabel lblVacuumCostDelayValue1;
    private javax.swing.JLabel lblVacuumCostLimit;
    private javax.swing.JLabel lblVacuumCostLimitToast;
    private javax.swing.JLabel lblVacuumCostLimitValue;
    private javax.swing.JLabel lblVacuumCostLimitValue1;
    private javax.swing.JLabel lblVacuumScaleFactor;
    private javax.swing.JLabel lblVacuumScaleFactorToast;
    private javax.swing.JLabel lblVacuumScaleFactorValue;
    private javax.swing.JLabel lblVacuumScaleFactorValue1;
    private javax.swing.JLabel partitionKeysLabel;
    private javax.swing.JComboBox<String> partitionTypeComboBox;
    private javax.swing.JLabel partitionTypeLabel;
    private javax.swing.JPanel pnlAutoVacuum;
    private javax.swing.JPanel pnlColumns;
    private javax.swing.JPanel pnlConstraints;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlInherits;
    private javax.swing.JPanel pnlLike;
    private javax.swing.JPanel pnlPartition;
    private javax.swing.JPanel pnlPrivileges;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JPanel pnlTable;
    private javax.swing.JPanel pnlTableToast;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tabPartitionKey;
    private javax.swing.JTable tabPartitions;
    private javax.swing.JTable tblColumns;
    private javax.swing.JTable tblConstraints;
    private javax.swing.JTable tblPrivileges;
    private javax.swing.JTable tblSecurityLabel;
    private javax.swing.JTable tblTableInherits;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdAnalyzeBaseThreshold;
    private javax.swing.JTextField txfdAnalyzeScaleFactor;
    private javax.swing.JTextField txfdFillFactor;
    private javax.swing.JTextField txfdFreezeMaximumAge;
    private javax.swing.JTextField txfdFreezeMaximumAgeToast;
    private javax.swing.JTextField txfdFreezeMinimumAge;
    private javax.swing.JTextField txfdFreezeMinimumAgeToast;
    private javax.swing.JTextField txfdFreezeTableAge;
    private javax.swing.JTextField txfdFreezeTableAgeToast;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdVacuumBaseThreshold;
    private javax.swing.JTextField txfdVacuumBaseThresholdToast;
    private javax.swing.JTextField txfdVacuumCostDelay;
    private javax.swing.JTextField txfdVacuumCostDelayToast;
    private javax.swing.JTextField txfdVacuumCostLimit;
    private javax.swing.JTextField txfdVacuumCostLimitToast;
    private javax.swing.JTextField txfdVacuumScaleFactor;
    private javax.swing.JTextField txfdVacuumScaleFactorToast;
    // End of variables declaration//GEN-END:variables
}
