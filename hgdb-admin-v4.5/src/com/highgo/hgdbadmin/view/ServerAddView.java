/* ------------------------------------------------ 
* 
* File: ServerAddView.java
*
* Abstract: 
* 		.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\ServerAddView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class ServerAddView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private String[] serverGroup;
    private boolean isSuccess;
    
    private XMLServerInfoDTO newServerInfo;//for add
    private XMLServerInfoDTO oldServerInfo;//for property 

    /**
     * Creates new form ServerAddView
     * @param parent
     * @param modal
     * @param serverGroup
     */
    public ServerAddView(JFrame parent, boolean modal, String[] serverGroup)
    {
        super(parent, modal);
        this.serverGroup = serverGroup;
        this.oldServerInfo = null;
        isSuccess = false;
        initComponents();
        this.setTitle(constBundle.getString("addNewServerCon"));
                
        KeyAdapter defineAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                definitionChangedForAddAction();
            }
        };
        txfdName.addKeyListener(defineAdapter);
        txfdHost.addKeyListener(defineAdapter);
        txfdPort.addKeyListener(defineAdapter);
        txfdMaintainDB.addKeyListener(defineAdapter);
        txfdUserName.addKeyListener(defineAdapter);
        /*
        pnlColor.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                pnlColorMouseClicked(evt);
            }
        });
        */
        
        /*
        btnChooseSSLCert.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnChooseSSLFile(txfdSSLCert);
            }
        });
        btnChooseSSLKey.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnChooseSSLFile(txfdSSLKey);
            }
        });
        btnChooseSSLRootCert.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnChooseSSLFile(txfdSSLRootCert);
            }
        });
        */        
        
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOK4AddActionPerformed(e);
            }
        });
        
    }

    public ServerAddView(JFrame parent, boolean modal, String[] serverGroup, XMLServerInfoDTO serverInfo)
    {
        super(parent, modal);
        this.serverGroup = serverGroup;
        this.oldServerInfo = serverInfo;
        initComponents();
        this.setTitle(constBundle.getString("serverCon") + " " + serverInfo.getName());
        txfdName.setText(serverInfo.getName());
        txfdName.setEnabled(false);
        txfdHost.setText(serverInfo.getHost());
        txfdHost.setEnabled(false);
        txfdPort.setText(serverInfo.getPort());
        txfdPort.setEnabled(false);
        if (serverInfo.getService() != null && !serverInfo.getService().isEmpty())
        {
            txfdService.setText(serverInfo.getService());
        }
        txfdService.setEnabled(false);
        txfdMaintainDB.setText(serverInfo.getMaintainDB());
        txfdMaintainDB.setEnabled(false);
        txfdUserName.setText(serverInfo.getUser());
        txfdUserName.setEnabled(!serverInfo.isConnected());
        txfdUserName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
               definitionChangedForChangeAction();
            }           
        });
        pwdfdPwd.setText(serverInfo.getPwd());
        pwdfdPwd.setEnabled(!serverInfo.isConnected());
        //cbStorePwd.setSelected(serverInfo.isSavePwd());
        //cbStorePwd.setEnabled(false);
        cbOnSSL.setSelected(serverInfo.isOnSSL());
        cbOnSSL.setEnabled(false);
        //pnlColor.setBackground();
        //pnlColor.setEnabled(false);
        cbbGroup.setSelectedItem(serverInfo.getGroup());
        cbbGroup.setEnabled(false);
        
        /*
        cbbSSLMode.setEnabled(false);
        txfdSSLCert.setEditable(false);
        btnChooseSSLCert.setEnabled(false);
        txfdSSLKey.setEditable(false);
        btnChooseSSLKey.setEnabled(false);
        txfdSSLRootCert.setEditable(false);
        btnChooseSSLRootCert.setEnabled(false);
        */
        
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOK4ChangeActionPerformed(e);
            }
        });
    }
    
     /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pnlSSL = new javax.swing.JPanel();
        lblSSLMode = new javax.swing.JLabel();
        cbbSSLMode = new javax.swing.JComboBox();
        lblSSLCert = new javax.swing.JLabel();
        lblSSLKey = new javax.swing.JLabel();
        lblSSLRootCert = new javax.swing.JLabel();
        txfdSSLCert = new javax.swing.JTextField();
        btnChooseSSLCert = new javax.swing.JButton();
        txfdSSLKey = new javax.swing.JTextField();
        btnChooseSSLKey = new javax.swing.JButton();
        txfdSSLRootCert = new javax.swing.JTextField();
        btnChooseSSLRootCert = new javax.swing.JButton();
        pblButtons = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        jtp = new javax.swing.JTabbedPane();
        pnlProperty = new javax.swing.JPanel();
        lblName1 = new javax.swing.JLabel();
        lblHost1 = new javax.swing.JLabel();
        lblPort1 = new javax.swing.JLabel();
        lblServer1 = new javax.swing.JLabel();
        lblMaintDB1 = new javax.swing.JLabel();
        lblUserName1 = new javax.swing.JLabel();
        lblPwd1 = new javax.swing.JLabel();
        lblGroup1 = new javax.swing.JLabel();
        txfdPort = new javax.swing.JTextField();
        txfdName = new javax.swing.JTextField();
        txfdHost = new javax.swing.JTextField();
        txfdService = new javax.swing.JTextField();
        txfdUserName = new javax.swing.JTextField();
        pwdfdPwd = new javax.swing.JPasswordField();
        cbbGroup = new javax.swing.JComboBox();
        lblOnSSL = new javax.swing.JLabel();
        cbOnSSL = new javax.swing.JCheckBox();
        txfdMaintainDB = new javax.swing.JTextField();

        pnlSSL.setBackground(new java.awt.Color(255, 255, 255));
        pnlSSL.setToolTipText("");

        lblSSLMode.setText(constBundle.getString("sslMode"));

        cbbSSLMode.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "disable", "allow", "prefer", "require", "verify-ca", "verify-full" }));

        lblSSLCert.setText(constBundle.getString("sslCert"));

        lblSSLKey.setText(constBundle.getString("sslKey"));

        lblSSLRootCert.setText(constBundle.getString("sslRootCert"));

        btnChooseSSLCert.setText(constBundle.getString("browse"));

        btnChooseSSLKey.setText(constBundle.getString("browse"));

        btnChooseSSLRootCert.setText(constBundle.getString("browse"));

        javax.swing.GroupLayout pnlSSLLayout = new javax.swing.GroupLayout(pnlSSL);
        pnlSSL.setLayout(pnlSSLLayout);
        pnlSSLLayout.setHorizontalGroup(
            pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSSLLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(lblSSLRootCert, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                    .addComponent(lblSSLKey, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblSSLCert, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblSSLMode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cbbSSLMode, 0, 413, Short.MAX_VALUE)
                    .addGroup(pnlSSLLayout.createSequentialGroup()
                        .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txfdSSLRootCert, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txfdSSLKey, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txfdSSLCert, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(5, 5, 5)
                        .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnChooseSSLKey, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                            .addComponent(btnChooseSSLCert, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnChooseSSLRootCert, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))))
                .addContainerGap())
        );
        pnlSSLLayout.setVerticalGroup(
            pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSSLLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSSLMode)
                    .addComponent(cbbSSLMode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSSLCert)
                    .addComponent(txfdSSLCert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChooseSSLCert))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdSSLKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnChooseSSLKey))
                    .addComponent(lblSSLKey))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlSSLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdSSLRootCert, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnChooseSSLRootCert))
                    .addComponent(lblSSLRootCert))
                .addContainerGap(280, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/server.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

btnHelp.setText(constBundle.getString("help"));
btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
btnHelp.addActionListener(new java.awt.event.ActionListener()
{
    public void actionPerformed(java.awt.event.ActionEvent evt)
    {
        btnHelpActionPerformed(evt);
    }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pblButtonsLayout = new javax.swing.GroupLayout(pblButtons);
    pblButtons.setLayout(pblButtonsLayout);
    pblButtonsLayout.setHorizontalGroup(
        pblButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pblButtonsLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 271, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pblButtonsLayout.setVerticalGroup(
        pblButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pblButtonsLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pblButtonsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pblButtons, java.awt.BorderLayout.PAGE_END);

    pnlProperty.setBackground(new java.awt.Color(255, 255, 255));

    lblName1.setText(constBundle.getString("name"));

    lblHost1.setText(constBundle.getString("host"));

    lblPort1.setText(constBundle.getString("port"));

    lblServer1.setText(constBundle.getString("service"));

    lblMaintDB1.setText(constBundle.getString("maintenanceDB"));

    lblUserName1.setText(constBundle.getString("userName"));

    lblPwd1.setText(constBundle.getString("pwd"));

    lblGroup1.setText(constBundle.getString("group"));

    txfdPort.setText("5866");

    pwdfdPwd.addKeyListener(new java.awt.event.KeyAdapter()
    {
        public void keyPressed(java.awt.event.KeyEvent evt)
        {
            pwdfdPwdKeyPressed(evt);
        }
    });

    cbbGroup.setModel(new DefaultComboBoxModel(serverGroup));

    lblOnSSL.setText(constBundle.getString("onSSL"));

    cbOnSSL.setBackground(new java.awt.Color(255, 255, 255));
    cbOnSSL.setSelected(true);

    txfdMaintainDB.setText("highgo");

    javax.swing.GroupLayout pnlPropertyLayout = new javax.swing.GroupLayout(pnlProperty);
    pnlProperty.setLayout(pnlPropertyLayout);
    pnlPropertyLayout.setHorizontalGroup(
        pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertyLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lblOnSSL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblName1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblHost1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPort1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblServer1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMaintDB1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblUserName1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblPwd1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblGroup1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertyLayout.createSequentialGroup()
                    .addComponent(cbOnSSL)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertyLayout.createSequentialGroup()
                    .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txfdMaintainDB, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdUserName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                        .addComponent(txfdService, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdPort, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdHost, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdName, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pwdfdPwd, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbbGroup, javax.swing.GroupLayout.Alignment.LEADING, 0, 413, Short.MAX_VALUE))
                    .addGap(10, 10, 10))))
    );
    pnlPropertyLayout.setVerticalGroup(
        pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertyLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName1)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblHost1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblPort1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblServer1)
                .addComponent(txfdService, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblMaintDB1)
                .addComponent(txfdMaintainDB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblUserName1)
                .addComponent(txfdUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblPwd1)
                .addComponent(pwdfdPwd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOnSSL, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbOnSSL))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblGroup1)
                .addComponent(cbbGroup, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(0, 135, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("property"), pnlProperty);

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName(constBundle.getString("ssl"));

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void definitionChangedForAddAction()
    {
        //logger.debug(txfdName.getText() + "," + txfdHost.getText() + "," + txfdPort.getText()
        //        + "," + txfdMaintainDB.getText() + "," + txfdUserName.getText());
        if (txfdName.getText().isEmpty()
                || txfdHost.getText().isEmpty()
                || txfdPort.getText().isEmpty()
                || txfdMaintainDB.getText().isEmpty()
                || txfdUserName.getText().isEmpty())
        {
            btnOK.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
        }
    }
    
    private void definitionChangedForChangeAction()
    {
        logger.debug(oldServerInfo.getUser()+ "," + txfdUserName.getText());
        if (txfdUserName.getText().isEmpty()
              || txfdUserName.getText().equals(oldServerInfo.getUser()))
        {
            btnOK.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
        }
    }
    
    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showAdminHelpPage(this, "connect.html");
    }//GEN-LAST:event_btnHelpActionPerformed

    private void pwdfdPwdKeyPressed(java.awt.event.KeyEvent evt)//GEN-FIRST:event_pwdfdPwdKeyPressed
    {//GEN-HEADEREND:event_pwdfdPwdKeyPressed
        int k = evt.getKeyCode();
        //logger.info("KeyConde="+k);
        if (k == evt.VK_ENTER)
        {
            btnOK4AddActionPerformed(null);
        }
    }//GEN-LAST:event_pwdfdPwdKeyPressed

    private void btnChooseSSLFile(JTextField txfd)
    {
        JFileChooser pathChooser = new JFileChooser();
        pathChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        pathChooser.setDialogTitle("Choose file");
        int response = pathChooser.showOpenDialog(this);
        if (response == 0)
        {
            String choosedFile = pathChooser.getSelectedFile().getAbsolutePath();
            logger.info("choosedFile=" + choosedFile);
            txfd.setText(choosedFile);
        }
    }
    
    /*private void pnlColorMouseClicked(MouseEvent evt)
    {
        Color newColor = JColorChooser.showDialog(
                this, "Choose Color", pnlColor.getBackground());
        if (newColor != null)
        {
            pnlColor.setBackground(newColor);
        }
    }*/   
    
    private void btnOK4AddActionPerformed(java.awt.event.ActionEvent evt)
    {
        logger.info("ok");
        try
        {
            /*if (ckbStorePwd.isSelected())
             {
             SuggestionDialog sdlg = new SuggestionDialog(this, true, "saving-passwords.html");
             sdlg.setLocationRelativeTo(this);
             sdlg.setVisible(true);
             if (!sdlg.isOk())
             {
             return;
             }
             }*/
            this.setNewServerInfo();
            String loginHint = TreeController.getInstance().addServer(getNewServerInfo());
            isSuccess = true;
            this.dispose();
            if (loginHint != null)
            {
                JOptionPane.showMessageDialog(this, loginHint,
                        constBundle.getString("hint"), JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    private void btnOK4ChangeActionPerformed(java.awt.event.ActionEvent evt)
    {
        logger.info("ok");
        try
        {
            String newUser = txfdUserName.getText();
            String newPwd =  String.valueOf(pwdfdPwd.getPassword());
            TreeController.getInstance().changeServerUser(oldServerInfo, newUser, newPwd);
            oldServerInfo.setUser(newUser);
            oldServerInfo.setPwd(newPwd);
            oldServerInfo.setConnected(true);
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
     
    private void setNewServerInfo()
    {
        if (newServerInfo == null)
        {
            newServerInfo = new XMLServerInfoDTO();
        }

        newServerInfo.setGroup(cbbGroup.getSelectedItem().toString());
        newServerInfo.setName(txfdName.getText());
        newServerInfo.setHost(txfdHost.getText());
        newServerInfo.setPort(txfdPort.getText());
        newServerInfo.setMaintainDB(txfdMaintainDB.getText());
        newServerInfo.setUser(txfdUserName.getText());
        newServerInfo.setPwd(String.valueOf(pwdfdPwd.getPassword()));
        /*serverInfo.setSavePwd(ckbStorePwd.isSelected());
        if (ckbStorePwd.isSelected())
        {
            serverInfo.setPwd(String.valueOf(pwdfdPwd.getPassword()));
        }*/        
        newServerInfo.setOnSSL(cbOnSSL.isSelected());
        newServerInfo.setService(txfdService.getText());
        newServerInfo.setConnected(false);
        //logger.info("color =" + pnlColor.getBackground()+", RGB="+pnlColor.getBackground().getRGB());
    }
    
    public XMLServerInfoDTO getNewServerInfo()
    {
        return newServerInfo;
    }
    
    public boolean isSuccess()
    {
        return isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnChooseSSLCert;
    private javax.swing.JButton btnChooseSSLKey;
    private javax.swing.JButton btnChooseSSLRootCert;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JCheckBox cbOnSSL;
    private javax.swing.JComboBox cbbGroup;
    private javax.swing.JComboBox cbbSSLMode;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblGroup1;
    private javax.swing.JLabel lblHost1;
    private javax.swing.JLabel lblMaintDB1;
    private javax.swing.JLabel lblName1;
    private javax.swing.JLabel lblOnSSL;
    private javax.swing.JLabel lblPort1;
    private javax.swing.JLabel lblPwd1;
    private javax.swing.JLabel lblSSLCert;
    private javax.swing.JLabel lblSSLKey;
    private javax.swing.JLabel lblSSLMode;
    private javax.swing.JLabel lblSSLRootCert;
    private javax.swing.JLabel lblServer1;
    private javax.swing.JLabel lblUserName1;
    private javax.swing.JPanel pblButtons;
    private javax.swing.JPanel pnlProperty;
    private javax.swing.JPanel pnlSSL;
    private javax.swing.JPasswordField pwdfdPwd;
    private javax.swing.JTextField txfdHost;
    private javax.swing.JTextField txfdMaintainDB;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdPort;
    private javax.swing.JTextField txfdSSLCert;
    private javax.swing.JTextField txfdSSLKey;
    private javax.swing.JTextField txfdSSLRootCert;
    private javax.swing.JTextField txfdService;
    private javax.swing.JTextField txfdUserName;
    // End of variables declaration//GEN-END:variables
}
