/* ------------------------------------------------ 
* 
* File: ViewView.java
*
* Abstract: 
* 		展示配置postgres.conf 文件信息.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\ViewView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.AutoVacuumDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author Liu Yuanyuan
 * All configuration file was saved default in C:\Users\Yuanyuan(you user name)\AppData\Roaming\postgresql.
 * name and defination must entered, if not button ok will be disable.
 * 
 */
public class ViewView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");  
    private HelperInfoDTO helperInfo;
    private ViewInfoDTO viewInfo;
    private boolean isSuccess = false;

    //add
    public ViewView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.viewInfo = null;        
        initComponents();
        jtp.setEnabledAt(4, false);
        this.setTitle(constBundle.getString("addView"));
        this.setNormalInfo(false);
        //define action
        KeyAdapter btnOkEnableKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAddAction(evt);
            }
        };
        txfdName.addKeyListener(btnOkEnableKeyAdapter);
        ttarDefinition.addKeyListener(btnOkEnableKeyAdapter);
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });
    }
    
    //property， 通过点击属性进入
    public ViewView(JFrame parent, boolean modal, ViewInfoDTO viewInfo)
    {
        super(parent, modal);
        this.viewInfo = viewInfo;
        this.helperInfo = viewInfo.getHelperInfo();
        initComponents();
        jtp.setEnabledAt(4, false);
        this.setTitle(constBundle.getString("views") + " " + viewInfo.getName());
        this.setNormalInfo(true);
        this.setProperty();        
        //对form窗口内的按钮增加处理事件
        KeyAdapter btnOkEnableKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnableKeyAdapter);
        cbbOwner.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        txarComment.addKeyListener(btnOkEnableKeyAdapter);
        cbSecurityBarrier.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnableForChange();
            }
        });
        ttarDefinition.addKeyListener(btnOkEnableKeyAdapter);
        DefaultTableModel aclModel = (DefaultTableModel) tblPrivileges.getModel();
        aclModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnableForChange();
            }
        });   
        if (viewInfo.isMaterializedView())
        {
            //option paramater
            cbbTablespace.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    btnOkEnableForChange();
                }
            });
            txfdFillFactor.addKeyListener(btnOkEnableKeyAdapter);
            ActionListener btnOkEnableAction = new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    btnOkEnableForChange();
                }
            };
            cbWithData.addActionListener(btnOkEnableAction);
            //table parameter
            cbCustomAutoVacuum.addActionListener(btnOkEnableAction);
            cbEnable.addActionListener(btnOkEnableAction);
            txfdVacuumBaseThreshold.addKeyListener(btnOkEnableKeyAdapter);
            txfdAnalyzeBaseThreshold.addKeyListener(btnOkEnableKeyAdapter);
            txfdVacuumScaleFactor.addKeyListener(btnOkEnableKeyAdapter);
            txfdAnalyzeScaleFactor.addKeyListener(btnOkEnableKeyAdapter);
            txfdVacuumCostDelay.addKeyListener(btnOkEnableKeyAdapter);
            txfdVacuumCostLimit.addKeyListener(btnOkEnableKeyAdapter);
            txfdFreezeMinimumAge.addKeyListener(btnOkEnableKeyAdapter);
            txfdFreezeMaximumAge.addKeyListener(btnOkEnableKeyAdapter);
            txfdFreezeTableAge.addKeyListener(btnOkEnableKeyAdapter);
            //toast table parameter
            cbCustomAutoVacuumToast.addActionListener(btnOkEnableAction);
            cbEnableToast.addActionListener(btnOkEnableAction);
            txfdVacuumBaseThresholdToast.addKeyListener(btnOkEnableKeyAdapter);
            txfdVacuumScaleFactorToast.addKeyListener(btnOkEnableKeyAdapter);
            txfdVacuumCostDelayToast.addKeyListener(btnOkEnableKeyAdapter);
            txfdVacuumCostLimitToast.addKeyListener(btnOkEnableKeyAdapter);
            txfdFreezeMinimumAgeToast.addKeyListener(btnOkEnableKeyAdapter);
            txfdFreezeMaximumAgeToast.addKeyListener(btnOkEnableKeyAdapter);
            txfdFreezeTableAgeToast.addKeyListener(btnOkEnableKeyAdapter);
        }

        
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });        
    }
    private void setProperty()
    {
        //property
        txfdName.setText(viewInfo.getName());
        txfdOID.setText(viewInfo.getOid().toString());
        cbbOwner.setSelectedItem(viewInfo.getOwner());
        cbbSchema.setSelectedItem(viewInfo.getSchema());
        txarComment.setText(viewInfo.getComment());
        cbSecurityBarrier.setSelected(viewInfo.isSecurityBarrier());
        ttarDefinition.setText(viewInfo.getDefinition());
        cbMaterializedView.setEnabled(false);
        cbMaterializedView.setSelected(viewInfo.isMaterializedView());       
        if (viewInfo.isMaterializedView())
        {
            this.cbMaterializedViewActionPerformed(null);
            cbbTablespace.setSelectedItem(viewInfo.getTablespace());
            txfdFillFactor.setText(viewInfo.getFillFactor());
            cbWithData.setSelected(viewInfo.isWithData());
            AutoVacuumDTO autoVacuum = viewInfo.getAutoVacuumInfo();
            cbCustomAutoVacuum.setSelected(autoVacuum.isCustomAutoVacuum());
            if (autoVacuum.isCustomAutoVacuum())
            {
                this.cbCustomAutoVacuumActionPerformed(null);
                cbEnable.setSelected(autoVacuum.isEnable());
                cbEnable.setEnabled(true);
                if (autoVacuum.isEnable())
                {
                    txfdVacuumBaseThreshold.setText(autoVacuum.getVacuumBaseThreshold());
                    txfdAnalyzeBaseThreshold.setText(autoVacuum.getAnalyzeBaseThreshold());
                    txfdVacuumScaleFactor.setText(autoVacuum.getVacuumScaleFactor());
                    txfdAnalyzeScaleFactor.setText(autoVacuum.getAnalyzeScaleFactor());
                    txfdVacuumCostDelay.setText(autoVacuum.getVacuumCostDelay());
                    txfdVacuumCostLimit.setText(autoVacuum.getVacuumCostLimit());
                    txfdFreezeMinimumAge.setText(autoVacuum.getFreezeMinimumAge());
                    txfdFreezeMaximumAge.setText(autoVacuum.getFreezeMaximumAge());
                    txfdFreezeTableAge.setText(autoVacuum.getFreezeTableAge());
                }
            }
            cbCustomAutoVacuumToast.setSelected(autoVacuum.isCustomAutoVacuumToast());    
            cbCustomAutoVacuumToast.setEnabled(false);
            if (autoVacuum.isCustomAutoVacuumToast())
            {               
                //this.cbCustomAutoVacuumToastActionPerformed(null);
                cbEnableToast.setSelected(autoVacuum.isEnableToast());
                if (autoVacuum.isEnableToast())
                {
                    txfdVacuumBaseThresholdToast.setText(autoVacuum.getVacuumBaseThresholdToast());
                    txfdVacuumScaleFactorToast.setText(autoVacuum.getVacuumScaleFactorToast());
                    txfdVacuumCostDelayToast.setText(autoVacuum.getVacuumCostDelayToast());
                    txfdVacuumCostLimitToast.setText(autoVacuum.getVacuumCostLimitToast());
                    txfdFreezeMinimumAgeToast.setText(autoVacuum.getFreezeMinimumAgeToast());
                    txfdFreezeMaximumAgeToast.setText(autoVacuum.getFreezeMaximumAgeToast());
                    txfdFreezeTableAgeToast.setText(autoVacuum.getFreezeTableAgeToast());
                }
            }
        }
        
        //权限处理
        /*处理流程如下:
            1、在库中获取ACL，ACL为空不做处理
            2、对内容以','符号进行拆分
            3、对拆分的内容重新组装权限的key和value
        */
        String acl = viewInfo.getAcl();
        if (acl != null && !acl.isEmpty() && !acl.equals("{}"))
        {
            DefaultTableModel sModel = (DefaultTableModel) tblPrivileges.getModel();
            acl = acl.substring(1, acl.length() - 1);
            String[] privilegeArray = acl.split(",");
            for (String privilege : privilegeArray)
            {
                logger.info("privilege:" + privilege);
                String[] pArray = privilege.split("=");
                String[] p = new String[2];
                if (pArray[0] == null || pArray[0].isEmpty())
                {
                    p[0] = "public";
                } else
                {
                    p[0] = pArray[0];
                }
                p[1] = pArray[1].split("/")[0];
                sModel.addRow(p);
            }
        }
        /*
            安全标签处理
            1、获取安全标签的内容
            2、对标签拆分， 并将拆分的内容展示导Model窗体
        */
        List<SecurityLabelInfoDTO> slist = viewInfo.getSecurityLabelInfoList();
        if (slist != null)
        {
            DefaultTableModel sModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[3];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                sModel.addRow(sl);
            }
        }
    }

    /* 设置基本信息 */
    private void setNormalInfo(boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
        //cbbSchema.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "schema")));
        cbbSchema.setModel(new DefaultComboBoxModel(new String[]{helperInfo.getSchema()}));
        cbbSchema.setSelectedItem(helperInfo.getSchema());
        try
        {
            cbbOwner.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "owner")));
            if(isChange)
            {
                cbbOwner.removeItemAt(0);
            }else
            {
                cbbOwner.setSelectedIndex(0);
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbRole.setModel(new DefaultComboBoxModel(tc.getRoles(helperInfo)));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 2))
        {
            jtp.setEnabledAt(2, true);
            try
            {
                cbbTablespace.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "tablespace")));
                if (isChange)
                {
                    cbbTablespace.removeItemAt(0);
                } else
                {
                    cbbTablespace.setSelectedIndex(0);
                }
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else
        {
            jtp.setEnabledAt(2, false);
        }
        
        //format action
        KeyAdapter valueKeyAdapter = new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                txfdValueKeyTyped(evt);
            }
        };
        txfdFillFactor.addKeyListener(valueKeyAdapter);
        txfdVacuumBaseThreshold.addKeyListener(valueKeyAdapter);
        txfdAnalyzeBaseThreshold.addKeyListener(valueKeyAdapter);
        txfdVacuumScaleFactor.addKeyListener(valueKeyAdapter);
        txfdAnalyzeScaleFactor.addKeyListener(valueKeyAdapter);
        txfdVacuumCostDelay.addKeyListener(valueKeyAdapter);
        txfdVacuumCostLimit.addKeyListener(valueKeyAdapter);
        txfdFreezeMinimumAge.addKeyListener(valueKeyAdapter);
        txfdFreezeMaximumAge.addKeyListener(valueKeyAdapter);
        txfdFreezeTableAge.addKeyListener(valueKeyAdapter);
        txfdVacuumBaseThresholdToast.addKeyListener(valueKeyAdapter);
        txfdVacuumScaleFactorToast.addKeyListener(valueKeyAdapter);
        txfdVacuumCostDelayToast.addKeyListener(valueKeyAdapter);
        txfdVacuumCostLimitToast.addKeyListener(valueKeyAdapter);
        txfdFreezeMinimumAgeToast.addKeyListener(valueKeyAdapter);
        txfdFreezeMaximumAgeToast.addKeyListener(valueKeyAdapter);
        txfdFreezeTableAgeToast.addKeyListener(valueKeyAdapter);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        cbbSchema = new javax.swing.JComboBox();
        lblSchema = new javax.swing.JLabel();
        lblUseSlony = new javax.swing.JLabel();
        txfdUseSlony = new javax.swing.JTextField();
        pnlDefinition = new javax.swing.JPanel();
        spAlterSQL1 = new javax.swing.JScrollPane();
        ttarDefinition = new javax.swing.JTextArea();
        lblSecurityBarrier = new javax.swing.JLabel();
        lblDefinition = new javax.swing.JLabel();
        cbSecurityBarrier = new javax.swing.JCheckBox();
        pnlMaterialization = new javax.swing.JPanel();
        lblMaterializedView = new javax.swing.JLabel();
        cbMaterializedView = new javax.swing.JCheckBox();
        jtpMaterializedView = new javax.swing.JTabbedPane();
        pnlOption = new javax.swing.JPanel();
        cbbTablespace = new javax.swing.JComboBox();
        lblTablesapce = new javax.swing.JLabel();
        lblFillFactor = new javax.swing.JLabel();
        lblWithData = new javax.swing.JLabel();
        cbWithData = new javax.swing.JCheckBox();
        txfdFillFactor = new javax.swing.JTextField();
        pnlTable = new javax.swing.JPanel();
        cbCustomAutoVacuum = new javax.swing.JCheckBox();
        lblEnable = new javax.swing.JLabel();
        cbEnable = new javax.swing.JCheckBox();
        lblCurrentValue = new javax.swing.JLabel();
        lblVacuumBaseThresholdValue = new javax.swing.JLabel();
        txfdVacuumBaseThreshold = new javax.swing.JTextField();
        lblVacuumBaseThreshold = new javax.swing.JLabel();
        lblAnalyzeBaseThreshold = new javax.swing.JLabel();
        lblVacuumScaleFactor = new javax.swing.JLabel();
        lblAnalyzeScaleFactor = new javax.swing.JLabel();
        lblVacuumCostDelay = new javax.swing.JLabel();
        lblVacuumCostLimit = new javax.swing.JLabel();
        lblFreezeMinimumAge = new javax.swing.JLabel();
        lblFreezeMaximumAge = new javax.swing.JLabel();
        lblFreezeTableAge = new javax.swing.JLabel();
        txfdFreezeTableAge = new javax.swing.JTextField();
        txfdFreezeMaximumAge = new javax.swing.JTextField();
        txfdFreezeMinimumAge = new javax.swing.JTextField();
        txfdVacuumCostLimit = new javax.swing.JTextField();
        txfdVacuumCostDelay = new javax.swing.JTextField();
        txfdAnalyzeScaleFactor = new javax.swing.JTextField();
        txfdVacuumScaleFactor = new javax.swing.JTextField();
        txfdAnalyzeBaseThreshold = new javax.swing.JTextField();
        lblAnalyzeBaseThresholdValue = new javax.swing.JLabel();
        lblVacuumScaleFactorValue = new javax.swing.JLabel();
        lblAnalyzeScaleFactorValue = new javax.swing.JLabel();
        lblVacuumCostDelayValue = new javax.swing.JLabel();
        lblVacuumCostLimitValue = new javax.swing.JLabel();
        lblFreezeMinimumAgeValue = new javax.swing.JLabel();
        lblFreezeMaximumAgeValue = new javax.swing.JLabel();
        lblFreezeTableAgeValue = new javax.swing.JLabel();
        pnlToastTable = new javax.swing.JPanel();
        cbCustomAutoVacuumToast = new javax.swing.JCheckBox();
        lblEnable1 = new javax.swing.JLabel();
        cbEnableToast = new javax.swing.JCheckBox();
        txfdVacuumBaseThresholdToast = new javax.swing.JTextField();
        lblVacuumBaseThreshold1 = new javax.swing.JLabel();
        lblCurrentValue2 = new javax.swing.JLabel();
        lblVacuumScaleFactor1 = new javax.swing.JLabel();
        lblVacuumBaseThresholdValue1 = new javax.swing.JLabel();
        txfdVacuumScaleFactorToast = new javax.swing.JTextField();
        txfdVacuumCostDelayToast = new javax.swing.JTextField();
        lblVacuumCostDelayValue1 = new javax.swing.JLabel();
        lblVacuumCostDelay1 = new javax.swing.JLabel();
        lblVacuumCostLimitValue1 = new javax.swing.JLabel();
        txfdVacuumCostLimitToast = new javax.swing.JTextField();
        lblVacuumCostLimit1 = new javax.swing.JLabel();
        lblFreezeMinimumAge1 = new javax.swing.JLabel();
        txfdFreezeMinimumAgeToast = new javax.swing.JTextField();
        lblFreezeMinimumAgeValue1 = new javax.swing.JLabel();
        txfdFreezeMaximumAgeToast = new javax.swing.JTextField();
        lblFreezeMaximumAge1 = new javax.swing.JLabel();
        lblFreezeMaximumAgeValue1 = new javax.swing.JLabel();
        lblFreezeTableAge1 = new javax.swing.JLabel();
        txfdFreezeTableAgeToast = new javax.swing.JTextField();
        lblFreezeTableAgeValue1 = new javax.swing.JLabel();
        lblVacuumScaleFactorValue1 = new javax.swing.JLabel();
        pnlPrivileges = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPrivileges = new javax.swing.JTable();
        btnAddChangePrivileges = new javax.swing.JButton();
        btnRemovePrivilege = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRole = new javax.swing.JLabel();
        cbbRole = new javax.swing.JComboBox();
        cbAll = new javax.swing.JCheckBox();
        cbUpdate = new javax.swing.JCheckBox();
        cbInsert = new javax.swing.JCheckBox();
        cbOptionAll = new javax.swing.JCheckBox();
        cbSelect = new javax.swing.JCheckBox();
        cbOptionUpdate = new javax.swing.JCheckBox();
        cbOptionInsert = new javax.swing.JCheckBox();
        cbOptionSelect = new javax.swing.JCheckBox();
        cbDelete = new javax.swing.JCheckBox();
        cbOptionDelete = new javax.swing.JCheckBox();
        cbRule = new javax.swing.JCheckBox();
        cbOptionRule = new javax.swing.JCheckBox();
        cbReferences = new javax.swing.JCheckBox();
        cbOptionReferences = new javax.swing.JCheckBox();
        cbTrigger = new javax.swing.JCheckBox();
        cbOptionTrigger = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addView"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/view.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setEnabled(false);
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane1.setViewportView(txarComment);

cbbSchema.setEnabled(false);

lblSchema.setText(constBundle.getString("schema"));
lblSchema.setEnabled(false);

lblUseSlony.setText(constBundle.getString("useSlony"));

txfdUseSlony.setEditable(false);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSchema, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdOID)
                    .addComponent(txfdName)
                    .addComponent(cbbOwner, 0, 418, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                    .addComponent(cbbSchema, 0, 418, Short.MAX_VALUE)))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txfdUseSlony, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblSchema)
                .addComponent(cbbSchema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblUseSlony))
            .addGap(19, 19, 19))
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(520, 470));

    ttarDefinition.setColumns(20);
    ttarDefinition.setRows(5);
    spAlterSQL1.setViewportView(ttarDefinition);

    lblSecurityBarrier.setText(constBundle.getString("securityBarrier"));

    lblDefinition.setText(constBundle.getString("definition"));

    cbSecurityBarrier.setBackground(new java.awt.Color(255, 255, 255));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblSecurityBarrier, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbSecurityBarrier)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblDefinition, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(spAlterSQL1, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblSecurityBarrier)
                .addComponent(cbSecurityBarrier))
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(lblDefinition, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(spAlterSQL1, javax.swing.GroupLayout.PREFERRED_SIZE, 399, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap(34, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlMaterialization.setBackground(new java.awt.Color(255, 255, 255));
    pnlMaterialization.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlMaterialization.setPreferredSize(new java.awt.Dimension(520, 470));

    lblMaterializedView.setText(constBundle.getString("materializedView"));

    cbMaterializedView.setBackground(new java.awt.Color(255, 255, 255));
    cbMaterializedView.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbMaterializedViewActionPerformed(evt);
        }
    });

    pnlOption.setBackground(new java.awt.Color(255, 255, 255));

    cbbTablespace.setEnabled(false);

    lblTablesapce.setText(constBundle.getString("tablespace"));

    lblFillFactor.setText(constBundle.getString("fillFactor"));

    lblWithData.setText(constBundle.getString("withData"));

    cbWithData.setBackground(new java.awt.Color(255, 255, 255));
    cbWithData.setSelected(true);
    cbWithData.setEnabled(false);

    txfdFillFactor.setEnabled(false);

    javax.swing.GroupLayout pnlOptionLayout = new javax.swing.GroupLayout(pnlOption);
    pnlOption.setLayout(pnlOptionLayout);
    pnlOptionLayout.setHorizontalGroup(
        pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOptionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblTablesapce, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblWithData, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbbTablespace, 0, 462, Short.MAX_VALUE)
                .addGroup(pnlOptionLayout.createSequentialGroup()
                    .addComponent(cbWithData)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(txfdFillFactor))
            .addContainerGap())
    );
    pnlOptionLayout.setVerticalGroup(
        pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlOptionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblTablesapce)
                .addComponent(cbbTablespace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblFillFactor)
                .addComponent(txfdFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblWithData)
                .addComponent(cbWithData))
            .addContainerGap(284, Short.MAX_VALUE))
    );

    jtpMaterializedView.addTab(constBundle.getString("options"), pnlOption);

    pnlTable.setBackground(new java.awt.Color(255, 255, 255));

    cbCustomAutoVacuum.setBackground(new java.awt.Color(255, 255, 255));
    cbCustomAutoVacuum.setText(constBundle.getString("customAutoVacuum"));
    cbCustomAutoVacuum.setEnabled(false);
    cbCustomAutoVacuum.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbCustomAutoVacuumActionPerformed(evt);
        }
    });

    lblEnable.setText(constBundle.getString("enable"));

    cbEnable.setBackground(new java.awt.Color(255, 255, 255));
    cbEnable.setEnabled(false);

    lblCurrentValue.setText(constBundle.getString("currentValue"));

    txfdVacuumBaseThreshold.setEnabled(false);

    lblVacuumBaseThreshold.setText(constBundle.getString("vacuumBaseThreshold"));

    lblAnalyzeBaseThreshold.setText(constBundle.getString("analyzeBaseThreshold"));

    lblVacuumScaleFactor.setText(constBundle.getString("vacuumScaleFactor"));

    lblAnalyzeScaleFactor.setText(constBundle.getString("analyzeScaleFactor"));

    lblVacuumCostDelay.setText(constBundle.getString("vacuumCostDelay"));

    lblVacuumCostLimit.setText(constBundle.getString("vacuumCostLimit"));

    lblFreezeMinimumAge.setText(constBundle.getString("freezeMinimumAge"));

    lblFreezeMaximumAge.setText(constBundle.getString("freezeMaximumAge"));

    lblFreezeTableAge.setText(constBundle.getString("freezeTableAge"));

    txfdFreezeTableAge.setEnabled(false);

    txfdFreezeMaximumAge.setEnabled(false);

    txfdFreezeMinimumAge.setEnabled(false);

    txfdVacuumCostLimit.setEnabled(false);

    txfdVacuumCostDelay.setEnabled(false);

    txfdAnalyzeScaleFactor.setEnabled(false);

    txfdVacuumScaleFactor.setEnabled(false);

    txfdAnalyzeBaseThreshold.setEnabled(false);

    javax.swing.GroupLayout pnlTableLayout = new javax.swing.GroupLayout(pnlTable);
    pnlTable.setLayout(pnlTableLayout);
    pnlTableLayout.setHorizontalGroup(
        pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTableLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lblAnalyzeBaseThreshold, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblVacuumScaleFactor, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblAnalyzeScaleFactor, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblVacuumCostDelay, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblVacuumCostLimit, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblEnable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbCustomAutoVacuum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumBaseThreshold, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeTableAge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMaximumAge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMinimumAge, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbEnable)
                .addComponent(txfdVacuumBaseThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdAnalyzeBaseThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdVacuumScaleFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdAnalyzeScaleFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdVacuumCostDelay, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdVacuumCostLimit, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdFreezeMinimumAge, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdFreezeMaximumAge, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdFreezeTableAge, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblCurrentValue, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                .addComponent(lblVacuumBaseThresholdValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAnalyzeScaleFactorValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostLimitValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMinimumAgeValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMaximumAgeValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeTableAgeValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostDelayValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblAnalyzeBaseThresholdValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumScaleFactorValue, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );
    pnlTableLayout.setVerticalGroup(
        pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlTableLayout.createSequentialGroup()
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlTableLayout.createSequentialGroup()
                    .addComponent(cbCustomAutoVacuum)
                    .addGap(10, 10, 10)
                    .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbEnable)
                        .addComponent(lblEnable)))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlTableLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(lblCurrentValue)))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumBaseThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumBaseThreshold)
                .addComponent(lblVacuumBaseThresholdValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdAnalyzeBaseThreshold, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblAnalyzeBaseThreshold)
                .addComponent(lblAnalyzeBaseThresholdValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumScaleFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumScaleFactor)
                .addComponent(lblVacuumScaleFactorValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdAnalyzeScaleFactor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblAnalyzeScaleFactor)
                .addComponent(lblAnalyzeScaleFactorValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostDelay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostDelay)
                .addComponent(lblVacuumCostDelayValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostLimit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostLimit)
                .addComponent(lblVacuumCostLimitValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMinimumAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMinimumAge)
                .addComponent(lblFreezeMinimumAgeValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMaximumAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMaximumAge)
                .addComponent(lblFreezeMaximumAgeValue))
            .addGap(10, 10, 10)
            .addGroup(pnlTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeTableAge, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeTableAge)
                .addComponent(lblFreezeTableAgeValue))
            .addContainerGap())
    );

    jtpMaterializedView.addTab(constBundle.getString("table"), pnlTable);

    pnlToastTable.setBackground(new java.awt.Color(255, 255, 255));

    cbCustomAutoVacuumToast.setBackground(new java.awt.Color(255, 255, 255));
    cbCustomAutoVacuumToast.setText(constBundle.getString("customAutoVacuum"));
    cbCustomAutoVacuumToast.setEnabled(false);
    cbCustomAutoVacuumToast.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbCustomAutoVacuumToastActionPerformed(evt);
        }
    });

    lblEnable1.setText(constBundle.getString("enable"));

    cbEnableToast.setBackground(new java.awt.Color(255, 255, 255));
    cbEnableToast.setEnabled(false);

    txfdVacuumBaseThresholdToast.setEnabled(false);

    lblVacuumBaseThreshold1.setText(constBundle.getString("vacuumBaseThreshold"));

    lblCurrentValue2.setText(constBundle.getString("currentValue"));

    lblVacuumScaleFactor1.setText(constBundle.getString("vacuumScaleFactor"));

    txfdVacuumScaleFactorToast.setEnabled(false);

    txfdVacuumCostDelayToast.setEnabled(false);

    lblVacuumCostDelay1.setText(constBundle.getString("vacuumCostDelay"));

    txfdVacuumCostLimitToast.setEnabled(false);

    lblVacuumCostLimit1.setText(constBundle.getString("vacuumCostLimit"));

    lblFreezeMinimumAge1.setText(constBundle.getString("freezeMinimumAge"));

    txfdFreezeMinimumAgeToast.setEnabled(false);

    txfdFreezeMaximumAgeToast.setEnabled(false);

    lblFreezeMaximumAge1.setText(constBundle.getString("freezeMaximumAge"));

    lblFreezeTableAge1.setText(constBundle.getString("freezeTableAge"));

    txfdFreezeTableAgeToast.setEnabled(false);

    javax.swing.GroupLayout pnlToastTableLayout = new javax.swing.GroupLayout(pnlToastTable);
    pnlToastTable.setLayout(pnlToastTableLayout);
    pnlToastTableLayout.setHorizontalGroup(
        pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlToastTableLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lblVacuumScaleFactor1, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblVacuumCostDelay1, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblVacuumCostLimit1, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                .addComponent(lblEnable1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbCustomAutoVacuumToast, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumBaseThreshold1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeTableAge1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMaximumAge1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMinimumAge1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdVacuumBaseThresholdToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdVacuumScaleFactorToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdVacuumCostDelayToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdVacuumCostLimitToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdFreezeMinimumAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdFreezeMaximumAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(txfdFreezeTableAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbEnableToast, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblCurrentValue2, javax.swing.GroupLayout.DEFAULT_SIZE, 73, Short.MAX_VALUE)
                .addComponent(lblVacuumBaseThresholdValue1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostLimitValue1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMinimumAgeValue1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeMaximumAgeValue1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblFreezeTableAgeValue1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumCostDelayValue1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblVacuumScaleFactorValue1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );
    pnlToastTableLayout.setVerticalGroup(
        pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlToastTableLayout.createSequentialGroup()
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlToastTableLayout.createSequentialGroup()
                    .addComponent(cbCustomAutoVacuumToast)
                    .addGap(10, 10, 10)
                    .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbEnableToast)
                        .addComponent(lblEnable1)))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlToastTableLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(lblCurrentValue2)))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumBaseThresholdToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumBaseThreshold1)
                .addComponent(lblVacuumBaseThresholdValue1))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVacuumScaleFactor1)
                .addComponent(txfdVacuumScaleFactorToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumScaleFactorValue1))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVacuumCostDelay1)
                .addComponent(txfdVacuumCostDelayToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostDelayValue1))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdVacuumCostLimitToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblVacuumCostLimit1)
                .addComponent(lblVacuumCostLimitValue1))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMinimumAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMinimumAge1)
                .addComponent(lblFreezeMinimumAgeValue1))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeMaximumAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeMaximumAge1)
                .addComponent(lblFreezeMaximumAgeValue1))
            .addGap(10, 10, 10)
            .addGroup(pnlToastTableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdFreezeTableAgeToast, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblFreezeTableAgeValue1)
                .addComponent(lblFreezeTableAge1))
            .addContainerGap())
    );

    jtpMaterializedView.addTab(constBundle.getString("toastTable"), pnlToastTable);

    javax.swing.GroupLayout pnlMaterializationLayout = new javax.swing.GroupLayout(pnlMaterialization);
    pnlMaterialization.setLayout(pnlMaterializationLayout);
    pnlMaterializationLayout.setHorizontalGroup(
        pnlMaterializationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlMaterializationLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlMaterializationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlMaterializationLayout.createSequentialGroup()
                    .addComponent(lblMaterializedView, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cbMaterializedView)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jtpMaterializedView))
            .addContainerGap())
    );
    pnlMaterializationLayout.setVerticalGroup(
        pnlMaterializationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlMaterializationLayout.createSequentialGroup()
            .addGap(6, 6, 6)
            .addGroup(pnlMaterializationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbMaterializedView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblMaterializedView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jtpMaterializedView)
            .addContainerGap())
    );

    jtpMaterializedView.getAccessibleContext().setAccessibleName(constBundle.getString("table"));

    jtp.addTab(constBundle.getString("materialization"), pnlMaterialization);

    pnlPrivileges.setBackground(new java.awt.Color(255, 255, 255));
    pnlPrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlPrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

    tblPrivileges.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblPrivileges.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblPrivileges.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblPrivilegesMousePressed(evt);
        }
    });
    jScrollPane4.setViewportView(tblPrivileges);
    if (tblPrivileges.getColumnModel().getColumnCount() > 0)
    {
        tblPrivileges.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("user")+"/"+constBundle.getString("group"));
        tblPrivileges.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("privileges"));
    }

    btnAddChangePrivileges.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangePrivileges.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangePrivilegesActionPerformed(evt);
        }
    });

    btnRemovePrivilege.setText(constBundle.getString("remove"));
    btnRemovePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemovePrivilegeActionPerformed(evt);
        }
    });

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("privileges"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("宋体", 0, 12), new java.awt.Color(204, 204, 204))); // NOI18N

    lblRole.setText(constBundle.getString("role"));

    cbbRole.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbRoleItemStateChanged(evt);
        }
    });

    cbAll.setText("ALL");
    cbAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbAllActionPerformed(evt);
        }
    });

    cbUpdate.setText("UPDATE");

    cbInsert.setText("INSERT");

    cbOptionAll.setText("WITH GRANT OPTION");
    cbOptionAll.setEnabled(false);

    cbSelect.setText("SELECT");

    cbOptionUpdate.setText("WITH GRANT OPTION");
    cbOptionUpdate.setEnabled(false);

    cbOptionInsert.setText("WITH GRANT OPTION");
    cbOptionInsert.setEnabled(false);

    cbOptionSelect.setText("WITH GRANT OPTION");
    cbOptionSelect.setEnabled(false);

    cbDelete.setText("DELETE");

    cbOptionDelete.setText("WITH GRANT OPTION");
    cbOptionDelete.setEnabled(false);

    cbRule.setText("RULE");
    cbRule.setEnabled(false);

    cbOptionRule.setText("WITH GRANT OPTION");
    cbOptionRule.setEnabled(false);

    cbReferences.setText("REFERENCES");

    cbOptionReferences.setText("WITH GRANT OPTION");
    cbOptionReferences.setEnabled(false);

    cbTrigger.setText("TRIGGER");

    cbOptionTrigger.setText("WITH GRANT OPTION");
    cbOptionTrigger.setEnabled(false);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbRole, 0, 453, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbInsert)
                        .addComponent(cbSelect)
                        .addComponent(cbUpdate)
                        .addComponent(cbAll)
                        .addComponent(cbDelete)
                        .addComponent(cbRule)
                        .addComponent(cbReferences)
                        .addComponent(cbTrigger))
                    .addGap(165, 165, 165)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbOptionUpdate)
                        .addComponent(cbOptionInsert)
                        .addComponent(cbOptionAll)
                        .addComponent(cbOptionSelect)
                        .addComponent(cbOptionDelete)
                        .addComponent(cbOptionRule)
                        .addComponent(cbOptionReferences)
                        .addComponent(cbOptionTrigger))))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblRole))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbOptionAll)
                .addComponent(cbAll))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbInsert)
                .addComponent(cbOptionInsert))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbSelect)
                .addComponent(cbOptionSelect))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbUpdate)
                .addComponent(cbOptionUpdate))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbDelete)
                .addComponent(cbOptionDelete))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbRule)
                .addComponent(cbOptionRule))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbReferences)
                .addComponent(cbOptionReferences))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbTrigger)
                .addComponent(cbOptionTrigger))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlPrivilegesLayout = new javax.swing.GroupLayout(pnlPrivileges);
    pnlPrivileges.setLayout(pnlPrivilegesLayout);
    pnlPrivilegesLayout.setHorizontalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlPrivilegesLayout.createSequentialGroup()
                    .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlPrivilegesLayout.setVerticalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("privileges"), pnlPrivileges);
    pnlPrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    lblProvider.setText(constBundle.getString("provider"));

    tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    ));
    tblSecurityLabels.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblSecurityLabelsMousePressed(evt);
        }
    });
    jScrollPane3.setViewportView(tblSecurityLabels);
    if (tblSecurityLabels.getColumnModel().getColumnCount() > 0)
    {
        tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveSecurityLabelActionPerformed(evt);
        }
    });

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeSecurityLabelActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSecurityLabelLayout.createSequentialGroup()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addContainerGap(6, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
                .addComponent(txfdProvider))
            .addContainerGap())
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
                    .addGap(10, 10, 10))))
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(18, 18, 18)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addContainerGap(47, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.PAGE_START);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
 
    /* 检测用户按下的 */
    private void txfdValueKeyTyped(KeyEvent evt)
    {
        //those textfields could only be enter number.
        char keyCh = evt.getKeyChar();
        logger.info("keych:" + keyCh);
        if (((keyCh < '0') || (keyCh > '9')) && (keyCh != '.'))
        {
            if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }
    }
    
    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);    
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnAddChangePrivilegesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangePrivilegesActionPerformed
    {//GEN-HEADEREND:event_btnAddChangePrivilegesActionPerformed
        // TODO add your handling code here:       
        String usergroup = cbbRole.getSelectedItem().toString();
        StringBuilder privileges = new StringBuilder();
        if (cbInsert.isSelected())
        {
            privileges.append("a");
        }
        if (cbOptionInsert.isSelected())
        {
            privileges.append("*");
        }
        if (cbSelect.isSelected())
        {
            privileges.append("r");
        }
        if (cbOptionSelect.isSelected())
        {
            privileges.append("*");
        }
        if (cbUpdate.isSelected())
        {
            privileges.append("w");
        }
        if (cbOptionUpdate.isSelected())
        {
            privileges.append("*");
        }
        if (cbDelete.isSelected())
        {
            privileges.append("d");
        }
        if (cbOptionDelete.isSelected())
        {
            privileges.append("*");
        }
        if (cbReferences.isSelected())
        {
            privileges.append("x");
        }
        if (cbOptionReferences.isSelected())
        {
            privileges.append("*");
        }
        if (cbTrigger.isSelected())
        {
            privileges.append("t");
        }
        if (cbOptionTrigger.isSelected())
        {
            privileges.append("*");
        }

        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String usergroupOld = tableModel.getValueAt(i, 0).toString();
            logger.info("usergroupOld=" + usergroupOld + usergroupOld.equals(usergroup));
            if (usergroupOld.equals(usergroup))
            {               
                tableModel.setValueAt(privileges.toString(), i, 1);
                isExist = true;
                break;
            }
        }
        if (!isExist)
        {
            String[] p = new String[2];
            p[0] = usergroup;
            p[1] = privileges.toString();
            tableModel.addRow(p);
        }         
    }//GEN-LAST:event_btnAddChangePrivilegesActionPerformed

    private void btnRemovePrivilegeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemovePrivilegeActionPerformed
    {//GEN-HEADEREND:event_btnRemovePrivilegeActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int selectedRow = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        tableModel.removeRow(selectedRow); 
    }//GEN-LAST:event_btnRemovePrivilegeActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.VIEW);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
         logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void cbAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbAllActionPerformed
    {//GEN-HEADEREND:event_cbAllActionPerformed
        // TODO add your handling code here:
        logger.info("command"+evt.getSource().toString());
        if(cbAll.isSelected())
        {     
            cbInsert.setSelected(true);
            cbInsert.setEnabled(false);
            cbSelect.setSelected(true);
            cbSelect.setEnabled(false);
            cbDelete.setSelected(true);
            cbDelete.setEnabled(false);
            cbUpdate.setSelected(true);
            cbUpdate.setEnabled(false); 
            cbReferences.setSelected(true);
            cbReferences.setEnabled(false); 
            cbTrigger.setSelected(true);
            cbTrigger.setEnabled(false); 
            if (cbbRole.getSelectedItem().toString().equals("public"))
            {
                cbOptionAll.setEnabled(false);
                cbOptionAll.setSelected(false);
                cbOptionInsert.setSelected(false);
                cbOptionInsert.setEnabled(false);
                cbOptionSelect.setEnabled(false);
                cbOptionSelect.setSelected(false);
                cbOptionUpdate.setEnabled(false);
                cbOptionUpdate.setSelected(false);
                cbOptionDelete.setEnabled(false);
                cbOptionDelete.setSelected(false);
                cbOptionReferences.setEnabled(false);
                cbOptionReferences.setSelected(false);
            } 
            else
            {
                cbOptionAll.setEnabled(true);
                cbInsert.setEnabled(false);
                cbOptionSelect.setEnabled(false);
                cbOptionUpdate.setEnabled(false);
                cbOptionDelete.setEnabled(false);
                cbOptionReferences.setEnabled(false);
                cbOptionTrigger.setEnabled(false);
            }
        }
        else
        {       
            cbInsert.setSelected(false);
            cbInsert.setEnabled(true);
            cbSelect.setSelected(false);
            cbSelect.setEnabled(true);
            cbDelete.setSelected(false);
            cbDelete.setEnabled(true);
            cbUpdate.setSelected(false);
            cbUpdate.setEnabled(true);
            cbDelete.setSelected(false);
            cbDelete.setEnabled(true);
            cbReferences.setSelected(false);
            cbReferences.setEnabled(true);
            cbTrigger.setSelected(false);
            cbTrigger.setEnabled(true);
            if (!cbbRole.getSelectedItem().toString().equals("public"))
            {        
                cbOptionAll.setSelected(false);
                cbOptionAll.setEnabled(false); 
                cbOptionInsert.setSelected(false);
                cbOptionInsert.setEnabled(true);
                cbOptionSelect.setSelected(false);
                cbOptionSelect.setEnabled(true);
                cbOptionUpdate.setSelected(false);
                cbOptionUpdate.setEnabled(true);
                cbOptionDelete.setSelected(false);
                cbOptionDelete.setEnabled(true);
                cbOptionReferences.setSelected(false);
                cbOptionReferences.setEnabled(true);
                cbOptionTrigger.setSelected(false);
                cbOptionTrigger.setEnabled(true);
            }
        }
               
    }//GEN-LAST:event_cbAllActionPerformed

    private void cbbRoleItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbRoleItemStateChanged
    {//GEN-HEADEREND:event_cbbRoleItemStateChanged
        // TODO add your handling code here:
        String role = cbbRole.getSelectedItem().toString();
        logger.info("role:" + role );
        if(role.equals("public"))
        {
            cbOptionAll.setEnabled(false);
            cbOptionAll.setSelected(false);
            cbOptionInsert.setEnabled(false);
            cbOptionInsert.setSelected(false);
            cbOptionSelect.setEnabled(false);
            cbOptionSelect.setSelected(false);
            cbOptionUpdate.setEnabled(false);
            cbOptionUpdate.setSelected(false);
            cbOptionDelete.setEnabled(false);
            cbOptionDelete.setSelected(false);
            cbOptionReferences.setEnabled(false);
            cbOptionReferences.setSelected(false);
            cbOptionTrigger.setEnabled(false);
            cbOptionTrigger.setSelected(false);
        }
        else
        {
           if(cbAll.isSelected())
           {
               cbOptionAll.setEnabled(true);
           }
           else
           {
                cbOptionAll.setSelected(false);                
                cbOptionAll.setEnabled(false);
                
                cbOptionInsert.setEnabled(true);                
                cbOptionSelect.setEnabled(true);
                cbOptionUpdate.setEnabled(true);
                cbOptionDelete.setEnabled(true);
                cbOptionReferences.setEnabled(true);
                cbOptionTrigger.setEnabled(true);
           }
        }
    }//GEN-LAST:event_cbbRoleItemStateChanged

    private void tblPrivilegesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblPrivilegesMousePressed
    {//GEN-HEADEREND:event_tblPrivilegesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int row = tblPrivileges.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        else
        {
            String user = tblPrivileges.getValueAt(row, 0).toString();
            cbbRole.setSelectedItem(user);
            String p = tblPrivileges.getValueAt(row, 1).toString();
            cbAll.setSelected(false);
            TreeController tc = TreeController.getInstance();
            String[] tags = tc.getPrivilegeTags(p);
            if(tags!=null)
            {
                
            }
         }
    }//GEN-LAST:event_tblPrivilegesMousePressed

    private void tblSecurityLabelsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblSecurityLabelsMousePressed
    {//GEN-HEADEREND:event_tblSecurityLabelsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press Security Label table");
        int row = tblSecurityLabels.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblSecurityLabels.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabels.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }//GEN-LAST:event_tblSecurityLabelsMousePressed

    private void cbMaterializedViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbMaterializedViewActionPerformed
    {//GEN-HEADEREND:event_cbMaterializedViewActionPerformed
        // TODO add your handling code here:
        boolean enable = cbMaterializedView.isSelected();
        logger.info("cbMaterializedView Enable=" + enable);
        cbSecurityBarrier.setEnabled(!enable);
        cbbTablespace.setEnabled(enable);
        txfdFillFactor.setEnabled(enable);
        cbWithData.setEnabled(enable);
        cbCustomAutoVacuum.setEnabled(enable);
        cbCustomAutoVacuumToast.setEnabled(enable);
        if (enable)
        {
            if (cbSecurityBarrier.isSelected())
            {
                JOptionPane.showMessageDialog(this,
                        constBundle.getString("securityBarrierWarnMessage"),
                        constBundle.getString("view"), JOptionPane.WARNING_MESSAGE);
                cbSecurityBarrier.setSelected(false);
            }
        } else
        {
            cbEnable.setEnabled(false);
            txfdVacuumBaseThreshold.setEnabled(false);
            txfdAnalyzeBaseThreshold.setEnabled(false);
            txfdVacuumScaleFactor.setEnabled(false);
            txfdAnalyzeScaleFactor.setEnabled(false);
            txfdVacuumCostDelay.setEnabled(false);
            txfdVacuumCostLimit.setEnabled(false);
            txfdFreezeMinimumAge.setEnabled(false);
            txfdFreezeMaximumAge.setEnabled(false);
            txfdFreezeTableAge.setEnabled(false);
            cbEnableToast.setEnabled(false);
            txfdVacuumBaseThresholdToast.setEnabled(false);
            txfdVacuumScaleFactorToast.setEnabled(false);
            txfdVacuumCostDelayToast.setEnabled(false);
            txfdVacuumCostLimitToast.setEnabled(false);
            txfdFreezeMinimumAgeToast.setEnabled(false);
            txfdFreezeMaximumAgeToast.setEnabled(false);
            txfdFreezeTableAgeToast.setEnabled(false);
        }      
    }//GEN-LAST:event_cbMaterializedViewActionPerformed
    
    private void cbCustomAutoVacuumActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbCustomAutoVacuumActionPerformed
    {//GEN-HEADEREND:event_cbCustomAutoVacuumActionPerformed
        // TODO add your handling code here:
        boolean enable = cbCustomAutoVacuum.isSelected();
        cbEnable.setEnabled(enable);
        txfdVacuumBaseThreshold.setEnabled(enable);
        txfdAnalyzeBaseThreshold.setEnabled(enable);
        txfdVacuumScaleFactor.setEnabled(enable);
        txfdAnalyzeScaleFactor.setEnabled(enable);
        txfdVacuumCostDelay.setEnabled(enable);
        txfdVacuumCostLimit.setEnabled(enable);
        txfdFreezeMinimumAge.setEnabled(enable);
        txfdFreezeMaximumAge.setEnabled(enable);
        txfdFreezeTableAge.setEnabled(enable);  
    }//GEN-LAST:event_cbCustomAutoVacuumActionPerformed

    private void cbCustomAutoVacuumToastActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbCustomAutoVacuumToastActionPerformed
    {//GEN-HEADEREND:event_cbCustomAutoVacuumToastActionPerformed
        // TODO add your handling code here:
        boolean enable = cbCustomAutoVacuumToast.isSelected();
        logger.info("enable="+enable);
        cbEnableToast.setEnabled(enable);
        txfdVacuumBaseThresholdToast.setEnabled(enable);
        txfdVacuumScaleFactorToast.setEnabled(enable);
        txfdVacuumCostDelayToast.setEnabled(enable);
        txfdVacuumCostLimitToast.setEnabled(enable);
        txfdFreezeMinimumAgeToast.setEnabled(enable);
        txfdFreezeMaximumAgeToast.setEnabled(enable);
        txfdFreezeTableAgeToast.setEnabled(enable);
    }//GEN-LAST:event_cbCustomAutoVacuumToastActionPerformed

    //create
    private void btnOkEnableForAddAction(KeyEvent evt)
    {
        String name = txfdName.getText();
        String defineStmt = ttarDefinition.getText();
        if (name == null || name.isEmpty()
                || defineStmt == null || defineStmt.isEmpty())
        {
            btnOK.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
        }
    }
    private void jtpForAddStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
        if (index == 5)
        {
            SyntaxController sc = SyntaxController.getInstance();
            if (txfdName.getText() == null || txfdName.getText().isEmpty()
                    || ttarDefinition.getText() == null || ttarDefinition.getText().isEmpty())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
                return;
            }
            tpDefinitionSQL.setText("");
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getViewInfo()));
        }
    }
    private void btnOKForAddActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        logger.info("dbInfo:" + txfdName.getText() + "," + cbbOwner.getSelectedItem());
        TreeController tc = TreeController.getInstance();
        try
        {
            tc.createObj(helperInfo, this.getViewInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }                              
    
    //change
    private void btnOkEnableForChange()
    {
        String name = txfdName.getText();
        String define = ttarDefinition.getText();
        String acl = this.getACL();
        if (name == null || name.isEmpty() || define == null || define.isEmpty())
        {
            btnOK.setEnabled(false);
        } else
        {
            if (!name.equals(viewInfo.getName()))
            {
                logger.info("name changed");
                btnOK.setEnabled(true);
            } else if (cbbOwner.getSelectedItem() != null && !cbbOwner.getSelectedItem().equals(viewInfo.getOwner()))
            {
                logger.info("owner changed");
                btnOK.setEnabled(true);
            } else if (!txarComment.getText().equals(viewInfo.getComment())
                    && !(txarComment.getText().isEmpty() && viewInfo.getComment() == null))
            {
                logger.info("comment changed");
                btnOK.setEnabled(true);
            } else if (!define.equals(viewInfo.getDefinition()))
            {
                logger.info("define changed");
                btnOK.setEnabled(true);
            } else if (cbSecurityBarrier.isSelected() != viewInfo.isSecurityBarrier())
            {
                logger.info("Security Barrier");
                btnOK.setEnabled(true);
            } else if (!acl.equals(viewInfo.getAcl())
                    && !(acl.isEmpty() && viewInfo.getAcl() == null))
            {
                logger.info("ACL changed");
                btnOK.setEnabled(true);
            } else if (cbMaterializedView.isSelected())
            {
                AutoVacuumDTO avc = viewInfo.getAutoVacuumInfo();
                if (!cbbTablespace.getSelectedItem().equals(viewInfo.getTablespace())                       
                        || cbWithData.isSelected() != viewInfo.isWithData())
                {
                    logger.info("option para changed");
                    btnOK.setEnabled(true);
                }else if(!txfdFillFactor.getText().trim().isEmpty() 
                        && !txfdFillFactor.getText().trim().equals(viewInfo.getFillFactor()))
                {
                     logger.info("fillfactor changed");
                     btnOK.setEnabled(true);
                }
                else if (cbCustomAutoVacuum.isSelected() != avc.isCustomAutoVacuum()
                        || cbEnable.isSelected() != avc.isEnable()
                        || (cbCustomAutoVacuum.isSelected() == true  && this.checkAutoVacuumChange(avc)))
                {
                    logger.info("table para changed");
                    btnOK.setEnabled(true);
                } else if (cbCustomAutoVacuumToast.isSelected() != avc.isCustomAutoVacuumToast()
                        || cbEnableToast.isSelected() != avc.isEnableToast()
                        || (cbCustomAutoVacuumToast.isSelected()==true  && this.checkAutoVacuumToastChange(avc)))
                {
                    logger.info("toast taable para changed");
                    btnOK.setEnabled(true);
                } else
                {
                    btnOK.setEnabled(false);
                }
            } else
            {
                btnOK.setEnabled(false);
            }
        }
    }
    private boolean checkAutoVacuumChange(AutoVacuumDTO avc)
    {
        if ((!txfdVacuumBaseThreshold.getText().trim().isEmpty() && !txfdVacuumBaseThreshold.getText().trim().equals(avc.getVacuumBaseThreshold()))
                || (!txfdAnalyzeBaseThreshold.getText().isEmpty() && !txfdAnalyzeBaseThreshold.getText().trim().equals(avc.getAnalyzeBaseThreshold()))
                || (!txfdVacuumScaleFactor.getText().isEmpty() && !txfdVacuumScaleFactor.getText().trim().equals(avc.getVacuumScaleFactor()))
                || (!txfdVacuumCostDelay.getText().isEmpty() && !txfdVacuumCostDelay.getText().trim().equals(avc.getVacuumCostDelay()))
                || (!txfdVacuumCostLimit.getText().isEmpty() && !txfdVacuumCostLimit.getText().trim().equals(avc.getVacuumCostLimit()))
                || (!txfdFreezeMinimumAge.getText().isEmpty() && !txfdFreezeMinimumAge.getText().equals(avc.getFreezeMaximumAge()))
                || (!txfdFreezeMaximumAge.getText().isEmpty() && !txfdFreezeMaximumAge.getText().equals(avc.getFreezeMaximumAge()))
                || (!txfdFreezeTableAge.getText().isEmpty() && !txfdFreezeTableAge.getText().equals(avc.getFreezeTableAge())))
        {
            return true;
        } else
        {
            return false;
        }
    }
    private boolean checkAutoVacuumToastChange(AutoVacuumDTO avc)
    {
        if (!txfdVacuumBaseThresholdToast.getText().trim().equals(avc.getVacuumBaseThresholdToast())
                || !txfdVacuumScaleFactorToast.getText().trim().equals(avc.getVacuumScaleFactorToast())
                || !txfdVacuumCostDelayToast.getText().trim().equals(avc.getVacuumCostDelayToast())
                || !txfdVacuumCostLimitToast.getText().trim().equals(avc.getVacuumCostLimitToast())
                || !txfdFreezeMinimumAgeToast.getText().equals(avc.getFreezeMaximumAgeToast())
                || !txfdFreezeMaximumAgeToast.getText().equals(avc.getFreezeMaximumAgeToast())
                || !txfdFreezeTableAgeToast.getText().equals(avc.getFreezeTableAgeToast()))
        {
            return true;
        }else
        {
            return false;
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
        if (index == 5)
        {
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
                return;
            }
            tpDefinitionSQL.setText("");
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getViewChangeSQL(this.viewInfo, this.getViewInfo()));
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        logger.info(txfdName.getText());
        TreeController tc = TreeController.getInstance();
        try
        {
            tc.executeSql(helperInfo,helperInfo.getDbName(), tc.getViewChangeSQL(this.viewInfo, this.getViewInfo()));
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    
    private String getACL()
    {
         return AclUtil.getACL(cbbOwner.getSelectedItem().toString(), helperInfo, viewInfo, (DefaultTableModel) tblPrivileges.getModel());
    }
    public ViewInfoDTO getViewInfo()
    {
        ViewInfoDTO newview = new ViewInfoDTO();
        newview.setHelperInfo(helperInfo);
        if (this.viewInfo != null)
        {
            newview.setOid(this.viewInfo.getOid());
        }
        if (cbbSchema.getSelectedItem() != null && !cbbSchema.getSelectedItem().toString().isEmpty())
        {
            newview.setSchema(cbbSchema.getSelectedItem().toString());
        } else
        {
            newview.setSchema(helperInfo.getSchema());
            newview.setSchemaOid(helperInfo.getSchemaOid());
        }   
        newview.setName(txfdName.getText());
        if (cbbOwner.getSelectedItem() == null || cbbOwner.getSelectedItem().toString().isEmpty())
        {
            newview.setOwner(helperInfo.getUser());
        } else
        {
            newview.setOwner(cbbOwner.getSelectedItem().toString());
        }
        newview.setComment(txarComment.getText());
        logger.info("cbSecurityBarrier.isSelected()" + cbSecurityBarrier.isSelected());
        newview.setSecurityBarrier(cbSecurityBarrier.isSelected());
        newview.setDefinition(ttarDefinition.getText());
        //for privilege       
        newview.setAcl(this.getACL());
        //for security label
        /*
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.info("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        newview.setSecurityLabelInfoList(securityList);
        */
        newview.setMaterializedView(cbMaterializedView.isSelected());
        if (cbMaterializedView.isSelected())
        {          
            newview.setTablespace(cbbTablespace.getSelectedItem().toString());
            newview.setFillFactor(txfdFillFactor.getText().trim());
            newview.setWithData(cbWithData.isSelected());
            AutoVacuumDTO autoVacuum = new AutoVacuumDTO();
            //the following rule different from pgadmin
            autoVacuum.setCustomAutoVacuum(cbCustomAutoVacuum.isSelected());
            if (cbCustomAutoVacuum.isSelected())
            {
                autoVacuum.setEnable(cbEnable.isSelected());
                autoVacuum.setVacuumBaseThreshold(txfdVacuumBaseThreshold.getText());
                autoVacuum.setAnalyzeBaseThreshold(txfdAnalyzeBaseThreshold.getText());
                autoVacuum.setVacuumScaleFactor(txfdVacuumScaleFactor.getText());
                autoVacuum.setAnalyzeScaleFactor(txfdAnalyzeScaleFactor.getText());
                autoVacuum.setVacuumCostDelay(txfdVacuumCostDelay.getText());
                autoVacuum.setVacuumCostLimit(txfdVacuumCostLimit.getText());
                autoVacuum.setFreezeMinimumAge(txfdFreezeMinimumAge.getText());
                autoVacuum.setFreezeMaximumAge(txfdFreezeMaximumAge.getText());
                autoVacuum.setFreezeTableAge(txfdFreezeTableAge.getText());
            }
            autoVacuum.setCustomAutoVacuumToast(cbCustomAutoVacuumToast.isSelected());
            if (cbCustomAutoVacuumToast.isSelected())
            {
                autoVacuum.setEnableToast(cbEnableToast.isSelected());
                autoVacuum.setVacuumBaseThresholdToast(txfdVacuumBaseThresholdToast.getText());
                autoVacuum.setVacuumScaleFactorToast(txfdVacuumScaleFactorToast.getText());
                autoVacuum.setVacuumCostDelayToast(txfdVacuumCostDelayToast.getText());
                autoVacuum.setVacuumCostLimitToast(txfdVacuumCostLimitToast.getText());
                autoVacuum.setFreezeMinimumAgeToast(txfdFreezeMinimumAgeToast.getText());
                autoVacuum.setFreezeMaximumAgeToast(txfdFreezeMaximumAgeToast.getText());
                autoVacuum.setFreezeTableAgeToast(txfdFreezeTableAgeToast.getText());
            }
            newview.setAutoVacuumInfo(autoVacuum);
        }
        return newview;
    }    
    public boolean isSuccess()
    {
        return isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangePrivileges;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemovePrivilege;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JCheckBox cbAll;
    private javax.swing.JCheckBox cbCustomAutoVacuum;
    private javax.swing.JCheckBox cbCustomAutoVacuumToast;
    private javax.swing.JCheckBox cbDelete;
    private javax.swing.JCheckBox cbEnable;
    private javax.swing.JCheckBox cbEnableToast;
    private javax.swing.JCheckBox cbInsert;
    private javax.swing.JCheckBox cbMaterializedView;
    private javax.swing.JCheckBox cbOptionAll;
    private javax.swing.JCheckBox cbOptionDelete;
    private javax.swing.JCheckBox cbOptionInsert;
    private javax.swing.JCheckBox cbOptionReferences;
    private javax.swing.JCheckBox cbOptionRule;
    private javax.swing.JCheckBox cbOptionSelect;
    private javax.swing.JCheckBox cbOptionTrigger;
    private javax.swing.JCheckBox cbOptionUpdate;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbReferences;
    private javax.swing.JCheckBox cbRule;
    private javax.swing.JCheckBox cbSecurityBarrier;
    private javax.swing.JCheckBox cbSelect;
    private javax.swing.JCheckBox cbTrigger;
    private javax.swing.JCheckBox cbUpdate;
    private javax.swing.JCheckBox cbWithData;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.JComboBox cbbRole;
    private javax.swing.JComboBox cbbSchema;
    private javax.swing.JComboBox cbbTablespace;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JTabbedPane jtpMaterializedView;
    private javax.swing.JLabel lblAnalyzeBaseThreshold;
    private javax.swing.JLabel lblAnalyzeBaseThresholdValue;
    private javax.swing.JLabel lblAnalyzeScaleFactor;
    private javax.swing.JLabel lblAnalyzeScaleFactorValue;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblCurrentValue;
    private javax.swing.JLabel lblCurrentValue2;
    private javax.swing.JLabel lblDefinition;
    private javax.swing.JLabel lblEnable;
    private javax.swing.JLabel lblEnable1;
    private javax.swing.JLabel lblFillFactor;
    private javax.swing.JLabel lblFreezeMaximumAge;
    private javax.swing.JLabel lblFreezeMaximumAge1;
    private javax.swing.JLabel lblFreezeMaximumAgeValue;
    private javax.swing.JLabel lblFreezeMaximumAgeValue1;
    private javax.swing.JLabel lblFreezeMinimumAge;
    private javax.swing.JLabel lblFreezeMinimumAge1;
    private javax.swing.JLabel lblFreezeMinimumAgeValue;
    private javax.swing.JLabel lblFreezeMinimumAgeValue1;
    private javax.swing.JLabel lblFreezeTableAge;
    private javax.swing.JLabel lblFreezeTableAge1;
    private javax.swing.JLabel lblFreezeTableAgeValue;
    private javax.swing.JLabel lblFreezeTableAgeValue1;
    private javax.swing.JLabel lblMaterializedView;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSchema;
    private javax.swing.JLabel lblSecurityBarrier;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblTablesapce;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JLabel lblVacuumBaseThreshold;
    private javax.swing.JLabel lblVacuumBaseThreshold1;
    private javax.swing.JLabel lblVacuumBaseThresholdValue;
    private javax.swing.JLabel lblVacuumBaseThresholdValue1;
    private javax.swing.JLabel lblVacuumCostDelay;
    private javax.swing.JLabel lblVacuumCostDelay1;
    private javax.swing.JLabel lblVacuumCostDelayValue;
    private javax.swing.JLabel lblVacuumCostDelayValue1;
    private javax.swing.JLabel lblVacuumCostLimit;
    private javax.swing.JLabel lblVacuumCostLimit1;
    private javax.swing.JLabel lblVacuumCostLimitValue;
    private javax.swing.JLabel lblVacuumCostLimitValue1;
    private javax.swing.JLabel lblVacuumScaleFactor;
    private javax.swing.JLabel lblVacuumScaleFactor1;
    private javax.swing.JLabel lblVacuumScaleFactorValue;
    private javax.swing.JLabel lblVacuumScaleFactorValue1;
    private javax.swing.JLabel lblWithData;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlMaterialization;
    private javax.swing.JPanel pnlOption;
    private javax.swing.JPanel pnlPrivileges;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JPanel pnlTable;
    private javax.swing.JPanel pnlToastTable;
    private javax.swing.JScrollPane spAlterSQL1;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblPrivileges;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarDefinition;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdAnalyzeBaseThreshold;
    private javax.swing.JTextField txfdAnalyzeScaleFactor;
    private javax.swing.JTextField txfdFillFactor;
    private javax.swing.JTextField txfdFreezeMaximumAge;
    private javax.swing.JTextField txfdFreezeMaximumAgeToast;
    private javax.swing.JTextField txfdFreezeMinimumAge;
    private javax.swing.JTextField txfdFreezeMinimumAgeToast;
    private javax.swing.JTextField txfdFreezeTableAge;
    private javax.swing.JTextField txfdFreezeTableAgeToast;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdUseSlony;
    private javax.swing.JTextField txfdVacuumBaseThreshold;
    private javax.swing.JTextField txfdVacuumBaseThresholdToast;
    private javax.swing.JTextField txfdVacuumCostDelay;
    private javax.swing.JTextField txfdVacuumCostDelayToast;
    private javax.swing.JTextField txfdVacuumCostLimit;
    private javax.swing.JTextField txfdVacuumCostLimitToast;
    private javax.swing.JTextField txfdVacuumScaleFactor;
    private javax.swing.JTextField txfdVacuumScaleFactorToast;
    // End of variables declaration//GEN-END:variables
}
