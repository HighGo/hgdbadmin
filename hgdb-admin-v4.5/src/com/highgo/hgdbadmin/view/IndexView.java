/* ------------------------------------------------ 
* 
* File: IndexView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\IndexView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.ColInfoDTO;
import com.highgo.hgdbadmin.model.ColItemInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.IndexInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * 
 * index name can be ommited,
 * column(s) must be added.
 * 
 */
public class IndexView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private HelperInfoDTO helperInfo;
    private IndexInfoDTO indexInfo;
    private boolean isSuccess=false;
    
    //add
    public IndexView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;      
        this.indexInfo = null;
        initComponents();
        this.setTitle(constBundle.getString("addIndex"));
        this.setNormalInfo(false);
        
        //define action
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAdd();
            }
        });
        tblColumns.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent evt)
            {
                btnOkEnableForAdd();
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpStateChangedForAdd(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformedForAdd(e);
            }
        });
    }
    
    //property
    public IndexView(JFrame parent, boolean modal, IndexInfoDTO indexInfo)
    {
        super(parent, modal);              
        this.indexInfo = indexInfo;
        this.helperInfo = indexInfo.getHelperInfo();
        initComponents();
        this.setTitle(constBundle.getString("indexes") + " " + indexInfo.getName());
        this.setNormalInfo(true);          
        this.setProperty();
        //define action
        KeyAdapter btnOkEnableAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnabledForChange();
            }
        };
        txfdName.addKeyListener(btnOkEnableAdapter);
        txarComment.addKeyListener(btnOkEnableAdapter);
        txfdFillFactor.addKeyListener(btnOkEnableAdapter);
        cbbTablespace.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        cbClustered.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnabledForChange();
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpStateChangedForChange(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformedForChange(e);
            }
        });
    }
    
    private void setProperty()
    {
        txfdName.setText(indexInfo.getName());
        txarComment.setText(indexInfo.getComment());
        cbbTablespace.setSelectedItem(indexInfo.getTablespace());
        //access method cannot be changed
        cbbAccessMethod.setSelectedItem(indexInfo.getAccessMethod());
        cbbAccessMethod.setEnabled(false);
        txfdFillFactor.setText(indexInfo.getFillFactor());
        //whether unique cannot be changed
        cbUnique.setSelected(indexInfo.isUnique());
        cbUnique.setEnabled(false);
        cbClustered.setSelected(indexInfo.isClustered());
        cbClustered.setEnabled(true);
        //concurrent build cannot be builded
        cbConcurrentBuild.setSelected(indexInfo.isConcurrently());
        cbConcurrentBuild.setEnabled(false);
        //constraint cannot be changed
        ttarConstraint.setText(indexInfo.getConstraint());
        ttarConstraint.setEnabled(false);
        //column cannot be changed
        List<ColInfoDTO> clist = indexInfo.getColInfoList();
        if (clist != null)
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            for (ColInfoDTO columnInfo : clist)
            {
                String[] v = new String[5];
                v[0] = columnInfo.getName();
                v[1] = columnInfo.getOrder();
                v[2] = columnInfo.getNullsWhen();
                if (columnInfo.isDefaultOpClass())
                {
                    v[3] = "";
                } else
                {
                    v[3] = columnInfo.getOperatorClass();
                }
                v[4] = columnInfo.getCollation();
                tableModel.addRow(v);
            }
        }
        btnAddChangeColumn.setEnabled(false);
        btnRemoveColumn.setEnabled(false);
        cbbColumn.setEnabled(false);
        cbbCollation.setEnabled(false);
        cbbOperatorClass.setEnabled(false);
        rbAsc.setEnabled(false);
        rbDesc.setEnabled(false);
        rbFirst.setEnabled(false);
        rbLast.setEnabled(false);
    }
    private void setNormalInfo(boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
        cbbCollation.setEnabled(tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0));
        try
        {
            cbbTablespace.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "tablespace")));
            if (isChange)
            {
                cbbTablespace.removeItemAt(0);
            } else
            {
                cbbTablespace.setSelectedIndex(0);
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        pnlSQL.getAccessibleContext().setAccessibleName("SQL");
        String[] accessMethod = new String[6];
        accessMethod[0] = "";
        accessMethod[1] = "btree";
        accessMethod[2] = "hash";
        accessMethod[3] = "gist";
        accessMethod[4] = "gin";
        accessMethod[5] = "spgist";
        cbbAccessMethod.setModel(new DefaultComboBoxModel(accessMethod));
        cbbAccessMethod.setSelectedIndex(0);

        String[] collation = new String[4];
        collation[0] = "";
        collation[1] = "pg_catalog.\"C\"";
        collation[2] = "pg_catalog.\"POSIX\"";
        collation[3] = "pg_catalog.\"default\"";
        cbbCollation.setModel(new DefaultComboBoxModel(collation));
        cbbCollation.setSelectedIndex(0);

        String[] operationClass = new String[6];
        operationClass[0] = "";
        operationClass[1] = "bpchar_pattern_ops";
        operationClass[2] = "cidr_ops";
        operationClass[3] = "text_pattern_ops";
        operationClass[4] = "varchar_ops";
        operationClass[5] = "varchar_pattern_ops";
        cbbOperatorClass.setModel(new DefaultComboBoxModel(operationClass));
        cbbOperatorClass.setSelectedIndex(0);
        try
        {
            ColItemInfoDTO[] columns = tc.getColumnOfRelation(helperInfo, helperInfo.getRelationOid());
            cbbColumn.setModel(new DefaultComboBoxModel(columns));
            cbbColumn.setSelectedIndex(-1);//default to select nothing     
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        tblColumns.getModel().addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent evt)
            {
                tblColumnChangedAction();
            }
        });
        txfdFillFactor.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                logger.info(txfdFillFactor.getText());
                valueKeyTypedAction(evt);
            }

            @Override
            public void keyReleased(KeyEvent evt)
            {
                logger.info(txfdFillFactor.getText());
                if (txfdFillFactor.getText() == null || txfdFillFactor.getText().isEmpty())
                {
                    return;
                }
                int fillFactor = Integer.valueOf(txfdFillFactor.getText());
                if (fillFactor > 100)
                {
                    JOptionPane.showConfirmDialog(null,
                            constBundle.getString("fillFactorValueRange"),
                            constBundle.getString("warning"), JOptionPane.YES_NO_OPTION);
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        rbGroupNulls = new javax.swing.ButtonGroup();
        rbGroupOrder = new javax.swing.ButtonGroup();
        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        cbbUseSlony = new javax.swing.JComboBox();
        lblUseSlony = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        pnlDefinition = new javax.swing.JPanel();
        lblTablespace = new javax.swing.JLabel();
        lblFillFactor = new javax.swing.JLabel();
        lblConstraint = new javax.swing.JLabel();
        lblClustered = new javax.swing.JLabel();
        txfdFillFactor = new javax.swing.JTextField();
        cbbTablespace = new javax.swing.JComboBox();
        cbClustered = new javax.swing.JCheckBox();
        cbbAccessMethod = new javax.swing.JComboBox();
        lblAccessMethod = new javax.swing.JLabel();
        lblConcurrentBuild = new javax.swing.JLabel();
        cbConcurrentBuild = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        ttarConstraint = new javax.swing.JTextArea();
        lblUnique = new javax.swing.JLabel();
        cbUnique = new javax.swing.JCheckBox();
        pnlColumns = new javax.swing.JPanel();
        spColumns = new javax.swing.JScrollPane();
        tblColumns = new javax.swing.JTable();
        btnAddChangeColumn = new javax.swing.JButton();
        btnRemoveColumn = new javax.swing.JButton();
        cbbColumn = new javax.swing.JComboBox();
        lblColumn = new javax.swing.JLabel();
        cbbOperatorClass = new javax.swing.JComboBox();
        lblOperatorClass = new javax.swing.JLabel();
        lblOrder = new javax.swing.JLabel();
        rbFirst = new javax.swing.JRadioButton();
        rbLast = new javax.swing.JRadioButton();
        lblNulls = new javax.swing.JLabel();
        lblCollation = new javax.swing.JLabel();
        cbbCollation = new javax.swing.JComboBox();
        rbAsc = new javax.swing.JRadioButton();
        rbDesc = new javax.swing.JRadioButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addIndex"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/index.png")));
setMinimumSize(new java.awt.Dimension(500, 500));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setBackground(new java.awt.Color(255, 255, 255));
jtp.setMinimumSize(new java.awt.Dimension(500, 440));
jtp.setPreferredSize(new java.awt.Dimension(500, 440));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(500, 420));
pnlProperties.setPreferredSize(new java.awt.Dimension(500, 420));

lblName.setText(constBundle.getString("name"));

lblComment.setText(constBundle.getString("comment"));

txfdName.setText(null);
txfdName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));

cbbUseSlony.setEditable(true);
cbbUseSlony.setEnabled(false);

lblUseSlony.setText(constBundle.getString("useSlony"));

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane4.setViewportView(txarComment);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblComment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdName)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 470, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbUseSlony, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())))
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblUseSlony)
                .addComponent(cbbUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("property"), pnlProperties);

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(500, 420));

    lblTablespace.setText(constBundle.getString("tablespace"));

    lblFillFactor.setText(constBundle.getString("fillFactor"));

    lblConstraint.setText(constBundle.getString("constraints"));

    lblClustered.setText(constBundle.getString("clustered"));

    cbClustered.setBackground(new java.awt.Color(255, 255, 255));

    cbbAccessMethod.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbAccessMethodItemStateChanged(evt);
        }
    });

    lblAccessMethod.setText(constBundle.getString("accessMethod"));

    lblConcurrentBuild.setText(constBundle.getString("concurrentBuild"));

    cbConcurrentBuild.setBackground(new java.awt.Color(255, 255, 255));

    ttarConstraint.setColumns(20);
    ttarConstraint.setRows(5);
    jScrollPane2.setViewportView(ttarConstraint);

    lblUnique.setText(constBundle.getString("unique"));

    cbUnique.setBackground(new java.awt.Color(255, 255, 255));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(lblTablespace, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(cbbTablespace, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblClustered, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblConcurrentBuild, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cbConcurrentBuild, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cbClustered))
                            .addGap(0, 453, Short.MAX_VALUE))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE))))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblFillFactor, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txfdFillFactor))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblAccessMethod, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(cbbAccessMethod, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
            .addContainerGap())
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(lblUnique, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(cbUnique)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblTablespace)
                .addComponent(cbbTablespace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblAccessMethod)
                .addComponent(cbbAccessMethod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(txfdFillFactor)
                .addComponent(lblFillFactor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbUnique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblUnique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbClustered, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblClustered, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblConcurrentBuild, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(cbConcurrentBuild, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblConstraint, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 196, Short.MAX_VALUE))
                .addComponent(jScrollPane2))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlColumns.setBackground(new java.awt.Color(255, 255, 255));
    pnlColumns.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlColumns.setOpaque(false);
    pnlColumns.setPreferredSize(new java.awt.Dimension(500, 420));

    tblColumns.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", "", "", "", ""
        }
    )
    {
        Class[] types = new Class []
        {
            java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
        };

        public Class getColumnClass(int columnIndex)
        {
            return types [columnIndex];
        }
    });
    tblColumns.setGridColor(new java.awt.Color(255, 255, 255));
    tblColumns.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblColumnsMousePressed(evt);
        }
    });
    spColumns.setViewportView(tblColumns);
    if (tblColumns.getColumnModel().getColumnCount() > 0)
    {
        tblColumns.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("columnName"));
        tblColumns.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("order"));
        tblColumns.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("nullsOrder"));
        tblColumns.getColumnModel().getColumn(3).setHeaderValue(constBundle.getString("operatorClass"));
        tblColumns.getColumnModel().getColumn(4).setHeaderValue(constBundle.getString("collation"));
    }

    btnAddChangeColumn.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeColumn.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeColumn.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeColumn.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeColumn.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeColumnActionPerformed(evt);
        }
    });

    btnRemoveColumn.setText(constBundle.getString("remove"));
    btnRemoveColumn.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveColumnActionPerformed(evt);
        }
    });

    cbbColumn.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbColumnItemStateChanged(evt);
        }
    });

    lblColumn.setText(constBundle.getString("column"));

    lblOperatorClass.setText(constBundle.getString("operatorClass"));

    lblOrder.setText(constBundle.getString("order"));

    rbFirst.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupNulls.add(rbFirst);
    rbFirst.setText("FIRST");

    rbLast.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupNulls.add(rbLast);
    rbLast.setSelected(true);
    rbLast.setText("LAST");

    lblNulls.setText("NULLs");

    lblCollation.setText(constBundle.getString("collation"));

    cbbCollation.setEditable(true);

    rbAsc.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupOrder.add(rbAsc);
    rbAsc.setSelected(true);
    rbAsc.setText("ASC");
    rbAsc.addChangeListener(new javax.swing.event.ChangeListener()
    {
        public void stateChanged(javax.swing.event.ChangeEvent evt)
        {
            rbAscStateChanged(evt);
        }
    });

    rbDesc.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupOrder.add(rbDesc);
    rbDesc.setText("DESC");

    javax.swing.GroupLayout pnlColumnsLayout = new javax.swing.GroupLayout(pnlColumns);
    pnlColumns.setLayout(pnlColumnsLayout);
    pnlColumnsLayout.setHorizontalGroup(
        pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlColumnsLayout.createSequentialGroup()
                    .addComponent(spColumns, javax.swing.GroupLayout.DEFAULT_SIZE, 558, Short.MAX_VALUE)
                    .addGap(10, 10, 10))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsLayout.createSequentialGroup()
                    .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlColumnsLayout.createSequentialGroup()
                            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(lblColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblCollation, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(cbbColumn, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbbCollation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(pnlColumnsLayout.createSequentialGroup()
                            .addComponent(lblOperatorClass, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(cbbOperatorClass, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addContainerGap())
                .addGroup(pnlColumnsLayout.createSequentialGroup()
                    .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlColumnsLayout.createSequentialGroup()
                            .addComponent(btnAddChangeColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pnlColumnsLayout.createSequentialGroup()
                            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lblNulls, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lblOrder, javax.swing.GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE))
                            .addGap(10, 10, 10)
                            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(pnlColumnsLayout.createSequentialGroup()
                                    .addComponent(rbAsc, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(rbDesc))
                                .addGroup(pnlColumnsLayout.createSequentialGroup()
                                    .addComponent(rbFirst)
                                    .addGap(18, 18, 18)
                                    .addComponent(rbLast)))))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
    );
    pnlColumnsLayout.setVerticalGroup(
        pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(spColumns, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnAddChangeColumn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(8, 8, 8)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblColumn)
                .addComponent(cbbColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblCollation)
                .addComponent(cbbCollation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOperatorClass)
                .addComponent(cbbOperatorClass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOrder)
                .addComponent(rbAsc)
                .addComponent(rbDesc))
            .addGap(6, 6, 6)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(rbFirst)
                .addComponent(rbLast)
                .addComponent(lblNulls))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("columns"), pnlColumns);
    pnlColumns.getAccessibleContext().setAccessibleName("");

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlSQL.setPreferredSize(new java.awt.Dimension(500, 420));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 558, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 328, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
   
    private void valueKeyTypedAction(KeyEvent evt)
    {
        char keyCh = evt.getKeyChar();
        //logger.info("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }
    }
    private void tblColumnChangedAction()
    {
        int row = tblColumns.getRowCount();
        //logger.info("rowCount:" + row);
        if (row <= 0 || txfdName.getText() == null || txfdName.getText().isEmpty())
        {
            btnRemoveColumn.setEnabled(false);
        } else
        {
            btnRemoveColumn.setEnabled(true);
        }
    }
    
    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.INDEX);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed
 
    private void btnRemoveColumnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveColumnActionPerformed
    {//GEN-HEADEREND:event_btnRemoveColumnActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int selectedRow = tblColumns.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveColumnActionPerformed

    private void btnAddChangeColumnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeColumnActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeColumnActionPerformed
        // TODO add your handling code here:
        if(cbbColumn.getSelectedIndex()<0)
        {
            return;
        }
        String nameNew = cbbColumn.getSelectedItem().toString();
        if (nameNew != null && !nameNew.isEmpty())
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            int row = tableModel.getRowCount();
            String order = "";
            if (rbAsc.isEnabled() && rbAsc.isSelected())
            {
                order = "ASC";
            }else
            {
                order = "DESC";
            }
            String nullsWhen = "";
            if(rbFirst.isEnabled() && rbFirst.isSelected())
            {
                nullsWhen = "FIRST"; 
            }else
            {
                nullsWhen = "LAST";
            }
                
            String collation = null;
            if (cbbCollation.isEnabled() && cbbCollation.getSelectedIndex() > 0)
            {
                collation = cbbCollation.getSelectedItem().toString();
            }
            String operationClass = null;
            if (cbbOperatorClass.isEnabled() && cbbOperatorClass.getSelectedIndex() > 0)//index of first item is 0.
            {
                operationClass = cbbOperatorClass.getSelectedItem().toString();
            }

            boolean isExist = false;
            for (int i = 0; i < row; i++)
            {
                logger.info("row=" + i);
                String nameOld = tableModel.getValueAt(i, 0).toString();
                logger.info("nameOld=" + nameOld + ",nameNew=" + nameNew);
                if (nameOld.equals(nameNew))
                {
                    isExist = true;
                    tableModel.setValueAt(order, i, 1);
                    tableModel.setValueAt(nullsWhen, i, 2);
                    tableModel.setValueAt(operationClass, i, 3);
                    tableModel.setValueAt(collation, i, 4);
                    break;
                }
//                if (isExist)
//                {
//                    break;
//                }
            }
            if (!isExist)
            {
                String[] v = new String[5];
                v[0] = nameNew;
                v[1] = order;
                v[2] = nullsWhen;
                v[3] = operationClass;
                v[4] = collation;
                tableModel.addRow(v);
            }
        }

    }//GEN-LAST:event_btnAddChangeColumnActionPerformed

    private void tblColumnsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblColumnsMousePressed
    {//GEN-HEADEREND:event_tblColumnsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press columns table");
        int row = tblColumns.getSelectedRow();
        logger.info("row = " + row);
        if (row < 0)
        {
            return; //nothing select
        }
        cbbColumn.setSelectedItem(tblColumns.getValueAt(row, 0).toString());
        if (tblColumns.getValueAt(row, 4) != null)
        {
            cbbCollation.setSelectedItem(tblColumns.getValueAt(row, 4).toString());
        } else
        {
            cbbCollation.setSelectedIndex(-1);
        }
        if (tblColumns.getValueAt(row, 3) != null)
        {
            cbbOperatorClass.setSelectedItem(tblColumns.getValueAt(row, 3));
        } else
        {
            cbbOperatorClass.setSelectedIndex(-1);
        }

        if (tblColumns.getValueAt(row, 1).toString().equals("DESC"))
        {
            rbDesc.setSelected(true);
        } else
        {
            rbAsc.setSelected(true);
        }
        if (tblColumns.getValueAt(row, 2).toString().equals("FIRST"))
        {
            rbFirst.setSelected(true);
        } else
        {
            rbLast.setSelected(true);
        }
    }//GEN-LAST:event_tblColumnsMousePressed

    private void cbbAccessMethodItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbAccessMethodItemStateChanged
    {//GEN-HEADEREND:event_cbbAccessMethodItemStateChanged
        // TODO add your handling code here:  
        String accessMethod = cbbAccessMethod.getSelectedItem().toString();
        logger.info("AccessMethod=" + accessMethod);
        if (tblColumns.getRowCount() < 1)
        {
            return;
        }
        int selectedValue = JOptionPane.showConfirmDialog(this,
                constBundle.getString("indexAccessMethodChangeWarnMessage"),
                constBundle.getString("indexAccessMethodChangeTitle"), JOptionPane.YES_NO_OPTION);
        logger.info("optionValue=" + selectedValue);
        // 0-yes,1-no,null-choosed nothing        
        if (selectedValue == 1)
        {
            logger.info("cancle");
        }
        else if (selectedValue == 0)
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            int rowCount = tableModel.getRowCount();
            for (int i = rowCount - 1; i >= 0; i--)
            {
                tableModel.removeRow(i);
            }
            if (accessMethod == null || accessMethod.isEmpty()
                    || accessMethod.equals("btree"))
            {
                cbbOperatorClass.setEnabled(true);
                rbDesc.setEnabled(true);
                rbFirst.setEnabled(true);
                rbLast.setEnabled(true);
            }
            else if (accessMethod.equals("hash")
                    || accessMethod.equals("gist")
                    || accessMethod.equals("gin")
                    || accessMethod.equals("spgist"))
            {
                cbbOperatorClass.setEnabled(false);
                rbAsc.setEnabled(true);
                rbFirst.setEnabled(false);
                rbLast.setEnabled(false);
            }
        }      
    }//GEN-LAST:event_cbbAccessMethodItemStateChanged

    private void rbAscStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_rbAscStateChanged
        // TODO add your handling code here:
        if(rbAsc.isSelected())
        {
            rbLast.setSelected(true);            
        }
        else
        {
            rbFirst.setSelected(true);
        }
    }//GEN-LAST:event_rbAscStateChanged

    private void cbbColumnItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbColumnItemStateChanged
    {//GEN-HEADEREND:event_cbbColumnItemStateChanged
        // TODO add your handling code here:
        logger.info("ColumnSelectedIndex = " + cbbColumn.getSelectedIndex());
        cbbCollation.setSelectedIndex(0);
        cbbOperatorClass.setSelectedIndex(0);
    }//GEN-LAST:event_cbbColumnItemStateChanged

    
    //for add
    //index name is not necessary for create but necessary for admin to show treenode
    private void btnOkEnableForAdd()
    {
        String name = txfdName.getText().trim();
        if (name == null || name.isEmpty() || tblColumns.getRowCount()<=0)
        {
            cbClustered.setEnabled(false);
            cbClustered.setSelected(false);
             btnOK.setEnabled(false);
        } else
        {
            cbClustered.setEnabled(true);
            btnOK.setEnabled(true);
        }
    }
    private void jtpStateChangedForAdd(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTab:" + index);
        SyntaxController sc = SyntaxController.getInstance();
        if (index == 3)
        {
            DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
            if (txfdName.getText() == null || txfdName.getText().isEmpty()
                    || columnModel.getRowCount() <= 0)
            {
                tpDefinitionSQL.setText("");//clear
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
                return;
            }
            tpDefinitionSQL.setText("");//clear
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getIndexInfo()));
        }
    }
    private void btnOKActionPerformedForAdd(ActionEvent evt)                                      
    {
        logger.info(evt.getActionCommand());
        logger.info("name=" + txfdName.getText());
        try
        {
            TreeController.getInstance().createObj(helperInfo, this.getIndexInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }

    //for change
    private void btnOkEnabledForChange()
    {
        if (txfdName.getText().isEmpty())
        {
            btnOK.setEnabled(false);
        } else if (!txfdName.getText().equals(indexInfo.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(indexInfo.getComment())
                && !(txarComment.getText().isEmpty() && indexInfo.getComment()==null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else if (!cbbTablespace.getSelectedItem().equals(indexInfo.getTablespace()))
        {
            logger.info("tablespace changed");
            btnOK.setEnabled(true);
        } else if (!txfdFillFactor.getText().trim().isEmpty() && !txfdFillFactor.getText().trim().equals(indexInfo.getFillFactor()))
        {
            logger.info("fillfactor changed");
            btnOK.setEnabled(true);
        } else if (cbClustered.isSelected() != indexInfo.isClustered())
        {
            logger.info("cluster changed");
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
     private void jtpStateChangedForChange(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTab:" + index);
        SyntaxController sc = SyntaxController.getInstance();
        if (index == 3)
        {
            DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
            if (!btnOK.isEnabled())
            {
                tpDefinitionSQL.setText("");//clear
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
                return;
            }
            tpDefinitionSQL.setText("");//clear
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getIndexChangeSQL(this.indexInfo, this.getIndexInfo()));
        }
    }
    private void btnOKActionPerformedForChange(ActionEvent evt)                                      
    {
        logger.info(evt.getActionCommand());
        logger.info("name=" + txfdName.getText());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getDbName(), tc.getIndexChangeSQL(this.indexInfo, this.getIndexInfo()));
            isSuccess = true;
            this.dispose();
        } catch (ClassNotFoundException | SQLException ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }

    public IndexInfoDTO getIndexInfo()
    {
       IndexInfoDTO newIndex = new IndexInfoDTO();
        if (indexInfo != null)
        {
            newIndex.setOid(newIndex.getOid());
        }
        newIndex.setHelperInfo(helperInfo);
        newIndex.setName(txfdName.getText());
        newIndex.setComment(txarComment.getText());
        newIndex.setTablespace(cbbTablespace.getSelectedItem().toString());
        newIndex.setAccessMethod(cbbAccessMethod.getSelectedItem().toString());
        newIndex.setFillFactor(txfdFillFactor.getText().trim());
        newIndex.setUnique(cbUnique.isSelected());
        newIndex.setClustered(cbClustered.isSelected());
        newIndex.setConcurrently(cbConcurrentBuild.isSelected());
        newIndex.setConstraint(ttarConstraint.getText());

        DefaultTableModel columnModel = (DefaultTableModel) tblColumns.getModel();
        int row = columnModel.getRowCount();
        List<ColInfoDTO> columnInfoList = new ArrayList();
        for (int i = 0; i < row; i++)
        {
            ColInfoDTO column = new ColInfoDTO();
            String name = columnModel.getValueAt(i, 0).toString();
            if (name == null || name.isEmpty())
            {
                break;
            }
            column.setName(columnModel.getValueAt(i, 0).toString());
            if(columnModel.getValueAt(i, 1)!=null)
            {
                column.setOrder(columnModel.getValueAt(i, 1).toString());
            }
            if (columnModel.getValueAt(i, 2) != null)
            {
                column.setNullsWhen(columnModel.getValueAt(i, 2).toString());
            }
            if (columnModel.getValueAt(i, 4) != null)
            {
                column.setCollation(columnModel.getValueAt(i, 4).toString());
            }
            if (columnModel.getValueAt(i, 3) != null)
            {
                column.setOperatorClass(columnModel.getValueAt(i, 3).toString());
            }            
            columnInfoList.add(column);
        }
        newIndex.setColInfoList(columnInfoList);
        return newIndex;
    }
    public boolean isSuccess()
    {
        return isSuccess;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangeColumn;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemoveColumn;
    private javax.swing.JCheckBox cbClustered;
    private javax.swing.JCheckBox cbConcurrentBuild;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbUnique;
    private javax.swing.JComboBox cbbAccessMethod;
    private javax.swing.JComboBox cbbCollation;
    private javax.swing.JComboBox cbbColumn;
    private javax.swing.JComboBox cbbOperatorClass;
    private javax.swing.JComboBox cbbTablespace;
    private javax.swing.JComboBox cbbUseSlony;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblAccessMethod;
    private javax.swing.JLabel lblClustered;
    private javax.swing.JLabel lblCollation;
    private javax.swing.JLabel lblColumn;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblConcurrentBuild;
    private javax.swing.JLabel lblConstraint;
    private javax.swing.JLabel lblFillFactor;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblNulls;
    private javax.swing.JLabel lblOperatorClass;
    private javax.swing.JLabel lblOrder;
    private javax.swing.JLabel lblTablespace;
    private javax.swing.JLabel lblUnique;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlColumns;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JRadioButton rbAsc;
    private javax.swing.JRadioButton rbDesc;
    private javax.swing.JRadioButton rbFirst;
    private javax.swing.ButtonGroup rbGroupNulls;
    private javax.swing.ButtonGroup rbGroupOrder;
    private javax.swing.JRadioButton rbLast;
    private javax.swing.JScrollPane spColumns;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblColumns;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarConstraint;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdFillFactor;
    private javax.swing.JTextField txfdName;
    // End of variables declaration//GEN-END:variables

}
