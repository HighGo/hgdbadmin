/* ------------------------------------------------ 
* 
* File: TreeCellRender.java
*
* Abstract: 
* 		针对主窗口左侧树不同的叶子节点设置不同的图片.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\TreeCellRender.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.model.*;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class TreeCellRenderer extends DefaultTreeCellRenderer
{
    private final Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass()); 
    
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, 
            boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        Object obj = node.getUserObject();
        //logger.info("CurrentNode = "+ node.toString()+", Class = "+obj.getClass().toString());    
        //this.setBackgroundNonSelectionColor(Color.GREEN);
        if (node.isRoot())
        {
            setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/servers.png")));
        } else if (obj instanceof XMLServerInfoDTO)
        {
            if (leaf)
            {                
                setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/serverbad-sm.png")));
            } else
            {                
                setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/server-sm.png")));
            }
        }else if(obj instanceof ObjGroupDTO)
        {
            ObjGroupDTO object = (ObjGroupDTO) obj;
            TreeEnum.TreeNode nodeType = object.getType();
            switch (nodeType)
            {
                // group
                case SERVER_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/servers.png")));
                    break;
                case DATABASE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/databases.png")));
                    break;
                case TABLESPACE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/tablespaces.png")));
                    break;
                case GROUPROLE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/roles.png")));
                    break;
                case LOGINROLE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/loginroles.png")));
                    break;
                case CATALOG_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/catalogs.png")));
                    break;
                case CAST_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/casts.png")));
                    break;
                case LANGUAGE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/languages.png")));
                    break;
                case EVENT_TRIGGER_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/event_trigger_group.png")));
                    break;
                case SCHEMA_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/namespaces.png")));
                    break;
                case SEQUENCE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/sequences.png")));
                    break;
                case FUNCTION_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/functions.png")));
                    break;
                case PROCEDURE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/coll-procedure.png")));
                    break;
                case VIEW_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/views.png")));
                    break;
                case TABLE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/tables.png")));
                    break;
                case PARTITION_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/partions.png")));//
                    break;
                case COLUMN_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/columns.png")));
                    break;
                case CONSTRAINT_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/constraints.png")));
                    break;
                case INDEX_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/indexes.png")));
                    break;
                case RULE_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/rules.png")));
                    break;
                case TRIGGER_GROUP:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/triggers.png")));
                    break;
            }
        }
        else if(obj instanceof AbstractObject)
        {
            AbstractObject object = (AbstractObject) obj;
            TreeEnum.TreeNode nodeType = object.getType();
            switch(nodeType)
            {    
                //for object
                case DATABASE:
                    if (obj instanceof ObjInfoDTO)//(leaf)
                    {
                        setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/closeddatabase-sm.png")));
                    } else
                    {
                        setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/database-sm.png")));
                    }
                    break;
                case TABLESPACE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/tablespace.png")));
                    break;
                case GROUPROLE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/users.png")));
                    break;
                case LOGINROLE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/user.png")));
                    break;
                case CATALOG:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/catalog-sm.png")));
                    break;
                case CAST:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/cast-sm.png")));
                    break;
                case LANGUAGE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/language-sm.png")));
                    break;
                case EVENT_TRIGGER:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/event_trigger.png")));
                    break;
                case SCHEMA:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/namespace-sm.png")));
                    break;
                case SEQUENCE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/sequence.png")));
                    break;
                case FUNCTION:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/function.png")));
                    break;
                case PROCEDURE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/procedure.png")));
                    break;
                case VIEW:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/view-sm.png")));
                    break;
                case MATERIALIZED_VIEW:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/mview-sm.png")));
                    break;
                case TABLE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/table-sm.png")));
                    break;
                case PARTITION:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/parition.png")));
                    break;
                case VIEW_COLUMN:
                case COLUMN:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/column-sm.png")));
                    break;
                case PRIMARY_KEY:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/primarykey.png")));
                    break;
                case FOREIGN_KEY:            
                    if (obj instanceof ConstraintInfoDTO)
                    {
                        ConstraintInfoDTO c = (ConstraintInfoDTO) obj;
                        if (c.isValidate())
                        {
                            setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/foreignkey.png")));
                        } else
                        {
                            setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/foreignkeybad.png")));
                        }
                    } else
                    {
                        setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/foreignkey.png")));
                    }
                    break;
                case UNIQUE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/unique.png")));
                    break;
                case EXCLUDE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/exclude.png")));
                    break;
                case CHECK:
                    if (obj instanceof ConstraintInfoDTO)
                    {
                        ConstraintInfoDTO c = (ConstraintInfoDTO) obj;
                        if (c.isValidate())
                        {
                            setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/check.png")));
                        } else
                        {
                            setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/checkbad.png")));
                        }
                    } else
                    {
                        setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/check.png")));
                    }
                    break;
                case INDEX:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/index.png")));
                    break;
                case RULE:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/rule.png")));
                    break;
                case TRIGGER:
                    setIcon(new ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/trigger.png")));
                    break;
                default:    
                    setIcon(null);
                    logger.warn("No icon for " + nodeType.toString());
                    break;
            }
        }else
        {
            setIcon(null);
            logger.error(obj+" is an exception obj, do nothing and return null icon.");
        }
        return this;
    }
}
