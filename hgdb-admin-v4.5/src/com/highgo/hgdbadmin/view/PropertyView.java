/* ------------------------------------------------ 
* 
* File: PropertyView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\PropertyView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;
import com.highgo.hgdbadmin.event.TreeNodeChangeEventHandler;
import com.highgo.hgdbadmin.model.CastInfoDTO;
import com.highgo.hgdbadmin.model.CatalogInfoDTO;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.EventTriggerInfoDTO;
import com.highgo.hgdbadmin.model.FunctionInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.IndexInfoDTO;
import com.highgo.hgdbadmin.model.LanguageInfoDTO;
import com.highgo.hgdbadmin.model.ObjGroupDTO;
import com.highgo.hgdbadmin.model.ObjInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.ProcedureInfoDTO;
import com.highgo.hgdbadmin.model.RoleInfoDTO;
import com.highgo.hgdbadmin.model.RuleInfoDTO;
import com.highgo.hgdbadmin.model.SchemaInfoDTO;
import com.highgo.hgdbadmin.model.SequenceInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.TablespaceInfoDTO;
import com.highgo.hgdbadmin.model.TriggerInfoDTO;
import com.highgo.hgdbadmin.model.ViewColumnInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.event.SimpleEventBus;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class PropertyView extends JTable
{
    private final Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private final ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    
    private SimpleEventBus eventBus;

    public PropertyView(SimpleEventBus eventBus)
    {
        this.eventBus = eventBus;
        this.setShowGrid(false);
        this.getTableHeader().setReorderingAllowed(false);
        if(this.getModel() != null)
        {
            this.removeAll();
        }
        this.addHandler();        
    }

    private void addHandler()
    {
        eventBus.addHandler(TreeNodeChangeEvent.TYPE, new TreeNodeChangeEventHandler()
        {
            @Override
            public void onChanged(TreeNodeChangeEvent evt)
            {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) evt.getNodeObject();
                if(node==null)
                {
                    return;
                }
                logger.info("CurrentNode = "+node.toString());
                populateData(node.getUserObject());
            }
        });
    }

    private void populateData(Object obj)
    {
        if (obj == null)
        {
            return;
        }
        
        String[] headers;
        if (obj instanceof ObjGroupDTO)
        {
            ObjGroupDTO objGroupInfo = (ObjGroupDTO) obj;
            headers = new String[]
            {
                objGroupInfo.getName(),
                constBundle.getString("owner"),
                constBundle.getString("comment")
            };
            this.setModel(new DefaultTableModel(headers, 0)
            {
                Class[] types = new Class[]
                {
                    String.class, String.class, String.class
                };
                boolean[] canEdit = new boolean[]
                {
                    false, false, false
                };
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
            });
        }else
        {
            headers = new String[]
            {
                constBundle.getString("attribute"),
                constBundle.getString("value")
            };
            this.setModel(new DefaultTableModel(headers, 0)
            {
                Class[] types = new Class[]
                {
                    String.class, String.class
                };
                boolean[] canEdit = new boolean[]
                {
                    false, false
                };
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
            });
        }
        this.addRows(obj);
    }

    private void addRows(Object obj)
    {
        logger.debug("Enter:" + obj.getClass());
        DefaultTableModel model = (DefaultTableModel) this.getModel();
        if (obj instanceof ObjGroupDTO)
        {
            ObjGroupDTO nodeObject = (ObjGroupDTO) obj;
            this.addRows(model, nodeObject);  
        } else if (obj instanceof XMLServerInfoDTO)
        {
            XMLServerInfoDTO nodeObject = (XMLServerInfoDTO) obj;
            this.addRows(model, nodeObject);            
        } else if (obj instanceof DBInfoDTO)
        {
            DBInfoDTO nodeObject = (DBInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof TablespaceInfoDTO)
        {
            TablespaceInfoDTO nodeObject = (TablespaceInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof RoleInfoDTO)
        {
            RoleInfoDTO nodeObject = (RoleInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof CatalogInfoDTO)
        {
            CatalogInfoDTO nodeObject = (CatalogInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof CastInfoDTO)
        {
            CastInfoDTO nodeObject = (CastInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof LanguageInfoDTO)
        {
            LanguageInfoDTO nodeObject = (LanguageInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof EventTriggerInfoDTO)
        {
            EventTriggerInfoDTO nodeObject = (EventTriggerInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof SchemaInfoDTO)
        {
            SchemaInfoDTO nodeObject = (SchemaInfoDTO) obj;
            this.addRows(model, nodeObject);
        }else if (obj instanceof FunctionInfoDTO)
        {
            FunctionInfoDTO nodeObject = (FunctionInfoDTO) obj;
            if (TreeEnum.TreeNode.FUNCTION == nodeObject.getType()) 
            {
                this.addRows(model, nodeObject);
            } else 
            {
                this.addRows(model, (ProcedureInfoDTO) obj);
            }
        } else if (obj instanceof SequenceInfoDTO) 
        {
            SequenceInfoDTO nodeObject = (SequenceInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof TableInfoDTO)
        {
            TableInfoDTO nodeObject = (TableInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof ViewInfoDTO)
        {
            ViewInfoDTO nodeObject = (ViewInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof PartitionInfoDTO)
        {
            PartitionInfoDTO nodeObject = (PartitionInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof ColumnInfoDTO)
        {
            ColumnInfoDTO nodeObject = (ColumnInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof ConstraintInfoDTO)
        {
            ConstraintInfoDTO nodeObject = (ConstraintInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof TriggerInfoDTO)
        {
            TriggerInfoDTO nodeObject = (TriggerInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof IndexInfoDTO)
        {
            IndexInfoDTO nodeObject = (IndexInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof RuleInfoDTO)
        {
            RuleInfoDTO nodeObject = (RuleInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else if (obj instanceof ViewColumnInfoDTO)
        {
            ViewColumnInfoDTO nodeObject = (ViewColumnInfoDTO) obj;
            this.addRows(model, nodeObject);
        } else
        {
            model.addRow(new Object[]{constBundle.getString("noProperty")});
        }
    }
    
    private void addRows(DefaultTableModel model, ObjGroupDTO nodeObject)
    {
        List<ObjInfoDTO> objList = nodeObject.getObjInfoList();
        //logger.info("size=" + (objList==null? "list is null": objList.size()));
        switch (nodeObject.getType())
        {
            case SERVER_GROUP:
                model.addRow(new Object[]{constBundle.getString("noProperty")});
                break;
            case FUNCTION_GROUP:
            case PROCEDURE_GROUP:
                for (ObjInfoDTO obj : objList)
                {
                    model.addRow(new Object[]
                    {
                        //toString() get the full name(like fun_name(arg1,arg2..) or fun_name())
                        obj.toString(), obj.getOwner(), obj.getComment()
                    });
                }
                break;
            default:
                for (ObjInfoDTO obj : objList)
                {
                    model.addRow(new Object[]
                    {
                        obj.getName(), obj.getOwner(), obj.getComment()
                    });
                }
                break;
        }
    }
    
    private void addRows(DefaultTableModel model, XMLServerInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("description"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("hostName"), nodeObject.getHost()});
        model.addRow(new Object[]{constBundle.getString("portNumber"), nodeObject.getPort()});
        model.addRow(new Object[]{constBundle.getString("maintenanceDB"), nodeObject.getMaintainDB()});
        model.addRow(new Object[]{constBundle.getString("userName"), nodeObject.getUser()});
        model.addRow(new Object[]{constBundle.getString("onSSL"), nodeObject.isOnSSL()});
        //model.addRow(new Object[]{constBundle.getString("isSavePwd"), nodeObject.isSavePwd()});
        //model.addRow(new Object[]{constBundle.getString("isRestoreEnv"), "false"});
        
        model.addRow(new Object[]{constBundle.getString("isConnected"), nodeObject.isConnected()});
        model.addRow(new Object[]{constBundle.getString("dbVersion"), nodeObject.getVersion()});
        /*if(nodeObject.isHighgoDB())
        {
            model.addRow(new Object[]{constBundle.getString("kernelVersion"), nodeObject.getKernelVersion()});
            model.addRow(new Object[]{constBundle.getString("hgdbCompatibility"), nodeObject.getHgdbCompatibility()});        
        }*/
        
        if(System.getProperty("os.name").startsWith("Windows"))
        {
            model.addRow(new Object[]{constBundle.getString("service"), nodeObject.getService()});
        }
        
        /*if(nodeObject.isConnected())
        {
            model.addRow(new Object[]{constBundle.getString("lastSysOid"),nodeObject.getDatLastSysOid()});
            model.addRow(new Object[]{constBundle.getString("inRecovery"),nodeObject.isInRecovery()});
        }*/
    }
    private void addRows(DefaultTableModel model, DBInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("tablespace"), nodeObject.getTablespace()});
        model.addRow(new Object[]{constBundle.getString("defaultTablespace"), nodeObject.getDefaultTablespace()});
        model.addRow(new Object[]{constBundle.getString("encoding"), nodeObject.getEncoding()});
        model.addRow(new Object[]{constBundle.getString("collation"), nodeObject.getCollation()});
        model.addRow(new Object[]{constBundle.getString("characterType"), nodeObject.getCharacterType()});
        model.addRow(new Object[]{constBundle.getString("defaultSchema"), nodeObject.getDefaultSchema()});
        HelperInfoDTO helperInfo = nodeObject.getHelperInfo();
        TreeController tc= TreeController.getInstance();
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 8, 4))
        {
            model.addRow(new Object[]{constBundle.getString("defaultTableAcl"), nodeObject.getDefaultTableAcl()});
            model.addRow(new Object[]{constBundle.getString("defaultSequenceAcl"), nodeObject.getDefaultSequenceAcl()});
            model.addRow(new Object[]{constBundle.getString("defaultFunctionAcl"), nodeObject.getDefaultFunctionAcl()});
        }
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            model.addRow(new Object[]{constBundle.getString("defaultTypeAcl"), nodeObject.getDefaultTypeAcl()});        
        }        
        model.addRow(new Object[]{constBundle.getString("isAllowConnection"), nodeObject.isAllowConnection()});
        model.addRow(new Object[]{constBundle.getString("isConnected"), nodeObject.isConnected()});
        model.addRow(new Object[]{constBundle.getString("connectionLimit"), nodeObject.getConnectionLimit()});
        model.addRow(new Object[]{constBundle.getString("isSystemDatabase"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model, TablespaceInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("location"), nodeObject.getLocation()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,RoleInfoDTO nodeObject)
    {
         model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
         model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
         model.addRow(new Object[]{constBundle.getString("accountExpires"), nodeObject.getAccountExpires()});
         model.addRow(new Object[]{constBundle.getString("isCanLogin"), nodeObject.isCanLogin()});
         //modify begin by sunqk at 2019.06.17 for bmj not display superuser
         //model.addRow(new Object[]{constBundle.getString("isSuperuser"), nodeObject.isSuperuser()});
         //modify begin by sunqk at 2019.06.17 for bmj not display superuser
         model.addRow(new Object[]{constBundle.getString("isCreateDatabases"), nodeObject.isCanCreateDB()});
         model.addRow(new Object[]{constBundle.getString("isCreateRoles"), nodeObject.isCanCreateRoles()});
         //ygq v5admin del start
         //model.addRow(new Object[]{constBundle.getString("isUpdateCatalogs"), nodeObject.isCanModifyCatalog()});
         //ygq v5admin del end
         model.addRow(new Object[]{constBundle.getString("isInherits"), nodeObject.isCanInherits()});
         HelperInfoDTO helper = nodeObject.getHelperInfo();
         if (TreeController.getInstance().isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 0))
         {
            model.addRow(new Object[]{constBundle.getString("isReplicate"), nodeObject.isCanInitStreamReplicationAndBackup()});
         }         
         model.addRow(new Object[]{constBundle.getString("connectionLimit"), nodeObject.getConnectionLimit()});
         model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
         model.addRow(new Object[]{constBundle.getString("memberOf"), nodeObject.toMemberOf()});
    }
    private void addRows(DefaultTableModel model,CatalogInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        HelperInfoDTO helperInfo = nodeObject.getHelperInfo();
        TreeController tc= TreeController.getInstance();
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 8, 4))
        {
            model.addRow(new Object[]{constBundle.getString("defaultTableAcl"), nodeObject.getDefaultTableAcl()});
            model.addRow(new Object[]{constBundle.getString("defaultSequenceAcl"), nodeObject.getDefaultSequenceAcl()});
            model.addRow(new Object[]{constBundle.getString("defaultFunctionAcl"), nodeObject.getDefaultFunctionAcl()});
        }
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            model.addRow(new Object[]{constBundle.getString("defaultTypeAcl"), nodeObject.getDefaultTypeAcl()});        
        }
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,CastInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("sourceType"), nodeObject.getSourceType()});
        model.addRow(new Object[]{constBundle.getString("targetType"), nodeObject.getTargetType()});
        model.addRow(new Object[]{constBundle.getString("function"), nodeObject.getFunction()});
        model.addRow(new Object[]{constBundle.getString("context"), nodeObject.getContext()});
        model.addRow(new Object[]{constBundle.getString("isSystemCast"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,LanguageInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("isTrusted"), nodeObject.isTrusted()});
        model.addRow(new Object[]{constBundle.getString("handler"), nodeObject.getHandler()});
        model.addRow(new Object[]{constBundle.getString("validator"), nodeObject.getValidator()});
        model.addRow(new Object[]{constBundle.getString("isSystemLanguage"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model, EventTriggerInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("enable"), nodeObject.getEnableWhat()});
        model.addRow(new Object[]{constBundle.getString("event"), nodeObject.getEvent()});
        model.addRow(new Object[]{constBundle.getString("triggerFunction"), nodeObject.getFunc()});
        model.addRow(new Object[]{constBundle.getString("whatTime"), nodeObject.getWhenTag()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,SchemaInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        HelperInfoDTO helperInfo = nodeObject.getHelperInfo();
        TreeController tc= TreeController.getInstance();
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 8, 4))
        {
            model.addRow(new Object[]{constBundle.getString("defaultTableAcl"), nodeObject.getDefaultTableAcl()});
            model.addRow(new Object[]{constBundle.getString("defaultSequenceAcl"), nodeObject.getDefaultSequenceAcl()});
            model.addRow(new Object[]{constBundle.getString("defaultFunctionAcl"), nodeObject.getDefaultFunctionAcl()});
        }
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            model.addRow(new Object[]{constBundle.getString("defaultTypeAcl"), nodeObject.getDefaultTypeAcl()});        
        }
        model.addRow(new Object[]{constBundle.getString("isSystemSchema"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,TableInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("tablespace"), nodeObject.getTablespace()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("ofType"), nodeObject.getOfType()});
        model.addRow(new Object[]{constBundle.getString("primaryKey"), nodeObject.getPrimaryKey()});
        model.addRow(new Object[]{constBundle.getString("rowsEstimated"), nodeObject.getRowsEstimated()});
        model.addRow(new Object[]{constBundle.getString("fillFactor"), nodeObject.getFillFactor()});
        model.addRow(new Object[]{constBundle.getString("rowsCounted"), nodeObject.getRowsCounted()});
        model.addRow(new Object[]{constBundle.getString("partitioned"), nodeObject.getKind()=='p'});
        model.addRow(new Object[]{constBundle.getString("beInherited"), nodeObject.isInheritedByTables()});
        model.addRow(new Object[]{constBundle.getString("inheritTablesCount"), nodeObject.getInheritTableList().size()});
        model.addRow(new Object[]{constBundle.getString("isUnlogged"),nodeObject.isUnlogged()});//unrealized    
        model.addRow(new Object[]{constBundle.getString("isHasOids"), nodeObject.isHasOID()});
        model.addRow(new Object[]{constBundle.getString("isHasToastTable"),nodeObject.isHasToastTable()});
        model.addRow(new Object[]{constBundle.getString("isSystemTable"), nodeObject.isSystem()});
        //need to add properties of storage parameter
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,ViewInfoDTO nodeObject)
    {
        HelperInfoDTO helper = nodeObject.getHelperInfo();
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("definition"), nodeObject.getDefinition()});
        if(TreeController.getInstance().isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 2))
        {
            model.addRow(new Object[]{constBundle.getString("isMaterializedView"), nodeObject.isMaterializedView()});
            if(nodeObject.isMaterializedView())
            {
                model.addRow(new Object[]{constBundle.getString("tablespace"), nodeObject.getTablespace()});
                model.addRow(new Object[]{constBundle.getString("fillFactor"), nodeObject.getFillFactor()});
                model.addRow(new Object[]{constBundle.getString("withData"), nodeObject.isWithData()});
            }       
        }
        model.addRow(new Object[]{constBundle.getString("isSystemView"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
        
    }
    private void addRows(DefaultTableModel model,FunctionInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getSimpleName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("argCount"), nodeObject.getInArgCount()});
        model.addRow(new Object[]{constBundle.getString("argLabelType"), nodeObject.getInArgTypes()});
        model.addRow(new Object[]{constBundle.getString("argDefine"), nodeObject.getAllArgDefine()});
        model.addRow(new Object[]{constBundle.getString("returnType"), nodeObject.getReturnType()});
        model.addRow(new Object[]{constBundle.getString("languages"), nodeObject.getLanguage()});
        model.addRow(new Object[]{constBundle.getString("src"), nodeObject.getSrc()});
        model.addRow(new Object[]{constBundle.getString("cost"), nodeObject.getCost()});
        model.addRow(new Object[]{constBundle.getString("volatile"), nodeObject.getVolatiles()});
        model.addRow(new Object[]{constBundle.getString("isReturnSet"), nodeObject.isReturnSet()});
        model.addRow(new Object[]{constBundle.getString("isSecurityDefiner"), nodeObject.isSecurityDefiner()});
        model.addRow(new Object[]{constBundle.getString("isStrict"), nodeObject.isStrict()});
        model.addRow(new Object[]{constBundle.getString("isWindow"), nodeObject.isWindow()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});        
    }
    private void addRows(DefaultTableModel model, ProcedureInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getSimpleName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("argCount"), nodeObject.getInArgCount()});
        model.addRow(new Object[]{constBundle.getString("argLabelType"), nodeObject.getInArgTypes()});
        model.addRow(new Object[]{constBundle.getString("argDefine"), nodeObject.getAllArgDefine()});
        //model.addRow(new Object[]{constBundle.getString("returnType"), nodeObject.getReturnType()});
        model.addRow(new Object[]{constBundle.getString("languages"), nodeObject.getLanguage()});
        model.addRow(new Object[]{constBundle.getString("src"), nodeObject.getSrc()});
        //model.addRow(new Object[]{constBundle.getString("cost"), nodeObject.getCost()});
        //model.addRow(new Object[]{constBundle.getString("volatile"), nodeObject.getVolatiles()});
        //model.addRow(new Object[]{constBundle.getString("isReturnSet"), nodeObject.isReturnSet()});
        model.addRow(new Object[]{constBundle.getString("isSecurityDefiner"), nodeObject.isSecurityDefiner()});
        //model.addRow(new Object[]{constBundle.getString("isStrict"), nodeObject.isStrict()});
        //model.addRow(new Object[]{constBundle.getString("isWindow"), nodeObject.isWindow()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});        
    }
    private void addRows(DefaultTableModel model,SequenceInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("currentValue"), nodeObject.getStrCurrent()});
        model.addRow(new Object[]{constBundle.getString("increment"), nodeObject.getStrIncrement()});
        model.addRow(new Object[]{constBundle.getString("startValue"), nodeObject.getStrStart()});
        model.addRow(new Object[]{constBundle.getString("minimum"), nodeObject.getStrMinimum()});
        model.addRow(new Object[]{constBundle.getString("maximum"), nodeObject.getStrMaximum()});        
        model.addRow(new Object[]{constBundle.getString("cache"), nodeObject.getStrCache()});
        model.addRow(new Object[]{constBundle.getString("isCycled"), nodeObject.isCycled()});
        model.addRow(new Object[]{constBundle.getString("isCalled"), nodeObject.isCalled()});
        model.addRow(new Object[]{constBundle.getString("isSystemSequence"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,PartitionInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("schema"), nodeObject.getSchema()});   
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("owner"), nodeObject.getOwner()});     
        model.addRow(new Object[]{constBundle.getString("isPartition"), nodeObject.isPartition()});
        //model.addRow(new Object[]{constBundle.getString("parent"), nodeObject.getParentOid()});        
        //model.addRow(new Object[]{constBundle.getString("tablespace"), nodeObject.getTablespace()});
        //model.addRow(new Object[]{constBundle.getString("isSystem"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
   }
    private void addRows(DefaultTableModel model,ColumnInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("position"), nodeObject.getPosition()});
        model.addRow(new Object[]{constBundle.getString("dataType"), nodeObject.getDatatype()});
        model.addRow(new Object[]{constBundle.getString("default"), nodeObject.getDefaultValue()});
        model.addRow(new Object[]{constBundle.getString("sequence"), nodeObject.getSequence()});
        model.addRow(new Object[]{constBundle.getString("isNotNull"), nodeObject.isNotNull()});
        model.addRow(new Object[]{constBundle.getString("isPKey"), nodeObject.isPk()});
        model.addRow(new Object[]{constBundle.getString("isFKey"), nodeObject.isFk()});
        model.addRow(new Object[]{constBundle.getString("storage"), nodeObject.getStorage()});
        model.addRow(new Object[]{constBundle.getString("isInherit"), nodeObject.isInherit()});
        model.addRow(new Object[]{constBundle.getString("statistics"), nodeObject.getStatistics()});
        model.addRow(new Object[]{constBundle.getString("isSystemColumn"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,ConstraintInfoDTO nodeObject)
    {     
        switch(nodeObject.getType())
        {
            case PRIMARY_KEY:
            case UNIQUE:
            case EXCLUDE:              
                model.addRow(new Object[]{constBundle.getString("constraintType"), nodeObject.getType().toString().replace("_", " ")});
                model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
                model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
                model.addRow(new Object[]{constBundle.getString("indexOid"), nodeObject.getIndexOid()});
                model.addRow(new Object[]{constBundle.getString("tablespace"), nodeObject.getTablespace()});
                model.addRow(new Object[] {constBundle.getString("columns"),nodeObject.getAllColumnDefine()});
                model.addRow(new Object[]{constBundle.getString("accessMethod"), nodeObject.getAccessMethod()});
                //model.addRow(new Object[]{constBundle.getString("constraint"), nodeObject.getConstraint()});//this is for index, not for coantraint            
                model.addRow(new Object[]{constBundle.getString("fillFactor"), nodeObject.getFillFactor()});
                //model.addRow(new Object[]{constBundle.getString("isSystemIndex"), nodeObject.isSystem()});
                break;
            case FOREIGN_KEY:
                model.addRow(new Object[]{constBundle.getString("constraintType"), nodeObject.getType()});
                model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
                model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
                model.addRow(new Object[]{constBundle.getString("childColumns"),nodeObject.getAllColumnDefine() });
                model.addRow(new Object[]{constBundle.getString("references"), nodeObject.getAllRelColumnDefine()});
                //model.addRow(new Object[]{constBundle.getString("coveringIndex"), "unrealized"});//this is for create, and cannot get for property
                model.addRow(new Object[]{constBundle.getString("matchType"), nodeObject.getMatchType()});
                model.addRow(new Object[]{constBundle.getString("onUpdate"), nodeObject.getUpdateActions()});
                model.addRow(new Object[]{constBundle.getString("onDelete"), nodeObject.getDeleteAction()});
                model.addRow(new Object[]{constBundle.getString("isDeferrable"), nodeObject.isDeferrable()});
                model.addRow(new Object[]{constBundle.getString("isDeferred"), nodeObject.isDeferred()});
                model.addRow(new Object[]{constBundle.getString("isValidate"),nodeObject.isValidate()});
                //model.addRow(new Object[]{constBundle.getString("isSystemForeignKey"), nodeObject.isSystem()});
                break;
            case CHECK:
                model.addRow(new Object[]{constBundle.getString("constraintType"), nodeObject.getType()});
                model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
                model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
                model.addRow(new Object[]{constBundle.getString("definition"), nodeObject.getCheckConditonQuoted()});
                model.addRow(new Object[]{constBundle.getString("noInherit"), nodeObject.isNoInherit()});
                model.addRow(new Object[]{constBundle.getString("isValidate"), nodeObject.isValidate()});
                break;
        }
        model.addRow(new Object[]{constBundle.getString("enable"), nodeObject.isEnable()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,TriggerInfoDTO nodeObject)
    {
        String foreach =  "STATEMENT";
        if(nodeObject.isRowTrigger())
        {
            foreach = "ROW";
        }
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("isConatraintTrigger"), nodeObject.isConstraintTrigger()});
        model.addRow(new Object[]{constBundle.getString("firesEvent"), nodeObject.getFire()});//this fire is fire and event
        model.addRow(new Object[]{constBundle.getString("forEach"), foreach});
        model.addRow(new Object[]{constBundle.getString("function"), nodeObject.getTriggerFunction()});
        model.addRow(new Object[]{constBundle.getString("isWhen"), nodeObject.getWhen()});
        model.addRow(new Object[]{constBundle.getString("isEnable"), nodeObject.isEnable()});
        model.addRow(new Object[]{constBundle.getString("isSystemTrigger"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }    
    private void addRows(DefaultTableModel model,IndexInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("tablespace"), nodeObject.getTablespace()});
        model.addRow(new Object[]{constBundle.getString("columns"), nodeObject.getAllColumnDefine()});
        model.addRow(new Object[]{constBundle.getString("isUnique"), nodeObject.isUnique()});
        model.addRow(new Object[]{constBundle.getString("isPrimary"), nodeObject.isPrimary()});
        model.addRow(new Object[]{constBundle.getString("isClustered"), nodeObject.isClustered()});   
        model.addRow(new Object[]{constBundle.getString("isValid"), nodeObject.isValid()});
        model.addRow(new Object[]{constBundle.getString("accessMethod"), nodeObject.getAccessMethod()});
        model.addRow(new Object[]{constBundle.getString("fillFactor"), nodeObject.getFillFactor()});
        model.addRow(new Object[]{constBundle.getString("constraint"), nodeObject.getConstraint()});
        model.addRow(new Object[]{constBundle.getString("isSystemIndex"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
    private void addRows(DefaultTableModel model,RuleInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("oid"), nodeObject.getOid()});
        model.addRow(new Object[]{constBundle.getString("parentType"), nodeObject.getParent()});
        model.addRow(new Object[]{constBundle.getString("event"), nodeObject.getEvent()});
        model.addRow(new Object[]{constBundle.getString("isDoInstead"), nodeObject.isDoInstead()});        
        model.addRow(new Object[]{constBundle.getString("doStatement"), nodeObject.getDoStatement()});
        model.addRow(new Object[]{constBundle.getString("whereCondition"), nodeObject.getWhereCondition()});
        model.addRow(new Object[]{constBundle.getString("isEnable"), nodeObject.isEnable()});
        model.addRow(new Object[]{constBundle.getString("isSystemRule"), nodeObject.isSystem()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
   }
    private void addRows(DefaultTableModel model,ViewColumnInfoDTO nodeObject)
    {
        model.addRow(new Object[]{constBundle.getString("name"), nodeObject.getName()});
        model.addRow(new Object[]{constBundle.getString("position"), nodeObject.getPosition()});
        model.addRow(new Object[]{constBundle.getString("dataType"), nodeObject.getDataType()});
        model.addRow(new Object[]{constBundle.getString("default"), nodeObject.getDefaultValue()});
        model.addRow(new Object[]{constBundle.getString("acl"), nodeObject.getAcl()});
        model.addRow(new Object[]{constBundle.getString("comment"), nodeObject.getComment()});
    }
}
