/* ------------------------------------------------ 
* 
* File: SequenceView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\SequenceView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.SequenceInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * privileges roles - [public] couldn't with grant option
 *                    [group role] could with grant option
 * 
 */
public class SequenceView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");  
    
    private HelperInfoDTO helperInfo;
    private SequenceInfoDTO sequenceInfo;
    private boolean isSuccess=false;
    
    //add
    public SequenceView(JFrame parent, boolean modal, HelperInfoDTO  helperInfo)
    {
        super(parent, modal);  
        this.helperInfo = helperInfo;
        this.sequenceInfo = null;        
        initComponents();      
        jtp.setEnabledAt(3, false);
        this.setTitle(constBundle.getString("addSequence"));        
        lblStart.setText(constBundle.getString("startValue"));
        this.setNormalInfo(false);
        
        //action
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAdd(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });
    }
    //property
    public SequenceView(JFrame parent, boolean modal, SequenceInfoDTO sequenceInfo)
    {
        super(parent, modal);
        this.sequenceInfo = sequenceInfo;
        this.helperInfo = sequenceInfo.getHelperInfo();
        initComponents();
        jtp.setEnabledAt(3, false);
        this.setTitle(constBundle.getString("sequences") + " " + sequenceInfo.getName());
        lblStart.setText(constBundle.getString("currentValue"));
        this.setNormalInfo(true);
        this.setProperty();
        
       //for btnok enable 
        KeyAdapter btnOKEnable = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        };
        txfdName.addKeyListener(btnOKEnable);
        cbbOwner.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        txarComment.addKeyListener(btnOKEnable);
        txfdIncrement.addKeyListener(btnOKEnable);
        txfdStart.addKeyListener(btnOKEnable);
        txfdMiniValue.addKeyListener(btnOKEnable);
        txfdMaxValue.addKeyListener(btnOKEnable);
        txfdCache.addKeyListener(btnOKEnable);
        cbCycled.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnableForChange();
            }
        });
        DefaultTableModel aclModel = (DefaultTableModel) tblPrivileges.getModel();
        aclModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnableForChange();
            }
        });
        //for index change
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        //for btnok action
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });
    }
    private void setProperty()
    {
        txfdName.setText(sequenceInfo.getName());
        txfdOID.setText(sequenceInfo.getOid().toString());
        cbbOwner.setSelectedItem(sequenceInfo.getOwner());
        cbbSchema.setSelectedItem(sequenceInfo.getSchema());
        txarComment.setText(sequenceInfo.getComment());
        txfdIncrement.setText(sequenceInfo.getStrIncrement());
        txfdStart.setText(sequenceInfo.getStrCurrent());//for change here is current values
        txfdMiniValue.setText(sequenceInfo.getStrMinimum());
        txfdMaxValue.setText(sequenceInfo.getStrMaximum());
        txfdCache.setText(sequenceInfo.getStrCache());
        cbCycled.setSelected(sequenceInfo.isCycled());
        String acl = sequenceInfo.getAcl();
        if (acl != null && !acl.isEmpty() && !acl.equals("{}"))
        {
            DefaultTableModel sModel = (DefaultTableModel) tblPrivileges.getModel();
            acl = acl.substring(1, acl.length() - 1);
            String[] privilegeArray = acl.split(",");
            for (String privilege : privilegeArray)
            {
                logger.info("privilege:" + privilege);
                String[] pArray = privilege.split("=");
                String[] p = new String[2];
                if (pArray[0] == null || pArray[0].isEmpty())
                {
                    p[0] = "public";
                } else
                {
                    p[0] = pArray[0];
                }
                p[1] = pArray[1].split("/")[0];
                sModel.addRow(p);
            }
        }
        List<SecurityLabelInfoDTO> slist = sequenceInfo.getSecurityLabelList();
        if (slist != null)
        {
            DefaultTableModel sModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[3];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                sModel.addRow(sl);
            }
        }
    }

    private void setNormalInfo(boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
//        String[] schemas = (String[]) tc.getOptionArrary(helperInfo, "schema", null);
//        cbbSchema.setModel(new DefaultComboBoxModel(schemas));
        cbbSchema.setModel(new DefaultComboBoxModel(new String[]
        {
            helperInfo.getSchema()
        }));
        cbbSchema.setSelectedItem(helperInfo.getSchema());
        try
        {
            String[] owners = (String[]) tc.getItemsArrary(helperInfo, "owner");
            cbbOwner.setModel(new DefaultComboBoxModel(owners));
            if(isChange)
            {
                cbbOwner.removeItemAt(0);
            }else
            {
                cbbOwner.setSelectedIndex(0);
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbRole.setModel(new DefaultComboBoxModel(tc.getRoles(helperInfo)));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }

        //normal action
        KeyAdapter keyAdapter = new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                txfdValueKeyTyped(evt);
            }
        };
        txfdIncrement.addKeyListener(keyAdapter);
        txfdStart.addKeyListener(keyAdapter);
        txfdMiniValue.addKeyListener(keyAdapter);
        txfdMaxValue.addKeyListener(keyAdapter);
        txfdCache.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                txfdCacheKeyTyped(evt);
            }
        });
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        cbbSchema = new javax.swing.JComboBox();
        lblSchema = new javax.swing.JLabel();
        lblUseSlony = new javax.swing.JLabel();
        txfdUseSlony = new javax.swing.JTextField();
        pnlDefinition = new javax.swing.JPanel();
        lblIncrement = new javax.swing.JLabel();
        lblStart = new javax.swing.JLabel();
        lblMinimum = new javax.swing.JLabel();
        lblMaximun = new javax.swing.JLabel();
        lblCache = new javax.swing.JLabel();
        lblCycled = new javax.swing.JLabel();
        txfdCache = new javax.swing.JTextField();
        txfdIncrement = new javax.swing.JTextField();
        txfdStart = new javax.swing.JTextField();
        txfdMiniValue = new javax.swing.JTextField();
        txfdMaxValue = new javax.swing.JTextField();
        cbCycled = new javax.swing.JCheckBox();
        pnlPrivileges = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPrivileges = new javax.swing.JTable();
        btnAddChangePrivileges = new javax.swing.JButton();
        btnRemovePrivilege = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRole = new javax.swing.JLabel();
        cbbRole = new javax.swing.JComboBox();
        cbAll = new javax.swing.JCheckBox();
        cbUpdate = new javax.swing.JCheckBox();
        cbInsert = new javax.swing.JCheckBox();
        cbOptionAll = new javax.swing.JCheckBox();
        cbSelect = new javax.swing.JCheckBox();
        cbOptionUpdate = new javax.swing.JCheckBox();
        cbOptionInsert = new javax.swing.JCheckBox();
        cbOptionSelect = new javax.swing.JCheckBox();
        cbDelete = new javax.swing.JCheckBox();
        cbOptionDelete = new javax.swing.JCheckBox();
        cbRule = new javax.swing.JCheckBox();
        cbOptionRule = new javax.swing.JCheckBox();
        cbReferences = new javax.swing.JCheckBox();
        cbOptionReferences = new javax.swing.JCheckBox();
        cbTrigger = new javax.swing.JCheckBox();
        cbOptionTrigger = new javax.swing.JCheckBox();
        cbOptionUsage = new javax.swing.JCheckBox();
        cbUsage = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addSequence"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/sequence.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane1.setViewportView(txarComment);

cbbSchema.setEnabled(false);

lblSchema.setText(constBundle.getString("schema"));

lblUseSlony.setText("OID");

txfdUseSlony.setEditable(false);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblSchema, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pnlPropertiesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdOID)
                    .addComponent(txfdName)
                    .addComponent(cbbOwner, 0, 418, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                    .addComponent(cbbSchema, 0, 418, Short.MAX_VALUE)))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txfdUseSlony, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblSchema)
                .addComponent(cbbSchema, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblUseSlony))
            .addGap(19, 19, 19))
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(520, 470));

    lblIncrement.setText(constBundle.getString("increment"));

    lblStart.setText(constBundle.getString("startValue"));

    lblMinimum.setText(constBundle.getString("minimum"));

    lblMaximun.setText(constBundle.getString("maximum"));

    lblCache.setText(constBundle.getString("cache"));

    lblCycled.setText(constBundle.getString("cycled"));

    cbCycled.setBackground(new java.awt.Color(255, 255, 255));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblCycled, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(cbCycled)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblIncrement, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txfdIncrement))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
                            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(lblMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(lblMaximun, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txfdMaxValue)
                                .addComponent(txfdMiniValue)))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblStart, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txfdStart))))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblCache, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(txfdCache, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(txfdStart)
                .addComponent(lblStart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblIncrement)
                .addComponent(txfdIncrement, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdMiniValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblMinimum))
            .addGap(10, 10, 10)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdMaxValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblMaximun))
            .addGap(10, 10, 10)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdCache, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblCache))
            .addGap(10, 10, 10)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblCycled)
                .addComponent(cbCycled))
            .addContainerGap(284, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlPrivileges.setBackground(new java.awt.Color(255, 255, 255));
    pnlPrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlPrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

    tblPrivileges.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblPrivileges.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblPrivileges.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblPrivilegesMousePressed(evt);
        }
    });
    jScrollPane4.setViewportView(tblPrivileges);
    if (tblPrivileges.getColumnModel().getColumnCount() > 0)
    {
        tblPrivileges.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("user")+"/"+constBundle.getString("group"));
        tblPrivileges.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("privileges"));
    }

    btnAddChangePrivileges.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangePrivileges.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangePrivilegesActionPerformed(evt);
        }
    });

    btnRemovePrivilege.setText(constBundle.getString("remove"));
    btnRemovePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemovePrivilegeActionPerformed(evt);
        }
    });

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("privileges"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("宋体", 0, 12), new java.awt.Color(204, 204, 204))); // NOI18N

    lblRole.setText(constBundle.getString("role"));

    cbbRole.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbRoleItemStateChanged(evt);
        }
    });

    cbAll.setText("ALL");
    cbAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbAllActionPerformed(evt);
        }
    });

    cbUpdate.setText("UPDATE");
    cbUpdate.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbUpdateActionPerformed(evt);
        }
    });

    cbInsert.setText("INSERT");
    cbInsert.setEnabled(false);

    cbOptionAll.setText("WITH GRANT OPTION");
    cbOptionAll.setEnabled(false);
    cbOptionAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbOptionAllActionPerformed(evt);
        }
    });

    cbSelect.setText("SELECT");
    cbSelect.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbSelectActionPerformed(evt);
        }
    });

    cbOptionUpdate.setText("WITH GRANT OPTION");
    cbOptionUpdate.setEnabled(false);

    cbOptionInsert.setText("WITH GRANT OPTION");
    cbOptionInsert.setEnabled(false);

    cbOptionSelect.setText("WITH GRANT OPTION");
    cbOptionSelect.setEnabled(false);

    cbDelete.setText("DELETE");
    cbDelete.setEnabled(false);

    cbOptionDelete.setText("WITH GRANT OPTION");
    cbOptionDelete.setEnabled(false);

    cbRule.setText("RULE");
    cbRule.setEnabled(false);

    cbOptionRule.setText("WITH GRANT OPTION");
    cbOptionRule.setEnabled(false);

    cbReferences.setText("REFERENCES");
    cbReferences.setEnabled(false);

    cbOptionReferences.setText("WITH GRANT OPTION");
    cbOptionReferences.setEnabled(false);

    cbTrigger.setText("TRIGGER");
    cbTrigger.setEnabled(false);

    cbOptionTrigger.setText("WITH GRANT OPTION");
    cbOptionTrigger.setEnabled(false);

    cbOptionUsage.setText("WITH GRANT OPTION");
    cbOptionUsage.setEnabled(false);

    cbUsage.setText("USAGE");
    cbUsage.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbUsageActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbRole, 0, 453, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbInsert)
                        .addComponent(cbSelect)
                        .addComponent(cbUpdate)
                        .addComponent(cbAll)
                        .addComponent(cbDelete)
                        .addComponent(cbRule)
                        .addComponent(cbReferences)
                        .addComponent(cbTrigger)
                        .addComponent(cbUsage))
                    .addGap(165, 165, 165)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbOptionUpdate)
                        .addComponent(cbOptionInsert)
                        .addComponent(cbOptionAll)
                        .addComponent(cbOptionSelect)
                        .addComponent(cbOptionDelete)
                        .addComponent(cbOptionRule)
                        .addComponent(cbOptionReferences)
                        .addComponent(cbOptionTrigger)
                        .addComponent(cbOptionUsage))))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblRole))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbOptionAll)
                .addComponent(cbAll))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbInsert)
                .addComponent(cbOptionInsert))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbSelect)
                .addComponent(cbOptionSelect))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbUpdate)
                .addComponent(cbOptionUpdate))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbDelete)
                .addComponent(cbOptionDelete))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbRule)
                .addComponent(cbOptionRule))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbReferences)
                .addComponent(cbOptionReferences))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbTrigger)
                .addComponent(cbOptionTrigger))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbUsage)
                .addComponent(cbOptionUsage))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlPrivilegesLayout = new javax.swing.GroupLayout(pnlPrivileges);
    pnlPrivileges.setLayout(pnlPrivilegesLayout);
    pnlPrivilegesLayout.setHorizontalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlPrivilegesLayout.createSequentialGroup()
                    .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlPrivilegesLayout.setVerticalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(45, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("privileges"), pnlPrivileges);
    pnlPrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    lblProvider.setText(constBundle.getString("provider"));

    tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    ));
    tblSecurityLabels.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblSecurityLabelsMousePressed(evt);
        }
    });
    jScrollPane3.setViewportView(tblSecurityLabels);
    if (tblSecurityLabels.getColumnModel().getColumnCount() > 0)
    {
        tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveSecurityLabelActionPerformed(evt);
        }
    });

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeSecurityLabelActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSecurityLabelLayout.createSequentialGroup()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addContainerGap(6, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
                .addComponent(txfdProvider))
            .addContainerGap())
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
                    .addGap(10, 10, 10))))
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(18, 18, 18)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addContainerGap(47, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    //allow positive intgeger, 0. and negative integer
    private void txfdValueKeyTyped(KeyEvent evt)                                       
    {
        JTextField txfd = (JTextField) evt.getSource();
        char keyCh = evt.getKeyChar();
        //logger.debug("keych:" + keyCh);
        if ((keyCh < 0 || keyCh > '9') && keyCh != '-')
        {
            logger.info("==========" + keyCh);
            //if (keyCh != ' ') //space 
            evt.setKeyChar('\0');
        } else if (keyCh == '-' && !txfd.getText().isEmpty())
        {
            evt.setKeyChar('\0');
        } else if (keyCh == '.' || keyCh == ' ')
        {
            evt.setKeyChar('\0');
        }
    }
    //only allow integer value>=1
    private void txfdCacheKeyTyped(KeyEvent evt)
    {
        char keyCh = evt.getKeyChar();
        logger.info("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            evt.setKeyChar('\0');
        } else if (keyCh == '0' && txfdCache.getText().isEmpty())
        {
            evt.setKeyChar('\0');
        } else if (keyCh == '.' || keyCh == ' ')
        {
            evt.setKeyChar('\0');
        }
    }     
    
    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);    
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnAddChangePrivilegesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangePrivilegesActionPerformed
    {//GEN-HEADEREND:event_btnAddChangePrivilegesActionPerformed
        // TODO add your handling code here:
        boolean select = cbSelect.isSelected();
        boolean update = cbUpdate.isSelected();
        boolean usage = cbUsage.isSelected();

        String usergroup = cbbRole.getSelectedItem().toString();
        StringBuilder privileges = new StringBuilder();
        if (select)
        {
            privileges.append("r");
        }
        if (cbOptionSelect.isSelected())
        {
            privileges.append("*");
        }
        if (update)
        {
            privileges.append("w");
        }
        if (cbOptionUpdate.isSelected())
        {
            privileges.append("*");
        }
        if (usage)
        {
            privileges.append("U");
        }
        if (cbOptionUsage.isSelected())
        {
            privileges.append("*");
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String usergroupOld = tableModel.getValueAt(i, 0).toString();
            logger.info("usergroupOld=" + usergroupOld + usergroupOld.equals(usergroup));
            if (usergroupOld.equals(usergroup))
            {
                isExist = true;
                tableModel.setValueAt(privileges.toString(), i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] p = new String[2];
            p[0] = usergroup;
            p[1] = privileges.toString();
            tableModel.addRow(p);
        }
    }//GEN-LAST:event_btnAddChangePrivilegesActionPerformed

    private void btnRemovePrivilegeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemovePrivilegeActionPerformed
    {//GEN-HEADEREND:event_btnRemovePrivilegeActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int selectedRow = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblPrivileges.getModel();
        tableModel.removeRow(selectedRow);     
    }//GEN-LAST:event_btnRemovePrivilegeActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
       logger.info(evt.getActionCommand());
       HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.SEQUENCE);       
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void cbAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbAllActionPerformed
    {//GEN-HEADEREND:event_cbAllActionPerformed
        // TODO add your handling code here:
        logger.info("cbAll click");
        if(cbAll.isSelected())
        {            
            cbSelect.setSelected(true);
            cbSelect.setEnabled(false);
            cbUpdate.setSelected(true);
            cbUpdate.setEnabled(false);  
            cbUsage.setSelected(true);
            cbUsage.setEnabled(false);
            if (cbbRole.getSelectedItem().toString().equals("public"))
            {
                cbOptionAll.setEnabled(false);
                cbOptionAll.setSelected(false);
                cbOptionSelect.setEnabled(false);
                cbOptionSelect.setSelected(false);
                cbOptionUpdate.setEnabled(false);
                cbOptionUpdate.setSelected(false);
                cbOptionUsage.setEnabled(false);
                cbOptionUsage.setSelected(false);
            } 
            else
            {
                cbOptionAll.setEnabled(true);
                cbOptionSelect.setEnabled(false);
                cbOptionUpdate.setEnabled(false);
                cbOptionUsage.setEnabled(false);
            }
        }
        else
        {            
            cbSelect.setSelected(false);
            cbSelect.setEnabled(true);
            cbUpdate.setSelected(false);
            cbUpdate.setEnabled(true);
            cbUsage.setSelected(false);
            cbUsage.setEnabled(true);
             if (!cbbRole.getSelectedItem().toString().equals("public"))
            {        
                cbOptionAll.setSelected(false);
                cbOptionAll.setEnabled(false);  
                cbOptionSelect.setSelected(false);
                cbOptionSelect.setEnabled(true);
                cbOptionUpdate.setSelected(false);
                cbOptionUpdate.setEnabled(true);
                cbOptionUsage.setSelected(false);
                cbOptionUsage.setEnabled(true);
            }
        }              
    }//GEN-LAST:event_cbAllActionPerformed

    private void cbbRoleItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbRoleItemStateChanged
    {//GEN-HEADEREND:event_cbbRoleItemStateChanged
        // TODO add your handling code here:
        String role = cbbRole.getSelectedItem().toString();
        logger.info("role:" + role );
        if(role.equals("public"))
        {
            cbOptionAll.setEnabled(false);
            cbOptionAll.setSelected(false);
            cbOptionSelect.setEnabled(false);
            cbOptionSelect.setSelected(false);
            cbOptionUpdate.setEnabled(false);
            cbOptionUpdate.setSelected(false);
            cbOptionUsage.setEnabled(false);
            cbOptionUsage.setSelected(false);
        }
        else
        {
           if(cbAll.isSelected())
           {
               cbOptionAll.setEnabled(true);
           }
           else
           {
                cbOptionAll.setSelected(false);                
                cbOptionAll.setEnabled(false);
                
                cbOptionSelect.setEnabled(true);
                cbOptionUpdate.setEnabled(true);
                cbOptionUsage.setEnabled(true);
           }
        }
    }//GEN-LAST:event_cbbRoleItemStateChanged

    private void tblPrivilegesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblPrivilegesMousePressed
    {//GEN-HEADEREND:event_tblPrivilegesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int row = tblPrivileges.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblPrivileges.getValueAt(row, 0).toString();
        cbbRole.setSelectedItem(user);
        String p = tblPrivileges.getValueAt(row, 1).toString();
        cbAll.setSelected(false);
        cbAll.setEnabled(true);
        cbSelect.setEnabled(true);
        cbUpdate.setEnabled(true);
        cbUsage.setEnabled(true);
        cbOptionAll.setSelected(false);
        if(!user.equals("public"))
        {
            cbOptionSelect.setEnabled(true);
            cbOptionUpdate.setEnabled(true);
            cbOptionUsage.setEnabled(true);
        }
        if (p.contains("r"))
        {
            cbSelect.setSelected(true);
            if(p.contains("r*"))
                cbOptionSelect.setSelected(true);
            else
                cbOptionSelect.setSelected(false);
        } else
        {
            cbSelect.setSelected(false);
            cbOptionSelect.setSelected(false);
        }
        if (p.contains("w"))
        {
            cbUpdate.setSelected(true);
            if(p.contains("w*"))
                cbOptionUpdate.setSelected(true);
            else
                cbOptionUpdate.setSelected(false);
        } else
        {
            cbUpdate.setSelected(false);
            cbOptionUpdate.setSelected(false);
        }
        if (p.contains("U"))
        {
            cbUsage.setSelected(true);
            if(p.contains("U*"))
                cbOptionUsage.setSelected(true);
            else
                cbOptionUsage.setSelected(false); 
        } else
        {
            cbUsage.setSelected(false);
            cbOptionUsage.setSelected(false); 
        }      
    }//GEN-LAST:event_tblPrivilegesMousePressed

    private void tblSecurityLabelsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblSecurityLabelsMousePressed
    {//GEN-HEADEREND:event_tblSecurityLabelsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press Security Label table");
        int row = tblSecurityLabels.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblSecurityLabels.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabels.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }//GEN-LAST:event_tblSecurityLabelsMousePressed

    private void cbOptionAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbOptionAllActionPerformed
    {//GEN-HEADEREND:event_cbOptionAllActionPerformed
        // TODO add your handling code here:
        if (cbOptionAll.isSelected())
        {
            cbOptionSelect.setSelected(true);
            cbOptionUpdate.setSelected(true);            
            cbOptionUsage.setSelected(true);
        }
        else
        {
            cbOptionSelect.setSelected(false);
            cbOptionUpdate.setSelected(false);         
            cbOptionUsage.setSelected(false);
        }              
    }//GEN-LAST:event_cbOptionAllActionPerformed

    private void cbSelectActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbSelectActionPerformed
    {//GEN-HEADEREND:event_cbSelectActionPerformed
        // TODO add your handling code here:
        if(!cbSelect.isSelected())
        {
            cbOptionSelect.setSelected(false);
        }
    }//GEN-LAST:event_cbSelectActionPerformed

    private void cbUpdateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbUpdateActionPerformed
    {//GEN-HEADEREND:event_cbUpdateActionPerformed
        // TODO add your handling code here:
        if(!cbUpdate.isSelected())
        {
            cbOptionUpdate.setSelected(false);
        }
    }//GEN-LAST:event_cbUpdateActionPerformed

    private void cbUsageActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbUsageActionPerformed
    {//GEN-HEADEREND:event_cbUsageActionPerformed
        // TODO add your handling code here:
        if(!cbUsage.isSelected())
        {
            cbOptionUsage.setSelected(false);
        }
    }//GEN-LAST:event_cbUsageActionPerformed

    //for add
    private void btnOkEnableForAdd(KeyEvent evt)
    {
        String dbName = txfdName.getText();
        btnOK.setEnabled(dbName != null && !dbName.isEmpty());
    }
    private void jtpForAddStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("index = " + index);
        if (index == 4)
        {
            SyntaxController sc = SyntaxController.getInstance();
            if (txfdName.getText() == null || txfdName.getText().isEmpty())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
                return;
            }
            tpDefinitionSQL.setText("");
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getSequenceInfo()));
        }
    }
    private void btnOKForAddActionPerformed(ActionEvent evt)                                      
    {                                        
        logger.info(evt.getActionCommand());
        logger.info("name=" + txfdName.getText());
        try
        {
            TreeController.getInstance().createObj(helperInfo, this.getSequenceInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    } 

    //for change
    private void btnOkEnableForChange()
    {
        String name = txfdName.getText();
        String acl = this.getACL();
        if (name != null && !name.isEmpty() && !name.equals(sequenceInfo.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (cbbOwner.getSelectedItem() != null && !cbbOwner.getSelectedItem().equals(sequenceInfo.getOwner()))
        {
            logger.info("owner changed");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(sequenceInfo.getComment())
                && !(txarComment.getText().isEmpty() && sequenceInfo.getComment()==null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else if (!acl.equals(sequenceInfo.getAcl())
                && !(acl.isEmpty() && sequenceInfo.getAcl() == null))
        {
            logger.info("ACL changed");
            btnOK.setEnabled(true);
        } else if (!txfdIncrement.getText().equals(sequenceInfo.getStrIncrement())
                || !txfdStart.getText().equals(sequenceInfo.getStrCurrent())
                || !txfdMiniValue.getText().equals(sequenceInfo.getStrMinimum())
                || !txfdMaxValue.getText().equals(sequenceInfo.getStrMaximum())
                || !txfdCache.getText().equals(sequenceInfo.getStrCache())
                || cbCycled.isSelected() != sequenceInfo.isCycled())
        {
            logger.info("parameter changed");
            btnOK.setEnabled(true);
        } else
        {
            logger.info("nothing changed");
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("index = " + index);
        if (index == 4)
        {
            SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
                return;
            }
            tpDefinitionSQL.setText("");
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getSequenceChangeSQL(this.sequenceInfo, this.getSequenceInfo()));
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        logger.info("name=" + txfdName.getText());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getDbName(), tc.getSequenceChangeSQL(this.sequenceInfo, this.getSequenceInfo()));
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
      
    //****************************    
    public boolean isSuccess()
    {
        return this.isSuccess;
    }
    private String getACL()
    {
        return AclUtil.getACL(cbbOwner.getSelectedItem().toString(), helperInfo, sequenceInfo, (DefaultTableModel) tblPrivileges.getModel());
    }
    public SequenceInfoDTO getSequenceInfo()
    {
        SequenceInfoDTO newseq = new SequenceInfoDTO();
        newseq.setHelperInfo(helperInfo);
        if (sequenceInfo != null)
        {
            newseq.setOid(sequenceInfo.getOid());
        }    
        if (cbbSchema.getSelectedItem() != null && !cbbSchema.getSelectedItem().toString().isEmpty())
        {
            newseq.setSchema(cbbSchema.getSelectedItem().toString());          ;
        }else
        {
            newseq.setSchema(helperInfo.getSchema());
            newseq.setSchemaOid(helperInfo.getSchemaOid());
        }
        newseq.setHelperInfo(helperInfo);
        newseq.setName(txfdName.getText());
        newseq.setComment(txarComment.getText());
        if (cbbOwner.getSelectedItem() == null || cbbOwner.getSelectedItem().toString().isEmpty())
        {
            newseq.setOwner(helperInfo.getUser());
        } else
        {
            newseq.setOwner(cbbOwner.getSelectedItem().toString());
        }
        newseq.setStrIncrement(txfdIncrement.getText());
        if (sequenceInfo != null)
        {
            newseq.setStrStart(sequenceInfo.getStrStart());
            newseq.setStrCurrent(txfdStart.getText());
        } else
        {
            newseq.setStrStart(txfdStart.getText());
            newseq.setStrCurrent(txfdStart.getText());
        }
        newseq.setStrMinimum(txfdMiniValue.getText());
        newseq.setStrMaximum(txfdMaxValue.getText());
        newseq.setStrCache(txfdCache.getText());
        newseq.setCycled(cbCycled.isSelected());
        //for privilege        
        newseq.setAcl(this.getACL());        
        //for security label
        /*
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.info("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        newseq.setSecurityLabelList(securityList);
        */
        return newseq;
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangePrivileges;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemovePrivilege;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JCheckBox cbAll;
    private javax.swing.JCheckBox cbCycled;
    private javax.swing.JCheckBox cbDelete;
    private javax.swing.JCheckBox cbInsert;
    private javax.swing.JCheckBox cbOptionAll;
    private javax.swing.JCheckBox cbOptionDelete;
    private javax.swing.JCheckBox cbOptionInsert;
    private javax.swing.JCheckBox cbOptionReferences;
    private javax.swing.JCheckBox cbOptionRule;
    private javax.swing.JCheckBox cbOptionSelect;
    private javax.swing.JCheckBox cbOptionTrigger;
    private javax.swing.JCheckBox cbOptionUpdate;
    private javax.swing.JCheckBox cbOptionUsage;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbReferences;
    private javax.swing.JCheckBox cbRule;
    private javax.swing.JCheckBox cbSelect;
    private javax.swing.JCheckBox cbTrigger;
    private javax.swing.JCheckBox cbUpdate;
    private javax.swing.JCheckBox cbUsage;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.JComboBox cbbRole;
    private javax.swing.JComboBox cbbSchema;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblCache;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblCycled;
    private javax.swing.JLabel lblIncrement;
    private javax.swing.JLabel lblMaximun;
    private javax.swing.JLabel lblMinimum;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSchema;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblStart;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlPrivileges;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblPrivileges;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdCache;
    private javax.swing.JTextField txfdIncrement;
    private javax.swing.JTextField txfdMaxValue;
    private javax.swing.JTextField txfdMiniValue;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdStart;
    private javax.swing.JTextField txfdUseSlony;
    // End of variables declaration//GEN-END:variables
}
