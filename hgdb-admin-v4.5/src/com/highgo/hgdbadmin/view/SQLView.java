/* ------------------------------------------------ 
* 
* File: SQLView.java
*
* Abstract: 
*     
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\SQLView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;
import com.highgo.hgdbadmin.event.TreeNodeChangeEventHandler;
import com.highgo.hgdbadmin.model.ObjGroupDTO;
import com.highgo.hgdbadmin.model.ObjInfoDTO;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ViewColumnInfoDTO;
import com.highgo.hgdbadmin.util.event.SimpleEventBus;
import java.awt.Font;
import java.util.ResourceBundle;
import javax.swing.tree.DefaultMutableTreeNode;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class SQLView extends RSyntaxTextArea //JTextPane
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private SimpleEventBus eventBus;
    
    private  RSyntaxTextArea rta;
    public SQLView(SimpleEventBus eventBus)
    {
        this.eventBus = eventBus;     
//        Font fontNormal = new Font(constBundle.getString("fontSongTi"),Font.PLAIN, 12);//constBundle.getString("fontSongTi")
//        this.setFont(fontNormal);
//        this.setEditable(false);
        
        this.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
        this.setCodeFoldingEnabled(true);
        this.setBracketMatchingEnabled(false);
        this.setFont(new Font(constBundle.getString("fontSongTi"),Font.PLAIN, 12));
        
        addHandler();
    }

    private void addHandler()
    {
        eventBus.addHandler(TreeNodeChangeEvent.TYPE, new TreeNodeChangeEventHandler()
        {
            @Override
            public void onChanged(TreeNodeChangeEvent evt)
            {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) evt.getNodeObject();
                logger.debug("current node = "+node);
                if(node==null)
                {
                    return;
                }
                populateData(node.getUserObject());
            }
        });
    }
    
    private void populateData(Object obj)
    {
        this.setText("");//clear the pane
        if (obj == null)
        {
            logger.error("obj is null, do nothing, return.");
            return;
        }
        if (obj instanceof ObjGroupDTO)
        {
            logger.info("This is a group node , no sql for it.");
            return;
        }
        if (obj instanceof ObjInfoDTO)
        {
            logger.info("This node still get detail information");
            return;
        }
        TreeController tc = TreeController.getInstance();
        if (obj instanceof AbstractObject)
        {
            AbstractObject object = (AbstractObject) obj;
            this.setText("-- " + object.getType().toString() + ": " + obj.toString() + "\n");
            String dropSQL = tc.getDropSQL(object, object.getHelperInfo(), false);
            if (obj instanceof ViewColumnInfoDTO)
            {
                //no defination and do nothing
            } else if (dropSQL != null)
            {
                this.setText(this.getText() + "-- " + dropSQL.replace("\n", "\n-- ") + "\n"
                        + tc.getDefinitionSQL(object));
            } else if (dropSQL == null && obj instanceof ColumnInfoDTO)
            {
                this.setText(this.getText() + "-- Inherited column, cannot be changed\n"
                        + tc.getDefinitionSQL(object));
            } else
            {
                this.setText(this.getText() + "-- SYSTEM " + object.getType().toString() + "\n");
            }
        }
    }

}
