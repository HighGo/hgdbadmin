/* ------------------------------------------------ 
* 
* File: EventTriggerView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\EventTriggerView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.EventTriggerInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * 
 */
public class EventTriggerView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private HelperInfoDTO helperInfo;
    private boolean isSuccess = false;
    
    private EventTriggerInfoDTO info;
    
    //add
    public EventTriggerView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.info = null;
        initComponents();
        jtp.setEnabledAt(2, false);//disable security label
        this.setTitle(constBundle.getString("addEventTrigger"));
        this.setNormalInfo(false);
        
        
        //enable btnOK
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAdd();
            }
        });
        cbbFuncs.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                 btnOkEnableForAdd();
            }
        });
        ActionListener listener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnableForAdd();
            }
        };
        rbtnDDLStart.addActionListener(listener);
        rbtnDDLEnd.addActionListener(listener);
        rbtnSQLDrop.addActionListener(listener);
        
        //tab change
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        
        //ok
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });  
    }

    //property  
    public EventTriggerView(JFrame parent, boolean modal, EventTriggerInfoDTO info)
    {
        super(parent, modal);
        this.helperInfo = info.getHelperInfo();
        this.info = info;
        initComponents();
        jtp.setEnabledAt(2, false);//disable security label
        this.setTitle(constBundle.getString("eventTrigger") + " " + info.getName());
        this.setNormalInfo(true);
        
        //property
        txfdName.setText(info.getName());
        txfdOID.setText(String.valueOf(info.getOid()));
        cbbOwner.setSelectedItem(info.getOwner());
        txarComment.setText(info.getComment());
        //defination
        txfdWhen.setText(info.getWhenTag());
        //cbbFuncs.addItem(info.getFunc());
        cbbFuncs.setSelectedItem(info.getFunc());
        switch(info.getEnableWhat())
        {
            case "ENABLE":
                rbtnEnable.setSelected(true);
                break;
            case "DISABLE":
                rbtnDisable.setSelected(true);
                break;
            case "REPLICA":
                rbtnReplica.setSelected(true);
                break;
            case "ALWAYS":
                rbtnAlways.setSelected(true);
                break;
        }
        switch(info.getEvent())
        {
            case "DDL_COMMAND_START":
                rbtnDDLStart.setSelected(true);
                break;
            case "DDL_COMMAND_END":
                rbtnDDLEnd.setSelected(true);
                break;
            case "SQL_DROP":
                rbtnSQLDrop.setSelected(true);
                break;
        }
        
        //security 
        /*List<SecurityLabelInfoDTO> slist = newInfo.getSecurityLabelList();
        if (slist != null)
        {
            DefaultTableModel slModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[3];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                slModel.addRow(sl);
            }
        }*/
        
        
        //define action
        KeyAdapter adapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        };
        txfdName.addKeyListener(adapter);
        cbbOwner.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        txarComment.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        });
        cbbFuncs.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        ActionListener listener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnableForChange();
            }
        };
        rbtnEnable.addActionListener(listener);
        rbtnDisable.addActionListener(listener);
        rbtnReplica.addActionListener(listener);
        rbtnAlways.addActionListener(listener);
        rbtnDDLStart.addActionListener(listener);
        rbtnDDLEnd.addActionListener(listener);
        rbtnSQLDrop.addActionListener(listener);
        txfdWhen.addKeyListener(adapter);
          
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });  
    }

    private void setNormalInfo(boolean isChange)
    {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/event_trigger.png")));
        try
        {
            String[] owners = (String[]) TreeController.getInstance()
                    .getItemsArrary(helperInfo, "owner");
            cbbOwner.setModel(new DefaultComboBoxModel(owners));
            if(isChange)
            {
                cbbOwner.setSelectedIndex(0);
            }else
            {
                cbbOwner.removeItemAt(0);
                cbbOwner.setSelectedIndex(-1);
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            String[] funcs = (String[]) TreeController.getInstance()
                    .getItemsArrary(helperInfo, "event_trigger_function");
            logger.debug("funcsize=" + funcs.length);
            cbbFuncs.setModel(new DefaultComboBoxModel(funcs));
            cbbFuncs.setSelectedIndex(-1);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        enableBtnGroup = new javax.swing.ButtonGroup();
        eventBtnGroup = new javax.swing.ButtonGroup();
        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        pnlDefination = new javax.swing.JPanel();
        lblEncoding = new javax.swing.JLabel();
        lblTemplate = new javax.swing.JLabel();
        lblTablesapce = new javax.swing.JLabel();
        lblCollation = new javax.swing.JLabel();
        rbtnDisable = new javax.swing.JRadioButton();
        rbtnEnable = new javax.swing.JRadioButton();
        rbtnAlways = new javax.swing.JRadioButton();
        rbtnReplica = new javax.swing.JRadioButton();
        cbbFuncs = new javax.swing.JComboBox<>();
        rbtnDDLStart = new javax.swing.JRadioButton();
        rbtnDDLEnd = new javax.swing.JRadioButton();
        rbtnSQLDrop = new javax.swing.JRadioButton();
        txfdWhen = new javax.swing.JTextField();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addDB"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/database.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane1.setViewportView(txarComment);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
        .addGap(10, 10, 10)
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txfdOID)
            .addComponent(txfdName)
            .addComponent(cbbOwner, 0, 399, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addGap(5, 5, 5)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(5, 5, 5)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE))
            .addContainerGap())
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    pnlDefination.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefination.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlDefination.setPreferredSize(new java.awt.Dimension(520, 470));

    lblEncoding.setText(constBundle.getString("enable"));

    lblTemplate.setText(constBundle.getString("triggerFunction"));

    lblTablesapce.setText(constBundle.getString("event"));

    lblCollation.setText(constBundle.getString("whatTime"));

    enableBtnGroup.add(rbtnDisable);
    rbtnDisable.setText("DISABLE");

    enableBtnGroup.add(rbtnEnable);
    rbtnEnable.setSelected(true);
    rbtnEnable.setText("ENABLE");

    enableBtnGroup.add(rbtnAlways);
    rbtnAlways.setText("ALWAYS");

    enableBtnGroup.add(rbtnReplica);
    rbtnReplica.setText("REPLICA");

    eventBtnGroup.add(rbtnDDLStart);
    rbtnDDLStart.setText("DDL_COMMAND_START");

    eventBtnGroup.add(rbtnDDLEnd);
    rbtnDDLEnd.setText("DDL_COMMAND_END");

    eventBtnGroup.add(rbtnSQLDrop);
    rbtnSQLDrop.setText("SQL_DROP");

    javax.swing.GroupLayout pnlDefinationLayout = new javax.swing.GroupLayout(pnlDefination);
    pnlDefination.setLayout(pnlDefinationLayout);
    pnlDefinationLayout.setHorizontalGroup(
        pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinationLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinationLayout.createSequentialGroup()
                    .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblTablesapce, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblTemplate, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(10, 10, 10)
                    .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbbFuncs, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(rbtnSQLDrop, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(rbtnDDLStart, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txfdWhen, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbtnDDLEnd, javax.swing.GroupLayout.Alignment.LEADING))))
                .addGroup(pnlDefinationLayout.createSequentialGroup()
                    .addComponent(lblEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(rbtnEnable)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(rbtnDisable)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(rbtnReplica)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(rbtnAlways))
                .addComponent(lblCollation, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10))
    );
    pnlDefinationLayout.setVerticalGroup(
        pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinationLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(rbtnEnable)
                .addComponent(rbtnDisable)
                .addComponent(rbtnReplica)
                .addComponent(rbtnAlways)
                .addComponent(lblEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblTemplate, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(pnlDefinationLayout.createSequentialGroup()
                    .addComponent(cbbFuncs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(rbtnDDLStart)
                        .addComponent(lblTablesapce, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(rbtnDDLEnd)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(rbtnSQLDrop)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlDefinationLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblCollation)
                .addComponent(txfdWhen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(278, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefination);

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    lblProvider.setText(constBundle.getString("provider"));

    tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][] {

        },
        new String [] {
            "", ""
        }
    ));
    tblSecurityLabels.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mousePressed(java.awt.event.MouseEvent evt) {
            tblSecurityLabelsMousePressed(evt);
        }
    });
    jScrollPane3.setViewportView(tblSecurityLabels);
    if (tblSecurityLabels.getColumnModel().getColumnCount() > 0) {
        tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnRemoveSecurityLabelActionPerformed(evt);
        }
    });

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnAddChangeSecurityLabelActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane3)
            .addGap(10, 10, 10))
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(375, 375, 375))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSecurityLabelLayout.createSequentialGroup()
                    .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdSecurityLabel)
                        .addComponent(txfdProvider))
                    .addContainerGap())))
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 483, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could be entered all integer.
        char keyCh = evt.getKeyChar();
        logger.info("keych=" + keyCh + ",isDigit=" + Character.isDigit(keyCh));
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '' && (keyCh != '-'))
            {
                evt.setKeyChar('\0');
            }
        }
    } 
      
    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);    
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.DATABASE);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void tblSecurityLabelsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblSecurityLabelsMousePressed
    {//GEN-HEADEREND:event_tblSecurityLabelsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press Security Label table");
        int row = tblSecurityLabels.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblSecurityLabels.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabels.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }//GEN-LAST:event_tblSecurityLabelsMousePressed

    //for add
    private void btnOkEnableForAdd()
    {
        //logger.newInfo("dbName:" + txfdName.getText());
        btnOK.setEnabled(!txfdName.getText().isEmpty()
                && cbbFuncs.getSelectedIndex() != -1
                && this.getEvent()!=null);
    }
    private void jtpForAddStateChanged(ChangeEvent evt)                                 
    {             
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:"+index);
        SyntaxController sc = SyntaxController.getInstance();      
        if (index == (jtp.getTabCount()-1))
        {
            if (txfdName.getText() == null || txfdName.getText().equals(""))
            {
                tpDefinitionSQL.setText("");//clear
                sc.setComment(tpDefinitionSQL.getDocument(), "-- "+constBundle.getString("definitonIncomplete"));
                return;
            }            
            tpDefinitionSQL.setText("");//clear
            EventTriggerInfoDTO newDbInfo = getEventTriggerInfo();
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(newDbInfo));
        }
    }   
    private void btnOKForAddActionPerformed(ActionEvent evt)
    {
        logger.debug(evt.getActionCommand());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.createObj(helperInfo, this.getEventTriggerInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error( ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    } 
    
   //for change
    private void btnOkEnableForChange()
    {
        if (txfdName.getText() == null || txfdName.getText().isEmpty())
        {
            logger.info("name is empty");
            btnOK.setEnabled(false);
        } else if (!txfdName.getText().equals(info.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (cbbOwner.getSelectedItem() != null 
                && !cbbOwner.getSelectedItem().equals(info.getOwner()))
        {
            logger.info("owner changed");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(info.getComment())
                && !(txarComment.getText().isEmpty() && info.getComment() == null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else if(!getEnableContent().equals(info.getEnableWhat()))
        {
            logger.info("enable changed");
            btnOK.setEnabled(true);
        } else if (!cbbFuncs.getSelectedItem().equals(info.getFunc()))
        {
            logger.info("function changed");
            btnOK.setEnabled(true);
        } else if (!getEvent().equals(info.getEvent()))
        {
            logger.info("event changed");
            btnOK.setEnabled(true);
        }else if (!info.getWhenTag().equals(txfdWhen.getText().trim()))
        {
            logger.info("when tag changed");
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
       
        if (index == (jtp.getTabCount()-1))
        {
            tpDefinitionSQL.setText("");//clear
             SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getEventTriggerChangeSQL(info, this.getEventTriggerInfo()));
            }
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        logger.info(txfdName.getText() + "," + cbbOwner.getSelectedItem().toString());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getMaintainDB(), tc.getEventTriggerChangeSQL(info, this.getEventTriggerInfo()));
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error( ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    } 
    
    private String getEnableContent()
    {
        String enable = null;
        if (rbtnEnable.isSelected()) {
            enable = "ENABLE";
        } else if (rbtnDisable.isSelected()) {
            enable = "DISABLE";
        } else if (rbtnReplica.isSelected()) {
            enable = "REPLICA";
        } else if(rbtnAlways.isSelected()){
            enable = "ALWAYS";
        }
        return enable;
    }
    private String getEvent()
    {
        String event = null;
        if (rbtnDDLStart.isSelected()) {
            event = "DDL_COMMAND_START";
        } else if (rbtnDDLEnd.isSelected()) {
            event = "DDL_COMMAND_END";
        } else if (rbtnSQLDrop.isSelected()) {
            event = "SQL_DROP";
        }
        return event;
    }
    public EventTriggerInfoDTO getEventTriggerInfo()
    {
        EventTriggerInfoDTO newInfo = new EventTriggerInfoDTO();
                
        newInfo.setHelperInfo(helperInfo);
        newInfo.setName(txfdName.getText());
        newInfo.setOwner(cbbOwner.getSelectedItem()==null?helperInfo.getUser() : cbbOwner.getSelectedItem().toString());
        newInfo.setComment(txarComment.getText());
        //defination
        newInfo.setEnableWhat(getEnableContent());
        newInfo.setFunc(cbbFuncs.getSelectedItem().toString());
        newInfo.setEvent(getEvent());
        newInfo.setWhenTag(txfdWhen.getText().trim()); 
        
        if (info != null)
        {
            newInfo.setOid(info.getOid());
            //info.setDatLastSysOid(newInfo.getDatLastSysOid());

            String oldWhen = info.getWhenTag() == null ? "" : info.getWhenTag();
            String newWhen = newInfo.getWhenTag() == null ? "" : newInfo.getWhenTag();
            if (!newInfo.getFunc().equals(info.getFunc())
                    || !newInfo.getEvent().equals(info.getEvent())
                    || !newWhen.equals(oldWhen)) {
                //if func,event and when tag changed then drop old and create new
                newInfo.setOid(null);
            }
        }
        
        //for security label
        /*
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.newInfo("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        newInfo.setSecurityLabelList(securityList);
        */
        return newInfo;
    }
    public boolean isSuccess()
    {
        return this.isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JComboBox<String> cbbFuncs;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.ButtonGroup enableBtnGroup;
    private javax.swing.ButtonGroup eventBtnGroup;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblCollation;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblEncoding;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblTablesapce;
    private javax.swing.JLabel lblTemplate;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefination;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JRadioButton rbtnAlways;
    private javax.swing.JRadioButton rbtnDDLEnd;
    private javax.swing.JRadioButton rbtnDDLStart;
    private javax.swing.JRadioButton rbtnDisable;
    private javax.swing.JRadioButton rbtnEnable;
    private javax.swing.JRadioButton rbtnReplica;
    private javax.swing.JRadioButton rbtnSQLDrop;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdWhen;
    // End of variables declaration//GEN-END:variables
}
