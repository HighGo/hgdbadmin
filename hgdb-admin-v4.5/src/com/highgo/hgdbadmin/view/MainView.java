/* ------------------------------------------------ 
* 
* File: MainView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\MainView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.audit.view.test.AuditLogDialog;
import com.highgo.hgdbadmin.audit.view.RegisterDialog;
import com.highgo.hgdbadmin.backup.view.BackupAllView;
import com.highgo.hgdbadmin.backup.view.BackupView;
import com.highgo.hgdbadmin.backup.view.RestoreView;
import com.highgo.hgdbadmin.configureeditor.controller.ConfigController;
import com.highgo.hgdbadmin.datamanager.view.DataSortFilterDialog;
import com.highgo.hgdbadmin.datamanager.view.DataEditView;
import com.highgo.hgdbadmin.maintance.view.MaintanceView;
import com.highgo.hgdbadmin.configureeditor.view.ConfigMainView;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.datamanager.controller.DataController;
import com.highgo.hgdbadmin.datatransfer.view.DataExportView;
import com.highgo.hgdbadmin.datatransfer.view.DataImportView;
import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;
import com.highgo.hgdbadmin.event.TreeNodeChangeEventHandler;
import com.highgo.hgdbadmin.migrationassistant.view.MigrationView;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.CastInfoDTO;
import com.highgo.hgdbadmin.model.CatalogInfoDTO;
import com.highgo.hgdbadmin.model.ColItemInfoDTO;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ConnectInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.EventTriggerInfoDTO;
import com.highgo.hgdbadmin.model.FunObjInfoDTO;
import com.highgo.hgdbadmin.model.FunctionInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.IndexInfoDTO;
import com.highgo.hgdbadmin.model.LanguageInfoDTO;
import com.highgo.hgdbadmin.model.ObjGroupDTO;
import com.highgo.hgdbadmin.model.ObjInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.RoleInfoDTO;
import com.highgo.hgdbadmin.model.RuleInfoDTO;
import com.highgo.hgdbadmin.model.SchemaInfoDTO;
import com.highgo.hgdbadmin.model.SequenceInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.TablespaceInfoDTO;
import com.highgo.hgdbadmin.model.TriggerInfoDTO;
import com.highgo.hgdbadmin.model.ViewColumnInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.optioneditor.view.OptionEditView;
import com.highgo.hgdbadmin.query.view.QueryMainView;
import com.highgo.hgdbadmin.serverconfig.view.ServerConfigView;
import com.highgo.hgdbadmin.serverstatus.view.ServerStatusView;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.util.MessageUtil;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.event.SimpleEventBus;
import com.highgo.hgdbadmin.view.dialog.PwdChangeDialog;
import com.highgo.hgdbadmin.view.dialog.ReassignDropOwnedToDialog;
import com.highgo.hgdbadmin.view.dialog.MoveTablespaceDialog;
import com.highgo.hgdbadmin.view.dialog.PwdInputDialog;
import com.highgo.hgdbadmin.view.dialog.ServerControlDialog;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.undo.UndoManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author Liu Yuanyuan
 * 
 * hgdbadmin dir: ./conf/
 *  pgadmin_favourites.xml
 *  pgadmin_histoqueries.xml
 *  pgadmin_macros.xml
 *  catalog.xml instead of pgpass.conf and regedit info
 * 
 */
public class MainView extends JFrame
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private ResourceBundle msgBundle = ResourceBundle.getBundle("messages");
    
    private SimpleEventBus eventBus;
    private String[] serverGroup;
    private TreeView treeView;
    private SQLView sqlView;
    
    private DefaultMutableTreeNode node;
    private UndoManager undoManager;
    
    /**
     * Creates new form Hgdbadmin
     *
     * @param eventBus
     */
    public MainView(SimpleEventBus eventBus)
    {        
        this.eventBus = eventBus;
        this.initComponents();
        this.disableUselessItems();
        
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle(constBundle.getString("hgdbAdminTitle"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/hgdb.png")));
        
        this.initTreeRightClickItems();
        treeView = new TreeView(eventBus, this);
        spaneTree.setViewportView(treeView);
        serverGroup = treeView.getServerGroup();
        treeView.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent evt)
            {
                treeNodePressAction(evt);
            }
        });
        
        PropertyView propertyView = new PropertyView(eventBus);
        spaneProperty.setViewportView(propertyView);

        sqlView = new SQLView(eventBus);
        spaneSqlWindow.setViewportView(sqlView);
        sqlView.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseReleased(MouseEvent e)
            {
                 populateCopyItemForSql(e);
            }
        });

        StatisticsView sv = new StatisticsView(eventBus);
        spaneStatistics.setViewportView(sv);
        
        this.addTreeNodeChangeHandler();
        
        //for menu item action perform
        this.registerAllAction();
        
        //for InActivity timeout
        this.registInActivityAction();
    }
    
    private void disableUselessItems()
    {
        //toolbar
        //add begin by sunqk at 2019.05.31 for hgdb4.3.4.2 remove oracle2hgdb 
        toolBar.remove(btnMigrationAssistant);
        toolBar.remove(btnAudit);
        //add end by sunqk at 2019.05.31 for hgdb4.3.4.2 remove oracle2hgdb
        toolBar.remove(btnExecuteLastPlugs);
        toolBar.remove(btnShowObjSuggestion);
        //menubar
        menuFile.remove(miSaveDefine);
        menuFile.remove(jsepAdd);
        menuFile.remove(miOpenPostgresql);
        menuFile.remove(miOpenPGHba);
        menuFile.remove(miOpenCatalog);
        menuFile.remove(jsepExit);
        menuBar.remove(menuPlugs);
        menuEdit.remove(miExtendDataTab);
        menuEdit.remove(miExtendIndex);
        menuTool.remove(menuDebug);
        menuTool.remove(menuReport);
        menuTool.remove(miWeighted);
        menuTool.remove(miServerConfig);
        menuTool.remove(menuConfigEditor);
        menuTool.remove(jsepRun);
        menuTool.remove(miRun);
        //add begin by sunqk at 2019.05.31 for hgdb4.3.4.2 remove oracle2hgdb 
        menuTool.remove(miMigrationAssistant);
        //add end by sunqk at 2019.05.31 for hgdb4.3.4.2 remove oracle2hgdb 
        menuHelp.remove(miHgdbAdminHelp);
        menuHelp.remove(miSuggestion);
        menuHelp.remove(miFAQ);
        menuHelp.remove(miBugReport);
        menuHelp.remove(spAbout);
        menuHelp.remove(miAbout);
        //properties
        tabbpane.remove(spaneDependent);//tabbpane.setEnabledAt(2, false);
        tabbpane.remove(spanePaneRelation);//tabbpane.setEnabledAt(3, false);
    }
    
    //for InActivity timeout
    private int inactivityTimeout = 0; 
    private PwdInputDialog timeoutDialog = null;
    private void registInActivityAction()
    {
        try
        {
            inactivityTimeout = Integer.valueOf(OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "inactivity_timeout"));
            logger.debug("inactivity_timeout=" + inactivityTimeout);
            if (inactivityTimeout <= 0)
            {
                logger.warn("inactivity timeout <=0, not regist inactivity action and return.");
                return;
            }
            Action hintAction = new AbstractAction()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    pwdInputHint();
                }
            };
            InactivityListener listener = new InactivityListener(this, hintAction, inactivityTimeout);//min
            listener.start();
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void pwdInputHint()
    {
        logger.debug("Enter");
        
        if (timeoutDialog != null)
        {
            //timeoutDialog.requestFocusInWindow();
            logger.debug("timeoutDialog already display.....");
            return;
        }

        HelperInfoDTO serverInfo = treeView.getCurrentConnInfo(true);//to test
        if (serverInfo == null)
        {
            logger.debug("last selected sever is null......");
            return;
        } else if(serverInfo.getHost()== null
                || serverInfo.getHost().isEmpty())
        {
            logger.debug("last selected sever is not connected......");
            return;
        }

        String url = serverInfo.getPartURL() + serverInfo.getMaintainDB();
        String message = MessageFormat.format(msgBundle.getString("pleaseEnterPasswordForUser"), serverInfo.getUser())
                + "\n" + MessageFormat.format(msgBundle.getString("onServer"),
                        (serverInfo.getPartURL() + serverInfo.getMaintainDB()).replace("jdbc:highgo://", ""));
        timeoutDialog = new PwdInputDialog(this, true, message, true);
        //timeoutDialog.setUsername(serverInfo.getUser());
        timeoutDialog.setLocationRelativeTo(this);
        timeoutDialog.setVisible(true);
        if (timeoutDialog.isSuccess())
        {
            String user = serverInfo.getUser();
            String pwd = timeoutDialog.getPassword();
            Connection conn = null;
            try
            {
                conn = JdbcHelper.getConnection(url, user, pwd, serverInfo.isOnSSL());
                serverInfo.setPwd(pwd);
                DefaultMutableTreeNode currentServerNode = (DefaultMutableTreeNode) treeView.getLastSelectedPathComponent();
                if (currentServerNode != null && currentServerNode.getUserObject() instanceof XMLServerInfoDTO)
                {
                    XMLServerInfoDTO server = (XMLServerInfoDTO) currentServerNode.getUserObject();
                    TreeController.getInstance().getMoreServerInfo(server);
                    eventBus.fireEvent(new TreeNodeChangeEvent(currentServerNode));
                    logger.info("------------ Fire Node = " + currentServerNode.toString() + " ------------");
                }
                timeoutDialog = null;
            } catch (SQLException ex)
            {
                treeView.dealSQLException(ex);
                timeoutDialog = null;
                pwdInputHint();
            } catch (ClassNotFoundException ex)
            {
                MessageUtil.showErrorMsg(this, ex);
                timeoutDialog = null;
                pwdInputHint();
            } finally
            {
                JdbcHelper.close(conn);
                timeoutDialog = null;
            }
        }
    }          
    
    //add for sm user privilege limit(for 2020gc-hgdb-sm3 )
    private void limitUserExecutePrivilege()
    {
        if(node == null || node.getUserObject() == null)
        {
            logger.warn("no node selected, so return.");
            return;
        }
        
        Object obj = node.getUserObject();
        if (obj == null || obj instanceof XMLServerInfoDTO) 
        {
            logger.warn("server node has no limit, so return.");
            return;
        }
        
        HelperInfoDTO helper = treeView.getCurrentConnInfo(false);
        if (helper == null || helper.getUser() == null || helper.getUser().isEmpty()) 
        {
            logger.warn("cannot get current user.");
            return;
        }

        TreeEnum.TreeNode type = null;
        if (obj instanceof AbstractObject)
        {
            AbstractObject abobj = (AbstractObject) obj;
            type = abobj.getType();
            logger.debug("obj=" + abobj.getName() + ", owner=" + abobj.getOwner() + ", type=" + abobj.getType());
        } else if (obj instanceof ObjGroupDTO)
        {
            ObjGroupDTO group = (ObjGroupDTO) obj;
            type = group.getType();
            logger.debug("group=" + group.getName() + ", type=" + group.getType());
        }
        if (type == null)
        {
            logger.warn("obj type is null so return .");
            return;
        }

        final String loginUser = helper.getUser();
        logger.debug("loginUser=" + loginUser);
        boolean isCreate = false;
        boolean isChange = false;
        boolean isDelete = false;
        boolean isBackupRestore = false;
        boolean isImportExport = false;
        boolean isViewData = false;
        switch (type)
        {
            case GROUPROLE_GROUP:
            case LOGINROLE_GROUP:
                isCreate = "sysdba".equals(loginUser);
                //no change and delete execution for group
                break;
            case LOGINROLE:
            case GROUPROLE:
                isCreate = "sysdba".equals(loginUser);
                String roleName = ((AbstractObject) obj).getName();
                isDelete = "sysdba".equals(loginUser)
                        && !"sysdba".equals(roleName) && !"syssso".equals(roleName) && !"syssao".equals(roleName);
                switch (loginUser)
                {
                    case "sysdba":
                        isChange = true;//change all
                        break;
                    case "syssso":
                        isChange = !("sysdba".equals(roleName) &&  "syssao".equals(roleName));//change self and non-sys
                        break;
                    case "syssao":
                        isChange = loginUser.equals(roleName);//change self
                        break;
                    default:
                        isChange = loginUser.equals(roleName);//change self
                        break;
                }
                break;
            case TABLESPACE_GROUP:
                isCreate = "sysdba".equals(loginUser);
                //no change and delete execution for group
                break;
            case TABLESPACE:
                isCreate = "sysdba".equals(loginUser);
                isChange = "sysdba".equals(loginUser);
                isDelete = "sysdba".equals(loginUser);
                break;
            case DATABASE_GROUP:
                isCreate = "sysdba".equals(loginUser);
                //no change and delete execution for group
                break;
            case DATABASE:
                isCreate = "sysdba".equals(loginUser);
                isChange = "sysdba".equals(loginUser);
                isDelete = "sysdba".equals(loginUser);
                String dbowner = ((AbstractObject) obj).getOwner();//
                isBackupRestore = loginUser.equals("sysdba") || loginUser.equals(dbowner);
                break;
            case COLUMN_GROUP:
            case CONSTRAINT_GROUP:
            case INDEX_GROUP:
            case RULE_GROUP:
            case TRIGGER_GROUP:
            case PARTITION_GROUP:
                isCreate = !"syssao".equals(loginUser) && !"syssso".equals(loginUser);
                //no change and delete execution for group
                break;
            case COLUMN:
            //constraint
            case PRIMARY_KEY:
            case FOREIGN_KEY:
            case EXCLUDE:
            case CHECK:
            case UNIQUE:
            case INDEX:
            case RULE:
            case TRIGGER:
            case PARTITION:
                isCreate = !"syssao".equals(loginUser) && !"syssso".equals(loginUser);
                DefaultMutableTreeNode columnGroup = (DefaultMutableTreeNode) node.getParent();
                DefaultMutableTreeNode tabNode = (DefaultMutableTreeNode) columnGroup.getParent();
                TableInfoDTO tab = (TableInfoDTO) tabNode.getUserObject();
                isChange = !loginUser.equals("syssso") && !loginUser.equals("syssao") && loginUser.equals(tab.getOwner());
                isDelete = isChange;
                isViewData = isChange || hasPrivilegeSelect(loginUser, tab.getAcl());
                break;
            default:
                isCreate = !"syssao".equals(loginUser) && !"syssso".equals(loginUser);
                if (obj instanceof AbstractObject)//obj node 
                {
                    String owner = ((AbstractObject) obj).getOwner();
                    logger.debug(owner);
                    isChange = !loginUser.equals("syssso") && !loginUser.equals("syssao") && loginUser.equals(owner) ;
                    isDelete = isChange;
                    isBackupRestore = loginUser.equals("sysdba") || loginUser.equals(owner);
                    isImportExport = !loginUser.equals("syssao") && !loginUser.equals("syssso")
                                && (loginUser.equals("sysdba") || loginUser.equals(owner));
                    if (obj instanceof TableInfoDTO || obj instanceof ViewInfoDTO)
                    {
                        String tvacl = (obj instanceof TableInfoDTO) ? 
                                ((TableInfoDTO) obj).getAcl() : ((ViewInfoDTO) obj).getAcl();
                        isViewData = isChange || hasPrivilegeSelect(loginUser, tvacl);
                    }
                } else
                {
                    logger.debug("group has no change and delete execution for group");
                }
                break;
        }
        miCreate.setEnabled(miCreate.isEnabled() && isCreate);
        btnCreate.setEnabled(btnCreate.isEnabled() && isCreate);
        itemCreate.setEnabled(isCreate);
        menuTreeAddObj.setEnabled(isCreate);

        miProperty.setEnabled(miProperty.isEnabled() && isChange);
        btnProperty.setEnabled(btnProperty.isEnabled() && isChange);
        itemProperty.setEnabled(isChange);

        miDrop.setEnabled(miDrop.isEnabled() && isDelete);
        miDropCascade.setEnabled(miDropCascade.isEnabled() && isDelete);
        btnDrop.setEnabled(btnDrop.isEnabled() && isDelete);
        itemDrop.setEnabled(isDelete);
        itemDropCascade.setEnabled(isDelete);

        // import/export
        miImportData.setEnabled(miImportData.isEnabled() && isImportExport);
        btnImportData.setEnabled(btnImportData.isEnabled() && isImportExport);
        itemImportData.setEnabled(isImportExport);
        miExportData.setEnabled(miImportData.isEnabled() && isImportExport);
        btnExportData.setEnabled(btnImportData.isEnabled() && isImportExport);
        itemExportData.setEnabled(isImportExport);
        
        // backup/restore
        miBackupAll.setEnabled(miBackupAll.isEnabled() && isBackupRestore);
        itemBackupAll.setEnabled(isBackupRestore);
        miBackup.setEnabled(miBackup.isEnabled() && isBackupRestore);
        itemBackup.setEnabled(isBackupRestore);
        miRestore.setEnabled(miRestore.isEnabled() && isBackupRestore);
        itemRestore.setEnabled(isBackupRestore);

        //view data: table/view/partition
        menuViewData.setEnabled(menuViewData.isEnabled() && isViewData);;
        btnDataView.setEnabled(btnDataView.isEnabled() && isViewData);
        btnFilterDataView.setEnabled(btnFilterDataView.isEnabled() && isViewData);
                
        /*
        //others execuion needn't limit now
        miMaintainData.setEnabled(miMaintainData.isEnabled() && isdba);
        btnMaintainData.setEnabled(btnMaintainData.isEnabled() && isdba);
        itemMaintainData.setEnabled(isdba);

        miTruncateTable.setEnabled(miTruncateTable.isEnabled() && isdba);
        miTruncateTableCascade.setEnabled(miTruncateTableCascade.isEnabled() && isdba); 
        itemTruncateTable.setEnabled(isdba);
        itemTruncateTableCascade.setEnabled(isdba); 

        cbmiEnableTrigger.setEnabled(cbmiEnableTrigger.isEnabled() && isdba);
        cbitemEnableTrigger.setEnabled(isdba);

        cbmiEnableRule.setEnabled(cbmiEnableRule.isEnabled() && isdba);
        cbitemEnableRule.setEnabled(isdba);

        miEnableAllTrigger.setEnabled(miEnableAllTrigger.isEnabled() && isdba);
        itemEnableAllTrigger.setEnabled(isdba);

        miDisableAllTrigger.setEnabled(miDisableAllTrigger.isEnabled() && isdba);
        itemDisableAllTrigger.setEnabled(isdba);

        miReassignDropOwnedTo.setEnabled(miReassignDropOwnedTo.isEnabled() && isdba);
        itemReassignDropOwnedTo.setEnabled(isdba);
        
        miMoveTablespace.setEnabled(miMoveTablespace.isEnabled() && isdba);
        itemMoveTablespace.setEnabled(isdba);

        miReloadConfiguration.setEnabled(miReloadConfiguration.isEnabled() && isdba);
        itemReloadConfiguration.setEnabled(isdba);

        miAddNameRestorePoint.setEnabled(miAddNameRestorePoint.isEnabled() && isdba);
        itemAddNameRestorePoint.setEnabled(isdba);

        miPauseWALReplay.setEnabled(miPauseWALReplay.isEnabled() && isdba);
        itemPauseWALReplay.setEnabled(isdba);

        miPauseWALReplay.setEnabled(miPauseWALReplay.isEnabled() && isdba);
        itemResumeWALReplay.setEnabled(isdba);
        */
    }
    private boolean hasPrivilegeSelect(String loginUser, String acl)
    {
        if(acl == null || acl.isEmpty())
        {
            return false;
        }
        if (acl.contains(loginUser))
        {
            String[] acls = acl.split(",");
            for (String a : acls)
            {
                logger.debug(a);
                if (a.contains("r"))
                {
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        spCopy = new javax.swing.JPopupMenu.Separator();
        toolBar = new javax.swing.JToolBar();
        btnAddServer = new javax.swing.JButton();
        btnCreate = new javax.swing.JButton();
        btnProperty = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnDrop = new javax.swing.JButton();
        btnExportData = new javax.swing.JButton();
        btnImportData = new javax.swing.JButton();
        btnQuery = new javax.swing.JButton();
        btnDataView = new javax.swing.JButton();
        btnFilterDataView = new javax.swing.JButton();
        btnMaintainData = new javax.swing.JButton();
        btnMigrationAssistant = new javax.swing.JButton();
        btnAudit = new javax.swing.JButton();
        btnExecuteLastPlugs = new javax.swing.JButton();
        btnShowObjSuggestion = new javax.swing.JButton();
        btnShowSqlHelp = new javax.swing.JButton();
        jSplitPaneLeft = new javax.swing.JSplitPane();
        spaneTree = new javax.swing.JScrollPane();
        spaneRight = new javax.swing.JSplitPane();
        tabbpane = new javax.swing.JTabbedPane();
        spaneProperty = new javax.swing.JScrollPane();
        spaneStatistics = new javax.swing.JScrollPane();
        spaneDependent = new javax.swing.JScrollPane();
        spanePaneRelation = new javax.swing.JScrollPane();
        spaneSqlWindow = new javax.swing.JScrollPane();
        jtxfState = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        miSaveDefine = new javax.swing.JMenuItem();
        jsepAdd = new javax.swing.JPopupMenu.Separator();
        miAddServer = new javax.swing.JMenuItem();
        miChangePwd = new javax.swing.JMenuItem();
        jsepChange = new javax.swing.JPopupMenu.Separator();
        miOption = new javax.swing.JMenuItem();
        jsepOpt = new javax.swing.JPopupMenu.Separator();
        miOpenPostgresql = new javax.swing.JMenuItem();
        miOpenPGHba = new javax.swing.JMenuItem();
        miOpenCatalog = new javax.swing.JMenuItem();
        jsepExit = new javax.swing.JPopupMenu.Separator();
        miExit = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        miCopy = new javax.swing.JMenuItem();
        spCreate = new javax.swing.JPopupMenu.Separator();
        miCreate = new javax.swing.JMenuItem();
        miDrop = new javax.swing.JMenuItem();
        miDropCascade = new javax.swing.JMenuItem();
        miTruncateTable = new javax.swing.JMenuItem();
        miTruncateTableCascade = new javax.swing.JMenuItem();
        miReassignDropOwnedTo = new javax.swing.JMenuItem();
        miMoveTablespace = new javax.swing.JMenuItem();
        spRefesh = new javax.swing.JPopupMenu.Separator();
        miRefresh = new javax.swing.JMenuItem();
        miRowCount = new javax.swing.JMenuItem();
        miExtendIndex = new javax.swing.JMenuItem();
        miExtendDataTab = new javax.swing.JMenuItem();
        spProperty = new javax.swing.JPopupMenu.Separator();
        miProperty = new javax.swing.JMenuItem();
        menuPlugs = new javax.swing.JMenu();
        miConsole = new javax.swing.JMenuItem();
        menuView = new javax.swing.JMenu();
        cbmiObjBrowser = new javax.swing.JCheckBoxMenuItem();
        cbmiSqlWindow = new javax.swing.JCheckBoxMenuItem();
        cbmiToolbar = new javax.swing.JCheckBoxMenuItem();
        spView = new javax.swing.JPopupMenu.Separator();
        miDefView = new javax.swing.JMenuItem();
        menuTool = new javax.swing.JMenu();
        cbmiEnableRule = new javax.swing.JCheckBoxMenuItem();
        cbmiEnableTrigger = new javax.swing.JCheckBoxMenuItem();
        miDisableAllTrigger = new javax.swing.JMenuItem();
        miEnableAllTrigger = new javax.swing.JMenuItem();
        jsepConnect = new javax.swing.JPopupMenu.Separator();
        miConnServer = new javax.swing.JMenuItem();
        miDisconnServer = new javax.swing.JMenuItem();
        miStartServer = new javax.swing.JMenuItem();
        miStopServer = new javax.swing.JMenuItem();
        jsepReload = new javax.swing.JPopupMenu.Separator();
        miReloadConfiguration = new javax.swing.JMenuItem();
        miAddNameRestorePoint = new javax.swing.JMenuItem();
        miPauseWALReplay = new javax.swing.JMenuItem();
        miResumeWALReplay = new javax.swing.JMenuItem();
        jsepDebug = new javax.swing.JPopupMenu.Separator();
        menuDebug = new javax.swing.JMenu();
        miAudit = new javax.swing.JMenuItem();
        miMigrationAssistant = new javax.swing.JMenuItem();
        miQuery = new javax.swing.JMenuItem();
        menuScript = new javax.swing.JMenu();
        miCreateScript = new javax.swing.JMenuItem();
        miSelectScript = new javax.swing.JMenuItem();
        miExecScript = new javax.swing.JMenuItem();
        miInsertScript = new javax.swing.JMenuItem();
        miUpdateScript = new javax.swing.JMenuItem();
        miDeleteScript = new javax.swing.JMenuItem();
        menuViewData = new javax.swing.JMenu();
        miDataViewAll = new javax.swing.JMenuItem();
        miDataFilter = new javax.swing.JMenuItem();
        menuReport = new javax.swing.JMenu();
        miPropertyReport = new javax.swing.JMenuItem();
        miDDLReport = new javax.swing.JMenuItem();
        miDataDictionaryReport = new javax.swing.JMenuItem();
        miStatisticsReport = new javax.swing.JMenuItem();
        miDependentReport = new javax.swing.JMenuItem();
        miAffiliatedRelationReport = new javax.swing.JMenuItem();
        miObjListReport = new javax.swing.JMenuItem();
        jsepReport = new javax.swing.JPopupMenu.Separator();
        miMaintainData = new javax.swing.JMenuItem();
        miBackupAll = new javax.swing.JMenuItem();
        miBackup = new javax.swing.JMenuItem();
        miRestore = new javax.swing.JMenuItem();
        miExportData = new javax.swing.JMenuItem();
        miImportData = new javax.swing.JMenuItem();
        miWeighted = new javax.swing.JMenuItem();
        miServerConfig = new javax.swing.JMenuItem();
        menuConfigEditor = new javax.swing.JMenu();
        miPostgresql = new javax.swing.JMenuItem();
        miHba = new javax.swing.JMenuItem();
        jsepRun = new javax.swing.JPopupMenu.Separator();
        miRun = new javax.swing.JMenuItem();
        jsepServer = new javax.swing.JPopupMenu.Separator();
        miServerState = new javax.swing.JMenuItem();
        menuHelp = new javax.swing.JMenu();
        miHgdbHelp = new javax.swing.JMenuItem();
        miHgdbAdminHelp = new javax.swing.JMenuItem();
        miSuggestion = new javax.swing.JMenuItem();
        miFAQ = new javax.swing.JMenuItem();
        miBugReport = new javax.swing.JMenuItem();
        spAbout = new javax.swing.JPopupMenu.Separator();
        miAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setName(""); // NOI18N

        toolBar.setRollover(true);
        toolBar.setPreferredSize(new java.awt.Dimension(123, 45));

        btnAddServer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/connect.png"))); // NOI18N
        btnAddServer.setToolTipText(constBundle.getString("addServerCon"));
        btnAddServer.setFocusable(false);
        btnAddServer.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAddServer.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnAddServer);

        btnCreate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/create.png"))); // NOI18N
        btnCreate.setToolTipText(constBundle.getString("createNewObj"));
        btnCreate.setEnabled(false);
        btnCreate.setFocusable(false);
        btnCreate.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCreate.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCreate);

        btnProperty.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/properties.png"))); // NOI18N
        btnProperty.setToolTipText(constBundle.getString("showObjectProperty"));
        btnProperty.setEnabled(false);
        btnProperty.setFocusable(false);
        btnProperty.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnProperty.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnProperty);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/refresh.png"))); // NOI18N
        btnRefresh.setToolTipText(constBundle.getString("refreshObject"));
        btnRefresh.setEnabled(false);
        btnRefresh.setFocusable(false);
        btnRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnRefresh);

        btnDrop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/drop.png"))); // NOI18N
        btnDrop.setToolTipText(constBundle.getString("delObj"));
        btnDrop.setEnabled(false);
        btnDrop.setFocusable(false);
        btnDrop.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDrop.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnDrop);

        btnExportData.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/export.png"))); // NOI18N
        btnExportData.setToolTipText(constBundle.getString("exportData")
        );
        btnExportData.setEnabled(false);
        btnExportData.setFocusable(false);
        btnExportData.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExportData.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnExportData);

        btnImportData.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/import.png"))); // NOI18N
        btnImportData.setToolTipText(constBundle.getString("importData"));
        btnImportData.setEnabled(false);
        btnImportData.setFocusable(false);
        btnImportData.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImportData.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnImportData);

        btnQuery.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/sql-32.png"))); // NOI18N
        btnQuery.setToolTipText(constBundle.getString("executeQuery"));
        btnQuery.setEnabled(false);
        btnQuery.setFocusable(false);
        btnQuery.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnQuery.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnQuery);

        btnDataView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/viewdata.png"))); // NOI18N
        btnDataView.setToolTipText(constBundle.getString("lookObjData"));
        btnDataView.setEnabled(false);
        btnDataView.setFocusable(false);
        btnDataView.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDataView.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnDataView);

        btnFilterDataView.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/viewfiltereddata.png"))); // NOI18N
        btnFilterDataView.setToolTipText(constBundle.getString("filterLookData"));
        btnFilterDataView.setEnabled(false);
        btnFilterDataView.setFocusable(false);
        btnFilterDataView.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFilterDataView.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnFilterDataView);

        btnMaintainData.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/vacuum.png"))); // NOI18N
        btnMaintainData.setToolTipText(constBundle.getString("maintainDatabase"));
        btnMaintainData.setEnabled(false);
        btnMaintainData.setFocusable(false);
        btnMaintainData.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnMaintainData.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnMaintainData);

        btnMigrationAssistant.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/migrateAssistant.png"))); // NOI18N
        btnMigrationAssistant.setToolTipText(constBundle.getString("executeMigration"));
        btnMigrationAssistant.setFocusable(false);
        btnMigrationAssistant.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnMigrationAssistant.setPreferredSize(new java.awt.Dimension(39, 39));
        btnMigrationAssistant.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnMigrationAssistant);

        btnAudit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/audit.png"))); // NOI18N
        btnAudit.setToolTipText(constBundle.getString("executeAudit"));
        btnAudit.setFocusable(false);
        btnAudit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnAudit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnAudit);

        btnExecuteLastPlugs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/plugins.png"))); // NOI18N
        btnExecuteLastPlugs.setToolTipText(constBundle.getString("executeLastPlug"));
        btnExecuteLastPlugs.setFocusable(false);
        btnExecuteLastPlugs.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExecuteLastPlugs.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnExecuteLastPlugs);

        btnShowObjSuggestion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/hint.png"))); // NOI18N
        btnShowObjSuggestion.setToolTipText(constBundle.getString("showObjSuggestion"));
        btnShowObjSuggestion.setEnabled(false);
        btnShowObjSuggestion.setFocusable(false);
        btnShowObjSuggestion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowObjSuggestion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnShowObjSuggestion);

        btnShowSqlHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/help2.png"))); // NOI18N
        btnShowSqlHelp.setToolTipText(constBundle.getString("openManualHelp"));
        btnShowSqlHelp.setFocusable(false);
        btnShowSqlHelp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnShowSqlHelp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnShowSqlHelp);

        getContentPane().add(toolBar, java.awt.BorderLayout.NORTH);

        jSplitPaneLeft.setDividerLocation(240);
        jSplitPaneLeft.setDividerSize(6);
        jSplitPaneLeft.setLeftComponent(spaneTree);

        spaneRight.setDividerLocation(200);
        spaneRight.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        tabbpane.setBackground(new java.awt.Color(255, 255, 255));
        tabbpane.addTab(constBundle.getString("property"), spaneProperty);
        tabbpane.addTab(constBundle.getString("statistics"), spaneStatistics);

        spaneDependent.setEnabled(false);
        tabbpane.addTab(constBundle.getString("dependence"), spaneDependent);

        spanePaneRelation.setEnabled(false);
        tabbpane.addTab(constBundle.getString("affiliation"), spanePaneRelation);

        spaneRight.setTopComponent(tabbpane);
        tabbpane.getAccessibleContext().setAccessibleName("");
        tabbpane.setEnabledAt(2, false);
        tabbpane.setEnabledAt(3, false);

        spaneSqlWindow.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createCompoundBorder(), constBundle.getString("sqlWindowShow"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.ABOVE_TOP));
        spaneRight.setRightComponent(spaneSqlWindow);
        spaneSqlWindow.getAccessibleContext().setAccessibleName(constBundle.getString("sqlWindowShow"));

        jSplitPaneLeft.setRightComponent(spaneRight);

        getContentPane().add(jSplitPaneLeft, java.awt.BorderLayout.CENTER);

        jtxfState.setEditable(false);
        getContentPane().add(jtxfState, java.awt.BorderLayout.SOUTH);

        menuFile.setText(constBundle.getString("file"));

        miSaveDefine.setText(constBundle.getString("saveDefine"));
        menuFile.add(miSaveDefine);
        menuFile.add(jsepAdd);

        miAddServer.setText(constBundle.getString("addServer"));
        menuFile.add(miAddServer);

        miChangePwd.setText(constBundle.getString("changePassword"));
        miChangePwd.setEnabled(false);
        menuFile.add(miChangePwd);
        menuFile.add(jsepChange);

        miOption.setText(constBundle.getString("option"));
        menuFile.add(miOption);
        menuFile.add(jsepOpt);

        miOpenPostgresql.setText(constBundle.getString("openConf"));
        miOpenPostgresql.setEnabled(false);
        menuFile.add(miOpenPostgresql);

        miOpenPGHba.setText(constBundle.getString("openHba"));
        miOpenPGHba.setEnabled(false);
        menuFile.add(miOpenPGHba);

        miOpenCatalog.setText(constBundle.getString("openCatalog"));
        menuFile.add(miOpenCatalog);
        menuFile.add(jsepExit);

        miExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        miExit.setText(constBundle.getString("exit"));
        menuFile.add(miExit);

        menuBar.add(menuFile);

        menuEdit.setText(constBundle.getString("edit"));

        miCopy.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        miCopy.setText(constBundle.getString("copy"));
        miCopy.setEnabled(false);
        menuEdit.add(miCopy);
        menuEdit.add(spCreate);

        miCreate.setText(constBundle.getString("create"));
        miCreate.setEnabled(false);
        menuEdit.add(miCreate);

        miDrop.setText(constBundle.getString("drop"));
        miDrop.setEnabled(false);
        menuEdit.add(miDrop);

        miDropCascade.setText(constBundle.getString("dropCascade"));
        miDropCascade.setEnabled(false);
        menuEdit.add(miDropCascade);

        miTruncateTable.setText(constBundle.getString("truncate"));
        miTruncateTable.setEnabled(false);
        menuEdit.add(miTruncateTable);

        miTruncateTableCascade.setText(constBundle.getString("truncateCascade"));
        miTruncateTableCascade.setEnabled(false);
        menuEdit.add(miTruncateTableCascade);

        miReassignDropOwnedTo.setText(constBundle.getString("ownerChange"));
        miReassignDropOwnedTo.setEnabled(false);
        menuEdit.add(miReassignDropOwnedTo);

        miMoveTablespace.setText(constBundle.getString("tablespaceChange"));
        miMoveTablespace.setEnabled(false);
        menuEdit.add(miMoveTablespace);
        menuEdit.add(spRefesh);

        miRefresh.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        miRefresh.setText(constBundle.getString("refresh"));
        menuEdit.add(miRefresh);

        miRowCount.setText(constBundle.getString("count"));
        miRowCount.setEnabled(false);
        menuEdit.add(miRowCount);

        miExtendIndex.setText(constBundle.getString("extendIndexCount"));
        miExtendIndex.setEnabled(false);
        menuEdit.add(miExtendIndex);

        miExtendDataTab.setText(constBundle.getString("extendDataTable"));
        miExtendDataTab.setEnabled(false);
        menuEdit.add(miExtendDataTab);
        menuEdit.add(spProperty);

        miProperty.setText(constBundle.getString("property"));
        miProperty.setEnabled(false);
        menuEdit.add(miProperty);

        menuBar.add(menuEdit);

        menuPlugs.setText(constBundle.getString("plug"));

        miConsole.setText("PSQL  Console");
        menuPlugs.add(miConsole);

        menuBar.add(menuPlugs);

        menuView.setText(constBundle.getString("view"));

        cbmiObjBrowser.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbmiObjBrowser.setSelected(true);
        cbmiObjBrowser.setText(constBundle.getString("objectBrower"));
        menuView.add(cbmiObjBrowser);

        cbmiSqlWindow.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbmiSqlWindow.setSelected(true);
        cbmiSqlWindow.setText(constBundle.getString("sqlWindow"));
        menuView.add(cbmiSqlWindow);

        cbmiToolbar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbmiToolbar.setSelected(true);
        cbmiToolbar.setText(constBundle.getString("toolbar"));
        menuView.add(cbmiToolbar);
        menuView.add(spView);

        miDefView.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        miDefView.setText(constBundle.getString("defaultView"));
        menuView.add(miDefView);

        menuBar.add(menuView);

        menuTool.setText(constBundle.getString("tool"));

        cbmiEnableRule.setText(constBundle.getString("enableRule"));
        cbmiEnableRule.setEnabled(false);
        menuTool.add(cbmiEnableRule);

        cbmiEnableTrigger.setText(constBundle.getString("enableTrigger"));
        cbmiEnableTrigger.setEnabled(false);
        menuTool.add(cbmiEnableTrigger);

        miDisableAllTrigger.setText(constBundle.getString("disableAllTrigger"));
        miDisableAllTrigger.setEnabled(false);
        menuTool.add(miDisableAllTrigger);

        miEnableAllTrigger.setText(constBundle.getString("enableAllTrigger"));
        miEnableAllTrigger.setEnabled(false);
        menuTool.add(miEnableAllTrigger);
        menuTool.add(jsepConnect);

        miConnServer.setText(constBundle.getString("connect"));
        miConnServer.setEnabled(false);
        menuTool.add(miConnServer);

        miDisconnServer.setText(constBundle.getString("disconnect"));
        miDisconnServer.setEnabled(false);
        menuTool.add(miDisconnServer);

        miStartServer.setText(constBundle.getString("startServer"));
        miStartServer.setEnabled(false);
        menuTool.add(miStartServer);

        miStopServer.setText(constBundle.getString("stopServer"));
        miStopServer.setEnabled(false);
        menuTool.add(miStopServer);
        menuTool.add(jsepReload);

        miReloadConfiguration.setText(constBundle.getString("reloadConfigure"));
        miReloadConfiguration.setEnabled(false);
        menuTool.add(miReloadConfiguration);

        miAddNameRestorePoint.setText(constBundle.getString("addNameRestorePoint"));
        miAddNameRestorePoint.setEnabled(false);
        menuTool.add(miAddNameRestorePoint);

        miPauseWALReplay.setText(constBundle.getString("pauseWALReplay")
        );
        miPauseWALReplay.setEnabled(false);
        menuTool.add(miPauseWALReplay);

        miResumeWALReplay.setText(constBundle.getString("resumeWALReplay"));
        miResumeWALReplay.setEnabled(false);
        menuTool.add(miResumeWALReplay);
        menuTool.add(jsepDebug);

        menuDebug.setText(constBundle.getString("debug"));
        menuDebug.setEnabled(false);
        menuTool.add(menuDebug);

        miAudit.setText(constBundle.getString("auditTool"));
        miAudit.setEnabled(false);
        menuTool.add(miAudit);

        miMigrationAssistant.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        miMigrationAssistant.setText(constBundle.getString("migrationAssistant"));
        menuTool.add(miMigrationAssistant);

        miQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        miQuery.setText(constBundle.getString("queryTool"));
        miQuery.setEnabled(false);
        menuTool.add(miQuery);

        menuScript.setText(constBundle.getString("script"));
        menuScript.setEnabled(false);

        miCreateScript.setText(constBundle.getString("createScript"));
        miCreateScript.setEnabled(false);
        menuScript.add(miCreateScript);

        miSelectScript.setText(constBundle.getString("selectScript"));
        miSelectScript.setEnabled(false);
        menuScript.add(miSelectScript);

        miExecScript.setText(constBundle.getString("execScript"));
        miExecScript.setEnabled(false);
        menuScript.add(miExecScript);

        miInsertScript.setText(constBundle.getString("insertScript"));
        miInsertScript.setEnabled(false);
        menuScript.add(miInsertScript);

        miUpdateScript.setText(constBundle.getString("updateScript"));
        miUpdateScript.setEnabled(false);
        menuScript.add(miUpdateScript);

        miDeleteScript.setText(constBundle.getString("deleteScript"));
        miDeleteScript.setEnabled(false);
        menuScript.add(miDeleteScript);

        menuTool.add(menuScript);

        menuViewData.setText(constBundle.getString("lookData"));
        menuViewData.setEnabled(false);

        miDataViewAll.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        miDataViewAll.setText(constBundle.getString("lookAllRow"));
        menuViewData.add(miDataViewAll);

        miDataFilter.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.CTRL_MASK));
        miDataFilter.setText(constBundle.getString("lookFilterRow"));
        menuViewData.add(miDataFilter);

        menuTool.add(menuViewData);

        menuReport.setText(constBundle.getString("report"));
        menuReport.setEnabled(false);

        miPropertyReport.setText(constBundle.getString("propertyReport"));
        menuReport.add(miPropertyReport);

        miDDLReport.setText(constBundle.getString("ddlReport"));
        menuReport.add(miDDLReport);

        miDataDictionaryReport.setText(constBundle.getString("dataDictionary"));
        menuReport.add(miDataDictionaryReport);

        miStatisticsReport.setText(constBundle.getString("statisticsReport"));
        menuReport.add(miStatisticsReport);

        miDependentReport.setText(constBundle.getString("dependentReport"));
        menuReport.add(miDependentReport);

        miAffiliatedRelationReport.setText(constBundle.getString("affiliatedRelationReport"));
        menuReport.add(miAffiliatedRelationReport);

        miObjListReport.setText(constBundle.getString("objListReport"));
        menuReport.add(miObjListReport);

        menuTool.add(menuReport);
        menuTool.add(jsepReport);

        miMaintainData.setText(constBundle.getString("maintenance"));
        miMaintainData.setEnabled(false);
        menuTool.add(miMaintainData);

        miBackupAll.setText(constBundle.getString("backupAll"));
        miBackupAll.setEnabled(false);
        menuTool.add(miBackupAll);

        miBackup.setText(constBundle.getString("backup"));
        miBackup.setEnabled(false);
        menuTool.add(miBackup);

        miRestore.setText(constBundle.getString("restore"));
        miRestore.setEnabled(false);
        menuTool.add(miRestore);

        miExportData.setText(constBundle.getString("exportData"));
        miExportData.setEnabled(false);
        menuTool.add(miExportData);

        miImportData.setText(constBundle.getString("importData"));
        miImportData.setEnabled(false);
        menuTool.add(miImportData);

        miWeighted.setText(constBundle.getString("weighted"));
        miWeighted.setEnabled(false);
        menuTool.add(miWeighted);

        miServerConfig.setText(constBundle.getString("serverConfig"));
        miServerConfig.setEnabled(false);
        menuTool.add(miServerConfig);

        menuConfigEditor.setText(constBundle.getString("configEditor"));

        miPostgresql.setText("postgresql.conf");
        miPostgresql.setEnabled(false);
        menuConfigEditor.add(miPostgresql);

        miHba.setText("pg_hba.conf");
        miHba.setEnabled(false);
        menuConfigEditor.add(miHba);

        menuTool.add(menuConfigEditor);
        menuTool.add(jsepRun);

        miRun.setText(constBundle.getString("run"));
        miRun.setEnabled(false);
        menuTool.add(miRun);
        menuTool.add(jsepServer);

        miServerState.setText(constBundle.getString("serverState"));
        miServerState.setEnabled(false);
        menuTool.add(miServerState);

        menuBar.add(menuTool);

        menuHelp.setText(constBundle.getString("help"));

        miHgdbHelp.setText(constBundle.getString("hgdbHelp"));
        menuHelp.add(miHgdbHelp);

        miHgdbAdminHelp.setText(constBundle.getString("hgdbAdminHelp"));
        menuHelp.add(miHgdbAdminHelp);

        miSuggestion.setText(constBundle.getString("suggestion"));
        miSuggestion.setEnabled(false);
        menuHelp.add(miSuggestion);

        miFAQ.setText("FAQ(F)");
        menuHelp.add(miFAQ);

        miBugReport.setText(constBundle.getString("bugReport"));
        menuHelp.add(miBugReport);
        menuHelp.add(spAbout);

        miAbout.setText(constBundle.getString("about"));
        menuHelp.add(miAbout);

        menuBar.add(menuHelp);

        setJMenuBar(menuBar);

        setSize(new java.awt.Dimension(800, 600));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
 
    
    //init tree right-click items
    private void initTreeRightClickItems()
    {
        popMenu = new JPopupMenu();
        separator0 = new JPopupMenu.Separator();
        separator1 = new JPopupMenu.Separator();
        separator2 = new JPopupMenu.Separator();
        
        menuTreeAddObj = new JMenu(constBundle.getString("addObject"));
        itemAddDB = new JMenuItem(constBundle.getString("addDB"));
        itemAddTablespace = new JMenuItem(constBundle.getString("addTablespace"));
        itemAddGroupRole = new JMenuItem(constBundle.getString("addGroupRole"));
        itemAddLoginRole = new JMenuItem(constBundle.getString("addLoginRole"));
        itemAddEventTrigger = new JMenuItem(constBundle.getString("addEventTrigger"));
        itemAddSchema = new JMenuItem(constBundle.getString("addSchema"));
        itemAddFunction = new JMenuItem(constBundle.getString("addFunction"));
        itemAddProcedure = new JMenuItem(constBundle.getString("addProcedure"));
        itemAddTable = new JMenuItem(constBundle.getString("addTable"));
        itemAddSequence = new JMenuItem(constBundle.getString("addSequence"));
        itemAddView = new JMenuItem(constBundle.getString("addView"));
        itemAddColumn = new JMenuItem(constBundle.getString("addColumn"));
        itemAddPK = new JMenuItem(constBundle.getString("addPKey"));
        itemAddFK = new JMenuItem(constBundle.getString("addFKey"));
        itemAddExclude = new JMenuItem(constBundle.getString("addExclude"));
        itemAddCheck = new JMenuItem(constBundle.getString("addCheck"));
        itemAddUnique = new JMenuItem(constBundle.getString("addUnique"));
        itemAddIndex = new JMenuItem(constBundle.getString("addIndex"));
        itemAddRule = new JMenuItem(constBundle.getString("addRule"));
        itemAddTrigger = new JMenuItem(constBundle.getString("addTrigger"));
        
        itemCreate = new JMenuItem(constBundle.getString("create"));
        
        itemDrop = new JMenuItem(constBundle.getString("drop"));
        itemDropCascade = new JMenuItem(constBundle.getString("dropCascade"));
        
        itemTruncateTable = new JMenuItem(constBundle.getString("truncate"));
        itemTruncateTableCascade = new JMenuItem(constBundle.getString("truncateCascade"));
        
        itemReassignDropOwnedTo = new JMenuItem(constBundle.getString("ownerChange"));        
        itemMoveTablespace = new JMenuItem(constBundle.getString("tablespaceChange"));
        
        itemChangePwd = new JMenuItem(constBundle.getString("changePassword"));
        itemConnServer = new JMenuItem(constBundle.getString("connect"));
        itemDisconnServer = new JMenuItem(constBundle.getString("disconnect"));
        itemAddServer = new JMenuItem(constBundle.getString("addServerCon"));
        itemProperty = new JMenuItem(constBundle.getString("property"));
        
        itemRefresh = new JMenuItem(constBundle.getString("refresh"));
        itemRowCount = new JMenuItem(constBundle.getString("count"));        
        
        menuTreeScript = new JMenu(constBundle.getString("script"));
        itemCreateScript = new JMenuItem(constBundle.getString("createScript"));
        itemSelectScript = new JMenuItem(constBundle.getString("selectScript"));
        itemInsertScript = new JMenuItem(constBundle.getString("insertScript"));
        itemDeleteScript = new JMenuItem(constBundle.getString("deleteScript"));
        itemUpdateScript = new JMenuItem(constBundle.getString("updateScript"));
        
        cbitemEnableConstraint = new JCheckBoxMenuItem(constBundle.getString("enableConstraint"));
        cbitemEnableTrigger = new JCheckBoxMenuItem(constBundle.getString("enableTrigger"));
        cbitemEnableRule = new JCheckBoxMenuItem(constBundle.getString("enableRule"));
        itemEnableAllTrigger = new JMenuItem(constBundle.getString("enableAllTrigger"));
        itemDisableAllTrigger = new JMenuItem(constBundle.getString("disableAllTrigger"));       
     
        itemMaintainData = new JMenuItem(constBundle.getString("maintain"));
        itemBackup = new JMenuItem(constBundle.getString("backup"));
        itemBackupAll = new JMenuItem(constBundle.getString("backupAll"));
        itemRestore = new JMenuItem(constBundle.getString("restore"));
        
        itemExportData =  new JMenuItem(constBundle.getString("exportData"));
        itemImportData = new JMenuItem(constBundle.getString("importData"));
        
        itemReloadConfiguration = new JMenuItem(constBundle.getString("reloadConfigure"));
        itemAddNameRestorePoint = new JMenuItem(constBundle.getString("addNameRestorePoint"));
        itemPauseWALReplay = new JMenuItem(constBundle.getString("pauseWALReplay"));
        itemResumeWALReplay = new JMenuItem(constBundle.getString("resumeWALReplay"));
    }
    //popup tree right-click items
    private void showTreeRightClickMenu(MouseEvent e)
    {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) treeView.getLastSelectedPathComponent(); 
        logger.debug("Current Node="+node.toString());
        popMenu.removeAll();
        menuTreeAddObj.removeAll();
        popMenu.add(itemRefresh);
        if (node.isRoot())
        {
            popMenu.show(treeView, e.getX(), e.getY());
            return;
        }      
        Object nodeObj = node.getUserObject();
        logger.info("ObjectClass=" + nodeObj.getClass().toString());
        if ((nodeObj instanceof ViewColumnInfoDTO)
                || (nodeObj instanceof CatalogInfoDTO)
                || (nodeObj instanceof LanguageInfoDTO))
        {
            popMenu.show(treeView, e.getX(), e.getY());
            return;
        }
        
        else if (nodeObj instanceof ObjGroupDTO)
        {
            //group cannot refresh and needn't seperator
            popMenu.remove(itemRefresh);
            //popMenu.add(separator0);
            DefaultMutableTreeNode parentNode =  (DefaultMutableTreeNode) node.getParent();
            Object parentObj = parentNode.getUserObject();            
            ObjGroupDTO obj = (ObjGroupDTO) nodeObj;
            TreeEnum.TreeNode nodeType = obj.getType();
            switch (nodeType)
            {
                case SERVER_GROUP:
                    popMenu.add(itemAddServer);
                    break;
                case DATABASE_GROUP:   
                case TABLESPACE_GROUP:
                case GROUPROLE_GROUP:
                case LOGINROLE_GROUP:
                case EVENT_TRIGGER_GROUP:
                case SCHEMA_GROUP:
                case SEQUENCE_GROUP:
                    popMenu.add(itemCreate);
                    break;
                case FUNCTION_GROUP:
                case PROCEDURE_GROUP:
                case TABLE_GROUP:
                case VIEW_GROUP:
                    if (!(parentObj instanceof CatalogInfoDTO)) {
                        popMenu.add(itemCreate);
                    }
                    break;             
                case CONSTRAINT_GROUP:
                    TableInfoDTO tbl1 = (TableInfoDTO) parentObj;
                    if (!tbl1.isSystem())
                    {
                        if (tbl1.getPrimaryKey() == null || tbl1.getPrimaryKey().isEmpty())
                        {
                            menuTreeAddObj.add(itemAddPK);
                        }
                        menuTreeAddObj.add(itemAddFK);
                        menuTreeAddObj.add(itemAddExclude);
                        menuTreeAddObj.add(itemAddCheck);
                        menuTreeAddObj.add(itemAddUnique);
                        popMenu.add(menuTreeAddObj);
                    }
                    break;
                case COLUMN_GROUP:
                    if (parentObj instanceof TableInfoDTO)
                    {
                        TableInfoDTO tbl = (TableInfoDTO) parentObj;
                        if (!tbl.isSystem())
                        {
                            popMenu.add(itemCreate);
                        }
                    }
                    break;
                case INDEX_GROUP:
                case RULE_GROUP:
                case TRIGGER_GROUP:          
                    if (parentObj instanceof TableInfoDTO)
                    {
                        TableInfoDTO tbl2 = (TableInfoDTO) parentObj;
                        if (tbl2.isSystem())
                        {
                            break;
                        }
                    } else if (parentObj instanceof ViewInfoDTO)
                    {
                        ViewInfoDTO view = (ViewInfoDTO) parentObj;
                        if (view.isSystem())
                        {
                            break;
                        }
                    } else
                    {
                        logger.error(nodeType + " is an Exception nodetype, do nothing and break.");
                        break;
                    }
                    switch (nodeType)
                    {
                        case INDEX_GROUP:
                        case RULE_GROUP:
                        case TRIGGER_GROUP:
                            popMenu.add(itemCreate);
                            break;
                    }
                    break;
                default:
                    popMenu.remove(separator0);
                    break;
            }
        } else
        {
            popMenu.add(separator0);
            if (nodeObj instanceof XMLServerInfoDTO)
            {
                XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) nodeObj;
                popMenu.add(itemAddServer);
                if (serverInfo.isConnected())
                {
                    popMenu.add(itemDisconnServer);
                    popMenu.add(itemChangePwd);
                    popMenu.add(itemReloadConfiguration);
                    TreeController tc = TreeController.getInstance();
                    HelperInfoDTO helper = tc.getHelperInfoFromServerInfo(serverInfo);
                    if (tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 0))
                    {
                        popMenu.add(itemAddNameRestorePoint);
                        if (serverInfo.isInRecovery() != null && serverInfo.isInRecovery().equals("true"))
                        {
                            popMenu.add(itemPauseWALReplay);
                            popMenu.add(itemResumeWALReplay);
                        }
                    }
                    popMenu.add(itemDrop);
                    popMenu.add(separator1);
                    
                    popMenu.add(itemBackupAll);
                } else
                {
                    //not connect server cannot refresh and needn't seperator
                    popMenu.remove(itemRefresh);
                    popMenu.remove(separator0);
                     
                    popMenu.add(itemConnServer);
                    popMenu.add(itemDrop);
                }
            } else if (nodeObj instanceof TablespaceInfoDTO)
            {
                popMenu.add(itemCreate);//popMenu.add(itemAddTablespace);
                popMenu.add(itemDrop);  
                TablespaceInfoDTO tablespace = (TablespaceInfoDTO) nodeObj;
                HelperInfoDTO helper = tablespace.getHelperInfo();
                TreeController tc = TreeController.getInstance();
                if (tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 3))
                {
                    popMenu.add(itemMoveTablespace);
                }
                popMenu.add(separator1);
                
                popMenu.add(itemCreateScript);
            } else if (nodeObj instanceof DBInfoDTO)
            {
                menuTreeAddObj.add(itemAddDB);
                menuTreeAddObj.add(itemAddSchema);
                popMenu.add(menuTreeAddObj);
                popMenu.add(itemDrop);
                popMenu.add(separator1);

                popMenu.add(itemCreateScript);
                popMenu.add(itemMaintainData);
                popMenu.add(itemBackup);
                popMenu.add(itemRestore);
            } else if (nodeObj instanceof RoleInfoDTO)
            {
                RoleInfoDTO role = (RoleInfoDTO) nodeObj;
                popMenu.add(itemCreate);
                popMenu.add(itemDrop);
                if (role.isCanLogin())
                {
                    //popMenu.add(itemAddLoginRole);
                    //popMenu.add(itemDrop);
                    popMenu.add(itemReassignDropOwnedTo);
                } else
                {
                    //popMenu.add(itemAddGroupRole);
                    //popMenu.add(itemDrop);
                }
                popMenu.add(separator1);

                popMenu.add(itemCreateScript);
            }
//            else if(nodeObj instanceof CatalogInfoDTO)
//            {
//                 popMenu.add(itemBackup);
//            }
            else if(nodeObj instanceof EventTriggerInfoDTO)
            {
                popMenu.add(itemCreate);//popMenu.add(itemAddEventTrigger);
                popMenu.add(itemDrop);
                popMenu.add(itemDropCascade);
                popMenu.add(separator1);
                popMenu.add(itemCreateScript);
            }
            else if (nodeObj instanceof SchemaInfoDTO)
            {
                menuTreeAddObj.add(itemAddSchema);
                menuTreeAddObj.add(itemAddSequence);
                menuTreeAddObj.add(itemAddTable);
                menuTreeAddObj.add(itemAddView);
                menuTreeAddObj.add(itemAddFunction);
                menuTreeAddObj.add(itemAddProcedure);
                popMenu.add(menuTreeAddObj);
                popMenu.add(itemDrop);
                popMenu.add(itemDropCascade);
                popMenu.add(separator1);

                popMenu.add(itemCreateScript);
                popMenu.add(itemBackup);
                popMenu.add(itemRestore);
            }else if(nodeObj instanceof FunctionInfoDTO)
            {
                //for func and procedure
                FunctionInfoDTO fun = (FunctionInfoDTO) nodeObj;
                if (!fun.isSystem())
                {
                    popMenu.add(itemCreate);//popMenu.add(TreeEnum.TreeNode.FUNCTION == fun.getType() ? itemAddFunction : itemAddProcedure);
                    popMenu.add(itemDrop);
                    popMenu.add(itemDropCascade);
                    popMenu.add(separator1);
                }
                popMenu.add(itemCreateScript);
            }            
            else if (nodeObj instanceof SequenceInfoDTO)
            {
                popMenu.add(itemCreate);//popMenu.add(itemAddSequence);
                popMenu.add(itemDrop);
                popMenu.add(itemDropCascade);
                popMenu.add(separator1);

                popMenu.add(itemCreateScript);
            } else if (nodeObj instanceof ViewInfoDTO)
            {
                ViewInfoDTO vw = (ViewInfoDTO) nodeObj;
                if (!vw.isSystem())
                {
                    menuTreeAddObj.add(itemAddView);
                    menuTreeAddObj.add(itemAddRule);
                    if (TreeController.getInstance().isVersionHigherThan(vw.getHelperInfo().getDbSys(),
                            vw.getHelperInfo().getVersionNumber(), 9, 0))
                    {
                        menuTreeAddObj.add(itemAddTrigger);
                    }
                    popMenu.add(menuTreeAddObj);
                    popMenu.add(itemDrop);
                    popMenu.add(itemDropCascade);
                    popMenu.add(separator1);
                }
                menuTreeScript = new JMenu(constBundle.getString("script"));
                menuTreeScript.add(itemCreateScript);
                menuTreeScript.add(itemSelectScript);
                popMenu.add(menuTreeScript);
            }
            else if (nodeObj instanceof TableInfoDTO)
            {
                TableInfoDTO tbl = (TableInfoDTO) nodeObj;
                popMenu.remove(separator0);
                popMenu.add(itemRowCount);                
                if (!tbl.isSystem())
                {
                    popMenu.add(itemEnableAllTrigger);
                    popMenu.add(itemDisableAllTrigger);
                    popMenu.add(separator1);

                    menuTreeAddObj.add(itemAddTable);
                    menuTreeAddObj.add(itemAddColumn);
                    if (tbl.getPrimaryKey() == null || tbl.getPrimaryKey().isEmpty())
                    {
                        menuTreeAddObj.add(itemAddPK);
                    }
                    menuTreeAddObj.add(itemAddFK);
                    menuTreeAddObj.add(itemAddExclude);
                    menuTreeAddObj.add(itemAddCheck);
                    menuTreeAddObj.add(itemAddUnique);
                    menuTreeAddObj.add(itemAddIndex);
                    menuTreeAddObj.add(itemAddRule);
                    menuTreeAddObj.add(itemAddTrigger);
                    popMenu.add(menuTreeAddObj);
                    popMenu.add(itemDrop);
                    popMenu.add(itemDropCascade);
                    popMenu.add(itemTruncateTable);
                    popMenu.add(itemTruncateTableCascade);
                }
                popMenu.add(separator2);

                menuTreeScript = new JMenu(constBundle.getString("script"));
                menuTreeScript.add(itemCreateScript);
                menuTreeScript.add(itemSelectScript);
                menuTreeScript.add(itemInsertScript);
                menuTreeScript.add(itemDeleteScript);
                menuTreeScript.add(itemUpdateScript);
                popMenu.add(menuTreeScript);
                popMenu.add(itemMaintainData);
                popMenu.add(itemBackup);
                if (!tbl.isSystem())
                {
                    popMenu.add(itemRestore);
                }
                popMenu.add(itemExportData);
                if (!tbl.isSystem())
                {
                    popMenu.add(itemImportData);
                }
            }
            else if (nodeObj instanceof ColumnInfoDTO)
            {
                ColumnInfoDTO column = (ColumnInfoDTO) nodeObj;
                if (column.getHelperInfo().getRelationOid() > column.getHelperInfo().getDatLastSysOid())
                {
                    popMenu.add(itemCreate);//popMenu.add(itemAddColumn);
                    if (!column.isInherit())
                    {
                        popMenu.add(itemDrop);
                        popMenu.add(itemDropCascade);
                    }
                    popMenu.add(separator1);
                }
                popMenu.add(itemCreateScript);
            } 
            else if (nodeObj instanceof IndexInfoDTO)
            {
                IndexInfoDTO index = (IndexInfoDTO) nodeObj;
                if (!index.isSystem())
                {
                    popMenu.add(itemCreate);//popMenu.add(itemAddIndex);
                    popMenu.add(itemDrop);
                    popMenu.add(itemDropCascade);
                    popMenu.add(separator1);
                }
                popMenu.add(itemCreateScript);
                popMenu.add(itemMaintainData);
                if (!index.isSystem())
                {
                    popMenu.add(itemRestore);
                }
            } else if (nodeObj instanceof RuleInfoDTO)
            {
                RuleInfoDTO rule = (RuleInfoDTO) nodeObj;
                if (!rule.isSystem())
                {
                    HelperInfoDTO helper = rule.getHelperInfo();
                    TreeController tc = TreeController.getInstance();
                    logger.info("type=" + helper.getType());
                    if (helper.getType().equals(TreeEnum.TreeNode.TABLE)
                            && tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 8, 2))
                    {
                        popMenu.remove(separator0);
                        cbitemEnableRule.setSelected(rule.isEnable());
                        popMenu.add(cbitemEnableRule);
                        popMenu.add(separator1);
                    }
                    popMenu.add(itemCreate);//popMenu.add(itemAddRule);
                    popMenu.add(itemDrop);//pro
                    popMenu.add(itemDropCascade);//pro
                    popMenu.add(separator2);
                }
                popMenu.add(itemCreateScript);
            } else if (nodeObj instanceof TriggerInfoDTO)
            {
                TriggerInfoDTO trigger = (TriggerInfoDTO) nodeObj;
                if (!trigger.isSystem())
                {
                    HelperInfoDTO helper = trigger.getHelperInfo();
                    TreeController tc = TreeController.getInstance();
                    if (helper.getType().equals(TreeEnum.TreeNode.TABLE)
                            && tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 8, 0))
                    {
                        popMenu.remove(separator0);
                        cbitemEnableTrigger.setSelected(trigger.isEnable());
                        popMenu.add(cbitemEnableTrigger);
                        popMenu.add(separator1);
                    }
                }
                popMenu.add(itemCreate);//popMenu.add(itemAddTrigger);
                if (!trigger.isSystem())
                {
                    popMenu.add(itemDrop);
                    popMenu.add(itemDropCascade);
                    popMenu.add(separator2);
                }
                popMenu.add(itemCreateScript);
            } else if (nodeObj instanceof ConstraintInfoDTO)
            {
                ConstraintInfoDTO constr = (ConstraintInfoDTO) nodeObj;
                if (constr.getOid() > constr.getHelperInfo().getDatLastSysOid())
                {
                    if (constr.getHelperInfo().getType().equals(TreeEnum.TreeNode.TABLE))
                    {
                        popMenu.remove(separator0);
                        cbitemEnableConstraint.setSelected(constr.isEnable());
                        popMenu.add(cbitemEnableConstraint);
                        popMenu.add(separator0);
                    }
                    TreeEnum.TreeNode type = constr.getType();
                    switch (type)
                    {
                        case PRIMARY_KEY://only allow one pk
                            break;
                        case FOREIGN_KEY:
                            popMenu.add(itemCreate);//popMenu.add(itemAddFK);
                            break;
                        case CHECK:
                            popMenu.add(itemCreate);//popMenu.add(itemAddCheck);
                            break;
                        case UNIQUE:
                            popMenu.add(itemCreate);//popMenu.add(itemAddUnique);
                            break;
                        case EXCLUDE:
                            popMenu.add(itemCreate);//popMenu.add(itemAddExclude);
                            break;
                    }
                    popMenu.add(itemDrop);
                    popMenu.add(itemDropCascade);
                    popMenu.add(separator1);
                }
                popMenu.add(itemCreateScript);                
            }
            if (nodeObj instanceof AbstractObject)
            {
                AbstractObject ao = (AbstractObject) nodeObj;
                /*if (nodeObj instanceof TablespaceInfoDTO)
                {
                    popMenu.add(new JPopupMenu.Separator());
                    popMenu.add(itemProperty);/*if (nodeObj instanceof TablespaceInfoDTO)
                {
                    popMenu.add(new JPopupMenu.Separator());
                    popMenu.add(itemProperty);
                } else*/ if (!ao.isSystem())
                {
                    if (nodeObj instanceof PartitionInfoDTO)
                    {
                        //refesh ,sep, remove
                        popMenu.add(itemDrop);
                    } else
                    {
                        popMenu.add(new JPopupMenu.Separator());
                        popMenu.add(itemProperty);
                    }
                }
            } else
            {
                popMenu.add(new JPopupMenu.Separator());
                popMenu.add(itemProperty);
            }
        }        
        popMenu.show(treeView, e.getX(), e.getY());
    }  
    //tree node press action
    private void treeNodePressAction(MouseEvent e)
    {
        DefaultMutableTreeNode pressedNode = (DefaultMutableTreeNode) treeView.getLastSelectedPathComponent();
        if (pressedNode == null)
        {
            return;
        }
        Object nodeObj = pressedNode.getUserObject();
        logger.info("PressNode = " + pressedNode.toString() + ", Class = " + nodeObj.getClass().toString());
        if (e.getButton() == 3)//popup right-click item
        {
            this.showTreeRightClickMenu(e);
            this.limitUserExecutePrivilege();
        } 
    }

    //handler for node changeable
    private void addTreeNodeChangeHandler()
    {
        eventBus.addHandler(TreeNodeChangeEvent.TYPE, new TreeNodeChangeEventHandler()
        {
            @Override
            public void onChanged(TreeNodeChangeEvent evt)
            {
                if (evt.getNodeObject() == null)
                {
                    return;
                }
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) evt.getNodeObject();
                populateObj(node);
                Object obj = node.getUserObject();
                if (obj == null)
                {
                    return;
                }
                populateWindowState(obj);
                populateMenuAndToolbarItem(obj);
                limitUserExecutePrivilege();
            }
        });
    } 
    private void populateObj(DefaultMutableTreeNode node)
    {
        this.node = node;
    }
    private void populateWindowState(Object obj)
    {
        String state = null;
        if (obj instanceof XMLServerInfoDTO)
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("servers"));
        } else if (obj instanceof DBInfoDTO
                || obj.toString().startsWith(constBundle.getString("databases")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("databases"));
        } else if (obj instanceof TablespaceInfoDTO
                || obj.toString().startsWith(constBundle.getString("tablespaces")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("tablespaces"));
        } else if (obj instanceof RoleInfoDTO)
        {
            RoleInfoDTO nodeObject = (RoleInfoDTO) obj;
            if (nodeObject.isCanLogin())
            {
                state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("loginRoles"));
            } else
            {
                state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("groupRoles"));
            }
        } else if (obj.toString().startsWith(constBundle.getString("loginRoles")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("loginRoles"));
        } else if (obj.toString().startsWith(constBundle.getString("groupRoles")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("groupRoles"));
        } else if (obj instanceof CatalogInfoDTO
                || obj.toString().startsWith(constBundle.getString("catalogs")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("catalogs"));
        } else if (obj instanceof CastInfoDTO
                || obj.toString().startsWith(constBundle.getString("casts")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("casts"));
        } else if (obj instanceof LanguageInfoDTO
                || obj.toString().startsWith(constBundle.getString("languages")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("languages"));
        } else if (obj instanceof SchemaInfoDTO
                || obj.toString().startsWith(constBundle.getString("schemas")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("schemas"));
        } else if (obj instanceof TableInfoDTO
                || obj.toString().startsWith(constBundle.getString("tables")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("tables"));
        } else if (obj instanceof ViewInfoDTO
                || obj.toString().startsWith(constBundle.getString("views")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("views"));
        } else if (obj instanceof SequenceInfoDTO
                || obj.toString().startsWith(constBundle.getString("sequences")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("sequences"));
        } else if (obj instanceof SequenceInfoDTO
                || obj.toString().startsWith(constBundle.getString("functions")))
        {
            state = MessageFormat.format(constBundle.getString("state"), constBundle.getString("functions"));
        }
        jtxfState.setText(state);
    }
    private void populateMenuAndToolbarItem(Object obj)
    {   
        logger.debug("Enter:node=" + node);
        //depend on current node   
        //edit
        miCreate.setEnabled(false);
        miDropCascade.setEnabled(false);
        miTruncateTable.setEnabled(false);
        miTruncateTableCascade.setEnabled(false);        
        miReassignDropOwnedTo.setEnabled(false);
        miMoveTablespace.setEnabled(false);
        miProperty.setEnabled(false);
        
        miRowCount.setEnabled(false);
        miRefresh.setEnabled(false);
        
        //tool
        miQuery.setEnabled(false);         
        miBackupAll.setEnabled(false);
        miBackup.setEnabled(false);
        miRestore.setEnabled(false);
        miMaintainData.setEnabled(false);
        miImportData.setEnabled(false);
        miExportData.setEnabled(false);
        
        menuScript.setEnabled(false);
        miCreateScript.setEnabled(false);
        miSelectScript.setEnabled(false);
        miInsertScript.setEnabled(false);
        miUpdateScript.setEnabled(false);
        miDeleteScript.setEnabled(false);
        menuViewData.setEnabled(false);
        
        cbmiEnableRule.setEnabled(false);
        cbmiEnableTrigger.setEnabled(false);
        miEnableAllTrigger.setEnabled(false);
        miDisableAllTrigger.setEnabled(false);
        
        miChangePwd.setEnabled(false);
        miConnServer.setEnabled(false);
        miDisconnServer.setEnabled(false);
        miStartServer.setEnabled(false);
        miStopServer.setEnabled(false);
        
        miReloadConfiguration.setEnabled(false);
        miAddNameRestorePoint.setEnabled(false);
        miPauseWALReplay.setEnabled(false);
        miResumeWALReplay.setEnabled(false);
        
        miServerConfig.setEnabled(false);
        miHba.setEnabled(false);
        miOpenPGHba.setEnabled(false);
        miPostgresql.setEnabled(false);
        miOpenPostgresql.setEnabled(false);        
        miServerState.setEnabled(false);
        
        //help
        miSuggestion.setEnabled(false);
        
        if (node.isRoot())
        {        
            miRefresh.setEnabled(true);
            miProperty.setEnabled(false);
            miCreate.setEnabled(false);
            miDrop.setEnabled(false);
        } else if (obj instanceof ObjGroupDTO)
        {
            miRefresh.setEnabled(false);
            miProperty.setEnabled(false);
            miDrop.setEnabled(false);
            ObjGroupDTO group = (ObjGroupDTO) obj;
            switch (group.getType())
            {
                case SERVER_GROUP:
                case CATALOG_GROUP:
                case LANGUAGE_GROUP:
                case CONSTRAINT_GROUP:
                case PARTITION_GROUP:
                    miCreate.setEnabled(false);
                    break;
                default:
                    //logger.debug("***********************" + group.getHelperInfo().getDatLastSysOid());
                    if (group.getHelperInfo() != null
                            && group.getHelperInfo().getSchema() != null && !group.getHelperInfo().getSchema().equals("public")
                            //&& group.getHelperInfo().getSchemaOid() < group.getHelperInfo().getDatLastSysOid())
                            && group.getHelperInfo().getSchema().equals("pg_catalog"))
                    {
                        miCreate.setEnabled(false);
                    } else
                    {
                        miCreate.setEnabled(true);
                    }
                    break;
            }       
        } 
        else if (obj instanceof XMLServerInfoDTO)
        {
            miProperty.setEnabled(true);
            miCreate.setEnabled(true);
            miDrop.setEnabled(true);
            XMLServerInfoDTO server = (XMLServerInfoDTO) obj;
            miRefresh.setEnabled(server.isConnected());
            miBackupAll.setEnabled(server.isConnected());
            miStartServer.setEnabled(true);
            miStopServer.setEnabled(true);
            miChangePwd.setEnabled(server.isConnected());
            if (!server.isConnected())
            {
                miConnServer.setEnabled(true);
                miDisconnServer.setEnabled(false); 
            } else
            {                
                miConnServer.setEnabled(false);
                miDisconnServer.setEnabled(true);
                miReloadConfiguration.setEnabled(true);
                miServerState.setEnabled(true);
                miHba.setEnabled(true);
                miOpenPGHba.setEnabled(true);
                miPostgresql.setEnabled(true);
                miOpenPostgresql.setEnabled(true);
                TreeController tc = TreeController.getInstance();
                HelperInfoDTO helper = tc.getHelperInfoFromServerInfo(server);
                if (tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 0))
                {
                    miAddNameRestorePoint.setEnabled(true);
                    if (server.isInRecovery() != null && server.isInRecovery().equals("true"))
                    {
                        miPauseWALReplay.setEnabled(true);
                        miResumeWALReplay.setEnabled(true);
                    }
                }
                if (tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 3))
                {
                    miServerConfig.setEnabled(true);
                }
            }          
        } else
        {
            miRefresh.setEnabled(true);
            miProperty.setEnabled(true);
            miCreate.setEnabled(true);
            miDrop.setEnabled(true);
            miDropCascade.setEnabled(true);
            
            //for script
            menuScript.setEnabled(true);
            miCreateScript.setEnabled(!(obj instanceof ViewColumnInfoDTO));
            if (obj instanceof TableInfoDTO)
            {
                miSelectScript.setEnabled(true);
                miInsertScript.setEnabled(true);
                miUpdateScript.setEnabled(true);
                miDeleteScript.setEnabled(true);
            } else if (obj instanceof ViewInfoDTO)
            {
                miSelectScript.setEnabled(true);
            }
            
            //for other menu item
            if (obj instanceof DBInfoDTO)
            {
                miDropCascade.setEnabled(true);
                miQuery.setEnabled(true);
                miMaintainData.setEnabled(true);
                miBackup.setEnabled(true);
                miRestore.setEnabled(true);
                miServerState.setEnabled(true);
                miHba.setEnabled(true);
                miOpenPGHba.setEnabled(true);
                miPostgresql.setEnabled(true);
                miOpenPostgresql.setEnabled(true);
            } else if (obj instanceof TablespaceInfoDTO)
            {
                miDropCascade.setEnabled(false);
                miQuery.setEnabled(false);
                TablespaceInfoDTO tablespace = (TablespaceInfoDTO) obj;
                HelperInfoDTO helper = tablespace.getHelperInfo();
                TreeController tc = TreeController.getInstance();
                if (tc.isVersionHigherThan(helper.getDbSys(), helper.getVersionNumber(), 9, 3))
                {
                     miMoveTablespace.setEnabled(true);
                }
            } else if (obj instanceof RoleInfoDTO)
            {
                miDropCascade.setEnabled(false);
                miQuery.setEnabled(false);
                RoleInfoDTO role = (RoleInfoDTO) obj;
                if (role.isCanLogin())
                {
                    miReassignDropOwnedTo.setEnabled(true);
                }
            } else if ((obj instanceof CatalogInfoDTO)
                    || (obj instanceof LanguageInfoDTO))
            {
                miCreate.setEnabled(false);
                miDrop.setEnabled(false);
                miDropCascade.setEnabled(false);
                miProperty.setEnabled(false);
            } else if (obj instanceof SchemaInfoDTO)
            {
                miBackup.setEnabled(true);
                miRestore.setEnabled(true);
            } else if (obj instanceof TableInfoDTO)
            {
                TableInfoDTO table = (TableInfoDTO) obj;
                if (table.isSystem())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);

                    miRowCount.setEnabled(true);
                    menuViewData.setEnabled(true);
                    miExportData.setEnabled(true);
                    miBackup.setEnabled(true);
                    miMaintainData.setEnabled(true);
                } else
                {
                    miTruncateTable.setEnabled(true);
                    miTruncateTableCascade.setEnabled(true);
                    miRowCount.setEnabled(true);
                    menuViewData.setEnabled(true);
                    miMaintainData.setEnabled(true);
                    miExportData.setEnabled(true);
                    miImportData.setEnabled(true);
                    miBackup.setEnabled(true);
                    miRestore.setEnabled(true);
                    miEnableAllTrigger.setEnabled(true);
                    miDisableAllTrigger.setEnabled(true);
                    TableInfoDTO t = (TableInfoDTO) obj;
                    if (t.getPrimaryKey() == null || t.getPrimaryKey().isEmpty())
                    {
                        miSuggestion.setEnabled(true);
                    }
                }
            } else if (obj instanceof ViewInfoDTO)
            {
                ViewInfoDTO view = (ViewInfoDTO) obj;
                if (view.isSystem())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);

                    menuViewData.setEnabled(true);
                } else
                {
                    menuViewData.setEnabled(true);
                    miSuggestion.setEnabled(true);
                }
            } else if (obj instanceof SequenceInfoDTO)
            {
                miDropCascade.setEnabled(true);
            } else if (obj instanceof FunctionInfoDTO)
            {
                FunctionInfoDTO fun = (FunctionInfoDTO) obj;
                if (fun.isSystem())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                } 
            } else if (obj instanceof ViewColumnInfoDTO)
            {
                miCreate.setEnabled(false);
                miDrop.setEnabled(false);
                miDropCascade.setEnabled(false);
                miProperty.setEnabled(false);
                
            } else if(obj instanceof ColumnInfoDTO)
            {
                ColumnInfoDTO column = (ColumnInfoDTO) obj;
                if (column.getHelperInfo().getRelationOid() < column.getHelperInfo().getDatLastSysOid())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                }
                if (column.isInherit())
                {
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                }
            } else if (obj instanceof ConstraintInfoDTO)
            {
                ConstraintInfoDTO constr = (ConstraintInfoDTO) obj;
                if (constr.getHelperInfo().getRelationOid() < constr.getHelperInfo().getDatLastSysOid())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                }
            } else if (obj instanceof IndexInfoDTO)
            {
                IndexInfoDTO index = (IndexInfoDTO) obj;
                if (index.isSystem())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                }
                miMaintainData.setEnabled(true);
                miRestore.setEnabled(!index.isSystem());
            } else if (obj instanceof RuleInfoDTO)
            {
                RuleInfoDTO rule = (RuleInfoDTO) obj;
                if (rule.isSystem())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                } else
                {
                    HelperInfoDTO help = rule.getHelperInfo();
                    TreeController tc = TreeController.getInstance();
                    logger.info("type=" + help.getType());
                    if (help.getType().equals(TreeEnum.TreeNode.TABLE)
                            && tc.isVersionHigherThan(help.getDbSys(), help.getVersionNumber(), 8, 2))
                    {
                        cbmiEnableRule.setEnabled(true);
                        cbmiEnableRule.setSelected(rule.isEnable());
                    }
                }
            } else if (obj instanceof TriggerInfoDTO)
            {
                TriggerInfoDTO trigger = (TriggerInfoDTO) obj;
                if (trigger.isSystem())
                {
                    miCreate.setEnabled(false);
                    miDrop.setEnabled(false);
                    miDropCascade.setEnabled(false);
                } else
                {
                    HelperInfoDTO help = trigger.getHelperInfo();
                    TreeController tc = TreeController.getInstance();
                    if (help.getType().equals(TreeEnum.TreeNode.TABLE)
                            && tc.isVersionHigherThan(help.getDbSys(), help.getVersionNumber(), 8, 0))
                    {
                        cbmiEnableTrigger.setEnabled(true);
                        cbmiEnableTrigger.setSelected(trigger.isEnable());
                    }
                }
            } else if (obj instanceof PartitionInfoDTO)
            {
                miProperty.setEnabled(false);
                miCreate.setEnabled(false);
                miDrop.setEnabled(true);
                miDropCascade.setEnabled(false);
                menuViewData.setEnabled(true);
            }
        }       
        //for button the same as above item
        btnRefresh.setEnabled(miRefresh.isEnabled());
        btnProperty.setEnabled(miProperty.isEnabled());
        btnCreate.setEnabled(miCreate.isEnabled());
        btnDrop.setEnabled(miDrop.isEnabled());
        btnQuery.setEnabled(miQuery.isEnabled());
        btnDataView.setEnabled(menuViewData.isEnabled());
        btnFilterDataView.setEnabled(menuViewData.isEnabled());
        btnMaintainData.setEnabled(miMaintainData.isEnabled());
        btnExportData.setEnabled(miExportData.isEnabled());
        btnImportData.setEnabled(miImportData.isEnabled());
        btnShowObjSuggestion.setEnabled(miSuggestion.isEnabled());
    }
    private void populateCopyItemForSql(MouseEvent e)
    {
        String sql = sqlView.getSelectedText();
        if (sql == null || sql.isEmpty())
        {
            miCopy.setEnabled(false);
        } else
        {
            miCopy.setEnabled(true);
        }
    }
  
    //register acition for object(tree item, menu, and toolbar item)
    private void registerAllAction()
    {
         ActionListener handleObjActionListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                handleObjMenuActionPerformed(evt);
            }
        };
         //for menu and toolbar item   
        miRefresh.addActionListener(handleObjActionListener);
        btnRefresh.addActionListener(handleObjActionListener);        
        miDropCascade.addActionListener(handleObjActionListener);
        miDrop.addActionListener(handleObjActionListener);
        btnDrop.addActionListener(handleObjActionListener);
        miCreate.addActionListener(handleObjActionListener);
        btnCreate.addActionListener(handleObjActionListener);
        miProperty.addActionListener(handleObjActionListener);
        btnProperty.addActionListener(handleObjActionListener);

        miTruncateTable.addActionListener(handleObjActionListener);
        miTruncateTableCascade.addActionListener(handleObjActionListener);//support only version>=8.2
      
        miCreateScript.addActionListener(handleObjActionListener);
        miSelectScript.addActionListener(handleObjActionListener);
        miInsertScript.addActionListener(handleObjActionListener);
        miUpdateScript.addActionListener(handleObjActionListener);
        miDeleteScript.addActionListener(handleObjActionListener);
        
        miReassignDropOwnedTo.addActionListener(handleObjActionListener);
        miMoveTablespace.addActionListener(handleObjActionListener);
        
        cbmiEnableRule.addActionListener(handleObjActionListener);
        cbmiEnableTrigger.addActionListener(handleObjActionListener);
        miDisableAllTrigger.addActionListener(handleObjActionListener);
        miEnableAllTrigger.addActionListener(handleObjActionListener);
             
        btnExportData.addActionListener(handleObjActionListener);
        miExportData.addActionListener(handleObjActionListener);
        btnImportData.addActionListener(handleObjActionListener);
        miImportData.addActionListener(handleObjActionListener);
        
        miRowCount.addActionListener(handleObjActionListener);
        
        miBackup.addActionListener(handleObjActionListener);
        miBackupAll.addActionListener(handleObjActionListener);
        miRestore.addActionListener(handleObjActionListener);        
       
        //for server
        miConnServer.addActionListener(handleObjActionListener);
        miDisconnServer.addActionListener(handleObjActionListener);
        
        miStartServer.addActionListener(handleObjActionListener);
        miStopServer.addActionListener(handleObjActionListener);
        
        miServerState.addActionListener(handleObjActionListener); 
        miReloadConfiguration.addActionListener(handleObjActionListener);        
        miAddNameRestorePoint.addActionListener(handleObjActionListener);
        miPauseWALReplay.addActionListener(handleObjActionListener);
        miResumeWALReplay.addActionListener(handleObjActionListener);
        
        miServerConfig.addActionListener(handleObjActionListener);
        miHba.addActionListener(handleObjActionListener);
        miOpenPGHba.addActionListener(handleObjActionListener);
        miPostgresql.addActionListener(handleObjActionListener);
        miOpenPostgresql.addActionListener(handleObjActionListener);
        miOpenCatalog.addActionListener(new ActionListener()//only edit file
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                miOpenPgpassActionPerformed(e);
            }
        });
        
        //register tree right-click item listener
        itemCreate.addActionListener(handleObjActionListener);
        itemRefresh.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0));        
        itemRefresh.addActionListener(handleObjActionListener);
        itemProperty.addActionListener(handleObjActionListener);
        itemDrop.addActionListener(handleObjActionListener);
        itemDropCascade.addActionListener(handleObjActionListener);
        //for server
        itemConnServer.addActionListener(handleObjActionListener);
        itemDisconnServer.addActionListener(handleObjActionListener);
        itemAddServer.addActionListener(handleObjActionListener);
        //for tablespace
        itemAddTablespace.addActionListener(handleObjActionListener);
        itemMoveTablespace.addActionListener(handleObjActionListener);
        //for login role
        itemReassignDropOwnedTo.addActionListener(handleObjActionListener);
        //for db
        itemAddDB.addActionListener(handleObjActionListener);
        itemAddLoginRole.addActionListener(handleObjActionListener);
        itemAddGroupRole.addActionListener(handleObjActionListener);
        itemAddEventTrigger.addActionListener(handleObjActionListener);
        itemAddSchema.addActionListener(handleObjActionListener);
        itemAddSequence.addActionListener(handleObjActionListener);
        itemAddTable.addActionListener(handleObjActionListener);        
        itemAddView.addActionListener(handleObjActionListener);
        itemAddFunction.addActionListener(handleObjActionListener);
        itemAddProcedure.addActionListener(handleObjActionListener);
        //for table
        itemTruncateTable.addActionListener(handleObjActionListener);
        itemTruncateTableCascade.addActionListener(handleObjActionListener);
        itemRowCount.addActionListener(handleObjActionListener);
        itemExportData.addActionListener(handleObjActionListener);
        itemImportData.addActionListener(handleObjActionListener);
        //for table or view
        itemAddColumn.addActionListener(handleObjActionListener);
        itemAddPK.addActionListener(handleObjActionListener);
        itemAddFK.addActionListener(handleObjActionListener);
        itemAddExclude.addActionListener(handleObjActionListener);
        itemAddCheck.addActionListener(handleObjActionListener);
        itemAddUnique.addActionListener(handleObjActionListener);
        itemAddIndex.addActionListener(handleObjActionListener);
        itemAddRule.addActionListener(handleObjActionListener);
        itemAddTrigger.addActionListener(handleObjActionListener);

        itemCreateScript.addActionListener(handleObjActionListener);
        itemSelectScript.addActionListener(handleObjActionListener);
        itemInsertScript.addActionListener(handleObjActionListener);
        itemDeleteScript.addActionListener(handleObjActionListener);
        itemUpdateScript.addActionListener(handleObjActionListener);        

        itemMaintainData.addActionListener(handleObjActionListener);
        itemBackup.addActionListener(handleObjActionListener);
        itemBackupAll.addActionListener(handleObjActionListener);
        itemRestore.addActionListener(handleObjActionListener);
        
        cbitemEnableConstraint.addActionListener(handleObjActionListener);//gc
        cbitemEnableRule.addActionListener(handleObjActionListener);
        cbitemEnableTrigger.addActionListener(handleObjActionListener);
        itemEnableAllTrigger.addActionListener(handleObjActionListener);
        itemDisableAllTrigger.addActionListener(handleObjActionListener);
        
        itemReloadConfiguration.addActionListener(handleObjActionListener);
        itemAddNameRestorePoint.addActionListener(handleObjActionListener);
        itemPauseWALReplay.addActionListener(handleObjActionListener);
        itemResumeWALReplay.addActionListener(handleObjActionListener);
        
        
         //FILE
        miSaveDefine.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miSaveDefineActionPerformed(evt);
            }
        });        
        ActionListener serverAddAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miAddServerActionPerformed(evt);
            }
        };
        miAddServer.addActionListener(serverAddAction);
        miOption.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miOptionActionPerformed(evt);
            }
        });
        ActionListener changepwd = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miChangePwdActionPerformed(evt);
            }
        };
        miChangePwd.addActionListener(changepwd);
        itemChangePwd.addActionListener(changepwd);
        miExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miExitActionPerformed(evt);
            }
        });
        //EDIT
        UndoableEditListener undoableEditListener = new UndoableEditListener()
        {
            @Override
            public void undoableEditHappened(UndoableEditEvent evt)
            {
                //logger.info("undoableEditHappened");
                undoManager = new UndoManager();
                undoManager.addEdit(evt.getEdit());                
            }
        };
        sqlView.getDocument().addUndoableEditListener(undoableEditListener);
        miCopy.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                logger.info("click copy");
                sqlView.copy();
            }
        });
        //PLUG
        ActionListener consoleAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miConsoleActionPerformed(evt);
            }
        };
        miConsole.addActionListener(consoleAction);
        //VIEW
        cbmiObjBrowser.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                cbmiObjBrowserActionPerformed(evt);
            }
        });
        cbmiSqlWindow.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                cbmiSqlWindowActionPerformed(evt);
            }
        });
        cbmiToolbar.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                cbmiToolbarActionPerformed(evt);
            }
        });
        miDefView.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miDefViewActionPerformed(evt);
            }
        });
        
        //TOOL
        ActionListener auditAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miAuditActionPerformed(evt);
            }
        };
        miAudit.addActionListener(auditAction);
        ActionListener migrateAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miMigrationAssistantActionPerformed(evt);
            }
        };
        miMigrationAssistant.addActionListener(migrateAction);
        ActionListener queryAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miQueryActionPerformed(evt);
            }
        };
        miQuery.addActionListener(queryAction);
        ActionListener allDataAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miViewDataActionPerformed(evt);
            }
        };
        miDataViewAll.addActionListener(allDataAction);
        ActionListener filterDataAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miFilterDataActionPerformed(evt);
            }
        };
        miDataFilter.addActionListener(filterDataAction);        
        ActionListener maintainAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miMaintainActionPerformed(evt);
            }
        };
        miMaintainData.addActionListener(maintainAction);       
        //HELP
        ActionListener helpAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                miHelpActionPerformed(evt);
            }
        };
        miHgdbAdminHelp.addActionListener(helpAction);
        miSuggestion.addActionListener(helpAction);
        miFAQ.addActionListener(helpAction);
        miBugReport.addActionListener(helpAction);
        miHgdbHelp.addActionListener(helpAction);
        miAbout.addActionListener(helpAction);
        
        //toolbar action perform
        btnAddServer.addActionListener(serverAddAction);      
        btnQuery.addActionListener(queryAction);
        btnDataView.addActionListener(allDataAction);
        btnFilterDataView.addActionListener(filterDataAction);
        btnMaintainData.addActionListener(maintainAction);        
        btnAudit.addActionListener(auditAction);
        btnMigrationAssistant.addActionListener(migrateAction);
        btnExecuteLastPlugs.addActionListener(consoleAction);
        btnShowObjSuggestion.addActionListener(helpAction);
        btnShowSqlHelp.addActionListener(helpAction);
    }
    
     //EDIT
//    private void miCopyActionPerformed(UndoableEditEvent evt)
//    {
//        logger.info("execute copy");
//        UndoManager undoManager = new UndoManager();
//        undoManager.addEdit(evt.getEdit());
//    }   
    private void scriptItemAction(Object item, Object obj)
    {
        if (!(obj instanceof AbstractObject))
        {
            return;
        }
        AbstractObject aobj = (AbstractObject) obj;
        TreeController tc = TreeController.getInstance();
        String script = "";
        if ((item == miCreateScript || item == itemCreateScript)
                && !(obj instanceof ViewColumnInfoDTO))
        {
            script = tc.getDefinitionSQL(aobj);
        } else if ((item == miSelectScript || item == itemSelectScript)
                && (obj instanceof TableInfoDTO || obj instanceof ViewInfoDTO))
        {
            try
            {
                script = tc.getSelectScript(aobj);
            } catch (ClassNotFoundException | SQLException ex)
            {
                jtxfState.setText(jtxfState.getText() + constBundle.getString("failed"));
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (obj instanceof TableInfoDTO)
        {
            TableInfoDTO tabInfo = (TableInfoDTO) obj;
            if (item == miInsertScript || item == itemInsertScript)
            {
                script = tc.getInsertScript(tabInfo);
            } else if (item == miUpdateScript || item == itemUpdateScript)
            {
                script = tc.getUpdateScript(tabInfo);
            } else if (item == miDeleteScript || item == itemDeleteScript)
            {
                script = tc.getDeleteScript(tabInfo);
            }
        }
        QueryMainView qmv = new QueryMainView(aobj.getHelperInfo(), script);
        qmv.setLocationRelativeTo(this);
        qmv.setVisible(true);
    }
    public void handleObjMenuActionPerformed(ActionEvent evt)
    {
        logger.debug("Node=" + node);
        if(node==null)
        {
            return;
        }
        Object obj = node.getUserObject();       
        
        Object item = evt.getSource();
        logger.debug("Item=" + (item==null? "null": item));
        if (item == miRefresh || item == btnRefresh || item ==itemRefresh)
        {
            treeView.refreshItemAction(node);
            return;
        } else if (item == miDrop || item == miDropCascade || item == btnDrop
                || item == itemDrop || item == itemDropCascade)
        {
            treeView.dropItemAction(node, (item == miDropCascade || item==itemDropCascade));
            return;
        } else if (item == miCreateScript || item == itemCreateScript
                || item == miInsertScript || item == itemInsertScript
                || item == miUpdateScript || item == itemUpdateScript
                || item == miDeleteScript || item == itemDeleteScript
                || item == miSelectScript || item == itemSelectScript)
        {
            this.scriptItemAction(item, obj);
            return;
        }       
        
        if (obj instanceof XMLServerInfoDTO)
        {
            XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) obj;
            serverNodeActionPerformed(item, serverInfo);
        } else if (obj instanceof DBInfoDTO)
        {
            DBInfoDTO dbInfo = (DBInfoDTO) obj;
            dbNodeActionPerformed(item, dbInfo);
        } else if (obj instanceof TablespaceInfoDTO)
        {
            TablespaceInfoDTO tablespaceInfo = (TablespaceInfoDTO) obj;
            tablespaceNodeActionPerformed(item, tablespaceInfo);
        } else if (obj instanceof RoleInfoDTO)
        {
            RoleInfoDTO roleInfo = (RoleInfoDTO) obj;
            roleNodeActionPerformed(item, roleInfo);
        } else if (obj instanceof EventTriggerInfoDTO)
        {
            EventTriggerInfoDTO info = (EventTriggerInfoDTO) obj;
            EventTriggerNodeActionPerformed(item, info);
        }
        else if (obj instanceof SchemaInfoDTO)
        {
            SchemaInfoDTO schemaInfo = (SchemaInfoDTO) obj;
            schemaNodeActionPerformed(item, schemaInfo);
        } else if (obj instanceof SequenceInfoDTO)
        {
            SequenceInfoDTO sequenceInfo = (SequenceInfoDTO) obj;
            sequenceNodeActionPerformed(item, sequenceInfo);
        } else if (obj instanceof ViewInfoDTO)
        {
            ViewInfoDTO viewInfo = (ViewInfoDTO) obj;
            viewNodeActionPerformed(item, viewInfo);
        } else if (obj instanceof TableInfoDTO)
        {
            TableInfoDTO tableInfo = (TableInfoDTO) obj;
            tableNodeActionPerformed(item, tableInfo);
        } else if (obj instanceof RuleInfoDTO)
        {
            RuleInfoDTO ruleInfo = (RuleInfoDTO) obj;
            ruleNodeActionPerformed(item, ruleInfo);
        } else if (obj instanceof IndexInfoDTO)
        {
            IndexInfoDTO indexInfo = (IndexInfoDTO) obj;
            indexNodeActionPerformed(item, indexInfo);
        } else if (obj instanceof TriggerInfoDTO)
        {
            TriggerInfoDTO triggerInfo = (TriggerInfoDTO) obj;
            triggerNodeActionPerformed(item, triggerInfo);
        } else if (obj instanceof ColumnInfoDTO)
        {
            ColumnInfoDTO columnInfo = (ColumnInfoDTO) obj;
            columnNodeActionPerformed(item, columnInfo);
        } else if (obj instanceof ConstraintInfoDTO)
        {
            ConstraintInfoDTO constraintInfo = (ConstraintInfoDTO) obj;
            constraintNodeActionPerformed(item, constraintInfo);
        } else if (obj instanceof ObjGroupDTO)
        {
            ObjGroupDTO groupInfo = (ObjGroupDTO) obj;
            groupNodeActionPerformed(item, groupInfo);
        } else if (obj instanceof FunctionInfoDTO)
        {
            funcProcNodeActionPerformed(item, (FunctionInfoDTO) obj);
        }
    }
    private void serverNodeActionPerformed(Object item, XMLServerInfoDTO serverInfo)
    {
        DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
        if (item == miConnServer
                || item ==  itemConnServer)
        {
            treeView.serverClickedAction(node);
        } else if (item == miDisconnServer
                || item == itemDisconnServer)
        {
            serverInfo.setConnected(false);
            node.removeAllChildren();
            model.reload();
            TreePath tp = new TreePath(node.getPath());
            treeView.scrollPathToVisible(tp);
            treeView.setSelectionPath(tp);
        } 
        else if (item == miAddServer || item == miCreate || item == btnCreate
                || item==itemAddServer)
        {
            ServerAddView sav = new ServerAddView(this, true, treeView.getServerGroup());
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if (sav.isSuccess())
            {
                XMLServerInfoDTO addServerInfo = sav.getNewServerInfo();
                DefaultMutableTreeNode objGroup = (DefaultMutableTreeNode) node.getParent();
                ObjGroupDTO objGroupInfo = (ObjGroupDTO) objGroup.getUserObject();
                objGroupInfo.getServerInfoList().add(addServerInfo);

                DefaultMutableTreeNode serverNode = new DefaultMutableTreeNode(addServerInfo, true);
                model.insertNodeInto(serverNode, objGroup, objGroup.getChildCount());   
                treeView.scrollPathToVisible(new TreePath(serverNode.getPath()));
                treeView.setSelectionPath(new TreePath(serverNode.getPath()));               
                //open server branch
                addServerInfo.setConnected(true);
                treeView.serverClickedAction(serverNode);
            }
        }       
        else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            ServerAddView sav = new ServerAddView(this, true, treeView.getServerGroup(), serverInfo);
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if(sav.isSuccess())
            {
                treeView.scrollPathToVisible(new TreePath(node.getPath()));
                treeView.setSelectionPath(new TreePath(node.getPath()));//select add node
                treeView.serverClickedAction(node);
            }
        } else if (item == miBackupAll
                || item == itemBackupAll)
        {
            BackupAllView bav = new BackupAllView(this, true, serverInfo);
            bav.setLocationRelativeTo(this);
            bav.setVisible(true);
        }  else if (item == miStartServer || item == miStopServer)
        {
            if (serverInfo.isConnected())
            {
                serverInfo.setConnected(false);
                node.removeAllChildren();
                model.reload();
                TreePath tp = new TreePath(node.getPath());
                treeView.scrollPathToVisible(tp);
                treeView.setSelectionPath(tp);
            }
            boolean isStart = (item == miStartServer);
            ServerControlDialog scd = new ServerControlDialog(this,  true, serverInfo, isStart);
            scd.setLocationRelativeTo(this);
            scd.setVisible(true);
        } else if (item == miReloadConfiguration
                || item == itemReloadConfiguration)
        {
            jtxfState.setText(constBundle.getString("configureReloading"));
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.reloadConfiguration(serverInfo);
                jtxfState.setText(jtxfState.getText() + constBundle.getString("success"));
            } catch (ClassNotFoundException | SQLException ex)
            {
                jtxfState.setText(jtxfState.getText() + constBundle.getString("failed"));
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (item == miAddNameRestorePoint
                || item == itemAddNameRestorePoint)
        {
            String resp = JOptionPane.showInputDialog(this, constBundle.getString("inputNewNameRestorePointName"),
                    constBundle.getString("addNameRestorePoint"), JOptionPane.OK_CANCEL_OPTION);
            logger.info("inputContent="+resp);
            if(resp==null)//cancle
            {
                return;
            }
            else if (resp.isEmpty())
            {
                JOptionPane.showMessageDialog(this, constBundle.getString("nameRestorePointNameIsEmpty"));
            } else
            {
                try
                {
                    jtxfState.setText(constBundle.getString("addNameRestorePoint"));
                    TreeController.getInstance().addNameRestorePoint(serverInfo, resp);
                    jtxfState.setText(jtxfState.getText() + "..." + constBundle.getString("success"));
                } catch (Exception ex)
                {
                    jtxfState.setText(jtxfState.getText() + "..." + constBundle.getString("failed"));
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (item == miPauseWALReplay
                || item == itemPauseWALReplay)
        {
            try
            {
                jtxfState.setText(constBundle.getString("pauseWALReplay"));
                TreeController.getInstance().pauseWALReplay(serverInfo);
                jtxfState.setText(jtxfState.getText() + "..." + constBundle.getString("success"));
            } catch (Exception ex)
            {
                jtxfState.setText(jtxfState.getText() + "..." + constBundle.getString("failed"));
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (item == miResumeWALReplay
                || item == itemResumeWALReplay)
        {
            try
            {
                jtxfState.setText(constBundle.getString("resumeWALReplay"));
                TreeController.getInstance().resumeWALReplay(serverInfo);
                jtxfState.setText(jtxfState.getText() + "..." + constBundle.getString("success"));
            } catch (Exception ex)
            {
                jtxfState.setText(jtxfState.getText() + "..." + constBundle.getString("failed"));
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (item == miServerState)
        {
            TreeController tc = TreeController.getInstance();
            HelperInfoDTO helperInfo = tc.getHelperInfoFromServerInfo(serverInfo);
            ServerStatusView ssv = new ServerStatusView(helperInfo);
            ssv.setLocationRelativeTo(this);
            ssv.setVisible(true);
        } else if (item == miServerConfig)
        {
            TreeController tc = TreeController.getInstance();
            HelperInfoDTO helperInfo = tc.getHelperInfoFromServerInfo(serverInfo);
            ServerConfigView scv = new ServerConfigView(helperInfo);
            scv.setLocationRelativeTo(this);
            scv.setVisible(true);
        } else if (item == miHba || item == miOpenPGHba)
        {
            TreeController tc = TreeController.getInstance();
            HelperInfoDTO helperInfo = tc.getHelperInfoFromServerInfo(serverInfo);
            ConfigMainView hbaView = new ConfigMainView(helperInfo,"pg_hba.conf");
            hbaView.setLocationRelativeTo(this);
            hbaView.setVisible(true);
            if (item == miOpenPGHba)
            {
                hbaView.openHbaFileAction();
            } else if (item == miHba)
            {
                ConfigController cc = ConfigController.getInstance();
                try
                {
                    String path = cc.getPath(helperInfo,"pg_hba.conf");
                    if (path != null)
                    {
                        hbaView.getHbaContent(path);
                    }
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }else if(item==miPostgresql || item == miOpenPostgresql)
        {
            TreeController tc = TreeController.getInstance();
            HelperInfoDTO helperInfo = tc.getHelperInfoFromServerInfo(serverInfo);
            ConfigMainView postgresqlView = new ConfigMainView(helperInfo,"postgresql.conf");
            postgresqlView.setLocationRelativeTo(this);
            postgresqlView.setVisible(true);
            if (item == miOpenPostgresql)
            {
                postgresqlView.openPostgresqlFileAction();
            } else if (item == miPostgresql)
            {
                ConfigController cc = ConfigController.getInstance();
                try
                {
                    String path = cc.getPath(helperInfo,"postgresql.conf");
                    if (path != null)
                    {
                        postgresqlView.getPostgresqlContent(path);
                    }
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        } 
        /*
        else if (item == miDrop || item == btnDrop)
        {
            String type = constBundle.getString("servers");
            DropConfirmDialog dropConfirm = new DropConfirmDialog();
            int response = dropConfirm.showDialog(this, type, serverInfo.toString(), false);
            if (response == JOptionPane.YES_OPTION)
            {
                TreeController tc = TreeController.getInstance();
                tc.deleteServerFromXML(serverInfo);

                DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
                ObjGroupDTO parentNode = (ObjGroupDTO) groupNode.getUserObject();
                for (XMLServerInfoDTO server : parentNode.getServerInfoList())
                {
                    if (server.equals(serverInfo))
                    {
                        parentNode.getServerInfoList().remove(server);
                        break;
                    }
                }
                model.removeNodeFromParent(node);
                treeView.scrollPathToVisible(new TreePath(node.getPath()));
                treeView.setSelectionPath(new TreePath(groupNode.getPath()));
            }
        }
        */
    }
    private void tablespaceNodeActionPerformed(Object item,TablespaceInfoDTO tablespaceInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();

        if (item == miCreate || item == btnCreate
                || item == itemCreate)//itemAddTablespace
        {
            TablespaceView tav = new TablespaceView(this, true, tablespaceInfo.getHelperInfo());
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSucccess())
            {
                this.addNewObjNode(tav.getTablespaceInfo(), groupNode);
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            TablespaceView tsav = new TablespaceView(this, true, tablespaceInfo);
            tsav.setLocationRelativeTo(this);
            tsav.setVisible(true);
            if (tsav.isSucccess())
            {
                node.setUserObject(tsav.getTablespaceInfo());
                //eventBus.fireEvent(new TreeNodeChangeEvent(node));
                treeView.refreshItemAction(node);                
            }
        }
        else if (item == miMoveTablespace
                || item == itemMoveTablespace)
        {
            MoveTablespaceDialog mtdlg = new MoveTablespaceDialog(this,  tablespaceInfo);
            mtdlg.setLocationRelativeTo(this);
            mtdlg.setVisible(true);
        } 
    }
    private void roleNodeActionPerformed(Object item, RoleInfoDTO roleInfo)
    {        
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        if (item == miCreate || item == btnCreate
                ||item == itemCreate) // itemAddLoginRole ||item == itemAddGroupRole  )
        {
            RoleView rav = new RoleView(this, true, roleInfo.getHelperInfo(), roleInfo.isCanLogin()); 
            rav.setLocationRelativeTo(this);
            rav.setVisible(true);
            if (rav.isSucccess())
            {
                this.addNewObjNode(rav.getRoleInfo(),  groupNode);
            }
        }  
        else if(item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            RoleView rav = new RoleView(this, true, roleInfo); 
            rav.setLocationRelativeTo(this);
            rav.setVisible(true);
            if(rav.isSucccess())
            {
                if(rav.changCurrentUserPwd())
                {
                    logger.debug("when change pwd of login user disconnect its server");                    
                    DefaultMutableTreeNode serverNode = (DefaultMutableTreeNode) groupNode.getParent();
                    XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) serverNode.getUserObject();
                    serverInfo.setConnected(false);
                    serverNode.removeAllChildren();
                    DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
                    model.reload();
                    TreePath tp = new TreePath(serverNode.getPath());
                    treeView.scrollPathToVisible(tp);
                    treeView.setSelectionPath(tp);
                }else
                {
                    node.setUserObject(rav.getRoleInfo());
                    //eventBus.fireEvent(new TreeNodeChangeEvent(node));   
                    treeView.refreshItemAction(node);
                }
            }
        } 
        else if(item == miReassignDropOwnedTo
                ||item==itemReassignDropOwnedTo)
        {
            ReassignDropOwnedToDialog rd = new ReassignDropOwnedToDialog(this,true,roleInfo);
            rd.setLocationRelativeTo(this);
            rd.setVisible(true);
        }
    }  
    //db has branch
    private void dbNodeActionPerformed(Object item, DBInfoDTO dbInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();  
        if (item == miCreate || item == btnCreate
                || item == itemAddDB)
        {
            ObjGroupDTO group = (ObjGroupDTO) groupNode.getUserObject();
            HelperInfoDTO helperInfo = group.getHelperInfo();
            
            DBView dbv = new DBView(this, true, helperInfo);
            dbv.setLocationRelativeTo(this);
            dbv.setVisible(true);
            if (dbv.isSucccess())
            {
                DBInfoDTO db = dbv.getDBInfo();
                //db.setName(db.getName().replaceAll("\"\"", "\""));
                this.addNewObjNode(db,  groupNode);
            }
        } 
        else if (item == miProperty || item == btnProperty
                ||item==itemProperty)
        {
            DBView dbav = new DBView(this, true, dbInfo);
            dbav.setLocationRelativeTo(this);
            dbav.setVisible(true);
            if (dbav.isSucccess())
            {
                DBInfoDTO newdb = dbav.getDBInfo();
                //newdb.setName(newdb.getName().replaceAll("\"\"", "\""));
                newdb.getHelperInfo().setDbName(newdb.getName());
                if (newdb.getTablespace() != null && !newdb.getTablespace().isEmpty())
                {
                    newdb.getHelperInfo().setDbTablespace(newdb.getTablespace());
                }
                node.setUserObject(newdb);
                treeView.refreshItemAction(node);//its branch will be effected
            }
        }
        else if (item == itemAddSchema)
        {
            SchemaView sav = new SchemaView(this, true, dbInfo.getHelperInfo());
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if (sav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.dbClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode schemaGroupNode = (DefaultMutableTreeNode) node.getChildAt(2);
                    SchemaInfoDTO newSchema = sav.getSchemaInfo();
                    this.addNewObjNode(newSchema, schemaGroupNode);
                }
            }
        }else if (item == miMaintainData
                ||item == itemMaintainData)
        {
            MaintanceView maintainView = new MaintanceView(null, true, dbInfo);
            maintainView.setLocationRelativeTo(this);
            maintainView.setVisible(true);
        } else if (item == miBackup
                || item == itemBackup)
        {
            BackupView bv = new BackupView(this, true, dbInfo);
            bv.setLocationRelativeTo(this);
            bv.setVisible(true);
        } else if (item == miRestore
                || item == itemRestore)
        {
            RestoreView rv = new RestoreView(this, true, dbInfo);
            rv.setLocationRelativeTo(this);
            rv.setVisible(true);
        } else if (item == miServerState) 
        {
            ServerStatusView ssv = new ServerStatusView(dbInfo.getHelperInfo());
            ssv.setLocationRelativeTo(this);
            ssv.setVisible(true);
        } else if (item == miOpenPGHba || item == miHba) 
        {
            ConfigMainView hbaView = new ConfigMainView(dbInfo.getHelperInfo(),"pg_hba.conf");
            hbaView.setLocationRelativeTo(this);
            hbaView.setVisible(true);
            if (item == miOpenPGHba)
            {
                hbaView.openHbaFileAction();
            } else if (item == miHba)
            {
                ConfigController cc = ConfigController.getInstance();
                try
                {
                    String path = cc.getPath(dbInfo.getHelperInfo(),"pg_hba.conf");
                    if (path != null)
                    {
                        hbaView.getHbaContent(path);
                    }
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }else if (item == miOpenPostgresql || item == miPostgresql)
        {
            ConfigMainView confView = new ConfigMainView(dbInfo.getHelperInfo(),"postgresql.conf");
            confView.setLocationRelativeTo(this);
            confView.setVisible(true);
            if (item == miOpenPostgresql)
            {
                confView.openPostgresqlFileAction();
            } else if (item == miPostgresql)
            {
                ConfigController cc = ConfigController.getInstance();
                try
                {
                    String path = cc.getPath(dbInfo.getHelperInfo(), "postgresql.conf");
                    if (path != null)
                    {
                        confView.getPostgresqlContent(path);
                    }
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    private void EventTriggerNodeActionPerformed(Object item, EventTriggerInfoDTO info)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        if (item == miCreate || item == btnCreate
                || item == itemCreate)//itemAddEventTrigger)
        {
            HelperInfoDTO helperInfo = info.getHelperInfo();
            EventTriggerView sav = new EventTriggerView(this, true, helperInfo);
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if (sav.isSuccess())
            {
                this.addNewObjNode(sav.getEventTriggerInfo(), groupNode);
            }
        } 
        else if (item == miProperty || item == btnProperty
                ||item == itemProperty)
        {
            EventTriggerView sav = new EventTriggerView(this, true, info);
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if(sav.isSuccess())
            {
                node.setUserObject(sav.getEventTriggerInfo());
                treeView.refreshItemAction(node);//its branch will be effected
            }
        }
    }
    //schema has branch
    private void schemaNodeActionPerformed(Object item, SchemaInfoDTO schemaInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        if (item == miCreate || item == btnCreate
                || item == itemAddSchema)
        {
            HelperInfoDTO helperInfo = schemaInfo.getHelperInfo();
            SchemaView sav = new SchemaView(this, true, helperInfo);
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if (sav.isSuccess())
            {
                this.addNewObjNode(sav.getSchemaInfo(), groupNode);
            }
        } 
        else if (item == miProperty || item == btnProperty
                ||item == itemProperty)
        {
            SchemaView sav = new SchemaView(this, true, schemaInfo);
            sav.setLocationRelativeTo(this);
            sav.setVisible(true);
            if(sav.isSuccess())
            {
                node.setUserObject(sav.getSchemaInfo());
                treeView.refreshItemAction(node);//its branch will be effected
            }
        }
        else if (item==itemAddTable)
        {
            TableView tav = new TableView(this, true, schemaInfo.getHelperInfo());
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.schemaClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode tableGroupNode = (DefaultMutableTreeNode) node.getChildAt(0);
                    TableInfoDTO newTab = tav.getTableInfo();
                    this.addNewObjNode(newTab, tableGroupNode);
                }
            }
        }
        else if (item == itemAddView)
        {
            ViewView vav = new ViewView(this, true, schemaInfo.getHelperInfo());
            vav.setLocationRelativeTo(this);
            vav.setVisible(true);
            if (vav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.schemaClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode viewGroupNode = (DefaultMutableTreeNode) node.getChildAt(1);
                    ViewInfoDTO newView = vav.getViewInfo();
                    this.addNewObjNode(newView, viewGroupNode);
                }
            }
        } else if (item == itemAddSequence)
        {
            SequenceView sqav = new SequenceView(this, true, schemaInfo.getHelperInfo());
            sqav.setLocationRelativeTo(this);
            sqav.setVisible(true);
            if (sqav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.schemaClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode seqGroupNode = (DefaultMutableTreeNode) node.getChildAt(2);
                    SequenceInfoDTO newSeq = sqav.getSequenceInfo();
                    this.addNewObjNode(newSeq, seqGroupNode);
                }
            }
        }
        else if (item == itemAddFunction || item == itemAddProcedure)
        {
            boolean isfunc = itemAddFunction == item;
            FunctionView fav = new FunctionView(this, true,
                    isfunc ? TreeEnum.TreeNode.FUNCTION : TreeEnum.TreeNode.PROCEDURE,
                     schemaInfo.getHelperInfo());
            fav.setLocationRelativeTo(this);
            fav.setVisible(true);
            if (fav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.schemaClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode funGroupNode = (DefaultMutableTreeNode) 
                            node.getChildAt(isfunc ? 3 : 4);
                    FunctionInfoDTO newFun = fav.getFunctionInfo();
                    this.addNewObjNode(newFun, funGroupNode);
                }
            }
        }
         else if (item == miBackup
                || item == itemBackup)
        {
            BackupView bv = new BackupView(this, true, schemaInfo);
            bv.setLocationRelativeTo(this);
            bv.setVisible(true);
        } else if (item == miRestore
                || item == itemRestore)
        {
            RestoreView rv = new RestoreView(this, true, schemaInfo);
            rv.setLocationRelativeTo(this);
            rv.setVisible(true);
        }         
    }
    private void funcProcNodeActionPerformed(Object item, FunctionInfoDTO fun)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();        
        if (item == miCreate || item == btnCreate
                || item == itemCreate)//itemAddFunction || item == itemAddProcedure)
        {
            FunctionView fav = new FunctionView(this, true, fun.getType(), fun.getHelperInfo());
            fav.setLocationRelativeTo(this);
            fav.setVisible(true);
            if (fav.isSuccess())
            {
                this.addNewObjNode(fav.getFunctionInfo(), groupNode);
            }
        }
        else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            FunctionView fav = new FunctionView(this, true, fun);
            fav.setLocationRelativeTo(this);
            fav.setVisible(true);
            if(fav.isSuccess())
            {
                node.setUserObject(fav.getFunctionInfo());
                treeView.refreshItemAction(node);
            }
        } 
    }
    private void sequenceNodeActionPerformed(Object item, SequenceInfoDTO sequenceInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        
        if (item == miCreate || item == btnCreate
                || item == itemCreate) //itemAddSequence)
        {
            SequenceView sqav = new SequenceView(this, true, sequenceInfo.getHelperInfo());
            sqav.setLocationRelativeTo(this);
            sqav.setVisible(true);
            if (sqav.isSuccess())
            {
                this.addNewObjNode(sqav.getSequenceInfo(),  groupNode);
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            SequenceView sqav = new SequenceView(this, true, sequenceInfo);
            sqav.setLocationRelativeTo(this);
            sqav.setVisible(true);
            if(sqav.isSuccess())
            {
                node.setUserObject(sqav.getSequenceInfo());
                treeView.refreshItemAction(node);
            }
        }
    }
    //view has branch
    private void viewNodeActionPerformed(Object item, ViewInfoDTO viewInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();          
//        HelperInfoDTO newHelper = new HelperInfoDTO();
//        newHelper.setType(TreeEnum.TreeNode.VIEW);
//        newHelper.setRelation(viewInfo.getName());
//        newHelper.setRelationOid(viewInfo.getOid());
        if (item == miCreate || item == btnCreate || item == itemAddView)
        {
            ViewView vav = new ViewView(this, true, viewInfo.getHelperInfo());
            vav.setLocationRelativeTo(this);
            vav.setVisible(true);
            if (vav.isSuccess())
            {
                this.addNewObjNode(vav.getViewInfo(), groupNode);
            }
        } else if (item == miProperty || item == btnProperty || item == itemProperty)
        {
            ViewView vav = new ViewView(this, true, viewInfo);
            vav.setLocationRelativeTo(this);
            vav.setVisible(true);
            if(vav.isSuccess())
            {
                node.setUserObject(vav.getViewInfo());
                treeView.refreshItemAction(node);
            }
        } else if (item == itemAddRule)
        {
            RuleView rav = new RuleView(this, true, viewInfo.getHelperInfo());
            rav.setLocationRelativeTo(this);
            rav.setVisible(true);
            if (rav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.viewClickedAction(node);
                    treeView.refreshItemAction(node);
                } else
                {
                    DefaultMutableTreeNode ruleGroupNode = (DefaultMutableTreeNode) node.getChildAt(1);
                    RuleInfoDTO newRuleInfo = rav.getRuleInfo();
                    this.addNewObjNode(newRuleInfo, ruleGroupNode);
                }
            }
        } else if (item == itemAddTrigger)
        {
            //TreeController tc = TreeController.getInstance();
            //try
            //{
                //ColItemInfoDTO[] columns = tc.getColumnOfRelation(viewInfo.getHelperInfo(), viewInfo.getOid());
                TriggerView tav = new TriggerView(this, true, viewInfo.getHelperInfo());
                tav.setLocationRelativeTo(this);
                tav.setVisible(true);
                if (tav.isSuccess())
                {
                    if (node.isLeaf())
                    {
                        treeView.viewClickedAction(node);
                    } else
                    {
                        DefaultMutableTreeNode triggerGroupNode = (DefaultMutableTreeNode) node.getChildAt(2);
                        TriggerInfoDTO newTriggerInfo = tav.getTriggerInfo();
                        this.addNewObjNode(newTriggerInfo, triggerGroupNode);
                    }
                }
//            } catch (Exception ex)
//            {
//                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
//            }
        }       
    }
    //table has branch
    private void tableNodeActionPerformed(Object item, TableInfoDTO tableInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();    
//        HelperInfoDTO newHelper = new HelperInfoDTO();
//        newHelper.setType(TreeEnum.TreeNode.TABLE);
//        newHelper.setRelation(tableInfo.getName());
//        newHelper.setRelationOid(tableInfo.getOid());

        if (item == miCreate || item == btnCreate
                || item == itemAddTable)
        {
            TableView tav = new TableView(this, true, tableInfo.getHelperInfo());//newHelper
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSuccess())
            {
                this.addNewObjNode(tav.getTableInfo(), groupNode);
            }
        }
        else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            TableView tav = new TableView(this, true, tableInfo);//newHelper,
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSuccess())
            {
                node.setUserObject(tav.getTableInfo());
                treeView.refreshItemAction(node);
            }
        }
        else if (item == itemAddColumn)
        {
            ColumnView cav = new ColumnView(this, true, tableInfo.getHelperInfo(), tableInfo.getOwner());//newHelper
            cav.setLocationRelativeTo(this);
            cav.setVisible(true);
            if (cav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.refreshItemAction(node);
                    //treeView.tableClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode columnGroupNode = (DefaultMutableTreeNode) node.getChildAt(0);
                    this.addNewObjNode(cav.getColumnInfo(), columnGroupNode);
                    treeView.refreshItemAction(node);
                }
            }
        } else if (item == itemAddPK
                || item == itemAddFK
                || item == itemAddExclude
                || item == itemAddCheck
                || item == itemAddUnique
                )
        {                    
            TreeController tc = TreeController.getInstance();
            try
            {                
                ColItemInfoDTO[] columns = tc.getColumnOfRelation(tableInfo.getHelperInfo(),tableInfo.getOid());//newHelper
                TreeEnum.TreeNode type = this.getConstraintTypeByItem(item);
                ConstraintView cav = new ConstraintView(null, true, type, columns, tableInfo.getHelperInfo(), false);//newHelper
                cav.setLocationRelativeTo(this);
                cav.setVisible(true);
                if (cav.isSuccess())
                {
                    if (node.isLeaf())
                    {
                        treeView.refreshItemAction(node);
                        //treeView.tableClickedAction(node);
                    } else
                    {
                        DefaultMutableTreeNode constrGroupNode = (DefaultMutableTreeNode) node.getChildAt(1);
                        this.addNewObjNode(cav.getConstrInfo(), constrGroupNode);
                        treeView.refreshItemAction(node);
                    }
                }
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (item == itemAddIndex)
        {
            IndexView iav = new IndexView(null, true, tableInfo.getHelperInfo());//newHelper
            iav.setLocationRelativeTo(this);
            iav.setVisible(true);
            if (iav.isSuccess())
            {
                if(node.isLeaf())
                {
                    treeView.tableClickedAction(node);
                }
                else
                {
                    DefaultMutableTreeNode indexGroupNode = (DefaultMutableTreeNode) node.getChildAt(2);
                    this.addNewObjNode(iav.getIndexInfo(), indexGroupNode);
                }
            }
        } else if (item == itemAddRule)
        {
            RuleView rav = new RuleView(null, true, tableInfo.getHelperInfo());//newHelper
            rav.setLocationRelativeTo(this);
            rav.setVisible(true);
            if (rav.isSuccess())
            {
                if (node.isLeaf())
                {
                    treeView.tableClickedAction(node);
                } else
                {
                    DefaultMutableTreeNode ruleGroupNode = (DefaultMutableTreeNode) node.getChildAt(3);
                    this.addNewObjNode(rav.getRuleInfo(), ruleGroupNode);
                }
            }
        } else if (item == itemAddTrigger)
        {
            TreeController tc = TreeController.getInstance();
//            try
//            {
//                ColItemInfoDTO[] columns = tc.getColumnOfRelation(tableInfo.getHelperInfo(), tableInfo.getOid());//newHelper
                TriggerView tav = new TriggerView(null, true, tableInfo.getHelperInfo());//newHelper
                tav.setLocationRelativeTo(this);
                tav.setVisible(true);
                if (tav.isSuccess())
                {
                    if (node.isLeaf())
                    {
                        treeView.tableClickedAction(node);
                    } else
                    {
                         DefaultMutableTreeNode triggerGroupNode = (DefaultMutableTreeNode) node.getChildAt(4);
                         this.addNewObjNode(tav.getTriggerInfo(), triggerGroupNode);
                    }
                }
//            } catch (Exception ex)
//            {
//                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
//            }
        }
        else if(item == miEnableAllTrigger
                || item == itemEnableAllTrigger)
        {
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.enableAllTrigger(tableInfo, true);
                treeView.refreshItemAction(node);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }  else if(item == miDisableAllTrigger
                || item == itemDisableAllTrigger)
        {
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.enableAllTrigger(tableInfo, false);
                treeView.refreshItemAction(node);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
        else if(item == miMaintainData
                || item == itemMaintainData)
        {
            MaintanceView maintainView = new MaintanceView(null, true, tableInfo);
            maintainView.setLocationRelativeTo(this);
            maintainView.setVisible(true);
        } 
        else if(item == miBackup
                || item == itemBackup)
        {
            BackupView bv = new BackupView(this, true, tableInfo);
            bv.setLocationRelativeTo(this);
            bv.setVisible(true);
        } else if (item == miRestore
                || item == itemRestore)
        {
            RestoreView rv = new RestoreView(this, true, tableInfo);
            rv.setLocationRelativeTo(this);
            rv.setVisible(true);
        } else if (item == miRowCount
                || item == itemRowCount)
        {
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.countTableRow(tableInfo);
                eventBus.fireEvent(new TreeNodeChangeEvent(node));
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (item == miTruncateTable
                || item == itemTruncateTable)
        {
            int response = JOptionPane.showConfirmDialog(this, 
                    MessageFormat.format(constBundle.getString("isTruncateTable"),tableInfo.getName()),
                    constBundle.getString("truncateTable"), JOptionPane.YES_NO_OPTION);
            if (response == JOptionPane.YES_OPTION)
            {
                TreeController tc = TreeController.getInstance();
                try
                {
                    tc.truncateTable(tableInfo, false);
                     eventBus.fireEvent(new TreeNodeChangeEvent(node));
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(),
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (item == miTruncateTableCascade
                || item == itemTruncateTableCascade)
        {
            int response = JOptionPane.showConfirmDialog(this, 
                    MessageFormat.format(constBundle.getString("isTruncateTableCascade"),tableInfo.getName()),
                    constBundle.getString("truncateTableCascade"), JOptionPane.YES_NO_OPTION);
            if (response == JOptionPane.YES_OPTION)
            {
                TreeController tc = TreeController.getInstance();
                try
                {
                    tc.truncateTable(tableInfo, true);
                    eventBus.fireEvent(new TreeNodeChangeEvent(node));
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(),
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (item == miExportData || item == btnExportData
                || item ==itemExportData )
        {
            DataExportView dev = new DataExportView(tableInfo);
            dev.setLocationRelativeTo(this);
            dev.setVisible(true);
        } else if (item == miImportData || item == btnImportData
               || item == itemImportData)
        {
            DataImportView dev = new DataImportView(tableInfo);
            dev.setLocationRelativeTo(this);
            dev.setVisible(true);
        }        
    }
    private void ruleNodeActionPerformed(Object item, RuleInfoDTO ruleInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        if (item == miCreate || item == btnCreate || item == itemCreate)//temAddRule)
        {
            RuleView rav = new RuleView(this, true, ruleInfo.getHelperInfo());
            rav.setLocationRelativeTo(this);
            rav.setVisible(true);
            if (rav.isSuccess())
            {
                this.addNewObjNode(rav.getRuleInfo(), groupNode);
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            RuleView rav = new RuleView(this, true, ruleInfo);
            rav.setLocationRelativeTo(this);
            rav.setVisible(true);
            if(rav.isSuccess())
            {
                node.setUserObject(rav.getRuleInfo());
                treeView.refreshItemAction(node);
            }
        }
        else if(item == cbmiEnableRule
                || item == cbitemEnableRule)
        {
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.enableTheRule(ruleInfo,!ruleInfo.isEnable());
                eventBus.fireEvent(new TreeNodeChangeEvent(node));
            } catch (ClassNotFoundException | SQLException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }      
    }
    private void indexNodeActionPerformed(Object item, IndexInfoDTO indexInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        HelperInfoDTO helperInfo = indexInfo.getHelperInfo();//group.getHelperInfo();
        if (item == miCreate || item == btnCreate || item == itemCreate)//itemAddIndex)
        {
            IndexView iav = new IndexView(this, true, helperInfo);
            iav.setLocationRelativeTo(this);
            iav.setVisible(true);
            if (iav.isSuccess())
            {
                this.addNewObjNode(iav.getIndexInfo(), groupNode);
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            IndexView iav = new IndexView(this, true, indexInfo);
            iav.setLocationRelativeTo(this);
            iav.setVisible(true);
            if (iav.isSuccess())
            {
                node.setUserObject(iav.getIndexInfo());
                treeView.refreshItemAction(node);
            }
        }
        else if(item == miMaintainData
                || item == itemMaintainData)
        {
            MaintanceView maintainView = new MaintanceView(this, true, indexInfo);
            maintainView.setLocationRelativeTo(this);
            maintainView.setVisible(true);
        }else if (item == miRestore
                || item == itemRestore)
        {
            RestoreView rv = new RestoreView(this, true, indexInfo);
            rv.setLocationRelativeTo(this);
            rv.setVisible(true);
        }
    }
    private void triggerNodeActionPerformed(Object item, TriggerInfoDTO triggerInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        HelperInfoDTO helperInfo = triggerInfo.getHelperInfo();//group.getHelperInfo();
        if (item == miCreate || item == btnCreate || item == itemCreate)//itemAddTrigger)
        {
            TriggerView tav = new TriggerView(this, true, helperInfo);
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSuccess())
            {
                this.addNewObjNode(tav.getTriggerInfo(), groupNode);
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            TriggerView tav = new TriggerView(this, true, triggerInfo);
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSuccess())
            {
                node.setUserObject(tav.getTriggerInfo());
                treeView.refreshItemAction(node);
            }
        } 
        else if(item == cbmiEnableTrigger
                || item == cbitemEnableTrigger)
        {
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.enableTheTrigger(triggerInfo, !triggerInfo.isEnable());
                eventBus.fireEvent(new TreeNodeChangeEvent(node));
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }      
    }
    private void columnNodeActionPerformed(Object item, ColumnInfoDTO columnInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        DefaultMutableTreeNode tableNode = (DefaultMutableTreeNode) groupNode.getParent();
        TableInfoDTO table = (TableInfoDTO) tableNode.getUserObject();
        HelperInfoDTO helperInfo = columnInfo.getHelperInfo();//group.getHelperInfo();
        if (item == miCreate || item == btnCreate || item == itemCreate)//itemAddColumn)
        {            
            ColumnView cav = new ColumnView(this, true, helperInfo, table.getOwner());
            cav.setLocationRelativeTo(this);
            cav.setVisible(true);
            if (cav.isSuccess())
            {
                this.addNewObjNode(cav.getColumnInfo(), groupNode);
                treeView.refreshItemAction((DefaultMutableTreeNode) groupNode.getParent());                
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            ColumnView cav = new ColumnView(this, true, columnInfo, table.getOwner());
            cav.setLocationRelativeTo(this);
            cav.setVisible(true);
            if (cav.isSuccess())
            {
                node.setUserObject(cav.getColumnInfo());
                treeView.refreshItemAction((DefaultMutableTreeNode) groupNode.getParent());
            }
        }
    }
    private void constraintNodeActionPerformed(Object item, ConstraintInfoDTO constraintInfo)
    {
        DefaultMutableTreeNode groupNode = (DefaultMutableTreeNode) node.getParent();
        HelperInfoDTO helperInfo = constraintInfo.getHelperInfo();        
        TreeEnum.TreeNode enumType = constraintInfo.getType();
        if (item == miCreate || item == btnCreate
                || item == itemCreate) 
                //|| item == itemAddPK 
                //|| item == itemAddFK 
                //|| item == itemAddUnique
                //|| item == itemAddCheck
                //|| item == itemAddExclude) 
        {
            try
            {
                ColItemInfoDTO[] columns = TreeController.getInstance().getColumnOfRelation(helperInfo,  helperInfo.getRelationOid());
                ConstraintView cav = new ConstraintView(this, true, enumType, columns, helperInfo, false);
                cav.setLocationRelativeTo(this);
                cav.setVisible(true);
                if (cav.isSuccess())
                {
                    this.addNewObjNode(cav.getConstrInfo(), groupNode);
                    treeView.refreshItemAction((DefaultMutableTreeNode) groupNode.getParent());
                }
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (item == miProperty || item == btnProperty
                || item == itemProperty)
        {
            ConstraintView tav = new ConstraintView(this, true, enumType, helperInfo, constraintInfo);
            tav.setLocationRelativeTo(this);
            tav.setVisible(true);
            if (tav.isSuccess())
            {
                this.addNewObjNode(tav.getConstrInfo(), groupNode);
                treeView.refreshItemAction((DefaultMutableTreeNode) groupNode.getParent());
            }
        }else if(item == cbitemEnableConstraint)
        {
            try
            {
                TreeController.getInstance().enableTheContraint(constraintInfo,!constraintInfo.isEnable());
                eventBus.fireEvent(new TreeNodeChangeEvent(node));
            } catch (ClassNotFoundException | SQLException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private TreeEnum.TreeNode getConstraintTypeByItem(Object item) throws Exception
    {
        TreeEnum.TreeNode enumType;
        if (item == itemAddPK)
        {
            enumType = TreeEnum.TreeNode.PRIMARY_KEY;
        } else if (item == itemAddFK)
        {
            enumType = TreeEnum.TreeNode.FOREIGN_KEY;
        } else if (item == itemAddExclude)
        {
            enumType = TreeEnum.TreeNode.EXCLUDE;
        } else if (item == itemAddCheck)
        {
            enumType = TreeEnum.TreeNode.CHECK;
        } else if (item == itemAddUnique)
        {
            enumType = TreeEnum.TreeNode.UNIQUE;
        } else
        {
            enumType = null;
            String error = item.toString() + " is an Exception item, do nothing and return null.";
            logger.error(error);
            throw new Exception(error);
        }
        return enumType;
    }
    private void addNewObjNode(AbstractObject obj, DefaultMutableTreeNode parentNode)
    {
        logger.info("Enter:newNode=" + obj +", parentNode=" + parentNode);
        
        DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
        ObjGroupDTO group = (ObjGroupDTO) parentNode.getUserObject();
        List<ObjInfoDTO> objInfoList = group.getObjInfoList();
        
        DefaultMutableTreeNode newNode;
        if (obj instanceof FunctionInfoDTO)
        {
            FunctionInfoDTO newFun = (FunctionInfoDTO) obj;
            FunObjInfoDTO funObjInfo = new FunObjInfoDTO();
            funObjInfo.setHelperInfo(obj.getHelperInfo());
            funObjInfo.setType(obj.getType());
            funObjInfo.setName(newFun.getSimpleName());
            funObjInfo.setInArgTypes(newFun.getInArgTypes());
            funObjInfo.setInArgTypeOids(newFun.getInArgTypeOids());
            if (newFun.getOwner() == null || newFun.getOwner().isEmpty())
            {
                funObjInfo.setOwner(newFun.getHelperInfo().getUser());
            } else
            {
                funObjInfo.setOwner(newFun.getOwner());
            }
            funObjInfo.setComment(newFun.getComment());
            objInfoList.add(funObjInfo);
            newNode = new DefaultMutableTreeNode(funObjInfo, true);
        } else
        {
            ObjInfoDTO objInfo = new ObjInfoDTO();
            objInfo.setHelperInfo(obj.getHelperInfo());
            objInfo.setType(obj.getType());
            objInfo.setName(obj.getName());            
            if (obj instanceof RoleInfoDTO
                    || obj instanceof ViewColumnInfoDTO
                    || obj instanceof ColumnInfoDTO
                    || obj instanceof TriggerInfoDTO
                    || obj instanceof RuleInfoDTO
                    || obj instanceof IndexInfoDTO)
            {
                objInfo.setOwner("");
            } else
            {
                if (obj.getOwner() == null || obj.getOwner().isEmpty())
                {
                    objInfo.setOwner(obj.getHelperInfo().getUser());
                } else
                {
                    objInfo.setOwner(obj.getOwner());
                }
            }
            objInfo.setComment(obj.getComment());
            objInfoList.add(objInfo);
            newNode = new DefaultMutableTreeNode(objInfo, true);
        }
        model.insertNodeInto(newNode, parentNode, parentNode.getChildCount());
        treeView.scrollPathToVisible(new TreePath(newNode.getPath()));
        treeView.setSelectionPath(new TreePath(newNode.getPath()));
        treeView.refreshItemAction(newNode);
    }
    private void groupNodeActionPerformed(Object item, ObjGroupDTO groupInfo)
    {
        HelperInfoDTO helperInfo = groupInfo.getHelperInfo();
        TreeEnum.TreeNode type = groupInfo.getType();
        if (TreeEnum.TreeNode.CONSTRAINT_GROUP == type) 
        {
            if (item == itemAddPK
                    || item == itemAddFK
                    || item == itemAddUnique
                    || item == itemAddCheck
                    || item == itemAddExclude) {

                try {
                    DefaultMutableTreeNode parentTableNode = (DefaultMutableTreeNode) node.getParent();
                    ColItemInfoDTO[] columns = TreeController.getInstance().getColumnOfRelation(helperInfo, helperInfo.getRelationOid());
                    TreeEnum.TreeNode enumType = this.getConstraintTypeByItem(item);
                    ConstraintView cstv = new ConstraintView(this, true, enumType, columns, helperInfo, false);
                    cstv.setLocationRelativeTo(this);
                    cstv.setVisible(true);
                    if (cstv.isSuccess()) {
                        this.addNewObjNode(cstv.getConstrInfo(), node);
                        treeView.refreshItemAction(parentTableNode);
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(this, ex.getMessage(),
                             constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (TreeEnum.TreeNode.SERVER_GROUP == type) 
        {
            if (item == btnAddServer || item == itemAddServer)
            {
                ServerAddView sav = new ServerAddView(this, true, treeView.getServerGroup());
                sav.setLocationRelativeTo(this);
                sav.setVisible(true);
                if (sav.isSuccess()) {
                    XMLServerInfoDTO newServer = sav.getNewServerInfo();

                    DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) node;
                    ObjGroupDTO objGroupInfo = (ObjGroupDTO) parentNode.getUserObject();
                    objGroupInfo.getServerInfoList().add(newServer);

                    DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
                    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(newServer, true);
                    model.insertNodeInto(newNode, parentNode, parentNode.getChildCount());
                    treeView.scrollPathToVisible(new TreePath(newNode.getPath()));
                    treeView.setSelectionPath(new TreePath(newNode.getPath()));
                    //open server branch
                    newServer.setConnected(true);
                    treeView.serverClickedAction(newNode);
                }
            }
        }
        else if (item == miCreate || item == btnCreate || item == itemCreate) 
        {
            switch (type) 
            {
                case TABLESPACE_GROUP:
                    TablespaceView tv = new TablespaceView(this, true, helperInfo);
                    tv.setLocationRelativeTo(this);
                    tv.setVisible(true);
                    if (tv.isSucccess()) {
                        this.addNewObjNode(tv.getTablespaceInfo(), node);
                    }
                    break;
                case GROUPROLE_GROUP:
                case LOGINROLE_GROUP:
                    logger.info(groupInfo.getType());
                    RoleView rav = new RoleView(this, true, helperInfo,
                             groupInfo.getType() == TreeEnum.TreeNode.LOGINROLE_GROUP);
                    rav.setLocationRelativeTo(this);
                    rav.setVisible(true);
                    if (rav.isSucccess()) {
                        this.addNewObjNode(rav.getRoleInfo(), node);
                    }
                    break;
                case DATABASE_GROUP:
                    DBView dbv = new DBView(this, true, helperInfo);
                    dbv.setLocationRelativeTo(this);
                    dbv.setVisible(true);
                    if (dbv.isSucccess()) {
                        DBInfoDTO db = dbv.getDBInfo();
                        //db.setName(db.getName().replaceAll("\"\"", "\""));
                        this.addNewObjNode(db, node);
                    }
                    break;
                case EVENT_TRIGGER_GROUP:
                    EventTriggerView view = new EventTriggerView(this, true, helperInfo);
                    view.setLocationRelativeTo(this);
                    view.setVisible(true);
                    if (view.isSuccess()) {
                        this.addNewObjNode(view.getEventTriggerInfo(), node);
                    }
                    break;
                case SCHEMA_GROUP:
                    SchemaView sav = new SchemaView(this, true, helperInfo);
                    sav.setLocationRelativeTo(this);
                    sav.setVisible(true);
                    if (sav.isSuccess()) {
                        this.addNewObjNode(sav.getSchemaInfo(), node);
                    }
                    break;
                case TABLE_GROUP:
                    TableView tav = new TableView(this, true, helperInfo);
                    tav.setLocationRelativeTo(this);
                    tav.setVisible(true);
                    if (tav.isSuccess()) {
                        this.addNewObjNode(tav.getTableInfo(), node);
                    }
                    break;
                case VIEW_GROUP:
                    ViewView vav = new ViewView(this, true, helperInfo);
                    vav.setLocationRelativeTo(this);
                    vav.setVisible(true);
                    if (vav.isSuccess()) {
                        this.addNewObjNode(vav.getViewInfo(), node);
                    }
                    break;
                case SEQUENCE_GROUP:
                    SequenceView sqav = new SequenceView(this, true, helperInfo);
                    sqav.setLocationRelativeTo(this);
                    sqav.setVisible(true);
                    if (sqav.isSuccess()) {
                        this.addNewObjNode(sqav.getSequenceInfo(), node);
                    }
                    break;
                case FUNCTION_GROUP:
                case PROCEDURE_GROUP:
                    FunctionView fav = new FunctionView(this, true,
                            TreeEnum.TreeNode.FUNCTION_GROUP == type ? TreeEnum.TreeNode.FUNCTION
                                    : TreeEnum.TreeNode.PROCEDURE, helperInfo);
                    fav.setLocationRelativeTo(this);
                    fav.setVisible(true);
                    if (fav.isSuccess()) {
                        this.addNewObjNode(fav.getFunctionInfo(), node);
                    }
                    break;
                case COLUMN_GROUP:
                    DefaultMutableTreeNode tableNode = (DefaultMutableTreeNode) node.getParent();
                    TableInfoDTO table = (TableInfoDTO) tableNode.getUserObject();
                    ColumnView cav = new ColumnView(this, true, helperInfo, table.getOwner());
                    cav.setLocationRelativeTo(this);
                    cav.setVisible(true);
                    if (cav.isSuccess()) {
                        this.addNewObjNode(cav.getColumnInfo(), node);
                        treeView.refreshItemAction(tableNode);
                    }
                    break;
                case INDEX_GROUP:
                    IndexView iav = new IndexView(this, true, helperInfo);
                    iav.setLocationRelativeTo(this);
                    iav.setVisible(true);
                    if (iav.isSuccess()) {
                        this.addNewObjNode(iav.getIndexInfo(), node);
                    }
                    break;
                case RULE_GROUP:
                    RuleView rulv = new RuleView(this, true, helperInfo);
                    rulv.setLocationRelativeTo(this);
                    rulv.setVisible(true);
                    if (rulv.isSuccess()) {
                        this.addNewObjNode(rulv.getRuleInfo(), node);
                    }
                    break;
                case TRIGGER_GROUP:
                    TriggerView trgv = new TriggerView(this, true, helperInfo);
                    trgv.setLocationRelativeTo(this);
                    trgv.setVisible(true);
                    if (trgv.isSuccess()) {
                        this.addNewObjNode(trgv.getTriggerInfo(), node);
                    }
                    break;
                default:
                    String error = type + " is an exception group type, do othing and return.";
                    logger.error(error);
                    JOptionPane.showMessageDialog(this, error,
                             constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                    break;
            }
        } else 
        {
            String error = "item " + item.toString() + " for type" + type + " is exception execution, do othing and return.";
            logger.error(error);
            JOptionPane.showMessageDialog(this, error,
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    

    //FILE [finish]
    private void miSaveDefineActionPerformed(ActionEvent evt)
    {
        String sql = sqlView.getText();
        if (sql == null || sql.isEmpty())
        {
            JOptionPane.showMessageDialog(this, constBundle.getString("nothingToSaveMsg"),
                    constBundle.getString("warning"), JOptionPane.WARNING_MESSAGE);
            return;
        }
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle(constBundle.getString("selectOutputFile"));
        fc.setDialogType(JFileChooser.SAVE_DIALOG);
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fc.setApproveButtonText(constBundle.getString("saveDefine"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter("SQL Script(*.sql)", "sql");
        fc.setFileFilter(filter);
        fc.setVisible(true);
        int resp = fc.showOpenDialog(this);
        String path = null;
        if (resp == JFileChooser.APPROVE_OPTION)
        {
            path = fc.getSelectedFile().toString();
            logger.info("filePath=" + path);
            File file = new File(path);
            TreeController tc = TreeController.getInstance();
            String name = fc.getSelectedFile().getName();
            logger.info("fielname=" + name);
            if (!name.endsWith(".sql"))
            {
                path = path + ".sql";
                file = new File(path);
            }
            logger.info("isExit=" + file.exists());
            if (file.exists())
            {
                int option = JOptionPane.showConfirmDialog(this,
                        MessageFormat.format(constBundle.getString("isReplaceExistFile"), name),
                        constBundle.getString("isSave"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (option == JOptionPane.NO_OPTION)
                {
                    return;
                }
            }
            try
            {
                tc.saveXMLFile(file, sql);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, "Filed to save defination.\n" + ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void miAddServerActionPerformed(ActionEvent evt)//add server anywhere
    {
        ServerAddView sav = new ServerAddView(this, true, serverGroup);//, maintDB, 
        sav.setLocationRelativeTo(this);
        sav.setVisible(true);
        if (sav.isSuccess())
        {
            XMLServerInfoDTO addedServerInfo = sav.getNewServerInfo();
            DefaultMutableTreeNode serverGroupNode = (DefaultMutableTreeNode) treeView.serverGroupNode;//.rootNode.getChildAt(0);//only one server group
            ObjGroupDTO objGroupInfo = (ObjGroupDTO) serverGroupNode.getUserObject();
            objGroupInfo.getServerInfoList().add(addedServerInfo);
            DefaultMutableTreeNode serverNode = new DefaultMutableTreeNode(addedServerInfo, true);

            DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
            model.insertNodeInto(serverNode, serverGroupNode, serverGroupNode.getChildCount());
            treeView.scrollPathToVisible(new TreePath(serverNode.getPath()));
            treeView.setSelectionPath(new TreePath(serverNode.getPath()));//select add node
            //open server branch
            addedServerInfo.setConnected(true);
            treeView.serverClickedAction(serverNode);
        }
    }
    private void miOptionActionPerformed(ActionEvent evt)
    {
        OptionEditView optionView = new OptionEditView(this,true);
        optionView.setLocationRelativeTo(this);
        optionView.setVisible(true);
    }
    private void miChangePwdActionPerformed(ActionEvent evt)
    {
        logger.info("change pwd");
        if(node==null)
        {
            return;
        }
        Object obj = node.getUserObject();
        //ConnectInfoDTO connInfo = null;
        if (obj instanceof XMLServerInfoDTO)
        {
            //connInfo = (ConnectInfoDTO) obj;
            XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) obj;
            PwdChangeDialog pwdDialog = new PwdChangeDialog(this, true, serverInfo);
            pwdDialog.setLocationRelativeTo(this);
            pwdDialog.setVisible(true);
            if(pwdDialog.isFinished())
            {
                /*String changeUser = pwdDialog.getChangedUser();
                logger.debug("currentUser=" + serverInfo.getUser() + " | changedUser=" + changeUser);
                if (!serverInfo.getUser().equals(changeUser))
                {
                    int scount = treeView.serverGroupNode.getChildCount();
                    DefaultMutableTreeNode server;
                    XMLServerInfoDTO s;
                    for (int i = 0; i < scount; i++)
                    {
                        server = (DefaultMutableTreeNode) treeView.serverGroupNode.getChildAt(i);
                        s = (XMLServerInfoDTO) server.getUserObject();
                        if (changeUser.equals(s.getUser()) && s.isConnected())
                        {
                            server.removeAllChildren();
                            DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
                            model.reload();
                            TreePath tp = new TreePath(server.getPath());
                            treeView.scrollPathToVisible(tp);
                            treeView.setSelectionPath(tp);
                            //connect server             
                            //treeView.serverClickedAction(server);
                            return;
                        }
                    }
                    return;
                }*/
                serverInfo.setPwd(pwdDialog.getNewPwd());
                //disconnect server
                serverInfo.setConnected(false);
                node.removeAllChildren();
                DefaultTreeModel model = (DefaultTreeModel) treeView.getModel();
                model.reload();
                TreePath tp = new TreePath(node.getPath());
                treeView.scrollPathToVisible(tp);
                treeView.setSelectionPath(tp);
                //connect server             
                treeView.serverClickedAction(node);
                /*//not save pwd
                try{
                    TreeController.getInstance().changePwdForServerFromXML(serverInfo);
                } catch (Exception ex){
                    JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }*/
            }
        }

    }
    private void miOpenPgpassActionPerformed(ActionEvent evt)
    {
        ConfigMainView pgpassView = new ConfigMainView(null,"catalog.xml");// "pgpass.conf");
        pgpassView.setLocationRelativeTo(this);
        pgpassView.setVisible(true);
        pgpassView.openPgPasswordFileAction();
    }
    private void miExitActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        this.dispose();
        System.exit(0);//exit all the windows of this application
    }
    //CONSOLE [finish]
    private void miConsoleActionPerformed(ActionEvent evt)
    {
        logger.info("Open PSQL Console");
        Process proc = null;
        try
        {
            String scriptDir = OptionController.getInstance().getOptionProperties().getProperty("script_path");
            if (scriptDir == null || scriptDir.isEmpty()) 
            {
                JOptionPane.showMessageDialog(this, constBundle.getString("scriptDirIsEmpty"),
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
                return;
            }else if (!(new File(scriptDir).exists()))
            {
                JOptionPane.showMessageDialog(this, constBundle.getString("scriptDirIsNotExist"),
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
                return;
            }

            String osName = System.getProperty("os.name").toLowerCase();
            logger.info(osName);
            if (osName.indexOf("windows") != -1)
            {
                String name;
                if (scriptDir.endsWith("scripts"))
                {
                    name = "runpsql.bat";
                } else
                {
                    name = "runipsql.bat";
                }
                String[] command = new String[]
                {
                    "cmd.exe", "/C", "start", name
                };
                ProcessBuilder pb = new ProcessBuilder(command);
                logger.info("script: " + scriptDir + File.separator + name);
                File dir = new File(scriptDir);
                logger.info("file exist: " + dir.exists());
                pb.directory(dir);
                proc = pb.start();
            } else if (osName.indexOf("linux") != -1)
            {
                proc = Runtime.getRuntime().exec(scriptDir + File.separator + "launchpsql.sh");
            }
//            BufferedReader br = new BufferedReader(new InputStreamReader(proc.getInputStream()));
//            String line = new String();
//            while ((line = br.readLine()) != null)
//            {
//                logger.info(line);
//            }
            logger.info("-------PSQL Console Opened-------");
        } catch (Exception ex)
        {
            if (proc != null)
            {
                proc.destroy();
            }
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, constBundle.getString(ex.getMessage()),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    //VIEW 
    private void cbmiSqlWindowActionPerformed(ActionEvent evt)
    {
        logger.info("Sql Windows isSelected:" + cbmiSqlWindow.isSelected());
        if (cbmiSqlWindow.isSelected())
        {
            spaneRight.setRightComponent(spaneSqlWindow);
        } else
        {
            spaneRight.remove(spaneSqlWindow);
        }
    }
    private void cbmiObjBrowserActionPerformed(ActionEvent evt)
    {
        logger.info("object view isSelected:" + cbmiObjBrowser.isSelected());
        if (cbmiObjBrowser.isSelected())
        {
            jSplitPaneLeft.setLeftComponent(spaneTree);
        } else
        {
            jSplitPaneLeft.remove(spaneTree);
        }
    }
    private void cbmiToolbarActionPerformed(ActionEvent evt)
    {
        // TODO add your handling code here:
        logger.info("toolbar viw isSelected:"+cbmiToolbar.isSelected());
        if(cbmiToolbar.isSelected())
        {
            getContentPane().add(toolBar, BorderLayout.NORTH);
        }else
        {
            getContentPane().remove(toolBar);            
        }
        getContentPane().validate();
    }      
    private void miDefViewActionPerformed(ActionEvent evt)
    {
        logger.info("Set to default view");
        if (!cbmiSqlWindow.isSelected())
        {
            spaneRight.setRightComponent(spaneSqlWindow);
            cbmiSqlWindow.setSelected(true);
        }
        if (!cbmiObjBrowser.isSelected())
        {
            jSplitPaneLeft.setLeftComponent(spaneTree);
            cbmiObjBrowser.setSelected(true);
        }
        if (!cbmiToolbar.isSelected())
        {
            getContentPane().add(toolBar, BorderLayout.NORTH);
            getContentPane().validate();
            cbmiToolbar.setSelected(true);
        }
    }
    //TOOL
    private void miQueryActionPerformed(ActionEvent evt)
    {
        logger.info("open query view");
        HelperInfoDTO helperInfo;
        if (node.getUserObject() instanceof AbstractObject)
        {
            AbstractObject obj = (AbstractObject) node.getUserObject();
            helperInfo = obj.getHelperInfo();
            QueryMainView queryView = new QueryMainView(helperInfo, "");
            queryView.setLocationRelativeTo(this);
            queryView.setVisible(true);
        } else if (node.getUserObject() instanceof ObjGroupDTO)
        {
            ObjGroupDTO obj = (ObjGroupDTO) node.getUserObject();
            helperInfo = obj.getHelperInfo();
            QueryMainView queryView = new QueryMainView(helperInfo, "");
            queryView.setLocationRelativeTo(this);
            queryView.setVisible(true);
        }
        //XMLServerInfoDTO serverInfo = (XMLServerInfoDTO) node.getUserObject();
//        QueryMainView queryView = new QueryMainView(helperInfo, "");
//        queryView.setLocationRelativeTo(this);
//        queryView.setVisible(true);
    }
    private void miAuditActionPerformed(ActionEvent evt)
    {
        RegisterDialog registerDialog = new RegisterDialog(this, true);
        registerDialog.setLocationRelativeTo(this);
        registerDialog.setAlwaysOnTop(true);
        registerDialog.setVisible(true);
        if (registerDialog.isSuccess())
        {
            com.highgo.hgdbadmin.audit.model.HelperInfoDTO helperInfo = registerDialog.getHelperInfo();
            //MonitorView mv = new MonitorView(helperInfo);
            //mv.setLocationRelativeTo(null);
            //mv.setVisible(true);
            AuditLogDialog alDialog = new AuditLogDialog(this, helperInfo);
            alDialog.setLocationRelativeTo(this);
            alDialog.setVisible(true);
        }
    }
    
    private void miMigrationAssistantActionPerformed(ActionEvent evt)
    {
        MigrationView migrationView = new MigrationView();
        migrationView.setLocationRelativeTo(this);
        migrationView.setVisible(true);
    }
    private void miViewDataActionPerformed(ActionEvent evt)
    {
        logger.info("View All Data");
        Object obj = node.getUserObject();
        if (obj instanceof AbstractObject)
        {
            if(obj instanceof TableInfoDTO)
            {
                TableInfoDTO table = (TableInfoDTO) obj;
                if(table.getColumnInfoList().size()<=0)
                {
                    JOptionPane.showMessageDialog(this,constBundle.getString("noColumnHint"),
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
                    return;
                }
            }
            AbstractObject relation = (AbstractObject) obj;
            int originalQueryMode = relation.getHelperInfo().getQueryMode();
            try
            {
                //add begin by sunqk at 2019.05.31 for sysdba can see other user's data set simple mode
                relation.getHelperInfo().setQueryMode(1);
                //add end by sunqk at 2019.05.31 for sysdba can see other user's data
                String relname;
                if (TreeEnum.TreeNode.PARTITION == relation.getType()) {
                    relname = SyntaxController.getInstance().getName(relation.getName());
                } else {
                    relname = SyntaxController.getInstance().getName(relation.getHelperInfo().getSchema())
                            + "." + SyntaxController.getInstance().getName(relation.getName());
                }
                DataEditView dataView = new DataEditView(relation, null,
                        DataController.getInstance().getRowCount(relation.getHelperInfo(), relname, null));
                dataView.setLocationRelativeTo(this);
                dataView.setVisible(true);
            } catch (ClassNotFoundException | SQLException ex)
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            } finally
            {
                relation.getHelperInfo().setQueryMode(originalQueryMode);
            }
        }
    }
    private void miFilterDataActionPerformed(ActionEvent evt)
    {
        logger.info("Filter Data");
        Object obj = node.getUserObject();
        if (obj instanceof AbstractObject)
        {
            AbstractObject relation = (AbstractObject) obj;
            //add begin by sunqk at 2019.05.31 for sysdba can see other user's data
            int iQueryMode = relation.getHelperInfo().getQueryMode();
            relation.getHelperInfo().setQueryMode(1);
            //add end by sunqk at 2019.05.31 for sysdba can see other user's data
            DataSortFilterDialog filterDialog = new DataSortFilterDialog(this, true, relation, true, null);
            filterDialog.setLocationRelativeTo(this);
            filterDialog.setVisible(true);
            if (filterDialog.isSuccess())
            {
                DataEditView dataView = new DataEditView(relation, filterDialog.getFilterCondition(), filterDialog.getTotalRowCount());
                dataView.setLocationRelativeTo(this);
                dataView.setVisible(true);
            }
            relation.getHelperInfo().setQueryMode(iQueryMode);
        }
    }
    private void miMaintainActionPerformed(ActionEvent evt)
    {
        logger.info("maintain");
        Object obj = node.getUserObject();
        if (obj instanceof AbstractObject)
        {
            MaintanceView maintainView = new MaintanceView(this, true, (AbstractObject) obj);
            maintainView.setLocationRelativeTo(this);
            maintainView.setVisible(true);
        }
    }
    //HELP
    private void miHelpActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        Object item = evt.getSource();
        String filePath = null;
        if (item == miHgdbHelp || item == btnShowSqlHelp)
        {
            filePath = OptionController.HgdbHelpPath;
        } else if(item == miHgdbAdminHelp)
        {
            filePath = OptionController.HgdbAdminPath;
        }
        try
        {
            logger.debug(filePath);
            if(filePath == null)
            {
                return;
            }
            Desktop.getDesktop().open(new File(filePath));//.browse(new URI(filePath));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
        
        /*
        if(item==miHighGoDBHelp)
        {
            try
            {
                Desktop.getDesktop().browse(new URI("http://www.highgo.com"));
            } catch (IOException | URISyntaxException ex)
            {
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            }
        }else if (item == btnShowSqlHelp)
        {
            HtmlPageHelper.getInstance().showPgHelpPage(this, "sql-commands.html");
        }        
        if (item == miHelpContent)
        {
            HtmlPageHelper.getInstance().showAdminHelpPage(this, "using.html");
        } else if (item == miFAQ)
        {
            try
            {
                String uri = "http://www.pgadmin.org/support/faq.php";
                Desktop.getDesktop().browse(new URI(uri));
            } catch (Exception ex)
            {
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            }
        } else if (item == miBugReport)
        {
            HtmlPageHelper.getInstance().showAdminHelpPage(this, "bugreport.html");
        } else if (item == miHighGoDBHelp)
        {
            HtmlPageHelper.getInstance().showPgHelpPage(this, "index.html");
        } else if (item == btnShowSqlHelp)
        {
            HtmlPageHelper.getInstance().showPgHelpPage(this, "sql-commands.html");
        } else if (item == miSuggestion
                || item == btnShowObjSuggestion)
        {
            AbstractObject obj = (AbstractObject) node.getUserObject();
            TreeEnum.TreeNode type = obj.getType();
            String fileName = null;
            switch (type)
            {
                case TABLE:
                    fileName = "pk.html";
                    break;
                case VIEW:
                case FUNCTION:
                case TRIGGER_FUNCTION:
                    fileName = "object-editing.html";
                    break;
            }
            SuggestionDialog suggestionDialog = new SuggestionDialog(this, true, fileName);
            suggestionDialog.setLocationRelativeTo(this);
            suggestionDialog.setVisible(true);
        }
        else if (item == miAbout)
        {
            logger.info("this is about hgdbadmin");
            AboutAdmin about = new AboutAdmin(this, true);
            about.setLocationRelativeTo(this);
            about.setVisible(true);
        }
        */
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try
        {
            System.out.println("SystemLookAndFeelClassName=" + UIManager.getSystemLookAndFeelClassName().toString());
            System.out.println("CrossPlatformLookAndFeelClassName=" + UIManager.getCrossPlatformLookAndFeelClassName().toString());
            //com.sun.java.swing.plaf.gtk.GTKLookAndFeel
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName().toString());
            //UIManager.setLookAndFeel(new org.jvnet.substance.skin.SubstanceOfficeSilver2007LookAndFeel());            
            //UIManager.installLookAndFeel("SeaGlass", "com.seaglasslookandfeel.SeaGlassLookAndFeel");
            //UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");//Chinese characters garbled
            //UIManager.getLookAndFeelDefaults().put("defaultFont", new Font("Microsoft Yahei",Font.PLAIN,13));//to solve the problem of seaglass not supporting Chinese default

            //UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }

        //</editor-fold>

        /* Create and display the form */
        //or SwingUtilities.invokeLater(...)// in fact SwingUtilities.invokeLater will call EventQueue.invokeLater.
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                SimpleEventBus eventBus = new SimpleEventBus();
                MainView mainView = new MainView(eventBus);
                mainView.setVisible(true);
                
//                HelperInfoDTO h = new HelperInfoDTO();
//                h.setHost("127.0.0.1");
//                h.setPort("5433");
//                h.setDbName("postgres");
//                h.setUser("postgres");
//                h.setPwd("postgres");
//                QueryMainView  qm = new QueryMainView(h, null);
//                qm.setLocationRelativeTo(null);
//                qm.setVisible(true);
                
//                MigrationView migrationView = new MigrationView();
//                migrationView.setLocationRelativeTo(null);
//                migrationView.setVisible(true);
            }
        });
    }
    
    //tree right-click item
    private JPopupMenu popMenu;
    private JPopupMenu.Separator separator0, separator1, separator2;   
    
    private JMenu menuTreeAddObj;
    private JMenuItem itemAddServer;
    private JMenuItem itemAddDB;
    private JMenuItem itemAddTablespace;
    private JMenuItem itemAddGroupRole;
    private JMenuItem itemAddLoginRole;
    private JMenuItem itemAddEventTrigger;
    private JMenuItem itemAddSchema;
    private JMenuItem itemAddFunction;
    private JMenuItem itemAddProcedure;
    private JMenuItem itemAddSequence;
    private JMenuItem itemAddView;
    private JMenuItem itemAddTable;
    private JMenuItem itemAddColumn;
    private JMenuItem itemAddPK;
    private JMenuItem itemAddFK;
    private JMenuItem itemAddExclude;
    private JMenuItem itemAddCheck;
    private JMenuItem itemAddUnique;
    private JMenuItem itemAddIndex;
    private JMenuItem itemAddRule;
    private JMenuItem itemAddTrigger;
    
    private JMenuItem itemCreate;
    private JMenuItem itemDrop;
    private JMenuItem itemDropCascade;
    private JMenuItem itemTruncateTable;
    private JMenuItem itemTruncateTableCascade;
    
    private JMenuItem itemProperty;
    
    private JMenuItem itemRefresh;
    
    private JMenuItem itemRowCount;
    private JMenuItem itemExportData;
    private JMenuItem itemImportData;

    private JMenu menuTreeScript;
    private JMenuItem itemCreateScript;//for all
    private JMenuItem itemSelectScript;//for table, view
    private JMenuItem itemInsertScript;//for table
    private JMenuItem itemDeleteScript;//for table
    private JMenuItem itemUpdateScript;//for table
   
    private JCheckBoxMenuItem cbitemEnableConstraint;
    private JCheckBoxMenuItem cbitemEnableTrigger;
    private JCheckBoxMenuItem cbitemEnableRule;
    private JMenuItem itemEnableAllTrigger;
    private JMenuItem itemDisableAllTrigger;
   
    private JMenuItem itemMaintainData;
    
    private JMenuItem itemBackup;
    private JMenuItem itemBackupAll;
    private JMenuItem itemRestore;

    private JMenuItem itemReassignDropOwnedTo;
    private JMenuItem itemMoveTablespace;
    
    private JMenuItem itemConnServer;
    private JMenuItem itemDisconnServer;
    private JMenuItem itemChangePwd;
    
    private JMenuItem itemReloadConfiguration;
    private JMenuItem itemAddNameRestorePoint;
    private JMenuItem itemPauseWALReplay;
    private JMenuItem itemResumeWALReplay;
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddServer;
    private javax.swing.JButton btnAudit;
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDataView;
    private javax.swing.JButton btnDrop;
    private javax.swing.JButton btnExecuteLastPlugs;
    private javax.swing.JButton btnExportData;
    private javax.swing.JButton btnFilterDataView;
    private javax.swing.JButton btnImportData;
    private javax.swing.JButton btnMaintainData;
    private javax.swing.JButton btnMigrationAssistant;
    private javax.swing.JButton btnProperty;
    private javax.swing.JButton btnQuery;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnShowObjSuggestion;
    private javax.swing.JButton btnShowSqlHelp;
    private javax.swing.JCheckBoxMenuItem cbmiEnableRule;
    private javax.swing.JCheckBoxMenuItem cbmiEnableTrigger;
    private javax.swing.JCheckBoxMenuItem cbmiObjBrowser;
    private javax.swing.JCheckBoxMenuItem cbmiSqlWindow;
    private javax.swing.JCheckBoxMenuItem cbmiToolbar;
    private javax.swing.JSplitPane jSplitPaneLeft;
    private javax.swing.JPopupMenu.Separator jsepAdd;
    private javax.swing.JPopupMenu.Separator jsepChange;
    private javax.swing.JPopupMenu.Separator jsepConnect;
    private javax.swing.JPopupMenu.Separator jsepDebug;
    private javax.swing.JPopupMenu.Separator jsepExit;
    private javax.swing.JPopupMenu.Separator jsepOpt;
    private javax.swing.JPopupMenu.Separator jsepReload;
    private javax.swing.JPopupMenu.Separator jsepReport;
    private javax.swing.JPopupMenu.Separator jsepRun;
    private javax.swing.JPopupMenu.Separator jsepServer;
    private javax.swing.JTextField jtxfState;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuConfigEditor;
    private javax.swing.JMenu menuDebug;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuHelp;
    private javax.swing.JMenu menuPlugs;
    private javax.swing.JMenu menuReport;
    private javax.swing.JMenu menuScript;
    private javax.swing.JMenu menuTool;
    private javax.swing.JMenu menuView;
    private javax.swing.JMenu menuViewData;
    private javax.swing.JMenuItem miAbout;
    private javax.swing.JMenuItem miAddNameRestorePoint;
    private javax.swing.JMenuItem miAddServer;
    private javax.swing.JMenuItem miAffiliatedRelationReport;
    private javax.swing.JMenuItem miAudit;
    private javax.swing.JMenuItem miBackup;
    private javax.swing.JMenuItem miBackupAll;
    private javax.swing.JMenuItem miBugReport;
    private javax.swing.JMenuItem miChangePwd;
    private javax.swing.JMenuItem miConnServer;
    private javax.swing.JMenuItem miConsole;
    private javax.swing.JMenuItem miCopy;
    private javax.swing.JMenuItem miCreate;
    private javax.swing.JMenuItem miCreateScript;
    private javax.swing.JMenuItem miDDLReport;
    private javax.swing.JMenuItem miDataDictionaryReport;
    private javax.swing.JMenuItem miDataFilter;
    private javax.swing.JMenuItem miDataViewAll;
    private javax.swing.JMenuItem miDefView;
    private javax.swing.JMenuItem miDeleteScript;
    private javax.swing.JMenuItem miDependentReport;
    private javax.swing.JMenuItem miDisableAllTrigger;
    private javax.swing.JMenuItem miDisconnServer;
    private javax.swing.JMenuItem miDrop;
    private javax.swing.JMenuItem miDropCascade;
    private javax.swing.JMenuItem miEnableAllTrigger;
    private javax.swing.JMenuItem miExecScript;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miExportData;
    private javax.swing.JMenuItem miExtendDataTab;
    private javax.swing.JMenuItem miExtendIndex;
    private javax.swing.JMenuItem miFAQ;
    private javax.swing.JMenuItem miHba;
    private javax.swing.JMenuItem miHgdbAdminHelp;
    private javax.swing.JMenuItem miHgdbHelp;
    private javax.swing.JMenuItem miImportData;
    private javax.swing.JMenuItem miInsertScript;
    private javax.swing.JMenuItem miMaintainData;
    private javax.swing.JMenuItem miMigrationAssistant;
    private javax.swing.JMenuItem miMoveTablespace;
    private javax.swing.JMenuItem miObjListReport;
    private javax.swing.JMenuItem miOpenCatalog;
    private javax.swing.JMenuItem miOpenPGHba;
    private javax.swing.JMenuItem miOpenPostgresql;
    private javax.swing.JMenuItem miOption;
    private javax.swing.JMenuItem miPauseWALReplay;
    private javax.swing.JMenuItem miPostgresql;
    private javax.swing.JMenuItem miProperty;
    private javax.swing.JMenuItem miPropertyReport;
    private javax.swing.JMenuItem miQuery;
    private javax.swing.JMenuItem miReassignDropOwnedTo;
    private javax.swing.JMenuItem miRefresh;
    private javax.swing.JMenuItem miReloadConfiguration;
    private javax.swing.JMenuItem miRestore;
    private javax.swing.JMenuItem miResumeWALReplay;
    private javax.swing.JMenuItem miRowCount;
    private javax.swing.JMenuItem miRun;
    private javax.swing.JMenuItem miSaveDefine;
    private javax.swing.JMenuItem miSelectScript;
    private javax.swing.JMenuItem miServerConfig;
    private javax.swing.JMenuItem miServerState;
    private javax.swing.JMenuItem miStartServer;
    private javax.swing.JMenuItem miStatisticsReport;
    private javax.swing.JMenuItem miStopServer;
    private javax.swing.JMenuItem miSuggestion;
    private javax.swing.JMenuItem miTruncateTable;
    private javax.swing.JMenuItem miTruncateTableCascade;
    private javax.swing.JMenuItem miUpdateScript;
    private javax.swing.JMenuItem miWeighted;
    private javax.swing.JPopupMenu.Separator spAbout;
    private javax.swing.JPopupMenu.Separator spCopy;
    private javax.swing.JPopupMenu.Separator spCreate;
    private javax.swing.JPopupMenu.Separator spProperty;
    private javax.swing.JPopupMenu.Separator spRefesh;
    private javax.swing.JPopupMenu.Separator spView;
    private javax.swing.JScrollPane spaneDependent;
    private javax.swing.JScrollPane spanePaneRelation;
    private javax.swing.JScrollPane spaneProperty;
    private javax.swing.JSplitPane spaneRight;
    private javax.swing.JScrollPane spaneSqlWindow;
    private javax.swing.JScrollPane spaneStatistics;
    private javax.swing.JScrollPane spaneTree;
    private javax.swing.JTabbedPane tabbpane;
    private javax.swing.JToolBar toolBar;
    // End of variables declaration//GEN-END:variables
}
