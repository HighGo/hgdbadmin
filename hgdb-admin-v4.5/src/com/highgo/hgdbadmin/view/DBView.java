/* ------------------------------------------------ 
* 
* File: DBView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\DBView.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.Compare;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.SysVariableInfoDTO;
import com.highgo.hgdbadmin.model.VariableInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * privileges - role inclouds [public] and [other group roles];
 *              defalut privileges are [TTEMP and CONNECT] for public;
 *              if privileges are default,nothing need to do;
 *              if privileges are not for [public],then must execute [REVOKE ALL ON DATABASE dd FROM public];
 *              if privileges are not [TEMP and CONNECT] for [public], like [CREATE, TEMP, CONNECT]
 *                  then must execute [REVOKE ALL ON DATABASE dd FROM public;GRANT ALL ON DATABASE dd TO public;]
 *              if privileges are for [group role], then must execute [GRANT ALL ON DATABASE dd TO GROUP ddd;] 
 * 
 * 
 */
public class DBView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private HelperInfoDTO helperInfo;
    private boolean isSuccess = false;
    
    private DBInfoDTO dbInfo;    
    private SysVariableInfoDTO currentVariable=null;   
    
    //add db
    public DBView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.dbInfo = null;
        initComponents();
        jtp.setEnabledAt(4, false);
        this.setTitle(constBundle.getString("addDB"));
        this.setNormalInfo(false);
        
        //define action
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForAdd(evt);
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });  
    }

    //view db property  
    public DBView(JFrame parent, boolean modal, DBInfoDTO dbInfo)
    {
        super(parent, modal);
        this.helperInfo = dbInfo.getHelperInfo();
        this.dbInfo = dbInfo;
        initComponents();
        jtp.setEnabledAt(4, false);
        this.setTitle(constBundle.getString("databases") + " " + dbInfo.getName());
        this.setNormalInfo(true);
        //property
        txfdName.setText(dbInfo.getName());
        txfdOID.setText(String.valueOf(dbInfo.getOid()));
        cbbOwner.setSelectedItem(dbInfo.getOwner());
        txarComment.setText(dbInfo.getComment());
        cbbEncoding.setSelectedItem(dbInfo.getEncoding());
        cbbEncoding.setEnabled(false);
        //cbbTemplate.setSelectedItem(parent);
        cbbTemplate.setEnabled(false);
        cbbTablespace.setSelectedItem(dbInfo.getTablespace());
        cbbCollation.setSelectedItem(dbInfo.getCollation());
        cbbCollation.setEnabled(false);
        cbbCharacterType.setSelectedItem(dbInfo.getCharacterType());
        cbbCharacterType.setEnabled(false);
        txfdConnectionLimit.setText(dbInfo.getConnectionLimit());
        //ttarSchemaRestriction.setText();        
        DefaultTableModel vModel = (DefaultTableModel) tblVariables.getModel();
        List<VariableInfoDTO> vlist = dbInfo.getVariableList();
        if (vlist != null)
        {            
            for (VariableInfoDTO variable : vlist)
            {
                String[] v = new String[3];
                v[0] = variable.getUser();
                v[1] = variable.getName();
                v[2] = variable.getValue();
                vModel.addRow(v);
            }
        }
        DefaultTableModel pModel = (DefaultTableModel) tblPrivileges.getModel();        
        pModel.removeRow(0);//clear default acl
        String acl = dbInfo.getAcl();
        if (acl != null && !acl.isEmpty() && !acl.equals("{}"))
        {
            acl = acl.substring(1, acl.length() - 1);
            String[] privilegeArray = acl.split(",");
            for (String privilege : privilegeArray)
            {
                logger.info("privilege:" + privilege);
                String[] pArray = privilege.split("=");
                String[] p = new String[2];
                if (pArray[0] == null || pArray[0].isEmpty())
                {
                    p[0] = "public";
                } else
                {
                    p[0] = pArray[0];
                }
                p[1] = pArray[1].split("/")[0];
                pModel.addRow(p);
            }
        }
        List<SecurityLabelInfoDTO> slist = dbInfo.getSecurityLabelList();
        if (slist != null)
        {
            DefaultTableModel slModel = (DefaultTableModel) tblSecurityLabels.getModel();
            for (SecurityLabelInfoDTO securityLabel : slist)
            {
                String[] sl = new String[3];
                sl[0] = securityLabel.getProvider();
                sl[1] = securityLabel.getSecurityLabel();
                slModel.addRow(sl);
            }
        }
        
        
        //define action
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        });
        cbbOwner.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        txarComment.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChange();
            }
        });
        cbbTablespace.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableForChange();
            }
        });
        txfdConnectionLimit.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                btnOkEnableForChange();
            }
        });
        vModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnableForChange();
            }
        });
        pModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                btnOkEnableForChange();
            }
        });        
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });  
    }

    private void setNormalInfo(boolean isChange)
    {
        TreeController tc = TreeController.getInstance();
        try
        {
            cbbTablespace.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "tablespace")));
            if(isChange)
            {
                cbbTablespace.removeItemAt(0);
            }else
            {
                cbbTablespace.setSelectedItem(0);
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            String[] owners = (String[]) tc.getItemsArrary(helperInfo, "owner");
            cbbOwner.setModel(new DefaultComboBoxModel(owners));
            if (isChange)
            {
                cbbOwner.removeItemAt(0);
            } else
            {
                cbbOwner.setSelectedIndex(0);
            }
            cbbUserName.setModel(new DefaultComboBoxModel(owners));
            if (isChange)
            {
                cbbUserName.removeItemAt(0);
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbTemplate.setModel(new DefaultComboBoxModel(tc.getItemsArrary(helperInfo, "template")));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbRole.setModel(new DefaultComboBoxModel(tc.getRoles(helperInfo)));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        try
        {
            cbbVariableNames.setModel(new DefaultComboBoxModel(tc.getVariables(helperInfo, "database")));
            currentVariable = (SysVariableInfoDTO) cbbVariableNames.getItemAt(0);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        cbValue.setEnabled(false);

        txfdConnectionLimit.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                //can only enter digit (>=0)
                txfdIntegerValueKeyTyped(evt);
            }
        });
        txfdVariableValue.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                variableValueForAddKeyTyped(evt);
            }
        });
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblOID = new javax.swing.JLabel();
        lblOwner = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        txfdOID = new javax.swing.JTextField();
        cbbOwner = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txarComment = new javax.swing.JTextArea();
        pnlDefinition = new javax.swing.JPanel();
        lblEncoding = new javax.swing.JLabel();
        lblTemplate = new javax.swing.JLabel();
        lblTablesapce = new javax.swing.JLabel();
        lblCollation = new javax.swing.JLabel();
        lblCharacterType = new javax.swing.JLabel();
        lblConnectionLimit = new javax.swing.JLabel();
        txfdConnectionLimit = new javax.swing.JTextField();
        cbbTablespace = new javax.swing.JComboBox();
        cbbTemplate = new javax.swing.JComboBox();
        cbbEncoding = new javax.swing.JComboBox();
        cbbCollation = new javax.swing.JComboBox();
        cbbCharacterType = new javax.swing.JComboBox();
        pnlVariables = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblVariables = new javax.swing.JTable();
        lblVariableValue = new javax.swing.JLabel();
        lblUserName = new javax.swing.JLabel();
        btnAddChangeVariable = new javax.swing.JButton();
        btnRemoveVariable = new javax.swing.JButton();
        lblVariableName = new javax.swing.JLabel();
        cbbUserName = new javax.swing.JComboBox();
        txfdVariableValue = new javax.swing.JTextField();
        cbbVariableNames = new javax.swing.JComboBox();
        cbValue = new javax.swing.JCheckBox();
        pnlPrivileges = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPrivileges = new javax.swing.JTable();
        btnAddChangePrivileges = new javax.swing.JButton();
        btnRemovePrivilege = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblRole = new javax.swing.JLabel();
        cbbRole = new javax.swing.JComboBox();
        cbAll = new javax.swing.JCheckBox();
        cbConnect = new javax.swing.JCheckBox();
        cbCreate = new javax.swing.JCheckBox();
        cbOptionAll = new javax.swing.JCheckBox();
        cbTemp = new javax.swing.JCheckBox();
        cbOptionConnect = new javax.swing.JCheckBox();
        cbOptionCreate = new javax.swing.JCheckBox();
        cbOptionTemp = new javax.swing.JCheckBox();
        pnlSecurityLabel = new javax.swing.JPanel();
        lblSecurityLabel = new javax.swing.JLabel();
        lblProvider = new javax.swing.JLabel();
        txfdProvider = new javax.swing.JTextField();
        txfdSecurityLabel = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblSecurityLabels = new javax.swing.JTable();
        btnRemoveSecurityLabel = new javax.swing.JButton();
        btnAddChangeSecurityLabel = new javax.swing.JButton();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addDB"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/database.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 470));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(520, 470));
pnlProperties.setPreferredSize(new java.awt.Dimension(520, 470));

lblName.setText(constBundle.getString("name"));

lblOID.setText("OID");

lblOwner.setText(constBundle.getString("owner"));

lblComment.setText(constBundle.getString("comment"));

txfdOID.setEditable(false);

cbbOwner.setEditable(true);

txarComment.setColumns(20);
txarComment.setRows(5);
jScrollPane1.setViewportView(txarComment);

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
        .addGap(10, 10, 10)
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblOwner, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(lblComment, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(txfdOID)
            .addComponent(txfdName)
            .addComponent(cbbOwner, 0, 413, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 487, Short.MAX_VALUE))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblName)
                .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblOID))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblOwner)
                .addComponent(cbbOwner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(lblComment)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 327, Short.MAX_VALUE))
            .addContainerGap())
    );

    lblOID.getAccessibleContext().setAccessibleName("");

    jtp.addTab(constBundle.getString("property"), pnlProperties);
    pnlProperties.getAccessibleContext().setAccessibleName("constBundle.getString(\"property\")");

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(520, 470));

    lblEncoding.setText(constBundle.getString("encoding"));

    lblTemplate.setText(constBundle.getString("template"));

    lblTablesapce.setText(constBundle.getString("tablespace"));

    lblCollation.setText(constBundle.getString("collation"));

    lblCharacterType.setText(constBundle.getString("characterType"));

    lblConnectionLimit.setText(constBundle.getString("connectionLimit"));

    txfdConnectionLimit.setText("-1");

    cbbEncoding.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "UTF8" }));

    cbbCollation.setEditable(true);
    cbbCollation.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "","C", "Chinese (Simplified)_China.936", "POSIX"}));

    cbbCharacterType.setEditable(true);
    cbbCharacterType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "","C", "Chinese (Simplified)_China.936", "POSIX" }));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblTemplate, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblTablesapce, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblCollation, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblCharacterType, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblConnectionLimit, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbbTemplate, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txfdConnectionLimit, javax.swing.GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
                .addComponent(cbbTablespace, 0, 413, Short.MAX_VALUE)
                .addComponent(cbbEncoding, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbbCollation, 0, 487, Short.MAX_VALUE)
                .addComponent(cbbCharacterType, javax.swing.GroupLayout.Alignment.TRAILING, 0, 413, Short.MAX_VALUE))
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblEncoding)
                .addComponent(cbbEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblTemplate)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblTablesapce)
                        .addComponent(cbbTablespace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCollation)
                        .addComponent(cbbCollation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCharacterType)
                        .addComponent(cbbCharacterType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblConnectionLimit)
                        .addComponent(txfdConnectionLimit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addComponent(cbbTemplate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(297, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlVariables.setBackground(new java.awt.Color(255, 255, 255));
    pnlVariables.setPreferredSize(new java.awt.Dimension(520, 500));

    tblVariables.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblVariables.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblVariables.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblVariablesMousePressed(evt);
        }
    });
    jScrollPane5.setViewportView(tblVariables);
    if (tblVariables.getColumnModel().getColumnCount() > 0)
    {
        tblVariables.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("userName"));
        tblVariables.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("variableName"));
        tblVariables.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("variableValue"));
    }

    lblVariableValue.setText(constBundle.getString("variableValue"));

    lblUserName.setText(constBundle.getString("userName"));

    btnAddChangeVariable.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeVariable.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeVariable.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeVariableActionPerformed(evt);
        }
    });

    btnRemoveVariable.setText(constBundle.getString("remove"));
    btnRemoveVariable.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveVariable.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveVariableActionPerformed(evt);
        }
    });

    lblVariableName.setText(constBundle.getString("variableName"));

    cbbUserName.setEditable(true);

    cbbVariableNames.setEditable(true);
    cbbVariableNames.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbVariableNamesItemStateChanged(evt);
        }
    });

    cbValue.setBackground(new java.awt.Color(255, 255, 255));
    cbValue.setDoubleBuffered(true);

    javax.swing.GroupLayout pnlVariablesLayout = new javax.swing.GroupLayout(pnlVariables);
    pnlVariables.setLayout(pnlVariablesLayout);
    pnlVariablesLayout.setHorizontalGroup(
        pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlVariablesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addComponent(lblUserName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(cbbUserName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addComponent(lblVariableName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(cbbVariableNames, 0, 404, Short.MAX_VALUE))
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(pnlVariablesLayout.createSequentialGroup()
                    .addComponent(lblVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(18, 18, 18)
                    .addComponent(cbValue)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(txfdVariableValue, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlVariablesLayout.setVerticalGroup(
        pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlVariablesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveVariable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblVariableName)
                .addComponent(cbbVariableNames, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblVariableValue)
                    .addComponent(cbValue))
                .addComponent(txfdVariableValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlVariablesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblUserName)
                .addComponent(cbbUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10))
    );

    jtp.addTab(constBundle.getString("variables"), pnlVariables);
    pnlVariables.getAccessibleContext().setAccessibleName("");

    pnlPrivileges.setBackground(new java.awt.Color(255, 255, 255));
    pnlPrivileges.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlPrivileges.setPreferredSize(new java.awt.Dimension(520, 470));

    tblPrivileges.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {
            {"public", "Tc"}
        },
        new String []
        {
            "", ""
        }
    )
    {
        boolean[] canEdit = new boolean []
        {
            false, false
        };

        public boolean isCellEditable(int rowIndex, int columnIndex)
        {
            return canEdit [columnIndex];
        }
    });
    tblPrivileges.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    tblPrivileges.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblPrivilegesMousePressed(evt);
        }
    });
    jScrollPane4.setViewportView(tblPrivileges);
    if (tblPrivileges.getColumnModel().getColumnCount() > 0)
    {
        tblPrivileges.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("user")+"/"+constBundle.getString("group"));
        tblPrivileges.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("privileges"));
    }

    btnAddChangePrivileges.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangePrivileges.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangePrivileges.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangePrivilegesActionPerformed(evt);
        }
    });

    btnRemovePrivilege.setText(constBundle.getString("remove"));
    btnRemovePrivilege.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemovePrivilege.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemovePrivilegeActionPerformed(evt);
        }
    });

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, constBundle.getString("privileges"), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("宋体", 0, 12), new java.awt.Color(204, 204, 204))); // NOI18N

    lblRole.setText(constBundle.getString("role"));

    cbbRole.addItemListener(new java.awt.event.ItemListener()
    {
        public void itemStateChanged(java.awt.event.ItemEvent evt)
        {
            cbbRoleItemStateChanged(evt);
        }
    });

    cbAll.setText("ALL");
    cbAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbAllActionPerformed(evt);
        }
    });

    cbConnect.setText("CONNECT");

    cbCreate.setText("CREATE");

    cbOptionAll.setText("WITH GRANT OPTION");
    cbOptionAll.setEnabled(false);
    cbOptionAll.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbOptionAllActionPerformed(evt);
        }
    });

    cbTemp.setText("TEMP");

    cbOptionConnect.setText("WITH GRANT OPTION");
    cbOptionConnect.setEnabled(false);

    cbOptionCreate.setText("WITH GRANT OPTION");
    cbOptionCreate.setEnabled(false);

    cbOptionTemp.setText("WITH GRANT OPTION");
    cbOptionTemp.setEnabled(false);

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(lblRole, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbRole, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbCreate)
                        .addComponent(cbTemp)
                        .addComponent(cbConnect)
                        .addComponent(cbAll))
                    .addGap(165, 165, 165)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbOptionConnect)
                        .addComponent(cbOptionCreate)
                        .addComponent(cbOptionAll)
                        .addComponent(cbOptionTemp))))
            .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
        jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblRole))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbOptionAll)
                .addComponent(cbAll))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbCreate)
                .addComponent(cbOptionCreate))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbTemp)
                .addComponent(cbOptionTemp))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(cbConnect)
                .addComponent(cbOptionConnect))
            .addContainerGap())
    );

    javax.swing.GroupLayout pnlPrivilegesLayout = new javax.swing.GroupLayout(pnlPrivileges);
    pnlPrivileges.setLayout(pnlPrivilegesLayout);
    pnlPrivilegesLayout.setHorizontalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPrivilegesLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pnlPrivilegesLayout.createSequentialGroup()
                    .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );
    pnlPrivilegesLayout.setVerticalGroup(
        pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPrivilegesLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPrivilegesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangePrivileges, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemovePrivilege, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("privileges"), pnlPrivileges);
    pnlPrivileges.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlSecurityLabel.setBackground(new java.awt.Color(255, 255, 255));
    pnlSecurityLabel.setDoubleBuffered(false);
    pnlSecurityLabel.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSecurityLabel.setPreferredSize(new java.awt.Dimension(520, 470));

    lblSecurityLabel.setText(constBundle.getString("securityLabels"));

    lblProvider.setText(constBundle.getString("provider"));

    tblSecurityLabels.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    ));
    tblSecurityLabels.addMouseListener(new java.awt.event.MouseAdapter()
    {
        public void mousePressed(java.awt.event.MouseEvent evt)
        {
            tblSecurityLabelsMousePressed(evt);
        }
    });
    jScrollPane3.setViewportView(tblSecurityLabels);
    if (tblSecurityLabels.getColumnModel().getColumnCount() > 0)
    {
        tblSecurityLabels.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("provider"));
        tblSecurityLabels.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("securityLabels"));
    }

    btnRemoveSecurityLabel.setText(constBundle.getString("remove"));
    btnRemoveSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveSecurityLabelActionPerformed(evt);
        }
    });

    btnAddChangeSecurityLabel.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeSecurityLabel.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeSecurityLabel.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeSecurityLabelActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlSecurityLabelLayout = new javax.swing.GroupLayout(pnlSecurityLabel);
    pnlSecurityLabel.setLayout(pnlSecurityLabelLayout);
    pnlSecurityLabelLayout.setHorizontalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane3)
            .addGap(10, 10, 10))
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
                    .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(375, 375, 375))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlSecurityLabelLayout.createSequentialGroup()
                    .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblProvider, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSecurityLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdSecurityLabel)
                        .addComponent(txfdProvider))
                    .addContainerGap())))
    );
    pnlSecurityLabelLayout.setVerticalGroup(
        pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSecurityLabelLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(txfdProvider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblProvider))
            .addGap(10, 10, 10)
            .addGroup(pnlSecurityLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(txfdSecurityLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblSecurityLabel))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("securityLabels"), pnlSecurityLabel);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSQL.setPreferredSize(new java.awt.Dimension(520, 470));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(cbReadOnly)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);
    pnlSQL.getAccessibleContext().setAccessibleName("SQL");

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 339, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could be entered all integer.
        char keyCh = evt.getKeyChar();
        logger.info("keych=" + keyCh + ",isDigit=" + Character.isDigit(keyCh));
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '' && (keyCh != '-'))
            {
                evt.setKeyChar('\0');
            }
        }
    } 
    private void variableValueForAddKeyTyped(KeyEvent evt)
    {
        String type = currentVariable.getValueType();
        char keyCh = evt.getKeyChar();
        //logger.info("keych=" + keyCh+",isDigit="+ Character.isDigit(keyCh));
        logger.info("Variable Type = " + type);
        switch (type)
        {
            case "real":
                if ((keyCh < '0') || (keyCh > '9'))
                {
                    if (keyCh != '' && keyCh != '.' && keyCh!='-')
                    {
                        evt.setKeyChar('\0');
                    }
                    if(!currentVariable.getMinValue().startsWith("-")
                            && keyCh=='-')
                    {
                         evt.setKeyChar('\0');
                    }
                }
                break;
            case "integer":
                if ((keyCh < '0') || (keyCh > '9'))
                {
                    if (keyCh != ''  && keyCh!='-')
                    {
                        evt.setKeyChar('\0');
                    }
                    if (!currentVariable.getMinValue().startsWith("-")
                            && keyCh == '-')
                    {
                        evt.setKeyChar('\0');
                    }
                }
                break;
        }
    }
      
    private void btnRemoveSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnRemoveSecurityLabelActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press security label table");
        int selectedRow = tblSecurityLabels.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblSecurityLabels.getModel();
        tableModel.removeRow(selectedRow);    
    }//GEN-LAST:event_btnRemoveSecurityLabelActionPerformed

    private void btnAddChangeSecurityLabelActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeSecurityLabelActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeSecurityLabelActionPerformed
        // TODO add your handling code here:
        String provider  = txfdProvider.getText();
        String label = txfdSecurityLabel.getText();
        logger.info("provider:"+provider+",label:"+label);
        if(provider ==null || provider.equals(""))
        {
            return;
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String providerOld = tableModel.getValueAt(i, 0).toString();
            logger.info("providerOld=" + providerOld + providerOld.equals(provider));
            if (providerOld.equals(provider))
            {
                isExist = true;
                tableModel.setValueAt(label, i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] labels = new String[2];
            labels[0] = provider;
            labels[1] = label;
            tableModel.addRow(labels);
        }
    }//GEN-LAST:event_btnAddChangeSecurityLabelActionPerformed

    private void btnAddChangePrivilegesActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangePrivilegesActionPerformed
    {//GEN-HEADEREND:event_btnAddChangePrivilegesActionPerformed
        // TODO add your handling code here:
        boolean all = cbAll.isSelected();
        boolean create = cbCreate.isSelected();
        boolean temp = cbTemp.isSelected();
        boolean connect = cbConnect.isSelected();

        String usergroup = cbbRole.getSelectedItem().toString();
        StringBuilder privileges = new StringBuilder();
        if (create)
        {
            privileges.append("C");
        }
        if (cbOptionCreate.isSelected())
        {
            privileges.append("*");
        }
        if (temp)
        {
            privileges.append("T");
        }
        if (cbOptionTemp.isSelected())
        {
            privileges.append("*");
        }
        if (connect)
        {
            privileges.append("c");
        }
        if (cbOptionConnect.isSelected())
        {
            privileges.append("*");
        }

        DefaultTableModel tableModel = (DefaultTableModel) tblPrivileges.getModel();
        int row = tableModel.getRowCount();
        boolean isExist = false;
        for (int i = 0; i < row; i++)
        {
            logger.info("row = " + row);
            String usergroupOld = tableModel.getValueAt(i, 0).toString();
            logger.info("usergroupOld=" + usergroupOld + usergroupOld.equals(usergroup));
            if (usergroupOld.equals(usergroup))
            {
                isExist = true;
                tableModel.setValueAt(privileges.toString(), i, 1);
            }
            if (isExist)
            {
                break;
            }
        }
        if (!isExist)
        {
            String[] p = new String[2];
            p[0] = usergroup;
            p[1] = privileges.toString();
            tableModel.addRow(p);
        }
    }//GEN-LAST:event_btnAddChangePrivilegesActionPerformed

    private void btnRemovePrivilegeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemovePrivilegeActionPerformed
    {//GEN-HEADEREND:event_btnRemovePrivilegeActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int selectedRow = tblPrivileges.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if(selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel =  (DefaultTableModel) tblPrivileges.getModel();
        tableModel.removeRow(selectedRow);     
    }//GEN-LAST:event_btnRemovePrivilegeActionPerformed

    private void btnAddChangeVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeVariableActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeVariableActionPerformed
        // TODO add your handling code here:
        String name = cbbVariableNames.getSelectedItem().toString();
        String value;
        if (txfdVariableValue.isEnabled())
        {
            value = txfdVariableValue.getText();
        } else
        {
            value = String.valueOf(cbValue.isSelected());
        }
        if(value==null || value.equals(""))
        {
            value = "DEFAULT";
        }
        String user = cbbUserName.getSelectedItem().toString();  
        String db = txfdName.getText();        
        if(name!=null && !name.equals(""))
        {
            DefaultTableModel tableModel =  (DefaultTableModel) tblVariables.getModel();
            
            int row = tableModel.getRowCount();
            boolean isExist = false;
            for(int i=0;i<row;i++)
            {
                logger.info("row="+row);
                String userOld = tableModel.getValueAt(i, 0).toString();
                String nameOld = tableModel.getValueAt(i, 1).toString();                
                logger.info("nameOld="+nameOld+nameOld.equals(name));
                if(userOld.equals(user) && nameOld.equals(name))
                {
                    isExist = true;
                    tableModel.setValueAt(value, i, 2);
                }
                if(isExist)
                {
                    break;
                }
            }
            if(!isExist)
            {
                String[] v = new String[3];
                v[0] = user;
                v[1] = name;
                v[2] = value;
                tableModel.addRow(v);
            }
        }  
    }//GEN-LAST:event_btnAddChangeVariableActionPerformed

    private void btnRemoveVariableActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveVariableActionPerformed
    {//GEN-HEADEREND:event_btnRemoveVariableActionPerformed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int selectedRow = tblVariables.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblVariables.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveVariableActionPerformed

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.DATABASE);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void tblVariablesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblVariablesMousePressed
    {//GEN-HEADEREND:event_tblVariablesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press variable table");
        int row = tblVariables.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblVariables.getValueAt(row, 0).toString();
        cbbUserName.setSelectedItem(user);
        String v = tblVariables.getValueAt(row, 1).toString();
        cbbVariableNames.setSelectedItem(v);
        String value = tblVariables.getValueAt(row, 2).toString();
        if (value.equals("true") || value.equals("false"))
        {
            cbValue.setEnabled(true);
            cbValue.setSelected(Boolean.valueOf(value));
            txfdVariableValue.setEnabled(false);
            txfdVariableValue.setText("");
        } else
        {
            cbValue.setEnabled(false);
            cbValue.setSelected(false);
            txfdVariableValue.setEnabled(true);
            txfdVariableValue.setText(value);
        }
    }//GEN-LAST:event_tblVariablesMousePressed

    private void cbAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbAllActionPerformed
    {//GEN-HEADEREND:event_cbAllActionPerformed
        // TODO add your handling code here:
        if(cbAll.isSelected())
        {            
            cbCreate.setSelected(true);
            cbCreate.setEnabled(false);
            cbTemp.setSelected(true);
            cbTemp.setEnabled(false);  
            cbConnect.setSelected(true);
            cbConnect.setEnabled(false);
            //deprecated following for sm privilege limit
            /*
            if (cbbRole.getSelectedItem().toString().equals("public"))
            {
                cbOptionAll.setEnabled(false);
                cbOptionAll.setSelected(false);
                cbOptionCreate.setEnabled(false);
                cbOptionCreate.setSelected(false);
                cbOptionTemp.setEnabled(false);
                cbOptionTemp.setSelected(false);
                cbOptionConnect.setEnabled(false);
                cbOptionConnect.setSelected(false);
            } 
            else
            {
                cbOptionAll.setEnabled(true);
                cbOptionCreate.setEnabled(false);
                cbOptionTemp.setEnabled(false);
                cbOptionConnect.setEnabled(false);
            }*/
        }
        else
        {            
            cbCreate.setSelected(false);
            cbCreate.setEnabled(true);
            cbTemp.setSelected(false);
            cbTemp.setEnabled(true);
            cbConnect.setSelected(false);
            cbConnect.setEnabled(true);
            //deprecated following for sm privilege limit
            /* if (!cbbRole.getSelectedItem().toString().equals("public"))
            {        
                cbOptionAll.setSelected(false);
                cbOptionAll.setEnabled(false);  
                cbOptionCreate.setSelected(false);
                cbOptionCreate.setEnabled(true);
                cbOptionTemp.setSelected(false);
                cbOptionTemp.setEnabled(true);
                cbOptionConnect.setSelected(false);
                cbOptionConnect.setEnabled(true);
            }*/
        }          
    }//GEN-LAST:event_cbAllActionPerformed

    private void cbbRoleItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbRoleItemStateChanged
    {//GEN-HEADEREND:event_cbbRoleItemStateChanged
        // TODO add your handling code here:
        String role = cbbRole.getSelectedItem().toString();
        logger.info("role:" + role );
        //deprecated following for sm privilege limit
        /*
       if(role.equals("public"))
        {
            cbOptionAll.setEnabled(false);
            cbOptionAll.setSelected(false);
            cbOptionCreate.setEnabled(false);
            cbOptionCreate.setSelected(false);
            cbOptionTemp.setEnabled(false);
            cbOptionTemp.setSelected(false);
            cbOptionConnect.setEnabled(false);
            cbOptionConnect.setSelected(false);
        }
        else
        {
           if(cbAll.isSelected())
           {
               cbOptionAll.setEnabled(true);
           }
           else
           {
                cbOptionAll.setSelected(false);                
                cbOptionAll.setEnabled(false);
                
                cbOptionCreate.setEnabled(true);
                cbOptionTemp.setEnabled(true);
                cbOptionConnect.setEnabled(true);
           }
        }
       */
    }//GEN-LAST:event_cbbRoleItemStateChanged

    private void tblPrivilegesMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblPrivilegesMousePressed
    {//GEN-HEADEREND:event_tblPrivilegesMousePressed
        // TODO add your handling code here:
        logger.info("mouse press privileges table");
        int row = tblPrivileges.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblPrivileges.getValueAt(row, 0).toString();
        cbbRole.setSelectedItem(user);
        String p = tblPrivileges.getValueAt(row, 1).toString();
        cbAll.setSelected(false);
        if(p.contains("C"))
        {
            cbCreate.setSelected(true);
            if(p.contains("C*"))
                cbOptionCreate.setSelected(true);
            else
                cbOptionCreate.setSelected(false);
        }
        else
        {
            cbCreate.setSelected(false);
            cbOptionCreate.setSelected(false);
        }
        if(p.contains("T"))
        {
            cbTemp.setSelected(true);
            if(p.contains("T*"))
                cbOptionTemp.setSelected(true);
            else
                cbOptionTemp.setSelected(false);
        }
        else
        {
            cbTemp.setSelected(false);
            cbOptionTemp.setSelected(false);
        }
        if(p.contains("c"))
        {
            cbConnect.setSelected(true);
            if(p.contains("c*"))
                cbOptionConnect.setSelected(true);
            else
                cbOptionConnect.setSelected(false);
        }
        else
        {
            cbConnect.setSelected(false);
            cbOptionConnect.setSelected(false);
        }
    }//GEN-LAST:event_tblPrivilegesMousePressed

    private void tblSecurityLabelsMousePressed(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tblSecurityLabelsMousePressed
    {//GEN-HEADEREND:event_tblSecurityLabelsMousePressed
        // TODO add your handling code here:
        logger.info("mouse press Security Label table");
        int row = tblSecurityLabels.getSelectedRow();
        logger.info("row = " + row);
        if(row<0)
        {
            return; //nothing select
        }
        String user = tblSecurityLabels.getValueAt(row, 0).toString();
        txfdProvider.setText(user);
        String sl = tblSecurityLabels.getValueAt(row, 1).toString();
        txfdSecurityLabel.setText(sl);
    }//GEN-LAST:event_tblSecurityLabelsMousePressed

    private void cbOptionAllActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbOptionAllActionPerformed
    {//GEN-HEADEREND:event_cbOptionAllActionPerformed
        // TODO add your handling code here:
        if (cbOptionAll.isSelected())
        {
            cbOptionCreate.setSelected(true);
            cbOptionTemp.setSelected(true);            
            cbOptionConnect.setSelected(true);
        }
        else
        {
            cbOptionCreate.setSelected(false);
            cbOptionTemp.setSelected(false);         
            cbOptionConnect.setSelected(false);
        }
    }//GEN-LAST:event_cbOptionAllActionPerformed

    private void cbbVariableNamesItemStateChanged(java.awt.event.ItemEvent evt)//GEN-FIRST:event_cbbVariableNamesItemStateChanged
    {//GEN-HEADEREND:event_cbbVariableNamesItemStateChanged
        // TODO add your handling code here:
        int index = cbbVariableNames.getSelectedIndex();
        if (index < 0)
        {
            return;
        }
        SysVariableInfoDTO variable = (SysVariableInfoDTO) cbbVariableNames.getSelectedItem();
        this.currentVariable = variable;
        boolean isEnableCkb = variable.getValueType().equals("bool");
        cbValue.setEnabled(isEnableCkb);
        cbValue.setSelected(false);
//        cbValue.setVisible(isEnableCkb);
        txfdVariableValue.setEnabled(!isEnableCkb);
        txfdVariableValue.setText("");
//        txfdVariableValue.setVisible(!isEnableCkb);
        txfdVariableValue.addKeyListener(new KeyAdapter()//variable value must >=0 
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                //can only enter digit (>=0)
                variableValueForAddKeyTyped(evt);
            }
        });
    }//GEN-LAST:event_cbbVariableNamesItemStateChanged

    //for add
    private void btnOkEnableForAdd(KeyEvent evt)
    {
        //logger.info("dbName:" + txfdName.getText());
        btnOK.setEnabled(!txfdName.getText().isEmpty());
    }
    private void jtpForAddStateChanged(ChangeEvent evt)                                 
    {             
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:"+index);
        SyntaxController sc = SyntaxController.getInstance();      
        if (index == 5)
        {
            if (txfdName.getText() == null || txfdName.getText().equals(""))
            {
                tpDefinitionSQL.setText("");//clear
                sc.setComment(tpDefinitionSQL.getDocument(), "-- "+constBundle.getString("definitonIncomplete"));
                return;
            }            
            tpDefinitionSQL.setText("");//clear
            DBInfoDTO newDbInfo = getDBInfo();
            TreeController tc = TreeController.getInstance();
            sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(newDbInfo));
        }
    }   
    private void btnOKForAddActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        logger.info(txfdName.getText() + "," + cbbOwner.getSelectedItem().toString());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.createObj(helperInfo, this.getDBInfo());
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error( ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    } 
    
   //for change
    private void btnOkEnableForChange()
    {
        String acl = this.getACL();
        if (txfdName.getText() == null || txfdName.getText().isEmpty())
        {
            logger.info("name is empty");
            btnOK.setEnabled(false);
        } else if (!txfdName.getText().equals(dbInfo.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (cbbOwner.getSelectedItem() != null 
                && !cbbOwner.getSelectedItem().equals(dbInfo.getOwner()))
        {
            logger.info("owner changed");
            btnOK.setEnabled(true);
        } else if (!txarComment.getText().equals(dbInfo.getComment())
                && !(txarComment.getText().isEmpty() && dbInfo.getComment() == null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else if (!acl.equals(dbInfo.getAcl())
                && !(acl.isEmpty() && dbInfo.getAcl() == null))
        {
            logger.info("acl changed");
            btnOK.setEnabled(true);
        } else if (!cbbTablespace.getSelectedItem().equals(dbInfo.getTablespace()))
        {
            logger.info("tablespace changed");
            btnOK.setEnabled(true);
        } else if (!txfdConnectionLimit.getText().trim().equals(dbInfo.getConnectionLimit()))
        {
            logger.info("connection limit changed");
            btnOK.setEnabled(true);
        } else if (!Compare.getInstance().equalVariableList(this.getVariableList(), dbInfo.getVariableList()))
        {
            logger.info("variable changed");
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        logger.info("SelectedTabIndex:" + index);
       
        if (index == 5)
        {
            tpDefinitionSQL.setText("");//clear
             SyntaxController sc = SyntaxController.getInstance();
            if (!btnOK.isEnabled())
            {
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDBChangeSQL(dbInfo, this.getDBInfo()));
            }
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        logger.info(txfdName.getText() + "," + cbbOwner.getSelectedItem().toString());
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getMaintainDB(), tc.getDBChangeSQL(dbInfo, this.getDBInfo()));
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error( ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    } 
    
    //
    private String getACL()
    {
        return AclUtil.getACL(cbbOwner.getSelectedItem().toString(), helperInfo,  dbInfo, (DefaultTableModel) tblPrivileges.getModel());
    }
    private List<VariableInfoDTO> getVariableList()
    {
        List<VariableInfoDTO> variableList = new ArrayList();
        DefaultTableModel variableModel = (DefaultTableModel) tblVariables.getModel();
        int variableRow = variableModel.getRowCount();
        logger.info("variableRow:" + variableRow);
        if (variableRow > 0)
        {
            for (int i = 0; i < variableRow; i++)
            {
                VariableInfoDTO v = new VariableInfoDTO();
                if (variableModel.getValueAt(i, 0) != null)
                {
                    v.setUser(variableModel.getValueAt(i, 0).toString());
                } else
                {
                    v.setUser(null);
                }
                v.setName(variableModel.getValueAt(i, 1).toString());
                v.setValue(variableModel.getValueAt(i, 2).toString());
                variableList.add(v);
            }
        }
        return variableList;
    }
    public DBInfoDTO getDBInfo()
    {
        DBInfoDTO newDB = new DBInfoDTO();
        if (dbInfo != null)
        {
           newDB.setOid(dbInfo.getOid());
           newDB.setDatLastSysOid(dbInfo.getDatLastSysOid());
        }        
        newDB.setHelperInfo(helperInfo);
        newDB.setName(txfdName.getText());
        newDB.setOwner(cbbOwner.getSelectedItem().toString());
        newDB.setComment(txarComment.getText());
        newDB.setEncoding(cbbEncoding.getSelectedItem().toString());
        newDB.setTemplate(cbbTemplate.getSelectedItem().toString());
        newDB.setTablespace(cbbTablespace.getSelectedItem().toString());
        newDB.setCollation(cbbCollation.getSelectedItem().toString());
        newDB.setCharacterType(cbbCharacterType.getSelectedItem().toString());
        newDB.setConnectionLimit(txfdConnectionLimit.getText());
        //for privilege        
        newDB.setAcl(this.getACL());
        //for variable    
        newDB.setVariableList(this.getVariableList());
        //for security label
        /*
        List<SecurityLabelInfoDTO> securityList = new ArrayList();
        DefaultTableModel securityLabelModel = (DefaultTableModel) tblSecurityLabels.getModel();
        int securityLabelRow = securityLabelModel.getRowCount();
        logger.info("SecurityLabelRow:" + securityLabelRow);
        if (securityLabelRow > 0)
        {
            for (int k = 0; k < securityLabelRow; k++)
            {
                SecurityLabelInfoDTO s = new SecurityLabelInfoDTO();
                s.setProvider(securityLabelModel.getValueAt(k, 0).toString());
                s.setSecurityLabel(securityLabelModel.getValueAt(k, 1).toString());
                securityList.add(s);
            }
        }
        newDB.setSecurityLabelList(securityList);
        */
        return newDB;
    }
    public boolean isSucccess()
    {
        return this.isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangePrivileges;
    private javax.swing.JButton btnAddChangeSecurityLabel;
    private javax.swing.JButton btnAddChangeVariable;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemovePrivilege;
    private javax.swing.JButton btnRemoveSecurityLabel;
    private javax.swing.JButton btnRemoveVariable;
    private javax.swing.JCheckBox cbAll;
    private javax.swing.JCheckBox cbConnect;
    private javax.swing.JCheckBox cbCreate;
    private javax.swing.JCheckBox cbOptionAll;
    private javax.swing.JCheckBox cbOptionConnect;
    private javax.swing.JCheckBox cbOptionCreate;
    private javax.swing.JCheckBox cbOptionTemp;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbTemp;
    private javax.swing.JCheckBox cbValue;
    private javax.swing.JComboBox cbbCharacterType;
    private javax.swing.JComboBox cbbCollation;
    private javax.swing.JComboBox cbbEncoding;
    private javax.swing.JComboBox cbbOwner;
    private javax.swing.JComboBox cbbRole;
    private javax.swing.JComboBox cbbTablespace;
    private javax.swing.JComboBox cbbTemplate;
    private javax.swing.JComboBox cbbUserName;
    private javax.swing.JComboBox cbbVariableNames;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblCharacterType;
    private javax.swing.JLabel lblCollation;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblConnectionLimit;
    private javax.swing.JLabel lblEncoding;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblOwner;
    private javax.swing.JLabel lblProvider;
    private javax.swing.JLabel lblRole;
    private javax.swing.JLabel lblSecurityLabel;
    private javax.swing.JLabel lblTablesapce;
    private javax.swing.JLabel lblTemplate;
    private javax.swing.JLabel lblUserName;
    private javax.swing.JLabel lblVariableName;
    private javax.swing.JLabel lblVariableValue;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlPrivileges;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JPanel pnlSecurityLabel;
    private javax.swing.JPanel pnlVariables;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblPrivileges;
    private javax.swing.JTable tblSecurityLabels;
    private javax.swing.JTable tblVariables;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea txarComment;
    private javax.swing.JTextField txfdConnectionLimit;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    private javax.swing.JTextField txfdProvider;
    private javax.swing.JTextField txfdSecurityLabel;
    private javax.swing.JTextField txfdVariableValue;
    // End of variables declaration//GEN-END:variables
}
