/* ------------------------------------------------ 
* 
* File: TriggerView.java
*
* Abstract: 
* 		触发器操作窗口.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\TriggerView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.ColItemInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.TriggerInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class TriggerView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    
    private HelperInfoDTO helperInfo;
    private boolean isSuccess;
    private TriggerInfoDTO triggerInfo;
   
    public TriggerView(JFrame parent, boolean modal, HelperInfoDTO helperInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.initComponents();
        isSuccess = false;
        this.setTitle(constBundle.getString("addTrigger"));
        this.setNormalProperty();

        //define action
        txfdName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                btnOkEnableForAddAction();
            }
        });
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForAddStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForAddActionPerformed(e);
            }
        });
    }

    public TriggerView(JFrame parent, boolean modal, TriggerInfoDTO triggerInfo)
    {
        super(parent, modal);
        this.triggerInfo = triggerInfo;
        helperInfo = triggerInfo.getHelperInfo();
        this.initComponents();
        this.setTitle(constBundle.getString("triggers") + " " + triggerInfo.getName());
        this.setNormalProperty();
        this.setProperty();

        //define action
        KeyAdapter btnOkEnableAdapter = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableForChangeAction();
            }
        };
        txfdName.addKeyListener(btnOkEnableAdapter);
        ttarComment.addKeyListener(btnOkEnableAdapter);
        jtp.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                jtpForChangeStateChanged(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKForChangeActionPerformed(e);
            }
        });
    }
    
    
    private void setProperty()
    {
        txfdName.setText(triggerInfo.getName());
        txfdOID.setText(triggerInfo.getOid().toString());
        ttarComment.setText(triggerInfo.getComment());

        cbRowTrigger.setSelected(triggerInfo.isRowTrigger());
        cbRowTrigger.setEnabled(false);
        cbConstraintTrigger.setSelected(triggerInfo.isConstraintTrigger());
        cbConstraintTrigger.setEnabled(false);
        cbDeferrable.setSelected(triggerInfo.isDeferrable());
        cbDeferrable.setEnabled(false);
        cbDeferred.setSelected(triggerInfo.isDeferred());
        cbDeferred.setEnabled(false);
        cbbTriggerFunction.setSelectedItem(triggerInfo.getTriggerFunction());
        cbbTriggerFunction.setEnabled(false);
        txfdArguments.setText(triggerInfo.getArguments());
        txfdArguments.setEnabled(false);
        switch (triggerInfo.getFire())
        {
            case "BEFORE":
                rbFireBefore.setSelected(true);
                break;
            case "AFTER":
                rbFireAfter.setSelected(true);
                break;
            case "INSTEAD OF":
                rbFireInsteadOf.setSelected(true);
                break;
        }
        rbFireBefore.setEnabled(false);
        rbFireAfter.setEnabled(false);
        rbFireInsteadOf.setEnabled(false);
        List<String> eventList = triggerInfo.getEventList();
        if (eventList != null)
        {
            for (String event : eventList)
            {
                switch (event)
                {
                    case "INSERT":
                        cbInsert.setSelected(true);
                        break;
                    case "UPDATE":
                        cbUpdate.setSelected(true);
                        break;
                    case "DELETE":
                        cbDelete.setSelected(true);
                        break;
                    case "TRUNCATE":
                        cbTruncate.setSelected(true);
                        break;
                }
            }
        }
        cbInsert.setEnabled(false);
        cbUpdate.setEnabled(false);
        cbDelete.setEnabled(false);
        cbTruncate.setEnabled(false);
        ttarWhenCondition.setText(triggerInfo.getWhen());
        ttarWhenCondition.setEnabled(false);
        List<String> triggerList = triggerInfo.getColumnList();
        if (triggerList != null)
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            for (String trigger : triggerList)
            {
                String[] t = new String[1];
                t[0] = trigger;
                tableModel.addRow(t);
            }
        }
        //ttarBody.setText();
        ttarBody.setEnabled(false);
    }

    private void setNormalProperty()
    {
        TreeController tc = TreeController.getInstance();
        try
        {
            ColItemInfoDTO[] columns = tc.getColumnOfRelation(helperInfo, helperInfo.getRelationOid());
            cbbColumn.setModel(new DefaultComboBoxModel(columns));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        if (helperInfo.getType().equals(TreeEnum.TreeNode.TABLE))
        {
            rbFireInsteadOf.setEnabled(false);//DEFAULT IS ENABLE.
        }
        try
        {
            cbbTriggerFunction.setModel(new DefaultComboBoxModel(tc.getTriggerFunctions(helperInfo)));
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        cbbTriggerFunction.setSelectedIndex(-1);

        ActionListener eventsActionListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnOkEnableForAddAction();
            }
        };
        cbbTriggerFunction.addActionListener(eventsActionListener);
        cbInsert.addActionListener(eventsActionListener);
        cbUpdate.addActionListener(eventsActionListener);
        cbDelete.addActionListener(eventsActionListener);
        cbConstraintTrigger.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                cbConstraintTriggerActionPerformed(evt);
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        rbGroupFires = new javax.swing.ButtonGroup();
        jtp = new javax.swing.JTabbedPane();
        pnlProperties = new javax.swing.JPanel();
        lblName = new javax.swing.JLabel();
        lblComment = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        cbbUseSlony = new javax.swing.JComboBox();
        lblUseSlony = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ttarComment = new javax.swing.JTextArea();
        txfdOID = new javax.swing.JTextField();
        lblOID = new javax.swing.JLabel();
        pnlDefinition = new javax.swing.JPanel();
        lblTriggerFunction = new javax.swing.JLabel();
        lblArguments = new javax.swing.JLabel();
        lblWhen = new javax.swing.JLabel();
        lblDeferred = new javax.swing.JLabel();
        txfdArguments = new javax.swing.JTextField();
        cbbTriggerFunction = new javax.swing.JComboBox();
        cbDeferred = new javax.swing.JCheckBox();
        lblEvents = new javax.swing.JLabel();
        cbInsert = new javax.swing.JCheckBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        ttarWhenCondition = new javax.swing.JTextArea();
        lblDeferrable = new javax.swing.JLabel();
        cbDeferrable = new javax.swing.JCheckBox();
        cbUpdate = new javax.swing.JCheckBox();
        cbTruncate = new javax.swing.JCheckBox();
        cbDelete = new javax.swing.JCheckBox();
        lblConcurrentBuild1 = new javax.swing.JLabel();
        rbFireBefore = new javax.swing.JRadioButton();
        rbFireAfter = new javax.swing.JRadioButton();
        rbFireInsteadOf = new javax.swing.JRadioButton();
        lblRowTrigger = new javax.swing.JLabel();
        lblConstraintTrigger = new javax.swing.JLabel();
        cbRowTrigger = new javax.swing.JCheckBox();
        cbConstraintTrigger = new javax.swing.JCheckBox();
        pnlColumns = new javax.swing.JPanel();
        spColumns = new javax.swing.JScrollPane();
        tblColumns = new javax.swing.JTable();
        btnAddChangeColumn = new javax.swing.JButton();
        btnRemoveColumn = new javax.swing.JButton();
        cbbColumn = new javax.swing.JComboBox();
        lblColumn = new javax.swing.JLabel();
        pnlBody = new javax.swing.JPanel();
        ttarBody = new javax.swing.JTextArea();
        pnlSQL = new javax.swing.JPanel();
        cbReadOnly = new javax.swing.JCheckBox();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpDefinitionSQL = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("addTrigger"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/trigger.png")));
setMinimumSize(new java.awt.Dimension(500, 500));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setBackground(new java.awt.Color(255, 255, 255));
jtp.setMinimumSize(new java.awt.Dimension(500, 440));
jtp.setPreferredSize(new java.awt.Dimension(500, 440));

pnlProperties.setBackground(new java.awt.Color(255, 255, 255));
pnlProperties.setMinimumSize(new java.awt.Dimension(500, 420));
pnlProperties.setPreferredSize(new java.awt.Dimension(500, 420));

lblName.setText(constBundle.getString("name"));

lblComment.setText(constBundle.getString("comment"));

txfdName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));
txfdName.setMinimumSize(new java.awt.Dimension(5, 19));
txfdName.setPreferredSize(new java.awt.Dimension(5, 19));

cbbUseSlony.setBackground(new java.awt.Color(240, 240, 240));
cbbUseSlony.setEditable(true);
cbbUseSlony.setEnabled(false);

lblUseSlony.setText(constBundle.getString("useSlony"));

ttarComment.setColumns(20);
ttarComment.setRows(5);
ttarComment.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));
jScrollPane1.setViewportView(ttarComment);

txfdOID.setBorder(javax.swing.BorderFactory.createEtchedBorder());
txfdOID.setEnabled(false);
txfdOID.setMinimumSize(new java.awt.Dimension(5, 19));
txfdOID.setPreferredSize(new java.awt.Dimension(5, 19));

lblOID.setText("OID");

javax.swing.GroupLayout pnlPropertiesLayout = new javax.swing.GroupLayout(pnlProperties);
pnlProperties.setLayout(pnlPropertiesLayout);
pnlPropertiesLayout.setHorizontalGroup(
    pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlPropertiesLayout.createSequentialGroup()
        .addContainerGap()
        .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlPropertiesLayout.createSequentialGroup()
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblComment, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblOID, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txfdName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addComponent(txfdOID, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(pnlPropertiesLayout.createSequentialGroup()
                .addComponent(lblUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbUseSlony, 0, 392, Short.MAX_VALUE)))
        .addContainerGap())
    );
    pnlPropertiesLayout.setVerticalGroup(
        pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlPropertiesLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(lblName)
                .addGroup(pnlPropertiesLayout.createSequentialGroup()
                    .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txfdOID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblOID))))
            .addGap(10, 10, 10)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(lblComment))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlPropertiesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblUseSlony)
                .addComponent(cbbUseSlony, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("property"), pnlProperties);

    pnlDefinition.setBackground(new java.awt.Color(255, 255, 255));
    pnlDefinition.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlDefinition.setPreferredSize(new java.awt.Dimension(500, 420));

    lblTriggerFunction.setText(constBundle.getString("triggerFunctions"));

    lblArguments.setText(constBundle.getString("arguments"));

    lblWhen.setText(constBundle.getString("when"));

    lblDeferred.setText(constBundle.getString("deferred"));

    txfdArguments.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));
    txfdArguments.setPreferredSize(new java.awt.Dimension(5, 19));

    cbbTriggerFunction.setEditable(true);
    cbbTriggerFunction.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));

    cbDeferred.setBackground(new java.awt.Color(255, 255, 255));
    cbDeferred.setEnabled(false);

    lblEvents.setText(constBundle.getString("event"));

    cbInsert.setBackground(new java.awt.Color(255, 255, 255));
    cbInsert.setText("INSERT");

    ttarWhenCondition.setColumns(20);
    ttarWhenCondition.setRows(5);
    ttarWhenCondition.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));
    jScrollPane2.setViewportView(ttarWhenCondition);

    lblDeferrable.setText(constBundle.getString("deferrable"));

    cbDeferrable.setBackground(new java.awt.Color(255, 255, 255));
    cbDeferrable.setEnabled(false);

    cbUpdate.setBackground(new java.awt.Color(255, 255, 255));
    cbUpdate.setText("UPDATE");
    cbUpdate.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbUpdateActionPerformed(evt);
        }
    });

    cbTruncate.setBackground(new java.awt.Color(255, 255, 255));
    cbTruncate.setText("TRUNCATE");

    cbDelete.setBackground(new java.awt.Color(255, 255, 255));
    cbDelete.setText("DELETE");

    lblConcurrentBuild1.setText(constBundle.getString("fires"));

    rbFireBefore.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupFires.add(rbFireBefore);
    rbFireBefore.setSelected(true);
    rbFireBefore.setText("BEFORE");

    rbFireAfter.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupFires.add(rbFireAfter);
    rbFireAfter.setText("AFTER");

    rbFireInsteadOf.setBackground(new java.awt.Color(255, 255, 255));
    rbGroupFires.add(rbFireInsteadOf);
    rbFireInsteadOf.setText("INSTEAD OF");

    lblRowTrigger.setText(constBundle.getString("rowTrigger"));

    lblConstraintTrigger.setText(constBundle.getString("constraintTrigger"));

    cbRowTrigger.setBackground(new java.awt.Color(255, 255, 255));
    cbRowTrigger.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbRowTriggerActionPerformed(evt);
        }
    });

    cbConstraintTrigger.setBackground(new java.awt.Color(255, 255, 255));

    javax.swing.GroupLayout pnlDefinitionLayout = new javax.swing.GroupLayout(pnlDefinition);
    pnlDefinition.setLayout(pnlDefinitionLayout);
    pnlDefinitionLayout.setHorizontalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lblTriggerFunction, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblArguments, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(8, 8, 8)
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txfdArguments, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbbTriggerFunction, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblWhen, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jScrollPane2))
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblEvents, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(cbInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblDeferred, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(cbDeferred))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblDeferrable, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(cbDeferrable))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblConstraintTrigger, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(cbConstraintTrigger))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblRowTrigger, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(cbRowTrigger))
                        .addGroup(pnlDefinitionLayout.createSequentialGroup()
                            .addComponent(lblConcurrentBuild1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(8, 8, 8)
                            .addComponent(rbFireBefore, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(10, 10, 10)
                            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(cbUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
                                .addComponent(rbFireAfter, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(10, 10, 10)
                            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(rbFireInsteadOf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(10, 10, 10)
                            .addComponent(cbTruncate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGap(29, 29, 29)))
            .addContainerGap())
    );
    pnlDefinitionLayout.setVerticalGroup(
        pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDefinitionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbRowTrigger, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblRowTrigger, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbConstraintTrigger, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblConstraintTrigger, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbDeferrable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDeferrable, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(cbDeferred, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblDeferred, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblTriggerFunction)
                .addComponent(cbbTriggerFunction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(lblArguments, javax.swing.GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                .addComponent(txfdArguments, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(rbFireAfter, javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(rbFireInsteadOf, javax.swing.GroupLayout.Alignment.TRAILING)
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblConcurrentBuild1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbFireBefore)))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblEvents, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2))
                .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbInsert, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbTruncate, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGap(8, 8, 8)
            .addGroup(pnlDefinitionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                .addGroup(pnlDefinitionLayout.createSequentialGroup()
                    .addComponent(lblWhen, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("definition"), pnlDefinition);

    pnlColumns.setBackground(new java.awt.Color(255, 255, 255));
    pnlColumns.setEnabled(false);
    pnlColumns.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlColumns.setOpaque(false);
    pnlColumns.setPreferredSize(new java.awt.Dimension(500, 420));

    tblColumns.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            ""
        }
    )
    {
        Class[] types = new Class []
        {
            java.lang.String.class
        };

        public Class getColumnClass(int columnIndex)
        {
            return types [columnIndex];
        }
    });
    tblColumns.setGridColor(new java.awt.Color(255, 255, 255));
    spColumns.setViewportView(tblColumns);
    if (tblColumns.getColumnModel().getColumnCount() > 0)
    {
        tblColumns.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("columnName"));
    }

    btnAddChangeColumn.setText(constBundle.getString("add")+"/"+constBundle.getString("change"));
    btnAddChangeColumn.setEnabled(false);
    btnAddChangeColumn.setMaximumSize(new java.awt.Dimension(80, 23));
    btnAddChangeColumn.setMinimumSize(new java.awt.Dimension(80, 23));
    btnAddChangeColumn.setPreferredSize(new java.awt.Dimension(80, 23));
    btnAddChangeColumn.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAddChangeColumnActionPerformed(evt);
        }
    });

    btnRemoveColumn.setText(constBundle.getString("remove"));
    btnRemoveColumn.setEnabled(false);
    btnRemoveColumn.setMaximumSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.setMinimumSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.setPreferredSize(new java.awt.Dimension(80, 23));
    btnRemoveColumn.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveColumnActionPerformed(evt);
        }
    });

    cbbColumn.setEnabled(false);

    lblColumn.setText(constBundle.getString("column"));
    lblColumn.setEnabled(false);

    javax.swing.GroupLayout pnlColumnsLayout = new javax.swing.GroupLayout(pnlColumns);
    pnlColumns.setLayout(pnlColumnsLayout);
    pnlColumnsLayout.setHorizontalGroup(
        pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlColumnsLayout.createSequentialGroup()
                    .addComponent(spColumns, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
                    .addGap(10, 10, 10))
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlColumnsLayout.createSequentialGroup()
                    .addComponent(lblColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(cbbColumn, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap())))
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addGap(12, 12, 12)
            .addComponent(btnAddChangeColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE))
    );
    pnlColumnsLayout.setVerticalGroup(
        pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlColumnsLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(spColumns, javax.swing.GroupLayout.DEFAULT_SIZE, 336, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAddChangeColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnRemoveColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblColumn)
                .addComponent(cbbColumn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("columns"), pnlColumns);
    pnlColumns.getAccessibleContext().setAccessibleName("");

    pnlBody.setBackground(new java.awt.Color(255, 255, 255));
    pnlBody.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlBody.setOpaque(false);

    ttarBody.setColumns(20);
    ttarBody.setForeground(new java.awt.Color(0, 0, 124));
    ttarBody.setRows(5);
    ttarBody.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(221, 236, 251), null, null));

    javax.swing.GroupLayout pnlBodyLayout = new javax.swing.GroupLayout(pnlBody);
    pnlBody.setLayout(pnlBodyLayout);
    pnlBodyLayout.setHorizontalGroup(
        pnlBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 500, Short.MAX_VALUE)
        .addGroup(pnlBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBodyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ttarBody, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
                .addContainerGap()))
    );
    pnlBodyLayout.setVerticalGroup(
        pnlBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 420, Short.MAX_VALUE)
        .addGroup(pnlBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBodyLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ttarBody, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                .addContainerGap()))
    );

    jtp.addTab(constBundle.getString("body"), pnlBody);

    pnlSQL.setBackground(new java.awt.Color(255, 255, 255));
    pnlSQL.setMinimumSize(new java.awt.Dimension(500, 420));
    pnlSQL.setPreferredSize(new java.awt.Dimension(500, 420));

    cbReadOnly.setBackground(new java.awt.Color(255, 255, 255));
    cbReadOnly.setSelected(true);
    cbReadOnly.setText(constBundle.getString("readOnly"));
    cbReadOnly.setActionCommand("");
    cbReadOnly.setEnabled(false);

    tpDefinitionSQL.setEditable(false);
    spDefinationSQL.setViewportView(tpDefinitionSQL);

    javax.swing.GroupLayout pnlSQLLayout = new javax.swing.GroupLayout(pnlSQL);
    pnlSQL.setLayout(pnlSQLLayout);
    pnlSQLLayout.setHorizontalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(0, 0, Short.MAX_VALUE))
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
            .addContainerGap())
    );
    pnlSQLLayout.setVerticalGroup(
        pnlSQLLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSQLLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbReadOnly)
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 371, Short.MAX_VALUE)
            .addContainerGap())
    );

    jtp.addTab("SQL", pnlSQL);

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 245, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbConstraintTriggerActionPerformed(ActionEvent evt)
    {
        boolean enable = cbConstraintTrigger.isSelected();
        cbRowTrigger.setEnabled(!enable);
        
        cbDeferrable.setEnabled(enable);
        cbDeferred.setEnabled(enable);
        
        rbFireBefore.setEnabled(!enable);
        rbFireAfter.setEnabled(!enable);
        if (!helperInfo.getType().equals(TreeEnum.TreeNode.TABLE))
        {
            rbFireInsteadOf.setEnabled(!enable);
        }
        if (enable)
        {
            cbRowTrigger.setSelected(true);
            rbFireAfter.setSelected(true);
        }
    }

    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showObjHelpPage(this, TreeEnum.TreeNode.TRIGGER);
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void cbRowTriggerActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbRowTriggerActionPerformed
    {//GEN-HEADEREND:event_cbRowTriggerActionPerformed
        // TODO add your handling code here:
        cbTruncate.setEnabled(!cbRowTrigger.isSelected());
        cbTruncate.setSelected(false);
    }//GEN-LAST:event_cbRowTriggerActionPerformed

    private void cbUpdateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbUpdateActionPerformed
    {//GEN-HEADEREND:event_cbUpdateActionPerformed
        // TODO add your handling code here:
        boolean enable = cbUpdate.isSelected();
        logger.info("Update isSelected = " + enable);
        if (!rbFireInsteadOf.isSelected())
        {
            cbbColumn.setEnabled(enable);
            btnAddChangeColumn.setEnabled(enable);
            btnRemoveColumn.setEnabled(enable);
        }
    }//GEN-LAST:event_cbUpdateActionPerformed

    private void btnAddChangeColumnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAddChangeColumnActionPerformed
    {//GEN-HEADEREND:event_btnAddChangeColumnActionPerformed
        // TODO add your handling code here:
        logger.info("add column");
        String columnName = cbbColumn.getSelectedItem().toString();
        if (columnName == null || columnName.isEmpty())
        {
            //
        } else
        {
            DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
            int row = tableModel.getRowCount();
            boolean isExist = false;
            for (int i = 0; i < row; i++)
            {
                String nameOld = tableModel.getValueAt(i, 0).toString();
                logger.info("nameOld=" + nameOld + nameOld.equals(columnName));
                if (nameOld.equals(columnName))
                {
                    isExist = true;
                    break;
                }
            }
            if (!isExist)
            {
                String[] v = new String[1];
                v[0] = columnName;
                tableModel.addRow(v);
            }
        }
    }//GEN-LAST:event_btnAddChangeColumnActionPerformed

    private void btnRemoveColumnActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveColumnActionPerformed
    {//GEN-HEADEREND:event_btnRemoveColumnActionPerformed
        // TODO add your handling code here:
        logger.info("remove row");
        int selectedRow = tblColumns.getSelectedRow();
        logger.info("selectedRow = " + selectedRow);
        if (selectedRow < 0)
        {
            return; //nothing select
        }
        DefaultTableModel tableModel = (DefaultTableModel) tblColumns.getModel();
        tableModel.removeRow(selectedRow);
    }//GEN-LAST:event_btnRemoveColumnActionPerformed
   
    
    //for add
    private void btnOkEnableForAddAction()
    {
        String triggerName = txfdName.getText();
        //logger.info("TriggerName:" + triggerName + ",triggerFunctionIndex:" + cbbTriggerFunction.getSelectedIndex());
        if (triggerName == null || triggerName.isEmpty()
                || cbbTriggerFunction.getSelectedIndex() < 0
                || (!cbInsert.isSelected() && !cbUpdate.isSelected() && !cbDelete.isSelected() && !cbTruncate.isSelected()))
        {
            btnOK.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
        }
    }
    private void jtpForAddStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        SyntaxController sc = SyntaxController.getInstance();  
        logger.info("SelectedTab:" + index);
        if (index == 4)
        {
            tpDefinitionSQL.setText("");//clear
            if (txfdName.getText() == null || txfdName.getText().isEmpty()
                    || cbbTriggerFunction.getSelectedIndex() < 0
                    || (!cbInsert.isSelected() && !cbUpdate.isSelected()
                    && !cbDelete.isSelected() && !cbTruncate.isSelected()))
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitonIncomplete"));
            } else
            {
                TreeController tc = TreeController.getInstance();
                tpDefinitionSQL.setText("");
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getDefinitionSQL(this.getTriggerInfo()));
            }
        }
    }
    private void btnOKForAddActionPerformed(ActionEvent evt)
    {
        String name = txfdName.getText();
        logger.info("index:" + name);
        triggerInfo = this.getTriggerInfo();
        TreeController tc = TreeController.getInstance();
        try
        {
            tc.createObj(helperInfo, triggerInfo);
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }    
    
    //for change
    private void btnOkEnableForChangeAction()
    {
        String triggerName = txfdName.getText();
        if (triggerName == null || triggerName.isEmpty())
        {
            btnOK.setEnabled(false);
        } else if(!txfdName.getText().equals(triggerInfo.getName()))
        {
            logger.info("name changed");
            btnOK.setEnabled(true);
        } else if (!ttarComment.getText().equals(triggerInfo.getComment())
                && !(ttarComment.getText().isEmpty() && triggerInfo.getComment() == null))
        {
            logger.info("comment changed");
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
    private void jtpForChangeStateChanged(ChangeEvent evt)
    {
        int index = jtp.getSelectedIndex();
        SyntaxController sc = SyntaxController.getInstance();
        logger.info("SelectedTab:" + index);
        if (index == 4)
        {
            tpDefinitionSQL.setText("");//clear
            if (!btnOK.isEnabled())
            {
                tpDefinitionSQL.setText("");
                sc.setComment(tpDefinitionSQL.getDocument(), "-- " + constBundle.getString("definitionNoChange"));
            } else
            {
                tpDefinitionSQL.setText("");
                TreeController tc = TreeController.getInstance();
                sc.setDefine(tpDefinitionSQL.getDocument(), tc.getTriggerChangeSQL(this.triggerInfo, this.getTriggerInfo()));
            }
        }
    }
    private void btnOKForChangeActionPerformed(ActionEvent evt)
    {
        String name = txfdName.getText();
        logger.info("index:" + name);
        try
        {
            TreeController tc = TreeController.getInstance();
            tc.executeSql(helperInfo, helperInfo.getDbName(), tc.getTriggerChangeSQL(triggerInfo, this.getTriggerInfo()));
            isSuccess = true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    
    
    public TriggerInfoDTO getTriggerInfo()
    {
        TriggerInfoDTO newTrigger = new TriggerInfoDTO();
        if (triggerInfo != null)
        {
            newTrigger.setOid(triggerInfo.getOid());
        }
        newTrigger.setHelperInfo(helperInfo);
        switch (helperInfo.getType())
        {
            case TABLE:
                newTrigger.setParent("TABLE");
                break;
            case VIEW:
                newTrigger.setParent("VIEW");
                break;
        }
        newTrigger.setName(txfdName.getText());
        newTrigger.setComment(ttarComment.getText());
        newTrigger.setRowTrigger(cbRowTrigger.isSelected());
        newTrigger.setConstraintTrigger(cbConstraintTrigger.isSelected());
        newTrigger.setDeferrable(cbDeferrable.isSelected());
        newTrigger.setDeferred(cbDeferred.isSelected());
        logger.info("cbbTriggerFunction;" + cbbTriggerFunction.getSelectedItem().toString());
        newTrigger.setTriggerFunWithoutArg(cbbTriggerFunction.getSelectedItem().toString());
        newTrigger.setArguments(txfdArguments.getText());
        if (rbFireBefore.isSelected())
        {
            newTrigger.setFire("BEFORE");
        } else if (rbFireAfter.isSelected())
        {
            newTrigger.setFire("AFTER");
        } else if (rbFireInsteadOf.isSelected())
        {
            newTrigger.setFire("INSTEAD OF");
        }
        List<String> eventList = new ArrayList();
        if (cbInsert.isSelected())
        {
            eventList.add("INSERT");
        }
        if (cbUpdate.isSelected())
        {
            eventList.add("UPDATE");
        }
        if (cbDelete.isSelected())
        {
            eventList.add("DELETE");
        }
        if (cbTruncate.isSelected())
        {
            eventList.add("TRUNCATE");
        }
        newTrigger.setEventList(eventList);

        if (cbUpdate.isSelected())
        {
            if (tblColumns.getRowCount() > 0)
            {
                DefaultTableModel tblModel = (DefaultTableModel) tblColumns.getModel();
                List<String> colList = new ArrayList();
                for (int i = 0; i < tblModel.getRowCount(); i++)
                {
                    colList.add(tblModel.getValueAt(i, 0).toString());
                }
                newTrigger.setColumnList(colList);
            }
        }
        newTrigger.setWhen(ttarWhenCondition.getText());

        return newTrigger;
    }

    public boolean isSuccess()
    {
        return isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddChangeColumn;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemoveColumn;
    private javax.swing.JCheckBox cbConstraintTrigger;
    private javax.swing.JCheckBox cbDeferrable;
    private javax.swing.JCheckBox cbDeferred;
    private javax.swing.JCheckBox cbDelete;
    private javax.swing.JCheckBox cbInsert;
    private javax.swing.JCheckBox cbReadOnly;
    private javax.swing.JCheckBox cbRowTrigger;
    private javax.swing.JCheckBox cbTruncate;
    private javax.swing.JCheckBox cbUpdate;
    private javax.swing.JComboBox cbbColumn;
    private javax.swing.JComboBox cbbTriggerFunction;
    private javax.swing.JComboBox cbbUseSlony;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblArguments;
    private javax.swing.JLabel lblColumn;
    private javax.swing.JLabel lblComment;
    private javax.swing.JLabel lblConcurrentBuild1;
    private javax.swing.JLabel lblConstraintTrigger;
    private javax.swing.JLabel lblDeferrable;
    private javax.swing.JLabel lblDeferred;
    private javax.swing.JLabel lblEvents;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblOID;
    private javax.swing.JLabel lblRowTrigger;
    private javax.swing.JLabel lblTriggerFunction;
    private javax.swing.JLabel lblUseSlony;
    private javax.swing.JLabel lblWhen;
    private javax.swing.JPanel pnlBody;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlColumns;
    private javax.swing.JPanel pnlDefinition;
    private javax.swing.JPanel pnlProperties;
    private javax.swing.JPanel pnlSQL;
    private javax.swing.JRadioButton rbFireAfter;
    private javax.swing.JRadioButton rbFireBefore;
    private javax.swing.JRadioButton rbFireInsteadOf;
    private javax.swing.ButtonGroup rbGroupFires;
    private javax.swing.JScrollPane spColumns;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JTable tblColumns;
    private javax.swing.JTextPane tpDefinitionSQL;
    private javax.swing.JTextArea ttarBody;
    private javax.swing.JTextArea ttarComment;
    private javax.swing.JTextArea ttarWhenCondition;
    private javax.swing.JTextField txfdArguments;
    private javax.swing.JTextField txfdName;
    private javax.swing.JTextField txfdOID;
    // End of variables declaration//GEN-END:variables

}
