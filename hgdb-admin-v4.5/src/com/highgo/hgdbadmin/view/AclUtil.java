/* ------------------------------------------------ 
* 
* File: AclUtil.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\view\AclUtil.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.FunctionInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.SchemaInfoDTO;
import com.highgo.hgdbadmin.model.SequenceInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.TablespaceInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class AclUtil
{
    private static Logger logger = LogManager.getLogger(AclUtil.class);
    
    public static String getACL( String owner, HelperInfoDTO helperInfo, Object objInfo, DefaultTableModel privilegeModel)
    {
        StringBuilder acl = new StringBuilder();
        
        // String owner = cbbOwner.getSelectedItem().toString();
        if (owner == null || owner.isEmpty())
        {
            owner = helperInfo.getUser();
        }

        acl.append("{");
        //revoke(for change)        
        int i = 0;
        if (objInfo != null)
        {
            String oldAcl = null;
            if (objInfo instanceof TablespaceInfoDTO)
            {
                oldAcl = ((TablespaceInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof DBInfoDTO)
            {
                oldAcl = ((DBInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof SchemaInfoDTO)
            {
                oldAcl = ((SchemaInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof TableInfoDTO)
            {
                oldAcl = ((TableInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof ColumnInfoDTO)
            {
                oldAcl = ((ColumnInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof ViewInfoDTO)
            {
                oldAcl = ((ViewInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof SequenceInfoDTO)
            {
                oldAcl = ((SequenceInfoDTO) objInfo).getAcl();
            } else if (objInfo instanceof FunctionInfoDTO)
            {
                oldAcl = ((FunctionInfoDTO) objInfo).getAcl();
            } else
            {
                logger.error("unsupported obj type.");
            }
            
            logger.debug("oldAcl=" + oldAcl);
            if (oldAcl != null && !oldAcl.isEmpty() && !oldAcl.equals("{}"))
            {                
                oldAcl = oldAcl.substring(1, oldAcl.length() - 1);
                String[] oldArray = oldAcl.split(",");
                String or;
                for (String s : oldArray)
                {
                    if (i > 0)
                    {
                        acl.append(",");
                    }
                    or = s.split("=")[0];
                    acl.append(or)
                            .append("=").append("")
                            .append("/").append(owner);

                    i++;
                }
            }
        }
        logger.debug(acl.toString());
        //revoke
        //grant
        //DefaultTableModel privilegeModel = (DefaultTableModel) tblPrivileges.getModel();
        int privilegeRow = privilegeModel.getRowCount();
        logger.info("privilegeRow=" + privilegeRow);
        if (privilegeRow > 0)
        {            
            String r;
            for (int j = 0; j < privilegeRow; j++)
            {
                if (i > 0)
                {
                    acl.append(",");
                }
                r = privilegeModel.getValueAt(j, 0).toString();
                acl.append(r.equals("public") ? "" : r)
                        .append("=").append(privilegeModel.getValueAt(j, 1).toString())
                        .append("/").append(owner);
            } 
        }   
        //grant
        acl.append("}");
        
        logger.info("Return: acl=" + acl.toString());
        return acl.toString();
    }
}
