/* ------------------------------------------------ 
* 
* File: StatisticsView.java
*
* Abstract: 
* 		统计量信息tab.
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*		src\com\highgo\hgdbadmin\view\StatisticsView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.view;

import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.event.TreeNodeChangeEvent;
import com.highgo.hgdbadmin.event.TreeNodeChangeEventHandler;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.FunctionInfoDTO;
import com.highgo.hgdbadmin.model.IndexInfoDTO;
import com.highgo.hgdbadmin.model.SequenceInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.TablespaceInfoDTO;
import com.highgo.hgdbadmin.model.ViewColumnInfoDTO;
import com.highgo.hgdbadmin.util.event.SimpleEventBus;
import java.awt.Color;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class StatisticsView extends JTable
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private SimpleEventBus eventBus;

    public StatisticsView(SimpleEventBus eventBus)
    {
        this.eventBus = eventBus;
        this.setShowGrid(false);
        this.setBackground(new Color(240, 240, 240));
        this.getTableHeader().setReorderingAllowed(false);
        if (this.getModel() != null)
        {
            this.removeAll();
        }
        this.addHandler();
    }

    private void addHandler()
    {
        eventBus.addHandler(TreeNodeChangeEvent.TYPE, new TreeNodeChangeEventHandler()
        {
            @Override
            public void onChanged(TreeNodeChangeEvent evt)
            {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) evt.getNodeObject();
                logger.info("CurrentNode = " + node);
                if(node == null)
                {
                    return;
                }
                populateData(node.getUserObject());
            }
        });
    }

    private void populateData(Object obj)
    {
        if (obj == null)
        {
            return;
        }
        DefaultTableModel model;
        if (obj instanceof TablespaceInfoDTO
                || obj instanceof DBInfoDTO
                || obj instanceof SequenceInfoDTO
                || obj instanceof TableInfoDTO
                || obj instanceof ColumnInfoDTO
                || obj instanceof ViewColumnInfoDTO
                || obj instanceof IndexInfoDTO 
                || obj instanceof FunctionInfoDTO)
        {
            String[] headers = new String[]
            {
                constBundle.getString("statistics"),
                constBundle.getString("value")
            };
            model = new DefaultTableModel(headers, 0)
            {
                Class[] types = new Class[]
                {
                    String.class, String.class
                };
                boolean[] canEdit = new boolean[]
                {
                    false, false
                };
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
           };
           this.setModel(model);
           try
           {
               TreeController.getInstance().getStatisticsList((AbstractObject) obj, model);
           } catch (Exception ex)
           {
               JOptionPane.showMessageDialog(null, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
           }
       } else
       {
            String[] headers = new String[]
            {
                constBundle.getString("statistics"),
                ""
            };
            model = new DefaultTableModel(headers, 0)
            {
                Class[] types = new Class[]
                {
                    String.class, String.class
                };
                boolean[] canEdit = new boolean[]
                {
                    false, false
                };
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
            };
            this.setModel(model);
            model.addRow(new String[]
            {
                constBundle.getString("noStatistics"), ""
            });
        }
    }    
}
