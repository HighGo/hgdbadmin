/* ------------------------------------------------
 *
 * File: TreeNodeChangeEvent.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\event\TreeNodeChangeEvent.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.event;

import com.highgo.hgdbadmin.util.event.Event;

/**
 *
 * @author Fanyi
 */
public class TreeNodeChangeEvent extends Event<TreeNodeChangeEventHandler>
{
    public static Type<TreeNodeChangeEventHandler> TYPE = new Type<TreeNodeChangeEventHandler>();
    private Object nodeObjcet;

    public TreeNodeChangeEvent(Object nodeObjcet)
    {
        this.nodeObjcet = nodeObjcet;
    }

    @Override
    public Type<TreeNodeChangeEventHandler> getAssociatedType()
    {
        return TYPE;
    }

    @Override
    protected void dispatch(TreeNodeChangeEventHandler handler)
    {
        handler.onChanged(this);
    }

    public Object getNodeObject()
    {
        return nodeObjcet;
    }
}
