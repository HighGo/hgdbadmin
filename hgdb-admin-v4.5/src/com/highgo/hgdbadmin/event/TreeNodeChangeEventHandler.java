/* ------------------------------------------------
 *
 * File: TreeNodeChangeEventHandler.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\event\TreeNodeChangeEventHandler.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.event;

import com.highgo.hgdbadmin.util.event.EventHandler;

/**
 *
 * @author Fanyi
 */
public interface TreeNodeChangeEventHandler extends EventHandler
{
    void onChanged(TreeNodeChangeEvent event);
    
}
