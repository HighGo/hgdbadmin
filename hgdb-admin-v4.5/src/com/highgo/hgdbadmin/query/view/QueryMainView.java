/* ------------------------------------------------ 
* 
* File: QueryMainView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\view\QueryMainView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.view;

import com.highgo.hgdbadmin.query.model.SmartPagingModel;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.query.util.XmlHelper;
import com.highgo.hgdbadmin.query.model.SQLResultInfoDTO;
import com.highgo.hgdbadmin.query.controller.QueryThread;
import com.highgo.hgdbadmin.query.listener.SourceListener;
import com.highgo.hgdbadmin.query.listener.SourceModel;
import com.highgo.hgdbadmin.datatransfer.model.FormatInfoDTO;
import com.highgo.hgdbadmin.query.util.ExportToFileDialog;
import com.highgo.hgdbadmin.query.util.NewConnectionDialog;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.controller.TreeController;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.text.BadLocationException;
import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.Gutter;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.fife.ui.rsyntaxtextarea.parser.Parser;

/**
 * @author Liu Yuanyuan
 *
 * main view of query
 *
 */
public class QueryMainView extends JFrame
{

    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private int PAGE_SIZE = 1000;

    private final String FavouriteRoot = "favourites";
    private final String Macro_Root = "macros";
    private final String HistoryQueryRoot = "histoqueries";//xml file root
    private File historyQueryFile;
    private Document historyQueryDoc;

    private HelperInfoDTO helperInfo;
    private Connection conn = null;
    private DefaultComboBoxModel connectModel;

    private DefaultComboBoxModel histoQueryModel;
    private RSyntaxTextArea syntexTextArea;//http://javadoc.fifesoft.com/rsyntaxtextarea/   

    private ImageIcon errorLineIcon;
    private Gutter gutter;

    
    /**
     * Creates new form QueryMainView
     *
     * @param helperInfo
     * @param initText
     */
    public QueryMainView(HelperInfoDTO helperInfo, String initText)
    {
        this.helperInfo = helperInfo;
        try
        {           
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName() + "?preferQueryMode=simple", helperInfo);
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }

        connectModel = new DefaultComboBoxModel();
        connectModel.addElement(helperInfo);
        connectModel.addElement(constBundle.getString("newConnection"));
        
        this.initComponents();
        menuFile.remove(miNewWindow);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/image/sql-32.png")));
        this.setTitle(MessageFormat.format(constBundle.getString("queryTitle"), helperInfo.getUser(), helperInfo.getHost(), helperInfo.getPort(), helperInfo.getDbName()));
        this.setExtendedState(QueryMainView.MAXIMIZED_BOTH);

        this.disableUselessItems();
        this.initQueryArea(initText);

        cbConnection.setSelectedIndex(0);
        cbConnection.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbConnectionItemStateChanged(e);
            }
        });
         try
        {
            String[] schemas = TreeController.getInstance().getItemsArrary(helperInfo, "schema");
            cbSchema.setModel(new DefaultComboBoxModel(schemas));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
        cbSchema.setSelectedItem("public");
        cbSchema.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbSchemaitemStateChanged(e);
            }
        });
        
        try
        {
            boolean setSchema = Boolean.valueOf(OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "set_schema"));
            if (!setSchema)
            {
                toolBar.remove(cbSchema);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
        
        //FILE
        ActionListener newWindows = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                newWindowAction(e);
            }
        };
        miNewWindow.addActionListener(newWindows);
        btnNewWindow.addActionListener(newWindows);
        ActionListener openFile = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                openFileAction(e);
            }
        };
        miOpen.addActionListener(openFile);
        btnOpen.addActionListener(openFile);
        ActionListener saveSql = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveSQLAction(e);
            }
        };
        miSave.addActionListener(saveSql);
        btnSave.addActionListener(saveSql);
        miSaveAsText.addActionListener(saveSql);
        miExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                exitAction(e);
            }
        });

        //EDIT
        ActionListener undo = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                undoAction(e);
            }
        };
        miUndo.addActionListener(undo);
        btnUndo.addActionListener(undo);
        ActionListener redo = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                redoAction(e);
            }
        };
        miRedo.addActionListener(redo);
        btnRedo.addActionListener(redo);
        ActionListener cut = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cutAction(e);
            }
        };
        miCut.addActionListener(cut);
        btnCut.addActionListener(cut);
        ActionListener copy = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                copyAction(e);
            }
        };
        miCopy.addActionListener(copy);
        btnCopy.addActionListener(copy);
        ActionListener paste = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                pasteAction(e);
            }
        };
        miPaste.addActionListener(paste);
        btnPaste.addActionListener(paste);
        ActionListener clear = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                clearAction(e);
            }
        };
        miClearWindow.addActionListener(clear);
        btnClearWindow.addActionListener(clear);
        ActionListener findAndReplace = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                findOrReplaceAction(e);
            }
        };
        miFindAndReplace.addActionListener(findAndReplace);
        btnFindAndReplace.addActionListener(findAndReplace);
        for (Action action : syntexTextArea.getActions())
        {
            String actionName = (String) action.getValue(Action.NAME);
            //logger.info("actionName=" + actionName);
            if (action instanceof ActionListener)
            {
                ActionListener actionListener = (ActionListener) action;
                switch (actionName)
                {
                    case "RTA.UpperCaseAction":
                        miUpperCase.addActionListener(actionListener);
                        break;
                    case "RTA.LowerCaseAction":
                        miLowerCase.addActionListener(actionListener);
                        break;
                    case "RSTA.ToggleCommentAction":
                        miCommentText.addActionListener(actionListener);
                        break;
                    case "insert-tab":
                        miBlockIndent.addActionListener(actionListener);
                        break;
                    case "RSTA.DecreaseIndentAction":
                        miReduceBlockIndent.addActionListener(actionListener);
                        break;
                    default:
                        break;
                }
            }
        }

        //FAVOURITE(not use)
        menuFavourites.addMenuListener(new MenuListener()
        {
            @Override
            public void menuDeselected(MenuEvent evt)
            {
                menuFavouritesMenuDeselected(evt);
            }

            @Override
            public void menuSelected(MenuEvent evt)
            {
                menuFavouritesMenuSelected(evt);
            }

            @Override
            public void menuCanceled(MenuEvent e)
            {
            }
        });
        menuFavourites.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                menuFavouritesItemStateChanged(evt);
            }
        });
        miAddFavourite.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                addToFavouriteAction(e);
            }
        });
        miManageFavourite.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                manageFavouriteAction(e);
            }
        });

        //MACRO(not use)
        miManageMacros.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                manageMacrosAction(e);
            }
        });
        menuMacros.addMenuListener(new MenuListener()
        {
            @Override
            public void menuDeselected(MenuEvent evt)
            {
            }

            @Override
            public void menuSelected(MenuEvent evt)
            {
                menuMacrosMenuSelected(evt);
            }

            @Override
            public void menuCanceled(MenuEvent evt)
            {
            }
        });

        //VIEW
        cmConnectionBar.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cmConnectionBarAction(e);
            }
        });
        cmScratchPad.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cmScratchPadAction(e);
            }
        });
        cmOutputPane.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cmOutputPaneAction(e);
            }
        });
        cmToolBar.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cmToolBarAction(e);
            }
        });
        cmDefaultView.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cmDefaultViewAction(e);
            }
        });

        //HELP(not use)
        ActionListener help = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                helpAction(e);
            }
        };
        miHelp.addActionListener(help);
        btnHelp.addActionListener(help);
        miSqlHelp.addActionListener(new ActionListener()
        {

            @Override
            public void actionPerformed(ActionEvent e)
            {
                sqlHelpAction(e);
            }
        });

        //QUERY MSG
        miClearHistoryMsg.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                clearHistoryMsgActionPerformed(e);
            }
        });
        miSaveHistoryMsg.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveHistoryMsgActionPerformed(e);
            }
        });

        //EXECUTE SQL
        ActionListener executeSql = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                executeSQLActionPerformed(e, false);
            }
        };
        miExecute.addActionListener(executeSql);
        btnExecute.addActionListener(executeSql);
        miExecuteToFile.addActionListener(executeSql);
        btnExecuteToFile.addActionListener(executeSql);
        //execute single one sql statement
        ActionListener executeSingleSql = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                executeSQLActionPerformed(e, true);
            }
        };
        btnExecuteSingle.addActionListener(executeSingleSql);
        //cancle current query        
        ActionListener cancleQuery = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cancleQueryActionPerformed(e);
            }
        };
        miCancleQuery.addActionListener(cancleQuery);
        btnCancleQuery.addActionListener(cancleQuery);

        //HISTORY SQL(not use)
        cbHisQuery.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbHisQueryItemStateChanged(e);
            }
        });
        btnRemoveHistoryQuery.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnRemoveHistoryQueryActionPerformed(e);
            }
        });
        btnRemoveAllHistoryQuery.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnRemoveAllHistoryQueryActionPerformed(e);
            }
        });

    }

    private void disableUselessItems()
    {
        splitQuery.remove(pnlHistory);

        menuSaveAs.remove(miGraphicalQuery);
        menuSaveAs.remove(miExplain);
        menuFile.remove(miExport);
        menuFile.remove(miQuickReport);
        menuFile.remove(menuRecentFiles);
        menuFile.remove(fileSp2);

        menuQuery.remove(miExecutePgScript);

        menuQuery.remove(miExplainQuery);
        menuQuery.remove(miExplainAnalyze);
        menuQuery.remove(menuExplainOptions);

        menuView.remove(miIndentGuides);
        menuView.remove(miLineEnds);
        menuView.remove(miWhitesSpace);
        menuView.remove(miWordWrap);
        menuView.remove(miLineNumber);
        menuView.remove(viewSp2);

        menuBar.remove(menuFavourites);
        menuBar.remove(menuMacros);
        menuBar.remove(menuHelp);

        toolBar.remove(btnPgScriptExc);
        toolBar.remove(btnQueExp);
        toolBar.remove(jsepCancel);
        toolBar.remove(btnHelp);

        tpMainQuery.remove(spGQuery);//tpMainQuery.setEnabledAt(1, false);
        splitTop.remove(spScratchPad);
    }

    private void initQueryArea(String initText)
    {
        logger.info("init query area");
        syntexTextArea = new RSyntaxTextArea();
        syntexTextArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
        syntexTextArea.setCodeFoldingEnabled(true);
        syntexTextArea.setBracketMatchingEnabled(false);
        syntexTextArea.setText(initText);
        syntexTextArea.setMinimumSize(new Dimension(2, 1));
        logger.info(syntexTextArea.getFont().toString());
        try
        {
            String fontSetting = OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "query_editor_font");
            logger.info("fontSetting=" + fontSetting);
            if (fontSetting != null && !fontSetting.isEmpty())
            {

                String[] font = fontSetting.split(",");
                Font f = new Font(font[0].trim(),
                        Integer.valueOf(font[1].trim()), Integer.valueOf(font[2].trim()));
                logger.info("query area font:" + f.toString());
                syntexTextArea.setFont(f);
                syntexTextArea.setForeground(new Color(Integer.valueOf(font[3].trim())));
            }
            logger.info(syntexTextArea.getFont().toString());
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }

        RTextScrollPane textSP = new RTextScrollPane(syntexTextArea, true);
        gutter = textSP.getGutter();
        gutter.setLineNumberColor(Color.red);
        //gutter.setActiveLineRangeColor(Color.yellow);//this is default setting
        gutter.setBorderColor(Color.green);
        //for error icon
        gutter.setBookmarkingEnabled(true);
        errorLineIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(
                getClass().getResource("/com/highgo/hgdbadmin/image/ddnull.png")));
        gutter.setBookmarkIcon(errorLineIcon);
        textSP.setPreferredSize(new Dimension(100, 50));//350，150
        splitQuery.setBottomComponent(textSP);

        //history query
        /*
        XmlHelper xmlHelper;
        try
        {
            xmlHelper = new XmlHelper();
            String  histoQuery = OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(),"history_query_file");
            logger.info(histoQuery);
            historyQueryFile = new File(histoQuery);
            historyQueryDoc = xmlHelper.getDocumentFromFile(historyQueryFile, HistoryQueryRoot);
            String[] array = new String[historyQueryDoc.getRootElement().elements().size()];
            int i = 0;
            if (!historyQueryDoc.getRootElement().elements().isEmpty())
            {
                for (Iterator it = historyQueryDoc.getRootElement().elementIterator(); it.hasNext();)
                {
                    Element element = (Element) it.next();
                    array[i] = element.getText();
                    i++;
                }
            }
            histoQueryModel = new DefaultComboBoxModel(array);
            cbHisQuery.setModel(histoQueryModel);
            cbHisQuery.setSelectedIndex(-1);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
         */
        syntexTextArea.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                queryAreaKeyReleased(e);
            }
        });

    }

    //connection
    private void cbConnectionItemStateChanged(ItemEvent evt)
    {
        Object selectedItem = cbConnection.getSelectedItem();
        if (selectedItem == null)
        {
            return;
        }
        logger.debug(selectedItem.toString());
        if (selectedItem.equals(constBundle.getString("newConnection")))
        {
            NewConnectionDialog mcd = new NewConnectionDialog(null, true, this.helperInfo);
            mcd.setLocationRelativeTo(this);
            if (mcd.showDialog())
            {
                cbConnection.removeItem(constBundle.getString("newConnection"));
                cbConnection.addItem(mcd.getNewHelperInfo());
                cbConnection.addItem(constBundle.getString("newConnection"));
                cbConnection.setSelectedIndex(cbConnection.getItemCount() - 2);
            } else
            {
                cbConnection.setSelectedIndex(-1);
            }
        } else
        {           
            try            
            {
                JdbcHelper.close(conn);
                this.helperInfo = (HelperInfoDTO) selectedItem;
                conn = JdbcHelper.getConnection(this.helperInfo.getPartURL() + this.helperInfo.getDbName() + "?preferQueryMode=simple", this.helperInfo);
            } catch (ClassNotFoundException | SQLException ex)
            {
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            }
        }
    }
    //schema
    private void cbSchemaitemStateChanged(ItemEvent evt)
    {
        if (cbSchema.getSelectedIndex() < 0)
        {
            logger.warn("no schema selected");
            return;
        }
        logger.info("selected schema=" + cbSchema.getSelectedItem());
        try
        {
            conn.setSchema((cbSchema.getSelectedItem().toString()));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }
    
    //history query
    private void cbHisQueryItemStateChanged(ItemEvent evt)
    {
        logger.info("PreQuery SelectedIndex:" + cbHisQuery.getSelectedIndex());
        if (cbHisQuery.getSelectedIndex() >= 0)
        {
            syntexTextArea.setText(cbHisQuery.getSelectedItem().toString());
            btnRemoveHistoryQuery.setEnabled(true);
        } else
        {
            btnRemoveHistoryQuery.setEnabled(false);
        }
        if (cbHisQuery.getItemCount() >= 1)
        {
            btnRemoveAllHistoryQuery.setEnabled(true);
        } else
        {
            btnRemoveAllHistoryQuery.setEnabled(false);
        }
    }
    private void btnRemoveHistoryQueryActionPerformed(ActionEvent evt)
    {
        if (cbHisQuery.getSelectedIndex() == -1)
        {
            logger.info("no preQuuery is selected, do nothing and return .");
            return;
        }
        int result = JOptionPane.showConfirmDialog(this, constBundle.getString("deleteHisQuery"),
                constBundle.getString("deleteConfirm"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        String selectedQuery = histoQueryModel.getSelectedItem().toString();
        if (result == JOptionPane.YES_OPTION)
        {
            for (Iterator it = historyQueryDoc.getRootElement().elementIterator(); it.hasNext();)
            {
                Element preQueryElement = (Element) it.next();
                if (selectedQuery.equals(preQueryElement.getText()))
                {
                    try
                    {
                        historyQueryDoc.getRootElement().remove(preQueryElement);
                        XmlHelper xmlHelper = new XmlHelper();
                        xmlHelper.writeDocumentToFile(historyQueryDoc, historyQueryFile);// write to  xml file
                        histoQueryModel.removeElement(histoQueryModel.getSelectedItem());
                        histoQueryModel.setSelectedItem(null);
                        break;
                    } catch (Exception ex)
                    {
                        JOptionPane.showMessageDialog(this, ex.getMessage(),
                                constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                    }

                }
            }

        }
    }
    private void btnRemoveAllHistoryQueryActionPerformed(ActionEvent evt)
    {
        logger.info("history query Size:" + histoQueryModel.getSize());
        if (histoQueryModel.getSize() < 1)
        {
            return;
        }
        int result = JOptionPane.showConfirmDialog(null, constBundle.getString("deleteAllHisquery"),
                constBundle.getString("deleteConfirm"), JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        if (result == JOptionPane.YES_OPTION)
        {
            for (Iterator it = historyQueryDoc.getRootElement().elementIterator(); it.hasNext();)
            {
                Element preQueryElement = (Element) it.next();
                historyQueryDoc.getRootElement().remove(preQueryElement);
            }
            try
            {
                XmlHelper xmlHelper = new XmlHelper();
                xmlHelper.writeDocumentToFile(historyQueryDoc, historyQueryFile);
                histoQueryModel.removeAllElements();
                btnRemoveAllHistoryQuery.setEnabled(false);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void queryAreaKeyReleased(KeyEvent evt)
    {
        miAddFavourite.setEnabled(!syntexTextArea.getText().isEmpty());
        btnSave.setEnabled(!syntexTextArea.getText().isEmpty());
        miSave.setEnabled(!syntexTextArea.getText().isEmpty());

        btnUndo.setEnabled(syntexTextArea.canUndo());
        miUndo.setEnabled(syntexTextArea.canUndo());
        btnRedo.setEnabled(syntexTextArea.canRedo());
        miRedo.setEnabled(syntexTextArea.canRedo());
    }

    /*
     first time to save needs to choose a file, after query view opened;
     if the file is choosed and always it until save as or query view closed.
     */
    private String sqlPath = null;
    private void saveSQLAction(ActionEvent evt)
    {
        logger.info("save" + evt.getActionCommand());
        File file = null;
        //save as
        if (sqlPath == null
                || evt.getActionCommand().equals(constBundle.getString("queryText")))
        {
            FileChooserDialog fc = new FileChooserDialog();
            int response = fc.showFileChooser(this, null, "");
            logger.info("response = " + response + ", path = " + fc.getChoosedFileDir());
            if (response == 0)//ok
            {
                sqlPath = fc.getChoosedFileDir();
                logger.info(sqlPath);
                if (new File(sqlPath).exists())
                {
                    int resp = JOptionPane.showConfirmDialog(this, MessageFormat.format(constBundle.getString("confirmSave"), sqlPath), "", JOptionPane.YES_NO_OPTION);
                    logger.info("confirm save = " + resp);
                    if (resp != 0)
                    {
                        return;
                    }
                }
            }
        }
        //save to recently opened file
        file = new File(sqlPath);
        try
        {
            if (!file.exists())
            {
                logger.warn(sqlPath + " not exist now, then create a new one.");
                file.createNewFile();
            }
            FileUtils.writeStringToFile(file, syntexTextArea.getText());
            btnSave.setEnabled(false);
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
    /*open history file*/
    private void exitAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        this.dispose();//System.exit(0);
    }

    //query action
    private void removePastErrorParser()
    {
        int parserCount = syntexTextArea.getParserCount();
        logger.debug("parserCount=" + parserCount);
        if (parserCount <= 0)
        {
            return;
        }
        Parser p;
        for (int i = parserCount - 1; i >= 0; i--)//must in this order for remove
        {
            //logger.debug("i=" + i);
            p = syntexTextArea.getParser(i);
            if (p instanceof ErrorTokenMarker)
            {
                syntexTextArea.removeParser(p);
            }
        }
    }
    private void executeSQLActionPerformed(ActionEvent evt, boolean isSingle)
    {
        logger.info(evt.getActionCommand());
        removePastErrorParser();

        final String sqlSequence;
        String selectedText = syntexTextArea.getSelectedText();
        //logger.info("selectStartPosition=" + syntexTextArea.getSelectionStart());
        int selectStartPosition = 0;
        if (selectedText != null && !selectedText.trim().isEmpty())
        {
            sqlSequence = selectedText;
            btnCancleQuery.setEnabled(true);
            miCancleQuery.setEnabled(true);
        } else if (syntexTextArea.getText() != null && !syntexTextArea.getText().trim().isEmpty())
        {
            sqlSequence = syntexTextArea.getText();
            btnCancleQuery.setEnabled(true);
            miCancleQuery.setEnabled(true);
        } else
        {
            btnCancleQuery.setEnabled(false);
            miCancleQuery.setEnabled(false);
            logger.info("There's no sql to execute, do nothing and return.");
            return;
        }

        SQLResultInfoDTO m = new SQLResultInfoDTO();
        SourceModel ourModel = new SourceModel(m);
        //create an anonymous class that implements DataListener
        final boolean isSaveToFile = ((evt.getSource() == btnExecuteToFile) || (evt.getSource() == miExecuteToFile));
        SourceListener listener = new SourceListener()
        {
            @Override
            public void dataChanged(Object o)
            {
                SourceModel m = (SourceModel) o;
                SQLResultInfoDTO result = m.getSource();
                logger.info("data changed");
                displayResult(result, sqlSequence, isSaveToFile);
            }
        };
        ourModel.addListener(listener);
        if (isSaveToFile)
        {
            ExportToFileDialog exportToFileDialog = new ExportToFileDialog(this, true);
            exportToFileDialog.setLocationRelativeTo(this);
            FormatInfoDTO formate = exportToFileDialog.showDialog();
            t = new QueryThread(conn, isSingle, sqlSequence, selectStartPosition, ourModel, formate);
        } else
        {
            t = new QueryThread(conn, isSingle, sqlSequence, selectStartPosition, ourModel, null);//, tpMsg.getDocument(), tpHistoryMsg.getDocument()
        }
        t.start();
    }
    private QueryThread t = null;
    private void displayResult(SQLResultInfoDTO result, String sqlSequence, boolean isSave)
    {
        logger.info("isSqlFinish=" + result.isFinished());
        btnCancleQuery.setEnabled(!result.isFinished());
        String lineSep = System.getProperty("line.separator", "\n");
        if (result.isFinished())
        {
            logger.info("isSuccess=" + result.isSuccess());
            if (result.isSuccess())
            {
                logger.info("returnedRow=" + result.getReturnedRow());
                if (result.getReturnedRow() == -1)//this is an uopdate sql execution
                {
                    tpResult.setSelectedIndex(2);

                    tpResult.setComponentAt(0, spDataOutput);
                    tpResult.setTitleAt(0, constBundle.getString("dataOutput"));

                    tpMsg.setText((result.getWarning() == null ? constBundle.getString("querySuccessWithoutResult")
                            : "WARNING: " + result.getWarning().getMessage() + lineSep + constBundle.getString("sqlState") + result.getWarning().getSQLState())
                            + lineSep + MessageFormat.format(constBundle.getString("queryTotalTime"), result.getConsumedMS())
                            + lineSep + MessageFormat.format(constBundle.getString("affectedRow"), result.getAffectedRow()));
                } else//this is query sql execution
                {
                    if (isSave)
                    {
                        tpResult.setSelectedIndex(2);
                    } else
                    {
                        tpResult.setSelectedIndex(0);
                        JTable tblOutput = new JTable();
                        tblOutput.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                        tblOutput.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
//                    tblOutput.setColumnSelectionAllowed(true);
//                    tblOutput.getColumnModel().getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                        SmartPagingModel pagingModel = new SmartPagingModel(conn, result.getSql(), PAGE_SIZE, null, result.getHeaders(), result.getData());
                        tblOutput.setModel(pagingModel);
                        pagingModel.addTableModelListener(new TableModelListener()
                        {
                            @Override
                            public void tableChanged(TableModelEvent e)
                            {
                                SmartPagingModel pagingModel = (SmartPagingModel) e.getSource();
                                logger.info("currentPage=" + (pagingModel.getPageOffset() + 1));
                            }
                        });
                        JScrollPane spOutput = SmartPagingModel.createPagingScrollPaneForTable(tblOutput);
                        tpResult.setComponentAt(0, spOutput);
                        tpResult.setTitleAt(0, constBundle.getString("dataOutput"));
                        tblOutput.repaint();
                    }

                    tpMsg.setText((result.getWarning() == null ? ""
                            : "WARNING: " + result.getWarning().getMessage() + lineSep
                            + constBundle.getString("sqlState") + result.getWarning().getSQLState() + lineSep)
                            + MessageFormat.format(constBundle.getString("queryTotalTime"), result.getConsumedMS())
                            + lineSep + MessageFormat.format(constBundle.getString("retrievedRow"), result.getReturnedRow()));
                }

                //this.saveHisQuery(sqlSequence);           
                txfdState.setText(constBundle.getString("queryFinish"));
            } else
            {
                tpResult.setSelectedIndex(2);

                tpResult.setComponentAt(0, spDataOutput);
                tpResult.setTitleAt(0, constBundle.getString("dataOutput"));

                if (result.getErrorPositionInLine() == -1)
                {
                    tpMsg.setText(result.getError().getMessage()
                            + lineSep + constBundle.getString("sqlState") + result.getError().getSQLState());
                } else
                {
                    //int basePosition = syntexTextArea.getCaretPosition();
                    //syntexTextArea.setCaretPosition(basePosition + result.getSqlStartPosition() + result.getErrorPositionInLine());//设置指针位置
                    int errorLine = syntexTextArea.getCaretLineNumber();//the line is one-based
                    logger.info("errorLine=" + errorLine);
                    //ErrorTokenMarker maker = new ErrorTokenMarker(syntexTextArea, result.getErrorPositionInLine(), errorLine, result.getErrorWord().length());//添加波浪线
                    //syntexTextArea.addParser(maker);
                    tpMsg.setText("LINE " + (errorLine + 1) + ":" 
                            + result.getSql()
                            + lineSep + result.getError().getMessage()
                            + lineSep + constBundle.getString("sqlState") + result.getError().getSQLState());
                    try
                    {
                        gutter.removeAllTrackingIcons();
                        gutter.addLineTrackingIcon(errorLine, errorLineIcon);//add an icon to the error line, the line is zero-based
                    } catch (BadLocationException ex)
                    {
                        logger.error(ex.getMessage());
                        ex.printStackTrace(System.out);
                    }
                }

                txfdState.setText(result.getError().getMessage());
            }
            tpHistoryMsg.setText(tpHistoryMsg.getText()
                    + constBundle.getString("execQuery") + lineSep + result.getSql() + lineSep + tpMsg.getText() + lineSep + lineSep);
        } else
        {
            tpResult.setSelectedIndex(0);

            tpResult.setComponentAt(0, spDataOutput);
            tpResult.setTitleAt(0, constBundle.getString("dataOutput"));

            tpMsg.setText("");

            txfdState.setText(constBundle.getString("queryRunning"));
        }
        miClearHistoryMsg.setEnabled(true);
        miSaveHistoryMsg.setEnabled(true);
    }
    private void saveHisQuery(String sqlSequence)
    {
        if (histoQueryModel == null)
        {
            return;
        }
        try
        {
            XmlHelper xmlHelper = new XmlHelper();
            Properties prop = OptionController.getInstance().getOptionProperties();
            int querySize = Integer.valueOf(OptionController.getInstance().getPropertyValue(prop, "query_max_size"));
            int queryCount = Integer.valueOf(OptionController.getInstance().getPropertyValue(prop, "query_max_count"));
            logger.info("queryCount=" + queryCount + ",querySize=" + querySize);
            if (sqlSequence.length() <= querySize)
            {
                int actualQueryCount = histoQueryModel.getSize();
                logger.info("actualQueryCount=" + actualQueryCount);
                if (actualQueryCount >= queryCount)
                {
                    for (int i = 0; i <= actualQueryCount - queryCount; i++)
                    {
                        logger.info("remove i=" + i);
                        historyQueryDoc.getRootElement().remove((Element) historyQueryDoc.getRootElement().elements().get(0));
                        histoQueryModel.removeElementAt(0);
                    }
                }
                //save sql to history query 
                historyQueryDoc.getRootElement().addElement("histoquery").setText(sqlSequence);
                histoQueryModel.addElement(sqlSequence);
            }
            xmlHelper.writeDocumentToFile(historyQueryDoc, historyQueryFile);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void cancleQueryActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (t != null)
        {
            t.stopQuery();
        }
    }
    private void clearHistoryMsgActionPerformed(ActionEvent evt)
    {
        tpHistoryMsg.setText("");
        miClearHistoryMsg.setEnabled(false);
        miSaveHistoryMsg.setEnabled(false);
    }
    private void saveHistoryMsgActionPerformed(ActionEvent evt)
    {
        logger.info("save" + evt.getActionCommand());
        String path;
        FileChooserDialog fc = new FileChooserDialog();
        int response = fc.showFileChooser(this, null, "");
        logger.info("response = " + response + ", path = " + fc.getChoosedFileDir());
        if (response == 0)//ok
        {
            path = fc.getChoosedFileDir();
            logger.info(path);
            int resp = JOptionPane.showConfirmDialog(this, MessageFormat.format(constBundle.getString("confirmSave"), path),
                    "", JOptionPane.YES_NO_OPTION);
            logger.info("confirm save = " + resp);
            if (resp != 0)
            {
                return;
            }
        } else
        {
            return;
        }
        File file = new File(path);
        try
        {
            if (!file.exists())
            {
                logger.warn(path + " not exist now, then create a new one.");
                file.createNewFile();
            }
            logger.info("history msg = " + tpHistoryMsg.getText());
            FileUtils.writeStringToFile(file, tpHistoryMsg.getText());
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }

    //FILE action
    @Deprecated
    private void newWindowAction(ActionEvent evt)
    {
        logger.info("new window");
        QueryMainView newView = new QueryMainView(helperInfo, "");
        newView.setLocationRelativeTo(null);
        newView.setVisible(true);        
    }
    private void openFileAction(ActionEvent evt)
    {
        logger.debug("Enter:" + evt.getActionCommand());
        FileChooserDialog fc = new FileChooserDialog();
        int response = fc.showFileChooser(this, null, "");
        logger.info("response=" + response + ", path=" + fc.getChoosedFileDir());
        if (response != 0)
        {
            return;
        }
        File file = new File(fc.getChoosedFileDir());
        try
        {
            if (!file.exists())
            {
                throw new Exception(sqlPath + " not exist, do nothing and return.");
            }
            sqlPath = file.getAbsolutePath();
            String content = FileUtils.readFileToString(file);
            syntexTextArea.append(content);
            String msg = constBundle.getString("fileOpenTitle");
            this.setTitle(this.getTitle() + MessageFormat.format(msg, file.getPath()));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }

    //EDIT 
    private void undoAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (syntexTextArea.canUndo())
        {
            syntexTextArea.undoLastAction();
        }
        btnUndo.setEnabled(syntexTextArea.canUndo());
        miUndo.setEnabled(syntexTextArea.canUndo());
        btnRedo.setEnabled(syntexTextArea.canRedo());
        miRedo.setEnabled(syntexTextArea.canRedo());
    }
    private void redoAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (syntexTextArea.canRedo())
        {
            syntexTextArea.redoLastAction();
        }
        miRedo.setEnabled(syntexTextArea.canRedo());
        btnRedo.setEnabled(syntexTextArea.canRedo());
        miUndo.setEnabled(syntexTextArea.canUndo());
        btnUndo.setEnabled(syntexTextArea.canUndo());
    }
    private void cutAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (syntexTextArea.getSelectedText() != null)
        {
            syntexTextArea.cut();
            btnPaste.setEnabled(true);
            miPaste.setEnabled(true);
        }
    }
    private void copyAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (syntexTextArea.getSelectedText() != null)
        {
            //syntexTextArea.copyAsRtf();//not support Chinese
            syntexTextArea.copy();
            btnPaste.setEnabled(true);
            miPaste.setEnabled(true);
        }
    }
    private void pasteAction(ActionEvent evt)
    {
        syntexTextArea.paste();
    }
    private void clearAction(ActionEvent evt)
    {
        syntexTextArea.setText("");
    }
    private void findOrReplaceAction(ActionEvent evt)
    {
        FindAndReplaceView findAndReplaceView = new FindAndReplaceView(this, false, syntexTextArea);
        findAndReplaceView.setLocationRelativeTo(syntexTextArea);
        findAndReplaceView.setVisible(true);
    }

    //FAVOURITE 
    private void addToFavouriteAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        AddFavouritesView addToFavourite = new AddFavouritesView(this, true, syntexTextArea.getText());
        addToFavourite.setLocationRelativeTo(this);
        addToFavourite.setVisible(true);
    }
    private void manageFavouriteAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        ManageFavouritesView favouritesView = new ManageFavouritesView(this, true);
        favouritesView.setLocationRelativeTo(this);
        favouritesView.setVisible(true);
    }
    private void menuFavouritesItemStateChanged(ItemEvent evt)
    {
        logger.info("Enter");
        if (menuFavourites.getItemCount() > 3)
        {
            for (int i = 3; i < menuFavourites.getItemCount(); i++)
            {
                menuFavourites.remove(menuFavourites.getMenuComponent(i));
            }
        }
        menuBar.updateUI();
        this.repaint();
    }
    private void menuFavouritesMenuSelected(MenuEvent evt)
    {
        logger.info("Enter");
        XmlHelper xmlHelper;
        try
        {
            xmlHelper = new XmlHelper();
            String favourite = OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "favourite_file");
            File favouriteFile = new File(favourite);
            Document doc = xmlHelper.getDocumentFromFile(favouriteFile, FavouriteRoot);
            addMenu(doc.getRootElement(), menuFavourites);
            menuBar.updateUI();
            this.repaint();
        } catch (Exception ex)
        {
            logger.error("Error :" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
    private void addMenu(Element element, JMenu menu)
    {
        logger.info("Enter: addMenu");
        if (element.isRootElement())
        {
            menu = this.menuFavourites;
        }
        if (element.elements().isEmpty())
        {
            return;
        }
        for (Iterator it = element.elementIterator(); it.hasNext();)
        {
            final Element subEle = (Element) it.next();
            String eName = subEle.getQName().getName();
            if (eName.equals("folder"))
            {
                JMenu subMenu = new JMenu(subEle.attributeValue("title"));
                menu.add(subMenu);
                addMenu(subEle, subMenu);
            } else if (eName.equals("favourite"))
            {
                JMenuItem subMenuItem = new JMenuItem(subEle.attributeValue("title"));
                subMenuItem.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent evt)
                    {
                        //clilk the item to finish its function
                        if (syntexTextArea.getText() != null)
                        {
                            int i = JOptionPane.showConfirmDialog(null, constBundle.getString("replaceQuery"),
                                    constBundle.getString("confirmReplace"), JOptionPane.YES_NO_CANCEL_OPTION);
                            if (i == JOptionPane.YES_OPTION)
                            {
                                syntexTextArea.setText(subEle.getText());
                            }
                        }
                    }
                });
                menu.add(subMenuItem);
            }
        }
    }
    private void menuFavouritesMenuDeselected(MenuEvent evt)
    {
        logger.info("Enter");
        if (menuFavourites.getItemCount() > 3)
        {
            for (int i = 3; i < menuFavourites.getItemCount(); i++)
            {
                menuFavourites.remove(menuFavourites.getMenuComponent(i));
            }
        }
        menuBar.updateUI();
    }

    //MACRO 
    private void manageMacrosAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        ManageMacrosView macrosView = new ManageMacrosView(this, true);
        macrosView.setLocationRelativeTo(this);
        macrosView.setVisible(true);
    }
    private void menuMacrosMenuSelected(MenuEvent evt)
    {
        logger.info("Enter");
        menuMacros.removeAll();
        menuMacros.add(miManageMacros);
        menuMacros.add(macroSp);

        XmlHelper xmlHelper;
        try
        {
            xmlHelper = new XmlHelper();
            String macro = OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "macro_file");
            File macroFile = new File(macro);
            Document doc = xmlHelper.getDocumentFromFile(macroFile, Macro_Root);
            //add menu based on xml
            for (Iterator it = doc.getRootElement().elementIterator(); it.hasNext();)
            {
                final Element ele = (Element) it.next();
                JMenuItem item = new JMenuItem(ele.attributeValue("name"));
                // add macro key
                item.setAccelerator(this.getKeyStrokeByKeyName(String.valueOf(ele.attributeValue("key"))));
                menuMacros.add(item);
                item.addActionListener(new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent evt)
                    {
                        logger.info(evt.getActionCommand());
                        syntexTextArea.setText(ele.getText());
                    }
                });
            }
            //this.menuMacros.updateUI();
        } catch (Exception ex)
        {
            logger.error("Error :" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
    private KeyStroke getKeyStrokeByKeyName(String key)
    {
        logger.info("key name:" + key);
        switch (key)
        {
            case "Alt-F1":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.ALT_MASK);
            case "Alt-F2":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F2, InputEvent.ALT_MASK);
            case "Alt-F3":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F3, InputEvent.ALT_MASK);
            case "Alt-F4":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK);
            case "Alt-F5":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F5, InputEvent.ALT_MASK);
            case "Alt-F6":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F6, InputEvent.ALT_MASK);
            case "Alt-F7":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F7, InputEvent.ALT_MASK);
            case "Alt-F8":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F8, InputEvent.ALT_MASK);
            case "Alt-F9":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F9, InputEvent.ALT_MASK);
            case "Alt-F10":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F10, InputEvent.ALT_MASK);
            case "Alt-F11":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F11, InputEvent.ALT_MASK);
            case "Alt-F12":
                return KeyStroke.getKeyStroke(KeyEvent.VK_F12, InputEvent.ALT_MASK);
            case "Ctrl-1":
                return KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.CTRL_MASK);
            case "Ctrl-2":
                return KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_MASK);
            case "Ctrl-3":
                return KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.CTRL_MASK);
            case "Ctrl-4":
                return KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.CTRL_MASK);
            case "Ctrl-5":
                return KeyStroke.getKeyStroke(KeyEvent.VK_5, InputEvent.CTRL_MASK);
            case "Ctrl-6":
                return KeyStroke.getKeyStroke(KeyEvent.VK_6, InputEvent.CTRL_MASK);
            case "Ctrl-7":
                return KeyStroke.getKeyStroke(KeyEvent.VK_7, InputEvent.CTRL_MASK);
            case "Ctrl-8":
                return KeyStroke.getKeyStroke(KeyEvent.VK_8, InputEvent.CTRL_MASK);
            case "Ctrl-9":
                return KeyStroke.getKeyStroke(KeyEvent.VK_9, InputEvent.CTRL_MASK);
            case "Ctrl-0":
                return KeyStroke.getKeyStroke(KeyEvent.VK_0, InputEvent.CTRL_MASK);
            default:
                logger.error(key + " is an exception key name, do nothing and return.");
                return null;
        }
    }

    //VIEW
    private void cmConnectionBarAction(ActionEvent evt)
    {
        logger.info("ConnectionBar isSelected:" + cmConnectionBar.isSelected());
        if (cmConnectionBar.isSelected())
        {
            toolBar.add(tbConnection);
        } else
        {
            toolBar.remove(tbConnection);
            cmDefaultView.setSelected(false);
        }
        this.getContentPane().validate();
    }
    private void cmScratchPadAction(ActionEvent evt)
    {
        logger.info("ScratchPad isSelected:" + cmScratchPad.isSelected());
        if (cmScratchPad.isSelected())
        {
            splitTop.setRightComponent(spScratchPad);
            cmDefaultView.setSelected(false);
        } else
        {
            splitTop.remove(spScratchPad);
        }
    }
    private void cmOutputPaneAction(ActionEvent evt)
    {
        logger.info("OutputPne isSelected:" + cmOutputPane.isSelected());
        if (cmOutputPane.isSelected())
        {
            spSqlEditor.setBottomComponent(tpResult);
        } else
        {
            spSqlEditor.remove(tpResult);
            cmDefaultView.setSelected(false);
        }
    }
    private void cmToolBarAction(ActionEvent evt)
    {
        logger.info("ToolBar isSelected:" + cmToolBar.isSelected());
        if (cmToolBar.isSelected())
        {
            //this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
            toolBar.setVisible(true);
        } else
        {
            //this.getContentPane().remove(toolBar);
            toolBar.setVisible(false);
            cmDefaultView.setSelected(false);
        }
        getContentPane().validate();
    }
    private void cmDefaultViewAction(ActionEvent evt)
    {
        logger.info("Set to default view");
        if (!cmConnectionBar.isSelected())
        {
            toolBar.add(tbConnection);
            cmConnectionBar.setSelected(true);
        }
        if (cmScratchPad.isSelected())//default not open this
        {
            splitTop.remove(spScratchPad);
            cmScratchPad.setSelected(false);
        }
        if (!cmOutputPane.isSelected())
        {
            spSqlEditor.setBottomComponent(tpResult);
            cmOutputPane.setSelected(true);
        }
        if (!cmToolBar.isSelected())
        {
            //this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
            //getContentPane().validate();
            toolBar.setVisible(true);
            cmToolBar.setSelected(true);
        }
    }

    //HELP
    private void helpAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showAdminHelpPage(this, "query.html");
    }

    private void sqlHelpAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showPgHelpPage(this, "sql-commands.html");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        toolBar = new javax.swing.JToolBar();
        btnNewWindow = new javax.swing.JButton();
        btnOpen = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        jsepSave = new javax.swing.JToolBar.Separator();
        btnCut = new javax.swing.JButton();
        btnCopy = new javax.swing.JButton();
        btnPaste = new javax.swing.JButton();
        btnClearWindow = new javax.swing.JButton();
        jsepClear = new javax.swing.JToolBar.Separator();
        btnUndo = new javax.swing.JButton();
        btnRedo = new javax.swing.JButton();
        jsepRedo = new javax.swing.JToolBar.Separator();
        btnFindAndReplace = new javax.swing.JButton();
        jsepFind = new javax.swing.JToolBar.Separator();
        btnExecute = new javax.swing.JButton();
        btnExecuteSingle = new javax.swing.JButton();
        btnExecuteToFile = new javax.swing.JButton();
        btnPgScriptExc = new javax.swing.JButton();
        btnQueExp = new javax.swing.JButton();
        btnCancleQuery = new javax.swing.JButton();
        jsepCancel = new javax.swing.JToolBar.Separator();
        btnHelp = new javax.swing.JButton();
        tbConnection = new javax.swing.JToolBar();
        cbConnection = new javax.swing.JComboBox();
        cbSchema = new javax.swing.JComboBox<>();
        tpMainQuery = new javax.swing.JTabbedPane();
        spSqlEditor = new javax.swing.JSplitPane();
        splitTop = new javax.swing.JSplitPane();
        spQueryArea = new javax.swing.JScrollPane();
        splitQuery = new javax.swing.JSplitPane();
        pnlHistory = new javax.swing.JPanel();
        lblPreQuery = new javax.swing.JLabel();
        cbHisQuery = new javax.swing.JComboBox();
        btnRemoveHistoryQuery = new javax.swing.JButton();
        btnRemoveAllHistoryQuery = new javax.swing.JButton();
        spScratchPad = new javax.swing.JScrollPane();
        ttarScratchPad = new javax.swing.JTextArea();
        tpResult = new javax.swing.JTabbedPane();
        spDataOutput = new javax.swing.JScrollPane();
        spExplain = new javax.swing.JScrollPane();
        pnlExplain = new javax.swing.JPanel();
        spMsg = new javax.swing.JScrollPane();
        tpMsg = new javax.swing.JTextPane();
        spHistory = new javax.swing.JScrollPane();
        tpHistoryMsg = new javax.swing.JTextPane();
        spGQuery = new javax.swing.JSplitPane();
        spConsTop = new javax.swing.JSplitPane();
        spTree = new javax.swing.JScrollPane();
        pnlDBDetail = new javax.swing.JPanel();
        spDeal = new javax.swing.JScrollPane();
        pnlRight = new javax.swing.JPanel();
        spCondition = new javax.swing.JTabbedPane();
        spColumns = new javax.swing.JScrollPane();
        pnlColumns = new javax.swing.JPanel();
        btnMoveTop = new javax.swing.JButton();
        btnMoveUp = new javax.swing.JButton();
        btnMoveDown = new javax.swing.JButton();
        btnMoveBottom = new javax.swing.JButton();
        spConditions = new javax.swing.JScrollPane();
        pnlConditions = new javax.swing.JPanel();
        btnAddCondituon = new javax.swing.JButton();
        btnRemoveCondition = new javax.swing.JButton();
        spOrder = new javax.swing.JScrollPane();
        pnlOrder = new javax.swing.JPanel();
        btnOrderAdd = new javax.swing.JButton();
        btnOrderAddAll = new javax.swing.JButton();
        btnOrderRemove = new javax.swing.JButton();
        btnOrderRemoveAll = new javax.swing.JButton();
        btnToTop = new javax.swing.JButton();
        btnToUp = new javax.swing.JButton();
        btnToDown = new javax.swing.JButton();
        btnToBottom = new javax.swing.JButton();
        spConnection = new javax.swing.JScrollPane();
        pnlConnection = new javax.swing.JPanel();
        btnAddConnect = new javax.swing.JButton();
        btnRemoveConnect = new javax.swing.JButton();
        txfdState = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        miNewWindow = new javax.swing.JMenuItem();
        miOpen = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        menuSaveAs = new javax.swing.JMenu();
        miSaveAsText = new javax.swing.JMenuItem();
        miGraphicalQuery = new javax.swing.JMenuItem();
        miExplain = new javax.swing.JMenuItem();
        fileSp1 = new javax.swing.JPopupMenu.Separator();
        miExport = new javax.swing.JMenuItem();
        miQuickReport = new javax.swing.JMenuItem();
        fileSp2 = new javax.swing.JPopupMenu.Separator();
        menuRecentFiles = new javax.swing.JMenu();
        miExit = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        miUndo = new javax.swing.JMenuItem();
        miRedo = new javax.swing.JMenuItem();
        editSp1 = new javax.swing.JPopupMenu.Separator();
        miCut = new javax.swing.JMenuItem();
        miCopy = new javax.swing.JMenuItem();
        miPaste = new javax.swing.JMenuItem();
        miClearWindow = new javax.swing.JMenuItem();
        editSp2 = new javax.swing.JPopupMenu.Separator();
        miFindAndReplace = new javax.swing.JMenuItem();
        editSp3 = new javax.swing.JPopupMenu.Separator();
        miAutoIndent = new javax.swing.JCheckBoxMenuItem();
        menuFormat = new javax.swing.JMenu();
        miUpperCase = new javax.swing.JMenuItem();
        miLowerCase = new javax.swing.JMenuItem();
        formatSp = new javax.swing.JPopupMenu.Separator();
        miBlockIndent = new javax.swing.JMenuItem();
        miReduceBlockIndent = new javax.swing.JMenuItem();
        miCommentText = new javax.swing.JMenuItem();
        menuLineEnds = new javax.swing.JMenu();
        miUnix = new javax.swing.JRadioButtonMenuItem();
        miDos = new javax.swing.JRadioButtonMenuItem();
        miMac = new javax.swing.JRadioButtonMenuItem();
        menuQuery = new javax.swing.JMenu();
        miExecute = new javax.swing.JMenuItem();
        miExecutePgScript = new javax.swing.JMenuItem();
        miExecuteToFile = new javax.swing.JMenuItem();
        miExplainQuery = new javax.swing.JMenuItem();
        miExplainAnalyze = new javax.swing.JMenuItem();
        menuExplainOptions = new javax.swing.JMenu();
        miVerbose = new javax.swing.JMenuItem();
        cbItemCosts = new javax.swing.JCheckBoxMenuItem();
        miBuffers = new javax.swing.JMenuItem();
        cbItemTiming = new javax.swing.JCheckBoxMenuItem();
        querySp1 = new javax.swing.JPopupMenu.Separator();
        miSaveHistoryMsg = new javax.swing.JMenuItem();
        miClearHistoryMsg = new javax.swing.JMenuItem();
        querySp2 = new javax.swing.JPopupMenu.Separator();
        miAutoRollBack = new javax.swing.JCheckBoxMenuItem();
        querySp3 = new javax.swing.JPopupMenu.Separator();
        miCancleQuery = new javax.swing.JMenuItem();
        menuFavourites = new javax.swing.JMenu();
        miAddFavourite = new javax.swing.JMenuItem();
        miManageFavourite = new javax.swing.JMenuItem();
        favouriteSp = new javax.swing.JPopupMenu.Separator();
        menuMacros = new javax.swing.JMenu();
        miManageMacros = new javax.swing.JMenuItem();
        macroSp = new javax.swing.JPopupMenu.Separator();
        menuView = new javax.swing.JMenu();
        cmConnectionBar = new javax.swing.JCheckBoxMenuItem();
        cmOutputPane = new javax.swing.JCheckBoxMenuItem();
        cmScratchPad = new javax.swing.JCheckBoxMenuItem();
        cmToolBar = new javax.swing.JCheckBoxMenuItem();
        viewSp1 = new javax.swing.JPopupMenu.Separator();
        miIndentGuides = new javax.swing.JMenuItem();
        miLineEnds = new javax.swing.JMenuItem();
        miWhitesSpace = new javax.swing.JMenuItem();
        miWordWrap = new javax.swing.JMenuItem();
        miLineNumber = new javax.swing.JMenuItem();
        viewSp2 = new javax.swing.JPopupMenu.Separator();
        cmDefaultView = new javax.swing.JCheckBoxMenuItem();
        menuHelp = new javax.swing.JMenu();
        miHelp = new javax.swing.JMenuItem();
        miSqlHelp = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(400, 300));
        setName(""); // NOI18N
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        toolBar.setRollover(true);
        toolBar.setMaximumSize(new java.awt.Dimension(13, 12));

        btnNewWindow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/file_new.png"))); // NOI18N
        btnNewWindow.setToolTipText(constBundle.getString("newWindow")
        );
        btnNewWindow.setFocusable(false);
        btnNewWindow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnNewWindow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnNewWindow);

        btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/file_open.png"))); // NOI18N
        btnOpen.setToolTipText(constBundle.getString("openFile")
        );
        btnOpen.setFocusable(false);
        btnOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOpen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnOpen);

        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/file_save.png"))); // NOI18N
        btnSave.setToolTipText(constBundle.getString("saveFile")
        );
        btnSave.setEnabled(false);
        btnSave.setFocusable(false);
        btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnSave);
        toolBar.add(jsepSave);

        btnCut.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/clip_cut.png"))); // NOI18N
        btnCut.setToolTipText(constBundle.getString("cutSelectionToTip")
        );
        btnCut.setFocusable(false);
        btnCut.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCut.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCut);

        btnCopy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/clip_copy.png"))); // NOI18N
        btnCopy.setToolTipText(constBundle.getString("copySelectionToTip")
        );
        btnCopy.setFocusable(false);
        btnCopy.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCopy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCopy);

        btnPaste.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/clip_paste.png"))); // NOI18N
        btnPaste.setToolTipText(constBundle.getString("pasteSelectText")
        );
        btnPaste.setEnabled(false);
        btnPaste.setFocusable(false);
        btnPaste.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPaste.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnPaste);

        btnClearWindow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/edit_clear.png"))); // NOI18N
        btnClearWindow.setToolTipText(constBundle.getString("emptyEditWindow")
        );
        btnClearWindow.setFocusable(false);
        btnClearWindow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnClearWindow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnClearWindow);
        toolBar.add(jsepClear);

        btnUndo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/edit_undo.png"))); // NOI18N
        btnUndo.setToolTipText(constBundle.getString("undoLastAction")
        );
        btnUndo.setEnabled(false);
        btnUndo.setFocusable(false);
        btnUndo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnUndo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnUndo);

        btnRedo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/edit_redo.png"))); // NOI18N
        btnRedo.setToolTipText(constBundle.getString("redoLastAction")
        );
        btnRedo.setEnabled(false);
        btnRedo.setFocusable(false);
        btnRedo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRedo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnRedo);
        toolBar.add(jsepRedo);

        btnFindAndReplace.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/edit_find.png"))); // NOI18N
        btnFindAndReplace.setToolTipText(constBundle.getString("findReplaceWord")
        );
        btnFindAndReplace.setFocusable(false);
        btnFindAndReplace.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFindAndReplace.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnFindAndReplace);
        toolBar.add(jsepFind);

        btnExecute.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_execute.png"))); // NOI18N
        btnExecute.setToolTipText(constBundle.getString("execQuery")
        );
        btnExecute.setFocusable(false);
        btnExecute.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExecute.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnExecute);

        btnExecuteSingle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_single_execute.png"))); // NOI18N
        btnExecuteSingle.setToolTipText(constBundle.getString("execSingleQuery")
        );
        btnExecuteSingle.setFocusable(false);
        btnExecuteSingle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExecuteSingle.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnExecuteSingle);

        btnExecuteToFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_execfile.png"))); // NOI18N
        btnExecuteToFile.setToolTipText(constBundle.getString("doQueryWriteResult")
        );
        btnExecuteToFile.setFocusable(false);
        btnExecuteToFile.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExecuteToFile.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnExecuteToFile);

        btnPgScriptExc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_pgscript.png"))); // NOI18N
        btnPgScriptExc.setToolTipText(constBundle.getString("executeScript")
        );
        btnPgScriptExc.setEnabled(false);
        btnPgScriptExc.setFocusable(false);
        btnPgScriptExc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPgScriptExc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnPgScriptExc);

        btnQueExp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_explain.png"))); // NOI18N
        btnQueExp.setToolTipText(constBundle.getString("explainQuery")
        );
        btnQueExp.setEnabled(false);
        btnQueExp.setFocusable(false);
        btnQueExp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnQueExp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnQueExp);

        btnCancleQuery.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_cancel.png"))); // NOI18N
        btnCancleQuery.setToolTipText(constBundle.getString("cancleQuery")
        );
        btnCancleQuery.setEnabled(false);
        btnCancleQuery.setFocusable(false);
        btnCancleQuery.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancleQuery.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCancleQuery);
        toolBar.add(jsepCancel);

        btnHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/help.png"))); // NOI18N
        btnHelp.setToolTipText(constBundle.getString("SQLHelpView")
        );
        btnHelp.setFocusable(false);
        btnHelp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnHelp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnHelp);

        tbConnection.setRollover(true);
        tbConnection.setMinimumSize(new java.awt.Dimension(100, 23));
        tbConnection.setPreferredSize(new java.awt.Dimension(290, 23));

        cbConnection.setModel(connectModel);
        cbConnection.setMinimumSize(new java.awt.Dimension(100, 21));
        cbConnection.setPreferredSize(new java.awt.Dimension(290, 21));
        tbConnection.add(cbConnection);

        toolBar.add(tbConnection);

        cbSchema.setMinimumSize(new java.awt.Dimension(50, 23));
        cbSchema.setPreferredSize(new java.awt.Dimension(90, 23));
        toolBar.add(cbSchema);

        getContentPane().add(toolBar, java.awt.BorderLayout.PAGE_START);

        tpMainQuery.setPreferredSize(new java.awt.Dimension(670, 540));

        spSqlEditor.setDividerLocation(200);
        spSqlEditor.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        spSqlEditor.setResizeWeight(0.5);
        spSqlEditor.setPreferredSize(new java.awt.Dimension(500, 450));

        splitTop.setDividerLocation(450);
        splitTop.setResizeWeight(0.6);
        splitTop.setPreferredSize(new java.awt.Dimension(680, 200));

        spQueryArea.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        spQueryArea.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        spQueryArea.setMaximumSize(new java.awt.Dimension(0, 0));
        spQueryArea.setMinimumSize(new java.awt.Dimension(0, 0));

        splitQuery.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        pnlHistory.setPreferredSize(new java.awt.Dimension(413, 40));

        lblPreQuery.setText(constBundle.getString("previousQuery"));

        btnRemoveHistoryQuery.setText(constBundle.getString("remove"));
        btnRemoveHistoryQuery.setEnabled(false);

        btnRemoveAllHistoryQuery.setText(constBundle.getString("deleteAll"));

        javax.swing.GroupLayout pnlHistoryLayout = new javax.swing.GroupLayout(pnlHistory);
        pnlHistory.setLayout(pnlHistoryLayout);
        pnlHistoryLayout.setHorizontalGroup(
            pnlHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHistoryLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPreQuery)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbHisQuery, 0, 182, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemoveHistoryQuery)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRemoveAllHistoryQuery)
                .addContainerGap())
        );

        pnlHistoryLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnRemoveAllHistoryQuery, btnRemoveHistoryQuery});

        pnlHistoryLayout.setVerticalGroup(
            pnlHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlHistoryLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlHistoryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPreQuery, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cbHisQuery, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRemoveHistoryQuery, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnRemoveAllHistoryQuery, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        pnlHistoryLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnRemoveAllHistoryQuery, btnRemoveHistoryQuery});

        splitQuery.setTopComponent(pnlHistory);

        spQueryArea.setViewportView(splitQuery);

        splitTop.setLeftComponent(spQueryArea);

        spScratchPad.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("theScratchPad")));
        spScratchPad.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        spScratchPad.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        spScratchPad.setMaximumSize(new java.awt.Dimension(0, 0));
        spScratchPad.setMinimumSize(new java.awt.Dimension(0, 0));
        spScratchPad.setPreferredSize(new java.awt.Dimension(150, 260));

        ttarScratchPad.setColumns(20);
        ttarScratchPad.setRows(5);
        spScratchPad.setViewportView(ttarScratchPad);

        splitTop.setRightComponent(spScratchPad);
        spScratchPad.getAccessibleContext().setAccessibleName(constBundle.getString("theScratchPad"));

        spSqlEditor.setTopComponent(splitTop);

        tpResult.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("theOutputPane")));
        tpResult.setPreferredSize(new java.awt.Dimension(680, 230));

        spDataOutput.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        spDataOutput.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        spDataOutput.setPreferredSize(new java.awt.Dimension(670, 200));
        tpResult.addTab(constBundle.getString("dataOutput"), spDataOutput);

        spExplain.setPreferredSize(new java.awt.Dimension(670, 202));

        pnlExplain.setPreferredSize(new java.awt.Dimension(800, 200));

        javax.swing.GroupLayout pnlExplainLayout = new javax.swing.GroupLayout(pnlExplain);
        pnlExplain.setLayout(pnlExplainLayout);
        pnlExplainLayout.setHorizontalGroup(
            pnlExplainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
        );
        pnlExplainLayout.setVerticalGroup(
            pnlExplainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 200, Short.MAX_VALUE)
        );

        spExplain.setViewportView(pnlExplain);

        tpResult.addTab(constBundle.getString("theExplanation"), spExplain);

        spMsg.setPreferredSize(new java.awt.Dimension(670, 200));

        tpMsg.setEditable(false);
        spMsg.setViewportView(tpMsg);

        tpResult.addTab(constBundle.getString("theNews"), spMsg);

        spHistory.setPreferredSize(new java.awt.Dimension(670, 200));

        tpHistoryMsg.setEditable(false);
        spHistory.setViewportView(tpHistoryMsg);

        tpResult.addTab(constBundle.getString("theHistory"), spHistory);

        spSqlEditor.setBottomComponent(tpResult);
        tpResult.getAccessibleContext().setAccessibleName(constBundle.getString("theOutputPane"));

        tpMainQuery.addTab(constBundle.getString("sqledit"), spSqlEditor);

        spGQuery.setDividerLocation(400);
        spGQuery.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        spGQuery.setPreferredSize(new java.awt.Dimension(500, 450));

        spConsTop.setDividerLocation(280);

        javax.swing.GroupLayout pnlDBDetailLayout = new javax.swing.GroupLayout(pnlDBDetail);
        pnlDBDetail.setLayout(pnlDBDetailLayout);
        pnlDBDetailLayout.setHorizontalGroup(
            pnlDBDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 277, Short.MAX_VALUE)
        );
        pnlDBDetailLayout.setVerticalGroup(
            pnlDBDetailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 495, Short.MAX_VALUE)
        );

        spTree.setViewportView(pnlDBDetail);

        spConsTop.setLeftComponent(spTree);

        javax.swing.GroupLayout pnlRightLayout = new javax.swing.GroupLayout(pnlRight);
        pnlRight.setLayout(pnlRightLayout);
        pnlRightLayout.setHorizontalGroup(
            pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1615, Short.MAX_VALUE)
        );
        pnlRightLayout.setVerticalGroup(
            pnlRightLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 478, Short.MAX_VALUE)
        );

        spDeal.setViewportView(pnlRight);

        spConsTop.setRightComponent(spDeal);

        spGQuery.setTopComponent(spConsTop);

        spCondition.setPreferredSize(new java.awt.Dimension(800, 200));

        spColumns.setPreferredSize(new java.awt.Dimension(800, 200));

        btnMoveTop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbUpTop.png"))); // NOI18N

        btnMoveUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbUp.png"))); // NOI18N

        btnMoveDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/down.png"))); // NOI18N

        btnMoveBottom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbDownBottom.png"))); // NOI18N

        javax.swing.GroupLayout pnlColumnsLayout = new javax.swing.GroupLayout(pnlColumns);
        pnlColumns.setLayout(pnlColumnsLayout);
        pnlColumnsLayout.setHorizontalGroup(
            pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlColumnsLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addGroup(pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnMoveBottom, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnMoveDown, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnMoveUp, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnMoveTop, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(1634, Short.MAX_VALUE))
        );

        pnlColumnsLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnMoveBottom, btnMoveDown, btnMoveTop, btnMoveUp});

        pnlColumnsLayout.setVerticalGroup(
            pnlColumnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlColumnsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnMoveTop)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnMoveUp, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(btnMoveDown)
                .addGap(10, 10, 10)
                .addComponent(btnMoveBottom)
                .addContainerGap(1190, Short.MAX_VALUE))
        );

        pnlColumnsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnMoveBottom, btnMoveDown, btnMoveTop});

        spColumns.setViewportView(pnlColumns);

        spCondition.addTab(constBundle.getString("columns"), spColumns);

        btnAddCondituon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbAddRest.png"))); // NOI18N

        btnRemoveCondition.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbRemoveRest.png"))); // NOI18N

        javax.swing.GroupLayout pnlConditionsLayout = new javax.swing.GroupLayout(pnlConditions);
        pnlConditions.setLayout(pnlConditionsLayout);
        pnlConditionsLayout.setHorizontalGroup(
            pnlConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConditionsLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addGroup(pnlConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAddCondituon)
                    .addComponent(btnRemoveCondition))
                .addContainerGap(1628, Short.MAX_VALUE))
        );

        pnlConditionsLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAddCondituon, btnRemoveCondition});

        pnlConditionsLayout.setVerticalGroup(
            pnlConditionsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConditionsLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnAddCondituon)
                .addGap(10, 10, 10)
                .addComponent(btnRemoveCondition)
                .addContainerGap(1232, Short.MAX_VALUE))
        );

        pnlConditionsLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAddCondituon, btnRemoveCondition});

        spConditions.setViewportView(pnlConditions);

        spCondition.addTab(constBundle.getString("conditions"), spConditions);

        btnOrderAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbOrderAdd.png"))); // NOI18N

        btnOrderAddAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbOrderAddAll.png"))); // NOI18N

        btnOrderRemove.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbOrderRemove.png"))); // NOI18N

        btnOrderRemoveAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbOrderRemoveAll.png"))); // NOI18N

        btnToTop.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbUpTop.png"))); // NOI18N

        btnToUp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbUp.png"))); // NOI18N

        btnToDown.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbDown.png"))); // NOI18N

        btnToBottom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbDownBottom.png"))); // NOI18N

        javax.swing.GroupLayout pnlOrderLayout = new javax.swing.GroupLayout(pnlOrder);
        pnlOrder.setLayout(pnlOrderLayout);
        pnlOrderLayout.setHorizontalGroup(
            pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrderLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addGroup(pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOrderAdd)
                    .addComponent(btnOrderAddAll)
                    .addComponent(btnOrderRemove)
                    .addComponent(btnOrderRemoveAll))
                .addGap(30, 30, 30)
                .addGroup(pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnToTop)
                    .addComponent(btnToUp)
                    .addComponent(btnToDown)
                    .addComponent(btnToBottom))
                .addContainerGap(1559, Short.MAX_VALUE))
        );
        pnlOrderLayout.setVerticalGroup(
            pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOrderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlOrderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlOrderLayout.createSequentialGroup()
                        .addComponent(btnOrderAdd)
                        .addGap(10, 10, 10)
                        .addComponent(btnOrderAddAll)
                        .addGap(10, 10, 10)
                        .addComponent(btnOrderRemove)
                        .addGap(10, 10, 10)
                        .addComponent(btnOrderRemoveAll))
                    .addGroup(pnlOrderLayout.createSequentialGroup()
                        .addComponent(btnToTop)
                        .addGap(10, 10, 10)
                        .addComponent(btnToUp)
                        .addGap(10, 10, 10)
                        .addComponent(btnToDown)
                        .addGap(10, 10, 10)
                        .addComponent(btnToBottom)))
                .addContainerGap(1190, Short.MAX_VALUE))
        );

        spOrder.setViewportView(pnlOrder);

        spCondition.addTab(constBundle.getString("order"), spOrder);

        btnAddConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbAdd.png"))); // NOI18N

        btnRemoveConnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbRemove.png"))); // NOI18N

        javax.swing.GroupLayout pnlConnectionLayout = new javax.swing.GroupLayout(pnlConnection);
        pnlConnection.setLayout(pnlConnectionLayout);
        pnlConnectionLayout.setHorizontalGroup(
            pnlConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConnectionLayout.createSequentialGroup()
                .addGap(200, 200, 200)
                .addGroup(pnlConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAddConnect)
                    .addComponent(btnRemoveConnect))
                .addContainerGap(1628, Short.MAX_VALUE))
        );

        pnlConnectionLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAddConnect, btnRemoveConnect});

        pnlConnectionLayout.setVerticalGroup(
            pnlConnectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlConnectionLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnAddConnect)
                .addGap(18, 18, 18)
                .addComponent(btnRemoveConnect)
                .addContainerGap(1224, Short.MAX_VALUE))
        );

        pnlConnectionLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAddConnect, btnRemoveConnect});

        spConnection.setViewportView(pnlConnection);

        spCondition.addTab(constBundle.getString("connection"), spConnection);

        spGQuery.setBottomComponent(spCondition);
        spCondition.getAccessibleContext().setAccessibleName(constBundle.getString("columns")
        );

        tpMainQuery.addTab(constBundle.getString("graphicalQueryBuilder"), spGQuery);

        getContentPane().add(tpMainQuery, java.awt.BorderLayout.CENTER);
        tpMainQuery.getAccessibleContext().setAccessibleName(constBundle.getString("sqledit"));

        txfdState.setEditable(false);
        txfdState.setText(constBundle.getString("preparation"));
        getContentPane().add(txfdState, java.awt.BorderLayout.PAGE_END);

        menuFile.setText(constBundle.getString("file"));

        miNewWindow.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        miNewWindow.setText(constBundle.getString("newWindow"));
        menuFile.add(miNewWindow);

        miOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        miOpen.setText(constBundle.getString("open"));
        menuFile.add(miOpen);

        miSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        miSave.setText(constBundle.getString("save"));
        miSave.setEnabled(false);
        menuFile.add(miSave);

        menuSaveAs.setText(constBundle.getString("otherSave"));

        miSaveAsText.setText(constBundle.getString("queryText"));
        menuSaveAs.add(miSaveAsText);

        miGraphicalQuery.setText(constBundle.getString("graphicalQuery"));
        miGraphicalQuery.setEnabled(false);
        menuSaveAs.add(miGraphicalQuery);

        miExplain.setText(constBundle.getString("explain"));
        miExplain.setEnabled(false);
        menuSaveAs.add(miExplain);

        menuFile.add(menuSaveAs);
        menuFile.add(fileSp1);

        miExport.setText(constBundle.getString("export"));
        miExport.setEnabled(false);
        menuFile.add(miExport);

        miQuickReport.setText(constBundle.getString("quickReport"));
        miQuickReport.setEnabled(false);
        menuFile.add(miQuickReport);
        menuFile.add(fileSp2);

        menuRecentFiles.setText(constBundle.getString("recentlyOpenFile"));
        menuRecentFiles.setEnabled(false);
        menuFile.add(menuRecentFiles);

        miExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        miExit.setText(constBundle.getString("exit"));
        menuFile.add(miExit);

        menuBar.add(menuFile);

        menuEdit.setText(constBundle.getString("edit"));

        miUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
        miUndo.setText(constBundle.getString("revoke"));
        miUndo.setEnabled(false);
        menuEdit.add(miUndo);

        miRedo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        miRedo.setText(constBundle.getString("reDo"));
        miRedo.setEnabled(false);
        menuEdit.add(miRedo);
        menuEdit.add(editSp1);

        miCut.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.CTRL_MASK));
        miCut.setText(constBundle.getString("cut"));
        menuEdit.add(miCut);

        miCopy.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        miCopy.setText(constBundle.getString("copy"));
        menuEdit.add(miCopy);

        miPaste.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_MASK));
        miPaste.setText(constBundle.getString("paste"));
        miPaste.setEnabled(false);
        menuEdit.add(miPaste);

        miClearWindow.setText(constBundle.getString("emptyWindow"));
        menuEdit.add(miClearWindow);
        menuEdit.add(editSp2);

        miFindAndReplace.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        miFindAndReplace.setText(constBundle.getString("findAndReplace"));
        menuEdit.add(miFindAndReplace);
        menuEdit.add(editSp3);

        miAutoIndent.setSelected(true);
        miAutoIndent.setText(constBundle.getString("autoIndentation"));
        menuEdit.add(miAutoIndent);

        menuFormat.setText(constBundle.getString("format"));

        miUpperCase.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.CTRL_MASK));
        miUpperCase.setText(constBundle.getString("uppercase"));
        menuFormat.add(miUpperCase);

        miLowerCase.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        miLowerCase.setText(constBundle.getString("lowercase"));
        menuFormat.add(miLowerCase);
        menuFormat.add(formatSp);

        miBlockIndent.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_TAB, 0));
        miBlockIndent.setText(constBundle.getString("blockIndentation"));
        menuFormat.add(miBlockIndent);

        miReduceBlockIndent.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_TAB, java.awt.event.InputEvent.SHIFT_MASK));
        miReduceBlockIndent.setText(constBundle.getString("reduceBlockIndentation"));
        menuFormat.add(miReduceBlockIndent);

        miCommentText.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        miCommentText.setText(constBundle.getString("notation"));
        menuFormat.add(miCommentText);

        menuEdit.add(menuFormat);

        menuLineEnds.setText(constBundle.getString("newline"));

        miUnix.setSelected(true);
        miUnix.setText(constBundle.getString("unix"));
        menuLineEnds.add(miUnix);

        miDos.setText(constBundle.getString("dos"));
        menuLineEnds.add(miDos);

        miMac.setText(constBundle.getString("mac"));
        menuLineEnds.add(miMac);

        menuEdit.add(menuLineEnds);

        menuBar.add(menuEdit);

        menuQuery.setText(constBundle.getString("query"));

        miExecute.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        miExecute.setText(constBundle.getString("execute"));
        menuQuery.add(miExecute);

        miExecutePgScript.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F6, 0));
        miExecutePgScript.setText(constBundle.getString("executeScript"));
        miExecutePgScript.setEnabled(false);
        menuQuery.add(miExecutePgScript);

        miExecuteToFile.setText(constBundle.getString("executeFile"));
        menuQuery.add(miExecuteToFile);

        miExplainQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        miExplainQuery.setText(constBundle.getString("explanation"));
        miExplainQuery.setEnabled(false);
        menuQuery.add(miExplainQuery);

        miExplainAnalyze.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, java.awt.event.InputEvent.SHIFT_MASK));
        miExplainAnalyze.setText(constBundle.getString("explainAnalysis"));
        miExplainAnalyze.setEnabled(false);
        menuQuery.add(miExplainAnalyze);

        menuExplainOptions.setText(constBundle.getString("explainOptions"));

        miVerbose.setText(constBundle.getString("verboseMode"));
        menuExplainOptions.add(miVerbose);

        cbItemCosts.setSelected(true);
        cbItemCosts.setText(constBundle.getString("spending"));
        menuExplainOptions.add(cbItemCosts);

        miBuffers.setText(constBundle.getString("cache"));
        menuExplainOptions.add(miBuffers);

        cbItemTiming.setSelected(true);
        cbItemTiming.setText(constBundle.getString("timing"));
        menuExplainOptions.add(cbItemTiming);

        menuQuery.add(menuExplainOptions);
        menuQuery.add(querySp1);

        miSaveHistoryMsg.setText(constBundle.getString("saveHistory"));
        miSaveHistoryMsg.setEnabled(false);
        menuQuery.add(miSaveHistoryMsg);

        miClearHistoryMsg.setText(constBundle.getString("emptyHistory"));
        miClearHistoryMsg.setEnabled(false);
        menuQuery.add(miClearHistoryMsg);
        menuQuery.add(querySp2);

        miAutoRollBack.setSelected(true);
        miAutoRollBack.setText(constBundle.getString("autoRollback"));
        miAutoRollBack.setEnabled(false);
        menuQuery.add(miAutoRollBack);
        menuQuery.add(querySp3);

        miCancleQuery.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_BACK_SPACE, java.awt.event.InputEvent.ALT_MASK));
        miCancleQuery.setText(constBundle.getString("cancleQuery"));
        miCancleQuery.setEnabled(false);
        menuQuery.add(miCancleQuery);

        menuBar.add(menuQuery);

        menuFavourites.setText(constBundle.getString("favorites"));

        miAddFavourite.setText(constBundle.getString("addFavorites"));
        miAddFavourite.setEnabled(false);
        menuFavourites.add(miAddFavourite);

        miManageFavourite.setText(constBundle.getString("manageFavorites"));
        menuFavourites.add(miManageFavourite);
        menuFavourites.add(favouriteSp);

        menuBar.add(menuFavourites);

        menuMacros.setText(constBundle.getString("shortcuts"));

        miManageMacros.setText(constBundle.getString("manageShortcuts"));
        menuMacros.add(miManageMacros);
        menuMacros.add(macroSp);

        menuBar.add(menuMacros);

        menuView.setText(constBundle.getString("view"));

        cmConnectionBar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cmConnectionBar.setSelected(true);
        cmConnectionBar.setText(constBundle.getString("connectionBar"));
        menuView.add(cmConnectionBar);

        cmOutputPane.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cmOutputPane.setSelected(true);
        cmOutputPane.setText(constBundle.getString("outputPane"));
        menuView.add(cmOutputPane);

        cmScratchPad.setText(constBundle.getString("scratchPad"));
        menuView.add(cmScratchPad);

        cmToolBar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cmToolBar.setSelected(true);
        cmToolBar.setText(constBundle.getString("toolbar"));
        menuView.add(cmToolBar);
        menuView.add(viewSp1);

        miIndentGuides.setText(constBundle.getString("indentGuide"));
        miIndentGuides.setEnabled(false);
        menuView.add(miIndentGuides);

        miLineEnds.setText(constBundle.getString("newline"));
        miLineEnds.setEnabled(false);
        menuView.add(miLineEnds);

        miWhitesSpace.setText(constBundle.getString("blank"));
        miWhitesSpace.setEnabled(false);
        menuView.add(miWhitesSpace);

        miWordWrap.setText(constBundle.getString("autoWrap"));
        miWordWrap.setEnabled(false);
        menuView.add(miWordWrap);

        miLineNumber.setText(constBundle.getString("lineNumber"));
        miLineNumber.setEnabled(false);
        miLineNumber.setSelected(true);
        menuView.add(miLineNumber);
        menuView.add(viewSp2);

        cmDefaultView.setText(constBundle.getString("defaultView"));
        menuView.add(cmDefaultView);

        menuBar.add(menuView);

        menuHelp.setText(constBundle.getString("help"));

        miHelp.setText(constBundle.getString("help"));
        menuHelp.add(miHelp);

        miSqlHelp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        miSqlHelp.setText(constBundle.getString("sqlHelp"));
        menuHelp.add(miSqlHelp);

        menuBar.add(menuHelp);

        setJMenuBar(menuBar);

        setBounds(0, 0, 721, 516);
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        logger.debug("dispose");
        JdbcHelper.close(conn);
        this.dispose();
    }//GEN-LAST:event_formWindowClosing

//    public String getText()
//    {
//        return syntexTextArea.getText();
//    }
//    public int getCaretPosition()
//    {
//        return syntexTextArea.getCaretPosition();
//    }
//    public void setCaretPosition(int caretPosition)
//    {
//        syntexTextArea.setCaretPosition(caretPosition);
//    }    
//    public void selectedContent(int start, int end)
//    {
//        syntexTextArea.select(start, end);
//    }
//    public void setNewText(String newText)
//    {
//        syntexTextArea.setText(newText);
//    }
//    public void replaceSelection(String toReplace)
//    {
//        syntexTextArea.replaceSelection(toReplace);
//    }
//    
    //batch add or delete comment function
    public void addOrDeleteComment()
    {
        /*        
        //1.get selected text
        String selection = txaSqlContent.getSelectedText();

        //2.if no selected text
        if (selection == null)
        {
            //get current caret position
            int cur = txaSqlContent.getCaretPosition();
            //2.1 if caretposition is on the begiing of the area
            if (cur == 0)
            {
                //if the line with the caret beginning with“--”or"//"，if yes then delete ,else insert
                String text = txaSqlContent.getText();
                if (text.startsWith("--") || text.startsWith("//"))
                {
                    txaSqlContent.replaceRange("", 0, 2);
                } else
                {
                    //insert comment
                    txaSqlContent.insert("--", 0);

                }
            } //2.2 if the caret is not at the beginning of the line
            else
            {
                //get text before the caret
                String preText = null;
                try
                {
                    preText = txaSqlContent.getText(0, cur);
                } catch (BadLocationException ex)
                {
                    logger.error("Error :" + ex.getMessage());
                    ex.printStackTrace(System.out);
                }
                //2.2.1 if before caret there contanins "\n"
                if (preText.contains("\n"))
                {
                    //get the fist 2 char in the line with caret
                    String preTwoChar = null;
                    try
                    {
                        preTwoChar = txaSqlContent.getText(preText.lastIndexOf("\n") + 1, 2);
                    } catch (BadLocationException ex)
                    {
                        logger.error("Error :" + ex.getMessage());
                        ex.printStackTrace(System.out);
                    }
                    //if the line is beginning with comment
                    if ("--".equals(preTwoChar) || "//".equals(preTwoChar))
                    {
                        int kaishi = preText.lastIndexOf("\n");
                        txaSqlContent.replaceRange(null, preText.lastIndexOf("\n") + 1, kaishi + 1 + 2);
                    } else
                    {
                        txaSqlContent.insert("--", preText.lastIndexOf("\n") + 1);
                    }
                } else
                {
                    //if the caret is on first line
                    String text = txaSqlContent.getText();
                    //the first has comment
                    if (text.startsWith("--") || text.startsWith("//"))
                    {
                        txaSqlContent.replaceRange("", 0, 2);
                    } else
                    {
                        //add the comment
                        txaSqlContent.insert("--", 0);

                    }
                }
            }
        } //3.exist selection text
        else
        {
            //split the text by "\n"
            String[] lines = selection.split("\n");
            //3.1 select only one line
            if (lines.length == 1)
            {
                int selectionStart = txaSqlContent.getSelectionStart();
                String selectTwoChar = "";
                try
                {
                    selectTwoChar = txaSqlContent.getText(selectionStart, 2);
                } catch (BadLocationException ex)
                {
                    logger.error("Error :" + ex.getMessage());
                    ex.printStackTrace(System.out);
                }
                //has comment,the delete
                if ("--".equals(selectTwoChar) || "//".equals(selectTwoChar))
                {
                    txaSqlContent.replaceRange("", selectionStart, selectionStart + 2);
                } else
                {
                    //no comment,then add
                    txaSqlContent.insert("--", selectionStart);
                    txaSqlContent.select(selectionStart, selectionStart + selection.length() + 2);
                }
            } //3.2 select several lines
            else
            {
                // deal with the first line,if the first has comment,then all delete comment,else all add comment
                boolean isStartWithComment = false;
                int selectionStart = txaSqlContent.getSelectionStart();
                int selectionEnd = txaSqlContent.getSelectionEnd();
                String selectFirstChar = "";
                try
                {
                    selectFirstChar = txaSqlContent.getText(selectionStart, 2);
                } catch (BadLocationException ex)
                {
                    logger.error("Error :" + ex.getMessage());
                    ex.printStackTrace(System.out);
                }
                if ("--".equals(selectFirstChar) || "//".equals(selectFirstChar))
                {
                    //the first line has comment
                    isStartWithComment = true;
                    txaSqlContent.replaceRange("", selectionStart, selectionStart + 2);
                } else
                {
                    //the first hasn't comment
                    isStartWithComment = false;
                }

                //deal with other lines
                StringBuilder builder = new StringBuilder();
                if (isStartWithComment)
                {
                    if (lines[0].contains("--") || lines[0].contains("//"))
                    {
                        lines[0] = lines[0].replaceAll("--", "");
                    }
                    builder.append(lines[0]);
                    for (int i = 1; i < lines.length; i++)
                    {
                        builder.append("\n");
                        if (lines[i].startsWith("--") || lines[i].startsWith("//"))
                        {
                            lines[i] = lines[i].substring(2);
                        }
                        builder.append(lines[i]);
                        selectionStart = txaSqlContent.getSelectionStart();
                        selectionEnd = txaSqlContent.getSelectionEnd();
                        txaSqlContent.replaceRange(builder.toString(), selectionStart, selectionEnd);

                        //reselect the selected text
                        selectionEnd = selectionStart + builder.length();
                        txaSqlContent.setSelectionStart(selectionStart);
                        txaSqlContent.setSelectionEnd(selectionEnd);
                    }
                } else
                {
                    int length = txaSqlContent.getSelectedText().substring(0, selection.indexOf("\n")).length();
                    txaSqlContent.insert("--", selectionStart);
                    String firstLine = "";
                    try
                    {
                        firstLine = txaSqlContent.getText(selectionStart, length + 2);
                    } catch (BadLocationException ex)
                    {
                        logger.error("Error :" + ex.getMessage());
                        ex.printStackTrace(System.out);
                    }
                    lines[0] = firstLine;
                    builder.append(lines[0]);
                    for (int i = 1; i < lines.length; i++)
                    {
                        builder.append("\n");
                        builder.append("--").append(lines[i]);

                    }
                    selectionStart = txaSqlContent.getSelectionStart();
                    selectionEnd = txaSqlContent.getSelectionEnd();
                    txaSqlContent.replaceRange(builder.toString(), selectionStart - 2, selectionEnd);
                    txaSqlContent.select(selectionStart - 2, selectionStart - 2 + selection.length() + 2 * lines.length);
                }
            }
        }
         */
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddCondituon;
    private javax.swing.JButton btnAddConnect;
    private javax.swing.JButton btnCancleQuery;
    private javax.swing.JButton btnClearWindow;
    private javax.swing.JButton btnCopy;
    private javax.swing.JButton btnCut;
    private javax.swing.JButton btnExecute;
    private javax.swing.JButton btnExecuteSingle;
    private javax.swing.JButton btnExecuteToFile;
    private javax.swing.JButton btnFindAndReplace;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnMoveBottom;
    private javax.swing.JButton btnMoveDown;
    private javax.swing.JButton btnMoveTop;
    private javax.swing.JButton btnMoveUp;
    private javax.swing.JButton btnNewWindow;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnOrderAdd;
    private javax.swing.JButton btnOrderAddAll;
    private javax.swing.JButton btnOrderRemove;
    private javax.swing.JButton btnOrderRemoveAll;
    private javax.swing.JButton btnPaste;
    private javax.swing.JButton btnPgScriptExc;
    private javax.swing.JButton btnQueExp;
    private javax.swing.JButton btnRedo;
    private javax.swing.JButton btnRemoveAllHistoryQuery;
    private javax.swing.JButton btnRemoveCondition;
    private javax.swing.JButton btnRemoveConnect;
    private javax.swing.JButton btnRemoveHistoryQuery;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnToBottom;
    private javax.swing.JButton btnToDown;
    private javax.swing.JButton btnToTop;
    private javax.swing.JButton btnToUp;
    private javax.swing.JButton btnUndo;
    private javax.swing.JComboBox cbConnection;
    private javax.swing.JComboBox cbHisQuery;
    private javax.swing.JCheckBoxMenuItem cbItemCosts;
    private javax.swing.JCheckBoxMenuItem cbItemTiming;
    private javax.swing.JComboBox<String> cbSchema;
    private javax.swing.JCheckBoxMenuItem cmConnectionBar;
    private javax.swing.JCheckBoxMenuItem cmDefaultView;
    private javax.swing.JCheckBoxMenuItem cmOutputPane;
    private javax.swing.JCheckBoxMenuItem cmScratchPad;
    private javax.swing.JCheckBoxMenuItem cmToolBar;
    private javax.swing.JPopupMenu.Separator editSp1;
    private javax.swing.JPopupMenu.Separator editSp2;
    private javax.swing.JPopupMenu.Separator editSp3;
    private javax.swing.JPopupMenu.Separator favouriteSp;
    private javax.swing.JPopupMenu.Separator fileSp1;
    private javax.swing.JPopupMenu.Separator fileSp2;
    private javax.swing.JPopupMenu.Separator formatSp;
    private javax.swing.JToolBar.Separator jsepCancel;
    private javax.swing.JToolBar.Separator jsepClear;
    private javax.swing.JToolBar.Separator jsepFind;
    private javax.swing.JToolBar.Separator jsepRedo;
    private javax.swing.JToolBar.Separator jsepSave;
    private javax.swing.JLabel lblPreQuery;
    private javax.swing.JPopupMenu.Separator macroSp;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuExplainOptions;
    private javax.swing.JMenu menuFavourites;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuFormat;
    private javax.swing.JMenu menuHelp;
    private javax.swing.JMenu menuLineEnds;
    private javax.swing.JMenu menuMacros;
    private javax.swing.JMenu menuQuery;
    private javax.swing.JMenu menuRecentFiles;
    private javax.swing.JMenu menuSaveAs;
    private javax.swing.JMenu menuView;
    private javax.swing.JMenuItem miAddFavourite;
    private javax.swing.JCheckBoxMenuItem miAutoIndent;
    private javax.swing.JCheckBoxMenuItem miAutoRollBack;
    private javax.swing.JMenuItem miBlockIndent;
    private javax.swing.JMenuItem miBuffers;
    private javax.swing.JMenuItem miCancleQuery;
    private javax.swing.JMenuItem miClearHistoryMsg;
    private javax.swing.JMenuItem miClearWindow;
    private javax.swing.JMenuItem miCommentText;
    private javax.swing.JMenuItem miCopy;
    private javax.swing.JMenuItem miCut;
    private javax.swing.JRadioButtonMenuItem miDos;
    private javax.swing.JMenuItem miExecute;
    private javax.swing.JMenuItem miExecutePgScript;
    private javax.swing.JMenuItem miExecuteToFile;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miExplain;
    private javax.swing.JMenuItem miExplainAnalyze;
    private javax.swing.JMenuItem miExplainQuery;
    private javax.swing.JMenuItem miExport;
    private javax.swing.JMenuItem miFindAndReplace;
    private javax.swing.JMenuItem miGraphicalQuery;
    private javax.swing.JMenuItem miHelp;
    private javax.swing.JMenuItem miIndentGuides;
    private javax.swing.JMenuItem miLineEnds;
    private javax.swing.JMenuItem miLineNumber;
    private javax.swing.JMenuItem miLowerCase;
    private javax.swing.JRadioButtonMenuItem miMac;
    private javax.swing.JMenuItem miManageFavourite;
    private javax.swing.JMenuItem miManageMacros;
    private javax.swing.JMenuItem miNewWindow;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miPaste;
    private javax.swing.JMenuItem miQuickReport;
    private javax.swing.JMenuItem miRedo;
    private javax.swing.JMenuItem miReduceBlockIndent;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JMenuItem miSaveAsText;
    private javax.swing.JMenuItem miSaveHistoryMsg;
    private javax.swing.JMenuItem miSqlHelp;
    private javax.swing.JMenuItem miUndo;
    private javax.swing.JRadioButtonMenuItem miUnix;
    private javax.swing.JMenuItem miUpperCase;
    private javax.swing.JMenuItem miVerbose;
    private javax.swing.JMenuItem miWhitesSpace;
    private javax.swing.JMenuItem miWordWrap;
    private javax.swing.JPanel pnlColumns;
    private javax.swing.JPanel pnlConditions;
    private javax.swing.JPanel pnlConnection;
    private javax.swing.JPanel pnlDBDetail;
    private javax.swing.JPanel pnlExplain;
    private javax.swing.JPanel pnlHistory;
    private javax.swing.JPanel pnlOrder;
    private javax.swing.JPanel pnlRight;
    private javax.swing.JPopupMenu.Separator querySp1;
    private javax.swing.JPopupMenu.Separator querySp2;
    private javax.swing.JPopupMenu.Separator querySp3;
    private javax.swing.JScrollPane spColumns;
    private javax.swing.JTabbedPane spCondition;
    private javax.swing.JScrollPane spConditions;
    private javax.swing.JScrollPane spConnection;
    private javax.swing.JSplitPane spConsTop;
    private javax.swing.JScrollPane spDataOutput;
    private javax.swing.JScrollPane spDeal;
    private javax.swing.JScrollPane spExplain;
    private javax.swing.JSplitPane spGQuery;
    private javax.swing.JScrollPane spHistory;
    private javax.swing.JScrollPane spMsg;
    private javax.swing.JScrollPane spOrder;
    private javax.swing.JScrollPane spQueryArea;
    private javax.swing.JScrollPane spScratchPad;
    private javax.swing.JSplitPane spSqlEditor;
    private javax.swing.JScrollPane spTree;
    private javax.swing.JSplitPane splitQuery;
    private javax.swing.JSplitPane splitTop;
    private javax.swing.JToolBar tbConnection;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JTextPane tpHistoryMsg;
    private javax.swing.JTabbedPane tpMainQuery;
    private javax.swing.JTextPane tpMsg;
    private javax.swing.JTabbedPane tpResult;
    private javax.swing.JTextArea ttarScratchPad;
    private javax.swing.JTextField txfdState;
    private javax.swing.JPopupMenu.Separator viewSp1;
    private javax.swing.JPopupMenu.Separator viewSp2;
    // End of variables declaration//GEN-END:variables
}
