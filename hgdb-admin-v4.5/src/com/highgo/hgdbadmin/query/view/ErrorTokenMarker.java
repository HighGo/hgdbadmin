/* ------------------------------------------------ 
* 
* File: ErrorTokenMarker.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\view\ErrorTokenMarker.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.view;

import java.awt.Color;
import javax.swing.text.Element;
import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.parser.AbstractParser;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParseResult;
import org.fife.ui.rsyntaxtextarea.parser.DefaultParserNotice;
import org.fife.ui.rsyntaxtextarea.parser.ParseResult;
import org.fife.ui.rsyntaxtextarea.parser.ParserNotice;

/**
 *  Displays Syntax errors at the level of tokens, these errors are generated by
 * {@link TokenMaker} like {@link CPlusPlusTokenMaker}
 *
 * @author Ricardo JL Rufino (ricardo@criativasoft.com.br)
 */
public class ErrorTokenMarker extends AbstractParser
{    
    private Integer line;
    private Integer errorPosition;
    private Integer errorWordLength;

    public ErrorTokenMarker(RSyntaxTextArea textarea, int errorPositionInAll, int line, int errorWordLength)
    {
        this.setEnabled(true);
        this.line = line;
        this.errorPosition = errorPositionInAll;
        this.errorWordLength = errorWordLength;
    }

    @Override
    public ParseResult parse(RSyntaxDocument doc, String style)
    {
        DefaultParseResult result = new DefaultParseResult(this);
        result.clearNotices();
        Element root = doc.getDefaultRootElement();
        int lineCount = root.getElementCount();
        result.setParsedLines(0, lineCount - 1);
        //在错误行line,错误位置errorPosition,添加长度为errorWordLength的波浪线
        DefaultParserNotice pn = new DefaultParserNotice(this, "syntax error", line, errorPosition, errorWordLength);
        pn.setLevel(ParserNotice.Level.ERROR);
        pn.setColor(Color.RED);
        result.addNotice(pn);
        return result;
    }

//    private boolean parseTokensErrors(RSyntaxDocument doc, int line)
//    {
//        boolean found = false;
//        Token t = doc.getTokenListForLine(line);
//        int start = -1;
//        int end = -1;
//
//        while (t != null)
//        {
//            if (isError(t))
//            {
//                if (start == -1)
//                {
//                    start = t.getOffset();
//                }
//            }
//            else
//            {
//                if (start > 0 && end == -1)
//                {
//                    end = t.getOffset();
//                }
//            }
//            t = t.getNextToken();
//        }
//        if (start > -1)
//        {
//            DefaultParserNotice pn = new DefaultParserNotice(this, "syntax error", line, start, (end - start));
//            pn.setLevel(ParserNotice.Level.ERROR);
//            pn.setColor(Color.RED);
//            result.addNotice(pn);
//            found = true;
//        }
//        return found;
//    }
//
//    private boolean isError(Token token)
//    {
////            switch (token.getType()) {
////                case Token.ERROR_CHAR:
////                case Token.ERROR_IDENTIFIER:
////                case Token.ERROR_NUMBER_FORMAT:
////                case Token.ERROR_STRING_DOUBLE:
////                    return true;
////            }
//        return true;
//    }

}
