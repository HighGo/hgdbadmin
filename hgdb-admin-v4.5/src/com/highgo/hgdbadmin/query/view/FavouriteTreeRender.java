/* ------------------------------------------------ 
* 
* File: FavouriteTreeRender.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\view\FavouriteTreeRender.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.view;

import java.awt.Component;
import java.awt.Toolkit;
import java.util.ResourceBundle;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import org.dom4j.Element;

/**
 *
 * @author Liu Yuanyuan
 */
public class FavouriteTreeRender extends DefaultTreeCellRenderer
{
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
    {
        JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus); 
        Element el = (Element) value;
        if (el.getParent() == null)
        {
            label.setText(constBundle.getString("favourite"));
        } else
        {
            label.setText(el.attributeValue("title"));
        }
        return label;
    }

    @Override
    public Icon getLeafIcon()
    {
        return new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/favourite.png"))); 
    }

    @Override
    public Icon getOpenIcon()
    {
        return new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/folder.png"))); 
    }

    @Override
    public Icon getDefaultLeafIcon()
    {
        return new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/favourite.png"))); 
    }

    @Override
    public Icon getDefaultOpenIcon()
    {
        return new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/folder.png")));
    }

}
