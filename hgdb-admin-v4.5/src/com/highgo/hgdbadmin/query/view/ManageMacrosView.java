/* ------------------------------------------------ 
* 
* File: ManageMacrosView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\view\ManageMacrosView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.view;

import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.query.util.XmlHelper;
import java.io.File;
import java.util.Iterator;
import java.util.ResourceBundle;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.dom4j.Document;
import org.dom4j.Element;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class ManageMacrosView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
  
    private File  macroFile;// = new File("highgo-macros.xml");
    private String macroRoot = "macros";
    
    private DefaultTableModel tabModel;
    private int selectRow;       
    private Document macroDoc;
    
    /**
     * Creates new form ManageMacrosView
     * @param parent
     * @param modal
     */
    public ManageMacrosView(JFrame parent, boolean modal)
    {
        super(parent, modal);
        initComponents();
        initTable();
    }

    private void initTable()
    {
        XmlHelper xmlHelper = null;
        try
        {
            xmlHelper = new XmlHelper();
            String macro = OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "macro_file");
            logger.info("macro file:" + macro);
            macroFile = new File(macro);            
            macroDoc = xmlHelper.getDocumentFromFile(macroFile, macroRoot);
            String[] header = new String[]
            {
                constBundle.getString("key"), constBundle.getString("name")
            };
            Object[][] data = new Object[22][2];
            for (int i = 0; i < 12; i++)
            {
                data[i][0] = "Alt-F" + (i + 1);
                data[i][1] = null;
            }
            for (int j = 12; j < 22; j++)
            {
                data[j][0] = "Ctrl-" + (j - 12);
                data[j][1] = null;
            }
            tabModel = new DefaultTableModel(data, header)
            {
                @Override
                public boolean isCellEditable(int row, int column)
                {
                    return false;
                }
            };
            for (Iterator it = macroDoc.getRootElement().elements().iterator(); it.hasNext();)
            {
                Element childEle = (Element) it.next();
                for (int n = 0; n < tabModel.getRowCount(); n++)
                {
                    if (childEle.attributeValue("key").equals(tabModel.getValueAt(n, 0)))
                    {
                        tabModel.setValueAt(childEle.attributeValue("name"), n, 1);
                    }
                }
            }
            tblMacros.setModel(tabModel);
            tblMacros.getSelectionModel().addListSelectionListener(new ListSelectionListener()
            {
                @Override
                public void valueChanged(ListSelectionEvent e)
                {
                    selectedRowChanged();
                }
            });
        } catch (Exception ex)
        {
            logger.error("Error :" + ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }

    public void selectedRowChanged()
    {
        selectRow = tblMacros.getSelectedRow();
        logger.info("selectRow=" + selectRow);
        if (selectRow < 0)
        {
            return;
        }
        
        if (tabModel.getValueAt(selectRow, 1) == null)
        {
            btnClear.setEnabled(false);
            txfdName.setText("");
            ttarMacros.setText("");
        } else
        {
            btnClear.setEnabled(true);
            for (Iterator it = macroDoc.getRootElement().elementIterator(); it.hasNext();)
            {
                Element subEle = (Element) it.next();
                if (subEle.attributeValue("key").equals(tabModel.getValueAt(selectRow, 0)))
                {
                    txfdName.setText(subEle.attributeValue("name"));
                    ttarMacros.setText(subEle.getText());
                }
            }
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pnlManageMacros = new javax.swing.JPanel();
        scroPnlMacrosTable = new javax.swing.JScrollPane();
        tblMacros = new javax.swing.JTable();
        btnHelp = new javax.swing.JButton();
        lblName = new javax.swing.JLabel();
        txfdName = new javax.swing.JTextField();
        scrPnlMacros = new javax.swing.JScrollPane();
        ttarMacros = new javax.swing.JTextArea();
        btnClear = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("manageShortcuts")
        );
        setAlwaysOnTop(true);
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocationByPlatform(true);
        setMinimumSize(new java.awt.Dimension(572, 349));
        setResizable(false);

        scroPnlMacrosTable.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroPnlMacrosTable.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        tblMacros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {
                {null, null}
            },
            new String []
            {
                "", ""
            }
        ));
        tblMacros.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblMacros.setShowHorizontalLines(false);
        tblMacros.setShowVerticalLines(false);
        tblMacros.getTableHeader().setReorderingAllowed(false);
        scroPnlMacrosTable.setViewportView(tblMacros);
        if (tblMacros.getColumnModel().getColumnCount() > 0)
        {
            tblMacros.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("key")
            );
            tblMacros.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("name")
            );
        }

        btnHelp.setText(constBundle.getString("help"));

        lblName.setText(constBundle.getString("name"));

        txfdName.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyReleased(java.awt.event.KeyEvent evt)
            {
                txfdNameKeyReleased(evt);
            }
        });

        scrPnlMacros.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);

        ttarMacros.setColumns(20);
        ttarMacros.setRows(5);
        ttarMacros.addKeyListener(new java.awt.event.KeyAdapter()
        {
            public void keyReleased(java.awt.event.KeyEvent evt)
            {
                ttarMacrosKeyReleased(evt);
            }
        });
        scrPnlMacros.setViewportView(ttarMacros);

        btnClear.setText(constBundle.getString("clear"));
        btnClear.setEnabled(false);
        btnClear.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnClearActionPerformed(evt);
            }
        });

        btnSave.setText(constBundle.getString("saveMacrosButton"));
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnSaveActionPerformed(evt);
            }
        });

        btnCancle.setText(constBundle.getString("cancle"));
        btnCancle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnCancleActionPerformed(evt);
            }
        });

        btnOk.setText(constBundle.getString("ok"));
        btnOk.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnOkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlManageMacrosLayout = new javax.swing.GroupLayout(pnlManageMacros);
        pnlManageMacros.setLayout(pnlManageMacrosLayout);
        pnlManageMacrosLayout.setHorizontalGroup(
            pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlManageMacrosLayout.createSequentialGroup()
                .addComponent(scroPnlMacrosTable, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlManageMacrosLayout.createSequentialGroup()
                        .addComponent(lblName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txfdName))
                    .addComponent(scrPnlMacros)
                    .addGroup(pnlManageMacrosLayout.createSequentialGroup()
                        .addComponent(btnClear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(pnlManageMacrosLayout.createSequentialGroup()
                .addComponent(btnHelp)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnOk)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancle))
        );

        pnlManageMacrosLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnClear, btnSave});

        pnlManageMacrosLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnCancle, btnHelp, btnOk});

        pnlManageMacrosLayout.setVerticalGroup(
            pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlManageMacrosLayout.createSequentialGroup()
                .addGroup(pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scroPnlMacrosTable, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                    .addGroup(pnlManageMacrosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txfdName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(scrPnlMacros, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnClear, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnSave, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pnlManageMacrosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnOk)
                    .addComponent(btnCancle)
                    .addComponent(btnHelp))
                .addContainerGap())
        );

        pnlManageMacrosLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnClear, btnSave});

        pnlManageMacrosLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnCancle, btnHelp, btnOk});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlManageMacros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlManageMacros, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txfdNameKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_txfdNameKeyReleased
    {//GEN-HEADEREND:event_txfdNameKeyReleased
        // TODO add your handling code here:
        if (txfdName.getText() != null && !txfdName.getText().isEmpty())
        {
            btnSave.setEnabled(true);
        }
    }//GEN-LAST:event_txfdNameKeyReleased

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnSaveActionPerformed
    {//GEN-HEADEREND:event_btnSaveActionPerformed
        // TODO add your handling code here:
        logger.info("save");
        selectRow = tblMacros.getSelectedRow();
        if (this.txfdName.getText() == null || "".equals(this.txfdName.getText().trim())
                || this.ttarMacros.getText() == null || "".equals(this.ttarMacros.getText().trim()))
        {
            JOptionPane.showMessageDialog(this, constBundle.getString("giveNameAndQueryToMacros"),
                    constBundle.getString("saveMacros"), JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (selectRow<0)
        {
            return;
        }
        if (tabModel.getValueAt(selectRow, 1) != null)//change
        {
            tabModel.setValueAt(txfdName.getText(), selectRow, 1);
            for (Iterator it = macroDoc.getRootElement().elementIterator(); it.hasNext();)
            {
                Element subEle = (Element) it.next();
                if (subEle.attributeValue("key").equals(tabModel.getValueAt(selectRow, 0)))
                {
                    subEle.attribute("name").setValue(txfdName.getText());
                    subEle.setText(ttarMacros.getText());
                }
            }
        } else//add 
        {
            tabModel.setValueAt(this.txfdName.getText(), selectRow, 1);
            Element root = macroDoc.getRootElement();
            root.addElement("macro").addAttribute("key", String.valueOf(tabModel.getValueAt(selectRow, 0))).addAttribute("name",
                    txfdName.getText()).setText(ttarMacros.getText());
        }
        this.btnSave.setEnabled(false);
        this.btnClear.setEnabled(true);
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
         // TODO add your handling code here:
        logger.info("cancle");
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnClearActionPerformed
    {//GEN-HEADEREND:event_btnClearActionPerformed
        // TODO add your handling code here:
        logger.info("clear");
        if (selectRow < 0)
        {
            return;
        }
        tabModel.setValueAt("", selectRow, 1);
        //delete from xml
        Element root = macroDoc.getRootElement();
        for (Iterator it = root.elementIterator(); it.hasNext();)
        {
            Element ele = (Element) it.next();
            if (ele.attributeValue("key").equals(String.valueOf(tabModel.getValueAt(selectRow, 0))))
            {
                root.remove(ele);
            }
        }
        btnClear.setEnabled(false);
        txfdName.setText("");
        ttarMacros.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOkActionPerformed
    {//GEN-HEADEREND:event_btnOkActionPerformed
        // TODO add your handling code here:
        logger.info(evt.getActionCommand());
        try
        {
            XmlHelper xmlHelper = new XmlHelper();
            xmlHelper.writeDocumentToFile(macroDoc, macroFile);
            this.dispose();
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), 
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnOkActionPerformed

    private void ttarMacrosKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_ttarMacrosKeyReleased
    {//GEN-HEADEREND:event_ttarMacrosKeyReleased
        // TODO add your handling code here:
        if (this.ttarMacros.getText() != null)
        {
            this.btnSave.setEnabled(true);
        }
    }//GEN-LAST:event_ttarMacrosKeyReleased

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel lblName;
    private javax.swing.JPanel pnlManageMacros;
    private javax.swing.JScrollPane scrPnlMacros;
    private javax.swing.JScrollPane scroPnlMacrosTable;
    private javax.swing.JTable tblMacros;
    private javax.swing.JTextArea ttarMacros;
    private javax.swing.JTextField txfdName;
    // End of variables declaration//GEN-END:variables
}
