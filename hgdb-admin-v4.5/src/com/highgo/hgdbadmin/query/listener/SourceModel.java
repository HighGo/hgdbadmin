/* ------------------------------------------------ 
* 
* File: SourceModel.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\listener\SourceModel.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.listener;

import com.highgo.hgdbadmin.query.model.SQLResultInfoDTO;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yuanyuan
 * 
 *
 */
public class SourceModel
{
    private SQLResultInfoDTO data;
    private List<SourceListener> listeners = new ArrayList();

    public SourceModel(SQLResultInfoDTO data)
    {
        super();
        setSource(data);
    }

    public void removeListener(SourceListener listener)
    {
        listeners.remove(listener);
    }

    public void addListener(SourceListener listener)
    {
        listeners.add(listener);
    }

    public void setSource(SQLResultInfoDTO newData)
    {
        this.data = newData;
        for (int i = 0; i < listeners.size(); i++)
        {
            listeners.get(i).dataChanged(this);
        }
    }

    public SQLResultInfoDTO getSource()
    {
        return this.data;
    }
}
