/* ------------------------------------------------ 
* 
* File: SourceListener.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\listener\SourceListener.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.listener;

/**
 * @author Yuanyuan
 * 
 */
public interface SourceListener
{
    /**
     * Called when data has changed.
     *
     * @param dataObj - object that has changed
     */
    void dataChanged(Object dataObj);
}
