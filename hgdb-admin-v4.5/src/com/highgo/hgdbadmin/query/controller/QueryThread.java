/* ------------------------------------------------ 
* 
* File: QueryThread.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\controller\QueryThread.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.controller;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.query.listener.SourceModel;
import com.highgo.hgdbadmin.datatransfer.model.FormatInfoDTO;
import com.highgo.hgdbadmin.query.model.SQLResultInfoDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class QueryThread extends Thread
{

    private final Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private final ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    //private HelperInfoDTO helperInfo;
    private final Connection conn;
    private boolean isSingleSql;
    private String sqlSequecne;
    private int selectStartPosition;
    private SourceModel model;
    private FormatInfoDTO formate = null;

    public QueryThread(Connection conn, boolean isSingleSql, String sqlSequecne,
            int selectStartPosition, SourceModel model, FormatInfoDTO formate)
    {
        //this.helperInfo = helperInfo;
        this.conn = conn;
        this.isSingleSql = isSingleSql;
        this.sqlSequecne = sqlSequecne;
        this.selectStartPosition = selectStartPosition;
        this.model = model;
        this.formate = formate;
    }

    @Override
    public void run()
    {
        if (formate == null)
        {
            executeSQL(conn, isSingleSql, sqlSequecne, selectStartPosition, model);//, docMsg, docHistory
        } else
        {
            executeSQLToFile(conn, isSingleSql, sqlSequecne, selectStartPosition, model, formate);
        }
    }

    public void stopQuery()
    {
        logger.info("stoping");
        try
        {
            if (stmt != null)
            {
                stmt.cancel();
                stmt.close();
            }
            if (rs != null)
            {
                rs.close();
            }
//          result.setSuccess(false);
            logger.info("stoped");
        } catch (SQLException e)
        {
//            result.setConsumedMS(System.currentTimeMillis() - startTime);
//            result.setIsSuccess(false);
//            result.setError(e);
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
        }
    }

   

    /*
    //add begin by sunqk at 2019.4.19 for wrong split
    private int getSpeCharCount(String esInfo, char cSpec)
    {
        int iCount = 0;

        for(int i = 0; i < esInfo.length(); i++)
        {
            if(esInfo.charAt(i) == cSpec)
            {
                    System.out.print("found="+i+"\n\r");
                    iCount++;
            }
        }
        return iCount;
    }
    private int reBuildSqls(String[] beDealList, String[] beDstList)
    {
        int iDst = 0;
        String esItem;
        esItem = "";
        for(int item = 0; item < beDealList.length; item++)
        {
            esItem += beDealList[item];
            System.out.print("index:" + item + " info:" + esItem);
            if(((getSpeCharCount(esItem, '\''))%2) == 0)
            {
                //put esItem into outparams
                beDstList[iDst] = esItem;
                iDst++;
                esItem = "";
            }
            esItem +=";";
        }
        return iDst;
    }
    //add end by sunqk at 2019.4.19 for wrong split
     */
      
    private List<String> seperateSqls(boolean isSingle, String sqlSequence)
    {
        List<String> queryList = new ArrayList();
        if (isSingle)
        {
            queryList.add(sqlSequence);
            return queryList;
        }

        // sqls = sqlSequence.split(";[^']");    
        String[] array = (sqlSequence.trim()).split(";");

        String item = "";
        for (int i = 0; i < array.length; i++)
        {
            item = item + array[i];
            logger.debug("index=" + i + " item=" + item);
            if ((countCharFor(item) % 2) == 0)
            {
                queryList.add(item);
                item = "";
            } else
            {
                item = item + ";";
            }
        }
        return queryList;
    }
    private int countCharFor(String sepstr)
    {
        int quoteCount = 0;
        for (int i = 0; i < sepstr.length(); i++)
        {
            if (sepstr.charAt(i) == '\''
                    || (sepstr.charAt(i) == '\\' && (i + 1) < sepstr.length() && sepstr.charAt(i + 1) == '\''))
            {
                System.out.print("found=" + i + "\n\r");
                quoteCount++;
            }
        }
        return quoteCount;
    }

    private Statement stmt = null;
    private ResultSet rs = null;
    private long startTime;
    private void executeSQL(Connection conn, boolean isSingleSql, String sqlSequence, int selectStartPosition, SourceModel model)//, Document docMsg, Document docHistory
    {
        logger.debug("sqlSequecne=" + sqlSequence);

        SQLResultInfoDTO result = new SQLResultInfoDTO();
        int positionInAll = selectStartPosition;
        List<String> sqls = seperateSqls(isSingleSql, sqlSequence);
        //Connection conn = null;
        try
        {
            startTime = System.currentTimeMillis();
            //simple means ('Q' execute, no parse, no bind, text mode only)
            //conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName() + "?preferQueryMode=simple, helperInfo);
            boolean inBlock = this.isInTransactionBlock(sqlSequence);
            conn.setAutoCommit(!inBlock);
            String sql;
            /*
            //modify begin by sunqk at 2019.4.19
            for (int k = 0; k < iRealSqls;)
            //modify begin by sunqk at 2019.4.19
             */
            for (int k = 0; k < sqls.size();)
            {
                if (k + 1 < sqls.size())
                {
                    if (sqls.get(k + 1).trim().startsWith("'"))
                    {
                        sql = sqls.get(k) + ";" + sqls.get(k + 1);
                        k = k + 2;
                    } else
                    {
                        sql = sqls.get(k);
                        k = k + 1;
                    }
                } else
                {
                    sql = sqls.get(k);
                    k = k + 1;
                }
                sql = sql.trim();
                if (sql.isEmpty())
                {
                    positionInAll = positionInAll + sql.length() + 1;
                } else
                {
                    result.setSql(sql);
                    result.setFinished(false);
                    model.setSource(result);
                    logger.debug("middle update");
                    try
                    {
                        logger.debug(sql);
                        this.dealSelectStatement(sql, conn, result, inBlock);
                        result.setConsumedMS(System.currentTimeMillis() - startTime);
                        positionInAll = positionInAll + sql.length() + 1;
                        result.setSqlStartPosition(positionInAll);
                        result.setSuccess(true);
                    } catch (SQLException e)
                    {
                        result.setConsumedMS(System.currentTimeMillis() - startTime);
                        result.setSuccess(false);
                        result.setError(e);
                        logger.error(e.getMessage());
                        e.printStackTrace(System.out);
                        logger.error("break becauseof sql error: errorCode=" + e.getSQLState());
                        //break;//throw new Exception(e);
                    } finally
                    {
                        JdbcHelper.close(rs);
                        result.setFinished(true);
                        model.setSource(result);
                        logger.debug("Close ResultSet");
                        //addResultToDoc(attr, docMsg, docHistory, result);
                    }
                }
                if (!result.isSuccess())
                {
                    break;
                }
            }
            if (!conn.getAutoCommit())
            {
                conn.commit();
                conn.setAutoCommit(true);
            }
        } catch (SQLException e)
        {
            result.setSql(sqlSequence);
            result.setConsumedMS(System.currentTimeMillis() - startTime);
            result.setSuccess(false);
            result.setError(e);
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            //JdbcHelper.close(conn);

            result.setFinished(true);
            model.setSource(result);
            logger.debug("Close Resource");
        }
    }
    private void dealSelectStatement(String sql, Connection conn, SQLResultInfoDTO result, boolean inBlock) throws SQLException
    {
        logger.debug("inBlock = " + inBlock + ",sql=" + sql);
        if (sql.lastIndexOf(";") == (sql.length() - 1))
        {
            sql = sql.substring(0, sql.length() - 1);
        }
        if (inBlock)
        {
            conn.setAutoCommit(false);
        }
        stmt = conn.createStatement();
        stmt.setFetchSize(1000);
        //rs = stmt.executeQuery(sql);    
        if (!stmt.execute(sql))
        {
            result.setAffectedRow(stmt.getUpdateCount());
            result.setHeaders(null);
            result.setData(null);
            result.setReturnedRow(-1);
            result.setWarning(stmt.getWarnings() == null ? null : stmt.getWarnings());
            stmt = null;
        } else //select
        {
            rs = stmt.getResultSet();
            ResultSetMetaData mdata = rs.getMetaData();
            int column = mdata.getColumnCount();
            String[] headers = new String[column];
            for (int i = 1; i <= mdata.getColumnCount(); i++)
            {
                headers[i - 1] = mdata.getColumnName(i);
            }
            result.setHeaders(headers);
            List<Object[]> data = new ArrayList<>();
            int row = 0;
            while (rs.next())
            {
                row++;
                if (row <= 1000)
                {
                    Object[] rowData = new Object[column];
                    for (int j = 1; j <= column; j++)
                    {
                        rowData[j - 1] = this.getValue(rs, j);
                        if (rowData[j - 1] == null)
                        {
                            rowData[j - 1] = "<NULL>";//to dispaly NUll result as <NULL> in query result
                        }
                    }
                    data.add(rowData);
                }
            }
            result.setData(data);
            result.setReturnedRow(row);
            result.setWarning(stmt.getWarnings() == null ? null : stmt.getWarnings());
            result.setAffectedRow(-1);
            rs = null;
            stmt = null;
        }
        if (inBlock)
        {
            conn.setAutoCommit(true);
        }
        logger.debug("success");
    }

    private boolean isInTransactionBlock(String sqlSequence)
    {
        String s = sqlSequence.trim().toLowerCase();
        if (s.startsWith("declare"))
        {
            return true;
        } else
        {
            return false;
        }
    }
    private Object getValue(ResultSet rs, int j) throws SQLException
    {
        //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
        //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
        logger.debug("type=" + rs.getMetaData().getColumnClassName(j));
        switch (rs.getMetaData().getColumnClassName(j))
        {
            case "com.highgo.jdbc.util.PGmoney":
            case "java.lang.Boolean":
            case "java.sql.SQLXML":
                return rs.getString(j);
            case "[B":
                return rs.getString(j);
            case "java.sql.ResultSet":
                return rs.getString(j);
            default:
                return rs.getObject(j);
        }
    }

    private void executeSQLToFile(Connection conn, boolean isSingleSql, String sqlSequence,
            int selectStartPosition, SourceModel model, FormatInfoDTO formate)
    {
        logger.debug("sqlSequecne=" + sqlSequence);
        logger.debug("formate=" + formate.getColumnQuoter() + "," + formate.getRowSeparator() + "," + formate.getFilePath());
        SQLResultInfoDTO result = new SQLResultInfoDTO();
        int positionInAll = selectStartPosition;
        List<String> sqls = seperateSqls(isSingleSql, sqlSequence);
        //Connection conn = null;
        try
        {
            startTime = System.currentTimeMillis();
            //conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(), helperInfo);       
            boolean inBlock = this.isInTransactionBlock(sqlSequence);
            conn.setAutoCommit(!inBlock);
            for (String sql : sqls)
            {
                sql = sql.trim();
                if (!sql.isEmpty())
                {
                    result.setSql(sql);
                    result.setFinished(false);
                    model.setSource(result);
                    try
                    {
                        logger.info(sql);
                        this.dealAndSaveSelect(sql, conn, result, inBlock);
                        result.setConsumedMS(System.currentTimeMillis() - startTime);
                        positionInAll = positionInAll + sql.length() + 1;
                        result.setSqlStartPosition(positionInAll);
                        result.setSuccess(true);
                    } catch (SQLException e)
                    {
                        result.setConsumedMS(System.currentTimeMillis() - startTime);
                        result.setSuccess(false);
                        result.setError(e);
                        logger.error(e.getMessage());
                        e.printStackTrace(System.out);
                        logger.info("break becauseof sql error:errorCode=" + e.getSQLState());
                        break; //throw new Exception(e);
                    } finally
                    {
                        JdbcHelper.close(rs);
                        result.setFinished(true);
                        model.setSource(result);
                        logger.info("Close ResultSet");
                        //addResultToDoc(attr, docMsg, docHistory, result);
                    }
                } else
                {
                    positionInAll = positionInAll + sql.length() + 1;
                }
            }
            if (!conn.getAutoCommit())
            {
                conn.commit();
                conn.setAutoCommit(true);
            }
        } catch (SQLException e)
        {
            result.setSql(sqlSequence);
            result.setConsumedMS(System.currentTimeMillis() - startTime);
            result.setSuccess(false);
            result.setError(e);
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
        } catch (UnsupportedEncodingException | FileNotFoundException oe)
        {
            logger.error(oe.getMessage());
            oe.printStackTrace(System.out);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            //JdbcHelper.close(conn);

            result.setFinished(true);
            model.setSource(result);
            logger.debug("Close Resource");
        }
    }

    private void dealAndSaveSelect(String sql, Connection conn, SQLResultInfoDTO result, boolean inBlock)
            throws UnsupportedEncodingException, FileNotFoundException, SQLException
    {
        PrintWriter pw = null;
        if (formate.getEncoding().equals("UTF-8"))
        {
            pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(formate.getFilePath()), formate.getEncoding()));
        } else
        {
            pw = new PrintWriter(formate.getFilePath());
        }
        if (!inBlock)
        {
            conn.setAutoCommit(false);
        }
        stmt = conn.createStatement();
        stmt.setFetchSize(1000);
        //rs = stmt.executeQuery(sql);
        if (!stmt.execute(sql))
        {
            result.setAffectedRow(stmt.getUpdateCount());
            result.setHeaders(null);
            result.setData(null);
            result.setReturnedRow(-1);
            stmt = null;
        } else
        {
            rs = stmt.getResultSet();
            ResultSetMetaData mdata = rs.getMetaData();
            int column = mdata.getColumnCount();
            if (formate.isWithHeader())
            {
                StringBuilder headers = new StringBuilder();
                for (int i = 1; i <= column; i++)
                {
                    headers.append(formate.getColumnQuoter()).append(mdata.getColumnName(i))
                            .append(formate.getColumnQuoter());
                    if (i < column)
                    {
                        headers.append(formate.getColumnSeparator());
                    }
                }
                headers.append(formate.getRowSeparator());
                write(pw, headers.toString());
            }
            StringBuilder data = new StringBuilder();
            int row = 0;
            while (rs.next())
            {
                row++;
                for (int j = 1; j <= column; j++)
                {
                    data.append(formate.getColumnQuoter()).append(this.getValue(rs, j)).append(formate.getColumnQuoter());
                    if (j < column)
                    {
                        data.append(formate.getColumnSeparator());
                    }
                }
                data.append(formate.getRowSeparator());
                this.write(pw, data.toString());
                data.delete(0, data.length());
            }
            result.setHeaders(null);
            result.setData(null);
            result.setReturnedRow(row);
            result.setAffectedRow(-1);
            if (pw != null)
            {
                pw.close();
            }
            rs = null;
            stmt = null;
        }
        if (!inBlock)
        {
            conn.setAutoCommit(true);
        }
    }
    private void write(PrintWriter pw, String content)
    {
        pw.write(content);
        // pw.println();
        pw.flush();
    }

}
