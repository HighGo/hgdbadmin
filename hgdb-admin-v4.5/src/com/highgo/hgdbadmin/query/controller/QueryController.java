/* ------------------------------------------------ 
* 
* File: QueryController.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\controller\QueryController.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.controller;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class QueryController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(this.getClass());
     
    private static QueryController qc;
    public static QueryController getInstance()
    {
        if (qc == null)
        {
            qc = new QueryController();
        }
        return qc;
    }
            
    
    private String getObjectSQL(String type)
    {
        logger.info("Enter");
        if (type == null)
        {
            logger.error("Error:objType is null, do nothing, return null.");
            return null;
        }
        logger.info("type = " + type);
        StringBuilder sql = new StringBuilder();
        switch (type)
        {
            case "user":
                sql.append("SELECT aut.oid, aut.rolname, 'user' AS type")
                        .append(" FROM pg_authid aut")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON aut.oid = des.objoid ")
                        .append(" WHERE aut.rolcanlogin = true")
                        .append(" ORDER BY aut.rolname ");
                break;
            case "database":
                sql.append("SELECT dat.oid, dat.datname, 'database' AS type")
                        .append(" FROM pg_database dat")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON dat.oid = des.objoid")
                        .append(" WHERE dat.datname NOT IN('template1','template0') ")
                        .append(" ORDER BY dat.datname");
                break;
            default:
                logger.error("There's no sql for this type.");
                break;
        }
        logger.info("Return: sql = " + sql.toString());
        return sql.toString();
    }

    public List<String> getObjectList(HelperInfoDTO helperInfo, String type)
    {
        logger.info("Enter:type=" + type);
        List<String> objList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return objList;
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = this.getObjectSQL(type);
        logger.info(sql);
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
//                ObjectInfoDTO obj = new ObjectInfoDTO();
//                obj.setOid(rs.getString(1));
//                obj.setName(rs.getString(2));
//                obj.setType(rs.getString(3));
                objList.add(rs.getString(2));
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        return objList;
    }

}
