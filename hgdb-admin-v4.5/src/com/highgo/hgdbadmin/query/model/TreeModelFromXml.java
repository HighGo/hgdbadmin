/* ------------------------------------------------ 
* 
* File: TreeModelFromXml.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\model\TreeModelFromXml.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.model;

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import org.dom4j.Document;
import org.dom4j.Element;

/**
 *
 * @author ZhangYueyun
 */
public class TreeModelFromXml implements TreeModel
{
    private Document doc;

    public TreeModelFromXml(Document doc)
    {
        this.doc = doc;
    }

    @Override
    public Object getRoot()
    {
        return doc.getRootElement();
    }

    @Override
    public Object getChild(Object parent, int index)
    {
        Element els = (Element) parent;
        return els.elements().get(index);
    }

    @Override
    public int getChildCount(Object parent)
    {
        Element els = (Element) parent;
        return els.elements().size();
    }

    @Override
    public boolean isLeaf(Object node)
    {
        Element els = (Element) node;
        return els.getQName().getName().equals("favourite");
    }

    @Override
    public int getIndexOfChild(Object parent, Object child)
    {
        Element els = (Element) parent;
        return els.elements().indexOf(child);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue)
    {

    }

    public void fireTreeChanged(Object[] path, int[] childIndices, Object[] children)
    {
        this.fireTreeStructureChanged(path, childIndices, children);
    }

    protected void fireTreeStructureChanged(Object[] path, int[] childIndices, Object[] children)
    {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        TreeModelEvent e = null;
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2)
        {
            if (listeners[i] == TreeModelListener.class)
            {
                // Lazily create the event:
                if (e == null)
                {
                    e = new TreeModelEvent(this, path, childIndices, children);
                }
                ((TreeModelListener) listeners[i + 1]).treeStructureChanged(e);
            }
        }
    }

    protected void fireTreeNodesInserted(Object[] path, int[] childIndices, Object[] children)
    {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        TreeModelEvent e = null;
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2)
        {
            if (listeners[i] == TreeModelListener.class)
            {
                if (e == null)
                {
                    e = new TreeModelEvent(this, path, childIndices, children);
                }
                ((TreeModelListener) listeners[i + 1]).treeNodesInserted(e);
            }
        }
    }

    protected void fireTreeNodesChanged(Object[] path, int[] childIndices, Object[] children)
    {
        Object[] listeners = listenerList.getListenerList();
        TreeModelEvent e = null;
        for (int i = listeners.length - 2; i >= 0; i -= 2)
        {
            if (listeners[i] == TreeModelListener.class)
            {
                if (e == null)
                {
                    e = new TreeModelEvent(this, path, childIndices, children);
                }
                ((TreeModelListener) listeners[i + 1]).treeNodesChanged(e);
            }
        }
    }

    protected void fireTreeNodesRemoved(Object[] path, int[] childIndices, Object[] children)
    {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        TreeModelEvent e = null;
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2)
        {
            if (listeners[i] == TreeModelListener.class)
            {
                if (e == null)
                {
                    e = new TreeModelEvent(this, path, childIndices, children);
                }
                ((TreeModelListener) listeners[i + 1]).treeNodesRemoved(e);
            }
        }
    }

    @Override
    public void addTreeModelListener(TreeModelListener l)
    {
        listenerList.add(TreeModelListener.class, l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l)
    {
        listenerList.remove(TreeModelListener.class, l);
    }
    
    protected EventListenerList listenerList = new EventListenerList();

}
