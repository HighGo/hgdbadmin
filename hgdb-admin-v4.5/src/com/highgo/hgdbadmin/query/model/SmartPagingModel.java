/* ------------------------------------------------ 
* 
* File: SmartPagingModel.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\model\SmartPagingModel.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.model;

import com.highgo.hgdbadmin.datamanager.paging.RowIdHeaderModel;
import com.highgo.hgdbadmin.datamanager.paging.PageTurnIcon;
import com.highgo.hgdbadmin.datamanager.paging.RowHeaderResizeListener;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 *
 * @author Yuanyuan
 *
 * A larger table model that performs "paging" of its data. This model reports a
 * small number of rows (like 1000 or so) as a "page" of data. You can switch
 * pages to view all of the rows as needed using the pageDown() and pageUp()
 * methods. Presumably, access to the other pages of data is dictated by other
 * GUI elements such as up/down buttons, or maybe a Spinner that allows you
 * to enter the page number you want to display.
 * 
 */
public class SmartPagingModel extends DefaultTableModel
{
    private final Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private final Object[] header;
    private final Class[] typeArray;  
    //private HelperInfoDTO helperInfo;
    private final Connection conn;
    private final String sql;
    
    private final int pageSize;
    private int pageOffset;//pageId = pageOffSet+1;
    private List<Object[]> thisPageData;
    private int thisPageRow;

    public SmartPagingModel(Connection conn, String sql,
            int pageSize, Class[] typeArray, Object[] headerArray, List<Object[]> dataOfFirstPage)
    {
        this.conn = conn;
        this.sql = sql;
        this.typeArray = typeArray;
        this.header = headerArray;
        this.pageSize = pageSize;
        this.pageOffset = 0;        
        this.thisPageData = dataOfFirstPage;
        thisPageRow = thisPageData.size();
        //refreshDataOfThePage();
    }

    @Override
    public boolean isCellEditable(int row, int column)
    {
        return false;
    }    
    @Override
    public int getRowCount()// Return values appropriate for the visible table part.
    {
        if (thisPageData == null)
        {
            return Math.min(pageSize, thisPageRow);
        } else
        {
            return thisPageData.size();
        }
    }
    @Override
    public int getColumnCount()
    {
        return header.length;
    }
    @Override
    public String getColumnName(int col)
    {
        if (col < 0)
        {
            return null;
        } else
        {
            return header[col].toString();
        }
    }
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        if(columnIndex<0)
        {
            return null;
        }
        else if(typeArray==null)//this is for 
        {
            return Object.class;
        } else
        {
            return typeArray[columnIndex];
        }
    }
    // Work only on the visible part of the table.  
    @Override
    public Object getValueAt(int row, int col)
    {
        if (row < 0 || col < 0)
        {
            return null;
        } else
        {
            return thisPageData.get(row)[col];
        }
    }
    // Use this method to figure out which page you are on(pageId = pageOffset+1). It starts from 0.
    public int getPageOffset()
    {
        return pageOffset;
    }
    public int getPageSize()
    {
        return pageSize;
    }

    private List<Object[]> getDataList(String sql, int pageOffset, int pageSize) throws Exception
    {
        logger.info("sqlSequecne=" + sql);
        List<Object[]> dataList = new ArrayList();
        if (conn == null)
        {
            logger.error("conn is null, return a empty list.");
            return dataList;
        }        
        if (sql.isEmpty())
        {
            logger.error("sql is empty, return a empty list.");
            return dataList;
        }        
        //Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            //conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(), helperInfo);
            
            if (sql.lastIndexOf(";") == (sql.length() - 1))
            {
                sql = sql.substring(0, sql.length() - 1);
            }
            logger.debug(sql + " limit " + pageSize + " offset " + (pageOffset * pageSize));            
            
            stmt = conn.prepareStatement(sql + " limit ? offset ?");
            stmt.setInt(1, pageSize);
            stmt.setInt(2, pageOffset * pageSize);
            rs = stmt.executeQuery();
            int column = rs.getMetaData().getColumnCount();
            //int rowId = pageOffset * pageSize + 1;
            Object[] rowData = null;
            while (rs.next())
            {
                //logger.info("rowId=" + rowId);
                rowData = new Object[column];
                for (int j = 1; j <= column; j++)
                {
                    rowData[j - 1] = this.getValue(rs, j);
                }
                dataList.add(rowData);
                //rowId++;                
            }
        } catch (SQLException e)
        {
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
            throw new Exception(e);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            //JdbcHelper.close(conn);
        }
        return dataList;
    }
    private Object getValue(ResultSet rs, int j) throws SQLException
    {
        //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
        //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
        switch (rs.getMetaData().getColumnClassName(j))
        {
            case "com.highgo.jdbc.util.PGmoney":
            case "java.lang.Boolean":
            case "java.sql.SQLXML":
                return rs.getString(j);
            case "[B":
                return rs.getString(j);
            default:
                return rs.getObject(j);
        }
    }
        
    //Update the page offset and fire a data changed (all rows).  
    public boolean pageDown() throws Exception
    {
        List<Object[]> newData = this.getDataList( sql, pageOffset+1, pageSize);
        logger.info("dataSize=" + newData.size());
        if (newData.size() > 0)
        {
            pageOffset++;
            thisPageData = newData;
            thisPageRow = thisPageData.size();
            this.fireTableDataChanged();
            logger.info("Turn up to page " + (pageOffset+1));
            return true;
        } else
        {
            logger.debug("Last page " + (pageOffset+1) + " cannot turn down.");
            return false;
        }
    }
    // Update the page offset and fire a data changed (all rows).  
    public void pageUp() throws Exception
    {
        if (pageOffset <= 0)
        {
            logger.debug("First page cannot turn up.");
        } else
        {
            pageOffset--;
            thisPageData = this.getDataList(sql, pageOffset, pageSize);
            thisPageRow = thisPageData.size();
            this.fireTableDataChanged();
            logger.info("Turn up to page " + (pageOffset + 1));
        }
    }
    /* 
    * We provide our own version of a scrollpane that includes  
    * the page up and page down buttons by default.  
    */
    public static JScrollPane createPagingScrollPaneForTable(JTable tbl)
    {
        JScrollPane jsp = new JScrollPane(tbl);
        TableModel tmodel = tbl.getModel();
        // Don't choke if this is called on a regular table . 
        if (!(tmodel instanceof SmartPagingModel))
        {
            return jsp;
        }
        //build the real scrollpane  
        final SmartPagingModel pagingModel = (SmartPagingModel) tmodel;
        final JButton upButton = new JButton(new PageTurnIcon(PageTurnIcon.UP));
        final JButton downButton = new JButton(new PageTurnIcon(PageTurnIcon.DOWN));
        upButton.setEnabled(true); // starts off at 0, so can't go up
        downButton.setEnabled(true);

        upButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                try
                {
                    pagingModel.pageUp();
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex, 
                            ResourceBundle.getBundle("constants").getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        downButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                try
                {
                    boolean isDownable = pagingModel.pageDown();
                    JButton downButton = (JButton) ae.getSource();
                    downButton.setEnabled(isDownable);//last page cannot turn down
                    upButton.setEnabled(true);
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex, 
                            ResourceBundle.getBundle("constants").getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        //Turn on the scrollbars; otherwise we won't get our corners.
        jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jsp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        //Add in the corners (page up/down).
        jsp.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, upButton);
        jsp.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, downButton);

        //row header to display row id
        final RowIdHeaderModel rowHeader = new RowIdHeaderModel(pagingModel);
        JTable headerTable = new JTable(rowHeader);
        headerTable.setFont(new Font("SansSerif", Font.BOLD, 12));
        headerTable.setBackground(new Color(240, 240, 240));
        headerTable.setEnabled(false);//disable selection
        headerTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        headerTable.getTableHeader().setReorderingAllowed(false);//disable reorder
//        headerTable.setIntercellSpacing(new Dimension(0, 0));
//        Dimension d = headerTable.getPreferredScrollableViewportSize();
//        d.width = headerTable.getPreferredSize().width;
//        headerTable.setPreferredScrollableViewportSize(d);
        headerTable.setRowHeight(tbl.getRowHeight());
        int columnWidth = 30;
        headerTable.getColumnModel().getColumn(0).setPreferredWidth(columnWidth);
        headerTable.getColumnModel().getColumn(0).setMaxWidth(100);
        headerTable.setPreferredScrollableViewportSize(new Dimension(columnWidth, 0));

        jsp.setCorner(JScrollPane.UPPER_LEFT_CORNER, headerTable.getTableHeader());
        jsp.setRowHeaderView(headerTable);
        headerTable.addComponentListener(new RowHeaderResizeListener(jsp, jsp.getRowHeader(), headerTable, tbl));
        pagingModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                rowHeader.fireTableDataChanged();//refresh row id
            }
        });        
        return jsp;
    }

}
