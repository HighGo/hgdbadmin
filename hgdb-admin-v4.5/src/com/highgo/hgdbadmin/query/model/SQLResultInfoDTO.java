/* ------------------------------------------------ 
* 
* File: SQLResultInfoDTO.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\query\model\SQLResultInfoDTO.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.model;

import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 * for sql query result
 * @author Yuanyuan
 */
public class SQLResultInfoDTO
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private String sql;
    private boolean finished;
    private boolean success;
    private SQLWarning warning;
    private long consumedMS;
    private int affectedRow;//update,delete,..
    private int returnedRow;
    private String[] headers;
    private List<Object[]> data;
    private SQLException error;
    private int sqlStartPosition;

    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)
    {
        this.sql = sql;
    }

    public boolean isFinished()
    {
        return finished;
    }

    public void setFinished(boolean finished)
    {
        this.finished = finished;
    }

    public boolean isSuccess()
    {
        return success;
    }

    public void setSuccess(boolean isSuccess)
    {
        this.success = isSuccess;
    }

    public SQLWarning getWarning()
    {
        return warning;
    }

    public void setWarning(SQLWarning warning)
    {
        this.warning = warning;
    }

    public long getConsumedMS()
    {
        return consumedMS;
    }

    public void setConsumedMS(long consumedMS)
    {
        this.consumedMS = consumedMS;
    }

    public int getAffectedRow()
    {
        return affectedRow;
    }

    public void setAffectedRow(int affectedRow)
    {
        this.affectedRow = affectedRow;
    }

    public int getReturnedRow()
    {
        return returnedRow;
    }

    public void setReturnedRow(int returnedRow)
    {
        this.returnedRow = returnedRow;
    }

    public String[] getHeaders()
    {
        return headers;
    }

    public void setHeaders(String[] headers)
    {
        this.headers = headers;
    }

    public List<Object[]> getData()
    {
        return data;
    }

    public void setData(List<Object[]> data)
    {
        this.data = data;
    }

    public SQLException getError()
    {
        return error;
    }

    public void setError(SQLException error)
    {
        this.error = error;
    }

    public String getErrorWord()
    {
        if (error != null && error.getMessage() != null
                && error.getMessage().indexOf("\"") >= 0)
        {
            return error.getMessage().substring(error.getMessage().indexOf("\"") + 1, error.getMessage().lastIndexOf("\""));
        } else
        {
            return "";
        }
    }

    public int getErrorPositionInLine()
    {
        if (error!=null && error.getMessage()!=null 
                && error.getMessage().lastIndexOf("：") == -1)//not exist.usually cause by manual stoping thread
        {
            return -1;
        } else
        {
            int row = 0;
            try
            {
                row = Integer.valueOf(error.getMessage().substring(error.getMessage().lastIndexOf("：") + 1));
            } catch (NumberFormatException ex)
            {
                logger.error(ex.getMessage());
            }
            return row-1;
        }
    }

    public int getSqlStartPosition()
    {
        return sqlStartPosition;
    }

    public void setSqlStartPosition(int sqlStartPosition)
    {
        this.sqlStartPosition = sqlStartPosition;
    }
    
    
}
