/* ------------------------------------------------ 
* 
* File: NewConnectionDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/query/util/NewConnectionDialog.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.util;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.query.controller.QueryController;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.util.ResourceBundle;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class NewConnectionDialog extends JDialog
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private HelperInfoDTO oldHelperInfo;
    private HelperInfoDTO newHelperInfo;
    private boolean isSuccess;
    /**
     * Creates new form NewConnectionDialog
     * @param parent
     * @param modal
     * @param oldHelperInfo
     */
    public NewConnectionDialog(Frame parent, boolean modal, HelperInfoDTO oldHelperInfo)
    {
        super(parent, modal);
        this.oldHelperInfo = oldHelperInfo;
        isSuccess = false;
        initComponents();
        txfdHost.setText(oldHelperInfo.getHost());
        txfdPort.setText(oldHelperInfo.getPort());
        QueryController qc = QueryController.getInstance();
        for (String db : qc.getObjectList(oldHelperInfo, "database"))
        {
            cbDatabase.addItem(db);
        }
        for (String user : qc.getObjectList(oldHelperInfo, "user"))
        {
            cbUser.addItem(user);
        }
        cbDatabase.setSelectedIndex(-1);
        cbUser.setSelectedIndex(-1);
        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnHelpActionPerformed(e);
            }
        });
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancleActionPerformed(e);
            }
        });
        btnOk.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkActionPerformed(e);
            }
        });
        
        
        KeyAdapter keyTypedAction = new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                if (evt.getComponent() != null && evt.getComponent() == txfdPort)
                {
                    txfdIntegerValueKeyTyped(evt);
                    if (evt.getKeyChar() != '\0')//when valid input happens
                    {
                        cbDatabase.removeAllItems();
                        cbUser.removeAllItems();
                    }
                } else
                {
                    cbDatabase.removeAllItems();
                    cbUser.removeAllItems();
                }
            }            
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableAction();
            }
        };
        txfdHost.addKeyListener(keyTypedAction);
        txfdPort.addKeyListener(keyTypedAction);        
                
        ItemListener itemListener = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                btnOkEnableAction();
            }
        };
        cbDatabase.addItemListener(itemListener);
        cbUser.addItemListener(itemListener);

        KeyAdapter keyReleasedAction = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                btnOkEnableAction();
            }
        };
        cbDatabase.getEditor().getEditorComponent().addKeyListener(keyReleasedAction);
        cbUser.getEditor().getEditorComponent().addKeyListener(keyReleasedAction);
        pfPwd.addKeyListener(keyReleasedAction);
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        lblHost = new javax.swing.JLabel();
        txfdHost = new javax.swing.JTextField();
        lblPort = new javax.swing.JLabel();
        cbDatabase = new javax.swing.JComboBox();
        lblDatabase = new javax.swing.JLabel();
        cbUser = new javax.swing.JComboBox();
        lblUser = new javax.swing.JLabel();
        txfdPort = new javax.swing.JTextField();
        btnOk = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        lblPassword = new javax.swing.JLabel();
        pfPwd = new javax.swing.JPasswordField();
        btnHelp = new javax.swing.JButton();
        lblOnSSL = new javax.swing.JLabel();
        cbOnSSL = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("connectToServer"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/image/connect.png")));
        setResizable(false);

        lblHost.setText(constBundle.getString("host"));

        txfdHost.setText("127.0.0.1");

        lblPort.setText(constBundle.getString("port"));

        cbDatabase.setEditable(true);

        lblDatabase.setText(constBundle.getString("database"));

        cbUser.setEditable(true);

        lblUser.setText(constBundle.getString("user"));

        txfdPort.setText("5866");

        btnOk.setText(constBundle.getString("ok"));
        btnOk.setEnabled(false);

        btnCancle.setText(constBundle.getString("cancle"));

        lblPassword.setText(constBundle.getString("pwd"));

        btnHelp.setText(constBundle.getString("help"));

        lblOnSSL.setText(constBundle.getString("onSSL"));

        cbOnSSL.setSelected(true);
        cbOnSSL.setIconTextGap(0);
        cbOnSSL.setMargin(new java.awt.Insets(2, 0, 2, 0));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnHelp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 72, Short.MAX_VALUE)
                        .addComponent(btnOk)
                        .addGap(10, 10, 10)
                        .addComponent(btnCancle))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblOnSSL, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(0, 0, 0)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblPort, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblDatabase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lblUser, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblHost, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(lblPassword, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pfPwd)
                            .addComponent(txfdHost)
                            .addComponent(txfdPort)
                            .addComponent(cbDatabase, 0, 284, Short.MAX_VALUE)
                            .addComponent(cbUser, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbOnSSL)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblHost)
                    .addComponent(txfdHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPort)
                    .addComponent(txfdPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbDatabase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDatabase))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUser))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPassword)
                    .addComponent(pfPwd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOnSSL)
                    .addComponent(cbOnSSL))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk)
                    .addComponent(btnCancle)
                    .addComponent(btnHelp))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnHelpActionPerformed(ActionEvent evt)
    {
        Desktop desktop = Desktop.getDesktop();
        try
        {
            desktop.browse(new URI("http://www.highgo.com.cn/docs/pgAdmin1-14-Online-Documentation/Query%20Tool.htm"));
        } catch (IOException | URISyntaxException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
    private void btnCancleActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        isSuccess = false;
        this.dispose();
    }
        
    private void btnOkActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        Connection conn = null;
        try
        {
            newHelperInfo = new HelperInfoDTO();
            newHelperInfo.setHost(txfdHost.getText());
            newHelperInfo.setPort(txfdPort.getText());
            newHelperInfo.setDbName(cbDatabase.getSelectedItem().toString());
            newHelperInfo.setUser(cbUser.getSelectedItem().toString());
            newHelperInfo.setPwd(String.valueOf(pfPwd.getPassword()));
            newHelperInfo.setOnSSL(cbOnSSL.isSelected());
            //newHelperInfo.setPartURL("jdbc:highgo://" + newHelperInfo.getHost() + ":" + newHelperInfo.getPort() + "/");
            conn = JdbcHelper.getConnection(newHelperInfo.getPartURL() + newHelperInfo.getDbName(), newHelperInfo);
            isSuccess =true;
            this.dispose();
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        } finally
        {
            JdbcHelper.close(conn);
        }
    }                                 
   
    private void btnOkEnableAction()
    {
        String host = txfdHost.getText();
        String port = txfdPort.getText();
        String db = cbDatabase.getEditor().getItem().toString();//cbDatabase.getSelectedItem() == null ? "" : cbDatabase.getSelectedItem().toString();
        String user = cbUser.getEditor().getItem().toString();//cbUser.getSelectedItem() == null ? "" : cbUser.getSelectedItem().toString();
        String pwd = String.valueOf(pfPwd.getPassword());
        //logger.debug("host=" + host + ",port=" + port + ",db=" + db + ",user=" + user + ",pwd=" + pwd);
        if (host.isEmpty() || port.isEmpty() || db.isEmpty() || user.isEmpty() || pwd.isEmpty())
        {
            btnOk.setEnabled(false);
        }else
        {
            btnOk.setEnabled(true);
        }
    }
    
    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could only be entered integer(>=0).
        char keyCh = evt.getKeyChar();
        Character.isDigit(keyCh);
        //logger.debug("keych:" + keyCh);
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh == '-')
            {
                evt.setKeyChar('\0');
            } else if (keyCh != '') //回车字符
            {
                evt.setKeyChar('\0');
            }
        }       
    }
    
    public HelperInfoDTO getNewHelperInfo()
    {
        return this.newHelperInfo;
    }
    public boolean showDialog()
    {
        this.setVisible(true);
        logger.info("isSuccess="+isSuccess);
        return isSuccess;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOk;
    private javax.swing.JComboBox cbDatabase;
    private javax.swing.JCheckBox cbOnSSL;
    private javax.swing.JComboBox cbUser;
    private javax.swing.JLabel lblDatabase;
    private javax.swing.JLabel lblHost;
    private javax.swing.JLabel lblOnSSL;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblPort;
    private javax.swing.JLabel lblUser;
    private javax.swing.JPasswordField pfPwd;
    private javax.swing.JTextField txfdHost;
    private javax.swing.JTextField txfdPort;
    // End of variables declaration//GEN-END:variables
}
