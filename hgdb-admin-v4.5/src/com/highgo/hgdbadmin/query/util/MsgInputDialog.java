/* ------------------------------------------------ 
* 
* File: MsgInputDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/query/util/MsgInputDialog.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.query.util;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Liu Yuanyuan
 */
public class MsgInputDialog
{
     private JTextField txfd;

    /*
    ok return 0;
    cancle return 2;
    */
    public int showDialog(Component parent, Icon icon, String msg, String title, String initValue)
    {
        txfd = new JTextField();
        if (initValue != null)
        {
            txfd.setText(initValue);
        }
        Object[] obj =
        {
            msg, txfd
        };
        return JOptionPane.showOptionDialog(parent, obj, title, JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE, icon, null, null);
    }

    public String getInput()
    {
        return txfd.getText();
    }
}
