/* ------------------------------------------------ 
* 
* File: MyFileFilter.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/query/util/MyFileFilter.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.query.util;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 * to do
 * overwrite file filter open or save file
 *
 */
@Deprecated
public class MyFileFilter extends FileFilter
{    
     String extension; //file extension
     String description; //file description

     public MyFileFilter(String extension, String description)
     {
        this.extension = extension; //set file extention
        this.description = description;//set file description
      }
     
    //show all directory,only comply the extension file
    @Override
    public boolean accept(File f)
    {
        if(f != null)
        {
            if (f.isDirectory())
            {
                return true;
            }
            String ext = getExtension();
            if (ext != null && ext.equalsIgnoreCase(this.extension))
            {
                return true;
            } 
        }
         return false;
    }

    @Override
    public String getDescription()
    {
        return this.description;
    }
    
     //return the file extension
     public String getExtension()
     {
         return this.extension;
//            if(f != null)
//            {
//                String fileName = f.getName();
//                int i = fileName.lastIndexOf(".");
//                if(i >0 && i<fileName.length()-1){
//                    return fileName.substring(i+1).toLowerCase();
//                }
//            }
//            return null;
      }
    
    
}
