/* ------------------------------------------------ 
* 
* File: XmlHelper.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/query/util/XmlHelper.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.query.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author ZhangYueyun
 */
public class XmlHelper
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private OutputFormat format;
    private XMLWriter writer;

    public XmlHelper() throws DocumentException, FileNotFoundException
    {
        format = OutputFormat.createPrettyPrint();
        format.setEncoding("utf-8");
        format.setNewlines(true);
        writer = null;
    }

    public Document getDocumentFromFile(File file, String rootName) throws Exception
    {
        logger.info("Enter");
        Document doc = null;
        try
        {
            logger.info(file.getAbsolutePath() + ", =exist: " + file.exists());
            if (!file.exists())
            {
                file.createNewFile();
                doc = DocumentHelper.createDocument();
                Element root = doc.addElement(rootName);
                doc.setRootElement(root);
            } else
            {
                SAXReader reader = new SAXReader();
                doc = reader.read(file);
            }
            writer = new XMLWriter(new FileOutputStream(file), format);
            writer.write(doc);
            writer.close();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);           
            throw new Exception( MessageFormat.format(constBundle.getString("cannotFindFile"),
                    rootName,file.getAbsolutePath(),ex.getMessage()));
        }
        logger.info("Return: doc=" + doc);
        return doc;
    }

    public void writeDocumentToFile(Document document, File writeFile) throws Exception
    {
        logger.info("Enter");
        try
        {
            writer = new XMLWriter(new FileOutputStream(writeFile), format);
            writer.write(document);
            writer.close();
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
    }

   
}
