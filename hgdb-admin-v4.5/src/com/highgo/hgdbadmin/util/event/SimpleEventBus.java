/* ------------------------------------------------ 
* 
* File: SimpleEventBus.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/event/SimpleEventBus.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Fanyi
 */
public class SimpleEventBus
{
    private final Map<Event.Type<? extends EventHandler>, List<? extends EventHandler>> eventMap 
            = new HashMap<Event.Type<? extends EventHandler>, List<? extends EventHandler>>();

    public SimpleEventBus()
    {
        
    }
    
    public <T extends EventHandler> void addHandler(Event.Type<T> type, T handler)
    {
        if (type == null)
        {
            throw new NullPointerException("Cannot add a handler with a null type");
        }
        
        if (handler == null)
        {
            throw new NullPointerException("Cannot add a null handler");
        }
        
        List<T> handlerList = (List<T>) eventMap.get(type);
        if (handlerList == null)
        {
            handlerList = new ArrayList<T>();
            eventMap.put(type, handlerList);
        }
        handlerList.add(handler);
    }
    
    public <T extends EventHandler> void fireEvent(Event<T> event)
    {
        fireEvent(event, null);
    }
    
    public <T extends EventHandler> void fireEvent(Event<T> event, Object source)
    {
        if (event == null)
        {
            throw new NullPointerException("Cannot fire a null event");
        }
        
         if (source != null)
        {
            event.setSource(source);
        }
        
        List<T> handlerList = (List<T>) eventMap.get(event.getAssociatedType());
        if (handlerList != null)
        {
            for (T handler : handlerList)
            {
                event.dispatch(handler);
            }
        }
    }
}
