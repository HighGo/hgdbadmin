/* ------------------------------------------------ 
* 
* File: Event.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/event/Event.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util.event;

/**
 *
 * @author Fanyi
 * @param <T>
 */
public abstract class Event<T extends EventHandler>
{
    public static class Type<T>
    {
        private static int nextHashCode;
        private static int index;
        
        public Type()
        {
            index = ++nextHashCode;
        }

        @Override
        public int hashCode()
        {
            return index;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            final Type<T> other = (Type<T>) obj;
            if (other.hashCode() != hashCode())
            {
                return false;
            }
            return true;
        }

        @Override
        public String toString()
        {
            return "Event Type" + index;
        }

    }
    
    private Object source;
    public Event()
    {
    }
    
    public Event(Object source)
    {
        this.source = source;
    }

    public Object getSource()
    {
        return source;
    }

    public void setSource(Object source)
    {
        this.source = source;
    }

    public abstract Type<T> getAssociatedType();
            
    protected abstract void dispatch(T handler);
}
