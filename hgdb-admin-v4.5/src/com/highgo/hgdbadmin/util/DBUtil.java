/* ------------------------------------------------
 *
 * File: DBUtil.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，2020
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved.
 *
 * Identification：
 *                  com.highgo.hgdbadmin.util.DBUtil.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.util;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author liu Yuanyuan
 */
public class DBUtil {
    
    private static Logger logger =  LogManager.getLogger(DBUtil.class);//LoggerFactory.getLogger(getClass());
    
    public static void saveXMLFile(File file, Object document)throws IOException
    {
        try
        {
            file.createNewFile();
            //in fact, default is UTF-8
            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setEncoding("utf-8");
            format.setNewlines(true);
            XMLWriter output = new XMLWriter(new FileWriter(file), format);
            output.write(document);
            output.close();
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        }
    }
    
    public static HelperInfoDTO getHelperFromServer(XMLServerInfoDTO server)
    {
        if (server == null) {
            return null;
        }

        HelperInfoDTO helper = new HelperInfoDTO();
        copyProperties(server, helper);
        return helper;
    }
    public static void copyProperties(XMLServerInfoDTO server, HelperInfoDTO helper)
    {
        if(server == null)
            return;
        if(helper == null)
            return;
        
        helper.setHost(server.getHost());
        helper.setPort(server.getPort());
        helper.setMaintainDB(server.getMaintainDB());
        helper.setUser(server.getUser());
        helper.setPwd(server.getPwd());
        helper.setOnSSL(server.isOnSSL());
        
        helper.setDatLastSysOid(server.getDatLastSysOid() == null ? -1 
                : server.getDatLastSysOid());
        helper.setDbSys(server.getDbSys());
        helper.setVersionNumber(server.getVersionNumber());
        /*
        String vinfo = server.getVersion().split(",")[0];
        if (vinfo.startsWith("HighGo Database"))
        {
            helper.setDbSys(TreeEnum.DBSYS.HIGHGO);
            helper.setVersionNumber(vinfo.split(" ")[2]);
        } else if (vinfo.startsWith("PostgreSQL"))
        {
            helper.setDbSys(TreeEnum.DBSYS.POSTGRESQL);
            helper.setVersionNumber(vinfo.split(" ")[1]);
        }
        */
    }
    
    @Deprecated
    public static boolean isVersionHigherThan(TreeEnum.DBSYS dbsys
            , String number, int majorComp, int minorComp)
    {
        //logger.debug("dbsys: " + dbsys + " number: " + number);
        
        int major = 0, minor = 0;
        /*if (dbsys.equals(TreeEnum.DBSYS.HIGHGO))
        {
            if (number.startsWith("2.0"))
            {
                major = 9;
                minor = 2;
            } else if (number.startsWith("1.3") || number.startsWith("1.2"))
            {
                major = 9;
                minor = 0;
            } else if (number.startsWith("1.0"))
            {
                major = 8;
                minor = 4;
            }
        } else */
        if (dbsys.equals(TreeEnum.DBSYS.POSTGRESQL))
        {
            String tmp = "";
            boolean hasPonit = true;
            for (char c : number.trim().toCharArray()) {
                if (c >= '0' && c <= '9') {
                    tmp = tmp + c;
                } else {
                    if (hasPonit) {
                        major = Integer.valueOf(tmp);
                        tmp = "";
                        hasPonit = false;
                    }
                }
            }

            if (hasPonit) {
                major = Integer.valueOf(tmp);
            } else {
                minor = tmp.isEmpty() ? 0 : Integer.valueOf(tmp);
            }
        } else
        {
            logger.warn(dbsys + " is an exception dbsys");
            return false;
        }
        
        if (major > majorComp)
        {
            return true;
        } else if (major == majorComp && minor > minorComp)
        {
            return true;
        } else
        {
            return false;
        }
    }
    
    protected boolean hasPrivilege(HelperInfoDTO helperInfo, String objType, String objName, String privilege) throws ClassNotFoundException, SQLException
    {
        boolean flag = false;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getMaintainDB();
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            
            String sql = "SELECT has_" + objType + "_privilege('"
                + objName + "','" + privilege + "')";
            logger.debug(sql);
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                flag = rs.getBoolean(1);
            }
        } catch (SQLException ex)
        {
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        return flag;
    }
    
}
