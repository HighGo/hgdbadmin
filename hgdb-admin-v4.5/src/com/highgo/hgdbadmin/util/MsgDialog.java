/* ------------------------------------------------ 
* 
* File: MsgDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/MsgDialog.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.util;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JOptionPane;

/**
 *
 * @author Liu Yuanyuan
 */
public class MsgDialog
{
    public int showDialog(Component parent,int msgType, Icon icon, String title,String msg)
    {
        return JOptionPane.showOptionDialog(parent, msg, title, JOptionPane.CLOSED_OPTION,
                msgType, icon, null, null);
    }
}
