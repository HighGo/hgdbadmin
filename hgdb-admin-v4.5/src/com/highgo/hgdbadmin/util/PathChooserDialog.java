/* ------------------------------------------------ 
* 
* File: PathChooserDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/PathChooserDialog.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;

/**
 *
 * @author Liu Yuanyuan
 */
public class PathChooserDialog
{
    private JFileChooser pathChooser;

    public int showPathChooser(Component parent,String defaultPath, String title)
    {   
        if (defaultPath != null )
        {
             File file = new File(defaultPath);
             pathChooser = new JFileChooser(file);
             
        } else
        {
            pathChooser = new JFileChooser("");
        }
        pathChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        pathChooser.setDialogTitle(title);
        return pathChooser.showOpenDialog(parent);
    }
    public String getChoosedPath()
    {
        return pathChooser.getSelectedFile().getAbsolutePath();
    }

    /*
    public FileChooserDialog(JTextField txtfld)
    {
        this.txtfld = txtfld;
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/file_open.png")));
        setTitle(constBundle.getString("logPath"));
        setSize( 550, 420);
        setResizable(false);
        setLayout(null);
        fileChooser = new JFileChooser(new File("").getAbsolutePath());
        setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //set the text of the file chooser's  default button
        //fileChooser.setApproveButtonText("");
        //disable the default buttons of file chooser
        fileChooser.setControlButtonsAreShown(false);
        fileChooser.setBounds(0, 0, 540, 350);
        //JButton btnGetPath = new JButton();
        //btnGetPath.setText(constBundle.getString("getPath"));
        //btnGetPath.setBounds(440, 350, 90, 23);
        add(fileChooser);
        add(btnGetPath);
        btnGetPath.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnGetPathActionPerformed();
            }
        });
    }

    private void btnGetPathActionPerformed()
    {
        txtfld.setText(fileChooser.getCurrentDirectory().getAbsolutePath() + "\\");
        logger.info("Choose Path:" + fileChooser.getCurrentDirectory().getAbsolutePath());
        dispose();
    }
*/
}
