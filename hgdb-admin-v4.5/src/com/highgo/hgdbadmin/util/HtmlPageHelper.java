/* ------------------------------------------------ 
* 
* File: HtmlPageHelper.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/HtmlPageHelper.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util;

import java.awt.Window;
import java.util.ResourceBundle;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class HtmlPageHelper
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
        
    public HtmlPageHelper()
    {
    }    
    private static HtmlPageHelper dataTypeController = null;
    public static HtmlPageHelper getInstance()
    {
        if (dataTypeController == null)
        {
            dataTypeController = new HtmlPageHelper();
        }
        return dataTypeController;
    }
    
    //for object create 
    public void showObjHelpPage(JDialog parent, TreeEnum.TreeNode type)
    {
        JOptionPane.showMessageDialog(parent, constBundle.getString("notSupportNow"),
                        constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);
        return;
        /*
        logger.info("Enter;" + type);
        String url = null;
        String dbHelpUrl = null;//"http://www.postgres.cn/docs/9.3/";
        try
        {
            dbHelpUrl = OptionController.getInstance().getOptionProperties().getProperty("db_help_url");
            if (dbHelpUrl == null || dbHelpUrl.isEmpty())
            {
                String warn = constBundle.getString("dbHelpPathEmptyHint");
                logger.warn(warn);
                JOptionPane.showMessageDialog(parent, warn,
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
                return;
            } else
            {
                if (dbHelpUrl.endsWith("index.html"))
                {
                    dbHelpUrl = dbHelpUrl.substring(0, dbHelpUrl.length() - 10);
                }
                if (!dbHelpUrl.endsWith("/"))
                {
                    dbHelpUrl = dbHelpUrl + "/";
                }
            }
        } catch (Exception ex)
        {
            logger.error(ex);
            JOptionPane.showMessageDialog(parent, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
            return;
        }
        
        //String url = null;
        //String dbHelpUrl = "doc/hgdb/html/";
        logger.info(dbHelpUrl);   
        switch (type)
        {
            case LOGINROLE:
            case GROUPROLE:
                url = dbHelpUrl + "sql-createrole.html";
                break;
            case TABLESPACE:
                url = dbHelpUrl + "sql-createtablespace.html";
                break;
            case DATABASE:
                url = dbHelpUrl + "sql-createdatabase.html";
                break;
            case SCHEMA:
                url = dbHelpUrl + "sql-createschema.html";
                break;
            case FUNCTION:
                url = dbHelpUrl + "sql-createfunction.html";
                break;
            case SEQUENCE:
                url = dbHelpUrl + "sql-createsequence.html";
                break;
            case TABLE:
                url = dbHelpUrl + "sql-createtable.html";
                break;
            case VIEW:
                url = dbHelpUrl + "sql-createview.html";
                break;
            case COLUMN:
            case CONSTRAINT:
                url = dbHelpUrl + "sql-altertable.html";
                break;
            case RULE:
                url = dbHelpUrl + "sql-createrule.html";
                break;
            case INDEX:
                url = dbHelpUrl + "sql-createindex.html";
                break;
            case TRIGGER:
                url = dbHelpUrl + "sql-createtrigger.html";
                break;
            default:
                String error = type + "is an exception type, no help for it.";
                logger.error(error);
                JOptionPane.showMessageDialog(parent, error,
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                return;
        }
        try
        {
            logger.info(url);
            if (url == null || url.isEmpty())
            {
                JOptionPane.showMessageDialog(parent, constBundle.getString("notSupportNow"),
                        constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);
                return;
            }
            //Desktop.getDesktop().browse(new URI(url));//browse web page
            Desktop.getDesktop().open(new File(url));//browse file
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(parent, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
        */
    }
    public void showPgHelpPage(Window parent, String filename)
    {
        JOptionPane.showMessageDialog(parent, constBundle.getString("notSupportNow"),
                        constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);
        return;
        /*
        String dbHelpUrl = null;//"http://www.postgres.cn/docs/9.3/";
        try
        {
            dbHelpUrl = OptionController.getInstance().getOptionProperties().getProperty("db_help_url");
            if (dbHelpUrl == null || dbHelpUrl.isEmpty())
            {
                String warn = constBundle.getString("dbHelpPathEmptyHint");
                logger.warn(warn);
                JOptionPane.showMessageDialog(parent, warn,
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);                
                return;
            } else
            {
                if (dbHelpUrl.endsWith("index.html"))
                {
                    dbHelpUrl = dbHelpUrl.substring(0, dbHelpUrl.length() - 10);
                }
                if (!dbHelpUrl.endsWith("/"))
                {
                    dbHelpUrl = dbHelpUrl + "/";
                }
            }
        } catch (Exception ex)
        {
            logger.error(ex);
            JOptionPane.showMessageDialog(parent, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
            return;
        }
        dbHelpUrl = dbHelpUrl + filename;
        logger.info(dbHelpUrl);
        try
        {
            logger.info(dbHelpUrl);
            Desktop.getDesktop().browse(new URI(dbHelpUrl));
        } catch (IOException | URISyntaxException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(parent, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
        */
    }    
    public void showAdminHelpPage(Window parent, String filename)
    {
        JOptionPane.showMessageDialog(parent, constBundle.getString("notSupportNow"),
                        constBundle.getString("hint"), JOptionPane.PLAIN_MESSAGE);
        return;
        /*
        String HelpUrl = null;
        try
        {
            HelpUrl = OptionController.getInstance().getOptionProperties().getProperty("admin_help_url");
            if (HelpUrl == null || HelpUrl.isEmpty())
            {
                String warn = constBundle.getString("dbHelpPathEmptyHint");
                logger.warn(warn);
                JOptionPane.showMessageDialog(parent, warn,
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
                return;
            } else
            {
                if (HelpUrl.endsWith("index.html"))
                {
                    HelpUrl = HelpUrl.substring(0, HelpUrl.length() - 10);
                }
                if (!HelpUrl.endsWith("/"))
                {
                    HelpUrl = HelpUrl + "/";
                }
            }
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(parent, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }        
        HelpUrl = HelpUrl + filename;
        logger.info(HelpUrl);
        try
        {
            logger.info(HelpUrl);
            Desktop.getDesktop().browse(new URI(HelpUrl));
        } catch (IOException | URISyntaxException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(parent, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
        */
    }
    
}
