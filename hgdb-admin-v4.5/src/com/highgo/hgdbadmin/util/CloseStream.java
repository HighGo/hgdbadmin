/* ------------------------------------------------ 
* 
* File: CloseStream.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/CloseStream.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

/**
 *
 * @author Administrator
 */
public class CloseStream
{

    public static void close(Writer writer)
    {
        try
        {
            if (writer != null)
            {
                writer.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(OutputStream os)
    {
        try
        {
            if (os != null)
            {
                os.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(Reader reader)
    {
        try
        {
            if (reader != null)
            {
                reader.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(InputStream is)
    {
        try
        {
            if (is != null)
            {
                is.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace(System.out);
        }
    }
}
