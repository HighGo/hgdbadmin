/* ------------------------------------------------ 
* 
* File: XmlHelper.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/XmlHelper.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util;

//import com.highgo.hgdbadmin.query.model.MacrosInfoDTO;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.Iterator;
//import java.util.List;
//import org.dom4j.Document;
//import org.dom4j.DocumentException;
//import org.dom4j.DocumentHelper;
//import org.dom4j.Element;
//import org.dom4j.io.OutputFormat;
//import org.dom4j.io.SAXReader;
//import org.dom4j.io.XMLWriter;
//import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
//
///**
// * no use any more
// * @author ZhangYueyun
// */
//public class XmlHelper
//{
//
//    private Logger logger = LoggerFactory.getLogger(getClass());
//    OutputFormat format;
//    XMLWriter writer = null;
//
//    public XmlHelper() throws DocumentException, FileNotFoundException
//    {
//
//        format = OutputFormat.createPrettyPrint();
//        format.setEncoding("utf-8");
//        format.setNewlines(true);
//
//    }
//
//    public String getQueryText(String key, String name, File macroFile, String rootName)
//    {
//        logger.info("Enter:getQueryText");
//        List<MacrosInfoDTO> macroList = iteratorXml(macroFile, rootName);
//        String text = null;
//        for (int i = 0; i < macroList.size(); i++)
//        {
//            if (macroList.get(i).getKey().equals(key) && macroList.get(i).getName().equals(name))
//            {
//                text = macroList.get(i).getText();
//            }
//        }
//        logger.info("Return:getQueryText");
//        return text;
//    }
//
//    //cancel back to the beginning state
//    public void cancelMacros(File macroFile, List<MacrosInfoDTO> macroList, String rootName)
//    {
//        //delete first ,then add
//        logger.info("Enter:cancelMacros");
//        Document doc = createXml(macroFile, rootName);
//        Element root = doc.getRootElement();
//        try
//        {
//            if (root.elements().size() > 0)
//            {
//                for (Iterator it = root.elementIterator(); it.hasNext();)
//                {
//                    root.remove((Element) it.next());
//                }
//            }
//            if (macroList.size() > 0)
//            {
//                for (int i = 0; i < macroList.size(); i++)
//                {
//                    root.addElement("macro").addAttribute("key", macroList.get(i).getKey()).addAttribute("name", macroList.get(i).getName()).addText(macroList.get(i).getText());
//                }
//            }
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(macroFile), format);
//            writerMacro.write(doc);
//            writerMacro.close();
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//
//    }
//
//    public void saveXml(File macroFile, String preQuery, String rootName, String eleName)
//    {
//        logger.info("Enter;saveXml");
//        Document doc = createXml(macroFile, rootName);
//        Element root = doc.getRootElement();
//        try
//        {
//            Element ele = root.addElement(eleName);
//            ele.addText(preQuery);
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(macroFile), format);
//            writerMacro.write(doc);
//            writerMacro.close();
//        } catch (UnsupportedEncodingException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//    }
//
//    public List<String> iteratorXml(File hisFile, String rootName, String eleName)
//    {
//        logger.info("Enter:iteratorXml");
//        List<String> hisQueryList = new ArrayList<String>();
//        String hisQuery;
////        HistoqueryInfoDTO hisQuery;
//        Document doc = createXml(hisFile, rootName);
//logger.info("doc=================================================================="+doc.getRootElement());
//        Element root = doc.getRootElement();
//        if (!root.elements().isEmpty())
//        {
//            for (Iterator it = root.elementIterator(); it.hasNext();)
//            {
//                Element subEle = (Element) it.next();
//                if (subEle.getQName().getName().equals(eleName))
//                {
//                    hisQuery = subEle.getText();
//                    hisQueryList.add(hisQuery);
//
//                }
//            }
//        }
//        logger.info("Return:iteratorXml");
//        return hisQueryList;
//    }
//
//    public String findHisQuery(File hisFile, String rootName, String eleName, String query)
//    {
//        logger.info("Enter:findHisQuery");
//        List<String> hisQueryList = iteratorXml(hisFile, rootName, eleName);
//        String hisQuery = null;
//        if (hisQueryList.isEmpty())
//        {
//            return null;
//        }
//        for (int i = 0; i < hisQueryList.size(); i++)
//        {
//            hisQuery = hisQueryList.get(i);
//            if (hisQuery.equals(query))
//            {
//                return hisQuery;
//            }
//        }
//        logger.info("Return:findHisQuery");
//        return null;
//    }
//
//    public void deleteXml(File preFile, String rootName, String query, boolean deleteAllFlag)
//    {
//        logger.info("Enter:deleteXml");
//        try
//        {
//            Document doc = createXml(preFile, rootName);
//            Element root = doc.getRootElement();
//            if (!root.elements().isEmpty())
//            {
//                for (Iterator it = root.elementIterator(); it.hasNext();)
//                {
//                    Element ele = (Element) it.next();
//                    if (deleteAllFlag)
//                    {
//                        root.remove(ele);
//                    } else
//                    {
//                        if (ele.getText().equals(query))
//                        {
//                            root.remove(ele);
//                        }
//                    }
//
//                }
//
//            }
//
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(preFile), format);
//            writerMacro.write(doc);
//            writerMacro.close();
//        } catch (FileNotFoundException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (UnsupportedEncodingException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//
//    }
//
//    public void setSize(File preFile, String rootName, String eleName)
//    {
//        logger.info("Enter:setSize");
//        try
//        {
//            Document doc = createXml(preFile, rootName);
//            Element root = doc.getRootElement();
//            List<String> queryList = iteratorXml(preFile, rootName, eleName);
//            if (queryList.size() >= 10)
//            {
//                root.remove((Element) root.elements().get(0));
//            }
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(preFile), format);
//            writerMacro.write(doc);
//            writerMacro.close();
//        } catch (FileNotFoundException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (UnsupportedEncodingException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//    }
//
//    public Document createXml(File file, String rootName)
//    {
//        logger.info("Enter:createXml----------------------------------------------");
//        Document document = null;
//        try
//        {
//logger.info("file exist?--------------------------------------"+file.exists());
////logger.info("file path---------------------------------"+file.getPath());
////logger.info("file name---------------------------------"+file.getName());
//            if (!file.exists())
//            {
//logger.info("file not exist-----------------------------------------------------------------");
//                file.createNewFile();
//                document = DocumentHelper.createDocument();
//                Element root = document.addElement(rootName);
//                document.setRootElement(root);
//            } else
//            {
//                SAXReader reader = new SAXReader();
//                document = reader.read(file);
//            }
//            writer = new XMLWriter(new FileOutputStream(file), format);
//            writer.write(document);
//            writer.close();
//
//        } catch (FileNotFoundException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (UnsupportedEncodingException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (IOException | DocumentException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//        logger.info("Return:createXml");
//        return document;
//    }
//
//    public void saveMacrosXml(File macroFile, List<MacrosInfoDTO> macroList, String rootName)
//    {
//        logger.info("Enter:saveMacrosXml");
//        try
//        {
//            Document document = createXml(macroFile, rootName);
//            Element root = document.getRootElement();
//            if (!macroList.isEmpty())
//            {
//                for (int i = 0; i < macroList.size(); i++)
//                {
//                    root.addElement("macro").addAttribute("key", macroList.get(i).getKey()).addAttribute("name", macroList.get(i).getName()).addText(macroList.get(i).getText());
//                }
//            }
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(macroFile), format);
//            writerMacro.write(document);
//            writerMacro.close();
//        } catch (FileNotFoundException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (UnsupportedEncodingException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//    }
//
//    public List<MacrosInfoDTO> iteratorXml(File macroFile, String rootName)
//    {
//        logger.info("Enter:iteratorXml");
//        List<MacrosInfoDTO> macroList = new ArrayList<MacrosInfoDTO>();
//        MacrosInfoDTO macros = null;
//        Document doc = createXml(macroFile, rootName);
//        Element root = doc.getRootElement();
//        if (root.elements().isEmpty())
//        {
//            return null;
//        }
//        for (Iterator it = root.elementIterator(); it.hasNext();)
//        {
//            Element subEle = (Element) it.next();
//            if (subEle.getQName().getName().equals("macro"))
//            {
//                macros = new MacrosInfoDTO();
//                macros.setKey(subEle.attributeValue("key"));
//                macros.setName(subEle.attributeValue("name"));
//                macros.setText(subEle.getText());
//                macroList.add(macros);
//            }
//        }
//        logger.info("Return:iteratorXml");
//        return macroList;
//    }
//
//    public void deleteMacroByKey(File macroFile, String key, String rootName)
//    {
//        logger.info("Enter:deleteMacrosByKey");
//        Document doc = createXml(macroFile, rootName);
//        Element root = doc.getRootElement();
//        try
//        {
//            if (!root.elements().isEmpty())
//            {
//                for (Iterator it = root.elementIterator(); it.hasNext();)
//                {
//                    Element ele = (Element) it.next();
//                    if (ele.attributeValue("key").equals(key))
//                    {
//                        root.remove(ele);
//                    }
//                }
//            }
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(macroFile), format);
//            writerMacro.write(doc);
//            writerMacro.close();
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//    }
//
//    public void updateMacro(File macroFile, MacrosInfoDTO macroInfo, String rootName)
//    {
//        logger.info("Enter:updateMacro");
//        Document doc = createXml(macroFile, rootName);
//        Element root = doc.getRootElement();
//        try
//        {
//            if (!root.elements().isEmpty())
//            {
//                for (Iterator it = root.elementIterator(); it.hasNext();)
//                {
//                    Element ele = (Element) it.next();
//                    if (ele.attributeValue("key").equals(macroInfo.getKey()))
//                    {
//                        ele.attribute("name").setValue(macroInfo.getName());
//                        ele.setText(macroInfo.getText());
//                    }
//                }
//            }
//            XMLWriter writerMacro = new XMLWriter(new FileOutputStream(macroFile), format);
//            writerMacro.write(doc);
//            writerMacro.close();
//        } catch (IOException ex)
//        {
//            logger.error("Error :" + ex.getMessage());
//            ex.printStackTrace(System.out);
//        }
//    }
//
//}
