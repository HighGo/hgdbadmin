/* ------------------------------------------------ 
* 
* File: PwdInputOptionDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/PwdInputOptionDialog.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.util;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

/**
 *
 * @author Fanyi
 */
@Deprecated
public class PwdInputOptionDialog
{
    private JPasswordField fldPassword;    
    public int showDialog(Component parent, Icon icon, String msg, String title)
    {
        fldPassword = new JPasswordField();
        fldPassword.setSelectionStart(0);
        fldPassword.requestFocusInWindow();
        Object[] obj =
        {
            msg, fldPassword
        };
        return JOptionPane.showOptionDialog(parent, obj, title, JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE, icon, null, null);
    }

    public String getPassword()
    {
        return new String(fldPassword.getPassword());
    }
}

