/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.hgdbadmin.util;

import java.awt.Window;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * note: Container includ frame, dialog, jtree
 *       Window includ frame, dialog
 * 
 * @author liuyuanyuan
 */
public class MessageUtil
{
    private static Logger logger = LogManager.getLogger(MessageUtil.class);
    private static ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    public static void showMsg(Window parent, Exception ex, String title, int msgType)
    {
        String msg = ex.getMessage();
        logger.error(msg);
        if (msg != null && !msg.isEmpty()) {
            msg = msg.replaceAll("[\\,\\ S;.!?\"] +", "." + System.lineSeparator());
        }
        JOptionPane.showMessageDialog(parent, msg, title, msgType);
        //ex.printStackTrace(System.err);
    }
    
    public static void showErrorMsg(Window parent, Exception ex)
    {
        showMsg(parent, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
    }
}
