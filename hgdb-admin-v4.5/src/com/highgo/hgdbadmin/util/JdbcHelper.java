/* ------------------------------------------------ 
* 
* File: jdbcHelper.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/jdbcHelper.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.util;

import com.highgo.hgdbadmin.model.ConnectInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Fanyi
 * @author Liu Yuanyuan
 */
public class JdbcHelper
{
    private static Logger logger = LogManager.getLogger(JdbcHelper.class);

    public static void main(String[] args)
    {
        String url = "jdbc:highgo://192.168.100.244:6666/highgo";
        String user = "syssso";
        String pwd = "a";
        boolean onSSL = false;
        try
        {
            JdbcHelper.getConnection(url, user, pwd, onSSL);
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }     
    }     
     
    //user for hgdbadmin
    public static Connection getConnection(String url, String user, String pwd, boolean onSSL) throws ClassNotFoundException, SQLException
    {
        //String driver = "org.postgresql.Driver";
        //String driver ="cn.com.highgo.jdbc.driver";
        String driver = "com.highgo.jdbc.Driver";
        Class.forName(driver);

        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", pwd);
        //System.err.println("*******************************************************onSSL=" + onSSL);
        if (onSSL)
        {
            //the follwing two paramenters to conn hgdb on ssl
            props.setProperty("ssl", "true");
            props.setProperty("sslfactory", "com.highgo.jdbc.ssl.NonValidatingFactory");
        }
        System.err.println("before.url=" + url);
        
        boolean compatablePercent = false;
        try
        {
            compatablePercent = Boolean.valueOf(OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "compatable_percent"));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        if(compatablePercent){
           url = url.replaceAll("%", "%25");
        }
 
        if (!url.endsWith("?preferQueryMode=simple"))
        {
            url = url + "?";
        }
        System.err.println("after.url=" + url);
        Connection conn = DriverManager.getConnection(url, props);
        return conn;
    }
    
    public static Connection getConnection(String url, ConnectInfoDTO connInfo) throws ClassNotFoundException, SQLException
    {
        return getConnection(url, connInfo.getUser(), connInfo.getPwd(), connInfo.isOnSSL());
    }
    
    public static void close(Connection conn)
    {
        try
        {
            if (conn != null)
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(Statement stmt)
    {
        try
        {
            if (stmt != null)
            {
                stmt.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(ResultSet rs)
    {
        try
        {
            if (rs != null)
            {
                rs.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.out);
        }
    }
        
}
