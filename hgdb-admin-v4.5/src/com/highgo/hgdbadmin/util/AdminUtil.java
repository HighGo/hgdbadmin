/* ------------------------------------------------ 
* 
* File: AdminUtil.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/AdminUtil.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Liu Yuanyuan
 */
public class AdminUtil
{
    
    public static String md5UserPwd(String user, String pwd) throws NoSuchAlgorithmException
    {
        return "md5" + md5(pwd + user);
    }
    
    public static String md5(String data) throws NoSuchAlgorithmException
    {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(data.getBytes());
        StringBuilder buf = new StringBuilder();
        byte[] bits = md.digest();
        for (int i = 0; i < bits.length; i++)
        {
            int a = bits[i];
            if (a < 0)
            {
                a += 256;
            }
            if (a < 16)
            {
                buf.append("0");
            }
            buf.append(Integer.toHexString(a));
        }
        return buf.toString();
    }
    
}
