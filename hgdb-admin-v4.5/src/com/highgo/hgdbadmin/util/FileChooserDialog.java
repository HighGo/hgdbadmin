/* ------------------------------------------------ 
* 
* File: FileChooserDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/FileChooserDialog.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.util;

import java.awt.Component;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Liu Yuanyuan
 */
public class FileChooserDialog
{
    private JFileChooser fileChooser;

    public int showFileChooser(Component parent, String title, String defaultPath)
    {
        this.init(title, defaultPath);
        return fileChooser.showOpenDialog(parent);
    }
    public int showFileChooser(Component parent, String title, String defaultPath, FileNameExtensionFilter filter)
    {
        this.init(title, defaultPath);
        if (filter != null)
        {
            fileChooser.setFileFilter(filter);
        }
        return fileChooser.showOpenDialog(parent);
    }
    private void init(String title, String defaultPath)
    {
        if (defaultPath != null)
        {
            File file = new File(defaultPath);
            if (file.exists())
            {
                fileChooser = new JFileChooser(file);
            } else
            {
                File f = new File("");
                fileChooser = new JFileChooser(new File(f.getAbsolutePath()));
            }
        } else
        {
            File f = new File("");
            fileChooser = new JFileChooser(new File(f.getAbsolutePath()));
        }

        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setDialogTitle(title);
    }
    
    public String getChoosedFileDir()
    {
        if(fileChooser.getSelectedFile()==null)
            return null;
        else 
            return fileChooser.getSelectedFile().getAbsolutePath();
    }

}
