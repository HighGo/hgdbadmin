/* ------------------------------------------------ 
* 
* File: TreeEnum.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/TreeEnum.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.util;

/**
 *
 * @author Liu Yuanyuan
 */
public class TreeEnum
{
    //this for hgdbAdmin
    public enum TreeNode
    {
        SERVER_GROUP,
        SERVER,
        DATABASE_GROUP, TABLESPACE_GROUP, GROUPROLE_GROUP,LOGINROLE_GROUP,
        DATABASE, TABLESPACE, GROUPROLE, LOGINROLE,

        CATALOG_GROUP,CAST_GROUP,LANGUAGE_GROUP,SCHEMA_GROUP,EVENT_TRIGGER_GROUP,
        CATALOG, CAST, LANGUAGE, SCHEMA, EVENT_TRIGGER,

        TABLE_GROUP,VIEW_GROUP,SEQUENCE_GROUP,FUNCTION_GROUP, PROCEDURE_GROUP,
        AGGREGATE, CONVERSION, DOMAIN, FTS_CONFIGURATION, FTS_DICTIONARIE,
        FTS_PARSER, FTS_TEMPLATE, OPERATOR, OPERATOR_CLASSE,
        OPERATOR_FAMILIE,
        SEQUENCE, SEQUENCE_WIDE,SEQUENCE_PARTICULAR,
        TABLE, TRIGGER_FUNCTION, TYPE, VIEW, MATERIALIZED_VIEW, 
        FUNCTION, PROCEDURE,
        
        COLUMN_GROUP, CONSTRAINT_GROUP, INDEX_GROUP, RULE_GROUP, TRIGGER_GROUP,PARTITION_GROUP,
        COLUMN, CONSTRAINT, RULE, TRIGGER, VIEW_COLUMN, PARTITION,
        PRIMARY_KEY, FOREIGN_KEY, EXCLUDE, CHECK, UNIQUE, INDEX,    
    }

    public enum MaintanceOperation
    {
        VACUUM, ANALYZE, REINDEX, CLUSTER
    }
    
    public enum DBSYS
    {
        HIGHGO,POSTGRESQL
    }
    
    public enum BACKUP_FORMATE
    {
        plain,custom,directory,tar
    }    
}
