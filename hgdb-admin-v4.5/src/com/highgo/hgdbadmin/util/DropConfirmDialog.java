/* ------------------------------------------------ 
* 
* File: DropConfirmDialog.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src/com/highgo/hgdbadmin/util/DropConfirmDialog.java
*
*-------------------------------------------------
*/
package com.highgo.hgdbadmin.util;

import java.awt.Component;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import javax.swing.JOptionPane;

/**
 *
 * @author Liu Yuanyuan
 */
public class DropConfirmDialog
{
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    public int showDialog(Component parent, String objType, String objName, boolean isDropCascade)
    {
        String title, msg;
        if (isDropCascade)
        {
            title = constBundle.getString("isDropCascadeTitle");
            msg = constBundle.getString("isDropCascadeMsg");
        } else
        {
            title = constBundle.getString("isDropTitle");
            msg = constBundle.getString("isDropMsg");
        }
        return JOptionPane.showConfirmDialog(parent, MessageFormat.format(msg, objType, objName),
                MessageFormat.format(title, objType), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    }
}
