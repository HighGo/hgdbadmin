/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.hgdbadmin.datamanager.controller;

import java.math.BigDecimal;

/**
 * never used untile now
 * @author liuyuanyuan
 */
@Deprecated
public class TypeUtil
{
    /**
     * this method copy from pgjdbc
     *
     * @param s
     * @return
     * @throws Exception
     */
    public static BigDecimal toBigDecimal(String s) throws Exception
    {
        if (s == null) {
            return null;
        }
        try {
            s = s.trim();
            return new BigDecimal(s);
        } catch (NumberFormatException e) {
            throw new Exception("Bad value for type BigDecimal:" + s);
        }
    }
}
