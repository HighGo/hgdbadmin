/* ------------------------------------------------
 *
 * File: DataController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datamanager\controller\DataController.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datamanager.controller;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.datamanager.model.ColumnHeaderInfoDTO;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.jdbc.PGStatement;
import com.highgo.jdbc.geometric.PGbox;
import com.highgo.jdbc.geometric.PGcircle;
import com.highgo.jdbc.geometric.PGline;
import com.highgo.jdbc.geometric.PGpath;
import com.highgo.jdbc.geometric.PGpoint;
import com.highgo.jdbc.geometric.PGpolygon;
import com.highgo.jdbc.util.PGInterval;
import com.highgo.jdbc.util.PGTimestamp;
import com.highgo.jdbc.util.PGmoney;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author Yuanyuan
 */
public class DataController
{
    private final Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());

    //thread-safe
    public static DataController getInstance() {
        return InstanceHolder.instance;
    }
    private DataController() {
    }
    private static class InstanceHolder {
        private static DataController instance = new DataController();
    }
    
    public int getRowCount(HelperInfoDTO helperInfo, String relname, String filterCondition)
            throws ClassNotFoundException, SQLException
    {
        logger.debug(relname);
        if (relname == null)
        {
            logger.error("relation is null, do nothing, return.");
            return 0;
        }        
       // HelperInfoDTO helperInfo = relation.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("connectInfo is null, do nothing, return.");
            return 0;
        }

        StringBuilder sql = new StringBuilder();
        //SyntaxController sc = SyntaxController.getInstance();
        sql.append("SELECT COUNT(1) FROM ").append(relname);
                //.append(sc.getName(helperInfo.getSchema())) .append(".").append(sc.getName(relation.getName()));//relation.getName()
        if (filterCondition != null && !filterCondition.isEmpty())
        {
            sql.append(" WHERE ").append(filterCondition);
        }
        //row count has no matter with sort condition, and sort condition will case error in sql.
        //if (sortConditon != null && !sortConditon.isEmpty())
        //{
        //   sql.append(" ORDER BY ").append(sortConditon);
        //}
        
        int row = this.getRowCount(helperInfo, sql.toString());
        return row;    
    }
    public int getRowCount(HelperInfoDTO helperInfo, String sql) 
            throws ClassNotFoundException, SQLException
    {
        int rowCount = 0;
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return rowCount;
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            //add begin by sunqk at 2019.06.03 for set jdbc preferQueryMode=simple
            if(1 == helperInfo.getQueryMode())
            {
                url += "?preferQueryMode=simple";
            }
            //add begin by sunqk at 2019.06.03 for set jdbc preferQueryMode=simple
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            logger.debug(sql);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                rowCount = rs.getInt(1);
            }
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        logger.debug("return:rowCount=" + rowCount);
        return rowCount;
    }

    
    public List<Object[]> getData(AbstractObject relation, String filterCondition, String sortCondition,
            boolean isEditable, int pageOffset, int pageSize)
            throws ClassNotFoundException, SQLException
    {
        List<Object[]> dl = new ArrayList();
        if (relation == null)
        {
            logger.error("relation is null, do nothing, return.");
            return dl;
        }
        logger.debug("relationName = " + relation.getName());
        HelperInfoDTO helperInfo = relation.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("connectInfo is null, do nothing, return.");
            return dl;
        }
        String sql = this.getSelectDataSQL(relation, filterCondition, sortCondition) + " limit ? offset ?";
        dl = this.getData(helperInfo, sql, isEditable, pageOffset, pageSize);
        return dl;
    }
     public List<Object[]> getData(HelperInfoDTO helperInfo, String sql,
            boolean isEditable, int pageOffset, int pageSize)
             throws ClassNotFoundException, SQLException
    {
        logger.debug("pageOffset=" + pageOffset);
        List<Object[]> dl = new ArrayList();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        //add begin by sunqk at 2019.06.03 for set jdbc preferQueryMode=simple
        //if(1 == helperInfo.getQueryMode())
        {
            url += "?preferQueryMode=simple";//lyy: for gc if not this, resultset will not right
        }
        //add begin by sunqk at 2019.06.03 for set jdbc preferQueryMode=simple
        try
        {          
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            //conn.setAutoCommit(false);            
            logger.info(sql);
            stmt = conn.prepareStatement(sql);
            stmt.setFetchSize(1000);//not work when auto-commit
            logger.info("limit=" + pageSize + " offset=" + (pageOffset * pageSize));
            stmt.setInt(1, pageSize);
            stmt.setInt(2, pageOffset * pageSize);
            rs = stmt.executeQuery();
            int columnCount = rs.getMetaData().getColumnCount();
            Object[] rowData = null;
            while (rs.next())
            {
                rowData = new Object[columnCount];
                for (int i = 1; i <= columnCount; i++)
                {
                    String name = rs.getMetaData().getColumnClassName(i);               
                    //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
                    //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
                    //logger.debug("*****" + name);
                    switch (name)
                    {
                        case "[B"://bytea
                            rowData[i-1] = rs.getBinaryStream(i);
                            break;
                        case "com.highgo.jdbc.util.PGInterval":
                        case "com.highgo.jdbc.util.PGmoney":
                        case "java.sql.SQLXML":
                            rowData[i-1] = rs.getString(i);
                            break;
                        default:
                            rowData[i-1] = rs.getObject(i);//case "java.lang.Boolean":
                            break;
                    }
                }
                dl.add(rowData);
                rowData = null;
            }
            //conn.setAutoCommit(true);
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        logger.info("Return: dataRow=" + dl.size());
        return dl;
    }
    public String getSelectDataSQL(AbstractObject relation, String filterCondition, String sortConditon)
    {
        SyntaxController sc = SyntaxController.getInstance();
        String relationName =  sc.getName(relation.getHelperInfo().getSchema()) + "." + sc.getName(relation.getName());
        
        StringBuilder selectSQL = new StringBuilder();
        if (relation instanceof TableInfoDTO)
        {
            TableInfoDTO table = (TableInfoDTO) relation;
            selectSQL.append("SELECT ");
            if (table.isHasOID())
            {
                selectSQL.append(" oid,");
            }
            List<ColumnInfoDTO> columnList = table.getColumnInfoList();
            if (columnList != null)
            {
                int size = columnList.size();
                for (int i = 0; i < size; i++)
                {
                    if (i > 0)
                    {
                        selectSQL.append(", ");
                    }
                    selectSQL.append(columnList.get(i).getName());
                }
            }
            selectSQL.append(" FROM ").append(relationName);
        } else
        {
            String[] headers = null;
            if (relation instanceof ViewInfoDTO)
            {
                ViewInfoDTO view = (ViewInfoDTO) relation;
                headers = view.getHeaders();
                //selectSQL.append(view.getDefinition().replace(";", ""));
            } else if (relation instanceof PartitionInfoDTO)
            {
                 PartitionInfoDTO part = (PartitionInfoDTO) relation;
                 headers = part.getHeaders();
            }            
            selectSQL.append("SELECT ");
            for (int i = 0; i < headers.length; i++)
            {
                if (i > 0)
                {
                    selectSQL.append(", ");
                }
                selectSQL.append(headers[i]);
            }
            selectSQL.append(" FROM ").append(relationName);
        }
        
        if (filterCondition != null && !filterCondition.isEmpty())
        {
            selectSQL.append(" WHERE ").append(filterCondition);
        }
        if (sortConditon != null && !sortConditon.isEmpty())
        {
            selectSQL.append(" ORDER BY ").append(sortConditon);
        }
        logger.debug("Return: SelectSQL = " + selectSQL.toString());
        return selectSQL.toString();
    }
       
    
    public void deleteRow(TableInfoDTO table, String condition, Object[] paras) 
            throws ClassNotFoundException, SQLException
    {
        if (table == null)
        {
            logger.warn("table is null, do nothing and return");
            return;
        }
        if (condition == null)
        {
            logger.warn("condition is null, do nothing and return");
            return;
        }
        if (paras == null)
        {
            logger.warn("paras is null, do nothing and return");
            return;
        }
        HelperInfoDTO helperInfo = table.getHelperInfo();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return");
            return;
        }
        
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE FROM ")
                .append(SyntaxController.getInstance().getName(helperInfo.getSchema())).append(".").append(SyntaxController.getInstance().getName(table.getName()))
                .append(" WHERE ").append(condition);
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName()  ;
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            logger.debug(sql.toString());
            stmt = conn.prepareStatement(sql.toString());
            for (int i = 0; i < paras.length; i++)
            {
                logger.debug(i + "=" + paras[i]);
                stmt.setObject(i + 1, paras[i]);
            }
            stmt.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
    public void insertRow(TableInfoDTO table, Object[] headerArray, Object[] rowData) 
            throws Exception
    {
        if (table == null)
        {
            logger.error("table is null, do nothing and return.");
            return;
        }
        HelperInfoDTO helperInfo = table.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return;
        }
        boolean compatableMoney = true;
        try
        {
            compatableMoney = Boolean.valueOf(OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "compatable_money"));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }

        StringBuilder columns = new StringBuilder();
        StringBuilder valueholders = new StringBuilder();
        List<Integer> inputIndexes = new ArrayList();
        ColumnHeaderInfoDTO header;
        for (int k = 0; k < headerArray.length; k++) {
            if (rowData[k] == null || rowData[k].toString().isEmpty()) {
                continue;
            }
            header = (ColumnHeaderInfoDTO) headerArray[k];
            if (("bytea".equals(header.getDataType()) || header.getDataType().endsWith("blob"))
                    && rowData[k].toString().startsWith("java.io.ByteArrayInputStream@") ) {
                continue;//make unchanged value not influnece
            }
            columns.append(inputIndexes.isEmpty()? "" : ",")
                    .append(header.getName());
            valueholders.append(inputIndexes.isEmpty() ? "" : ",").append("?");
            if (compatableMoney) {
                if ("money".equalsIgnoreCase(header.getDataType())
                        || "date".equalsIgnoreCase(header.getDataType())) {
                    valueholders.append("::").append(header.getDataType());
                }
            }
            inputIndexes.add(k);
        }
        if(inputIndexes.isEmpty())
        {
            logger.warn("no valid value so needn't insert.");
            return;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(SyntaxController.getInstance().getName(helperInfo.getSchema())).append(".").append(SyntaxController.getInstance().getName(table.getName()))
                .append("(").append(columns).append(")")
                .append(" VALUES").append("(").append(valueholders).append(");");
        
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.prepareStatement(sql.toString());
            logger.debug(sql.toString());
            int index;
            for (int i = 0; i < inputIndexes.size(); i++) 
            {
                index = inputIndexes.get(i);
                ColumnHeaderInfoDTO ch = (ColumnHeaderInfoDTO)headerArray[index];
                this.setParameter(stmt, i+1, ch.getDataType(), rowData[index]);
            }
            stmt.execute();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
    public void updateValue(TableInfoDTO table
            , Object[] headerArray, List<Integer> pkColumnIndexs
            , Object[] rowData, Object[] oldRowData) 
            throws Exception
    {
        logger.debug("Enter");
        if (table == null)
        {
            logger.warn("table is null, do nothing and return.");
            return;
        }
        HelperInfoDTO helperInfo = table.getHelperInfo();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE ").append(SyntaxController.getInstance().getName(helperInfo.getSchema()))
                .append(".").append(SyntaxController.getInstance().getName(table.getName()))
                .append(" SET ");
        List<Integer> changedIndexes = new ArrayList();
        ColumnHeaderInfoDTO header;
        for (int i = 0; i < headerArray.length; i++) 
        {
            if (rowData[i] == oldRowData[i]) 
            {
                continue;
            }
            header = (ColumnHeaderInfoDTO) headerArray[i];
            if (("bytea".equals(header.getDataType()) || header.getDataType().endsWith("blob"))
                    && rowData[i].toString().startsWith("java.io.ByteArrayInputStream@")) {
                continue;
            }
            logger.debug(rowData[i] + " | " + oldRowData[i]);
            sql.append(changedIndexes.isEmpty() ? "" : ",")
                    .append(header.getName()).append("=?");
            if ("money".equalsIgnoreCase(header.getDataType())
                    || "date".equalsIgnoreCase(header.getDataType()))
            {
                sql.append("::").append(header.getDataType());
            }
            changedIndexes.add(i);
        }
        if(changedIndexes.isEmpty())
        {
            logger.debug("nothing changed so needn't update");
            return;
        }
        sql.append(" WHERE ");
        for (int i = 0; i < pkColumnIndexs.size(); i++) 
        {
            header = (ColumnHeaderInfoDTO) headerArray[pkColumnIndexs.get(i)];
            sql.append(i == 0 ? "" : " AND ")
                    .append(header.getName()).append("=?");
        }
        
        Connection conn = null;
        PreparedStatement stmt = null;
        try 
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.prepareStatement(sql.toString());
            logger.debug(sql.toString());
            ColumnHeaderInfoDTO ch;
            int index;
            for (int c = 0; c < changedIndexes.size(); c++) 
            {
                index = changedIndexes.get(c);
                ch = (ColumnHeaderInfoDTO) headerArray[index];
                this.setParameter(stmt, c + 1
                        , ch.getDataType(), rowData[index]);
            }
            for (int p = 0; p < pkColumnIndexs.size(); p++) 
            {
                index = pkColumnIndexs.get(p);
                ch = (ColumnHeaderInfoDTO) headerArray[index];
                this.setParameter(stmt, changedIndexes.size() + p + 1
                        , ch.getDataType(), oldRowData[index]);
            }
            stmt.execute();
        } catch (Exception ex) {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
     /* reference pgjdbc - TypeInfoCache
      // basic pg types info: 0 - type name, 1 - type oid, 2 - sql type, 3 - java class, 4 - array type oid
      private static final Object[][] types = {
      {"int2", Oid.INT2, Types.SMALLINT, "java.lang.Integer", Oid.INT2_ARRAY},
      {"int4", Oid.INT4, Types.INTEGER, "java.lang.Integer", Oid.INT4_ARRAY},
      {"oid", Oid.OID, Types.BIGINT, "java.lang.Long", Oid.OID_ARRAY},
      {"int8", Oid.INT8, Types.BIGINT, "java.lang.Long", Oid.INT8_ARRAY},
      {"money", Oid.MONEY, Types.DOUBLE, "java.lang.Double", Oid.MONEY_ARRAY},
      {"numeric", Oid.NUMERIC, Types.NUMERIC, "java.math.BigDecimal", Oid.NUMERIC_ARRAY},
      {"float4", Oid.FLOAT4, Types.REAL, "java.lang.Float", Oid.FLOAT4_ARRAY},
      {"float8", Oid.FLOAT8, Types.DOUBLE, "java.lang.Double", Oid.FLOAT8_ARRAY},
      {"char", Oid.CHAR, Types.CHAR, "java.lang.String", Oid.CHAR_ARRAY},
      {"bpchar", Oid.BPCHAR, Types.CHAR, "java.lang.String", Oid.BPCHAR_ARRAY},
      {"varchar", Oid.VARCHAR, Types.VARCHAR, "java.lang.String", Oid.VARCHAR_ARRAY},
      {"text", Oid.TEXT, Types.VARCHAR, "java.lang.String", Oid.TEXT_ARRAY},
      {"name", Oid.NAME, Types.VARCHAR, "java.lang.String", Oid.NAME_ARRAY},
      {"bytea", Oid.BYTEA, Types.BINARY, "[B", Oid.BYTEA_ARRAY},
      {"bool", Oid.BOOL, Types.BIT, "java.lang.Boolean", Oid.BOOL_ARRAY},
      {"bit", Oid.BIT, Types.BIT, "java.lang.Boolean", Oid.BIT_ARRAY},
      {"date", Oid.DATE, Types.DATE, "java.sql.Date", Oid.DATE_ARRAY},
      {"time", Oid.TIME, Types.TIME, "java.sql.Time", Oid.TIME_ARRAY},
      {"timetz", Oid.TIMETZ, Types.TIME, "java.sql.Time", Oid.TIMETZ_ARRAY},
      {"timestamp", Oid.TIMESTAMP, Types.TIMESTAMP, "java.sql.Timestamp", Oid.TIMESTAMP_ARRAY},
      {"timestamptz", Oid.TIMESTAMPTZ, Types.TIMESTAMP, "java.sql.Timestamp",
          Oid.TIMESTAMPTZ_ARRAY},
      //#if mvn.project.property.postgresql.jdbc.spec >= "JDBC4.2"
      {"refcursor", Oid.REF_CURSOR, Types.REF_CURSOR, "java.sql.ResultSet", Oid.REF_CURSOR_ARRAY},
      //#endif
      {"json", Oid.JSON, Types.OTHER, "org.postgresql.util.PGobject", Oid.JSON_ARRAY},
      {"point", Oid.POINT, Types.OTHER, "org.postgresql.geometric.PGpoint", Oid.POINT_ARRAY}
      */
    private void setParameter(PreparedStatement stmt, int i, String datatype, Object value)
            throws FileNotFoundException, SQLException, Exception
    {
        logger.debug("value=" + value + ", IsNull=" + (value == null));
        logger.debug(datatype);
        switch (datatype)
        {
            //PreparedStatement sometime need setNull(index, type) to set null value.
            //reference pgjdbc - TypeInfoCache.java， PgPreparedSatement.java
            //binary types
            case "bytea":
            case "oracle.blob":
            case "blob":
                if (value == null) {
                    stmt.setNull(i, java.sql.Types.BINARY);
                } else if (value instanceof InputStream) {
                    stmt.setBinaryStream(i, (InputStream) value);
                } else {
                    String fpath = value.toString();
                    File file = new File(fpath);
                    InputStream ips = new FileInputStream(file);
                    stmt.setBinaryStream(i, ips, file.length());
                }
                break;
            case "oracle.clob":
            case "clob":
                if (value == null){
                    stmt.setString(i, null);
                } else
                {
                    String str = value.toString();
                    File file = new File(str);
                    logger.debug(str + "| file.exists=" + file.exists());
                    if (file.exists())
                    {
                        InputStreamReader ips = new InputStreamReader(new FileInputStream(file),"utf-8");
                        BufferedReader reader = new BufferedReader(ips);
                        StringBuilder content = new StringBuilder();
                        String line = null;
                        while((line = reader.readLine()) != null){
                            content.append(line);
                        }
                        logger.debug(content.toString());
                        stmt.setString(i, content.toString());
                    } else
                    {
                        stmt.setString(i, str);
                    }
                }
                break;
            //numeric types
            case "smallint"://int2
            case "smallserial"://int2, small autoincrementing integer
            case "integer"://int4
            case "serial"://int4, autoincrementing integer
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, datatype.startsWith("small") ?
                            java.sql.Types.SMALLINT : java.sql.Types.INTEGER);
                } else {
                    stmt.setInt(i, Integer.valueOf(value.toString()));
                }
                break;
            case "oid":
            case "bigint"://"int8":
            case "bigserial"://int8, large autoincrementing integer
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, java.sql.Types.BIGINT);
                } else {
                    stmt.setLong(i, Long.valueOf(value.toString()));
                }
                break;
            case "numeric"://pass test
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, java.sql.Types.NUMERIC);
                } else {
                    stmt.setObject(i, value);
                    //stmt.setBigDecimal(i, StringToBigDecimal(value.toString()));
                }
                break;
            case "float4":
            case "real":
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, java.sql.Types.REAL);//ref pgjdbc
                } else {
                    stmt.setFloat(i, Float.valueOf(value.toString()));
                }
                break;
            case "float8":
            case "double precision":
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, java.sql.Types.DOUBLE);
                } else {
                    stmt.setDouble(i, Double.valueOf(value.toString()));
                }
                break;
            //date/time types
            case "date":
                //support java.sql.Date
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, java.sql.Types.DATE);
                } else {
                    stmt.setString(i, value.toString());
                }
                break;
            case "timestamp":
            case "timestamp without time zone":
                //pgjdbc support PGTimestamp and java.sql.Timestamp
                if (value == null || value.toString().isEmpty())
                {
                    stmt.setNull(i, java.sql.Types.TIMESTAMP);
                } else
                {
                    stmt.setObject(i, Timestamp.valueOf(value.toString()));
                }
                break;
            case "timestamptz":
            case "timestamp with time zone": //pgjdbc support PGTimestamp and java.sql.Timestamp
                if (value == null || value.toString().isEmpty())
                {
                    stmt.setNull(i, java.sql.Types.TIMESTAMP_WITH_TIMEZONE);
                } else
                {
                    stmt.setObject(i, PGTimestamp.valueOf(value.toString()));
                }
                 break;
            case "time":
            case "time without time zone":
                //support java.sql.Date
                if (value == null || value.toString().isEmpty())
                {
                    stmt.setNull(i, java.sql.Types.TIME);
                } else
                {
                    stmt.setTime(i, Time.valueOf(value.toString()));
                }
                break;
            case "timetz":
            case "time with time zone":
                //support java.sql.Date
                if (value == null || value.toString().isEmpty())
                {
                    stmt.setNull(i, java.sql.Types.TIME_WITH_TIMEZONE);
                } else
                {
                    stmt.setTime(i, Time.valueOf(value.toString()));
                }
                break;
            case "interval":
                //support PGInterval
                if (value == null) {
                    stmt.setObject(i, null);
                } else {
                    stmt.setObject(i, new PGInterval(value.toString()));
                }
                break;
            //Character Types
            case "char":
            case "bchar":
            case "varchar":
            case "name":
            case "text":
                stmt.setString(i, value == null? null : value.toString());
                break;
            //Boolean types
            case "boolean"://special
            case "bool"://?
                if (value == null || value.toString().isEmpty()) {
                    stmt.setNull(i, java.sql.Types.BIT);//ref pgjdbc
                } else {
                    stmt.setBoolean(i, "1".equals(value.toString())
                            || "t".equalsIgnoreCase(value.toString())
                            || "true".equalsIgnoreCase(value.toString()));
                }
                break;
            //Money type 
            case "money":
                //refer pgjdbc-PGmoney.java use fixed $ is a bug, 
                //so I write PGNoUnitMoney. 
                if (value == null || value.toString().isEmpty()) {
                    stmt.setObject(i, null);
                } else {
                    boolean compatableMoney = true;
                    try {
                        compatableMoney = Boolean.valueOf(OptionController.getInstance().getPropertyValue(
                                OptionController.getInstance().getOptionProperties(), "compatable_money"));
                    } catch (Exception ex) {
                        logger.error(ex.getMessage());
                        ex.printStackTrace(System.out);
                    }
                    if (compatableMoney) {
                        stmt.setString(i, value.toString());
                    } else {
                        PGNoUnitMoney mm = new PGNoUnitMoney(value.toString());
                        System.err.println("moneynum=" + mm.getValue());
                        stmt.setObject(i, mm);
                    }
                }
                break;
            //json
            case "json":
                stmt.setObject(i, value == null ? null : value);
                break;
            //sqlxml
            /*
            case "sqlxml":
                if (value == null || value.toString().isEmpty()) {
                    stmt.setObject(i, null);
                } else {
                PgSQLXML xml = PgSQLXML(conn, value.toString());
                 stmt.setObject(i, xml);
                break;
            */
            //geometric
            case "box":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGbox(value.toString()));
                break;
            case "circle":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGcircle(value.toString()));
                break;
            case "line":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGline(value.toString()));
                break;
            case "lseg":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGline(value.toString()));
                break;
            case "path":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGpath(value.toString()));
                break;
            case "point":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGpoint(value.toString()));
                break;
            case "polygon":
                stmt.setObject(i, (value == null || value.toString().isEmpty())
                        ? null : new PGpolygon(value.toString()));
                break;
            default:
                stmt.setObject(i, value == null? null : value);//value sometime toString
                break;
        }
    }
    private boolean isValueNotSupportEmptyString(String fullDatatype)
    {
        logger.debug("enter: " + fullDatatype);
        return fullDatatype.equalsIgnoreCase("bigint")
                    || fullDatatype.equalsIgnoreCase("bigint[]")
                    || fullDatatype.equalsIgnoreCase("integer")
                    || fullDatatype.equalsIgnoreCase("binary_double")
                    || fullDatatype.equalsIgnoreCase("binary_float")
                    || fullDatatype.equalsIgnoreCase("bit(1)[]")
                    || fullDatatype.equalsIgnoreCase("blob")
                    || fullDatatype.equalsIgnoreCase("abstime")
                    || fullDatatype.equalsIgnoreCase("double precision")
                    || fullDatatype.equalsIgnoreCase("aclitem")
                    || fullDatatype.equalsIgnoreCase("aclitem[]")
                    || fullDatatype.equalsIgnoreCase("box[]")
                    || fullDatatype.equalsIgnoreCase("bytea")
                    || fullDatatype.equalsIgnoreCase("bytea[]")
                    || fullDatatype.equalsIgnoreCase("\"char\"[]")
                    || fullDatatype.equalsIgnoreCase("character(1)[]")
                    || fullDatatype.equalsIgnoreCase("character varying[]")
                    || fullDatatype.equalsIgnoreCase("cid[]")
                    || fullDatatype.equalsIgnoreCase("cidr")
                    || fullDatatype.equalsIgnoreCase("cidr[]")
                    || fullDatatype.equalsIgnoreCase("date");
    }
      
    public void executeSql(HelperInfoDTO helperInfo, String sql) 
            throws ClassNotFoundException, SQLException
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return;
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            logger.debug(sql);
            stmt = conn.prepareStatement(sql);
            stmt.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
    
    public String[] getHearderArray(HelperInfoDTO helperInfo, String relname) throws ClassNotFoundException, SQLException
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return null;
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            String sql = "SELECT * FROM " + relname + " LIMIT 0";
            logger.debug(sql);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            ResultSetMetaData mdata = rs.getMetaData();
            String[] header = new String[mdata.getColumnCount()];
            logger.debug("columnCount=" + header.length);
            for (int i = 0; i < header.length; i++)
            {
                //logger.debug(i);
                header[i] = mdata.getColumnName(i+1);//start from 1
            }
            return header;
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
    
}
