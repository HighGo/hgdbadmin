/* ------------------------------------------------
 *
 * File: PGNoUnitMoney.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，202002
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datamanager\controller\PGNoUnitMoney.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.datamanager.controller;

import com.highgo.jdbc.util.PGmoney;
import java.sql.SQLException;

/**
 *  Override method getValue() of PGMoney to remove fixed $.
 *  to support all money unit.
 * 
 * @author liuyuanyuan
 */

public class PGNoUnitMoney extends PGmoney
{

    public PGNoUnitMoney()
    {
        this.setType("money");
    }

    public PGNoUnitMoney(double value)
    {
        this();
        val = value;
    }

    public PGNoUnitMoney(String value) throws SQLException
    {
        this();
        setValue(value);//this has problem： trim fist character
        //val = Double.parseDouble(value);
    }

    @Override
    public String getValue()
    {
        String num;
        if (val < 0) {
            num = "-" + val;
        } else {
            num = "" + val;
        }
        return num;
    }

}
