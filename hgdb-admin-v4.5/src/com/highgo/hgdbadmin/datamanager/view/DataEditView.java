/* ------------------------------------------------
 *
 * File: DataEditView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datamanager\view\DataEditView.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.datamanager.view;

import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.datamanager.paging.PagingModel;
import com.highgo.hgdbadmin.controller.DataTypeController;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.datamanager.model.ColumnHeaderInfoDTO;
import com.highgo.hgdbadmin.datamanager.controller.DataController;
import com.highgo.hgdbadmin.datamanager.paging.RowIdHeaderModel;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import com.highgo.hgdbadmin.util.CloseStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 * 
 * since pg12 table has oid not support anymore, so ignore this condition.
 */
public class DataEditView extends JFrame
{
    private final Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private final ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private final int PAGE_SIZE = 1000;
    private final AbstractObject relation;   
    private final HelperInfoDTO helperInfo;
    
    private DataSortFilterDialog sfDialog;
    private String filterCondition;
    private String sortCondition;
    
    private Object[] headerArray;
    private boolean isEditable;
    private List<Integer> pkColumnNum;
    private PagingModel pagingModel;

    private int sortColumn;  

    private RowIdHeaderModel rowIdModel;
    //add new row
    private int newRowNum=-1;//start from 0,-1 needn't insert
    //change an exist row
    private int changedRowNum = -1;
    private Object[] oldRowData = null;

    /**
     * Creates new form DataEditView
     *
     * @param relation
     * @param filterCondition
     * @param totalRowCount
     */
    public DataEditView(AbstractObject relation, String filterCondition, int totalRowCount)
    {
        logger.info("filterCondition = " + filterCondition);
        this.relation = relation;
        this.filterCondition = filterCondition;
        this.helperInfo = relation.getHelperInfo();
        
        sfDialog = new DataSortFilterDialog(this, true, relation, false, filterCondition);
        sortCondition = null;
        isEditable = false;
        //isInsertable = (TreeEnum.TreeNode.TABLE == relation.getType());
        pkColumnNum = new ArrayList();
        sortColumn = 0;      
        initComponents();    
        this.setTitle(constBundle.getString("editData") + " " + helperInfo.getDbName()
                + " - " + helperInfo.getSchema() + " - " + relation.getName());  
        
        try
        {
            boolean viewData = Boolean.valueOf(OptionController.getInstance().getPropertyValue(
                    OptionController.getInstance().getOptionProperties(), "view_data"));
            if (!viewData)
            {
                toolBar.remove(cbType);
                toolBar.remove(btnOpen);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
        
        try {
            this.initTabData(totalRowCount);
        } catch (ClassNotFoundException | SQLException ex) {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }

        //regist global listener    
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent evt)
            {
                exitActionPerformed();
            }
        });
        //TOOL sort/filter
        ActionListener sortFilterAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemSortFilterActionPerformed(e);
            }
        };
        itemSortFilter.addActionListener(sortFilterAction);
        btnSortFilter.addActionListener(sortFilterAction);
        //itemFilterBySelection //not support
        //itemFilterExcludingSelection //not support
        itemRemoveFilter.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemRemoveFilterActionPerformed(e);
            }
        });
        itemRemoveSort.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemRemoveSortActionPerformed(e);
            }
        });
        itemSortAscending.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemSortAscendingActionPerformed(e);
            }
        });
        itemSortDescending.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemSortDescendingActionPerformed(e);
            }
        });
        //FILE
        itemExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                exitActionPerformed();
            }
        });
        //EDIT  
        //HELP
        ActionListener helpListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                itemHelpContentActionPerformed(evt);
            }
        };
        itemHelpContent.addActionListener(helpListener);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        cbLimit = new javax.swing.JComboBox();
        itemFilterBySelection = new javax.swing.JMenuItem();
        itemFilterExcludingSelection = new javax.swing.JMenuItem();
        ifValueViewer = new javax.swing.JInternalFrame();
        pnlValueImage = new javax.swing.JPanel();
        buttonGroup1 = new javax.swing.ButtonGroup();
        toolbarValueViewer = new javax.swing.JToolBar();
        cbbValueViewType = new javax.swing.JComboBox<>();
        toolBar = new javax.swing.JToolBar();
        btnSave = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnUndo = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JToolBar.Separator();
        btnSortFilter = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        limitBar = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        spnPaging = new javax.swing.JSpinner();
        cbType = new javax.swing.JComboBox<>();
        btnOpen = new javax.swing.JButton();
        toolbarData = new javax.swing.JToolBar();
        btnAddRow = new javax.swing.JButton();
        btnUndoRow = new javax.swing.JButton();
        btnSaveRow = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        btnDeleteRow = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JToolBar.Separator();
        btnRefreshCurrentPage = new javax.swing.JButton();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(100, 0), new java.awt.Dimension(100, 0), new java.awt.Dimension(1000, 0));
        btnFirstPage = new javax.swing.JButton();
        btnPrePage = new javax.swing.JButton();
        spnPage = new javax.swing.JSpinner();
        btnNextPage = new javax.swing.JButton();
        btnLastPage = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        pnlData = new javax.swing.JPanel();
        spTable = new javax.swing.JScrollPane(tblData);
        tblData = new javax.swing.JTable();
        pnlValueViewer = new javax.swing.JPanel();
        spnlValueNormal = new javax.swing.JScrollPane();
        ttarValueViewer = new javax.swing.JTextArea();
        pnlState = new javax.swing.JPanel();
        txfdState = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        itemExit = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        itemRefresh = new javax.swing.JMenuItem();
        itemUndo = new javax.swing.JMenuItem();
        sepEdit = new javax.swing.JPopupMenu.Separator();
        itemSave = new javax.swing.JMenuItem();
        itemDelete = new javax.swing.JMenuItem();
        menuTool = new javax.swing.JMenu();
        itemSortFilter = new javax.swing.JMenuItem();
        sepTool = new javax.swing.JPopupMenu.Separator();
        itemRemoveFilter = new javax.swing.JMenuItem();
        sepTool2 = new javax.swing.JPopupMenu.Separator();
        itemSortAscending = new javax.swing.JMenuItem();
        itemSortDescending = new javax.swing.JMenuItem();
        itemRemoveSort = new javax.swing.JMenuItem();
        menuView = new javax.swing.JMenu();
        cbItemToolBar = new javax.swing.JCheckBoxMenuItem();
        cbItemLimitBar = new javax.swing.JCheckBoxMenuItem();
        cbItemValueViewer = new javax.swing.JCheckBoxMenuItem();
        sepView2 = new javax.swing.JPopupMenu.Separator();
        cbItemDefaultView = new javax.swing.JCheckBoxMenuItem();
        menuHelp = new javax.swing.JMenu();
        itemHelpContent = new javax.swing.JMenuItem();

        cbLimit.setEditable(true);
        cbLimit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { constBundle.getString("noLimit"),"1000","500","100" }));
        cbLimit.setMaximumSize(new java.awt.Dimension(120, 21));
        cbLimit.setMinimumSize(new java.awt.Dimension(120, 21));
        cbLimit.setPreferredSize(new java.awt.Dimension(120, 21));

        itemFilterBySelection.setText(constBundle.getString("filterBySelection"));
        itemFilterBySelection.setEnabled(false);

        itemFilterExcludingSelection.setText(constBundle.getString("filterExcludingSelection"));
        itemFilterExcludingSelection.setEnabled(false);

        ifValueViewer.setClosable(true);
        ifValueViewer.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        ifValueViewer.setTitle(constBundle.getString("valueViewer")
        );
        ifValueViewer.setFrameIcon(null);
        ifValueViewer.setInheritsPopupMenu(true);
        ifValueViewer.setPreferredSize(new java.awt.Dimension(162, 50));
        ifValueViewer.setVisible(true);
        ifValueViewer.addInternalFrameListener(new javax.swing.event.InternalFrameListener()
        {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt)
            {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt)
            {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt)
            {
                ifValueViewerInternalFrameClosing(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt)
            {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt)
            {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt)
            {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt)
            {
            }
        });

        toolbarValueViewer.setRollover(true);

        cbbValueViewType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Plain", "Image", "Text", "Binary" }));
        cbbValueViewType.setMaximumSize(new java.awt.Dimension(90, 27));
        cbbValueViewType.setMinimumSize(new java.awt.Dimension(60, 27));
        toolbarValueViewer.add(cbbValueViewType);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/viewdata.png")));
setMinimumSize(new java.awt.Dimension(500, 400));

toolBar.setRollover(true);

btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/storedata.png"))); // NOI18N
btnSave.setToolTipText(constBundle.getString("saveChange"));
btnSave.setEnabled(false);
btnSave.setFocusable(false);
btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
toolBar.add(btnSave);

btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/readdata.png"))); // NOI18N
btnRefresh.setToolTipText(constBundle.getString("refreshPageData"));
btnRefresh.setFocusable(false);
btnRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
toolBar.add(btnRefresh);

btnUndo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/edit_undo.png"))); // NOI18N
btnUndo.setToolTipText(constBundle.getString("undoChange"));
btnUndo.setEnabled(false);
btnUndo.setFocusable(false);
btnUndo.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnUndo.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
toolBar.add(btnUndo);

btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/delete.png"))); // NOI18N
btnDelete.setToolTipText(constBundle.getString("deleteSelectedRow"));
btnDelete.setEnabled(false);
btnDelete.setFocusable(false);
btnDelete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnDelete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
toolBar.add(btnDelete);
toolBar.add(jSeparator4);

btnSortFilter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/sortfilter.png"))); // NOI18N
btnSortFilter.setToolTipText(constBundle.getString("sortFilterToolTipText"));
btnSortFilter.setFocusable(false);
btnSortFilter.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnSortFilter.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
toolBar.add(btnSortFilter);
toolBar.add(jSeparator5);

limitBar.setMaximumSize(new java.awt.Dimension(200, 22));
limitBar.setMinimumSize(new java.awt.Dimension(160, 22));
limitBar.setOpaque(false);
limitBar.setPreferredSize(new java.awt.Dimension(110, 22));

jLabel1.setText(constBundle.getString("rowPerPage")
    );
    limitBar.add(jLabel1);

    spnPaging.setModel(new javax.swing.SpinnerNumberModel(1000, 1, 10000, 1));
    spnPaging.setEnabled(false);
    spnPaging.setMaximumSize(new java.awt.Dimension(110, 22));
    spnPaging.setMinimumSize(new java.awt.Dimension(80, 22));
    spnPaging.setName(""); // NOI18N
    spnPaging.setPreferredSize(new java.awt.Dimension(80, 22));
    limitBar.add(spnPaging);

    toolBar.add(limitBar);

    cbType.setEditable(true);
    cbType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { ".txt", ".jpg", ".jpeg", ".png", ".pdf", ".doc", ".xlsx", ".xls", ".xml" }));
    cbType.setMaximumSize(new java.awt.Dimension(100, 21));
    toolBar.add(cbType);

    btnOpen.setText(constBundle.getString("openFile"));
    btnOpen.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnOpenActionPerformed(evt);
        }
    });
    toolBar.add(btnOpen);

    getContentPane().add(toolBar, java.awt.BorderLayout.PAGE_START);

    toolbarData.setRollover(true);
    toolbarData.setMaximumSize(new java.awt.Dimension(1330, 500));

    btnAddRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/addRow.png"))); // NOI18N
    btnAddRow.setToolTipText(constBundle.getString("addNewRow")
    );
    btnAddRow.setEnabled(false);
    btnAddRow.setFocusable(false);
    btnAddRow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnAddRow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnAddRow);

    btnUndoRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/cancleRow.png"))); // NOI18N
    btnUndoRow.setToolTipText(constBundle.getString("undoChange")
    );
    btnUndoRow.setEnabled(false);
    btnUndoRow.setFocusable(false);
    btnUndoRow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnUndoRow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnUndoRow);

    btnSaveRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/saveRow.png"))); // NOI18N
    btnSaveRow.setToolTipText(constBundle.getString("saveChange"));
    btnSaveRow.setEnabled(false);
    btnSaveRow.setFocusable(false);
    btnSaveRow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnSaveRow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnSaveRow);
    toolbarData.add(jSeparator1);

    btnDeleteRow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/deleteRow.png"))); // NOI18N
    btnDeleteRow.setToolTipText(constBundle.getString("deleteSelectedRow"));
    btnDeleteRow.setEnabled(false);
    btnDeleteRow.setFocusable(false);
    btnDeleteRow.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnDeleteRow.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnDeleteRow);
    toolbarData.add(jSeparator2);

    btnRefreshCurrentPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/refreshCurrentPage.png"))); // NOI18N
    btnRefreshCurrentPage.setToolTipText(constBundle.getString("refreshPageData"));
    btnRefreshCurrentPage.setFocusable(false);
    btnRefreshCurrentPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnRefreshCurrentPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnRefreshCurrentPage);
    toolbarData.add(filler1);

    btnFirstPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/previousFirstPage.png"))); // NOI18N
    btnFirstPage.setFocusable(false);
    btnFirstPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnFirstPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnFirstPage);

    btnPrePage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/prePage.png"))); // NOI18N
    btnPrePage.setFocusable(false);
    btnPrePage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnPrePage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnPrePage);

    spnPage.setMaximumSize(new java.awt.Dimension(40, 22));
    spnPage.setMinimumSize(new java.awt.Dimension(40, 22));
    toolbarData.add(spnPage);

    btnNextPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/nextPage.png"))); // NOI18N
    btnNextPage.setFocusable(false);
    btnNextPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnNextPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnNextPage);

    btnLastPage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/forwardLastPage.png"))); // NOI18N
    btnLastPage.setFocusable(false);
    btnLastPage.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
    btnLastPage.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
    toolbarData.add(btnLastPage);

    getContentPane().add(toolbarData, java.awt.BorderLayout.PAGE_END);

    splitPane.setDividerLocation(400);
    splitPane.setDividerSize(3);
    splitPane.setResizeWeight(0.7);

    pnlData.setLayout(new java.awt.BorderLayout());

    spTable.setAutoscrolls(true);

    tblData.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {
            {null, null, null, null},
            {null, null, null, null},
            {null, null, null, null},
            {null, null, null, null}
        },
        new String []
        {
            "Title 1", "Title 2", "Title 3", "Title 4"
        }
    ));
    tblData.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
    tblData.setGridColor(new java.awt.Color(204, 204, 204));
    tblData.setOpaque(false);
    tblData.setRowHeight(20);
    tblData.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    spTable.setViewportView(tblData);

    pnlData.add(spTable, java.awt.BorderLayout.CENTER);

    splitPane.setTopComponent(pnlData);

    spnlValueNormal.setMinimumSize(new java.awt.Dimension(50, 30));
    spnlValueNormal.setPreferredSize(new java.awt.Dimension(146, 50));

    ttarValueViewer.setColumns(20);
    ttarValueViewer.setLineWrap(true);
    ttarValueViewer.setRows(5);
    spnlValueNormal.setViewportView(ttarValueViewer);

    javax.swing.GroupLayout pnlValueViewerLayout = new javax.swing.GroupLayout(pnlValueViewer);
    pnlValueViewer.setLayout(pnlValueViewerLayout);
    pnlValueViewerLayout.setHorizontalGroup(
        pnlValueViewerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(spnlValueNormal, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE)
    );
    pnlValueViewerLayout.setVerticalGroup(
        pnlValueViewerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(spnlValueNormal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 421, Short.MAX_VALUE)
    );

    splitPane.setBottomComponent(pnlValueViewer);

    getContentPane().add(splitPane, java.awt.BorderLayout.CENTER);

    pnlState.setLayout(new java.awt.GridLayout(1, 0));

    txfdState.setEditable(false);
    txfdState.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.background"));
    txfdState.setMaximumSize(new java.awt.Dimension(100, 30));
    txfdState.setMinimumSize(new java.awt.Dimension(50, 26));
    pnlState.add(txfdState);

    getContentPane().add(pnlState, java.awt.BorderLayout.SOUTH);

    menuFile.setText(constBundle.getString("file"));

    itemExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
    itemExit.setText(constBundle.getString("exit"));
    menuFile.add(itemExit);

    menuBar.add(menuFile);

    menuEdit.setText(constBundle.getString("edit"));

    itemRefresh.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
    itemRefresh.setText(constBundle.getString("refresh"));
    menuEdit.add(itemRefresh);

    itemUndo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
    itemUndo.setText(constBundle.getString("undo"));
    itemUndo.setEnabled(false);
    menuEdit.add(itemUndo);
    menuEdit.add(sepEdit);

    itemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
    itemSave.setText(constBundle.getString("save"));
    itemSave.setEnabled(false);
    menuEdit.add(itemSave);

    itemDelete.setText(constBundle.getString("delete"));
    itemDelete.setEnabled(false);
    menuEdit.add(itemDelete);

    menuBar.add(menuEdit);

    menuTool.setText(constBundle.getString("tool"));

    itemSortFilter.setText(constBundle.getString("sortOrFilter"));
    menuTool.add(itemSortFilter);
    menuTool.add(sepTool);

    itemRemoveFilter.setText(constBundle.getString("removeFilter"));
    menuTool.add(itemRemoveFilter);
    menuTool.add(sepTool2);

    itemSortAscending.setText(constBundle.getString("sortAscending"));
    itemSortAscending.setEnabled(false);
    menuTool.add(itemSortAscending);

    itemSortDescending.setText(constBundle.getString("sortDescending"));
    itemSortDescending.setEnabled(false);
    menuTool.add(itemSortDescending);

    itemRemoveSort.setText(constBundle.getString("removeSort"));
    menuTool.add(itemRemoveSort);

    menuBar.add(menuTool);

    menuView.setText(constBundle.getString("view"));

    cbItemToolBar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
    cbItemToolBar.setSelected(true);
    cbItemToolBar.setText(constBundle.getString("toolbar")
    );
    cbItemToolBar.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbItemToolBarActionPerformed(evt);
        }
    });
    menuView.add(cbItemToolBar);

    cbItemLimitBar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
    cbItemLimitBar.setSelected(true);
    cbItemLimitBar.setText(constBundle.getString("limitBar"));
    cbItemLimitBar.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbItemLimitBarActionPerformed(evt);
        }
    });
    menuView.add(cbItemLimitBar);

    cbItemValueViewer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
    cbItemValueViewer.setSelected(true);
    cbItemValueViewer.setText(constBundle.getString("valueViewer"));
    cbItemValueViewer.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbItemValueViewerActionPerformed(evt);
        }
    });
    menuView.add(cbItemValueViewer);
    menuView.add(sepView2);

    cbItemDefaultView.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
    cbItemDefaultView.setSelected(true);
    cbItemDefaultView.setText(constBundle.getString("defaultView"));
    cbItemDefaultView.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            cbItemDefaultViewActionPerformed(evt);
        }
    });
    menuView.add(cbItemDefaultView);

    menuBar.add(menuView);

    menuHelp.setText(constBundle.getString("help"));

    itemHelpContent.setText(constBundle.getString("helpContent"));
    menuHelp.add(itemHelpContent);

    menuBar.add(menuHelp);

    setJMenuBar(menuBar);

    pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void initTabData(int totalRowCount) throws ClassNotFoundException, SQLException
    {
        //table model
        makePagingModel(totalRowCount);
        tblData.setModel(pagingModel);
        //column header to display row id
        rowIdModel = new RowIdHeaderModel(pagingModel);       
        //table ScrollPanel
        JScrollPane spData = PagingModel.createPagingScrollPaneForTable(tblData, rowIdModel);
        pnlData.remove(spTable);
        pnlData.add(spData, BorderLayout.CENTER);
        splitPane.setTopComponent(pnlData);
        //listener
        pagingModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                tableModelChangeActionPerformed(e);
            }
        });
        
        JTableHeader tblHeader = tblData.getTableHeader();
        tblHeader.setFont(new Font("SansSerif", Font.BOLD, 12));    
        //make header and data scroll concurrently
        tblData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);         
        tblData.getTableHeader().setPreferredSize(
                new Dimension(10000, tblHeader.getPreferredSize().height));
        tblHeader.setResizingAllowed(true);
        tblHeader.setReorderingAllowed(false);
        //tblHeader.setPreferredSize(new Dimension(this.tblData.getTableHeader().getPreferredSize().width, 40));
        //tblData.updateUI();
        /*
        //sorting
        tblHeader.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                columHeaderMouseClicked(e);
            }
        });*/
        
        //make data cell and row allowed
        tblData.setColumnSelectionAllowed(false);
        tblData.setCellSelectionEnabled(true);
        //btn status
        btnRefresh.setEnabled(true);//btnRefresh always enable
        btnAddRow.setEnabled(isEditable);//only depend on isEditable
        this.updateButtonStatus(false, false, false);     
        //Value Viewer default not to show
        cbItemValueViewer.setSelected(false);
        cbItemValueViewerActionPerformed(null); 
        
        //paging
        spinnerNumModel = new SpinnerNumberModel();
        spinnerNumModel.setMinimum(1);
        spinnerNumModel.setStepSize(1);
        spinnerNumModel.setValue(1);
        spnPage.setModel(spinnerNumModel);
        spinnerNumModel.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                SpinnerNumberModel snm = (SpinnerNumberModel) e.getSource();
                int pageOffset = (Integer.valueOf(snm.getValue().toString()) - 1);
                logger.info("current page offset=" + pageOffset);
                try
                {
                    pagingModel.setPageOffSet(pageOffset);
                    tblData.repaint();
                } catch (Exception ex)
                {
                    logger.error(ex.getMessage());
                    JOptionPane.showMessageDialog(null, ex, 
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                    ex.printStackTrace(System.out);
                }
            }
        });
        ActionListener pagingAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                pagingActionPeformed(e);
            }
        };
        btnFirstPage.addActionListener(pagingAction);
        btnPrePage.addActionListener(pagingAction);
        btnNextPage.addActionListener(pagingAction);
        btnLastPage.addActionListener(pagingAction);

        //editing
        tblData.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tblDataMouseClicked(evt);
            }
        });
        tblData.addKeyListener(new KeyAdapter(){
            @Override
            public void keyReleased(KeyEvent evt) {
                tblDataKeyPressed(evt);
            }
        });
        tblData.addPropertyChangeListener(new PropertyChangeListener()
        {
            @Override
            public void propertyChange(PropertyChangeEvent evt)
            {
                tblDataPropertyChange(evt);
            }
        });
        
        //save 
        ActionListener saveAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveDataChangeAction(e);
            }
        };
        itemSave.addActionListener(saveAction);
        btnSave.addActionListener(saveAction);
        btnSaveRow.addActionListener(saveAction);        
        //undo row -- 1.cancle new row;2.undo existed row change
        ActionListener undoAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                undoDataChangeAction(e);
            }
        };
        itemUndo.addActionListener(undoAction);
        btnUndo.addActionListener(undoAction);
        btnUndoRow.addActionListener(undoAction);
        //add
        btnAddRow.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                addNewRowActionPerformed(e);               
            }
        });
        //delete
        ActionListener deleteAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                deleteActionPerformed(evt);
            }
        };
        btnDeleteRow.addActionListener(deleteAction);
        itemDelete.addActionListener(deleteAction);
        btnDelete.addActionListener(deleteAction);
        //refresh
        ActionListener refreshAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                refreshCurrentPageData();
            }
        };
        btnRefreshCurrentPage.addActionListener(refreshAction);
        itemRefresh.addActionListener(refreshAction);
        btnRefresh.addActionListener(refreshAction);
        
        //data fill
        this.refreshCurrentPageData();
    }
    private void makePagingModel(int totalRowCount) throws ClassNotFoundException, SQLException
    {
        TreeEnum.TreeNode type = relation.getType();
        Class[] typeArray;
        switch (type) {
            case TABLE:
                TableInfoDTO table = (TableInfoDTO) relation;
                List<ColumnInfoDTO> columns = table.getColumnInfoList();
                int columnCount = columns.size();
                typeArray = new Class[columnCount];
                for (int j = 0; j < columnCount; j++) {
                    typeArray[j] = DataTypeController.getInstance().getTypeClassFromInternalType(columns.get(j).getDatatype().getName());
                }
                headerArray = new Object[columnCount];
                for (int i = 0; i < columnCount; i++) {
                    headerArray[i] = this.getHeaderFromColumn(columns.get(i));
                    if (columns.get(i).isPk()) {
                        pkColumnNum.add(i);
                    }
                }
                isEditable = pkColumnNum.size() > 0;
                break;
            case VIEW:
            case MATERIALIZED_VIEW:
                isEditable = false;
                /*
                String headerStr = (view.getDefinition().replace(";", "").split("FROM")[0]).split("SELECT")[1];
                logger.info("header str=" + headerStr);
                String[] columnArray = (headerStr.replace("\n", "")).split(",");
                int length = columnArray.length;
                headerArray = new String[length];
                System.arraycopy(columnArray, 0, headerArray, 0, length);
                */
                headerArray = DataController.getInstance().getHearderArray(relation.getHelperInfo()
                        , SyntaxController.getInstance().getName(relation.getHelperInfo().getSchema()) + "." + SyntaxController.getInstance().getName(relation.getName()));
                ViewInfoDTO view = (ViewInfoDTO) relation;
                view.setHeaders((String[]) headerArray);
                typeArray = null;
                break;
            case PARTITION:
                isEditable = false;
                PartitionInfoDTO partion = (PartitionInfoDTO)relation;
                
                headerArray = DataController.getInstance().getHearderArray(relation.getHelperInfo()
                        , SyntaxController.getInstance().getName(relation.getName()));
                partion.setHeaders((String[]) headerArray);
                typeArray = null;
                break;
            default:
                logger.error(relation.getType() + " is a wrong type");
                return;
        }

        pagingModel = new PagingModel(relation, filterCondition, null,
                totalRowCount, PAGE_SIZE, isEditable, typeArray, headerArray);
    }
    private ColumnHeaderInfoDTO getHeaderFromColumn(ColumnInfoDTO columnInfo)
    {
        ColumnHeaderInfoDTO headerInfo = new ColumnHeaderInfoDTO();
        headerInfo.setName(columnInfo.getName());
        headerInfo.setPosition(columnInfo.getPosition());
        headerInfo.setPk(columnInfo.isPk());
        headerInfo.setFullDatatype(columnInfo.getDatatype().getFullname());
        headerInfo.setNotNull(columnInfo.isNotNull());
        headerInfo.setDefaultValue(columnInfo.getDefaultValue());
        headerInfo.setDataType(columnInfo.getDatatype().getName());
        return headerInfo;
    }
     
    //tblData action order: KeyPressed/MouseClicked > (cellEditor start editing)PropertyChange > (model setValue)ModelChangeAction
    private void tblDataKeyPressed(KeyEvent evt)
    {
        logger.debug("enter");
        int column = tblData.getSelectedColumn();
        int row = tblData.getSelectedRow();        
        logger.debug("row=" + row + ", column=" + column);
        if (row == -1 || column == -1)
        {
            logger.warn("no valid row/column is selected.");
            return;
        }
        
        this.updateValueViewer(tblData.getValueAt(row, column));
        this.updateButtonStatus((row == changedRowNum) && isEditable,
                (row == changedRowNum || row == newRowNum) && isEditable, isEditable);
    }
    private void tblDataMouseClicked(MouseEvent evt)
    {
        logger.debug("enter, ClickCount=" + evt.getClickCount());
        int column = tblData.getSelectedColumn();
        int row = tblData.getSelectedRow();        
        logger.debug("row=" + row + ", column=" + column);
        if (row == -1 || column == -1)
        {
            logger.warn("no valid row/column is selected.");
            return;
        }
        
        this.updateValueViewer(tblData.getValueAt(row, column));
        this.updateButtonStatus((row == changedRowNum) && isEditable,
                (row == changedRowNum || row == newRowNum) && isEditable, isEditable);
        /*
        logger.debug("ClickCount=" + evt.getClickCount());//here always if(evt.getClickCount() == 1) 
        logger.debug("Mouse clicked: Row=" + row + ",newRowNum=" + newRowNum + ",changedRowNum=" + changedRowNum);
        
        // the following must in this order 
        //in the same row with last insert/update row
        //if (row == newRowNum || row == changedRowNum) //do nothing
        //in a different same row with last insert/update row
        if ((newRowNum != -1 && row != newRowNum)
                || (changedRowNum != -1 && row != changedRowNum)) {
            //save last insert/update row
            this.saveDataChangeAction(null);
//            int response = JOptionPane.showConfirmDialog(this
//                    , constBundle.getString("hint"), constBundle.getString("changeSaveOrNot")
//                    , JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
//            if(JOptionPane.YES_OPTION == response)
//            {
//                this.saveDataChangeAction(null);
//            }else
//            {
//                this.undoDataChangeAction(null);
//            }
        }
        //first time to update this row
        if (newRowNum == -1 && changedRowNum != row) //change
        {
            changedRowNum = row;
            //record original row data
            Object[] array = pagingModel.getRowData(row);
            oldRowData = new Object[array.length];
            //System.arraycopy(array, 0, oldRowData, 0, array.length);
            for (int i = 0; i < array.length; i++) {
                //[important]because Boolean's special CellEditor(checkbox) update value
                //before tableChangeAction and this action,
                //so record old row like this or from setValueAt of PagingModel
                if (Boolean.class == pagingModel.getColumnClass(i)) {
                    //because has action before this, soinverse value is the old value
                    //(also work for origin value is NULL(NULL>TRUE same to FALSE>TRUE), because this's also just for compare diff)
                    oldRowData[i] = !(Boolean) array[i];
                }else
                {
                    oldRowData[i] = array[i];
                }
            }
            logger.debug("changedRowNum=" + changedRowNum + ", oldRowData=" + Arrays.toString(oldRowData));
        }

        //update edited value
        if (newRowNum != -1 && newRowNum == row) {
            ColumnHeaderInfoDTO header = (ColumnHeaderInfoDTO) headerArray[column];
            logger.debug(header.getDataType());
            if ("bytea".equals(header.getDataType())) {
                FileChooserDialog filechooser = new FileChooserDialog();
                int response = filechooser.showFileChooser(this, constBundle.getString("chooseFileToInsert"), "");
                if (response == JFileChooser.APPROVE_OPTION) {
                    String choosed = filechooser.getChoosedFileDir();
                    logger.debug(choosed);
                    if (choosed != null) {
                        pagingModel.setValueAt(choosed, row, column);
                    }
                }
            }
        }
        
        //for all
        this.updateButtonStatus((row == changedRowNum) && isEditable,
                (row == changedRowNum || row == newRowNum) && isEditable, isEditable);
        this.updateValueViewer(tblData.getValueAt(row, column));
        //evt.consume();
        */
    }
    private void tblDataPropertyChange(PropertyChangeEvent evt)
    {
        logger.debug("enter");
        if (evt.getPropertyName().equals("tableCellEditor")
                && evt.getNewValue() != null)
        {
            logger.debug("cell edithing:oldValue=" + evt.getOldValue()
                    + ", newValue=" + evt.getNewValue());
            int row = tblData.getSelectedRow();
            int column = tblData.getSelectedColumn();
            logger.debug("row=" + row + ", column=" + column);
            if (newRowNum != row) {
                this.recordOldData(row);
                changedRowNum = row;
            }
           
            //use FileChooser to edit bytea value(not work)
            ColumnHeaderInfoDTO header = (ColumnHeaderInfoDTO) headerArray[column];
            logger.debug("DataType=" + (header ==null? "null":header.getDataType()));
            if ("bytea".equals(header.getDataType()) 
                    || header.getDataType().endsWith("blob")
                    || header.getDataType().endsWith("clob")) {
                FileChooserDialog filechooser = new FileChooserDialog();
                int response = filechooser.showFileChooser(this,
                         constBundle.getString("chooseFileToInsert"), "");
                if (response == JFileChooser.APPROVE_OPTION) {
                    String choosed = filechooser.getChoosedFileDir();
                    logger.debug("choosed=" + choosed);
                    if (choosed != null) {
                        //update value of CellEditor with choosed dir
                        tblData.getCellEditor()
                                .getTableCellEditorComponent(tblData, choosed, true, row, column);
                    }
                }
            }

            this.updateButtonStatus(isEditable, isEditable, isEditable);
        }
    }
    //pagingModel
    private void tableModelChangeActionPerformed(TableModelEvent e)
    {
        //INSERT=1, UPDATE=0, DELETE=-1
        logger.info("tabChangedEvent Type=" 
                + (e.getType()==1?"INSERT" : (e.getType()==0?"UPDATE":"DELETE"))
                + ",isAllColumn=" + (e.getColumn() == TableModelEvent.ALL_COLUMNS));
        try
        {
            Object value = spinnerNumModel.getValue();
            if (value != null && Integer.valueOf(value.toString()) != (pagingModel.getPageOffset() + 1))
            {
                logger.info("Turn to Page=" + (pagingModel.getPageOffset() + 1) 
                        + ",realTotalRow=" + pagingModel.getRealRowCount());
                spinnerNumModel.setMaximum((pagingModel.getRealRowCount() / PAGE_SIZE + 1));
                spinnerNumModel.setValue(pagingModel.getPageOffset() + 1);
                rowIdModel.fireTableDataChanged();//refresh row id                
            } 
        } catch (ClassNotFoundException | NumberFormatException | SQLException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(), 
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }  
    private void recordOldData(int row)
    {
        //first time to update this row
        if (newRowNum == -1 && changedRowNum != row) //change
        {
            //changedRowNum = row;
            //record original row data
            Object[] array = pagingModel.getRowData(row);
            oldRowData = new Object[array.length];
            //System.arraycopy(array, 0, oldRowData, 0, array.length);
            for (int i = 0; i < array.length; i++) {
                //[important]because Boolean's special CellEditor(checkbox) update value
                //before tableChangeAction and this action,
                //so record old row like this or from setValueAt of PagingModel
                if (Boolean.class == pagingModel.getColumnClass(i)) {
                    //because has action before this, soinverse value is the old value
                    //(also work for origin value is NULL(NULL>TRUE same to FALSE>TRUE), because this's also just for compare diff)
                    oldRowData[i] = !(Boolean) array[i];
                }else
                {
                    oldRowData[i] = array[i];
                }
            }
            //logger.debug("changedRowNum=" + changedRowNum + ", oldRowData=" + Arrays.toString(oldRowData));
        } 
    }
    private void updateValueViewer(Object value) 
    {
        logger.debug("enter");
        if (!cbItemValueViewer.isSelected()) {
            logger.warn("valueViewer is not show.");
            return;
        }

        if (value == null || !(value instanceof InputStream)) {
            /*if (!cbItemValueViewer.isSelected()) {
                logger.warn("valueViewer is not show.");
                return;
            }*/
            logger.debug("value(part)="
                    + (value == null ? "null"
                            : value.toString().substring(1, value.toString().length() > 50 ? 50 : value.toString().length())));
            pnlValueViewer.removeAll();
            pnlValueViewer.add(spnlValueNormal, BorderLayout.CENTER);
            ttarValueViewer.setText(value == null ? "<null>" : value.toString());
        } else 
        {
            logger.debug("value(part)="
                + (value.toString().length() > 50 ? value.toString().substring(1, 50) : value.toString()));
            try {
                InputStream ips = (InputStream) value;
                logger.debug("ips available=" + ips.available());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ips.mark(0);                
                //ips.transferTo(baos);//jre9
                final int DEFAULT_BUFFER_SIZE = 8192;
                byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
                int read;
                while ((read = ips.read(buffer, 0, DEFAULT_BUFFER_SIZE)) >= 0) {
                    baos.write(buffer, 0, read);
                }
                logger.debug("ips after transferTo=" + ips.available());
                ips.reset();
                logger.debug("ips after reset=" + ips.available());
                InputStream ips4show = new ByteArrayInputStream(baos.toByteArray());
                InputStream ips4type = new ByteArrayInputStream(baos.toByteArray());               
                String typeStr = StreamValueConvertor.getMimeTypeInfo(ips4type).toLowerCase();
                logger.info(typeStr);
               
                /* this.saveToFile(ips4show);
                if (!cbItemValueViewer.isSelected())
                {
                    return;
                } */
                
                pnlValueViewer.removeAll();//cleanValueViewer();
                if (typeStr.contains("png")
                        || typeStr.contains("jpg")) {
                    logger.debug("image");
                    JPanel pnlImage = StreamValueConvertor.generateImagePanel(ips4show);
                    pnlValueViewer.add(pnlImage, BorderLayout.CENTER);
                    pnlImage.setVisible(true);
                } else {
                    pnlValueViewer.add(spnlValueNormal, BorderLayout.CENTER);
                    ttarValueViewer.setText(StreamValueConvertor.streamToString(ips4show));
                }
            } catch (IOException ex) {
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex,
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out);
            }
        }
        pnlValueViewer.repaint();
        pnlValueViewer.validate();
    }
    
    //paging action
    private void pagingActionPeformed(ActionEvent evt)
    {
        JButton btn = (JButton) evt.getSource();
        if (newRowNum >= 0 || changedRowNum >= 0)
        {
            int response = JOptionPane.showConfirmDialog(this, constBundle.getString("isStoreUnsavedData"),
                    constBundle.getString("thereUnsavedData"), JOptionPane.YES_NO_CANCEL_OPTION);
            logger.debug("response=" + response);
            switch (response) {
                case JOptionPane.YES_OPTION:
                    this.saveDataChangeAction(null);
                    break;
                case JOptionPane.NO_OPTION:
                    if (tblData.getCellEditor() != null)
                    {
                        tblData.getCellEditor().stopCellEditing();
                    }   break;
                case JOptionPane.CANCEL_OPTION:
                    return;//do nothing
            }
        }
        try
        {
            if (btn == btnFirstPage)
            {
                logger.info("go to first page");
                spinnerNumModel.setValue(1);
                pagingModel.setPageOffSet(0);
                tblData.repaint();
            } else if (btn == btnPrePage)
            {
                logger.info("go to pre page");
                int currentPage = Integer.valueOf(spinnerNumModel.getValue().toString());
                if (currentPage > 1)
                {
                    spinnerNumModel.setValue(currentPage - 1);
                    pagingModel.setPageOffSet(currentPage - 2);
                    tblData.repaint();
                }
            } else if (btn == btnNextPage)
            {
                logger.info("go to next page");
                int currentPage = Integer.valueOf(spinnerNumModel.getValue().toString());
                spinnerNumModel.setValue(currentPage + 1);
                pagingModel.setPageOffSet(currentPage + 1 - 1);
                tblData.repaint();
            } else if (btn == btnLastPage)
            {
                logger.info("go to last page");
                int totalPage = pagingModel.getPageCount();
                spinnerNumModel.setValue(totalPage);
                pagingModel.setPageOffSet(totalPage - 1);
                tblData.repaint();
            }
            newRowNum = -1;
            changedRowNum = -1;
        } catch (ClassNotFoundException | NumberFormatException | SQLException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex, 
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    
    //button action
    //Refresh whole current page
    private void refreshCurrentPageData()
    {
        logger.debug("Refresh");
        try {
            //status
            tblData.clearSelection();
            tblData.removeEditor();
            newRowNum = -1;
            changedRowNum = -1;
            this.updateButtonStatus(false, false, false);
            //fill date
            pagingModel.updateQueryCondition(filterCondition, sortCondition);
            rowIdModel.update(pagingModel);
            tblData.repaint();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(), 
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    //undo change 
    private void undoDataChangeAction(ActionEvent evt)
    {
        logger.debug("undo");
        int currentRow = tblData.getSelectedRow();
        logger.info("row=" + currentRow);
        if (currentRow < 0)
        {
            return;
        }
        logger.info("newRowNum =" + newRowNum + ",changedRow=" + changedRowNum);
        if (currentRow == newRowNum)
        {
            pagingModel.removeRow(newRowNum);
            newRowNum = -1;
        } else if (currentRow == changedRowNum)
        {
            if(tblData.getCellEditor()!=null)
            {
                tblData.getCellEditor().cancelCellEditing();
            }
            pagingModel.setRowData(changedRowNum, oldRowData);
            tblData.repaint();            
        } 
        this.updateButtonStatus(false, false, isEditable);
    }
    //delete row
    private void deleteActionPerformed(ActionEvent evt)
    {
        logger.debug("Delete");
        
        int row = tblData.getSelectedRow();
        logger.debug("row=" + row);
        if (row < 0)
        {
            logger.warn("no row selected and return.");
            return;
        }
        if (!(relation instanceof TableInfoDTO))
        {
            logger.warn("this is not a table so cannot delete row from it.");
            return;
        }
        int size = pkColumnNum.size();
        if (size == 0) {
            logger.warn("This table has no pk, so we cannnot delete row from it.");
            return;
        }
        
        int response=JOptionPane.showConfirmDialog(this, 
                constBundle.getString("sureToDeleteRow"), 
                constBundle.getString("sureToDelete"), JOptionPane.YES_NO_OPTION);
        if(response == JOptionPane.NO_OPTION)
        {
            return;
        }
        //delete from table model
        if(row == newRowNum)
        {
            pagingModel.removeRow(row);
            rowIdModel.update(pagingModel);
        }
        //delete from db table and table model
        Object[] paras = new Object[size];
        StringBuilder condition = new StringBuilder();
        for (int i = 0; i < size; i++) {
            paras[i] = pagingModel.getValueAt(row, pkColumnNum.get(i));
            condition.append(i == 0 ? "" : "AND ")
                    .append(pagingModel.getColumnName(i).split("<br>")[0].replace("<html>", ""))
                    .append(" = ? ");
        }
        try {
            DataController.getInstance()
                    .deleteRow((TableInfoDTO) relation, condition.toString(), paras);
            pagingModel.removeRow(row);
            rowIdModel.update(pagingModel);
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }        
    }
    //add empty row
    private void addNewRowActionPerformed(ActionEvent evt)
    {  
        logger.debug("Add new row");
        newRowNum = pagingModel.getRowCount();
        logger.info("newRowNum=" + newRowNum);
        Object[] newRow = new Object[pagingModel.getColumnCount()];
        pagingModel.addRow(newRow);
        rowIdModel.update(pagingModel);
        tblData.setEditingRow(newRowNum);
        
        this.updateButtonStatus(isEditable, isEditable, true);
    }
    //save change(update/insert)
    private void saveDataChangeAction(ActionEvent evt)
    {
        logger.debug("Save");
        if (tblData.getCellEditor() != null)
        {
            //false when editor has invalid value
            if(!tblData.getCellEditor().stopCellEditing())
            {
                logger.debug("stopCellEditing is false then wait.");
                return;
            }
        }
        logger.debug("newRowNum=" + newRowNum + "changedRowNum" + changedRowNum);
        if (newRowNum >= 0) {
            this.insertRow(newRowNum);
        } else //if (changedRowNum >= 0)
        {
             Object[] array = pagingModel.getRowData(changedRowNum);
             logger.debug(Arrays.toString(array) + " | " + Arrays.toString(oldRowData));
             if(!Arrays.equals(array, oldRowData))
             {
                 this.updateRow(changedRowNum);
             }else
             {
                 logger.debug("the row changed nothing so needn't update.");
             }
        }
    } 
    //db sql execution
    private void insertRow(int row)
    {
        logger.debug("Enter:row=" + row);       
        //List<Integer> columnNumList = new ArrayList(); 
        boolean hasInputedValue = false;
        int column = pagingModel.getColumnCount();
        for (int i = 0; i < column; i++)
        {
            Object value = pagingModel.getValueAt(row, i);
            if (value != null && value.toString() != null)
            {
                //columnNumList.add(i);
                hasInputedValue = !value.toString().isEmpty();
                if(hasInputedValue)
                {
                    break;
                }
            }
        }
        
        if(!hasInputedValue)
        {
            logger.warn("The new row is an empty row, then needn't insert it.");
            pagingModel.removeRow(row);
            newRowNum = -1;
        }else
        {
            try
            {
                DataController.getInstance().insertRow(
                        (TableInfoDTO) relation, headerArray, pagingModel.getRowData(row));
                newRowNum = -1;
                this.updateButtonStatus(false, false, isEditable);
                this.refreshCurrentPageData();
            } catch (Exception ex)
            { 
                logger.error(ex.getMessage());
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace(System.out); 
                //error then step into editing row
                tblData.setEditingRow(row);
            }
        } 
    }
    private void updateRow(int row)
    {
        logger.debug("Enter: row=" + row); 
        TableInfoDTO table = (TableInfoDTO) relation;
        try
        {
            DataController.getInstance().updateValue(table, headerArray, pkColumnNum
                    , pagingModel.getRowData(row) , oldRowData);
            changedRowNum = -1;
            this.updateButtonStatus(false, false, isEditable);
            this.refreshCurrentPageData();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
            //error then step into editing row
            tblData.setEditingRow(row);
        }
    }

    //btn status
    private void updateButtonStatus(boolean enbaleUndo
            , boolean enbaleSave, boolean enableDelete)
    {
        logger.debug("enbaleUndo=" + enbaleUndo
                + " enbaleSave=" + enbaleSave
                + " enableDelete=" + enableDelete);
        //btnRefresh.setEnabled(true);//btnRefresh always enable
        //btnAddRow.setEnabled(isEditable);//only depend on isEditable
        this.enableUndoBtns(enbaleUndo);
        this.enableSaveBtns(enbaleSave);
        this.enableDeleteBTns(enableDelete);
    }
    private void enableSaveBtns(boolean flag)
    {
        itemSave.setEnabled(flag);
        btnSave.setEnabled(flag);
        btnSaveRow.setEnabled(flag);
    }
    private void enableUndoBtns(boolean flag)
    {
        itemUndo.setEnabled(flag);
        btnUndo.setEnabled(flag);
        btnUndoRow.setEnabled(flag);
    }
    private void enableDeleteBTns(boolean flag)
    {
        itemDelete.setEnabled(flag);
        btnDelete.setEnabled(flag);
        btnDeleteRow.setEnabled(flag);
    }

    //TOOL-Sort/Filter
    @Deprecated
    private void columHeaderMouseClicked(MouseEvent evt)
    {
        /*logger.debug("ClickCount=" + evt.getClickCount());
        JTableHeader columnHeader = tblData.getTableHeader();
        if (columnHeader.getCursor().getType() == Cursor.E_RESIZE_CURSOR)
        {
            itemSortAscending.setEnabled(false);
            itemSortDescending.setEnabled(false);
            evt.consume();//3917.91
        } else
        {
            //tblData.clearSelection();
            //tblData.setRowSelectionAllowed(false);
            //tblData.setColumnSelectionAllowed(true);            
            int pick = columnHeader.columnAtPoint(evt.getPoint());
            logger.info("pick=" + pick);
            sortColumn = pick;
            if (sortColumn >= 0)
            {
                tblData.setColumnSelectionInterval(pick, pick);
                //tableModel[selectedTab].sortArrayList(columnIndex);  
                itemSortAscending.setEnabled(true);
                itemSortDescending.setEnabled(true);
            } else
            {
                itemSortAscending.setEnabled(false);
                itemSortDescending.setEnabled(false);
            }
        }*/
    }
    private void itemSortFilterActionPerformed(ActionEvent evt)
    {
        logger.debug(evt.getActionCommand());
        sfDialog.setLocationRelativeTo(this);
        sfDialog.setVisible(true);
        if (sfDialog.isSuccess())
        {
            filterCondition = sfDialog.getFilterCondition();
            sortCondition = sfDialog.getSortCondition();
            
            pagingModel.setRealRowCount(sfDialog.getTotalRowCount());
            this.refreshCurrentPageData();
        }
    }
    private void itemRemoveFilterActionPerformed(ActionEvent evt)
    {
        logger.debug(evt.getActionCommand());
        sfDialog.clearFilterCondition();
        filterCondition = null;
        try
        {
            String relname = SyntaxController.getInstance().getName(relation.getHelperInfo().getSchema()) 
                + "." + SyntaxController.getInstance().getName(relation.getName());
            pagingModel.setRealRowCount(DataController.getInstance().getRowCount(relation.getHelperInfo(), relname, filterCondition));//sort won't effect total row
            this.refreshCurrentPageData();
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }
    private void itemRemoveSortActionPerformed(ActionEvent evt)
    {
        logger.debug(evt.getActionCommand());
        sfDialog.clearSortCondition();
        sortCondition = null;
        this.refreshCurrentPageData();
    }
    private void itemSortAscendingActionPerformed(ActionEvent evt)
    {
        logger.debug(evt.getActionCommand());
        logger.info("column[" + sortColumn + "] ASC");
        StringBuilder sort = new StringBuilder();
        sort.append(pagingModel.getColumnName(sortColumn).split("<br>")[0].replace("<html>", ""))
               .append(" ").append(" ASC ");
        sortCondition = sort.toString();
        this.refreshCurrentPageData();
    }
    private void itemSortDescendingActionPerformed(ActionEvent evt)
    {
        logger.debug(evt.getActionCommand());
        logger.info("column[" + sortColumn + "] DESC");
        StringBuilder sort = new StringBuilder();
        sort.append(pagingModel.getColumnName(sortColumn).split("<br>")[0].replace("<html>", ""))
                .append(" ").append(" DESC ");
        sortCondition = sort.toString();
        this.refreshCurrentPageData();
    }
  
    //FILE exit
    private void exitActionPerformed()
    {
        logger.debug("Exit");
        if (newRowNum < 0 || changedRowNum < 0)
        {
            this.dispose();
        } else
        {
            int response = JOptionPane.showConfirmDialog(this, constBundle.getString("isStoreUnsavedData"),
                    constBundle.getString("thereUnsavedData"), JOptionPane.YES_NO_CANCEL_OPTION);
            logger.info("response=" + response);
            switch (response) {
                case JOptionPane.YES_OPTION:
                    this.saveDataChangeAction(null);
                    this.dispose();
                    break;
                case JOptionPane.NO_OPTION:
                    this.dispose();
                    break;
                default:
                    //cancle do nothing
                    break;
            }
        }
    }
    
    //VIEW(default show all except ValueViewer)
    private void cbItemLimitBarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbItemLimitBarActionPerformed
    {//GEN-HEADEREND:event_cbItemLimitBarActionPerformed
        logger.info("Limit Bar isSelected:" + cbItemLimitBar.isSelected());
        if (cbItemLimitBar.isSelected())
        {
            toolBar.add(limitBar);
        } else
        {
            toolBar.remove(limitBar);
            cbItemDefaultView.setSelected(false);
        }
        getContentPane().validate();
    }//GEN-LAST:event_cbItemLimitBarActionPerformed
    private void cbItemValueViewerActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbItemValueViewerActionPerformed
    {//GEN-HEADEREND:event_cbItemValueViewerActionPerformed
        logger.debug("Scratch Pad isSelected=" + cbItemValueViewer.isSelected());
        if (cbItemValueViewer.isSelected())
        {
            splitPane.setBottomComponent((pnlValueViewer));//.setBottomComponent
            pnlValueViewer.setSize(splitPane.getWidth(), splitPane.getHeight()/3);
        } else
        {
            splitPane.remove(pnlValueViewer); //ifValueViewer
            ttarValueViewer.setText("");//clean
        }
    }//GEN-LAST:event_cbItemValueViewerActionPerformed
    private void cbItemToolBarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbItemToolBarActionPerformed
    {//GEN-HEADEREND:event_cbItemToolBarActionPerformed
        logger.debug("Tool Bar isSelected=" + cbItemToolBar.isSelected());
        if (cbItemToolBar.isSelected())
        {
            getContentPane().add(toolBar, BorderLayout.NORTH);
        } else
        {
            getContentPane().remove(toolBar);
            cbItemDefaultView.setSelected(false);
        }
        getContentPane().validate();
    }//GEN-LAST:event_cbItemToolBarActionPerformed
    private void cbItemDefaultViewActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_cbItemDefaultViewActionPerformed
    {//GEN-HEADEREND:event_cbItemDefaultViewActionPerformed
        logger.debug("default view selected=" + cbItemDefaultView.isSelected());        
        if(cbItemValueViewer.isSelected())
        {
            cbItemValueViewer.setSelected(false);
            splitPane.remove(pnlValueViewer);//
        }
        if (!cbItemLimitBar.isSelected())
        {
            toolBar.add(limitBar);
            cbItemLimitBar.setSelected(true);
        }
        if (!cbItemToolBar.isSelected())
        {
            getContentPane().add(toolBar, BorderLayout.NORTH);
            getContentPane().validate();
            cbItemToolBar.setSelected(true);
        }
    }//GEN-LAST:event_cbItemDefaultViewActionPerformed
    private void ifValueViewerInternalFrameClosing(javax.swing.event.InternalFrameEvent evt)//GEN-FIRST:event_ifValueViewerInternalFrameClosing
    {//GEN-HEADEREND:event_ifValueViewerInternalFrameClosing
        logger.debug("valueViewer closing");
        cbItemValueViewer.setSelected(false);
        ttarValueViewer.setText("");//clean
    }//GEN-LAST:event_ifValueViewerInternalFrameClosing

    private void btnOpenActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOpenActionPerformed
    {//GEN-HEADEREND:event_btnOpenActionPerformed
        // TODO add your handling code here:
        int row = tblData.getSelectedRow();
        int column = tblData.getSelectedColumn();
        if (row < 0 || column < 0)
        {
            JOptionPane.showMessageDialog(this, constBundle.getString("selectValueFirst"),
                    constBundle.getString("hint"), JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        Object value = tblData.getValueAt(row, column);
        if(value == null){
            JOptionPane.showMessageDialog(this, constBundle.getString("valueIsNull"),
                    constBundle.getString("hint"), JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        logger.debug("fileType selected index = "+cbType.getSelectedIndex());
        String type = cbType.getSelectedItem()==null? "" : cbType.getSelectedItem().toString();
        logger.debug(type);
        try
        {
            if(!(value instanceof InputStream)){
                logger.warn("this is not instanceof InputStream");
                return;
            }
            InputStream ips = (InputStream) value;
            logger.debug("ips available=" + ips.available());
            /*
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ips.mark(0);
            //ips.transferTo(baos);//jre9
            final int DEFAULT_BUFFER_SIZE = 8192;
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int read;
            while ((read = ips.read(buffer, 0, DEFAULT_BUFFER_SIZE)) >= 0)
            {
                baos.write(buffer, 0, read);
            }
            logger.debug("ips after transferTo=" + ips.available());
            ips.reset();
            logger.debug("ips after reset=" + ips.available());
            InputStream ips4show = new ByteArrayInputStream(baos.toByteArray());
            */
            this.saveToFile(ips);
            logger.debug("ips available=" + ips.available());
            
           // File oldf = new File("temp");
            File file = new File("temp" + type);
            logger.debug(file.getAbsolutePath());
            //oldf.renameTo(file);
           // logger.debug("renameto:" + oldf.getAbsolutePath());
            Desktop.getDesktop().open(file);
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }//GEN-LAST:event_btnOpenActionPerformed
    
    private void saveToFile(InputStream is) throws IOException
    { 
        is.mark(0);
        FileOutputStream fos = null;
        try
        {
            String type = cbType.getSelectedItem().toString();
            logger.debug(type);
            File file = new File("temp" + type);
            logger.debug(file==null? null: file.getAbsolutePath());
            
            fos  = new FileOutputStream(file);
            int read;
            byte[] bytes = new byte[1024];
            while((read=is.read(bytes))!=-1)
            {
               fos.write(bytes, 0, read);
            }
            logger.debug("saved");
        }catch(IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        }finally{
            is.reset();
            CloseStream.close(fos);
        }
    }
    
    
    
    //HELP
    private void itemHelpContentActionPerformed(ActionEvent evt)
    {
        try
        {
            Desktop.getDesktop().browse(new URI("http://www.highgo.com"));
        } catch (IOException | URISyntaxException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }

    private SpinnerNumberModel spinnerNumModel;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddRow;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDeleteRow;
    private javax.swing.JButton btnFirstPage;
    private javax.swing.JButton btnLastPage;
    private javax.swing.JButton btnNextPage;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnPrePage;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnRefreshCurrentPage;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSaveRow;
    private javax.swing.JButton btnSortFilter;
    private javax.swing.JButton btnUndo;
    private javax.swing.JButton btnUndoRow;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBoxMenuItem cbItemDefaultView;
    private javax.swing.JCheckBoxMenuItem cbItemLimitBar;
    private javax.swing.JCheckBoxMenuItem cbItemToolBar;
    private javax.swing.JCheckBoxMenuItem cbItemValueViewer;
    private javax.swing.JComboBox cbLimit;
    private javax.swing.JComboBox<String> cbType;
    private javax.swing.JComboBox<String> cbbValueViewType;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JInternalFrame ifValueViewer;
    private javax.swing.JMenuItem itemDelete;
    private javax.swing.JMenuItem itemExit;
    private javax.swing.JMenuItem itemFilterBySelection;
    private javax.swing.JMenuItem itemFilterExcludingSelection;
    private javax.swing.JMenuItem itemHelpContent;
    private javax.swing.JMenuItem itemRefresh;
    private javax.swing.JMenuItem itemRemoveFilter;
    private javax.swing.JMenuItem itemRemoveSort;
    private javax.swing.JMenuItem itemSave;
    private javax.swing.JMenuItem itemSortAscending;
    private javax.swing.JMenuItem itemSortDescending;
    private javax.swing.JMenuItem itemSortFilter;
    private javax.swing.JMenuItem itemUndo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar.Separator jSeparator2;
    private javax.swing.JToolBar.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar limitBar;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuHelp;
    private javax.swing.JMenu menuTool;
    private javax.swing.JMenu menuView;
    private javax.swing.JPanel pnlData;
    private javax.swing.JPanel pnlState;
    private javax.swing.JPanel pnlValueImage;
    private javax.swing.JPanel pnlValueViewer;
    private javax.swing.JPopupMenu.Separator sepEdit;
    private javax.swing.JPopupMenu.Separator sepTool;
    private javax.swing.JPopupMenu.Separator sepTool2;
    private javax.swing.JPopupMenu.Separator sepView2;
    private javax.swing.JScrollPane spTable;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JSpinner spnPage;
    private javax.swing.JSpinner spnPaging;
    private javax.swing.JScrollPane spnlValueNormal;
    private javax.swing.JTable tblData;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JToolBar toolbarData;
    private javax.swing.JToolBar toolbarValueViewer;
    private javax.swing.JTextArea ttarValueViewer;
    private javax.swing.JTextField txfdState;
    // End of variables declaration//GEN-END:variables
}
