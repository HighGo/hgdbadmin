/* ------------------------------------------------
 *
 * File: DataSortFilterDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datamanager\view\DataSortFilterDialog.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.datamanager.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.datamanager.controller.DataController;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import java.awt.Desktop;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class DataSortFilterDialog extends JDialog
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private final AbstractObject relation;
    private final boolean onlyFilter;

    private DefaultTableModel model;
    private List<Object[]> data;

    private boolean isSuccess;
    
    /**
     * Creates new form ServerAddView
     * @param parent
     * @param modal
     * @param relation
     * @param onlyFilter
     * @param filterConditon
     */
    public DataSortFilterDialog(JFrame parent, boolean modal,
            AbstractObject relation, boolean onlyFilter, String filterConditon)
    {
        super(parent, modal);
        this.relation = relation;
        this.onlyFilter = onlyFilter;
        initComponents();
        model = (DefaultTableModel) tblColumns.getModel();
        isSuccess = false;
        if (onlyFilter)
        {
            jtp.remove(pnlSort);
        } else
        {
            if (filterConditon != null)
            {
                btnOK.setEnabled(true);
            }
            tpFilterCondition.setText(filterConditon);
            this.addColumnItem();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        jtp = new javax.swing.JTabbedPane();
        pnlFilter = new javax.swing.JPanel();
        spDefinationSQL = new javax.swing.JScrollPane();
        tpFilterCondition = new javax.swing.JTextPane();
        lblExample = new javax.swing.JLabel();
        btnValidate = new javax.swing.JButton();
        pnlSort = new javax.swing.JPanel();
        spDefinationSQL1 = new javax.swing.JScrollPane();
        tblColumns = new javax.swing.JTable();
        lblChoosedColumns = new javax.swing.JLabel();
        btnAsc = new javax.swing.JButton();
        btnDesc = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        cbbColumns = new javax.swing.JComboBox();
        lblAvailableColumns = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("viewDataOption"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/viewfiltereddata.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 420));

pnlFilter.setMinimumSize(new java.awt.Dimension(520, 470));
pnlFilter.setPreferredSize(new java.awt.Dimension(520, 440));

tpFilterCondition.addKeyListener(new java.awt.event.KeyAdapter()
{
    public void keyReleased(java.awt.event.KeyEvent evt)
    {
        tpFilterConditionKeyReleased(evt);
    }
    });
    spDefinationSQL.setViewportView(tpFilterCondition);

    lblExample.setText(constBundle.getString("filterStringExample"));

    btnValidate.setText(constBundle.getString("validate"));
    btnValidate.setEnabled(false);
    btnValidate.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnValidateActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout pnlFilterLayout = new javax.swing.GroupLayout(pnlFilter);
    pnlFilter.setLayout(pnlFilterLayout);
    pnlFilterLayout.setHorizontalGroup(
        pnlFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlFilterLayout.createSequentialGroup()
            .addGroup(pnlFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlFilterLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 507, Short.MAX_VALUE))
                .addGroup(pnlFilterLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblExample, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addContainerGap())
        .addGroup(pnlFilterLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(btnValidate)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnlFilterLayout.setVerticalGroup(
        pnlFilterLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlFilterLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(lblExample, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(btnValidate)
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("filter"), pnlFilter);
    pnlFilter.getAccessibleContext().setAccessibleName("SQL");

    pnlSort.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlSort.setPreferredSize(new java.awt.Dimension(579, 440));

    spDefinationSQL1.setBackground(new java.awt.Color(255, 255, 255));

    tblColumns.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][]
        {

        },
        new String []
        {
            "", ""
        }
    ));
    spDefinationSQL1.setViewportView(tblColumns);
    if (tblColumns.getColumnModel().getColumnCount() > 0)
    {
        tblColumns.getColumnModel().getColumn(0).setHeaderValue(constBundle.getString("columnName")
        );
        tblColumns.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("order"));
    }

    lblChoosedColumns.setText(constBundle.getString("choosedColumns"));

    btnAsc.setText(constBundle.getString("ascend"));
    btnAsc.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnAscActionPerformed(evt);
        }
    });

    btnDesc.setText(constBundle.getString("descend"));
    btnDesc.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnDescActionPerformed(evt);
        }
    });

    btnRemove.setText(constBundle.getString("remove"));
    btnRemove.setEnabled(false);
    btnRemove.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnRemoveActionPerformed(evt);
        }
    });

    lblAvailableColumns.setText(constBundle.getString("availableColumn")
    );

    javax.swing.GroupLayout pnlSortLayout = new javax.swing.GroupLayout(pnlSort);
    pnlSort.setLayout(pnlSortLayout);
    pnlSortLayout.setHorizontalGroup(
        pnlSortLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSortLayout.createSequentialGroup()
            .addGroup(pnlSortLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pnlSortLayout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(spDefinationSQL1))
                .addGroup(pnlSortLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(lblChoosedColumns, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGap(10, 10, 10))
        .addGroup(pnlSortLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSortLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbbColumns, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pnlSortLayout.createSequentialGroup()
                    .addComponent(btnAsc)
                    .addGap(10, 10, 10)
                    .addComponent(btnDesc)
                    .addGap(10, 10, 10)
                    .addComponent(btnRemove)
                    .addGap(0, 0, Short.MAX_VALUE))
                .addGroup(pnlSortLayout.createSequentialGroup()
                    .addComponent(lblAvailableColumns)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 499, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );
    pnlSortLayout.setVerticalGroup(
        pnlSortLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSortLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addComponent(lblChoosedColumns, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(spDefinationSQL1, javax.swing.GroupLayout.DEFAULT_SIZE, 307, Short.MAX_VALUE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(lblAvailableColumns)
            .addGap(10, 10, 10)
            .addComponent(cbbColumns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addGroup(pnlSortLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnAsc)
                .addComponent(btnDesc)
                .addComponent(btnRemove))
            .addContainerGap())
    );

    jtp.addTab(constBundle.getString("sort"), pnlSort);

    getContentPane().add(jtp, java.awt.BorderLayout.CENTER);
    jtp.getAccessibleContext().setAccessibleName("SQL");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));
    btnHelp.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnHelpActionPerformed(evt);
        }
    });

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));
    btnOK.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnOKActionPerformed(evt);
        }
    });

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));
    btnCancle.addActionListener(new java.awt.event.ActionListener()
    {
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {
            btnCancleActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 277, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap())
    );

    getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

    pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addColumnItem()
    {
        if (relation instanceof TableInfoDTO)
        {
            TableInfoDTO table = (TableInfoDTO) relation;
            if (table.isHasOID())
            {
                cbbColumns.addItem("oid");
            }
            for (ColumnInfoDTO columnInfo : table.getColumnInfoList())
            {
                cbbColumns.addItem(columnInfo.getName());
            }
        } else if (relation instanceof ViewInfoDTO)
        {
            ViewInfoDTO view = (ViewInfoDTO) relation;
            String headerStr = (view.getDefinition().split("FROM")[0]).replace("SELECT", "");
            String[] columns = (headerStr.replace(" ", "")).split(",");
            cbbColumns.setModel(new DefaultComboBoxModel(columns));
        }
        tblColumns.getSelectionModel().addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                int row = e.getLastIndex();
                logger.info("row=" + row);
                if (row >= 0)
                {
                    btnRemove.setEnabled(true);
                    if (!onlyFilter)
                    {
                        btnOK.setEnabled(true);
                    }
                } else
                {
                    btnRemove.setEnabled(false);
                }
            }
        });        
    }
    private void btnHelpActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnHelpActionPerformed
    {//GEN-HEADEREND:event_btnHelpActionPerformed
        logger.debug(evt.getActionCommand());
        Desktop desktop = Desktop.getDesktop();
        try
        {
            desktop.browse(new URI("http://www.pgadmin.org/docs/1.20/editgrid.html"));
        } catch (IOException | URISyntaxException ex)
        {
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.WARNING_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }//GEN-LAST:event_btnHelpActionPerformed

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        logger.debug(evt.getActionCommand());
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void tpFilterConditionKeyReleased(java.awt.event.KeyEvent evt)//GEN-FIRST:event_tpFilterConditionKeyReleased
    {//GEN-HEADEREND:event_tpFilterConditionKeyReleased
        String condition = tpFilterCondition.getText();
        if (condition == null || condition.isEmpty())
        {
            btnOK.setEnabled(false);
            btnValidate.setEnabled(false);
        } else
        {
            btnOK.setEnabled(true);
            btnValidate.setEnabled(true);
        }
    }//GEN-LAST:event_tpFilterConditionKeyReleased

    private void btnValidateActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnValidateActionPerformed
    {//GEN-HEADEREND:event_btnValidateActionPerformed
        try
        {
            String condition = tpFilterCondition.getText();
            if (condition.trim().endsWith(";"))
            {
                throw new Exception(constBundle.getString("filterConditionNoSemicolon"));
            }
            String relname = SyntaxController.getInstance().getName(relation.getHelperInfo().getSchema())
                    + "." + SyntaxController.getInstance().getName(relation.getName());
            totalRowCount = DataController.getInstance().getRowCount(relation.getHelperInfo(), relname, condition);
            JOptionPane.showMessageDialog(this, constBundle.getString("filterStringOK"),
                    constBundle.getString("filterStringValidate"), JOptionPane.PLAIN_MESSAGE);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());            
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.WARNING_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }//GEN-LAST:event_btnValidateActionPerformed

    private void btnAscActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAscActionPerformed
    {//GEN-HEADEREND:event_btnAscActionPerformed
        logger.debug(evt.getActionCommand());
        Object[] row = new Object[2];
        row[0] = cbbColumns.getSelectedItem();
        row[1] = "ASC";
        model.addRow(row);
        cbbColumns.removeItem(cbbColumns.getSelectedItem());
        btnOK.setEnabled(true);
    }//GEN-LAST:event_btnAscActionPerformed

    private void btnDescActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnDescActionPerformed
    {//GEN-HEADEREND:event_btnDescActionPerformed
        logger.debug(evt.getActionCommand());
        Object[] row = new Object[2];
        row[0] = cbbColumns.getSelectedItem();
        row[1] = "DESC";
        model.addRow(row);
        cbbColumns.removeItem(cbbColumns.getSelectedItem());
        btnOK.setEnabled(true);
    }//GEN-LAST:event_btnDescActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnRemoveActionPerformed
    {//GEN-HEADEREND:event_btnRemoveActionPerformed
        logger.debug(evt.getActionCommand());
        cbbColumns.addItem(tblColumns.getValueAt(tblColumns.getSelectedRow(), 0));
        model.removeRow(tblColumns.getSelectedRow());
        btnOK.setEnabled(model.getRowCount()>0);
    }//GEN-LAST:event_btnRemoveActionPerformed
   
    private int totalRowCount =0 ;
    private void btnOKActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOKActionPerformed
    {//GEN-HEADEREND:event_btnOKActionPerformed
        logger.info("Filter Condition = " + tpFilterCondition.getText());
        try
        {
            String relname = SyntaxController.getInstance().getName(relation.getHelperInfo().getSchema())
                    + "." + SyntaxController.getInstance().getName(relation.getName());
            totalRowCount = DataController.getInstance().getRowCount(relation.getHelperInfo(), relname, tpFilterCondition.getText());
            this.dispose();
            isSuccess = true;
        } catch (Exception ex)
        {
            isSuccess = false;
            logger.error(ex.getMessage());
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace(System.out);
        }
    }//GEN-LAST:event_btnOKActionPerformed
    public boolean isSuccess()
    {
        return isSuccess;
    }
    public int getTotalRowCount()
    {
        return totalRowCount;
    }
    
    public String getSortCondition()
    {
        int row = model.getRowCount();
        if (row > 0)
        {
            StringBuilder sortOrder = new StringBuilder();
            for (int i = 0; i < row; i++)
            {
                if (i > 0)
                {
                    sortOrder.append(", ");
                }
                sortOrder.append(model.getValueAt(i, 0)).append(" ").append(model.getValueAt(i, 1));
            }
            return sortOrder.toString();
        } else
        {
            return "";
        }
    }
    public String getFilterCondition()
    {
        return tpFilterCondition.getText();
    }

    public void clearSortCondition()
    {
        int row = model.getRowCount();
        for (int i = row - 1; i >= 0; i--)
        {
            cbbColumns.addItem(model.getValueAt(i, 0));//must before the row being remove 
            model.removeRow(i);            
        }
    }
    public void clearFilterCondition()
    {
        tpFilterCondition.setText(null);
    }
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAsc;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnDesc;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnRemove;
    private javax.swing.JButton btnValidate;
    private javax.swing.JComboBox cbbColumns;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblAvailableColumns;
    private javax.swing.JLabel lblChoosedColumns;
    private javax.swing.JLabel lblExample;
    private javax.swing.JPanel pnlFilter;
    private javax.swing.JPanel pnlSort;
    private javax.swing.JScrollPane spDefinationSQL;
    private javax.swing.JScrollPane spDefinationSQL1;
    private javax.swing.JTable tblColumns;
    private javax.swing.JTextPane tpFilterCondition;
    // End of variables declaration//GEN-END:variables
}
