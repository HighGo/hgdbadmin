/* ------------------------------------------------
 *
 * File: StreamValueConvertor.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，2020.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datamanager\view\StreamValueConvertor.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.datamanager.view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author liuyuanyuan
 */
public class StreamValueConvertor {

    private final static Logger logger = LogManager.getLogger(StreamValueConvertor.class);

    /*public static JFrame ShowImageFrame(InputStream ips) throws IOException {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setSize(400, 400);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ips.mark(0);
        ips.transferTo(baos);
        ips.reset;
        InputStream ips4show = new ByteArrayInputStream(baos.toByteArray());
        InputStream ips4type = new ByteArrayInputStream(baos.toByteArray());
        String typeStr = getMimeTypeInfo(ips4type).toLowerCase();
        logger.warn(typeStr);
        if (typeStr.contains("png")
                || typeStr.contains("jpg")) {
            logger.debug("image");
            JPanel pane = generateImagePanel(ips4show);
            frame.add(pane);
            pane.setVisible(true);
        } else if (typeStr.contains("mp4")) {
            logger.debug("vedio");
            //need java fx
            //MediaPlayer mp = (MediaPlayer) Manager.createRealizedPlayer("");
        } else //text
        {
            logger.debug("text");
            JScrollPane spane = generateTextPanel(ips4show);
            frame.add(spane);
            spane.setVisible(true);
        }
        frame.setVisible(true);
        
        //ips.read(baos.toByteArray());
        //logger.debug(ips.available());
        return frame;
    }*/

    public static JPanel generateImagePanel(InputStream ips) throws IOException {
        if (ips == null) {
            return new JPanel();
        }
        BufferedImage image = ImageIO.read(ips);
        JPanel pnl = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(image, 0, 0, null);
            }
        };
        pnl.setVisible(true);
        return pnl;
    }
    @Deprecated //not used now
    public static JScrollPane generateTextPanel(InputStream ips) throws IOException
    {
        //must in this order to show
        JScrollPane scrolPane = new JScrollPane();
        
        JTextArea textArea = new JTextArea();
        textArea.setLineWrap(true);
        textArea.setEditable(false);
        String txt = streamToString(ips);
        textArea.setText(txt);

        scrolPane.setViewportView(textArea);  
        return scrolPane;
    }
    public static String streamToString(InputStream ips) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(ips, writer, StandardCharsets.UTF_8.name());
        String str = writer.toString();
        return str;
        /*
        BufferedInputStream bis = new BufferedInputStream(ips);
        ByteArrayOutputStream buf = new ByteArrayOutputStream();
        int result = bis.read();
        while (result != -1) {
            buf.write((byte) result);
            result = bis.read();
        }
        String str = buf.toString();
        return str;
        */
    }

    /*
    public static String streamToHEX(InputStream ips) throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ips.transferTo(baos);
        byte[] bytes = baos.toByteArray();
        
        final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return String.valueOf(hexChars);
    }*/
   
    
    @Deprecated //not used now
    public static String saveToLocalFile(InputStream ips) throws IOException {
        ips.mark(0);
        byte[] buffer = new byte[ips.available()];
        ips.read(buffer);
        File targetFile = new File("tmp");
        OutputStream outStream = new FileOutputStream(targetFile);
        outStream.write(buffer);
        ips.reset();
        
        return targetFile.getAbsolutePath();
    }
    
    //this method not good enough
    public static String getMimeTypeInfo(InputStream ips) throws IOException
    {
        /*
        http://www.bejson.com/convert/ox2str/
        https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types
        */
        //byte[] bytes = new byte[ips.available()];
        //ips.read(bytes);
        byte[] bytes = IOUtils.toByteArray(ips);//read out
        if(bytes.length<=0)
        {
            return "";
        }
        
        byte[] headerBytes = new byte[bytes.length >= 32 ? 32 : bytes.length];
        System.arraycopy(bytes, 0, headerBytes, 0, headerBytes.length);

        return new String(headerBytes, StandardCharsets.UTF_8);
    }
    
}
