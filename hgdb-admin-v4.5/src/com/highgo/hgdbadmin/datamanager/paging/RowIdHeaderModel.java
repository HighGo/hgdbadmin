/* ------------------------------------------------
 *
 * File: RowIdHeaderModel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\paging\RowIdHeaderModel.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datamanager.paging;

import com.highgo.hgdbadmin.query.model.SmartPagingModel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class RowIdHeaderModel extends DefaultTableModel
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());

    private DefaultTableModel pagingModel;
    public RowIdHeaderModel(DefaultTableModel pagingModel)
    {
        this.pagingModel = pagingModel;
    }
    public void update(DefaultTableModel pagingModel)
    {
        this.pagingModel = pagingModel;
        this.fireTableDataChanged();
    }
    
    @Override
    public Object getValueAt(int row, int col)
    {
        if (row < 0 || col < 0)
        {
            return null;
        } else
        {
            Object value = 0;
            if (pagingModel instanceof SmartPagingModel)//for query tool
            {
                SmartPagingModel pm = (SmartPagingModel) pagingModel;
                value = pm.getPageOffset() * pm.getPageSize() + row + 1;
            } else if (pagingModel instanceof PagingModel)//for data view
            {
                PagingModel pm = (PagingModel) pagingModel;

                value = pm.getPageOffset() * pm.getPageSize() + row + 1;
            } else
            {
                value = "";
                logger.error("this is an exception model type,return empty value");
            }
            return value;
        }
    }    
    
    @Override
    public boolean isCellEditable(int row, int column)
    {
        return false;
    }
    @Override
    public int getColumnCount()
    {
        return 1;
    }
    @Override
    public int getRowCount()
    {
        if (pagingModel == null)
        {
            return 1000;
        } else
        {
            return pagingModel.getRowCount();
        }
    }

    @Override
    public String getColumnName(int column)
    {
        return "ID";
    }
}
