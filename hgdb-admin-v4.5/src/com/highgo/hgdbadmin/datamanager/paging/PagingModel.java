/* ------------------------------------------------
 *
 * File: PagingModel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\paging\PagingModel.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datamanager.paging;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.datamanager.controller.DataController;
import com.highgo.hgdbadmin.model.AbstractObject;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


/**
 * @description this is for DataEditView
 * @author Yuanyuan
 *
 * A larger table model that performs "paging" of its data. This model reports a
 * small number of rows (like 1000 or so) as a "page" of data. You can switch
 * pages to view all of the rows as needed using the pageDown() and pageUp()
 * methods. Presumably, access to the other pages of data is dictated by other
 * GUI elements such as up/down buttons, or maybe a Spinner that allows you
 * to enter the page number you want to display.
 * 
 */
public class PagingModel extends DefaultTableModel
{
    private final Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    protected int pageSize;
    protected int pageOffset;//pageId = pageOffSet+1;
    
    protected Object[] header;
    protected Class[] typeArray;
    protected boolean isEditable;    
    
    private AbstractObject relation;
    private String filterCondition;
    private String sortCondition;
    
    protected List<Object[]> data;
    protected int realTotalRow;

    public PagingModel(AbstractObject relation, String filterCondition, String sortCondition, //HelperInfoDTO helperInfo, String sql,
            int totalRow, int size, boolean isEditable,  Class[] typeArray, Object[] headerArray) 
            throws ClassNotFoundException, SQLException 
    {
        this.relation = relation;
        this.filterCondition = filterCondition;
        this.sortCondition = sortCondition;
        
        this.isEditable = isEditable;        
        this.typeArray = typeArray;
        this.header = headerArray;
        this.realTotalRow = totalRow;
            
        this.pageSize = size;
        this.pageOffset = 0;

        logger.info("totalRow=" + totalRow + ",pageOffset=" + pageOffset + ",pageSize =" + pageSize);
        this.refreshDataOfThePage();
    }
    
    // Return values appropriate for the visible table part.
    @Override
    public int getRowCount()
    {
        if (data == null)
        {
            return Math.min(pageSize, realTotalRow);
        } else
        {
            return data.size();
        }
    }   
    @Override
    public boolean isCellEditable(int row, int column)
    {
        return isEditable;// && !this.getColumnName(column).equals("oid")
    }
    @Override
    public int getColumnCount()
    {
        return header.length;
    }
    @Override
    public String getColumnName(int col)
    {
        if (col < 0)
        {
            return null;
        } else
        {
            return header[col].toString();
        }
    }
    /*public DatatypeDTO getColumnType(int col)
    {
        if (col >= 0 && (relation instanceof TableInfoDTO))
        {
            return ((TableInfoDTO)relation).getColumnInfoList().get(col).getDatatype();
        }
        return null;
    }*/
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        if(columnIndex<0)
        {
            return null;
        }
        else if(typeArray==null)//this is for 
        {
            return Object.class;
        } else
        {
            return typeArray[columnIndex];
        }
    }
    @Override
    public Object getValueAt(int row, int col)
    {
        if (row < 0 || col < 0) {
            return null;
        } else {
            if (Boolean.class == this.getColumnClass(col)) {
                if (data.get(row)[col] == null) {
                    return null;
                } else {
                    return "t".equalsIgnoreCase(data.get(row)[col].toString())
                            || "true".equalsIgnoreCase(data.get(row)[col].toString());
                }
            } else {
                return data.get(row)[col];
            }
        }
    }
    
    /*private Object[] oldRowData = null;
    //data is just old before your call
    public Object[] getOldRowData4BooleanTable(int row)
    {
        if (oldRowData == null) {
            Object[] array = data.get(row);
            oldRowData = new Object[array.length];
            System.arraycopy(array, 0, oldRowData, 0, array.length);
        }
        return oldRowData;
    }*/
    
    @Override
    public void setValueAt(Object aValue, int row, int column)
    {
        //logger.debug("PagingModel set value:" + aValue);
        if (row >= 0 && column >= 0 && row<this.getRowCount())//update
        {
            //record old row for Boolean cell value,because it's setValueAt before MouseLick action
            //Object[] array = data.get(row);
            //oldRowData = new Object[array.length];
            //System.arraycopy(array, 0, oldRowData, 0, array.length);
            data.get(row)[column] = aValue;
        }else if(row >= 0 && column >= 0 && row>=this.getRowCount())//insert
        {
            data.add(row, new Object[this.getColumnCount()]);
            data.get(row)[column] = aValue;
        }
        fireTableCellUpdated(row, column);
    }   
    @Override
    public void addRow(Object[] rowData)
    {
        data.add(rowData);
        realTotalRow++;
        int row = this.getRowCount() + 1;
        this.fireTableRowsInserted(row, row);
    }  
    @Override
    public void removeRow(int row)
    {
        if (row >= 0)
        {
            data.remove(row);
            realTotalRow--;
            this.fireTableRowsDeleted(row, row);
        }
    }
    
    public int getPageSize()
    {
        return pageSize;
    }    
    // Use this method to turn to the specified page
    public void setPageOffSet(int theOffset) throws ClassNotFoundException, SQLException 
    {
        if (theOffset >= 0)
        {
            pageOffset = theOffset;
            this.refreshDataOfThePage();
        }
    }
    // Use this method to figure out which page you are on(pageId = pageOffset+1). It starts from 0.
    public int getPageOffset()
    {
        return pageOffset;
    }
    public int getPageCount()
    {
         return (int) Math.ceil((double)realTotalRow/pageSize);
    }
 
    //get old row data for row update
    public Object[] getRowData(int row)
    {
        if (pageOffset < 0 || row < 0)
        {
            return null;
        } else
        {
            return data.get(row);
        }
    }
    //restore old row data for row update undo
    public void setRowData(int row, Object[] rowData)
    {
        if (row >= 0)
        {
            data.set(row, rowData);
            this.fireTableRowsUpdated(row, row);
        }
    }

    // how many rows the really table has . 
    public int getRealRowCount() throws ClassNotFoundException, SQLException 
    {
        this.refreshRealTotalRow();
        return realTotalRow;
    }
    //maybe removed,this is only used when filter condition is changed.
    public void setRealRowCount(int newTotalRow)
    {
        logger.info("newTotalRow=" + newTotalRow);
        if (newTotalRow >= 0)
        {
            this.realTotalRow = newTotalRow;
            this.fireTableDataChanged();
        }
    }

    private void refreshDataOfThePage() throws ClassNotFoundException, SQLException 
    {
        data = DataController.getInstance().getData(relation, filterCondition, sortCondition,
                isEditable, pageOffset, pageSize);
        this.fireTableDataChanged();
    }
    private void refreshRealTotalRow() throws ClassNotFoundException, SQLException 
    {
        String relname = SyntaxController.getInstance().getName(relation.getHelperInfo().getSchema()) 
                + "." + SyntaxController.getInstance().getName(relation.getName());
        realTotalRow = DataController.getInstance().getRowCount(relation.getHelperInfo(), relname, filterCondition);
    }
    public void updateQueryCondition(String filterCondition, String sortCondition) throws Exception
    {
        //this.sql = sql;
        this.filterCondition = filterCondition;
        this.sortCondition = sortCondition;
        //pageOffset = 0;
        this.refreshDataOfThePage();
        //this.refreshRealTotalRow();
    }
       
    
    // We provide our own version of a scrollpane that includes  
    // the page up and page down buttons by default.  
    public static JScrollPane createPagingScrollPaneForTable
        (JTable tbl, RowIdHeaderModel rowIdModel)
    {
        JScrollPane jsp = new JScrollPane(tbl);
        TableModel tmodel = tbl.getModel();
        // Don't choke if this is called on a regular table . 
        if (!(tmodel instanceof PagingModel))
        {
            return jsp;
        }
        
        // build the real scrollpane
        /*
        //romove page up and down button
        final PagingModel pagingModel = (PagingModel) tmodel;
        final JButton upButton = new JButton(new PageTurnIcon(PageTurnIcon.UP));
        upButton.setEnabled(false); // starts off at 0, so can't go up  
        final JButton downButton = new JButton(new PageTurnIcon(PageTurnIcon.DOWN));
        if (pagingModel.getPageCount() <= 1)
        {
            downButton.setEnabled(false); // One page...can't scroll down  
        }
        upButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {               
                pagingModel.pageUp();
                JButton upButton = (JButton) ae.getSource();
                // If we hit the top of the data, disable the up button.  
                if (pagingModel.getPageOffset() == 0)
                {
                    upButton.setEnabled(false);
                }
                downButton.setEnabled(true);
            }
        });
        downButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
//                boolean isDownable=  pagingModel.pageDown();
//                JButton downButton = (JButton) ae.getSource();
//                downButton.setEnabled(isDownable);
                
//                // If we hit the bottom of the data, disable the down button.  
//                System.out.println("pageOffSet="+pagingModel.getPageOffset()+",pageCount="+pagingModel.getPageCount());
//                if (pagingModel.getPageOffset() == (pagingModel.getPageCount() - 1))
//                {
//                    downButton.setEnabled(false);
//                }
                upButton.setEnabled(true);
            }
        });
        */

        //Turn on the scrollbars; otherwise we won't get our corners.  
        jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jsp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        
        //Add in the corners (page up/down).  
        //jsp.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, upButton);
        //jsp.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, downButton);

        JTable rowIdTable = new JTable(rowIdModel);
        rowIdTable.setFont(new Font("SansSerif", Font.BOLD, 12));
        rowIdTable.setBackground(new Color(240, 240, 240));
        rowIdTable.setEnabled(false);//disable selection
        rowIdTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //disable reorder
        rowIdTable.getTableHeader().setReorderingAllowed(false);
        //headerTable.setIntercellSpacing(new Dimension(0, 0));
        //Dimension d = headerTable.getPreferredScrollableViewportSize();
        //d.width = headerTable.getPreferredSize().width;
        //headerTable.setPreferredScrollableViewportSize(d);
        rowIdTable.setRowHeight(tbl.getRowHeight());

        int columnWidth = 30;
        rowIdTable.getColumnModel().getColumn(0).setPreferredWidth(columnWidth);
        rowIdTable.getColumnModel().getColumn(0).setMaxWidth(100);
        rowIdTable.setPreferredScrollableViewportSize(new Dimension(columnWidth, 0));

        jsp.setCorner(JScrollPane.UPPER_LEFT_CORNER, rowIdTable.getTableHeader());
        jsp.setRowHeaderView(rowIdTable);
        rowIdTable.addComponentListener(new RowHeaderResizeListener(jsp, jsp.getRowHeader(), rowIdTable, tbl));
        
        return jsp;
    }
    
    /*
    public boolean pageDown()
    {
        pageOffset++;
        DataController c = DataController.getInstance();
        List<Object[]> newData = new ArrayList();
        try
        {
            //newData = c.getData(helperInfo, sql, isEditable, pageOffset, pageSize);
            data = c.getData(relation, filterCondition, sortCondition, 
                    isEditable, pageOffset, pageSize);
            //realTotalRow = c.getRowCount(relation, filterCondition);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }
        
        logger.info("dataSize=" + newData.size());
        if (newData.size() > 0)
        {
            data = newData;
            this.fireTableDataChanged();
            logger.info("pageOffset=" + pageOffset);
            return newData.size() == pageSize;//only this page is full, we can down to next page
        } else
        {
            pageOffset--;
            logger.info("pageOffset=" + pageOffset);
            return false;
        }
    }
    // Update the page offset and fire a data changed (all rows).  
    public void pageUp()
    {
        if (pageOffset > 0)
        {
            pageOffset--;
            logger.info("pageOffset=" + pageOffset + ",pageSize =" + pageSize);
            this.refreshDataOfThePage();
        }
    }
    */
}
