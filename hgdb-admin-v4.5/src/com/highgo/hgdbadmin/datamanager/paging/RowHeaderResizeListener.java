/* ------------------------------------------------
 *
 * File: RowHeaderResizeListener.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\paging\RowHeaderResizeListener.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.datamanager.paging;

/**
 *
 * @author Yuanyuan
 */
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Listen for resizes of the row header table's header, then resize the
 * containing viewport. These resize events happen when the user drags the row
 * header's column header to resize the row header columns.
 *
 * @author Alex Kluge
 */
public class RowHeaderResizeListener extends ComponentAdapter {

    private final Logger logger = LogManager.getLogger(getClass());
    private JScrollPane identityTable;
    private JTable mainTable;
    private JTable rowHeaderTable;
    private JViewport rowHeaderView;

    /**
     * <p>
     * Create a RowHeaderResizeListener, which listens for resize events on the
     * row header part of an IdentityTable. These happen when the user drags to
     * resize the row header column.
     * </p>
     *
     * @param identityTable A JScrollPane, which has been configured as an
     * IdentityTable, with a rowheader table, and a main table.
     *
     * @param rowHeaderView A JViewport containing the rowheader. The viewport
     * is the component through only a section of the rowheader is visible.
     *
     * @param rowHeaderTable A JTable containing the row header columns.
     *
     * @param mainTable A JTable containing the main data columns. This table
     * must be resized whenever the rowHeaderTable resizes.
     *
     * @see com.vizitsolutions.identitytable.IdentityTable
     */
    public RowHeaderResizeListener(JScrollPane identityTable, JViewport rowHeaderView,
            JTable rowHeaderTable, JTable mainTable) {
        this.identityTable = identityTable;
        this.rowHeaderView = rowHeaderView;
        this.rowHeaderTable = rowHeaderTable;
        this.mainTable = mainTable;
    }

    /**
     * Called when the rowHeaderTable header is resized. The rowHeaderView is
     * resized to the appropriate size for the rowHeaderTable, then layout the
     * main table and finally, call revalidate to trigger a layout of the
     * containing IdentityTable (JScrollPane).
     *
     * @param e A ComponentEvent, which is ignored because we are only listening
     * on the rowheader.
     */
    @Override
    public void componentResized(ComponentEvent e) {
        //logger.debug("Enter:" + e.getSource());
        Dimension size = rowHeaderView.getPreferredSize();
        size.width = rowHeaderTable.getWidth();
        logger.debug("width="+  size.width);
        rowHeaderView.setPreferredSize(size);
        mainTable.doLayout();
        identityTable.revalidate();
    }
    
    

}
