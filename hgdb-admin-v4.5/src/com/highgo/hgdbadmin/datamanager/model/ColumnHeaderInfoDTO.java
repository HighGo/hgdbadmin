/* ------------------------------------------------
 *
 * File: ColumnHeaderInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\model\ColumnHeaderInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datamanager.model;

/**
 * 
 * @author Yuanyuan
 */
public class ColumnHeaderInfoDTO 
{
//    private boolean isOid;
    private int position;
    private String name;        
    private String dataType;
    private String fullDatatype;
    private boolean pk = false;
    private boolean notNull;
    private String defaultValue;
    private String sequence;//?

    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public String getFullDatatype()
    {
        return fullDatatype;
    }

    public void setFullDatatype(String fullDatatype)
    {
        this.fullDatatype = fullDatatype;
    }

    public boolean isPk()
    {
        return pk;
    }

    public void setPk(boolean pk)
    {
        this.pk = pk;
    }

    public boolean isNotNull()
    {
        return notNull;
    }

    public void setNotNull(boolean notNull)
    {
        this.notNull = notNull;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getSequence()
    {
        return sequence;
    }

    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }
    
    @Override
    public String toString()
    {
        String constraint = "";
        if (this.pk)
        {
            constraint = "[PK]";
        }
        String tmptype = this.fullDatatype.replace("oracle.blob", "blob").replace("oracle.clob", "clob");
        return "<html>" + this.name + "<br>" + constraint + " " + tmptype + "</html>";
    }
}
