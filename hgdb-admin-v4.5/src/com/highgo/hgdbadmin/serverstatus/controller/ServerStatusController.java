/* ------------------------------------------------ 
* 
* File: ServerStatusController.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverstatus\controller\ServerStatusController.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverstatus.controller;

import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.serverstatus.model.Feature;
import com.highgo.hgdbadmin.serverstatus.model.LogFileDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.util.TreeEnum;
//ygq v5 update start
//import com.highgo.jdbc.HGConnection;
import com.highgo.jdbc.PGConnection;
//ygq v5 update end
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class ServerStatusController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(this.getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private static ServerStatusController ssc;

    public static ServerStatusController getInstance()
    {
        if (ssc == null)
        {
            ssc = new ServerStatusController();
        }
        return ssc;
    }

    public List<String> getDBList(HelperInfoDTO helperInfo)
    {
        logger.info("url=" + helperInfo.getPartURL() + helperInfo.getMaintainDB());
        List<String> dbArray = new ArrayList();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT c.oid, c.datname ")
                .append(" FROM pg_database c")
                .append(" WHERE  c.datallowconn ORDER BY c.datname");
        logger.info(sql.toString());
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getMaintainDB(),
                    helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                dbArray.add(rs.getString(2));
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return dbArray;
    }
    public List<String> getProcessColumns(HelperInfoDTO helperInfo)
    {
        List<String> headers = new ArrayList();
        TreeController tc = TreeController.getInstance();
        TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        headers.add("pid");
        if (tc.isVersionHigherThan(dbsys, version, 8, 4))
        {
            headers.add("application_name");
        }
        headers.add("database");
        headers.add("user");
        if (tc.isVersionHigherThan(dbsys, version, 8, 0))
        {
            headers.add("client");
            headers.add("backend_start");
        }
        if (tc.isVersionHigherThan(dbsys, version, 7, 3))
        {
            headers.add("query_start");
        }
        if (tc.isVersionHigherThan(dbsys, version, 8, 2))
        {
            headers.add("xact_start");
        }
        if (tc.isVersionHigherThan(dbsys, version, 9, 1))
        {
            headers.add("state");
            headers.add("state_change");
        }
        if (tc.isVersionHigherThan(dbsys, version, 9, 3))
        {
            headers.add("backend_xid");
            headers.add("backend_xmin");
        }
        headers.add("blockedby");
        headers.add("query");
        //headers.add("slowquery");
        return headers;
    }
    public String[] getProcessHeaders(List<String> columnList)
    {
        int size = columnList.size();
        String[] headers = new String[size];
        for (int i = 0; i < size; i++)
        {
            switch (columnList.get(i))
            {
                case "pid":
                    headers[i] = constBundle.getString("pid");
                    break;
                case "application_name":
                    headers[i] = constBundle.getString("appName");
                    break;
                case "database":
                    headers[i] = constBundle.getString("database");
                    break;
                case "user":
                    headers[i] = constBundle.getString("user");
                    break;
                case "client":
                    headers[i] = constBundle.getString("client");
                    break;
                case "backend_start":
                    headers[i] = constBundle.getString("clientStart");
                    break;
                case "query_start":
                    headers[i] = constBundle.getString("queryStart");
                    break;
                case "xact_start":
                    headers[i] = constBundle.getString("xactStart");
                    break;
                case "state":
                    headers[i] = constBundle.getString("status");
                    break;
                case "state_change":
                    headers[i] = constBundle.getString("statusChange");
                    break;
                case "backend_xid":
                    headers[i] = constBundle.getString("backendXid");
                    break;
                case "backend_xmin":
                    headers[i] = constBundle.getString("backendXMin");
                    break;
                case "blockedby":
                    headers[i] = constBundle.getString("blockedBy");
                    break;
                case "query":
                    headers[i] = constBundle.getString("query1");
                    break;
//                case "slowquery":
//                    headers[i] = "slow query";
//                    break;
                default:
                    headers[i] = "";
                    logger.error(columnList.get(i) + " is an exception column, assign a null String and break.");
                    break;
            }
        }
        return headers;
    }
    public String[] getLockHeaders(HelperInfoDTO helperInfo)
    {
        TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        logger.info("dbVersion=" + dbsys + version);
        TreeController tc = TreeController.getInstance();
        String[] headers;
        if (tc.isVersionHigherThan(dbsys, version, 8, 2))
        {
            headers = new String[]
            {
                "pid",
                constBundle.getString("database"),//dbname
                constBundle.getString("relation"),//"class",
                constBundle.getString("user"),
                constBundle.getString("virtualTransaction"),//"virtualxid",
                constBundle.getString("virtualTransactionId"),//"transaction",
                constBundle.getString("mode"),
                constBundle.getString("granted"),
                constBundle.getString("queryStartTime"),//"query_start",
                constBundle.getString("query1")
            };

        } else if (tc.isVersionHigherThan(dbsys, version, 7, 3))
        {
            headers = new String[]
            {
                "pid",
                constBundle.getString("database"),//"dbname",
                constBundle.getString("relation"),//"class",
                constBundle.getString("user"),
                constBundle.getString("virtualTransactionId"),//"transaction",
                constBundle.getString("mode"),
                constBundle.getString("granted"),
                constBundle.getString("queryStartTime"),//"query_start",
                constBundle.getString("query1")//query
            };
        } else
        {
            headers = new String[]
            {
                "pid",
                constBundle.getString("database"),//"dbname",
                constBundle.getString("relation"),//"class",
                constBundle.getString("user"),
                constBundle.getString("virtualTransactionId"),//"transaction",
                constBundle.getString("mode"),
                constBundle.getString("granted"),
                constBundle.getString("query1")//query
            };
        }
        return headers;
    } 
    
    
    public void getProcessList(HelperInfoDTO helperInfo, List<String> processColumn, DefaultTableModel processModel)throws SQLException, ClassNotFoundException
    {
        //List<Object[]> list = new ArrayList();
        //List<ProcessInfoDTO> processList = new ArrayList();
        if (helperInfo == null)
        {
            logger.error("helperInfo is null , do nothing and return an empty list.");
            return; //list;//processList;
        }
        StringBuilder sql = new StringBuilder();
        TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        logger.info("dbVersion=" + dbsys + version);
        TreeController tc = TreeController.getInstance();
        String pidCol = "p.procpid";
        String queryCol = "current_query";
        if(tc.isVersionHigherThan(dbsys, version, 9, 1))
        {
             pidCol = "p.pid";
             queryCol = "query";
        }
        //pid
        sql.append("SELECT ").append(pidCol).append(" AS pid");
        //application name (when available)
        if(tc.isVersionHigherThan(dbsys, version, 8, 4))
        {
           sql.append(", application_name");
        }
        //database, user name
        sql.append(", datname AS database , usename AS user");
        //client connection method
        if(tc.isVersionHigherThan(dbsys, version, 8, 0))
        {
            sql.append(", CASE WHEN client_port=-1 THEN 'local pipe' ");
            if(tc.isVersionHigherThan(dbsys, version, 9, 0))
            {
                sql.append(" WHEN length(client_hostname)>0 THEN client_hostname||':'||client_port ");
            }
            sql.append(" ELSE textin(inet_out(client_addr))||':'||client_port END AS client ");
        }
        //backend start timestamp
        if(tc.isVersionHigherThan(dbsys, version,8,0))
        {
            sql.append(", date_trunc('second', backend_start) AS backend_start");
        }
        //Query start timestamp (when available)
        if(tc.isVersionHigherThan(dbsys, version, 9, 1))
        {
            sql.append(", CASE WHEN state='active' THEN date_trunc('second', query_start)::text ELSE '' END ");
        }else if(tc.isVersionHigherThan(dbsys, version, 7, 3))
        {
            sql.append(", CASE WHEN ").append(queryCol).append("='' OR ")
                    .append(queryCol).append("='<IDLE>' THEN '' ")
                    .append(" ELSE date_trunc('second', query_start)::text END ");
        }else
        {
            sql.append("'' ");
        }
        sql.append(" AS query_start");
        //Transaction start timestamp
        if(tc.isVersionHigherThan(dbsys, version, 8, 2))
        {
            sql.append(", date_trunc('second', xact_start) AS xact_start");
        }
        //State
         if(tc.isVersionHigherThan(dbsys, version, 9, 1))
        {
            sql.append(", state, date_trunc('second', state_change) AS state_change");
        }
        //Xmin and XID
         if(tc.isVersionHigherThan(dbsys, version, 9, 3))
         {
             sql.append(", backend_xid::text, backend_xmin::text");
        }
        //Blocked by...
        sql.append(", (SELECT min(l1.pid) FROM pg_locks l1 WHERE GRANTED AND (")
                .append("relation IN (SELECT relation FROM pg_locks l2 WHERE l2.pid=").append(pidCol)
                .append(" AND NOT granted)")
                .append(" OR ")
                .append("transactionid IN (SELECT transactionid FROM pg_locks l3 WHERE l3.pid=").append(pidCol)
                .append(" AND NOT granted)")
                .append(")) AS blockedby");         
        //Query
	sql.append(", ").append(queryCol).append(" AS query");
        //Slow query?
        if (tc.isVersionHigherThan(dbsys, version, 9, 1))
        {
            sql.append(", CASE WHEN query_start IS NULL OR state<>'active' THEN false ELSE query_start < now() - '10 seconds'::interval END ");
        } else if (tc.isVersionHigherThan(dbsys, version, 7, 3))
        {
            sql.append(", CASE WHEN query_start IS NULL OR ").append(queryCol)
                    .append(" LIKE '<IDLE>%' THEN false ELSE query_start < now() - '10 seconds'::interval END ");
        } else
        {
            sql.append(", false ");
        }
        sql.append(" AS slowquery");
        // And the rest of the query...
        sql.append(" FROM pg_stat_activity p ");
        if (tc.isVersionHigherThan(dbsys, version, 8, 3))
        {
            sql.append(" WHERE ").append(pidCol).append(" != pg_backend_pid() ");
        }
        sql.append(" ORDER BY pid"); // NumToStr((long)statusSortColumn) + wxT(" ") + statusSortOrder;
        logger.info(sql.toString());
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            //ygq v5 update start
            //HGConnection hgconn = (HGConnection) conn;
            PGConnection hgconn = (PGConnection) conn;
            //ygq v5 update end
            logger.info("Current BackendPID=" + hgconn.getBackendPID());
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
//            ProcessInfoDTO process;
            Object[] row;
            int size = processColumn.size();
            while (rs.next())
            {
                row = new Object[size];
                for (int i = 0; i < size; i++)
                {
                    row[i] = rs.getObject(processColumn.get(i).toString());
                }
                processModel.addRow(row);
//                process = new ProcessInfoDTO();
//                process.setPid(rs.getInt("pid"));
//                if (tc.isVersionHigherThan(dbsys, version, 8, 4))
//                {
//                    process.setAppName(rs.getString("application_name"));
//                }
//                process.setDbName(rs.getString("datname"));
//                process.setUserName(rs.getString("usename"));
//                if (tc.isVersionHigherThan(dbsys, version, 8, 0))
//                {
//                    process.setClient(rs.getString("client"));
//                    process.setBackendStart(rs.getTimestamp("backend_start"));
//                }
//                if (tc.isVersionHigherThan(dbsys, version, 7, 3))
//                {
//                    process.setQueryStart(rs.getString("query_start"));
//                }
//                if (tc.isVersionHigherThan(dbsys, version, 8, 2))
//                {
//                    process.setXactStart(rs.getTimestamp("xact_start"));
//                }
//                if (tc.isVersionHigherThan(dbsys, version, 9, 1))
//                {
//                    process.setState(rs.getString("state"));
//                    process.setStateChange(rs.getTimestamp("state_change"));
//                }
//                if (tc.isVersionHigherThan(dbsys, version, 9, 3))
//                {
//                    process.setBackendXid(rs.getString("backend_xid"));
//                    process.setBackendXmin(rs.getString("backend_xmin"));
//                }
//                process.setBlockBy(rs.getInt("blockedby"));
//                process.setCurrentQuery(rs.getString("query"));
//
//                process.setSlowQuery(rs.getBoolean("slowquery"));
//                processList.add(process);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        //return list;//processList;
    }
    public void getLockList(HelperInfoDTO helperInfo, String[] lockHeaders, DefaultTableModel lockModel)throws Exception
    {        
        TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        logger.info("dbVersion=" + dbsys + version);
        TreeController tc = TreeController.getInstance();
        StringBuilder sql = new StringBuilder();
        if (tc.isVersionHigherThan(dbsys, version, 8, 2))
        {
            sql.append("SELECT pg_stat_get_backend_pid(svrid) AS pid, ")
                    .append(" (SELECT datname FROM pg_database WHERE oid = pgl.database) AS dbname, ")
                    .append(" coalesce(pgc.relname, pgl.relation::text) AS class, ")
                    .append(" pg_get_userbyid(pg_stat_get_backend_userid(svrid)) as user, ")
                    .append(" pgl.virtualxid::text, pgl.virtualtransaction::text AS transaction, pgl.mode, pgl.granted, ")
                    .append(" date_trunc('second', pg_stat_get_backend_activity_start(svrid)) AS query_start, ")
                    .append(" pg_stat_get_backend_activity(svrid) AS query ")
                    .append(" FROM pg_stat_get_backend_idset() svrid, pg_locks pgl ")
                    .append(" LEFT JOIN pg_class pgc ON pgl.relation=pgc.oid ")
                    .append(" WHERE pgl.pid = pg_stat_get_backend_pid(svrid) ");
            if (tc.isVersionHigherThan(dbsys, version, 8, 3))
            {
                sql.append(" AND pid != pg_backend_pid() ");
            }
            sql.append(" ORDER BY pid "); //NumToStr((long)lockSortColumn) + wxT(" ") + lockSortOrder;    
        } else if (tc.isVersionHigherThan(dbsys, version, 7, 3))
        {
            sql.append("SELECT pg_stat_get_backend_pid(svrid) AS pid, ")
                    .append(" (SELECT datname FROM pg_database WHERE oid = pgl.database) AS dbname, ")
                    .append(" coalesce(pgc.relname, pgl.relation::text) AS class, ")
                    .append(" pg_get_userbyid(pg_stat_get_backend_userid(svrid)) as user, ")
                    .append(" pgl.transaction, pgl.mode, pgl.granted, ")
                    .append(" date_trunc('second', pg_stat_get_backend_activity_start(svrid)) AS query_start, ")
                    .append(" pg_stat_get_backend_activity(svrid) AS query ")
                    .append(" FROM pg_stat_get_backend_idset() svrid, pg_locks pgl ")
                    .append(" LEFT JOIN pg_class pgc ON pgl.relation=pgc.oid ")
                    .append(" WHERE pgl.pid = pg_stat_get_backend_pid(svrid) ");
            sql.append("ORDER BY pid ");// NumToStr((long)lockSortColumn) + wxT(" ") + lockSortOrder;         
        } else
        {
            sql.append("SELECT pg_stat_get_backend_pid(svrid) AS pid, ")
                    .append("(SELECT datname FROM pg_database WHERE oid = pgl.database) AS dbname, ")
                    .append("coalesce(pgc.relname, pgl.relation::text) AS class, ")
                    .append("pg_get_userbyid(pg_stat_get_backend_userid(svrid)) as user, ")
                    .append("pgl.transaction, pgl.mode, pgl.granted, ")
                    .append("pg_stat_get_backend_activity(svrid) AS query ")
                    .append("FROM pg_stat_get_backend_idset() svrid, pg_locks pgl ")
                    .append("LEFT JOIN pg_class pgc ON pgl.relation=pgc.oid ")
                    .append("WHERE pgl.pid = pg_stat_get_backend_pid(svrid) ");
            sql.append("ORDER BY pid ");// NumToStr((long)lockSortColumn) + wxT(" ") + lockSortOrder;
        }
        logger.info(sql.toString());
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            Object[] row;
            int size = lockHeaders.length;
            while (rs.next())
            {
                row = new Object[size];
                for (int i = 0; i < size; i++)
                {
                    row[i] = rs.getObject(i+1);//(lockHeaders[i]);
                }
                lockModel.addRow(row);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }        
    } 
    public void getPreparedTransaction(HelperInfoDTO helperInfo, DefaultTableModel tsactModel)
            throws ClassNotFoundException, SQLException
    {
        TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        logger.info("dbVersion=" + dbsys + version);
        TreeController tc = TreeController.getInstance();
        StringBuilder sql = new StringBuilder();
        if (tc.isVersionHigherThan(dbsys, version, 8, 2))
        {
            sql.append("SELECT transaction::text, gid, prepared, owner, database ")
                    .append("FROM pg_prepared_xacts ");
            //("ORDER BY ") + NumToStr((long)xactSortColumn) + wxT(" ") + xactSortOrder;
        } else
        {
            sql.append("SELECT transaction, gid, prepared, owner, database ")
                    .append("FROM pg_prepared_xacts ");
            //wxT("ORDER BY ") + NumToStr((long)xactSortColumn) + wxT(" ") + xactSortOrder;
        }
        logger.info(sql.toString());
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();       
            rs = stmt.executeQuery(sql.toString());
            Object[] row;
            while (rs.next())
            {
                row = new Object[5];
                row[0] = rs.getObject("transaction");
                row[1] = rs.getObject("gid");
                row[2] = rs.getObject("prepared");
                row[3] = rs.getObject("owner");
                row[4] = rs.getObject("database");
                tsactModel.addRow(row);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }        
    }    
    
    public boolean canViewLog(HelperInfoDTO helperInfo) throws ClassNotFoundException, SQLException
    {
        boolean formateOk = false;
        boolean canReadFile = false;
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getMaintainDB(),
                    helperInfo);
            stmt = conn.createStatement();

            String logformatSQL = "select  current_setting('logging_collector') , current_setting('log_directory'),"
                    + "current_setting('log_line_prefix'), current_setting('log_filename')";
            logger.info(logformatSQL);
            rs = stmt.executeQuery(logformatSQL);
            while (rs.next())
            {
                logger.info("logging_collector=" + rs.getString(1)
                        + ",log_directory=" + rs.getString(2)
                        + ",log_line_prefix=" + rs.getString(3)
                        + ",log_filename=" + rs.getString(4));
                formateOk = rs.getString(4).equals("postgresql-%Y-%m-%d_%H%M%S.log");
            }
            
            if (this.hasFeature(Feature.readfile, stmt))
            {
                canReadFile = true;
            } else
            {
                canReadFile = false;
                try
                {
                    stmt.execute("CREATE EXTENSION adminpack;");
                    canReadFile = true;
                } catch (SQLException ex)
                {
                    canReadFile = false;
                    logger.error(ex.getMessage());
                    ex.printStackTrace(System.out);
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        logger.info("Return: formateOk=" + formateOk + ", canReadFile=" + canReadFile);
        return formateOk && canReadFile;
    }
    public List<LogFileDTO> getLogFileList(HelperInfoDTO helperInfo)throws SQLException, ClassNotFoundException
    {
        List<LogFileDTO> fileArray = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getMaintainDB(),
                    helperInfo);
            stmt = conn.createStatement();
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT filetime, filename, pg_file_length(filename) AS len ")
                    .append(" FROM pg_logdir_ls() AS A(filetime timestamp, filename text)")
                    .append(" order by filetime desc");
            logger.info(sql.toString());
            rs = stmt.executeQuery(sql.toString());
            LogFileDTO log;
            while (rs.next())
            {
                log = new LogFileDTO();
                log.setTime(rs.getTimestamp(1));
                log.setName(rs.getString(2));
                log.setLenght(rs.getLong(3));
                fileArray.add(log);
            }
        } catch (SQLException ex)
        {  
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return fileArray;
    }
    public void getLogContent(HelperInfoDTO helperInfo, LogFileDTO log, DefaultTableModel logModel) //throws ClassNotFoundException, SQLException
    {        
        StringBuilder sql = new StringBuilder();        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();            
            //sql.append(" select * from  pg_read_file('./").append(log.getName()).append("')");
            sql.append("select * from pg_read_file('").append(log.getName()).append("')");//default jdbc client encoding is UTF-8
            logger.info(sql.toString());
            rs = stmt.executeQuery(sql.toString());       
            String sep = System.getProperty("line.separator", "\n");
            while (rs.next())
            {
                String[] lines = rs.getString(1).split(sep);
                String[] row;
                for (String line : lines)
                {
                    line = line.trim();
                    if (! line.isEmpty())
                    {
                        row = new String[1];
                        row[0] = line;
                        logModel.addRow(row);
                    }
                }
            }
        } catch (Exception ex)
        {
            logModel.addRow(new String[]{ex.getMessage()});
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            //throw new SQLException(ex.getMessage());
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
    }

    
    public void rotateLogFile(HelperInfoDTO helperInfo) throws Exception
    {
        String sql = "SELECT pg_logfile_rotate()";
        logger.info(sql);
        this.executeQuery(helperInfo, sql);
    }
    public void cancleBackend(HelperInfoDTO helperInfo, String pid) throws Exception
    {
        String sql = "SELECT pg_cancel_backend(" + pid + ")";
        logger.info(sql);
        this.executeQuery(helperInfo, sql);
    }
    public void terminateBackend(HelperInfoDTO helperInfo, String pid) throws Exception
    {
        String sql = "SELECT pg_terminate_backend(" + pid + ")";
        logger.info(sql);
        this.executeQuery(helperInfo, sql);
    }
    public void commitPreparedTransaction(HelperInfoDTO helperInfo, String gid) throws Exception
    {
        String sql = "COMMIT PREPARED '" + gid + "'";
        logger.info(sql);
        this.executeQuery(helperInfo, sql);
    }
    public void rollbackTransaction(HelperInfoDTO helperInfo, String gid) throws Exception
    {
        String sql = "ROLLBACK PREPARED '" + gid + "'";
        logger.info(sql);
        this.executeQuery(helperInfo, sql);
    }    
    private void executeQuery(HelperInfoDTO helperInfo, String sql) throws Exception
    {
        Connection conn = null;
        Statement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();
            stmt.execute(sql);
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
    }
    
    
    
        
    private boolean hasFeature(Feature feature, Statement stmt) throws SQLException
    {
        logger.info("Enter:" + feature);
        boolean has = false;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT proname, pronargs, proargtypes[0] AS arg0, proargtypes[1] AS arg1, proargtypes[2] AS arg2\n")
                .append("  FROM pg_proc\n")
                .append("  JOIN pg_namespace n ON n.oid=pronamespace\n")
                .append(" WHERE proname IN ('pg_tablespace_size', 'pg_file_read', 'pg_logfile_rotate',")
                .append(" 'pg_postmaster_starttime', 'pg_terminate_backend', 'pg_reload_conf',")
                .append(" 'pgstattuple', 'pgstatindex')\n")
                .append("   AND nspname IN ('pg_catalog', 'public')");
        ResultSet rs = null;
        try
        {
            logger.info(sql.toString());
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                String proname = rs.getString("proname");
                long pronargs = rs.getLong("pronargs");
                switch (feature)
                {
                    case readfile:
                        if (proname.equals("pg_file_read") && pronargs == 3 && rs.getLong("arg0") == 25
                                && rs.getLong("arg1") == 20 && rs.getLong("arg2") == 20)
                        {
                            logger.info("Return: true");
                            return true;
                        }
                        break;
                    case logfile_rotate:
                        if (proname.equals("pg_logfile_rotate") && pronargs == 0)
                        {
                            logger.info("Return: true");
                            return true;
                        }
                        break;
                }
//                if (proname.equals("pg_tablespace_size") && pronargs == 1 && rs.getLong("arg0") == 26)
//                {
//                    has = true;
//                } else if (proname.equals("pg_postmaster_starttime") && pronargs == 0)
//                {
//                    has = true;
//                } else if (proname.equals("pg_terminate_backend") && pronargs == 1 && rs.getLong("arg0") == 23)
//                {
//                    has = true;
//                } else if (proname.equals("pg_reload_conf") && pronargs == 0)
//                {
//                    has = true;
//                } else if (proname.equals("pgstattuple") && pronargs == 1 && rs.getLong("arg0") == 25)
//                {
//                    has = true;
//                } else if (proname.equals("pgstatindex") && pronargs == 1 && rs.getLong("arg0") == 25)
//                {
//                    has = true;
//                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex);
            ex.printStackTrace(System.out);
            throw new  SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
        }
        logger.info("Return:"+has);
        return has;
    }
    
}
