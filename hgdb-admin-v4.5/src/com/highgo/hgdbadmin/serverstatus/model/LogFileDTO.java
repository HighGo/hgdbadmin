/* ------------------------------------------------ 
* 
* File: LogFileDTO.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverstatus\model\LogFileDTO.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverstatus.model;

import java.sql.Timestamp;

/**
 *
 * @author Yuanyuan
 */
public class LogFileDTO
{
    private Timestamp time;
    private String name;
    private long lenght;

    public Timestamp getTime()
    {
        return time;
    }

    public void setTime(Timestamp time)
    {
        this.time = time;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getLenght()
    {
        return lenght;
    }

    public void setLenght(long lenght)
    {
        this.lenght = lenght;
    }
    
    @Override
    public String toString()
    {
        return this.time.toString();
    }
    
}
