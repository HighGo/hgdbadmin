/* ------------------------------------------------ 
* 
* File: ProcessInfoDTO.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverstatus\model\ProcessInfoDTO.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverstatus.model;

import java.sql.Timestamp;

/**
 *
 * @author Yuanyuan
 */
public class ProcessInfoDTO
{
    private int pid;
    private String appName;
    private String dbName;
    private String userName;
    private String client;
    private Timestamp backendStart;
    private String queryStart;
    private Timestamp xactStart;
    private String state;
    private Timestamp stateChange;
    private String backendXid;
    private String backendXmin;
    private int blockBy;
    private String currentQuery;
    private boolean slowQuery;

    public int getPid()
    {
        return pid;
    }

    public void setPid(int pid)
    {
        this.pid = pid;
    }

    public String getAppName()
    {
        return appName;
    }

    public void setAppName(String appName)
    {
        this.appName = appName;
    }

    public String getDbName()
    {
        return dbName;
    }

    public void setDbName(String dbName)
    {
        this.dbName = dbName;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getClient()
    {
        return client;
    }

    public void setClient(String client)
    {
        this.client = client;
    }

    public Timestamp getBackendStart()
    {
        return backendStart;
    }

    public void setBackendStart(Timestamp backendStart)
    {
        this.backendStart = backendStart;
    }

    public String getQueryStart()
    {
        return queryStart;
    }

    public void setQueryStart(String queryStart)
    {
        this.queryStart = queryStart;
    }

    public Timestamp getXactStart()
    {
        return xactStart;
    }

    public void setXactStart(Timestamp xactStart)
    {
        this.xactStart = xactStart;
    }

    public String getState()
    {
        return state;
    }

    public void setState(String state)
    {
        this.state = state;
    }

    public Timestamp getStateChange()
    {
        return stateChange;
    }

    public void setStateChange(Timestamp stateChange)
    {
        this.stateChange = stateChange;
    }

    public String getBackendXid()
    {
        return backendXid;
    }

    public void setBackendXid(String backendXid)
    {
        this.backendXid = backendXid;
    }

    public String getBackendXmin()
    {
        return backendXmin;
    }

    public void setBackendXmin(String backendXmin)
    {
        this.backendXmin = backendXmin;
    }

    public int getBlockBy()
    {
        return blockBy;
    }

    public void setBlockBy(int blockBy)
    {
        this.blockBy = blockBy;
    }

    public String getCurrentQuery()
    {
        return currentQuery;
    }

    public void setCurrentQuery(String currentQuery)
    {
        this.currentQuery = currentQuery;
    }

    public boolean isSlowQuery()
    {
        return slowQuery;
    }

    public void setSlowQuery(boolean slowQuery)
    {
        this.slowQuery = slowQuery;
    }
    
    
    

}
