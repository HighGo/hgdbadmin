/* ------------------------------------------------ 
* 
* File: Feature.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverstatus\model\Feature.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverstatus.model;

/**
 *
 * @author Yuanyuan
 */
public enum Feature
{
    readfile,
	logfile_rotate
}
