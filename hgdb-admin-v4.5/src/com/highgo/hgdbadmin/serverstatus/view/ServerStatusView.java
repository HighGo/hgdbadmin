/* ------------------------------------------------ 
* 
* File: ServerStausView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverstatus\view\ServerStausView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverstatus.view;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.query.view.QueryMainView;
import com.highgo.hgdbadmin.serverstatus.controller.ServerStatusController;
import com.highgo.hgdbadmin.serverstatus.model.LogFileDTO;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 * admin doc: http://www.pgadmin.org/docs/1.20/status.html
 * pgdoc: http://www.postgresql.org/docs/9.4/static/functions-admin.html
 * pg system table chang release: http://www.postgresql.org/docs/9.4/static/release-9-2.html
 * must reference admin source code to get sql
 * 
 */
public class ServerStatusView extends JFrame
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private HelperInfoDTO helperInfo;
    
    private List<String> processColumn;
    private DefaultTableModel processModel;
    private String[] lockHeaders;
    private DefaultTableModel lockModel;
    private DefaultTableModel transactionModel;
    private DefaultTableModel logModel;
    
    private int refreshRate;
    private TimerTask task;
    private Timer timer;
    
    /**
     * Creates new form ServerStatusView
     * @param helperInfo
     */
    public ServerStatusView(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
        initComponents();
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/hgdb.png")));
        setTitle(MessageFormat.format(constBundle.getString("serverStatusTitle") 
                , helperInfo.getUser(),  helperInfo.getHost(), helperInfo.getPort(), helperInfo.getDbName()));
        
        refreshRate = 0;
        String[] refreshTimes = new String[]
        {
            constBundle.getString("notRefresh"),
            //MessageFormat.format(constBundle.getString("xSecond"), 1),//too fast
            MessageFormat.format(constBundle.getString("xSecond"), 5),
            MessageFormat.format(constBundle.getString("xSecond"), 10),
            MessageFormat.format(constBundle.getString("xSecond"), 30),
            MessageFormat.format(constBundle.getString("xMinute"), 1),
            MessageFormat.format(constBundle.getString("xMinute"), 5),
            MessageFormat.format(constBundle.getString("xMinute"), 10),
            MessageFormat.format(constBundle.getString("xMinute"), 30),
            MessageFormat.format(constBundle.getString("xHour"), 1),
        };
        cbRefreshRate.setModel(new DefaultComboBoxModel(refreshTimes));

        ServerStatusController ssc = ServerStatusController.getInstance();
        List<String> dbList = ssc.getDBList(helperInfo);
        for (String db : dbList)
        {
            cbDatabases.addItem(db);
        }
        cbDatabases.setSelectedItem(helperInfo.getDbName());
         
        processColumn = ssc.getProcessColumns(helperInfo);
        String[] processHeader = ssc.getProcessHeaders(processColumn);
        processModel = new DefaultTableModel(processHeader, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return Object.class;
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tblProcess.setModel(processModel);
        tblProcess.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblProcess.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
        tblProcess.getTableHeader().setReorderingAllowed(false);
        //ssc.getProcessList(helperInfo, processColumn, processModel);
        
        lockHeaders = ssc.getLockHeaders(helperInfo);
        lockModel = new DefaultTableModel(lockHeaders, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return Object.class;
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tblLock.setModel(lockModel);
        tblLock.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblLock.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
        tblLock.getTableHeader().setReorderingAllowed(false);
        //ssc.getLockList(helperInfo, lockHeaders, lockModel);

        String[] transactionHeaders = new String[]
        {
            constBundle.getString("prepareStatementId"),
            constBundle.getString("globalId"),
            constBundle.getString("prepareTime"),
            constBundle.getString("owner"),
            constBundle.getString("database")
        };
        transactionModel = new DefaultTableModel(transactionHeaders, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return Object.class;
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tblTransaction.setModel(transactionModel);
        tblTransaction.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblTransaction.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
        tblTransaction.getTableHeader().setReorderingAllowed(false);
        //ssc.getPreparedTransaction(helperInfo, transactionModel);
        
        logModel = new DefaultTableModel(new String[]{"log"}, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return Object.class;
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tblLog.setModel(logModel);
        tblLog.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        tblLog.getTableHeader().setFont(new Font("SansSerif", Font.BOLD, 12));
        tblLog.getTableHeader().setReorderingAllowed(false);     
        tblLog.getColumn("log").setPreferredWidth(300);        
        try
        {
            if (ssc.canViewLog(helperInfo))
            {
                List<LogFileDTO> logList = ssc.getLogFileList(helperInfo);
                for (LogFileDTO log : logList)
                {
                    cbLogFiles.addItem(log);
                }
                //ssc.getLogContent(helperInfo, (LogFileDTO) cbLogFiles.getSelectedItem(), logModel);
            } else
            {
                logModel.addRow(new String[]
                {
                    constBundle.getString("logNotAvailableForTheServer")
                });
            }
        } catch (ClassNotFoundException | SQLException ex)
        {
            logModel.addRow(new String[]
            {
                constBundle.getString("logNotAvailableForTheServer")
            });
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }

        //action
        itemExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                exit(e);
            }
        });
        ActionListener copySelectedRow = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                copySelectedContent(e);
            }
        };
        btnCopy.addActionListener(copySelectedRow);
        itemCopy.addActionListener(copySelectedRow);
        ActionListener startQueryWithQuery = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                startQueryViewWithSelectedQuery(e);
            }
        };
        btnStartQueryByCurrentQuery.addActionListener(startQueryWithQuery);
        itemCopyToQueryTool.addActionListener(startQueryWithQuery);
        
        cbRefreshRate.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbRefreshRateChanged(e);
            }
        });
        cbLogFiles.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbLogFiLeChanged(e);
            }
        });
        cbDatabases.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbDatabaseChanged(e);
            }
        }); 
        btnRefresh.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                refreshCurrentTable(e);
            }
        });
        btnRotateLogFile.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnRoateLogFileAction(e);
            }
        });
        ActionListener cancaleQueryAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancleQuery(e);
            }
        };
        btnCancleQuery.addActionListener(cancaleQueryAction);
        itemCancleQuery.addActionListener(cancaleQueryAction);
        ActionListener terminateBackendAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnTerminateBackend(e);
            }
        };
        btnTerminateBackend.addActionListener(terminateBackendAction);
        itemTerminateBackend.addActionListener(terminateBackendAction);
        ActionListener commitPrepared = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                dealPreparedStatement("commit");
            }
        };
        btnCommitPreparedTransaction.addActionListener(commitPrepared);
        itemCommitPreparedTransaction.addActionListener(commitPrepared);
        ActionListener rollbackPrepared = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                dealPreparedStatement("rollback");
            }
        };
        btnRollbackPreparedTransaction.addActionListener(rollbackPrepared);
        itemRollbackPreparedTransaction.addActionListener(rollbackPrepared);
        //VIEW
        cbItemActivity.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cbItemActivityChanged(e);
            }
        });
        cbItemLocks.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cbItemLocksChanged(e);
            }
        });
        cbItemPreparedTransaction.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cbItemPreparedTransactionChanged(e);
            }
        });
        cbItemLogFile.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cbItemLogFileChanged(e);
            }
        });
        cbItemToolbar.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cbItemToolbarChanged(e);
            }
        });
        cbItemDefaultView.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cbItemDefaultView(e);
            }
        });
        InternalFrameAdapter internalFrameAction = new InternalFrameAdapter()
        {
            @Override
            public void internalFrameClosing(InternalFrameEvent evt)
            {
                internalFrameClosingAction(evt);
            }
            @Override
            public void internalFrameActivated(InternalFrameEvent evt)
            {
                currentActiveFrameChangeAction(evt);
            }
        };
        frameActivity.addInternalFrameListener(internalFrameAction);
        frameLock.addInternalFrameListener(internalFrameAction);
        framePreparedTransaction.addInternalFrameListener(internalFrameAction);
        frameLogFile.addInternalFrameListener(internalFrameAction);
        //HELP 
        ActionListener helpAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                helpAction(e);
            }
        };
        itemHelpContent.addActionListener(helpAction);
        itemServerStatusHelp.addActionListener(helpAction);
        
        
        MouseAdapter mouseClickTableAction = new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tableMouseClicked(evt);
            }
        };
        tblProcess.addMouseListener(mouseClickTableAction);
        tblLock.addMouseListener(mouseClickTableAction);
        tblTransaction.addMouseListener(mouseClickTableAction);
        tblLog.addMouseListener(mouseClickTableAction);

        this.newTask();
        timer.schedule(task, 5);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        toolBar = new javax.swing.JToolBar();
        btnRefresh = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        btnCopy = new javax.swing.JButton();
        btnStartQueryByCurrentQuery = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JToolBar.Separator();
        btnCancleQuery = new javax.swing.JButton();
        btnTerminateBackend = new javax.swing.JButton();
        btnCommitPreparedTransaction = new javax.swing.JButton();
        btnRollbackPreparedTransaction = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JToolBar.Separator();
        cbRefreshRate = new javax.swing.JComboBox();
        jSeparator7 = new javax.swing.JToolBar.Separator();
        cbDatabases = new javax.swing.JComboBox();
        jSeparator8 = new javax.swing.JToolBar.Separator();
        cbLogFiles = new javax.swing.JComboBox();
        btnRotateLogFile = new javax.swing.JButton();
        splitPaneMain = new javax.swing.JSplitPane();
        splitLeft = new javax.swing.JSplitPane();
        splitBottom = new javax.swing.JSplitPane();
        frameLock = new javax.swing.JInternalFrame();
        spLock = new javax.swing.JScrollPane();
        tblLock = new javax.swing.JTable();
        framePreparedTransaction = new javax.swing.JInternalFrame();
        spTransaction = new javax.swing.JScrollPane();
        tblTransaction = new javax.swing.JTable();
        frameActivity = new javax.swing.JInternalFrame();
        spProcess = new javax.swing.JScrollPane();
        tblProcess = new javax.swing.JTable();
        frameLogFile = new javax.swing.JInternalFrame();
        spLog = new javax.swing.JScrollPane();
        tblLog = new javax.swing.JTable();
        txfdStatus = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        itemExit = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        itemCopy = new javax.swing.JMenuItem();
        menuAction = new javax.swing.JMenu();
        itemRefresh = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        itemCopyToQueryTool = new javax.swing.JMenuItem();
        itemCancleQuery = new javax.swing.JMenuItem();
        itemTerminateBackend = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        itemCommitPreparedTransaction = new javax.swing.JMenuItem();
        itemRollbackPreparedTransaction = new javax.swing.JMenuItem();
        menuView = new javax.swing.JMenu();
        cbItemActivity = new javax.swing.JCheckBoxMenuItem();
        cbItemLocks = new javax.swing.JCheckBoxMenuItem();
        cbItemPreparedTransaction = new javax.swing.JCheckBoxMenuItem();
        cbItemLogFile = new javax.swing.JCheckBoxMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        cbItemToolbar = new javax.swing.JCheckBoxMenuItem();
        cbItemHighLightActivityItem = new javax.swing.JCheckBoxMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        cbItemDefaultView = new javax.swing.JCheckBoxMenuItem();
        menuHelp = new javax.swing.JMenu();
        itemHelpContent = new javax.swing.JMenuItem();
        itemServerStatusHelp = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("serverStatus"));
        addWindowListener(new java.awt.event.WindowAdapter()
        {
            public void windowClosing(java.awt.event.WindowEvent evt)
            {
                formWindowClosing(evt);
            }
        });

        toolBar.setRollover(true);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/readdata.png"))); // NOI18N
        btnRefresh.setToolTipText(constBundle.getString("refreshData"));
        btnRefresh.setFocusable(false);
        btnRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnRefresh);
        toolBar.add(jSeparator5);

        btnCopy.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/clip_copy.png"))); // NOI18N
        btnCopy.setToolTipText(constBundle.getString("copySelectedTextToClipboard")
        );
        btnCopy.setFocusable(false);
        btnCopy.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCopy.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCopy);

        btnStartQueryByCurrentQuery.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/clip_copy.png"))); // NOI18N
        btnStartQueryByCurrentQuery.setToolTipText(constBundle.getString("openQueryToolWithSelectedQuery")
        );
        btnStartQueryByCurrentQuery.setEnabled(false);
        btnStartQueryByCurrentQuery.setFocusable(false);
        btnStartQueryByCurrentQuery.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnStartQueryByCurrentQuery.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnStartQueryByCurrentQuery);
        toolBar.add(jSeparator6);

        btnCancleQuery.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/query_cancel.png"))); // NOI18N
        btnCancleQuery.setToolTipText(constBundle.getString("cancleQuery")
        );
        btnCancleQuery.setEnabled(false);
        btnCancleQuery.setFocusable(false);
        btnCancleQuery.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancleQuery.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCancleQuery);

        btnTerminateBackend.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/terminate_backend.png"))); // NOI18N
        btnTerminateBackend.setToolTipText(constBundle.getString("terminateBackend")
        );
        btnTerminateBackend.setEnabled(false);
        btnTerminateBackend.setFocusable(false);
        btnTerminateBackend.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTerminateBackend.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnTerminateBackend);

        btnCommitPreparedTransaction.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/storedata.png"))); // NOI18N
        btnCommitPreparedTransaction.setToolTipText(constBundle.getString("commitPreparedTransaction")
        );
        btnCommitPreparedTransaction.setEnabled(false);
        btnCommitPreparedTransaction.setFocusable(false);
        btnCommitPreparedTransaction.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCommitPreparedTransaction.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnCommitPreparedTransaction);

        btnRollbackPreparedTransaction.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/delete.png"))); // NOI18N
        btnRollbackPreparedTransaction.setToolTipText(constBundle.getString("rollbackPreparedTransaction")
        );
        btnRollbackPreparedTransaction.setEnabled(false);
        btnRollbackPreparedTransaction.setFocusable(false);
        btnRollbackPreparedTransaction.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRollbackPreparedTransaction.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnRollbackPreparedTransaction);
        toolBar.add(jSeparator9);

        toolBar.add(cbRefreshRate);
        toolBar.add(jSeparator7);

        toolBar.add(cbDatabases);
        toolBar.add(jSeparator8);

        cbLogFiles.setEnabled(false);
        toolBar.add(cbLogFiles);

        btnRotateLogFile.setText(constBundle.getString("rotate"));
        btnRotateLogFile.setEnabled(false);
        btnRotateLogFile.setFocusable(false);
        btnRotateLogFile.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRotateLogFile.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnRotateLogFile);

        getContentPane().add(toolBar, java.awt.BorderLayout.NORTH);

        splitPaneMain.setDividerLocation(500);
        splitPaneMain.setResizeWeight(0.8);

        splitLeft.setDividerLocation(200);
        splitLeft.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitLeft.setResizeWeight(0.8);

        splitBottom.setDividerLocation(150);
        splitBottom.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        splitBottom.setResizeWeight(0.8);

        frameLock.setBackground(new java.awt.Color(255, 255, 255));
        frameLock.setClosable(true);
        frameLock.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        frameLock.setTitle(constBundle.getString("locks"));
        frameLock.setFrameIcon(null);
        frameLock.setVisible(true);

        tblLock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblLock.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        spLock.setViewportView(tblLock);

        javax.swing.GroupLayout frameLockLayout = new javax.swing.GroupLayout(frameLock.getContentPane());
        frameLock.getContentPane().setLayout(frameLockLayout);
        frameLockLayout.setHorizontalGroup(
            frameLockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spLock, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE)
        );
        frameLockLayout.setVerticalGroup(
            frameLockLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spLock, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );

        splitBottom.setTopComponent(frameLock);

        framePreparedTransaction.setBackground(new java.awt.Color(255, 255, 255));
        framePreparedTransaction.setClosable(true);
        framePreparedTransaction.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        framePreparedTransaction.setTitle(constBundle.getString("preparedTransactions")
        );
        framePreparedTransaction.setFrameIcon(null);
        framePreparedTransaction.setVisible(true);

        tblTransaction.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblTransaction.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        spTransaction.setViewportView(tblTransaction);

        javax.swing.GroupLayout framePreparedTransactionLayout = new javax.swing.GroupLayout(framePreparedTransaction.getContentPane());
        framePreparedTransaction.getContentPane().setLayout(framePreparedTransactionLayout);
        framePreparedTransactionLayout.setHorizontalGroup(
            framePreparedTransactionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 479, Short.MAX_VALUE)
            .addGroup(framePreparedTransactionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spTransaction, javax.swing.GroupLayout.DEFAULT_SIZE, 479, Short.MAX_VALUE))
        );
        framePreparedTransactionLayout.setVerticalGroup(
            framePreparedTransactionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 65, Short.MAX_VALUE)
            .addGroup(framePreparedTransactionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spTransaction, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE))
        );

        splitBottom.setRightComponent(framePreparedTransaction);

        splitLeft.setBottomComponent(splitBottom);

        frameActivity.setBackground(new java.awt.Color(255, 255, 255));
        frameActivity.setClosable(true);
        frameActivity.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        frameActivity.setTitle(constBundle.getString("activities"));
        frameActivity.setFrameIcon(null);
        frameActivity.setVisible(true);

        spProcess.setBackground(new java.awt.Color(255, 255, 255));

        tblProcess.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblProcess.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        spProcess.setViewportView(tblProcess);

        javax.swing.GroupLayout frameActivityLayout = new javax.swing.GroupLayout(frameActivity.getContentPane());
        frameActivity.getContentPane().setLayout(frameActivityLayout);
        frameActivityLayout.setHorizontalGroup(
            frameActivityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 481, Short.MAX_VALUE)
            .addGroup(frameActivityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spProcess, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE))
        );
        frameActivityLayout.setVerticalGroup(
            frameActivityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 170, Short.MAX_VALUE)
            .addGroup(frameActivityLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spProcess, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
        );

        splitLeft.setLeftComponent(frameActivity);

        splitPaneMain.setLeftComponent(splitLeft);

        frameLogFile.setBackground(new java.awt.Color(255, 255, 255));
        frameLogFile.setClosable(true);
        frameLogFile.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        frameLogFile.setTitle(constBundle.getString("logFiles")
        );
        frameLogFile.setFrameIcon(null);
        frameLogFile.setVisible(true);

        tblLog.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        spLog.setViewportView(tblLog);

        javax.swing.GroupLayout frameLogFileLayout = new javax.swing.GroupLayout(frameLogFile.getContentPane());
        frameLogFile.getContentPane().setLayout(frameLogFileLayout);
        frameLogFileLayout.setHorizontalGroup(
            frameLogFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 214, Short.MAX_VALUE)
            .addGroup(frameLogFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spLog, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE))
        );
        frameLogFileLayout.setVerticalGroup(
            frameLogFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 427, Short.MAX_VALUE)
            .addGroup(frameLogFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spLog, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE))
        );

        splitPaneMain.setRightComponent(frameLogFile);

        getContentPane().add(splitPaneMain, java.awt.BorderLayout.CENTER);

        txfdStatus.setText("status");
        getContentPane().add(txfdStatus, java.awt.BorderLayout.SOUTH);

        menuFile.setText(constBundle.getString("file"));

        itemExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        itemExit.setText(constBundle.getString("exit"));
        menuFile.add(itemExit);

        menuBar.add(menuFile);

        menuEdit.setText(constBundle.getString("edit"));

        itemCopy.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        itemCopy.setText(constBundle.getString("copy"));
        menuEdit.add(itemCopy);

        menuBar.add(menuEdit);

        menuAction.setText(constBundle.getString("actionMenu"));

        itemRefresh.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.CTRL_MASK));
        itemRefresh.setText(constBundle.getString("refresh"));
        menuAction.add(itemRefresh);
        menuAction.add(jSeparator1);

        itemCopyToQueryTool.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        itemCopyToQueryTool.setText(constBundle.getString("copyToQueryTool"));
        itemCopyToQueryTool.setEnabled(false);
        menuAction.add(itemCopyToQueryTool);

        itemCancleQuery.setText(constBundle.getString("cancleQuery"));
        itemCancleQuery.setEnabled(false);
        menuAction.add(itemCancleQuery);

        itemTerminateBackend.setText(constBundle.getString("terminateBackend"));
        itemTerminateBackend.setEnabled(false);
        menuAction.add(itemTerminateBackend);
        menuAction.add(jSeparator2);

        itemCommitPreparedTransaction.setText(constBundle.getString("commitPreparedTransaction"));
        itemCommitPreparedTransaction.setEnabled(false);
        menuAction.add(itemCommitPreparedTransaction);

        itemRollbackPreparedTransaction.setText(constBundle.getString("rollbackPreparedTransaction"));
        itemRollbackPreparedTransaction.setEnabled(false);
        menuAction.add(itemRollbackPreparedTransaction);

        menuBar.add(menuAction);

        menuView.setText(constBundle.getString("view"));

        cbItemActivity.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbItemActivity.setSelected(true);
        cbItemActivity.setText(constBundle.getString("activity"));
        menuView.add(cbItemActivity);

        cbItemLocks.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbItemLocks.setSelected(true);
        cbItemLocks.setText(constBundle.getString("lock"));
        menuView.add(cbItemLocks);

        cbItemPreparedTransaction.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbItemPreparedTransaction.setSelected(true);
        cbItemPreparedTransaction.setText(constBundle.getString("preparedTransaction"));
        menuView.add(cbItemPreparedTransaction);

        cbItemLogFile.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbItemLogFile.setSelected(true);
        cbItemLogFile.setText(constBundle.getString("logFile"));
        menuView.add(cbItemLogFile);
        menuView.add(jSeparator3);

        cbItemToolbar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbItemToolbar.setSelected(true);
        cbItemToolbar.setText(constBundle.getString("toolbar"));
        menuView.add(cbItemToolbar);

        cbItemHighLightActivityItem.setText(constBundle.getString("highlightActivityItem")
        );
        cbItemHighLightActivityItem.setEnabled(false);
        menuView.add(cbItemHighLightActivityItem);
        menuView.add(jSeparator4);

        cbItemDefaultView.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        cbItemDefaultView.setSelected(true);
        cbItemDefaultView.setText(constBundle.getString("defaultView")
        );
        menuView.add(cbItemDefaultView);

        menuBar.add(menuView);

        menuHelp.setText(constBundle.getString("help"));

        itemHelpContent.setText(constBundle.getString("helpContent"));
        menuHelp.add(itemHelpContent);

        itemServerStatusHelp.setText(constBundle.getString("serverStatusHelp"));
        menuHelp.add(itemServerStatusHelp);

        menuBar.add(menuHelp);

        setJMenuBar(menuBar);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        // TODO add your handling code here:
        logger.debug(evt.getSource());
        stopAndExit();
    }//GEN-LAST:event_formWindowClosing
    private void exit(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        stopAndExit();
    }
    private void stopAndExit()
    {
        timer.cancel();
        this.dispose();
    }

    private void helpAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        Object obj = evt.getSource();
        if (obj == itemHelpContent)
        {
           HtmlPageHelper.getInstance().showAdminHelpPage(this, "using.html");
        } else if (obj == itemServerStatusHelp)
        {
           HtmlPageHelper.getInstance().showAdminHelpPage(this, "status.html");
        }
    }
    private void copySelectedContent(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();

        JTable tbl = null;
        if (frameActivity.isSelected())
        {
            tbl = tblProcess;
        } else if (frameLock.isSelected())
        {
            tbl = tblLock;
        } else if (framePreparedTransaction.isSelected())
        {
            tbl = tblTransaction;
        } else if (frameLogFile.isSelected())
        {
            tbl = tblLog;
        }
        
        if (tbl == null)
        {
            logger.info("no frame active, do nothing and return.");
            return;
        }
        
        int row = tbl.getSelectedRow();
        logger.info("row=" + row);
        if (row < 0)
        {
            logger.info("no row is selected, do nothing and return.");
            return;
        }      
        
        StringBuilder content = new StringBuilder();
        int column = tbl.getColumnCount();
        logger.info("column=" + column);
        for (int i = 0; i < column; i++)
        {
            content.append(tbl.getValueAt(row, i)).append("\t");
        }
        logger.info("selected content = " + content.toString());
        Transferable tText = new StringSelection(content.toString());
        clip.setContents(tText, null);
    }
    private void startQueryViewWithSelectedQuery(ActionEvent evt)
    {
        String query = null;
        int row;
        if (frameActivity.isSelected())
        {
            row = tblProcess.getSelectedRow();
            if (row >= 0)
            {
                query = tblProcess.getValueAt(row, tblProcess.getColumnCount() - 1).toString();
            }
        } else if (frameLock.isSelected())
        {
            row = tblLock.getSelectedRow();
            if (row >= 0)
            {
                query = tblLock.getValueAt(row, tblLock.getColumnCount() - 1).toString();
            }
        } else
        {
            return;
        }
        logger.info("query=" + query);
        if (query != null && !query.isEmpty())
        {
            QueryMainView queryView = new QueryMainView(helperInfo, query);
            queryView.setLocationRelativeTo(this);
            queryView.setVisible(true);
        }
    }
    
    //view
    private void cbItemActivityChanged(ActionEvent evt)
    {
        if (cbItemActivity.isSelected())
        {
            splitLeft.setLeftComponent(frameActivity);
        } else
        {
            splitLeft.remove(frameActivity);
            cbItemDefaultView.setSelected(false);
        }
    }    
    private void cbItemLocksChanged(ActionEvent evt)
    {
        if (cbItemLocks.isSelected())
        {
            splitBottom.setTopComponent(frameLock);
        } else
        {
            splitBottom.remove(frameLock);
            cbItemDefaultView.setSelected(false);
        }
    }
    private void cbItemPreparedTransactionChanged(ActionEvent evt)
    {
        if (cbItemPreparedTransaction.isSelected())
        {
            splitBottom.setRightComponent(framePreparedTransaction);
        } else
        {
            splitBottom.remove(framePreparedTransaction);
            cbItemDefaultView.setSelected(false);
        }
    }
    private void cbItemLogFileChanged(ActionEvent evt)
    {
        if (cbItemLogFile.isSelected())
        {
            splitPaneMain.setRightComponent(frameLogFile);
        } else
        {
            splitPaneMain.remove(frameLogFile);
            cbItemDefaultView.setSelected(false);
        }
    }
    private void cbItemToolbarChanged(ActionEvent evt)
    {
        if (cbItemToolbar.isSelected())
        {
            toolBar.setVisible(true);
        } else
        {
            toolBar.setVisible(false);
            cbItemDefaultView.setSelected(false);
        }
    }
    private void cbItemDefaultView(ActionEvent evt)
    {
        if (cbItemDefaultView.isSelected())
        {
            if (!cbItemActivity.isSelected())
            {
                splitLeft.setLeftComponent(frameActivity);
                cbItemActivity.setSelected(true);
            }
            if (!cbItemLocks.isSelected())
            {
                splitBottom.setTopComponent(frameLock);
                cbItemLocks.setSelected(true);
            }
            if (!cbItemPreparedTransaction.isSelected())
            {
                splitBottom.setRightComponent(framePreparedTransaction);
                cbItemPreparedTransaction.setSelected(true);
            }
            if (cbItemLogFile.isSelected())
            {
                splitPaneMain.setRightComponent(frameLogFile);
                cbItemLogFile.setSelected(true);
            }
            if (!cbItemToolbar.isSelected())
            {
                toolBar.setVisible(true);
                cbItemToolbar.setSelected(true);
            }
        }
    }
    //internal frames close
    private void internalFrameClosingAction(InternalFrameEvent evt)
    {
        JInternalFrame frame = (JInternalFrame) evt.getSource();
        if (frame == frameActivity)
        {
            splitLeft.remove(frameActivity);
            cbItemDefaultView.setSelected(false);
            cbItemActivity.setSelected(false);
        } else if (frame == frameLock)
        {
            splitBottom.remove(frameLock);
            cbItemDefaultView.setSelected(false);
            cbItemLocks.setSelected(false);
        } else if (frame == framePreparedTransaction)
        {
            splitBottom.remove(framePreparedTransaction);
            cbItemDefaultView.setSelected(false);
            cbItemPreparedTransaction.setSelected(false);

        } else if (frame == frameLogFile)
        {
            splitPaneMain.remove(frameLogFile);
            cbItemDefaultView.setSelected(false);
            cbItemLogFile.setSelected(false);
        }
    }
    //internal frames active
    private void currentActiveFrameChangeAction(InternalFrameEvent evt)
    {
        JInternalFrame frame = (JInternalFrame) evt.getSource();
        logger.info("isSelected=" + frame.isSelected());
        if (frame.isSelected())
        {
            if (frame == frameActivity)
            {
                cbLogFiles.setEnabled(false);
                btnRotateLogFile.setEnabled(false);

                tblLock.clearSelection();
                tblTransaction.clearSelection();
                tblLog.clearSelection();
            } else if (frame == frameLock)
            {
                cbLogFiles.setEnabled(false);
                btnRotateLogFile.setEnabled(false);

                tblProcess.clearSelection();
                tblTransaction.clearSelection();
                tblLog.clearSelection();
            } else if (frame == framePreparedTransaction)
            {
                cbLogFiles.setEnabled(false);
                btnRotateLogFile.setEnabled(false);

                tblProcess.clearSelection();
                tblLock.clearSelection();
                tblLog.clearSelection();
            } else if (frame == frameLogFile)
            {
                cbLogFiles.setEnabled(true);
                btnRotateLogFile.setEnabled(true);

                tblProcess.clearSelection();
                tblLock.clearSelection();
                tblTransaction.clearSelection();
            }
        }
    }
    //tables selected
    private void tableMouseClicked(MouseEvent evt)                                        
    {                                            
        JTable tbl = (JTable) evt.getSource();
        logger.info("selectedRow=" + tbl.getSelectedRow());
        if (tbl == tblProcess)
        {
            // boolean isSelectRow = (tblProcess.getSelectedRow() >= 0);
            btnStartQueryByCurrentQuery.setEnabled(true);
            btnCancleQuery.setEnabled(true);
            btnTerminateBackend.setEnabled(true);
            btnCommitPreparedTransaction.setEnabled(false);
            btnRollbackPreparedTransaction.setEnabled(false);            
        } else if (tbl == tblLock)
        {
           // boolean isSelectedRow = (tblLock.getSelectedRow() >= 0);
            btnCancleQuery.setEnabled(true);
            btnTerminateBackend.setEnabled(true);
            btnStartQueryByCurrentQuery.setEnabled(false);
            btnCommitPreparedTransaction.setEnabled(false);
            btnRollbackPreparedTransaction.setEnabled(false);

        } else if (tbl == tblTransaction)
        {
           // boolean isSelectedRow = (tblTransaction.getSelectedRow() > 0);
            btnCommitPreparedTransaction.setEnabled(true);
            btnRollbackPreparedTransaction.setEnabled(true);
            btnCancleQuery.setEnabled(false);
            btnTerminateBackend.setEnabled(false);
            btnStartQueryByCurrentQuery.setEnabled(false);
        } else if (tbl == tblLog)
        {
            btnStartQueryByCurrentQuery.setEnabled(false);
            btnCancleQuery.setEnabled(false);
            btnTerminateBackend.setEnabled(false);
            btnCommitPreparedTransaction.setEnabled(false);
            btnRollbackPreparedTransaction.setEnabled(false);
        }
        itemCopyToQueryTool.setEnabled(btnStartQueryByCurrentQuery.isEnabled());
        itemCancleQuery.setEnabled(btnCancleQuery.isEnabled());
        itemTerminateBackend.setEnabled(btnTerminateBackend.isEnabled());
        itemCommitPreparedTransaction.setEnabled(btnCommitPreparedTransaction.isEnabled());
        itemRollbackPreparedTransaction.setEnabled(btnRollbackPreparedTransaction.isEnabled());
        
    }
    //change database
    private void cbDatabaseChanged(ItemEvent evt)
    {
        if(cbDatabases.getSelectedItem()==null)
        {
            return;
        }
        logger.info(cbDatabases.getSelectedItem().toString());
        helperInfo.setDbName(cbDatabases.getSelectedItem().toString());
    }
    //change refresh rate
    private void newTask()
    {
        task = new TimerTask()
        {
            @Override
            public void run()
            {
                refreshCurrentTable(null);
            }
        };
        timer = new Timer();
    }
    private void cbRefreshRateChanged(ItemEvent evt)
    {
        if(cbRefreshRate.getSelectedItem()==null)
        {
            return;
        }
        String rateStr = cbRefreshRate.getSelectedItem().toString();
        logger.info("rate=" + rateStr);
        
        int rate = 0;
        if (rateStr.equals(constBundle.getString("notRefresh")))
        {
            rate = 0;
        } else
        {

            rate = Integer.valueOf(rateStr.split(" ")[0]);
            if (rateStr.equals(MessageFormat.format(constBundle.getString("xMinute"), rate)))
            {
                rate = rate * 60;
            } else if (rateStr.equals(MessageFormat.format(constBundle.getString("xHour"), rate)))
            {
                rate = rate * 3600;
            }
        }
        logger.info("rate=" + rate);
        refreshRate = rate;

        task.cancel();//destory
        this.newTask();//new
        if (rate == 0)
        {
            timer.schedule(task, 5);
        } else
        {
            timer.scheduleAtFixedRate(task, 5, refreshRate * 1000);
        }
    }

    //for activity and locks
    //cancle query
    private String getPid()
    {
        String pid = null;
        if (frameActivity.isSelected())
        {
            if (tblProcess.getSelectedRow() >= 0)
            {
                pid = tblProcess.getValueAt(tblProcess.getSelectedRow(), 0).toString();
            }
        } else if (frameLock.isSelected())
        {
            if (tblLock.getSelectedRow() >= 0)
            {
                pid = tblLock.getValueAt(tblLock.getSelectedRow(), 0).toString();
            }
        }
        logger.info("pid = " + pid);
        return pid;
    }          
    private void btnCancleQuery(ActionEvent evt)
    {
        int response = JOptionPane.showConfirmDialog(this, constBundle.getString("isSureToCancleTheQuery"),
                constBundle.getString("cancleQuery"), JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            String pid = this.getPid();
            if (pid == null)
            {
                logger.warn("pid is null, do nothing and return.");
                return;
            }
            ServerStatusController ssc = ServerStatusController.getInstance();
            try
            {
                ssc.cancleBackend(helperInfo, pid);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    //terminate backend
    private void btnTerminateBackend(ActionEvent evt)
    {
        int response = JOptionPane.showConfirmDialog(this, constBundle.getString("isSureToTerminateTheBackend"),
                constBundle.getString("terminateBackend"), JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            String pid = this.getPid() ;
            if (pid == null)
            {
                logger.warn("pid is null, do nothing and return.");
                return;
            }
            ServerStatusController ssc = ServerStatusController.getInstance();
            try
            {               
                ssc.terminateBackend(helperInfo,pid);
                this.refreshCurrentTable(null);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
   
    //for prepare statement
    //commit prepared
    private void dealPreparedStatement(String command)
    {
        String msg = null;
        String title = null;
        switch (command)
        {
            case "commit":
                msg = constBundle.getString("isSureToCommitThePreparedTransaction");
                title = constBundle.getString("commitPreparedTransaction");
                break;
            case "rollback":
                msg = constBundle.getString("isSureToRollbackThePreparedTransaction");
                title = constBundle.getString("rollbackPreparedTransaction");
                break;
            default:
                logger.error(command + " is an exception command, do nothing and return.");
                break;
        }
        int response = JOptionPane.showConfirmDialog(this, msg, title, JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            String gid = null;
            if (framePreparedTransaction.isSelected())
            {
                if (tblTransaction.getSelectedRow() >= 0)
                {
                    gid = tblTransaction.getValueAt(tblTransaction.getSelectedRow(), 1).toString();
                }
            }
            if (gid == null)
            {
                logger.warn("pid is null, do nothing and return.");
                return;
            }
            ServerStatusController ssc = ServerStatusController.getInstance();
            try
            {
                switch (command)
                {
                    case "commit":
                        ssc.commitPreparedTransaction(helperInfo, gid);
                        break;
                    case "rollback":
                        ssc.rollbackTransaction(helperInfo, gid);
                        break;
                }
                this.refreshCurrentTable(null);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
   
    //rotate log
    private void btnRoateLogFileAction(ActionEvent evt)
    {
        int response = JOptionPane.showConfirmDialog(this, constBundle.getString("isSureToRotateTheLogFile"),
                constBundle.getString("rotate"), JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            ServerStatusController ssc = ServerStatusController.getInstance();
            try
            {
                ssc.rotateLogFile(helperInfo);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    //for log    
    private void removeAllRow(DefaultTableModel model)
    {
        int row = model.getRowCount();
        if (row == 0)
        {
            return;
        }
        for (int i = row-1; i >=0; i--)
        {
            model.removeRow(i);
        }
    }
    private void cbLogFiLeChanged(ItemEvent evt)
    {
        if(cbLogFiles.getSelectedItem()==null)
        {
            return;
        }
        logger.info(cbLogFiles.getSelectedItem().toString());
        this.removeAllRow(logModel);
        ServerStatusController ssc = ServerStatusController.getInstance();
        /*try
        {*/
            ssc.getLogContent(helperInfo, (LogFileDTO) cbLogFiles.getSelectedItem(), logModel);
        /*} catch (Exception ex)
        {            
            JOptionPane.showMessageDialog(this, constBundle.getString("logFileReadError") + "\n" + ex.getMessage().replaceFirst(ex.getClass().getCanonicalName(),""),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }*/
    }
    
    //for all internal frames    
    private void refreshCurrentTable(ActionEvent evt)
    {
        ServerStatusController ssc = ServerStatusController.getInstance();
        //if (frameActivity.isSelected())
        //{
        this.removeAllRow(processModel);
        try
        {
            ssc.getProcessList(helperInfo, processColumn, processModel);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        //} else if (frameLock.isSelected())
        //{
        try
        {
            this.removeAllRow(lockModel);
            ssc.getLockList(helperInfo, lockHeaders, lockModel);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        //} else if (framePreparedTransaction.isSelected())
        //{
        try
        {
            this.removeAllRow(transactionModel);
            ssc.getPreparedTransaction(helperInfo, transactionModel);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        //} else if (frameLogFile.isSelected())
        //{
            cbLogFiLeChanged(null);
        //}
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancleQuery;
    private javax.swing.JButton btnCommitPreparedTransaction;
    private javax.swing.JButton btnCopy;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnRollbackPreparedTransaction;
    private javax.swing.JButton btnRotateLogFile;
    private javax.swing.JButton btnStartQueryByCurrentQuery;
    private javax.swing.JButton btnTerminateBackend;
    private javax.swing.JComboBox cbDatabases;
    private javax.swing.JCheckBoxMenuItem cbItemActivity;
    private javax.swing.JCheckBoxMenuItem cbItemDefaultView;
    private javax.swing.JCheckBoxMenuItem cbItemHighLightActivityItem;
    private javax.swing.JCheckBoxMenuItem cbItemLocks;
    private javax.swing.JCheckBoxMenuItem cbItemLogFile;
    private javax.swing.JCheckBoxMenuItem cbItemPreparedTransaction;
    private javax.swing.JCheckBoxMenuItem cbItemToolbar;
    private javax.swing.JComboBox cbLogFiles;
    private javax.swing.JComboBox cbRefreshRate;
    private javax.swing.JInternalFrame frameActivity;
    private javax.swing.JInternalFrame frameLock;
    private javax.swing.JInternalFrame frameLogFile;
    private javax.swing.JInternalFrame framePreparedTransaction;
    private javax.swing.JMenuItem itemCancleQuery;
    private javax.swing.JMenuItem itemCommitPreparedTransaction;
    private javax.swing.JMenuItem itemCopy;
    private javax.swing.JMenuItem itemCopyToQueryTool;
    private javax.swing.JMenuItem itemExit;
    private javax.swing.JMenuItem itemHelpContent;
    private javax.swing.JMenuItem itemRefresh;
    private javax.swing.JMenuItem itemRollbackPreparedTransaction;
    private javax.swing.JMenuItem itemServerStatusHelp;
    private javax.swing.JMenuItem itemTerminateBackend;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JToolBar.Separator jSeparator6;
    private javax.swing.JToolBar.Separator jSeparator7;
    private javax.swing.JToolBar.Separator jSeparator8;
    private javax.swing.JToolBar.Separator jSeparator9;
    private javax.swing.JMenu menuAction;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuHelp;
    private javax.swing.JMenu menuView;
    private javax.swing.JScrollPane spLock;
    private javax.swing.JScrollPane spLog;
    private javax.swing.JScrollPane spProcess;
    private javax.swing.JScrollPane spTransaction;
    private javax.swing.JSplitPane splitBottom;
    private javax.swing.JSplitPane splitLeft;
    private javax.swing.JSplitPane splitPaneMain;
    private javax.swing.JTable tblLock;
    private javax.swing.JTable tblLog;
    private javax.swing.JTable tblProcess;
    private javax.swing.JTable tblTransaction;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JTextField txfdStatus;
    // End of variables declaration//GEN-END:variables
}
