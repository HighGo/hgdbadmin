/* ------------------------------------------------
 *
 * File: MaintanceController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\maintance\MaintanceController\MaintanceController.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.maintance.controller;

import com.highgo.hgdbadmin.maintance.model.MaintanceInfoDTO;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.sql.Connection;
import java.sql.SQLWarning;
import java.sql.Statement;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class MaintanceController
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private MaintanceController()
    {
    }
    private static MaintanceController maintanceController = null;
    public static MaintanceController getInstance()
    {
        if (maintanceController == null)
        {
            maintanceController = new MaintanceController();
        }
        return maintanceController;
    }     
   
    private boolean isStop;
    private class MaintainRunnable implements Runnable
    {
        private AbstractObject obj;
        private MaintanceInfoDTO maintanceInfo;
        private Document docs;
        private JButton btnOK;

        public MaintainRunnable(AbstractObject obj, MaintanceInfoDTO maintanceInfo, Document docs, JButton btnOK)
        {
            this.obj = obj;
            this.maintanceInfo = maintanceInfo;
            this.docs = docs;
            this.btnOK = btnOK;
        }

        @Override
        public void run()
        {
            maintain(obj, maintanceInfo, docs, btnOK);
        }
    }
    public void startMaintainThread(AbstractObject obj, MaintanceInfoDTO maintanceInfo, Document docs, JButton btnOK)
    {
        logger.info("start");
        isStop = false;
        MaintainRunnable vr = new MaintainRunnable(obj, maintanceInfo, docs, btnOK);
        Thread thread = new Thread(vr);
        thread.start();
    }
    public void stopMaintain()
    {
        logger.info("stop");
        isStop = true;
    }
    public boolean isStop()
    {
        return isStop;
    }

    //for vacuum
    public void maintain(AbstractObject obj, MaintanceInfoDTO maintanceInfo, Document docs, JButton btnOK)
    {
        if (obj == null)
        {
            logger.error("Error：relation is null, do nothing, return.");
            return;//msg.toString();
        }
        logger.info("Enter:relationName = " + obj.getName());
        HelperInfoDTO helperInfo = obj.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("Error：connectInfo is null, do nothing, return.");
            return;// msg.toString();
        }
        Connection conn = null;
        Statement stmt = null;
        String url = helperInfo.getPartURL() + helperInfo.getDbName();        
        logger.info("url: " + url);
        
        SimpleAttributeSet attr = new SimpleAttributeSet();
        try
        {
            if (isStop)
            {
                return;
            }
            conn = JdbcHelper.getConnection(url, helperInfo);
            logger.info("Connected");
            if (isStop)
            {
                return;
            }
            stmt = conn.createStatement();
            if (isStop)
            {
                return;
            }
            long startTime = System.currentTimeMillis();
            String sql = getMaintainSQL(maintanceInfo, obj);
            stmt.executeUpdate(sql);
            
            docs.insertString(0, "", attr);
            SQLWarning warnings = stmt.getWarnings();
            while (warnings != null && !isStop)
            {
                docs.insertString(docs.getLength(), warnings.toString() + "\n", attr);
                warnings = warnings.getNextWarning();
            }
            long endTime = System.currentTimeMillis();
            docs.insertString(docs.getLength(), constBundle.getString("totalTimeConsuming") + (endTime - startTime) + " ms", attr);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            //throw new Exception(ex);
            try
            {
                docs.insertString(docs.getLength(), ex.getMessage(), attr);
            } catch (BadLocationException ex1)
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            isStop = true;
            btnOK.setText(constBundle.getString("finish"));
            btnOK.setEnabled(true);
            logger.info("Close Resource");
        }
    }
    private String getMaintainSQL(MaintanceInfoDTO maintanceInfo,AbstractObject obj)
    {
        StringBuilder sql = new StringBuilder();
        TreeEnum.TreeNode type = obj.getType();
        TreeEnum.MaintanceOperation operation = maintanceInfo.getMaintanceOpertion();
        logger.info("type=" + type + ", operation=" + operation);
        switch (operation)
        {
            case VACUUM://db,table
                sql.append("VACUUM");
                if (maintanceInfo.isFull())
                {
                    sql.append(" FULL");
                }
                if (maintanceInfo.isFreeze())
                {
                    sql.append(" FREEZE");
                }
                if (maintanceInfo.isVerbose())
                {
                    sql.append(" VERBOSE");
                }
                if (maintanceInfo.isAnalyze())
                {
                    sql.append(" ANALYZE");
                }                
                if(type.equals( TreeEnum.TreeNode.TABLE))
                {
                    sql.append(" ").append(obj.getHelperInfo().getSchema()).append(".").append(obj.getName());
                }
                break;
            case ANALYZE:
                sql.append("ANALYZE");
                if (maintanceInfo.isVerbose())
                {
                    sql.append(" VERBOSE");
                }
                if (type.equals(TreeEnum.TreeNode.TABLE))
                {
                    sql.append(" ").append(obj.getHelperInfo().getSchema()).append(".").append(obj.getName());
                }
                break;
            case REINDEX:
                sql.append("REINDEX");
                switch (type)
                {
                    case DATABASE:
                        sql.append(" DATABASE ").append(obj.getName());
                        break;
                    case TABLE:
                        sql.append(" TABLE ").append(obj.getHelperInfo().getSchema()).append(".").append(obj.getName());
                        break;
                    case INDEX:
                        sql.append(" INDEX ").append(obj.getHelperInfo().getSchema()).append(".").append(obj.getName());
                        break;
                    default:
                        logger.error(type + " is an exception type");
                        break;
                }
                break;
            case CLUSTER:
                sql.append("CLUSTER ");
                if (maintanceInfo.isVerbose())
                {
                    sql.append(" VERBOSE");
                }
                if (type.equals(TreeEnum.TreeNode.TABLE))
                {
                    sql.append(" ").append(obj.getHelperInfo().getSchema()).append(".").append(obj.getName());
                } else if (type.equals(TreeEnum.TreeNode.INDEX))
                {
                    sql.append(" ").append(obj.getHelperInfo().getSchema()).append(".").append(obj.getHelperInfo().getRelation())
                            .append(" USING ").append(obj.getName());//index name never with schema
                }
                break;
        }
        logger.info("Return: sql=" + sql.toString());
        return sql.toString();
    }
    
    
}
