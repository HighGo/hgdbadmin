/* ------------------------------------------------
 *
 * File: MaintanceInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\maintance\model\MaintanceInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.maintance.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Yuanyuan
 */
public class MaintanceInfoDTO
{
    private TreeEnum.MaintanceOperation maintanceOpertion;
    private boolean full;
    private boolean freeze;
    private boolean analyze;
    private boolean verbose;

    public TreeEnum.MaintanceOperation getMaintanceOpertion()
    {
        return maintanceOpertion;
    }

    public void setMaintanceOpertion(TreeEnum.MaintanceOperation maintanceOpertion)
    {
        this.maintanceOpertion = maintanceOpertion;
    }

    public boolean isFull()
    {
        return full;
    }

    public void setFull(boolean full)
    {
        this.full = full;
    }

    public boolean isFreeze()
    {
        return freeze;
    }

    public void setFreeze(boolean freeze)
    {
        this.freeze = freeze;
    }

    public boolean isAnalyze()
    {
        return analyze;
    }

    public void setAnalyze(boolean analyze)
    {
        this.analyze = analyze;
    }

    public boolean isVerbose()
    {
        return verbose;
    }

    public void setVerbose(boolean verbose)
    {
        this.verbose = verbose;
    }    
}
