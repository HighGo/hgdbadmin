/* ------------------------------------------------
 *
 * File: BackupController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\backup\view\BackupController.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.backup.controller;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class BackupController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(this.getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private static BackupController bc;
    public static BackupController getInstance()
    {
        if (bc == null)
        {
            bc = new BackupController();
        }
        return bc;
    }
    
    public void backup(HelperInfoDTO helperInfo, List<String> cmds, Document docs)throws Exception
    {
        Process proc = null;
        SimpleAttributeSet attr = new SimpleAttributeSet();
        try
        {            
            ProcessBuilder pb = new ProcessBuilder(cmds);
            Map<String, String> map = pb.environment();
            map.put("PGPASSWORD", helperInfo.getPwd());
            //File log = new File("loglyy");
            pb.redirectErrorStream(true);
            //pb.redirectOutput(ProcessBuilder.Redirect.to(log));
            proc = pb.start();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream())))//, "GBK"
            {
                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    docs.insertString(docs.getLength(), line + System.lineSeparator(), attr);
                    //logger.info(line);
                }
            }
            int exitValue = proc.waitFor(); //proc.exitValue();
            logger.debug("exitValue=" + exitValue);
            docs.insertString(docs.getLength(), MessageFormat.format(constBundle.getString("backupExitValue"), exitValue), attr);
        } catch (IOException | BadLocationException ex)
        {
            if (proc != null)
            {
                proc.destroy();
            }
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);            
        }
    }
    
    public void backupAll(HelperInfoDTO helperInfo, List<String> cmds, Document docs) throws Exception
    {
        Process proc = null;
        SimpleAttributeSet attr = new SimpleAttributeSet();
        try
        {

            ProcessBuilder pb = new ProcessBuilder(cmds);
            Map<String, String> map = pb.environment();
            map.put("PGPASSWORD", helperInfo.getPwd());
            //File log = new File("loglyy");
            pb.redirectErrorStream(true);
            //pb.redirectOutput(ProcessBuilder.Redirect.to(log));

            proc = pb.start();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream())))//, "GBK"
            {
                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    docs.insertString(docs.getLength(), line + System.lineSeparator(), attr);
                }
            }
            int exitValue = proc.waitFor(); //proc.exitValue();
            logger.debug("exitValue=" + exitValue);
            docs.insertString(docs.getLength(), MessageFormat.format(constBundle.getString("backupExitValue"), exitValue), attr);
        } catch (IOException | BadLocationException ex)
        {
            if (proc != null)
            {
                proc.destroy();
            }
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
    }
    
    public void restore(HelperInfoDTO helperInfo, List<String> cmds, Document docs) throws Exception
    {
        Process proc = null;
        SimpleAttributeSet attr = new SimpleAttributeSet();
        try
        {
            ProcessBuilder pb = new ProcessBuilder(cmds);
            Map<String, String> map = pb.environment();
            map.put("PGPASSWORD", helperInfo.getPwd());
            //File log = new File("loglyy");
            pb.redirectErrorStream(true);
            //pb.redirectOutput(ProcessBuilder.Redirect.to(log));
            proc = pb.start();
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream())))//, "GBK"
            {
                String line;
                while ((line = bufferedReader.readLine()) != null)
                {
                    docs.insertString(docs.getLength(), line + System.lineSeparator(), attr);
                    //logger.info(line);
                }
            }
            int exitValue = proc.waitFor(); //proc.exitValue();
            logger.debug("exitValue=" + exitValue);
            docs.insertString(docs.getLength(), MessageFormat.format(constBundle.getString("backupExitValue"), exitValue), attr);
        } catch (IOException | BadLocationException ex)
        {
            if (proc != null)
            {
                proc.destroy();
            }
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
    }
    
    
}
