/* ------------------------------------------------
 *
 * File: BackupAllView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\backup\view\BackupAllView.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.backup.view;

import com.highgo.hgdbadmin.backup.controller.BackupController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import com.highgo.hgdbadmin.util.TreeEnum;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.text.Document;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 * 
 * dump all:
 * D:\Program Files (x86)\PostgreSQL\9.4\bin\pg_dumpall.exe
 * --host localhost --port 5433 --username "postgres" --database "postgres" --no-password 
 * --verbose --file "C:\Users\Yuanyuan\Desktop\ee2" --globals-only
 * 
 * dump server:
 * D:\Program Files (x86)\PostgreSQL\9.4\bin\pg_dumpall.exe 
 * --host localhost --port 5433 --username "postgres" --role "postgres" --no-password  
 * --verbose --file "C:\Users\Yuanyuan\Desktop\ee.txt"
 * 
 * 
 */
public class BackupAllView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private HelperInfoDTO helperInfo;
    
    /**
     * Creates new form BackupAllView
     * @param parent
     * @param modal
     * @param serverInfo
     */
    public BackupAllView(Frame parent, boolean modal, XMLServerInfoDTO serverInfo)
    {
        super(parent, modal);
        initComponents();
        helperInfo = new HelperInfoDTO();
        helperInfo.setHost(serverInfo.getHost());
        helperInfo.setPort(serverInfo.getPort());
        helperInfo.setMaintainDB(serverInfo.getMaintainDB());
        helperInfo.setOnSSL(serverInfo.isOnSSL());
        helperInfo.setUser(serverInfo.getUser());
        helperInfo.setPwd(serverInfo.getPwd());
        String vinfo = serverInfo.getVersion().split(",")[0];
        if (vinfo.startsWith("HighGo Database"))
        {
            helperInfo.setDbSys(TreeEnum.DBSYS.HIGHGO);
            helperInfo.setVersionNumber(vinfo.split(" ")[2]);
        } else if (vinfo.startsWith("PostgreSQL"))
        {
            helperInfo.setDbSys(TreeEnum.DBSYS.POSTGRESQL);
            helperInfo.setVersionNumber(vinfo.split(" ")[1]);
        }
        this.setTitle(MessageFormat.format(constBundle.getString("backupAllServer"), serverInfo.getHost()));
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/backup.png")));        
        String[] formatArray = new String[]
        {
            constBundle.getString("plain")
        };
        cbFormat.setModel(new DefaultComboBoxModel(formatArray));
        cbFormat.setSelectedIndex(0);
        
        TreeController tc = TreeController.getInstance();
        try
        {
//            String[] owners = (String[]) tc.getItemsArrary(helperInfo, "owner");
            String[] owners = tc.getItemsArrary(helperInfo, "owner");
            cbbRoleName.setModel(new DefaultComboBoxModel(owners));           
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
        {
            cbForceDoubleQuote.setEnabled(true);
        }
        
        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnHelpActionPerformed(e);
            }
        });
        txfdFileName.addKeyListener(new KeyAdapter()
        {
            public void KeyReleased(KeyEvent evt)
            {
                txfdFileNameKeyTyped(evt);
            }
        });
        btnChooseFile.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnChooseFileActionPerformed(e);
            }
        });
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancleActionPerformed(e);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOKActionPerformed(e);
            }
        }); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        tpl = new javax.swing.JTabbedPane();
        pnlOption = new javax.swing.JPanel();
        lblFileName = new javax.swing.JLabel();
        txfdFileName = new javax.swing.JTextField();
        btnChooseFile = new javax.swing.JButton();
        cbbRoleName = new javax.swing.JComboBox();
        lblRoleName = new javax.swing.JLabel();
        cbFormat = new javax.swing.JComboBox();
        lblFormat = new javax.swing.JLabel();
        cbVerboseMsg = new javax.swing.JCheckBox();
        cbForceDoubleQuote = new javax.swing.JCheckBox();
        cbOnlyGlobalObject = new javax.swing.JCheckBox();
        pnlMsg = new javax.swing.JPanel();
        spMsg = new javax.swing.JScrollPane();
        tpMsg = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnCancle = new javax.swing.JButton();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        tpl.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        lblFileName.setText(constBundle.getString("fileName"));

        btnChooseFile.setText("...");

        lblRoleName.setText(constBundle.getString("roleName"));

        cbFormat.setEnabled(false);

        lblFormat.setText(constBundle.getString("format"));

        cbVerboseMsg.setSelected(true);
        cbVerboseMsg.setText(constBundle.getString("verboseMsg"));

        cbForceDoubleQuote.setText(constBundle.getString("forceDoubleQuoteOnIdentifier"));
        cbForceDoubleQuote.setEnabled(false);

        cbOnlyGlobalObject.setSelected(true);
        cbOnlyGlobalObject.setText(constBundle.getString("onlyGlobalObject"));

        javax.swing.GroupLayout pnlOptionLayout = new javax.swing.GroupLayout(pnlOption);
        pnlOption.setLayout(pnlOptionLayout);
        pnlOptionLayout.setHorizontalGroup(
            pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlOptionLayout.createSequentialGroup()
                        .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlOptionLayout.createSequentialGroup()
                                .addComponent(lblFileName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txfdFileName, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnChooseFile, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlOptionLayout.createSequentialGroup()
                                .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblRoleName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblFormat, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cbFormat, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbbRoleName, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(10, 10, 10))
                    .addGroup(pnlOptionLayout.createSequentialGroup()
                        .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbVerboseMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE)
                            .addComponent(cbForceDoubleQuote, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbOnlyGlobalObject, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        pnlOptionLayout.setVerticalGroup(
            pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFileName)
                    .addComponent(txfdFileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChooseFile))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblRoleName)
                    .addComponent(cbbRoleName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblFormat)
                    .addComponent(cbFormat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 165, Short.MAX_VALUE)
                .addComponent(cbOnlyGlobalObject)
                .addGap(10, 10, 10)
                .addComponent(cbForceDoubleQuote)
                .addGap(10, 10, 10)
                .addComponent(cbVerboseMsg)
                .addContainerGap())
        );

        tpl.addTab(constBundle.getString("fileOption"), pnlOption);

        spMsg.setViewportView(tpMsg);

        javax.swing.GroupLayout pnlMsgLayout = new javax.swing.GroupLayout(pnlMsg);
        pnlMsg.setLayout(pnlMsgLayout);
        pnlMsgLayout.setHorizontalGroup(
            pnlMsgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spMsg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 511, Short.MAX_VALUE)
        );
        pnlMsgLayout.setVerticalGroup(
            pnlMsgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(spMsg, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
        );

        tpl.addTab(constBundle.getString("msg"), pnlMsg);

        btnCancle.setText(constBundle.getString("cancle"));

        btnHelp.setText(constBundle.getString("help"));

        btnOK.setText(constBundle.getString("ok"));
        btnOK.setEnabled(false);

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnHelp)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnOK)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancle)
                .addContainerGap())
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancle)
                    .addComponent(btnHelp)
                    .addComponent(btnOK))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tpl)
            .addComponent(pnlButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(tpl)
                .addGap(0, 0, 0)
                .addComponent(pnlButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnHelpActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showPgHelpPage(this, "app-pg-dumpall.html");
    }
    private void txfdFileNameKeyTyped(KeyEvent evt)
    {
        logger.info(txfdFileName.getText());
        if (!txfdFileName.getText().isEmpty())
        {
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }    
    private void btnChooseFileActionPerformed(ActionEvent evt)
    {
        FileChooserDialog chooser = new FileChooserDialog();
        int response = chooser.showFileChooser(this, constBundle.getString("selectOutputFile"), null);
        if (response == JFileChooser.APPROVE_OPTION)
        {
            txfdFileName.setText(chooser.getChoosedFileDir());
            btnOK.setEnabled(true);
        }
    }    
    private void btnCancleActionPerformed(ActionEvent evt)
    {
        this.dispose();
    }
    private void btnOKActionPerformed(ActionEvent evt)
    {
        if (btnOK.getText().equals(constBundle.getString("finish")))
        {
            this.dispose();
            return;
        }
        File f = new File(txfdFileName.getText());
        if (f.exists())
        {
            int response = JOptionPane.showConfirmDialog(this,
                    MessageFormat.format(constBundle.getString("confirmSave"), txfdFileName.getText()),
                    constBundle.getString("confirmSaveTitle"), JOptionPane.OK_CANCEL_OPTION);
            if (response == JOptionPane.CANCEL_OPTION)
            {
                return;
            }
        }
        logger.info("host=" + helperInfo.getHost() + ",port=" + helperInfo.getPort()
                + ",user=" + helperInfo.getUser() + ",db=" + helperInfo.getDbName());
        tpl.setSelectedIndex(1);
        try
        {
            StringBuilder cmdStr = new StringBuilder();
            List<String> cmds = this.getCommandList(cmdStr);
            logger.info("cmd: " + cmdStr.toString());
            tpMsg.setText(cmdStr.toString() + System.lineSeparator());
            Document docs = tpMsg.getDocument();
            BackupController.getInstance().backupAll(helperInfo, cmds, docs);
            btnOK.setText(constBundle.getString("finish"));
            btnOK.setEnabled(true);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }    
    private List<String> getCommandList(StringBuilder cmdStr) throws Exception
    {
        List<String> cmds = new ArrayList<>();        
        OptionController oc = OptionController.getInstance();
        String bin = oc.getOptionProperties().getProperty("bin_path");
        if (bin == null || bin.isEmpty())
        {
            throw new Exception(constBundle.getString("binPathNeedSet"));
        }
        cmds.add(bin + File.separator + "pg_dumpall");
        cmds.add("--host");
        cmds.add(helperInfo.getHost());
        cmds.add("--port");
        cmds.add(helperInfo.getPort());
        cmds.add("--username");
        cmds.add(helperInfo.getUser());        
        cmdStr.append(bin).append(File.separator).append("pg_dumpall")
                .append(" --host ").append(helperInfo.getHost())
                .append(" --port ").append(helperInfo.getPort())
                .append(" --username \"").append(helperInfo.getUser()).append("\"");
        
        if (cbOnlyGlobalObject.isSelected())
        {
            cmds.add("--database");
            cmds.add(helperInfo.getMaintainDB());            
            cmdStr.append(" --database \"").append(helperInfo.getMaintainDB()).append("\"");
        }
        if (cbbRoleName.getSelectedIndex() >0)
        {
            cmds.add("--role");
            cmds.add(cbbRoleName.getSelectedItem().toString());
            cmdStr.append(" --role \"").append(cbbRoleName.getSelectedItem().toString()).append("\"");
        }
        //cmds.add("--no-password");
        if (cbVerboseMsg.isSelected())
        {
            cmds.add("--verbose");
            cmdStr.append(" --verbose ");
        }
        if (cbForceDoubleQuote.isEnabled())
        {
            if (cbForceDoubleQuote.isSelected())
            {
                cmds.add("--quote-all-identifiers");
                cmdStr.append(" --quote-all-identifiers");
            }
        }
        cmds.add("--file");
        cmds.add(txfdFileName.getText());
        cmdStr.append(" --file ").append(txfdFileName.getText());
        if (cbOnlyGlobalObject.isSelected())
        {
            cmds.add("--globals-only");
            cmdStr.append(" --globals-only");
        }
        return cmds;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnChooseFile;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.JCheckBox cbForceDoubleQuote;
    private javax.swing.JComboBox cbFormat;
    private javax.swing.JCheckBox cbOnlyGlobalObject;
    private javax.swing.JCheckBox cbVerboseMsg;
    private javax.swing.JComboBox cbbRoleName;
    private javax.swing.JLabel lblFileName;
    private javax.swing.JLabel lblFormat;
    private javax.swing.JLabel lblRoleName;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlMsg;
    private javax.swing.JPanel pnlOption;
    private javax.swing.JScrollPane spMsg;
    private javax.swing.JTextPane tpMsg;
    private javax.swing.JTabbedPane tpl;
    private javax.swing.JTextField txfdFileName;
    // End of variables declaration//GEN-END:variables
}
