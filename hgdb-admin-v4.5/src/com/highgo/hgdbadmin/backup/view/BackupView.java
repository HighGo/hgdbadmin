/* ------------------------------------------------
 *
 * File: BackupView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\backup\view\BackupView.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.backup.view;

import com.highgo.hgdbadmin.backup.controller.BackupController;
import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.text.Document;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author Liu Yuanyuan
 * 
 * 
 */
public class BackupView extends JDialog
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private AbstractObject obj;
    private HelperInfoDTO helperInfo;
    
    /**
     * Creates new form ServerAddView
     * @param parent
     * @param modal
     * @param obj
     */
    public BackupView(JFrame parent, boolean modal, AbstractObject obj)
    {
        super(parent, modal);
        this.obj = obj;
        initComponents();
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/backup.png")));
        jtp.setEnabledAt(3, false);
        switch (obj.getType())
        {
            case DATABASE:
            case SCHEMA:
            case TABLE:
                this.setTitle(MessageFormat.format(constBundle.getString("backupObject"), 
                        constBundle.getString(obj.getType().toString().toLowerCase()), obj.getName()));
                break;
            default:
                logger.error(obj.getType() + " is an exception type, do nothing and break.");
                break;
        }

        helperInfo = obj.getHelperInfo();
        String[] formatArray = new String[]
        {
            constBundle.getString("custom")
            ,"Tar"
            //,constBundle.getString("plain")
        };
        cbFormat.setModel(new DefaultComboBoxModel(formatArray));
        cbFormat.setSelectedIndex(0);       
        TreeController tc = TreeController.getInstance();
        /*if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
        {
            cbFormat.addItem(constBundle.getString("directory"));
        }*/       
       
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 2))
        {
            txfdJobNumber.setEnabled(true);
        } else
        {
            txfdJobNumber.setEditable(false);
            txfdJobNumber.setEnabled(false);
        }
        try
        {
//            String[] owners = (String[]) tc.getItemsArrary(helperInfo, "owner");
            String[] owners = tc.getItemsArrary(helperInfo, "owner");
            cbbRoleName.setModel(new DefaultComboBoxModel(owners));
        } catch (ClassNotFoundException | SQLException ex)
        {
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
        {
            cbUnloggedTableData.setEnabled(true);
            cbForceDoubleQuote.setEnabled(true);           
            if (tc.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
            {
                pnlSection.setEnabled(true);
                cbPreData.setEnabled(true);
                cbData.setEnabled(true);
                cbPostData.setEnabled(true);
            }
        }            
        try
        {
            cbEncoding.setModel(new DefaultComboBoxModel(tc.getEncodingList(helperInfo)));
        } catch (ClassNotFoundException | SQLException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        
        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnHelpActionPerformed(evt);
            }
        });
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnCancleActionPerformed(evt);
            }
        });
        btnOK.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                btnOKActionPerformed(evt);
            }
        });
        btnChooseFile.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnChooseFileActionPerformed(e);
            }
        });
//        jtp.addChangeListener(new ChangeListener()
//        {
//            @Override
//            public void stateChanged(ChangeEvent e)
//            {
//                jtpStateChanged(e);
//            }
//        });
        txfdFileName.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                txfdFileNameKeyTyped(evt);
            }
        });
        txfdJobNumber.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyTyped(KeyEvent evt)
            {
                txfdIntegerValueKeyTyped(evt);
            }
        });
        cbFormat.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbFormatItemStateChanged(e);
            }
        });
        ActionListener selectedActionListener = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                selectedActionPerformed(e);
            }
        };
        cbPreData.addActionListener(selectedActionListener);
        cbData.addActionListener(selectedActionListener);
        cbPostData.addActionListener(selectedActionListener);
        cbOnlyData.addActionListener(selectedActionListener);
        cbOnlySchema.addActionListener(selectedActionListener);        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jtp = new javax.swing.JTabbedPane();
        pnlFiltOption = new javax.swing.JPanel();
        lblFileName = new javax.swing.JLabel();
        lblFormat = new javax.swing.JLabel();
        txfdFileName = new javax.swing.JTextField();
        cbFormat = new javax.swing.JComboBox();
        txfdCompressRatio = new javax.swing.JTextField();
        lblCompressRatio = new javax.swing.JLabel();
        txfdJobNumber = new javax.swing.JTextField();
        lblJobNumber = new javax.swing.JLabel();
        lblEncoding = new javax.swing.JLabel();
        lblRoleName = new javax.swing.JLabel();
        cbEncoding = new javax.swing.JComboBox();
        cbbRoleName = new javax.swing.JComboBox();
        btnChooseFile = new javax.swing.JButton();
        pnlDumpOption1 = new javax.swing.JPanel();
        pnlSection = new javax.swing.JPanel();
        cbPreData = new javax.swing.JCheckBox();
        cbData = new javax.swing.JCheckBox();
        cbPostData = new javax.swing.JCheckBox();
        pnlObjType = new javax.swing.JPanel();
        cbOnlyData = new javax.swing.JCheckBox();
        cbOnlySchema = new javax.swing.JCheckBox();
        cbBlobs = new javax.swing.JCheckBox();
        pnlNotSave = new javax.swing.JPanel();
        cbOwner = new javax.swing.JCheckBox();
        cbPrivilege = new javax.swing.JCheckBox();
        cbTablespace = new javax.swing.JCheckBox();
        cbUnloggedTableData = new javax.swing.JCheckBox();
        pnlDumpOption2 = new javax.swing.JPanel();
        pnlQuery = new javax.swing.JPanel();
        cbIncludeCreateDB = new javax.swing.JCheckBox();
        cbIncludeDropDB = new javax.swing.JCheckBox();
        cbUseColumnInsert = new javax.swing.JCheckBox();
        cbUseInsertCommand = new javax.swing.JCheckBox();
        pnlDisable = new javax.swing.JPanel();
        cbTrigger = new javax.swing.JCheckBox();
        cbDollarQuoting = new javax.swing.JCheckBox();
        pnlMiscellanous = new javax.swing.JPanel();
        cbUseSetSession = new javax.swing.JCheckBox();
        cbWithOids = new javax.swing.JCheckBox();
        cbVerboseMsg = new javax.swing.JCheckBox();
        cbForceDoubleQuote = new javax.swing.JCheckBox();
        pnlObject = new javax.swing.JPanel();
        pnlMsg = new javax.swing.JPanel();
        spMsg = new javax.swing.JScrollPane();
        tpMsg = new javax.swing.JTextPane();
        pnlButton = new javax.swing.JPanel();
        btnHelp = new javax.swing.JButton();
        btnOK = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("backup"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/backup.png")));
setModal(true);
setName("dlgServerAdd"); // NOI18N

jtp.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);
jtp.setMinimumSize(new java.awt.Dimension(520, 470));
jtp.setPreferredSize(new java.awt.Dimension(520, 420));

pnlFiltOption.setBackground(new java.awt.Color(255, 255, 255));
pnlFiltOption.setMinimumSize(new java.awt.Dimension(520, 470));
pnlFiltOption.setPreferredSize(new java.awt.Dimension(520, 470));

lblFileName.setText(constBundle.getString("fileName"));

lblFormat.setText(constBundle.getString("format"));

lblCompressRatio.setText(constBundle.getString("compressRatio"));

txfdJobNumber.setEditable(false);

lblJobNumber.setText(constBundle.getString("jobNumber"));

lblEncoding.setText(constBundle.getString("encoding"));

lblRoleName.setText(constBundle.getString("roleName"));

btnChooseFile.setText("...");

javax.swing.GroupLayout pnlFiltOptionLayout = new javax.swing.GroupLayout(pnlFiltOption);
pnlFiltOption.setLayout(pnlFiltOptionLayout);
pnlFiltOptionLayout.setHorizontalGroup(
    pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(pnlFiltOptionLayout.createSequentialGroup()
        .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlFiltOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlFiltOptionLayout.createSequentialGroup()
                        .addComponent(lblEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(cbEncoding, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pnlFiltOptionLayout.createSequentialGroup()
                        .addComponent(lblRoleName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(cbbRoleName, 0, 407, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlFiltOptionLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(lblFileName, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txfdFileName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnChooseFile, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pnlFiltOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblFormat, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(cbFormat, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(pnlFiltOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCompressRatio, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txfdCompressRatio))
            .addGroup(pnlFiltOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblJobNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(txfdJobNumber)))
        .addContainerGap())
    );
    pnlFiltOptionLayout.setVerticalGroup(
        pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlFiltOptionLayout.createSequentialGroup()
            .addGap(9, 9, 9)
            .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblFileName)
                .addComponent(txfdFileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnChooseFile))
            .addGap(9, 9, 9)
            .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblFormat)
                .addComponent(cbFormat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblCompressRatio)
                .addComponent(txfdCompressRatio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblEncoding)
                .addComponent(cbEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblJobNumber)
                .addComponent(txfdJobNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addGroup(pnlFiltOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(lblRoleName)
                .addComponent(cbbRoleName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addContainerGap(284, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("fileOption"), pnlFiltOption);
    pnlFiltOption.getAccessibleContext().setAccessibleName("");

    pnlDumpOption1.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlDumpOption1.setPreferredSize(new java.awt.Dimension(520, 470));

    pnlSection.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("section")));
    pnlSection.setEnabled(false);

    cbPreData.setText("Pre-data");
    cbPreData.setEnabled(false);

    cbData.setText(constBundle.getString("data"));
    cbData.setEnabled(false);

    cbPostData.setText("Post-data");
    cbPostData.setEnabled(false);

    javax.swing.GroupLayout pnlSectionLayout = new javax.swing.GroupLayout(pnlSection);
    pnlSection.setLayout(pnlSectionLayout);
    pnlSectionLayout.setHorizontalGroup(
        pnlSectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSectionLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlSectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbPreData)
                .addComponent(cbData)
                .addComponent(cbPostData))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnlSectionLayout.setVerticalGroup(
        pnlSectionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlSectionLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbPreData)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbData)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbPostData)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    pnlObjType.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("objType")));

    cbOnlyData.setText(constBundle.getString("onlyData"));

    cbOnlySchema.setText(constBundle.getString("onlySchema"));

    cbBlobs.setText("Blobs");

    javax.swing.GroupLayout pnlObjTypeLayout = new javax.swing.GroupLayout(pnlObjType);
    pnlObjType.setLayout(pnlObjTypeLayout);
    pnlObjTypeLayout.setHorizontalGroup(
        pnlObjTypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlObjTypeLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlObjTypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbOnlyData)
                .addComponent(cbOnlySchema)
                .addComponent(cbBlobs))
            .addContainerGap(392, Short.MAX_VALUE))
    );
    pnlObjTypeLayout.setVerticalGroup(
        pnlObjTypeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlObjTypeLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbOnlyData)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbOnlySchema)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbBlobs)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    pnlNotSave.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("notSave")));

    cbOwner.setText(constBundle.getString("owner"));

    cbPrivilege.setText(constBundle.getString("privileges"));

    cbTablespace.setText(constBundle.getString("tablespace"));

    cbUnloggedTableData.setText(constBundle.getString("unloggedTableData"));
    cbUnloggedTableData.setEnabled(false);

    javax.swing.GroupLayout pnlNotSaveLayout = new javax.swing.GroupLayout(pnlNotSave);
    pnlNotSave.setLayout(pnlNotSaveLayout);
    pnlNotSaveLayout.setHorizontalGroup(
        pnlNotSaveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlNotSaveLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlNotSaveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbOwner)
                .addComponent(cbPrivilege)
                .addComponent(cbTablespace)
                .addComponent(cbUnloggedTableData))
            .addContainerGap(392, Short.MAX_VALUE))
    );
    pnlNotSaveLayout.setVerticalGroup(
        pnlNotSaveLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlNotSaveLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbOwner)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbPrivilege)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbTablespace)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbUnloggedTableData)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlDumpOption1Layout = new javax.swing.GroupLayout(pnlDumpOption1);
    pnlDumpOption1.setLayout(pnlDumpOption1Layout);
    pnlDumpOption1Layout.setHorizontalGroup(
        pnlDumpOption1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDumpOption1Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDumpOption1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlSection, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlObjType, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlNotSave, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );
    pnlDumpOption1Layout.setVerticalGroup(
        pnlDumpOption1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDumpOption1Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(pnlSection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(pnlObjType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(pnlNotSave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(10, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("dumpOption1"), pnlDumpOption1);
    pnlDumpOption1.getAccessibleContext().setAccessibleName("constBundle.getString(\"authority\")");

    pnlDumpOption2.setMinimumSize(new java.awt.Dimension(520, 470));

    pnlQuery.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("query1")));

    cbIncludeCreateDB.setText(constBundle.getString("includeCreateDB"));

    cbIncludeDropDB.setText(constBundle.getString("includeDropDB"));

    cbUseColumnInsert.setText(constBundle.getString("useColumnInsert"));

    cbUseInsertCommand.setText(constBundle.getString("useInsertCommand"));

    javax.swing.GroupLayout pnlQueryLayout = new javax.swing.GroupLayout(pnlQuery);
    pnlQuery.setLayout(pnlQueryLayout);
    pnlQueryLayout.setHorizontalGroup(
        pnlQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlQueryLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbIncludeCreateDB)
                .addComponent(cbIncludeDropDB)
                .addComponent(cbUseColumnInsert)
                .addComponent(cbUseInsertCommand))
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    pnlQueryLayout.setVerticalGroup(
        pnlQueryLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlQueryLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbIncludeCreateDB)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbIncludeDropDB)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbUseColumnInsert)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbUseInsertCommand)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    pnlDisable.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("disable")));

    cbTrigger.setText(constBundle.getString("triggers"));

    cbDollarQuoting.setText(constBundle.getString("dollarQuoting"));

    javax.swing.GroupLayout pnlDisableLayout = new javax.swing.GroupLayout(pnlDisable);
    pnlDisable.setLayout(pnlDisableLayout);
    pnlDisableLayout.setHorizontalGroup(
        pnlDisableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDisableLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDisableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbTrigger)
                .addComponent(cbDollarQuoting))
            .addContainerGap(392, Short.MAX_VALUE))
    );
    pnlDisableLayout.setVerticalGroup(
        pnlDisableLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDisableLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbTrigger)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbDollarQuoting)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    pnlMiscellanous.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("miscellanous")));

    cbUseSetSession.setText(constBundle.getString("useSetSessionAuthorization"));

    cbWithOids.setText(constBundle.getString("withOids"));

    cbVerboseMsg.setSelected(true);
    cbVerboseMsg.setText(constBundle.getString("verboseMsg"));

    cbForceDoubleQuote.setText(constBundle.getString("forceDoubleQuoteOnIdentifier"));
    cbForceDoubleQuote.setEnabled(false);

    javax.swing.GroupLayout pnlMiscellanousLayout = new javax.swing.GroupLayout(pnlMiscellanous);
    pnlMiscellanous.setLayout(pnlMiscellanousLayout);
    pnlMiscellanousLayout.setHorizontalGroup(
        pnlMiscellanousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlMiscellanousLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlMiscellanousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(cbUseSetSession)
                .addComponent(cbWithOids)
                .addComponent(cbVerboseMsg)
                .addComponent(cbForceDoubleQuote))
            .addContainerGap(392, Short.MAX_VALUE))
    );
    pnlMiscellanousLayout.setVerticalGroup(
        pnlMiscellanousLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlMiscellanousLayout.createSequentialGroup()
            .addContainerGap()
            .addComponent(cbUseSetSession)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbWithOids)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbVerboseMsg)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(cbForceDoubleQuote)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout pnlDumpOption2Layout = new javax.swing.GroupLayout(pnlDumpOption2);
    pnlDumpOption2.setLayout(pnlDumpOption2Layout);
    pnlDumpOption2Layout.setHorizontalGroup(
        pnlDumpOption2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDumpOption2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(pnlDumpOption2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(pnlQuery, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlDisable, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pnlMiscellanous, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addContainerGap())
    );
    pnlDumpOption2Layout.setVerticalGroup(
        pnlDumpOption2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlDumpOption2Layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(pnlQuery, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(pnlDisable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(pnlMiscellanous, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(10, Short.MAX_VALUE))
    );

    jtp.addTab(constBundle.getString("dumpOption2"), pnlDumpOption2);

    pnlObject.setMinimumSize(new java.awt.Dimension(520, 470));

    javax.swing.GroupLayout pnlObjectLayout = new javax.swing.GroupLayout(pnlObject);
    pnlObject.setLayout(pnlObjectLayout);
    pnlObjectLayout.setHorizontalGroup(
        pnlObjectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 520, Short.MAX_VALUE)
    );
    pnlObjectLayout.setVerticalGroup(
        pnlObjectLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGap(0, 470, Short.MAX_VALUE)
    );

    jtp.addTab(constBundle.getString("object"), pnlObject);

    pnlMsg.setMinimumSize(new java.awt.Dimension(520, 470));
    pnlMsg.setPreferredSize(new java.awt.Dimension(520, 470));

    spMsg.setViewportView(tpMsg);

    javax.swing.GroupLayout pnlMsgLayout = new javax.swing.GroupLayout(pnlMsg);
    pnlMsg.setLayout(pnlMsgLayout);
    pnlMsgLayout.setHorizontalGroup(
        pnlMsgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(spMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 515, Short.MAX_VALUE)
    );
    pnlMsgLayout.setVerticalGroup(
        pnlMsgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(spMsg, javax.swing.GroupLayout.DEFAULT_SIZE, 391, Short.MAX_VALUE)
    );

    jtp.addTab(constBundle.getString("msg"), pnlMsg);
    pnlMsg.getAccessibleContext().setAccessibleName("SQL");

    btnHelp.setText(constBundle.getString("help"));
    btnHelp.setMaximumSize(new java.awt.Dimension(80, 23));
    btnHelp.setMinimumSize(new java.awt.Dimension(80, 23));
    btnHelp.setPreferredSize(new java.awt.Dimension(80, 23));

    btnOK.setText(constBundle.getString("ok"));
    btnOK.setEnabled(false);
    btnOK.setMaximumSize(new java.awt.Dimension(80, 23));
    btnOK.setMinimumSize(new java.awt.Dimension(80, 23));
    btnOK.setPreferredSize(new java.awt.Dimension(80, 23));

    btnCancle.setText(constBundle.getString("cancle"));
    btnCancle.setMaximumSize(new java.awt.Dimension(80, 23));
    btnCancle.setMinimumSize(new java.awt.Dimension(80, 23));
    btnCancle.setPreferredSize(new java.awt.Dimension(80, 23));

    javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
    pnlButton.setLayout(pnlButtonLayout);
    pnlButtonLayout.setHorizontalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(pnlButtonLayout.createSequentialGroup()
            .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );
    pnlButtonLayout.setVerticalGroup(
        pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
            .addGap(10, 10, 10)
            .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(btnHelp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnOK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(btnCancle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addComponent(jtp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        .addComponent(pnlButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addComponent(jtp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGap(0, 0, Short.MAX_VALUE)
            .addComponent(pnlButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
    );

    jtp.getAccessibleContext().setAccessibleName("SQL");

    pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnHelpActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        HtmlPageHelper.getInstance().showPgHelpPage(this, "app-pgdump.html");
    }  
    private void txfdFileNameKeyTyped(KeyEvent evt)
    {
        logger.info(txfdFileName.getText());
        if (!txfdFileName.getText().isEmpty())
        {
            btnOK.setEnabled(true);
        } else
        {
            btnOK.setEnabled(false);
        }
    }
    private void btnChooseFileActionPerformed(ActionEvent evt)
    {
        FileChooserDialog chooser = new FileChooserDialog();
        int response = chooser.showFileChooser(this, constBundle.getString("selectOutputFile"), null);
        if (response == JFileChooser.APPROVE_OPTION)
        {
            txfdFileName.setText(chooser.getChoosedFileDir());
            btnOK.setEnabled(true);
        }
    }
    private void cbFormatItemStateChanged(ItemEvent evt)
    {
        if (cbFormat.getSelectedIndex() < 0)
        {
            return;
        }

        if (cbFormat.getSelectedItem().equals("Tar"))
        {
            txfdCompressRatio.setEnabled(false);
            txfdCompressRatio.setText("");
        } else
        {
            txfdCompressRatio.setEnabled(true);
        }
        txfdJobNumber.setEditable(txfdJobNumber.isEnabled() && cbFormat.getSelectedItem().equals(constBundle.getString("directory")));
    }
    private void selectedActionPerformed(ActionEvent evt)
    {
        JCheckBox cbox = (JCheckBox) evt.getSource();
        boolean isSelect = cbox.isSelected();
        logger.info(cbox.getText() + "," + isSelect);
        if (cbox == cbOnlyData)
        {
            cbOnlySchema.setEnabled(!isSelect);
            cbOnlySchema.setSelected(false);
            if (pnlSection.isEnabled())
            {
                cbPreData.setEnabled(!isSelect);
                cbData.setEnabled(!isSelect);
                cbPostData.setEnabled(!isSelect);
                if (isSelect)
                {
                    cbPreData.setSelected(false);
                    cbData.setSelected(false);
                    cbPostData.setSelected(false);
                }
            }
        } else if (cbox == cbOnlySchema)
        {
            cbOnlyData.setEnabled(!isSelect);
            cbOnlyData.setSelected(false);
            if (pnlSection.isEnabled())
            {
                cbPreData.setEnabled(!isSelect);
                cbData.setEnabled(!isSelect);
                cbPostData.setEnabled(!isSelect);
                if (isSelect)
                {
                    cbPreData.setSelected(false);
                    cbData.setSelected(false);
                    cbPostData.setSelected(false);
                }
            }
        } else if (cbox == cbPreData || cbox == cbData || cbox == cbPostData)
        {
            if (cbPreData.isSelected() || cbData.isSelected() || cbPostData.isSelected())
            {
                cbOnlySchema.setEnabled(false);
                cbOnlyData.setEnabled(false);

                cbOnlySchema.setSelected(false);
                cbOnlyData.setSelected(false);
            } else
            {
                cbOnlySchema.setEnabled(true);
                cbOnlyData.setEnabled(true);
            }
        }
    }
    private void txfdIntegerValueKeyTyped(KeyEvent evt)
    {
        //those textfields could be entered all integer.
        char keyCh = evt.getKeyChar();
        logger.info("keych=" + keyCh + ",isDigit=" + Character.isDigit(keyCh));
        if ((keyCh < '0') || (keyCh > '9'))
        {
            if (keyCh != '' && (keyCh != '-'))
            {
                evt.setKeyChar('\0');
            }
        }
    }
    private void btnCancleActionPerformed(ActionEvent evt)
    {
        this.dispose();
    }
    private void btnOKActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (btnOK.getText().equals(constBundle.getString("finish")))
        {
            this.dispose();
            return;
        }
        logger.info("objType=" + obj.getType() + ",host=" + helperInfo.getHost() + ",port=" + helperInfo.getPort()
                + ",user=" + helperInfo.getUser() + ",db=" + helperInfo.getDbName() + ",file=" + txfdFileName.getText());
        File f = new File(txfdFileName.getText());
        if (f.exists())
        {
            int response = JOptionPane.showConfirmDialog(this, MessageFormat.format(constBundle.getString("confirmSave"),
                    txfdFileName.getText()), constBundle.getString("confirmSaveTitle"), JOptionPane.OK_CANCEL_OPTION);
            if (response == JOptionPane.CANCEL_OPTION)
            {
                return;
            }
        }
        jtp.setSelectedIndex(4);
        try
        {
            StringBuilder cmd = new StringBuilder();
            List<String> cmds = this.getCommandList(cmd);
            logger.info(cmd.toString());
            tpMsg.setText(cmd.toString() + System.lineSeparator());
            Document docs = tpMsg.getDocument();
            logger.debug(cmds);
            BackupController.getInstance().backup(helperInfo, cmds, docs);
            btnOK.setText(constBundle.getString("finish"));
            btnOK.setEnabled(true);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //pg_dump命令中对象名有大写或者关键字时，需要引号引起，pg_restore则不需要引号引起。
    //pg_dump指定表的模式是通过在表名前边添加，如schema1.table1，而pg_restore是通过指定--schema参数来指定表的模式。
    private List<String> getCommandList(StringBuilder cmdStr) throws Exception
    {
        List<String> cmds = new ArrayList<>();
        //connection option
        OptionController oc = OptionController.getInstance();
        String bin = oc.getOptionProperties().getProperty("bin_path");  
        if (bin == null || bin.isEmpty())
        {
            throw new Exception(constBundle.getString("binPathNeedSet"));
        }
        cmds.add(bin + File.separator + "pg_dump");  
        cmds.add("--host");
        cmds.add(helperInfo.getHost());
        cmds.add("--port");
        cmds.add(helperInfo.getPort());
        cmds.add("--username");
        cmds.add(helperInfo.getUser());
        cmdStr.append(bin).append(File.separator).append("pg_dump")
                .append(" --host ").append(helperInfo.getHost())
                .append(" --port ").append(helperInfo.getPort())
                .append(" --username \"").append(helperInfo.getUser()).append("\"");
        if (cbbRoleName.getSelectedIndex() > 0)
        {
            cmds.add("--role");
            cmds.add(cbbRoleName.getSelectedItem().toString());
            cmdStr.append(" --role \"").append(cbbRoleName.getSelectedItem().toString().replaceAll("\"", "")).append("\"");
        }
        //if access is trust or exists  .pgpass file, then user --no-password
        //here we always use temporary environment variable PGPASSWORD to provide a password.
       // cmds.add("--no-password");
       // cmdStr.append(" --no-password");

        //other options
        switch (cbFormat.getSelectedIndex())
        {
            case 0:
                cmds.add("--format");
                cmds.add("custom");
                cmdStr.append(" --format custom");
                break;
            case 1:
                cmds.add("--format");
                cmds.add("tar");
                cmdStr.append(" --format tar");
                break;
            case 2:
                cmds.add("--format");
                cmds.add("plain");
                cmdStr.append(" --format plain");
                break;
            case 3:
                cmds.add("--format");
                cmds.add("directory");
                cmdStr.append(" --format directory"); 
                break;
            default:
                logger.error(cbFormat.getSelectedItem() + " is an exception format, do nothing and break.");
                break;
        }
        if (!txfdCompressRatio.getText().isEmpty())
        {
            cmds.add("--compress=" + txfdCompressRatio.getText());
            cmdStr.append(" --compress=").append(txfdCompressRatio.getText());
        }
        if (cbEncoding.getSelectedIndex() > 0)
        {
            cmds.add("--encoding");
            cmds.add(cbEncoding.getSelectedItem().toString());
            cmdStr.append(" --encoding ").append(cbEncoding.getSelectedItem().toString());
        }
        //section support。=9.2
        if (pnlSection.isEnabled())
        {
            if (cbPreData.isSelected())
            {
                cmds.add("--section");
                cmds.add("pre-data");
                cmdStr.append(" --section pre-data");
            }
            if (cbData.isSelected())
            {
                cmds.add("--section");
                cmds.add("data");
                cmdStr.append(" --section data");
            }
            if (cbPostData.isSelected())
            {
                cmds.add("--section");
                cmds.add("post-data");
                cmdStr.append(" --section post-data");
            }
        }
        //object type
        if (cbOnlyData.isSelected())
        {
            cmds.add("--data-only");
            cmdStr.append(" --data-only");
        }
        if (cbOnlySchema.isSelected())
        {
            cmds.add("--schema-only");
            cmdStr.append(" --schema-only");
        }
        if (cbBlobs.isSelected())
        {
            cmds.add("--blobs");
            cmdStr.append(" --blobs");
        }
        
        //not save
        if (cbOwner.isSelected())
        {
            cmds.add("--no-owner");
            cmdStr.append(" --no-owner");
        }
        if (cbPrivilege.isSelected())
        {
            cmds.add("--no-privileges");
            cmdStr.append(" --no-privileges");
        }
        if (cbTablespace.isSelected())
        {
            cmds.add("--no-tablespaces");
            cmdStr.append(" --no-tablespaces");
        }
        if (cbUnloggedTableData.isSelected())
        {
            cmds.add("--no-unlogged-table-data");
            cmdStr.append(" --no-unlogged-table-data");
        }
        
        //query
        if (cbIncludeCreateDB.isSelected())
        {
            cmds.add("--create");
            cmdStr.append(" --create");
        }
        if (cbIncludeDropDB.isSelected())
        {
            cmds.add("--clean");
            cmdStr.append(" --clean");
        }
        if (cbUseColumnInsert.isSelected())
        {
            cmds.add("--column-inserts");
            cmdStr.append(" --column-inserts");
        }
        if (cbUseInsertCommand.isSelected())
        {
            cmds.add("--inserts");
            cmdStr.append(" --inserts");
        }
        //disable
        if (cbTrigger.isSelected())
        {
            cmds.add("--disable-triggers");
            cmdStr.append(" --disable-triggers");
        }
        if (cbDollarQuoting.isSelected())
        {
            cmds.add("--disable-dollar-quoting");
            cmdStr.append(" --disable-dollar-quoting");
        }
        //Miscellanous
        if (cbUseSetSession.isSelected())
        {
            cmds.add("--use-set-session-authorization");
            cmdStr.append(" --use-set-session-authorization");
        }
        if (cbWithOids.isSelected())
        {
            cmds.add("--oids");
            cmdStr.append(" --oids");
        }
        if (cbVerboseMsg.isSelected())
        {
            cmds.add("--verbose");
            cmdStr.append(" --verbose");
        }
        if (cbForceDoubleQuote.isEnabled() && cbForceDoubleQuote.isSelected())
        {
            cmds.add("--quote-all-identifiers");
            cmdStr.append(" --quote-all-identifiers");
        }

        cmds.add("--file");
        cmds.add(txfdFileName.getText());
        cmdStr.append(" --file \"").append(txfdFileName.getText()).append("\"");

        if (cbFormat.getSelectedIndex() == 3)
        {
            if (!txfdJobNumber.getText().isEmpty())
            {
                cmds.add("--jobs");
                cmds.add(txfdJobNumber.getText());
                cmdStr.append(" --jobs ").append(txfdJobNumber.getText());
            }
        }
       
        //needn't quoted object name when use pg_restore
        //but must quoted object name when use pg_dump
        switch (obj.getType())
        {
            case DATABASE:
                cmds.add(obj.getName());
                cmdStr.append(" \"").append(obj.getName()).append("\"");
                break;
            case SCHEMA:                
                cmds.add("--" + obj.getType().toString().toLowerCase());
                cmds.add(this.getName(obj.getName()));
                cmds.add(helperInfo.getDbName());

                cmdStr.append(" --").append(obj.getType().toString().toLowerCase())
                        .append(" \"").append(this.getName(obj.getName())).append("\"")
                        .append(" \"").append(helperInfo.getDbName()).append("\"");
                break;
            case TABLE:                
                cmds.add("--" + obj.getType().toString().toLowerCase());
                cmds.add(this.getName(helperInfo.getSchema()) + "." + this.getName(obj.getName()));
                cmds.add(helperInfo.getDbName());
                
                cmdStr.append(" --").append(obj.getType().toString().toLowerCase())
                        .append(" \"").append(this.getName(helperInfo.getSchema()) + "." + this.getName(obj.getName())).append("\"")
                        .append(" \"").append(helperInfo.getDbName()).append("\"");
                break;
            default:
                logger.error(obj.getType() + " is an exception type, do nothing and break.");
                break;
        }
        return cmds;
    }
    
     private String getName(String name)
    {
        String thename = SyntaxController.getInstance().getName(name);
        if (thename.startsWith("\"") && thename.endsWith("\""))
        {
            thename = "\\" + thename.substring(0, thename.length()-1) + "\\"+"\"";
        }
        return thename;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnChooseFile;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnOK;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cbBlobs;
    private javax.swing.JCheckBox cbData;
    private javax.swing.JCheckBox cbDollarQuoting;
    private javax.swing.JComboBox cbEncoding;
    private javax.swing.JCheckBox cbForceDoubleQuote;
    private javax.swing.JComboBox cbFormat;
    private javax.swing.JCheckBox cbIncludeCreateDB;
    private javax.swing.JCheckBox cbIncludeDropDB;
    private javax.swing.JCheckBox cbOnlyData;
    private javax.swing.JCheckBox cbOnlySchema;
    private javax.swing.JCheckBox cbOwner;
    private javax.swing.JCheckBox cbPostData;
    private javax.swing.JCheckBox cbPreData;
    private javax.swing.JCheckBox cbPrivilege;
    private javax.swing.JCheckBox cbTablespace;
    private javax.swing.JCheckBox cbTrigger;
    private javax.swing.JCheckBox cbUnloggedTableData;
    private javax.swing.JCheckBox cbUseColumnInsert;
    private javax.swing.JCheckBox cbUseInsertCommand;
    private javax.swing.JCheckBox cbUseSetSession;
    private javax.swing.JCheckBox cbVerboseMsg;
    private javax.swing.JCheckBox cbWithOids;
    private javax.swing.JComboBox cbbRoleName;
    private javax.swing.JTabbedPane jtp;
    private javax.swing.JLabel lblCompressRatio;
    private javax.swing.JLabel lblEncoding;
    private javax.swing.JLabel lblFileName;
    private javax.swing.JLabel lblFormat;
    private javax.swing.JLabel lblJobNumber;
    private javax.swing.JLabel lblRoleName;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlDisable;
    private javax.swing.JPanel pnlDumpOption1;
    private javax.swing.JPanel pnlDumpOption2;
    private javax.swing.JPanel pnlFiltOption;
    private javax.swing.JPanel pnlMiscellanous;
    private javax.swing.JPanel pnlMsg;
    private javax.swing.JPanel pnlNotSave;
    private javax.swing.JPanel pnlObjType;
    private javax.swing.JPanel pnlObject;
    private javax.swing.JPanel pnlQuery;
    private javax.swing.JPanel pnlSection;
    private javax.swing.JScrollPane spMsg;
    private javax.swing.JTextPane tpMsg;
    private javax.swing.JTextField txfdCompressRatio;
    private javax.swing.JTextField txfdFileName;
    private javax.swing.JTextField txfdJobNumber;
    // End of variables declaration//GEN-END:variables
}
