/* ------------------------------------------------
 *
 * File: FunObjInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\FunObjInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * this is only for function object short information .
 * because function need not only schema and name, but also IN argument oid list,to distinguish.
 * 
 * @author Yuanyuan
 * function use schema, name and in arguments to distinguish
 * 
 */
public class FunObjInfoDTO extends ObjInfoDTO
{   
    //private String fullname;//name with in arg
    private String inArgTypes;//comma seperated string
    private String inArgTypeOids;//space seperated string

    @Override
    public String toString()
    {
        //return fullname;
        if (inArgTypes == null || inArgTypes.isEmpty())
        {
            return this.getName() + "()";
        } else
        {
            return this.getName() + "(" + inArgTypes + ")";
        }
    }
  
    
    public String getInArgTypes()
    {
        return inArgTypes;
    }
    public void setInArgTypes(String inArgTypes)
    {
        this.inArgTypes = inArgTypes;
    }

    public String getInArgTypeOids()
    {
        if (this.inArgTypeOids == null)
        {
            return "";
        } else
        {
            return this.inArgTypeOids;
        }
    }
    public void setInArgTypeOids(String inArgTypeOids)
    {
        this.inArgTypeOids = inArgTypeOids;
    } 
    
    
    
}
