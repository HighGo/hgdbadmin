/* ------------------------------------------------
 *
 * File: ColumnInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ColumnInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class ColumnInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;     
    private String name;
    private int position;
    private DatatypeDTO datatype;
    private String comment;      
    private String defaultValue;
    private String sequence;//?
    private boolean notNull;
    private boolean pk;
    private boolean unique;
    private boolean fk;
    private String storage;
    private boolean isHerited;
    private String statistics;
    private String acl;
     
    //for creation    
    private String collation;
    private String inheritedTable;
    private List<VariableInfoDTO> variableInfoList;
    private List<SecurityLabelInfoDTO> securityLabelInfoList;
    //for constraint creation 
    private String order;
    private String nullsOrder;
    private String operatorClass;
    private String operator;
   
    @Override
    public String getOwner()
    {
        return  null;
    }
    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.COLUMN;
    }
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }  
    
    @Override
    public Long getOid()
    {
        return Long.valueOf(position);
    }
    public int getPosition()
    {
        return position;
    }
    public void setPosition(int position)
    {
        this.position = position;
    }

    public DatatypeDTO getDatatype()
    {
        return datatype;
    }
    public void setDatatype(DatatypeDTO datatype)
    {
        this.datatype = datatype;
    }
    
    public String getDefaultValue()
    {
        return defaultValue;
    }
    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getStorage()
    {
        if(storage==null)
        {
            storage="";
        }
        return storage;
    }
    public void setStorage(String storage)
    {
        this.storage = storage;
    }
    
    public String getCollation()
    {
        if (collation == null)
        {
            collation = "";
        }
        return collation;
    }
    public void setCollation(String collation)
    {
        this.collation = collation;
    }
    
    public String getStatistics()
    {
        if(statistics==null)
        {
            statistics="";
        }
        return statistics;
    }
    public void setStatistics(String statistics)
    {
        this.statistics = statistics;
    }
    
    public String getSequence()
    {
        return sequence;
    }
    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }

    public boolean isNotNull()
    {
        return notNull;
    }
    public void setNotNull(boolean notNull)
    {
        this.notNull = notNull;
    }

    public boolean isPk()
    {
        return pk;
    }
    public void setPk(boolean pk)
    {
        this.pk = pk;
    }

    public boolean isUnique()
    {
        return unique;
    }
    public void setUnique(boolean unique)
    {
        this.unique = unique;
    }

    public boolean isFk()
    {
        return fk;
    }
    public void setFk(boolean fk)
    {
        this.fk = fk;
    }

    public boolean isInherit()
    {
        return isHerited;
    }
    public void setInherit(boolean isHerited)
    {
        this.isHerited = isHerited;
    }
    
    @Override
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    } 

    public String getInheritedTable()
    {
        return inheritedTable;
    }

    public void setInheritedTable(String inheritedTable)
    {
        this.inheritedTable = inheritedTable;
    }

    public List<VariableInfoDTO> getVariableInfoList()
    {
        return variableInfoList;
    }

    public void setVariableInfoList(List<VariableInfoDTO> variableInfoList)
    {
        this.variableInfoList = variableInfoList;
    }

    public List<SecurityLabelInfoDTO> getSecurityLabelInfoList()
    {
        return securityLabelInfoList;
    }

    public void setSecurityLabelInfoList(List<SecurityLabelInfoDTO> securityLabelInfoList)
    {
        this.securityLabelInfoList = securityLabelInfoList;
    }

    public String getOrder()
    {
        return order;
    }

    public void setOrder(String order)
    {
        this.order = order;
    }

    public String getNullsOrder()
    {
        return nullsOrder;
    }

    public void setNullsOrder(String nullsOrder)
    {
        this.nullsOrder = nullsOrder;
    }

    public String getOperatorClass()
    {
        return operatorClass;
    }

    public void setOperatorClass(String operatorClass)
    {
        this.operatorClass = operatorClass;
    }

    public String getOperator()
    {
        return operator;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }
    
    @Override
    public boolean isSystem()
    {
        return helperInfo.getRelationOid() < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
