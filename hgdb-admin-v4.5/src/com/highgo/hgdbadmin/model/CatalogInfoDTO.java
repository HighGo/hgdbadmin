/* ------------------------------------------------
 *
 * File: CatalogInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\CatalogInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Liu Yuanyuan
 */
public class CatalogInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;

    private String name;
    private Long oid;
    private String owner;
    private String acl;
    private String defaultTableAcl;
    private String defaultSequenceAcl;
    private String defaultFunctionAcl;
    private String defaultTypeAcl;
    private String comment;

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.CATALOG;
    }

    @Override
    public boolean isSystem()
    {
        return true;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public String getDefaultTableAcl()
    {
        return defaultTableAcl;
    }

    public void setDefaultTableAcl(String defaultTableAcl)
    {
        this.defaultTableAcl = defaultTableAcl;
    }

    public String getDefaultSequenceAcl()
    {
        return defaultSequenceAcl;
    }

    public void setDefaultSequenceAcl(String defaultSequenceAcl)
    {
        this.defaultSequenceAcl = defaultSequenceAcl;
    }

    public String getDefaultFunctionAcl()
    {
        return defaultFunctionAcl;
    }

    public void setDefaultFunctionAcl(String defaultFunctionAcl)
    {
        this.defaultFunctionAcl = defaultFunctionAcl;
    }

    public String getDefaultTypeAcl()
    {
        return defaultTypeAcl;
    }

    public void setDefaultTypeAcl(String defaultTypeAcl)
    {
        this.defaultTypeAcl = defaultTypeAcl;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

}
