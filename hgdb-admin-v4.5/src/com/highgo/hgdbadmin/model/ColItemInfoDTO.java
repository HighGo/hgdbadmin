/* ------------------------------------------------
 *
 * File: ColItemInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ColItemInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 */
public class ColItemInfoDTO
{

    private String name;
    private Long typeOid;

    @Override
    public String toString()
    {
        return this.name;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getTypeOid()
    {
        return typeOid;
    }

    public void setTypeOid(Long typeOid)
    {
        this.typeOid = typeOid;
    }
    
}
