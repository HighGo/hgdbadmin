/* ------------------------------------------------
 *
 * File: RoleInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\RoleInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class RoleInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
       
    private String name;
    private long oid;    
    private boolean isEncrypted;
    private String pwd;//for creation
    private String accountExpires;
    private String connectionLimit; 
    
    private boolean canLogin;
    private boolean canInherits;
    private boolean superuser;
    private boolean canCreateDB;
    private boolean canCreateRoles;
    //ygq v5 admin del start
    //private boolean canModifyCatalog;
    //ygq v5 admin del end
    private boolean canInitStreamReplicationAndBackup;//for creation    
    private String comment; 
    
    private List<String> memeberOf;//this maybe on eor more    
    private boolean withAdminOption;
    private List<VariableInfoDTO> variableList;
    private List<SecurityLabelInfoDTO> securityLabelList;

    private boolean displaySQL = false;//displaySQL=true then pwd use *******
    
   @Override
    public String getOwner()
    {
        return null;
    }
    @Override
     public TreeEnum.TreeNode getType()
    {        
        if(canLogin)
        {
           return TreeEnum.TreeNode.LOGINROLE; 
        }else
        {
            return TreeEnum.TreeNode.GROUPROLE;
        }
    }
 
    @Override
    public boolean isSystem()
    {
        //return this.oid < helperInfo.getDatLastSysOid();
        return false;
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }
    
    public String getAccountExpires()
    {
        return accountExpires;
    }

    public void setAccountExpires(String accountExpires)
    {
        this.accountExpires = accountExpires;
    }

    public boolean isCanLogin()
    {
        return canLogin;
    }

    public void setCanLogin(boolean canLogin)
    {
        this.canLogin = canLogin;
    }

    public boolean isSuperuser()
    {
        return superuser;
    }

    public void setSuperuser(boolean superuser)
    {
        this.superuser = superuser;
    }

    public boolean isCanCreateDB()
    {
        return canCreateDB;
    }

    public void setCanCreateDB(boolean canCreateDB)
    {
        this.canCreateDB = canCreateDB;
    }

    public boolean isCanCreateRoles()
    {
        return canCreateRoles;
    }

    public void setCanCreateRoles(boolean createRoles)
    {
        this.canCreateRoles = createRoles;
    }

    //ygq v5 admin del start
    //public boolean isCanModifyCatalog()
    //{
    //    return canModifyCatalog;
    //}

    //public void setCanModifyCatalog(boolean canModifyCatalog)
    //{
    //    this.canModifyCatalog = canModifyCatalog;
    //}
    //ygq v5 admin del end

    public boolean isCanInherits()
    {
        return canInherits;
    }

    public void setCanInherits(boolean canInherits)
    {
        this.canInherits = canInherits;
    }

    public String getConnectionLimit()
    {
        return connectionLimit;
    }

    public void setConnectionLimit(String connectionLimit)
    {
        this.connectionLimit = connectionLimit;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public List<String> getMemeberOf()
    {
        return memeberOf;
    }

    public boolean isWithAdminOption()
    {
        return withAdminOption;
    }

    public void setWithAdminOption(boolean withAdminOption)
    {
        this.withAdminOption = withAdminOption;
    }

    public void setMemeberOf(List<String> memeberOf)
    {
        this.memeberOf = memeberOf;
    }

    public String toMemberOf()
    {
        StringBuilder owners = new StringBuilder();
        int size = this.memeberOf.size();
        for (int i = 0; i < size; i++)
        {
            if (i > 0)
            {
                owners.append(",");
            }
            owners.append(memeberOf.get(i).replace("(*)", ""));
        }
        return owners.toString();
    }
    
    public List<VariableInfoDTO> getVariableList()
    {
        return variableList;
    }

    public void setVariableList(List<VariableInfoDTO> variableList)
    {
        this.variableList = variableList;
    }

    public List<SecurityLabelInfoDTO> getSecurityLabelList()
    {
        return securityLabelList;
    }

    public void setSecurityLabelList(List<SecurityLabelInfoDTO> securityLabelList)
    {
        this.securityLabelList = securityLabelList;
    }

    public boolean isEncrypted()
    {
        return isEncrypted;
    }

    public void setEncrypted(boolean isEncrypted)
    {
        this.isEncrypted = isEncrypted;
    }
    
    public String getPwd()
    {
        return pwd;
    }

    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }

    public boolean isCanInitStreamReplicationAndBackup()
    {
        return canInitStreamReplicationAndBackup;
    }

    public void setCanInitStreamReplicationAndBackup(boolean canInitStreamReplicationAndBackup)
    {
        this.canInitStreamReplicationAndBackup = canInitStreamReplicationAndBackup;
    }

    public boolean isDisplaySQL()
    {
        return displaySQL;
    }

    public void setDisplaySQL(boolean displaySQL)
    {
        this.displaySQL = displaySQL;
    }
    
}
