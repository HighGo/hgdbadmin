/* ------------------------------------------------
 *
 * File: ColInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ColInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.controller.SyntaxController;
/**
 *FOR column of index or constraint
 * @author Yuanyuan
 */
public class ColInfoDTO
{
    private int position;
    
    private String name;
    private String collation;
    private String operatorClass;
    private boolean defaultOpClass;
    private String order;
    private String nullsWhen;

    private String operator;//for Exclude constraint
    
    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }
    
    
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCollation()
    {
        return collation;
    }

    public void setCollation(String collation)
    {
        this.collation = collation;
    }

    public String getOperatorClass()
    {
        return operatorClass;
    }

    public void setOperatorClass(String operatorClass)
    {
        this.operatorClass = operatorClass;
    }

    public boolean isDefaultOpClass()
    {
        return defaultOpClass;
    }

    public void setDefaultOpClass(boolean defaultOpClass)
    {
        this.defaultOpClass = defaultOpClass;
    }

    public String getOrder()
    {
        return order;
    }

    public void setOrder(String order)
    {
        this.order = order;
    }

    public String getNullsWhen()
    {
        return nullsWhen;
    }

    public void setNullsWhen(String nullsWhen)
    {
        this.nullsWhen = nullsWhen;
    }

    public String toColumnDefine()
    {
        StringBuilder columnDefine = new StringBuilder();
        columnDefine.append(SyntaxController.getInstance().getName(this.name));
        if (this.collation != null && !this.collation.isEmpty())
        {
            columnDefine.append(" COLLATE ").append(this.collation);
        }
        if (!this.defaultOpClass && this.operatorClass != null && !this.operatorClass.isEmpty())
        {
            columnDefine.append(" ").append(this.operatorClass);
        }
        if (this.order != null && !this.order.isEmpty())
        {
            columnDefine.append(" ").append(order);
        }
        if (this.nullsWhen != null && !this.nullsWhen.isEmpty())
        {
            columnDefine.append(" NULLS ").append(nullsWhen);
        }
        if(this.operator!= null && !this.operator.isEmpty())
        {
            columnDefine.append(" WITH ").append(operator);
        }
        return columnDefine.toString();
    }

    public String getOperator()
    {
        return operator;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }
    
    
    
    
}
