/* ------------------------------------------------
 *
 * File: ObjItemInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ObjItemInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 * For parent table item in object creation 
 * 
 */
public class ObjItemInfoDTO
{
    private String schema;
    private String name;
    private long oid;

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public String getTable()
    {
        return name;
    }

    public void setTable(String name)
    {
        this.name = name;
    }

    public long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }
    
    @Override
    public String toString()
    {
        //SyntaxController sc = SyntaxController.getInstance();
        //return sc.getName(schema) + "." + sc.getName(name);
        return this.schema+"."+this.name;
    }

}
