/* ------------------------------------------------
 *
 * File: PrivilegeInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\PrivilegeInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 */
public class PrivilegeInfoDTO
{
    private String userAndGroup;
    private String privileges;

    public String getUserAndGroup()
    {
        return userAndGroup;
    }

    public void setUserAndGroup(String userAndGroup)
    {
        this.userAndGroup = userAndGroup;
    }

    public String getPrivileges()
    {
        return privileges;
    }

    public void setPrivileges(String privileges)
    {
        this.privileges = privileges;
    }
    
}
