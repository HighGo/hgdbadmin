/* ------------------------------------------------
 *
 * File: AutoVacuumDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\AutoVacuumDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 */
public class AutoVacuumDTO
{
    private boolean customAutoVacuum;
    private boolean enable;
    private String vacuumBaseThreshold;
    private String analyzeBaseThreshold;
    private String analyzeScaleFactor;
    private String vacuumCostDelay;
    private String vacuumCostLimit;
    private String vacuumScaleFactor;
    private String freezeMinimumAge;
    private String freezeMaximumAge;
    private String freezeTableAge;
    
    private boolean customAutoVacuumToast;
    private boolean enableToast;
    private String vacuumBaseThresholdToast;
    private String vacuumScaleFactorToast;
    private String vacuumCostDelayToast;
    private String vacuumCostLimitToast;
    private String freezeMinimumAgeToast;
    private String freezeMaximumAgeToast;
    private String freezeTableAgeToast;

    public boolean isCustomAutoVacuum()
    {
        return customAutoVacuum;
    }

    public void setCustomAutoVacuum(boolean customAutoVacuum)
    {
        this.customAutoVacuum = customAutoVacuum;
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }

    public String getVacuumBaseThreshold()
    {
        return vacuumBaseThreshold;
    }

    public void setVacuumBaseThreshold(String vacuumBaseThreshold)
    {
        this.vacuumBaseThreshold = vacuumBaseThreshold;
    }

    public String getAnalyzeBaseThreshold()
    {
        return analyzeBaseThreshold;
    }

    public void setAnalyzeBaseThreshold(String analyzeBaseThreshold)
    {
        this.analyzeBaseThreshold = analyzeBaseThreshold;
    }

    public String getAnalyzeScaleFactor()
    {
        return analyzeScaleFactor;
    }

    public void setAnalyzeScaleFactor(String analyzeScaleFactor)
    {
        this.analyzeScaleFactor = analyzeScaleFactor;
    }

    public String getVacuumCostDelay()
    {
        return vacuumCostDelay;
    }

    public void setVacuumCostDelay(String vacuumCostDelay)
    {
        this.vacuumCostDelay = vacuumCostDelay;
    }

    public String getVacuumCostLimit()
    {
        return vacuumCostLimit;
    }

    public void setVacuumCostLimit(String vacuumCostLimit)
    {
        this.vacuumCostLimit = vacuumCostLimit;
    }

    public String getVacuumScaleFactor()
    {
        return vacuumScaleFactor;
    }

    public void setVacuumScaleFactor(String vacuumScaleFactor)
    {
        this.vacuumScaleFactor = vacuumScaleFactor;
    }

    public String getFreezeMinimumAge()
    {
        return freezeMinimumAge;
    }

    public void setFreezeMinimumAge(String freezeMinimumAge)
    {
        this.freezeMinimumAge = freezeMinimumAge;
    }

    public String getFreezeMaximumAge()
    {
        return freezeMaximumAge;
    }

    public void setFreezeMaximumAge(String freezeMaximumAge)
    {
        this.freezeMaximumAge = freezeMaximumAge;
    }

    public String getFreezeTableAge()
    {
        return freezeTableAge;
    }

    public void setFreezeTableAge(String freezeTableAge)
    {
        this.freezeTableAge = freezeTableAge;
    }

    public boolean isCustomAutoVacuumToast()
    {
        return customAutoVacuumToast;
    }

    public void setCustomAutoVacuumToast(boolean customAutoVacuumToast)
    {
        this.customAutoVacuumToast = customAutoVacuumToast;
    }

    public boolean isEnableToast()
    {
        return enableToast;
    }

    public void setEnableToast(boolean enableToast)
    {
        this.enableToast = enableToast;
    }

    public String getVacuumBaseThresholdToast()
    {
        return vacuumBaseThresholdToast;
    }

    public void setVacuumBaseThresholdToast(String vacuumBaseThresholdToast)
    {
        this.vacuumBaseThresholdToast = vacuumBaseThresholdToast;
    }

    public String getVacuumScaleFactorToast()
    {
        return vacuumScaleFactorToast;
    }

    public void setVacuumScaleFactorToast(String vacuumScaleFactorToast)
    {
        this.vacuumScaleFactorToast = vacuumScaleFactorToast;
    }

    public String getVacuumCostDelayToast()
    {
        return vacuumCostDelayToast;
    }

    public void setVacuumCostDelayToast(String vacuumCostDelayToast)
    {
        this.vacuumCostDelayToast = vacuumCostDelayToast;
    }

    public String getVacuumCostLimitToast()
    {
        return vacuumCostLimitToast;
    }

    public void setVacuumCostLimitToast(String vacuumCostLimitToast)
    {
        this.vacuumCostLimitToast = vacuumCostLimitToast;
    }

    public String getFreezeMinimumAgeToast()
    {
        return freezeMinimumAgeToast;
    }

    public void setFreezeMinimumAgeToast(String freezeMinimumAgeToast)
    {
        this.freezeMinimumAgeToast = freezeMinimumAgeToast;
    }

    public String getFreezeMaximumAgeToast()
    {
        return freezeMaximumAgeToast;
    }

    public void setFreezeMaximumAgeToast(String freezeMaximumAgeToast)
    {
        this.freezeMaximumAgeToast = freezeMaximumAgeToast;
    }

    public String getFreezeTableAgeToast()
    {
        return freezeTableAgeToast;
    }

    public void setFreezeTableAgeToast(String freezeTableAgeToast)
    {
        this.freezeTableAgeToast = freezeTableAgeToast;
    }
    
        
}
