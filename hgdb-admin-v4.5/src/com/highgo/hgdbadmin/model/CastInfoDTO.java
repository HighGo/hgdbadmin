/* ------------------------------------------------
 *
 * File: CastInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\CastInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Liu Yuanyuan
 */
public class CastInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;//no use now
    private String name;
    private Long oid;
    private String sourceType;
    private String targetType;
    private String function;
    private String context;
    private String comment;

    
    @Override
    public String getOwner()
    {
        return null;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.CAST;
    }
    
    @Override
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }
    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    public String getSourceType()
    {
        return sourceType;
    }
    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }

    public String getTargetType()
    {
        return targetType;
    }
    public void setTargetType(String targetType)
    {
        this.targetType = targetType;
    }

    public String getFunction()
    {
        return function;
    }
    public void setFunction(String function)
    {
        this.function = function;
    }

    public String getContext()
    {
        return context;
    }
    public void setContext(String context)
    {
        this.context = context;
    }

    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
