/* ------------------------------------------------
 *
 * File: FunctionInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\FunctionInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 * 
 * @author Yuanyuan
 */
public class FunctionInfoDTO extends ProcedureInfoDTO
{
    private TreeEnum.TreeNode type = TreeEnum.TreeNode.FUNCTION;
    
    private String returnType;
    //option
    private String volatiles;
    private Boolean returnSet;
    //private boolean securityDefiner;
    private boolean strict;
    private boolean window;
    private boolean leafProof;
    private int cost;
    private String parallel;

    public void setType(TreeEnum.TreeNode type)
    {
        this.type = type;
    }
    @Override
    public TreeEnum.TreeNode getType()
    {
        return type;
    }
    
    public String printDebug()
    {
        return returnType + "-" + volatiles + "-" + strict + "-" + window + "-" + leafProof + "-" + cost + "-" + parallel;
    }

    public String getVolatiles()
    {
        return volatiles;
    }
    public void setVolatiles(String volatiles)
    {
        this.volatiles = volatiles;
    }
    
    public String getReturnType()
    {
        return returnType;
    }
    public void setReturnType(String returnType)
    {
        this.returnType = returnType;
    }

    public Boolean isReturnSet()
    {
        return returnSet;
    }
    public void setReturnSet(Boolean returnSet)
    {
        this.returnSet = returnSet;
    }
    
    public boolean isStrict()
    {
        return strict;
    }
    public void setStrict(boolean strict)
    {
        this.strict = strict;
    }

    public boolean isWindow()
    {
        return window;
    }
    public void setWindow(boolean window)
    {
        this.window = window;
    }

    public boolean isLeafProof()
    {
        return leafProof;
    }
    public void setLeafProof(boolean leafProof)
    {
        this.leafProof = leafProof;
    }

    public int getCost()
    {
        return cost;
    }

    public void setCost(int cost)
    {
        this.cost = cost;
    }
    
    public String getParallel()
    {
        return parallel;
    }
    public void setParallel(String parallel)
    {
        this.parallel = parallel;
    }

    
}
