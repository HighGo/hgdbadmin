/* ------------------------------------------------
 *
 * File: SecurityLabelInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\SecurityLabelInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

/**
 * 
 * @author Yuanyuan
 * for database creation
 */
public class SecurityLabelInfoDTO
{
    private String provider;
    private String securityLabel;

    public String getProvider()
    {
        return provider;
    }

    public void setProvider(String userAndGroup)
    {
        this.provider = userAndGroup;
    }

    public String getSecurityLabel()
    {
        return securityLabel;
    }

    public void setSecurityLabel(String securityLabel)
    {
        this.securityLabel = securityLabel;
    }    
}
