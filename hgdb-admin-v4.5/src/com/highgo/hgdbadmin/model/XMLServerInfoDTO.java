/* ------------------------------------------------ 
* 
* File: XMLServerInfoDTO.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\model\XMLServerInfoDTO.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Liu Yuanyuan
 */
public class XMLServerInfoDTO extends ConnectInfoDTO
{
    private String group;
    private String name;
   
    private String host;
    private String port; 
    private String maintainDB;
    private String user;
    private String pwd; //only when savePwd = true, save pwd to catalog.xml
    private boolean savePwd = false;
    private boolean onSSL = false;
       
    private boolean connected;      

    private String version;
    private boolean highgoDB;
    private String kernelVersion;
    private String hgdbCompatibility;

    //extend peroperties when connected
    private Long datLastSysOid;
    private String inRecovery;

    //only for default server,not forcible for new server
    private String service; 

    @Override
    public String getPartURL()
    {
        //like jdbc:postgresql://localhost:5866/
        return "jdbc:highgo://" + this.host + ":" + this.port + "/";
    }

    public TreeEnum.DBSYS getDbSys()
    {
        if(version.startsWith("PostgreSQL"))
        {
            return TreeEnum.DBSYS.POSTGRESQL;
        }else if (version.startsWith("HighGo Database"))
        {
            return TreeEnum.DBSYS.HIGHGO;
        }
        return null;
    }
    
    public String getVersionNumber()
    {
        if (version.startsWith("HighGo Database"))
        {
            return version.split(" ")[2];
        } else if (version.startsWith("PostgreSQL"))
        {
            return version.split(" ")[1];
        }
        return null;
    }
    
    public String getGroup()
    {
        return group;
    }
    public void setGroup(String group)
    {
        this.group = group;
    }

    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    
    public String getHost()
    {
        return host;
    }
    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }
    public void setPort(String port)
    {
        this.port = port;
    }
    
    @Override
    public String getMaintainDB()
    {
        return maintainDB;
    }
    @Override
    public void setMaintainDB(String maintDB)
    {
        this.maintainDB = maintDB;
    }

    @Override
    public String getUser()
    {
        return user;
    }
    @Override
    public void setUser(String user)
    {
        this.user = user;
    }
    
    @Override
    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }
    @Override
    public String getPwd()
    {
        return this.pwd;
    }
    
    @Override
    public boolean isOnSSL()
    {
        return onSSL;
    }
    @Override
    public void setOnSSL(boolean onSSL)
    {
        this.onSSL = onSSL;
    }

    public boolean isConnected()
    {
        return connected;
    }
    public void setConnected(boolean connected)
    {
        this.connected = connected;
    }

    public boolean isSavePwd()
    {
        return savePwd;
    }
    public void setSavePwd(boolean savePwd)
    {
        this.savePwd = savePwd;
    }

    public String getVersion() {
        return version;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public boolean isHighgoDB()
    {
        return highgoDB;
    }
    public void setHighgoDB(boolean highgoDB)
    {
        this.highgoDB = highgoDB;
    }

    public String getKernelVersion()
    {
        return kernelVersion;
    }
    public void setKernelVersion(String kernelVersion)
    {
        this.kernelVersion = kernelVersion;
    }

    public String getHgdbCompatibility()
    {
        return hgdbCompatibility;
    }
    public void setHgdbCompatibility(String hgdbCompatibility)
    {
        this.hgdbCompatibility = hgdbCompatibility;
    }
    
    public String getService()
    {
        return service;
    }
    //service defined by manually input
    public void setService(String service)
    {
        this.service = service;
    }
     
    public String isInRecovery()
    {
        return inRecovery;
    }    
    public void setInRecovery(String inRecovery)
    {
        this.inRecovery = inRecovery;
    }
  
    @Override
    public Long getDatLastSysOid()
    {
        return datLastSysOid;
    }
    @Override
    public void setDatLastSysOid(Long datLastSysOid)
    {
        this.datLastSysOid = datLastSysOid;
    }
             
    @Override
    public String toString()
    {
        return name + "(" + host + ":" + port + ")";
    }    
    
}
