/* ------------------------------------------------
 *
 * File: DBInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\DBInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class DBInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
    
    private String name;
    private Long oid;
    private String owner;
    private String acl;   
    private String template;//for database creation
    private String defaultTablespace;//for database property
    private String tablespace;
    private String encoding;
    private String collation;
    private String characterType;
    private String defaultSchema;  
    private boolean allowConnection;
    private boolean connected;
    private String connectionLimit;
    private String comment;
    
    private String defaultTableAcl=null;
    private String defaultSequenceAcl=null;
    private String defaultFunctionAcl=null;
    private String defaultTypeAcl=null;//>=pg9.2
    
    //for db creation
    private List<VariableInfoDTO> variableList;// variableName,variableValue, variableUser;
    private List<SecurityLabelInfoDTO> securityLabelList;// provider,securityLabel;

    private Long datLastSysOid;

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();//pg_template0's oid=last system oid
    }
    
    public Long getDatLastSysOid()
    {
        return datLastSysOid;
    }

    public void setDatLastSysOid(Long datLastSysOid)
    {
        this.datLastSysOid = datLastSysOid;
    }
    
    @Override
     public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.DATABASE;
    }
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public String getTemplate()
    {
        return template;
    }

    public void setTemplate(String template)
    {
        this.template = template;
    }

    public String getTablespace()
    {
        return tablespace;
    }

    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }

    public String getDefaultTablespace()
    {
        return defaultTablespace;
    }

    public void setDefaultTablespace(String defaultTablespace)
    {
        this.defaultTablespace = defaultTablespace;
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public String getCollation()
    {
        return collation;
    }

    public void setCollation(String collation)
    {
        this.collation = collation;
    }

    public String getCharacterType()
    {
        return characterType;
    }

    public void setCharacterType(String characterType)
    {
        this.characterType = characterType;
    }

    public String getDefaultSchema()
    {
        return defaultSchema;
    }

    public void setDefaultSchema(String defaultSchema)
    {
        this.defaultSchema = defaultSchema;
    }

    public String getDefaultTableAcl()
    {
        return defaultTableAcl;
    }

    public void setDefaultTableAcl(String defaultTableAcl)
    {
        this.defaultTableAcl = defaultTableAcl;
    }

    public String getDefaultSequenceAcl()
    {
        return defaultSequenceAcl;
    }

    public void setDefaultSequenceAcl(String defaultSequenceAcl)
    {
        this.defaultSequenceAcl = defaultSequenceAcl;
    }

    public String getDefaultFunctionAcl()
    {
        return defaultFunctionAcl;
    }

    public void setDefaultFunctionAcl(String defaultFunctionAcl)
    {
        this.defaultFunctionAcl = defaultFunctionAcl;
    }

    public String getDefaultTypeAcl()
    {
        return defaultTypeAcl;
    }

    public void setDefaultTypeAcl(String defaultTypeAcl)
    {
        this.defaultTypeAcl = defaultTypeAcl;
    }

    public boolean isAllowConnection()
    {
        return allowConnection;
    }

    public void setAllowConnection(boolean isAllowConnection)
    {
        this.allowConnection = isAllowConnection;
    }

    public boolean isConnected()
    {
        return connected;
    }

    public void setConnected(boolean iscConnected)
    {
        this.connected = iscConnected;
    }

    public String getConnectionLimit()
    {
        return connectionLimit;
    }

    public void setConnectionLimit(String connectionLimit)
    {
        this.connectionLimit = connectionLimit;
    }
    
    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
   
    @Override
    public String toString()
    {
        return name;
    }

    public List<VariableInfoDTO> getVariableList()
    {
        return variableList;
    }
    public void setVariableList(List<VariableInfoDTO> variableList)
    {
        this.variableList = variableList;
    }

    public List<SecurityLabelInfoDTO> getSecurityLabelList()
    {
        return securityLabelList;
    }
    public void setSecurityLabelList(List<SecurityLabelInfoDTO> securityLabelList)
    {
        this.securityLabelList = securityLabelList;
    }

}
