/* ------------------------------------------------
 *
 * File: XMLServerBranchInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\XMLServerBranchInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class XMLServerBranchInfoDTO
{
   String groupName;
   List<XMLServerInfoDTO> xmlServerInfoList = new ArrayList();

    public XMLServerBranchInfoDTO()
    {
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public List<XMLServerInfoDTO> getXmlServerInfoList()
    {
        return xmlServerInfoList;
    }

    public void setXmlServerInfoList(List<XMLServerInfoDTO> xmlServerInfo)
    {
        this.xmlServerInfoList = xmlServerInfo;
    }

}
