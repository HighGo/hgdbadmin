/* ------------------------------------------------
 *
 * File: DatatypeDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\DatatypeDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 */
public class DatatypeDTO
{   
    private Long oid;//just for select and get alternative types 
    private String fullname;
    
    private String name;
    //for type like varchar(n), n is length
    //for type like numeric(p,s), p is length and s is scale
    private int length = 0;
    private int scale = 0;
    
    private String shortname;//useless untill now, like varcahr for character varing

    @Override
    public String toString()
    {
        return this.name.replace("oracle.blob", "blob").replace("oracle.clob", "clob");
    }
    
    public String getShortname()
    {
        return shortname;
    }
    public void setShortname(String shortname)
    {
        this.shortname = shortname;
    }
    
    public Long getOid()
    {
        return oid;
    }
    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    public boolean hasLength()
    {
        /*if (this.getName() != null && !this.getName().isEmpty()
                && ((this.name.matches("timestamp with time zone") || this.name.matches("timestamp without time zone")
                || this.name.matches("time with time zone") || this.name.matches("time without time zone")
                || this.name.equals("interval"))))
        {
            hasArgs = true;
        }else if("text".equalsIgnoreCase(this.getName()))
        {
            hasArgs = false;
        }
        return hasArgs;*/
        /*
        //D
         #define PGOID_TYPE_TIME                     1083L
         #define PGOID_TYPE_TIME_ARRAY               1183L
         #define PGOID_TYPE_TIMETZ                   1266L
         #define PGOID_TYPE_TIMETZ_ARRAY             1270L
         #define PGOID_TYPE_TIMESTAMP                1114L
         #define PGOID_TYPE_TIMESTAMP_ARRAY          1115L
         #define PGOID_TYPE_TIMESTAMPTZ              1184L
         #define PGOID_TYPE_TIMESTAMPTZ_ARRAY        1185L
         #define PGOID_TYPE_INTERVAL                 1186L
         #define PGOID_TYPE_INTERVAL_ARRAY           1187L

         //L
         #define PGOID_TYPE_BIT                      1560L
         #define PGOID_TYPE_BIT_ARRAY                1561L
         #define PGOID_TYPE_VARBIT                   1562L
         #define PGOID_TYPE_VARBIT_ARRAY             1563L
         #define PGOID_TYPE_BPCHAR                   1042L
         #define PGOID_TYPE_BPCHAR_ARRAY             1014L
         #define PGOID_TYPE_VARCHAR                  1043L
         #define PGOID_TYPE_VARCHAR_ARRAY            1015L

         //P
         #define PGOID_TYPE_NUMERIC                  1700L
         #define PGOID_TYPE_NUMERIC_ARRAY            1231L
        
         //P has precision, D L P has length (ref pgamdin)
         */
        return this.oid == 1083L || this.oid == 1183L
                || this.oid == 1266L || this.oid == 1270L
                || this.oid == 1114L || this.oid == 1115L
                || this.oid == 1184L || this.oid == 1185L
                || this.oid == 1186L || this.oid == 1187L
                
                || this.oid == 1560L || this.oid == 1561L
                || this.oid == 1562L || this.oid == 1563L
                || this.oid == 1042L || this.oid == 1014L
                || this.oid == 1043L || this.oid == 1015L
                
                || this.oid == 1700L || this.oid == 1231L
                || this.name.equalsIgnoreCase("varchar2")|| this.name.equalsIgnoreCase("varchar2[]")
                || this.name.equalsIgnoreCase("nvarchar2")|| this.name.equalsIgnoreCase("nvarchar2[]");
    }
    public boolean hasScale()
    {
        return this.name.equalsIgnoreCase("numeric")||this.name.equalsIgnoreCase("numeric[]");
    }
    public boolean hasDefaultValue()
    {
        return !this.name.equalsIgnoreCase("serial")
                && !this.name.equalsIgnoreCase("bigserial")
                && !this.name.equalsIgnoreCase("smallserial");
    }

    public String getFullname()
    {
        if (this.fullname == null || this.fullname.isEmpty())
        {
            if (this.length != 0)
            {
                if (this.scale != 0)
                {
                    if (this.name.endsWith("[]"))//array
                    {
                        this.fullname = this.name.replace("[]", "") 
                                + "(" + this.length + "," + this.scale + ")" + "[]";
                    } else
                    {
                        this.fullname = this.name + "(" + this.length + "," + this.scale + ")";
                    }
                } else
                {
                    if (this.name.equals("timestamp with time zone"))
                    {
                        this.fullname = "timestamp(" + this.length + ") with time zone";
                    } else if (this.name.equals("timestamp without time zone"))
                    {
                        this.fullname = "timestamp(" + this.length + ") without time zone";
                    } else if (this.name.equals("time with time zone"))
                    {
                        this.fullname = "time(" + this.length + ") with time zone";
                    } else if (this.name.equals("time without time zone"))
                    {
                        this.fullname = "time(" + this.length + ") without time zone";
                    } else if (this.name.endsWith("[]"))//array
                    {
                        this.fullname = this.name.replace("[]", "") + "(" + this.length + ")" + "[]";
                    } else
                    {
                        this.fullname = this.name + "(" + this.length + ")";
                    }
                }
            } else
            {
                this.fullname = this.name;
            }
        }
        return this.fullname;
    }
    public void setFullname(String fullname)
    {
        this.fullname = fullname;        
        if (this.fullname.indexOf("(") > 0 && this.fullname.indexOf(")") > 0)
        {
            if (fullname.startsWith("time") && fullname.endsWith(" time zone"))
            {
                this.length = Integer.valueOf(fullname.substring(fullname.indexOf("(") + 1, fullname.indexOf(")")));
                this.name = fullname.substring(0, fullname.indexOf("("))
                        + fullname.substring(fullname.indexOf(")") + 1, fullname.length());
            } else
            {
                this.name = fullname.substring(0, fullname.indexOf("("));
                String arg = fullname.substring(fullname.indexOf("(") + 1, fullname.indexOf(")"));
                System.out.println("com.highgo.hgdbadmin.model.DatatypeDTO; arg=" + arg);
                if (arg.indexOf(",") <= 0)
                {
                    this.length = Integer.valueOf(arg);
                } else
                {
                    this.length = Integer.valueOf(arg.split(",")[0]);
                    this.scale = Integer.valueOf(arg.split(",")[1]);
                }
            }
        } else
        {
            this.name = this.fullname;
        }
    }

    //////////////////////////////////////////
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }   

    //negative stands for variable-length type, its length can be defined
    public int getLength()
    {
        return length;
    }
    public void setLength(int length)
    {
        this.length = length;
    }

    public int getScale()
    {
        return scale;
    }
    public void setScale(int scale)
    {
        this.scale = scale;
    }    
    
}
