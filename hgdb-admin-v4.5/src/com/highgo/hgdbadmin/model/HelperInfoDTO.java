/* ------------------------------------------------
 *
 * File: HelperInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\HelperInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * 
 * @author Liu Yuanyuan
 */
public class HelperInfoDTO extends ConnectInfoDTO
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    //private String partURL;//like jdbc:postgresql://localhost:5866/ , need dbName name
    private String host;
    private String port;
    private String maintainDB;
    private String user;
    private String pwd;
    private String onSSL;
    private TreeEnum.DBSYS dbSys;//highgo or postgresql, this is for version compatibility
    private String versionNumber;
    
    private String dbName;    
    private String dbTablespace;//dbName's tablespace
    private long schemaOid;
    private String schema;    
    private TreeEnum.TreeNode type;//VIEW OR TABLE
    private String relation;
    private long relationOid;
    
    //add begin by sunqk at 2019.06.03 for jdbc append preferQueryMode=simple
    // value 1 is simple
    // default extent mode
    private int m_iQueryMode;
    //add end by sunqk at 2019.06.03 for jdbc append preferQueryMode=simple
    
    private long datLastSysOid;

    
    
    //add begin by sunqk at 2019.06.03 for jdbc append preferQueryMode=simple
    public void setQueryMode(int iMode)
    {
        this.m_iQueryMode = iMode;
    }
    
    public int getQueryMode()
    {
        return this.m_iQueryMode;
    }
    //add end by sunqk at 2019.06.03 for jdbc append preferQueryMode=simple
    
    
    @Override
    public String getPartURL()
    {
        return "jdbc:highgo://" + this.host + ":" + this.port + "/";
    }
    
    public String getHost()
    {
        return host;
    }
    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }
    public void setPort(String port)
    {
        this.port = port;
    }

    @Override
    public String getUser()
    {
        return user;
    }
    @Override
    public void setUser(String user)
    {
        this.user = user;
    }

    @Override
    public String getPwd()
    {
        return pwd;
    }
    @Override
    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }
    
    public String getOnSSL()
    {
        return onSSL;
    }
    public void setOnSSL(String onSSL)
    {
        this.onSSL = onSSL;
    }

    @Override
    public String getMaintainDB()
    {
        return maintainDB;
    }
    @Override
    public void setMaintainDB(String maintainDB)
    {
        this.maintainDB = maintainDB;
    }

        
    public String getDbTablespace()
    {
        return dbTablespace;
    }
    public void setDbTablespace(String dbTablespace)
    {
        this.dbTablespace = dbTablespace;
    }
    
     public String getDbName()
    {
        if (dbName == null || dbName.isEmpty())
        {
            //logger.debug("--------------------return a maintainDB--------------------");
            return maintainDB;
        } else
        {
            return dbName;
        }
    }
    public void setDbName(String dbName)
    {
        this.dbName = dbName;
    }
    
    public TreeEnum.DBSYS getDbSys()
    {
        return dbSys;
    }
    public void setDbSys(TreeEnum.DBSYS dbSys)
    {
        this.dbSys = dbSys;
    }

    public String getVersionNumber()
    {
        return versionNumber;
    }
    public void setVersionNumber(String versionNumber)
    {
        this.versionNumber = versionNumber;
    }

    public long getSchemaOid()
    {
        return schemaOid;
    }
    public void setSchemaOid(long schemaOid)
    {
        this.schemaOid = schemaOid;
    }

    public String getSchema()
    {
        return schema;
    }
    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public TreeEnum.TreeNode getType()
    {
        return type;
    }
    public void setType(TreeEnum.TreeNode type)
    {
        this.type = type;
    }

    public String getRelation()
    {
        return relation;
    }
    public void setRelation(String relation)
    {
        this.relation = relation;
    }

    public Long getRelationOid()
    {
        return relationOid;
    }
    public void setRelationOid(long relationOid)
    {
        this.relationOid = relationOid;
    }
    
    @Override
    public Long getDatLastSysOid()
    {
        return this.datLastSysOid;
    }
    @Override
    public void setDatLastSysOid(Long datLastSysOid)
    {
        this.datLastSysOid = datLastSysOid;
    }
    
    private final ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    @Override
    public String toString()
    {
        return MessageFormat.format(constBundle.getString("connection"), 
                this.user, this.getDbName(), this.host, this.port);
    }
}
