/* ------------------------------------------------
 *
 * File: TableInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\TableInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class TableInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
    private String schema;//only for add new table
    private long schemaOid;//only for add new table
    
    private String name;
    private long oid;
    private String owner;
    private String tablespace;
    private String acl;
    private String ofType;
    private String primaryKey;
    private int rowsEstimated;
    private String fillFactor;
    private int rowsCounted;
    private char kind = 'r'; //r = oridinary table, p = partition main table
    private String partitionSchema;
    private List<String> partitionKey;
    private boolean inheritedByTables;
    private boolean hasOID = false;//has oid not support since pg12 
    private boolean hasToastTable;//
    private String comment;
    private boolean replication;
    
    //for creation
    private boolean unlogged;//not realized
    private List<PartitionInfoDTO> partitionList;//partition of this main table
    private List<ObjItemInfoDTO> inheritTableList;//inherit table and partition table
    private String likeRelation;
    private boolean includeDefaultValue;
    private boolean includeConstraint;
    private boolean includeIndexes;
    private boolean includeStorage;
    private boolean includeComments;
    private List<ColumnInfoDTO> columnInfoList;
    private List<ConstraintInfoDTO> constraintInfoList;
    private List<SecurityLabelInfoDTO> securityInfoList;
    private AutoVacuumDTO autoVacuumInfo;

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.TABLE;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        return this.name;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getSchema()
    {
        return schema;
    }
    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public long getSchemaOid()
    {
        return schemaOid;
    }
    public void setSchemaOid(long schemaOid)
    {
        this.schemaOid = schemaOid;
    }
    
   

    public String getTablespace()
    {
        return tablespace;
    }
    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }

    public String getAcl()
    {
        return acl;
    }
    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public String getOfType()
    {
        return ofType;
    }
    public void setOfType(String ofType)
    {
        this.ofType = ofType;
    }

    public String getPrimaryKey()
    {
        return primaryKey;
    }
    public void setPrimaryKey(String primaryKey)
    {
        this.primaryKey = primaryKey;
    }

    public int getRowsEstimated()
    {
        return rowsEstimated;
    }
    public void setRowsEstimated(int rowsEstimated)
    {
        this.rowsEstimated = rowsEstimated;
    }

    public String getFillFactor()
    {
        return fillFactor;
    }
    public void setFillFactor(String fillFactor)
    {
        this.fillFactor = fillFactor;
    }

    public int getRowsCounted()
    {
        return rowsCounted;
    }
    public void setRowsCounted(int rowsCounted)
    {
        this.rowsCounted = rowsCounted;
    }

    public char getKind()
    {
        return kind;
    }

    public void setKind(char kind)
    {
        this.kind = kind;
    }

    public boolean isPartitioned()
    {
        return (kind=='p');
    }


    public String getPartitionSchema()
    {
        return partitionSchema;
    }

    public void setPartitionSchema(String partitionSchema)
    {
        this.partitionSchema = partitionSchema;
    }

    
    public String getPartitionKeyStr4add()
    {
        if (partitionKey == null || partitionKey.isEmpty())
        {
            System.err.println("key is null ..............");
            return "";
        }
        StringBuilder sql = new StringBuilder();
        sql.append("(");
        boolean first = true;
        for (String byarg : partitionKey)
        {
            sql.append(first ? "" : ",").append(byarg);
        }
        sql.append(")");
        return sql.toString();
    }
    public List<String> getPartitionKey()
    {
        return partitionKey;
    }

    public void setPartitionKey(List<String> partitionKey)
    {
        this.partitionKey = partitionKey;
    }

    public List<PartitionInfoDTO> getPartitionList()
    {
        return partitionList;
    }
    public void setPartitionList(List<PartitionInfoDTO> partitionList)
    {
        this.partitionList = partitionList;
    }
    
    public boolean isInheritedByTables()
    {
        return inheritedByTables;
    }
    public void setInheritedByTables(boolean inheritedByTables)
    {
        this.inheritedByTables = inheritedByTables;
    }

    public boolean isHasOID()
    {
        return hasOID;
    }

    public void setHasOID(boolean isHasOID)
    {
        this.hasOID = isHasOID;
    }

    public boolean isHasToastTable()
    {
        return hasToastTable;
    }

    public void setHasToastTable(boolean hasToastTable)
    {
        this.hasToastTable = hasToastTable;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
    public boolean isUnlogged()
    {
        return unlogged;
    }

    public void setUnlogged(boolean unlogged)
    {
        this.unlogged = unlogged;
    }

    public List<ObjItemInfoDTO> getInheritTableList()
    {
        return inheritTableList;
    }

    public void setInheritTableList(List<ObjItemInfoDTO> inheritTableList)
    {
        this.inheritTableList = inheritTableList;
    }

    public String getLikeRelation()
    {
        return likeRelation;
    }

    public void setLikeRelation(String likeRelation)
    {
        this.likeRelation = likeRelation;
    }

    public boolean isIncludeDefaultValue()
    {
        return includeDefaultValue;
    }

    public void setIncludeDefaultValue(boolean includeDefaultValue)
    {
        this.includeDefaultValue = includeDefaultValue;
    }

    public boolean isIncludeConstraint()
    {
        return includeConstraint;
    }

    public void setIncludeConstraint(boolean includeCOnstraint)
    {
        this.includeConstraint = includeCOnstraint;
    }

    public boolean isIncludeIndexes()
    {
        return includeIndexes;
    }

    public void setIncludeIndexes(boolean includeIndexes)
    {
        this.includeIndexes = includeIndexes;
    }

    public boolean isIncludeStorage()
    {
        return includeStorage;
    }

    public void setIncludeStorage(boolean includeStorage)
    {
        this.includeStorage = includeStorage;
    }

    public boolean isIncludeComments()
    {
        return includeComments;
    }

    public void setIncludeComments(boolean includeComments)
    {
        this.includeComments = includeComments;
    }

    public List<ColumnInfoDTO> getColumnInfoList()
    {
        return columnInfoList;
    }

    public void setColumnInfoList(List<ColumnInfoDTO> columnInfoList)
    {
        this.columnInfoList = columnInfoList;
    }

    public List<ConstraintInfoDTO> getConstraintInfoList()
    {
        return constraintInfoList;
    }

    public void setConstraintInfoList(List<ConstraintInfoDTO> constraintInfoList)
    {
        this.constraintInfoList = constraintInfoList;
    }

    public List<SecurityLabelInfoDTO> getSecurityInfoList()
    {
        return securityInfoList;
    }

    public void setSecurityInfoList(List<SecurityLabelInfoDTO> securityInfoList)
    {
        this.securityInfoList = securityInfoList;
    }

    public AutoVacuumDTO getAutoVacuumInfo()
    {
        return autoVacuumInfo;
    }

    public void setAutoVacuumInfo(AutoVacuumDTO autoVacuumInfo)
    {
        this.autoVacuumInfo = autoVacuumInfo;
    }

    public boolean isReplication()
    {
        return replication;
    }

    public void setReplication(boolean replication)
    {
        this.replication = replication;
    }
}
