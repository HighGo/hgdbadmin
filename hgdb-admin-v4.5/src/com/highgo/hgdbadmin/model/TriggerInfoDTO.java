/* ------------------------------------------------
 *
 * File: TriggerInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\TriggerInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Yuanyuan
 */
public class TriggerInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;    
    private String parent;

    //for creation
    //name,event,trigger function must not null for a trigger.
    private String name;
    private long oid;
    private String comment;
    private boolean rowTrigger;//for table,trigger either for each ROW or for each STATEMENT
    private boolean constraintTrigger;
    private boolean deferrable;
    private boolean deferred;
    private String triggerFunction;
    //for create, trigger fun=triggerFunWithoutArg + arg
    private String triggerFunWithoutArg;
    private String arguments;
    
    private String fire;
    private List<String> eventList;
    private List<String> columnList;
    private String when;
    private String defineSQL;
    //for property    
    private boolean enable;
    //private boolean system;

    @Override
    public String getOwner()
    {
       return null;
    }
    
    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.TRIGGER;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }

    public String getParent()
    {
        return parent;
    }

    public void setParent(String parent)
    {
        this.parent = parent;
    }
   
    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public boolean isRowTrigger()
    {
        return rowTrigger;
    }

    public void setRowTrigger(boolean rowTrigger)
    {
        this.rowTrigger = rowTrigger;
    }

    public boolean isConstraintTrigger()
    {
        return constraintTrigger;
    }

    public void setConstraintTrigger(boolean constraintTrigger)
    {
        this.constraintTrigger = constraintTrigger;
    }

    public boolean isDeferrable()
    {
        return deferrable;
    }

    public void setDeferrable(boolean deferrable)
    {
        this.deferrable = deferrable;
    }

    public boolean isDeferred()
    {
        return deferred;
    }

    public void setDeferred(boolean deferred)
    {
        this.deferred = deferred;
    }

    public String getTriggerFunction()
    {
        return triggerFunction;
    }

    public void setTriggerFunction(String triggerFunction)
    {
        this.triggerFunction = triggerFunction;
    }

    public String getTriggerFunWithoutArg()
    {
        return triggerFunWithoutArg;
    }

    public void setTriggerFunWithoutArg(String triggerFunWithoutArg)
    {
        this.triggerFunWithoutArg = triggerFunWithoutArg;
    }

    public String getArguments()
    {
        return arguments;
    }

    public void setArguments(String arguments)
    {
        this.arguments = arguments;
    }

    public String getFire()
    {
        return fire;
    }

    public void setFire(String fire)
    {
        this.fire = fire;
    }

    public List<String> getEventList()
    {
        return eventList;
    }

    public void setEventList(List<String> eventList)
    {
        this.eventList = eventList;
    }

    public List<String> getColumnList()
    {
        return columnList;
    }

    public void setColumnList(List<String> columnList)
    {
        this.columnList = columnList;
    }

    public String getWhen()
    {
        return when;
    }

    public void setWhen(String when)
    {
        this.when = when;
    }
 
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    public String getDefineSQL()
    {
        return defineSQL;
    }

    public void setDefineSQL(String defineSQL)
    {
        this.defineSQL = defineSQL;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
}
