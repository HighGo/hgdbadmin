/* ------------------------------------------------
 *
 * File: ViewInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ViewInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 * 
 * 创建视图
   CREATE [ OR REPLACE ] [ TEMP | TEMPORARY ] [ RECURSIVE ] VIEW name [ ( column_name [, ...] ) ]
    [ WITH ( view_option_name [= view_option_value] [, ... ] ) ]
    AS query
 * 
 * 
 */
public class ViewInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
    private String schema;
    private Long schemaOid;
            
    private String name;
    private long oid;
    private String owner;
    private String acl;
    private String definition;
    private String comment;
    //for view creation
    private boolean securityBarrier;
    private List<SecurityLabelInfoDTO> securityLabelInfoList;
    //for materialized view creation
    private boolean materializedView;
    //if it is materializedView, then the following is useful. 
    private String tablespace;
    private String fillFactor;
    private boolean withData;
    private AutoVacuumDTO autoVacuumInfo;

    private String[] headers;//for data view
    
    @Override
    public TreeEnum.TreeNode getType()
    {
        if (this.isMaterializedView())
        {
            return TreeEnum.TreeNode.MATERIALIZED_VIEW;
        } else
        {
            return TreeEnum.TreeNode.VIEW;
        }
    }
    
    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public Long getSchemaOid()
    {
        return schemaOid;
    }

    public void setSchemaOid(Long schemaOid)
    {
        this.schemaOid = schemaOid;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public String getDefinition()
    {
        return definition;
    }

    public void setDefinition(String definition)
    {
        this.definition = definition;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public boolean isSecurityBarrier()
    {
        return securityBarrier;
    }

    public void setSecurityBarrier(boolean securityBarrier)
    {
        this.securityBarrier = securityBarrier;
    }
    
    public List<SecurityLabelInfoDTO> getSecurityLabelInfoList()
    {
        return securityLabelInfoList;
    }

    public void setSecurityLabelInfoList(List<SecurityLabelInfoDTO> securityLabelInfoList)
    {
        this.securityLabelInfoList = securityLabelInfoList;
    }

    public boolean isMaterializedView()
    {
        return materializedView;
    }

    public void setMaterializedView(boolean materializedView)
    {
        this.materializedView = materializedView;
    }

    public String getTablespace()
    {
        return tablespace;
    }

    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }

    public String getFillFactor()
    {
        return fillFactor;
    }

    public void setFillFactor(String fillFactor)
    {
        this.fillFactor = fillFactor;
    }

    public boolean isWithData()
    {
        return withData;
    }

    public void setWithData(boolean withData)
    {
        this.withData = withData;
    }

    public AutoVacuumDTO getAutoVacuumInfo()
    {
        return autoVacuumInfo;
    }

    public void setAutoVacuumInfo(AutoVacuumDTO autoVacuumInfo)
    {
        this.autoVacuumInfo = autoVacuumInfo;
    }

    public String[] getHeaders()
    {
        return headers;
    }

    public void setHeaders(String[] headers)
    {
        this.headers = headers;
    }
}
