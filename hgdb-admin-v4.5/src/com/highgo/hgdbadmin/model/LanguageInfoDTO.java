/* ------------------------------------------------
 *
 * File: LanguageInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\LanguageInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Liu Yuanyuan
 */
public class LanguageInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;//no use now
    private String name;
    private long oid;
    private String owner;
    private String acl;
    private boolean trusted;
    private String handler;
    private String validator;
    private String comment;

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.LANGUAGE;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public boolean isTrusted()
    {
        return trusted;
    }

    public void setTrusted(boolean trusted)
    {
        this.trusted = trusted;
    }

    public String getHandler()
    {
        return handler;
    }

    public void setHandler(String handler)
    {
        this.handler = handler;
    }

    public String getValidator()
    {
        return validator;
    }

    public void setValidator(String validator)
    {
        this.validator = validator;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return this.name;
    }
}
