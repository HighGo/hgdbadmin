/* ------------------------------------------------
 *
 * File: ObjGroupDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ObjGroupDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum.TreeNode;
import java.util.List;
import java.util.ResourceBundle;

/**
 * @Description for collection node
 * @author Liu Yuanyuan
 */
public class ObjGroupDTO //extends AbstractObject
{
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private HelperInfoDTO helperInfo;
    private TreeNode type;
    private String name;
    //only for server group
    private List<XMLServerInfoDTO> serverInfoList;
    //for other object group ,except server group.
    private List<ObjInfoDTO> objInfoList;

    
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }

    public TreeNode getType()
    {
        return this.type;
    }

    public void setType(TreeNode type)
    {
        this.type = type;
        switch (type)
        {
            case SERVER_GROUP:
                name = constBundle.getString("servers");
                break;
            //server branch            
            case DATABASE_GROUP:
                name = constBundle.getString("databases");
                break;
            case TABLESPACE_GROUP:
                name = constBundle.getString("tablespaces");
                break;
            case LOGINROLE_GROUP:
                name = constBundle.getString("loginRoles");
                break;
            case GROUPROLE_GROUP:
                name = constBundle.getString("groupRoles");
                break;
            //db branch
            case CATALOG_GROUP:
                name = constBundle.getString("catalogs");
                break;
            case CAST_GROUP:
                name = constBundle.getString("casts");
                break;
            case LANGUAGE_GROUP:
                name = constBundle.getString("languages");
                break;
             case EVENT_TRIGGER_GROUP:
                name = constBundle.getString("eventTrigger");
                break;
            case SCHEMA_GROUP:
                name = constBundle.getString("schemas");
                break;
            //schema branch
            case FUNCTION_GROUP:
                name = constBundle.getString("functions");
                break;
            case PROCEDURE_GROUP:
                name = constBundle.getString("procedures");
                break;
            case TABLE_GROUP:
                name = constBundle.getString("tables");
                break;
            case VIEW_GROUP:
                name = constBundle.getString("views");
                break;
            case SEQUENCE_GROUP:
                name = constBundle.getString("sequences");
                break;
            //table or view branch
            case PARTITION_GROUP:
                name = constBundle.getString("partions");
                break;
            case COLUMN_GROUP:
                name = constBundle.getString("columns");
                break;
            case CONSTRAINT_GROUP:
                name = constBundle.getString("constraints");
                break;
            case INDEX_GROUP:
                name = constBundle.getString("indexes");
                break;
            case RULE_GROUP:
                name = constBundle.getString("rules");
                break;
            case TRIGGER_GROUP:
                name = constBundle.getString("triggers");
                break;
            default:
                name = type.toString();
                break;
        }
    }
     
   // @Override
    public String getName()
    {        
        return this.name;
    }

    public List<ObjInfoDTO> getObjInfoList()
    {
        return objInfoList;
    }

    public void setObjInfoList(List<ObjInfoDTO> objInfoList)
    {
        this.objInfoList = objInfoList;
    }

    public List<XMLServerInfoDTO> getServerInfoList()
    {
        return serverInfoList;
    }

    public void setServerInfoList(List<XMLServerInfoDTO> serverInfoList)
    {
        this.serverInfoList = serverInfoList;
    }
    

    @Override
     public String toString()
    {
        switch (type)
        {
            case SERVER:
                return name;
            case SERVER_GROUP:
                return name + "(" + serverInfoList.size() + ")";
            default:
                return name + "(" + objInfoList.size() + ")";
        }
    }
}
