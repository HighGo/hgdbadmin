/* ------------------------------------------------
 *
 * File: VariableInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\VariableInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 * 
 */
public class VariableInfoDTO implements Comparable<VariableInfoDTO>
{
    private String name;
    private String value;
    
    private String user;//for db
    private String db;//for role

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }  

    public String getDb()
    {
        return db;
    }

    public void setDb(String db)
    {
        this.db = db;
    }


    @Override
    public int compareTo(VariableInfoDTO o)
    {
        return this.name.compareTo(o.getName());
    }
    
}
