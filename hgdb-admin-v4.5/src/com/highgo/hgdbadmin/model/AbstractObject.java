/* ------------------------------------------------
 *
 * File: AbstractObject.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\AbstractObject.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Yuanyuan
 * 
 * This is a parent class for all db object model class.
 */
public abstract class AbstractObject
{    
    public abstract TreeEnum.TreeNode getType(); 
    public abstract Long getOid();//for column this is psostion
    public abstract String getName();
    public abstract HelperInfoDTO getHelperInfo();
    public abstract String getComment();
   
    public abstract boolean isSystem();
    public abstract String getOwner();//role, view and table column, constraint, trigger, rule, index
    
}
