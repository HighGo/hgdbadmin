/* ------------------------------------------------
 *
 * File: SysVariableInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\SysVariableInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 * 
 * For creation of database, role, tablespace.
 * Not for column.
 */
public class SysVariableInfoDTO
{
    String name;
    String context;
    String valueType;
    String minValue;
    String maxValue;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getContext()
    {
        return context;
    }

    public void setContext(String context)
    {
        this.context = context;
    }

    public String getValueType()
    {
        return valueType;
    }

    public void setValueType(String valueType)
    {
        this.valueType = valueType;
    }

    public String getMinValue()
    {
        return minValue;
    }

    public void setMinValue(String minValue)
    {
        this.minValue = minValue;
    }

    public String getMaxValue()
    {
        return maxValue;
    }

    public void setMaxValue(String maxValue)
    {
        this.maxValue = maxValue;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
}
