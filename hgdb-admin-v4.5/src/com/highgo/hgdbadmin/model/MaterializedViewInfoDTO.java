/* ------------------------------------------------
 *
 * File: MaterializedViewInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\MaterializedViewInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

/**
 *@deprecated 
 * @author Yuanyuan
 */
public class MaterializedViewInfoDTO
{
    private String tablespace;
    private String fillFactor;
    private boolean withData;
    private AutoVacuumDTO autoVacuumInfo;
    

    public String getTablespace()
    {
        return tablespace;
    }

    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }

    public String getFillFactor()
    {
        return fillFactor;
    }

    public void setFillFactor(String fillFactor)
    {
        this.fillFactor = fillFactor;
    }

    public boolean isWithData()
    {
        return withData;
    }

    public void setWithData(boolean withData)
    {
        this.withData = withData;
    }

    public AutoVacuumDTO getAutoVacuumInfo()
    {
        return autoVacuumInfo;
    }

    public void setAutoVacuumInfo(AutoVacuumDTO autoVacuumInfo)
    {
        this.autoVacuumInfo = autoVacuumInfo;
    }

}
