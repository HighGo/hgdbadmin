/* ------------------------------------------------
 *
 * File: EventTriggerInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\EventTriggerInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author liuyuanyuan
 */
public class EventTriggerInfoDTO extends AbstractObject {
    
    private HelperInfoDTO helperInfo;
    
    private String name;
    private Long oid;
    private String owner;
    
    private String func;
    private String event;
    private String enableWhat;
    private String whenTag;
    
    private String comment;
    
    @Override
    public TreeEnum.TreeNode getType() {
        return TreeEnum.TreeNode.EVENT_TRIGGER;
    }

    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo() {
        return helperInfo; 
    }

     @Override
    public boolean isSystem() {
        return false;
    }
    
    @Override
    public Long getOid() {
       return oid;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getOwner() {
        return owner;
    }
    
    @Override
    public String getComment() {
        return comment;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo) {
        this.helperInfo = helperInfo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOid(Long oid) {
        this.oid = oid;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    
    public String getFunc() {
        return func;
    }

    public void setFunc(String func) {
        this.func = func;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEnableWhat() {
        return enableWhat;
    }

    public void setEnableWhat(String enableWhat) {
        this.enableWhat = enableWhat;
    }

    public String getWhenTag() {
        return whenTag;
    }

    public void setWhenTag(String tagWhen) {
        this.whenTag = tagWhen;
    }

   
    
    
    
    
}
