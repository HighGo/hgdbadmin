/* ------------------------------------------------
 *
 * File: ObjInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ObjInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Liu Yuanyuan
 */
public class ObjInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;//no use now
    private TreeEnum.TreeNode type;
    private Long oid;//for column this is position
    private String name;
    private String owner;
    private String comment;

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
     
    @Override
    public TreeEnum.TreeNode getType()
    {
        return type;
    }
    public void setType(TreeEnum.TreeNode type)
    {
        this.type = type;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }
    @Override
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public boolean isSystem()
    {
        switch (type)
        {
            case CATALOG:
                return true;
//            case SCHEMA:
//                return (!this.name.equals("public")) && this.oid < helperInfo.getDatLastSysOid();              
            default:
                return this.oid < helperInfo.getDatLastSysOid();
        }
    }
}
