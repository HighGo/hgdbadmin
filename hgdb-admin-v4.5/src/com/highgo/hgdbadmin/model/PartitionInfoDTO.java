/* ------------------------------------------------
 *
 * File: PartitionInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\PartitionInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author highgoer
 */
public class PartitionInfoDTO extends AbstractObject 
{
     //for creation
    private HelperInfoDTO helperInfo;  
    
    private long oid;
    private String name;
    private String schema;
    private String owner;
    //for property
    private String value;
    //for create
    private String partitionSchema;
    private boolean defaultValue;
    private String fromValue;
    private String toValue;
    private String inValue;
    private String modulusValue;
    private String remainderValue;
    
    private boolean partition;
    private String comment;
    private long parentOid;
    private String tablespace;

    
    private String[] headers;//for data view

    
    @Override
    public TreeEnum.TreeNode getType()
    {
         return TreeEnum.TreeNode.PARTITION;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public Long getOid()
    {
       return oid;
    }
    public void setOid(long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getName()
    {
       return name;
    } 
    public void setName(String name)
    {
        this.name = name;
    }
   
    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }
    
    @Override
    public String getComment()
    {
        return comment;
    } 
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String getOwner()
    {
       return owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public boolean isPartition()
    {
        return partition;
    }

    public void setPartition(boolean partition)
    {
        this.partition = partition;
    }

    public String getTablespace()
    {
        return tablespace;
    }
    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }       
    
    public long getParentOid()
    {
        return parentOid;
    }
    public void setParentOid(long parentOid)
    {
        this.parentOid = parentOid;
    }

    public String getPartitionSchema()
    {
        return partitionSchema;
    }
    public void setPartitionSchema(String partitionSchema)
    {
        this.partitionSchema = partitionSchema;
    }

    public boolean isDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(boolean defaultValue)
    {
        this.defaultValue = defaultValue;
    }
    
    public String getFromValue()
    {
        return fromValue;
    }
    public void setFromValue(String fromValue)
    {
        this.fromValue = fromValue;
    }

    public String getToValue()
    {
        return toValue;
    }
    public void setToValue(String toValue)
    {
        this.toValue = toValue;
    }

    public String getInValue()
    {
        return inValue;
    }
    public void setInValue(String inValue)
    {
        this.inValue = inValue;
    }

    public String getModulusValue() {
        return modulusValue;
    }
    public void setModulusValue(String modulusValue) {
        this.modulusValue = modulusValue;
    }

    public String getRemainderValue() {
        return remainderValue;
    }
    public void setRemainderValue(String remainderValue) {
        this.remainderValue = remainderValue;
    }
    
    

    public String getValue()
    {
        return value;
    }
    public void setValue(String value)
    {
        this.value = value;
    }

    public String[] getHeaders()
    {
        return headers;
    }

    public void setHeaders(String[] headers)
    {
        this.headers = headers;
    }
   
    
    
}
