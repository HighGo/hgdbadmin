/* ------------------------------------------------
 *
 * File: BranchDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\BranchDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum.TreeNode;
import java.util.ArrayList;
import java.util.List;

/**
 * @description for node has branch 
 * @author Liu Yuanyuan
 */
public class BranchDTO
{
    private final TreeNode type;
    private List<ObjGroupDTO> groupList;

    public BranchDTO(TreeNode type)
    {
        this.type = type;
        groupList = new ArrayList();
    }

    public TreeNode getType()
    {
        return this.type;
    }

    public void add(ObjGroupDTO groupNode)
    {
        groupList.add(groupNode);
    }
   
    public ObjGroupDTO get(int i)
    {
        return groupList.get(i);
    }
    
    public List<ObjGroupDTO> getAll()
    {
        return groupList;
    }

}
