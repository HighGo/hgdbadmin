/* ------------------------------------------------
 *
 * File: SchemaBranchDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\SchemaBranchDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Yuanyuan
 */
public class RuleInfoDTO extends AbstractObject
{
    //for creation
    private HelperInfoDTO helperInfo;  
    
    private long oid;
    private String name;
    private String parent;//TABLE or VIEW

    private String event;
    private boolean doInstead;
    private String doStatement;
    private String whereCondition;   
    private boolean enable; //for property
    private String comment;
    private String defineSQL;//remain to use

    @Override
    public boolean isSystem()
    {
        //return this.oid < helperInfo.getDatLastSysOid();
        return "_RETURN".equals(this.name);
    }

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.RULE;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public String getOwner()
    {
        return null;
    }

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getParent()
    {
        return parent;
    }

    public void setParent(String parent)
    {
        this.parent = parent;
    }

    public String getEvent()
    {
        return event;
    }

    public void setEvent(String event)
    {
        this.event = event;
    }

    public boolean isDoInstead()
    {
        return doInstead;
    }

    public void setDoInstead(boolean doInstead)
    {
        this.doInstead = doInstead;
    }

    public String getDoStatement()
    {
        return doStatement;
    }

    public void setDoStatement(String doStatement)
    {
        this.doStatement = doStatement;
    }

    public String getWhereCondition()
    {
        return whereCondition;
    }

    public void setWhereCondition(String whereCondition)
    {
        this.whereCondition = whereCondition;
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getDefineSQL()
    {
        return defineSQL;
    }

    public void setDefineSQL(String defineSQL)
    {
        this.defineSQL = defineSQL;
    }
}
