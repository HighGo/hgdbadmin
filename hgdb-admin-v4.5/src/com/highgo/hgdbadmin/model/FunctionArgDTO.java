/* ------------------------------------------------
 *
 * File: FunctionArgDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\FunctionArgDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 */
public class FunctionArgDTO
{
    private String mode;
    private String name;
    private String type;
    private long typeOid;
    private String defaultValue;

    public FunctionArgDTO()
    {
        this.mode = "IN";
        this.name = "";
        this.defaultValue = null;
    }
    
    @Override
    public String toString()
    {
        if (this.defaultValue == null || this.defaultValue.isEmpty())
        {
            return this.mode + " " + this.name + " " + this.type;
        } else
        {            
            return this.mode + " " + this.name + " " + this.type + " DEFAULT " + this.defaultValue;
        }
    }

    public String getMode()
    {
        return mode;
    }

    public void setMode(String mode)
    {
        this.mode = mode;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public long getTypeOid()
    {
        return typeOid;
    }

    public void setTypeOid(long typeOid)
    {
        this.typeOid = typeOid;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }
           
}
