/* ------------------------------------------------
 *
 * File: ConnectInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ConnectInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

/**
 *
 * @author Yuanyuan
 * parent of HelperInfoDTO and XMLServerInfoDTO
 */
public class ConnectInfoDTO
{
    private String partURL;//like jdbc:postgresql://localhost:5866/ , need db name
    private String user;
    private String pwd;
    private String maintainDB;
    private boolean onSSL;
    private Long datLastSysOid;

    public String getPartURL()
    {
        return partURL;
    }

    public String getUser()
    {
        return user;
    }
    public void setUser(String user)
    {
        this.user = user;
    }

    public String getPwd()
    {
        return pwd;
    }
    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }

    public boolean isOnSSL()
    {
        return onSSL;
    }
    public void setOnSSL(boolean onSSL)
    {
        this.onSSL = onSSL;
    }
    
    public String getMaintainDB()
    {
        return maintainDB;
    }
    public void setMaintainDB(String maintainDB)
    {
        this.maintainDB = maintainDB;
    }   
        
    public Long getDatLastSysOid()
    {
        return datLastSysOid;
    }    
    public void setDatLastSysOid(Long datLastSysOid)
    {
        this.datLastSysOid = datLastSysOid;
    }
    
}
