/* ------------------------------------------------
 *
 * File: IndexInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\IndexInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Yuanyuan
 */
public class IndexInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
  
    private String name;
    private Long oid;
    private List<ColInfoDTO> colInfoList;//column string include:column,collation,operation class , desc/asc ,null first/last
    private String tablespace;
    private String accessMethod;
    private boolean unique;    
    private boolean concurrently;//only for creation, has nothing to do with other.
    private String fillFactor;
    private String constraint;
    private String comment;    
    private boolean clustered;//alter table table_name cluster on index_name    
    //for property
    private boolean primary;
    private boolean exclude;
    private boolean valid;
    private boolean system;

    private String defineSQL;

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.INDEX;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public String getOwner()
    {
        return null;
    }

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }


    @Override
    public Long getOid()
    {
        return oid;
    }
    public void setOid(Long oid)
    {
        this.oid = oid;
    }
    
    @Override
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }   

    public List<ColInfoDTO> getColInfoList()
    {
        return colInfoList;
    }
    public void setColInfoList(List<ColInfoDTO> colInfoList)
    {
        this.colInfoList = colInfoList;
    }
    public String getAllColumnDefine()
    {
        if(this.colInfoList==null || this.colInfoList.size()<=0)
        {
            return "";
        }
        StringBuilder sql = new  StringBuilder();
        for(ColInfoDTO  col : this.colInfoList)
        {
            sql.append(col.toColumnDefine()).append(",");
        }
        sql.deleteCharAt(sql.length()-1);
        return sql.toString();
    }
    
    @Override
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getTablespace()
    {
        return tablespace;
    }
    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }

    public String getAccessMethod()
    {
        return accessMethod;
    }
    public void setAccessMethod(String accessMethod)
    {
        this.accessMethod = accessMethod;
    }

    public String getFillFactor()
    {
        return fillFactor;
    }
    public void setFillFactor(String fillFactor)
    {
        this.fillFactor = fillFactor;
    }

    public boolean isUnique()
    {
        return unique;
    }
    public void setUnique(boolean unique)
    {
        this.unique = unique;
    }

    public boolean isClustered()
    {
        return clustered;
    }
    public void setClustered(boolean clustered)
    {
        this.clustered = clustered;
    }

    public boolean isConcurrently()
    {
        return concurrently;
    }
    public void setConcurrently(boolean concurrentBuild)
    {
        this.concurrently = concurrentBuild;
    }

    public String getConstraint()
    {
        return constraint;
    }
    public void setConstraint(String constraint)
    {
        this.constraint = constraint;
    }

    public boolean isPrimary()
    {
        return primary;
    }
    public void setPrimary(boolean primary)
    {
        this.primary = primary;
    }

    public boolean isExclude()
    {
        return exclude;
    }
    public void setExclude(boolean exclude)
    {
        this.exclude = exclude;
    }

    public boolean isValid()
    {
        return valid;
    }
    public void setValid(boolean valid)
    {
        this.valid = valid;
    }

    public String getDefineSQL()
    {
        return defineSQL;
    }
    public void setDefineSQL(String defineSQL)
    {
        this.defineSQL = defineSQL;
    }  
}
