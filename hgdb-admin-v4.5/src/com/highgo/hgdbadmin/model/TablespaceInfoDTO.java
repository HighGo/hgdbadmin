/* ------------------------------------------------
 *
 * File: TablespaceInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\TablespaceInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class TablespaceInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
    private String name;
    private long oid;
    private String owner;
    private String location;
    private String acl;
    private String comment;
    private List<VariableInfoDTO> variableList;
    private List<SecurityLabelInfoDTO> securityLabelList;   

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.TABLESPACE;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        return name;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getLocation()
    {
        return location;
    }

    public void setLocation(String location)
    {
        this.location = location;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public List<VariableInfoDTO> getVariableList()
    {
        return variableList;
    }

    public void setVariableList(List<VariableInfoDTO> variableList)
    {
        this.variableList = variableList;
    }

    public List<SecurityLabelInfoDTO> getSecurityLabelList()
    {
        return securityLabelList;
    }

    public void setSecurityLabelList(List<SecurityLabelInfoDTO> securityLabelList)
    {
        this.securityLabelList = securityLabelList;
    }
    
}
