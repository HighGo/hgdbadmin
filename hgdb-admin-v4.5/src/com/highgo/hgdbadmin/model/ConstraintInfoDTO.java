/* ------------------------------------------------
 *
 * File: ConstraintInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ConstraintInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Yuanyuan
 */
public class ConstraintInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;    
    
     private String defineSQL;//all for add constraint in table creation, different for diffferent kind of  constraint    
    
    //for all
    private TreeEnum.TreeNode type;//PRIMARY KEY, FOREIGN KEY, UNIQUE, CHECK,EXCLUDE
    private Long oid;
    private String name;
    private String comment;    
    private Long indexOid;

    //for unique and PK
    private String usingIndex;//just for create
    //except check
    private String tablespace;   
    private String accessMethod;//btree, gist, hash, spgist
    private String fillFactor;
    private boolean deferrable;
    private boolean deferred;
    private String constraint;
    private List<ColInfoDTO> colInfoList;//column of Exclude has: operator class, desc/asc, nullsWhen, operator

    //fkey
    private String matchType;//f-FULL, p-PARTIAL, s-SIMPLE
    private Long refTabOid;
    private String refTable;//private ItemInfoDTO reftab;
    private List<ColInfoDTO> refColumnList;
    private String updateActions;
    private String deleteAction;
    private String coveringIndex;//create the index while create FK
    
     //fk and check
    private boolean validate;   

    //check
    private String checkConditonQuoted;
    private boolean noInherit;   

    private boolean enable = true;
    
    @Override
    public String getOwner()
    {
       return null;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(Long oid)
    {
        this.oid = oid;
    }
     
    @Override
    public TreeEnum.TreeNode getType()
    {
        return type;
    }

    public void setType(TreeEnum.TreeNode type)
    {
        this.type = type;
    }

    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getIndexOid()
    {
        return indexOid;
    }

    public void setIndexOid(Long indexOid)
    {
        this.indexOid = indexOid;
    }
    
    public List<ColInfoDTO> getColInfoList()
    {
        return colInfoList;
    }

    public void setColInfoList(List<ColInfoDTO> colInfoList)
    {
        this.colInfoList = colInfoList;
    }
    
    public String getAllColumnDefine()
    {
        if(this.colInfoList==null || this.colInfoList.size()<=0)
        {
            return "";
        }
        StringBuilder sql = new  StringBuilder();
        for(ColInfoDTO  col : this.colInfoList)
        {
            sql.append(SyntaxController.getInstance().getName(col.toColumnDefine())).append(",");
        }
        sql.deleteCharAt(sql.length()-1);
        return sql.toString();
    }
    
     public String getAllRelColumnDefine()
    {
        if(this.refColumnList==null || this.refColumnList.size()<=0)
        {
            return "";
        }
        StringBuilder sql = new  StringBuilder();
        sql.append(this.refTable).append("( ");
        for(ColInfoDTO  col : this.refColumnList)
        {
            sql.append(col.toColumnDefine()).append(",");
        }
        sql.deleteCharAt(sql.length()-1);
        sql.append(" )");        
        return sql.toString();
    }

    public String getUsingIndex()
    {
        return usingIndex;
    }

    public void setUsingIndex(String usingIndex)
    {
        this.usingIndex = usingIndex;
    }
    
    public boolean isDeferrable()
    {
        return deferrable;
    }

    public void setDeferrable(boolean deferrable)
    {
        this.deferrable = deferrable;
    }

    public boolean isDeferred()
    {
        return deferred;
    }

    public void setDeferred(boolean deferred)
    {
        this.deferred = deferred;
    }

    public String getTablespace()
    {
        return tablespace;
    }

    public void setTablespace(String tablespace)
    {
        this.tablespace = tablespace;
    }
      
    public String getFillFactor()
    {
        return fillFactor;
    }

    public void setFillFactor(String fillFactor)
    {
        this.fillFactor = fillFactor;
    }

    public String getMatchType()
    {
        return matchType;
    }

    public void setMatchType(String matchType)
    {
        this.matchType = matchType;
    }
    
    public String getCoveringIndex()
    {
        return coveringIndex;
    }

    public void setCoveringIndex(String coveringIndex)
    {
        this.coveringIndex = coveringIndex;
    }

    
    public boolean isValidate()
    {
        return validate;
    }

    public void setValidate(boolean validat)
    {
        this.validate = validat;
    }

    public List<ColInfoDTO> getRefColumnList()
    {
        return refColumnList;
    }

    public void setRefColList(List<ColInfoDTO> referenceColumn)
    {
        this.refColumnList = referenceColumn;
    }

    public String getRefTable()
    {
        return refTable;
    }

    public void setRefTable(String refTable)
    {
        this.refTable = refTable;
    }

    public Long getRefTabOid()
    {
        return refTabOid;
    }

    public void setRefTabOid(Long refTabOid)
    {
        this.refTabOid = refTabOid;
    }

    public String getUpdateActions()
    {
        return updateActions;
    }

    public void setUpdateActions(String updateActions)
    {
        this.updateActions = updateActions;
    }

    public String getDeleteAction()
    {
        return deleteAction;
    }

    public void setDeleteAction(String deleteAction)
    {
        this.deleteAction = deleteAction;
    }

    public String getAccessMethod()
    {
        return accessMethod;
    }

    public void setAccessMethod(String accessMethod)
    {
        this.accessMethod = accessMethod;
    }

    public String getConstraint()
    {
        return constraint;
    }

    public void setConstraint(String constraint)
    {
        this.constraint = constraint;
    }

    
    public String getDefineSQL()
    {
        //change  CHECK((id<0)) to  CHECK(id<0)
        if (defineSQL != null && !defineSQL.isEmpty()
                && defineSQL.toUpperCase().trim().startsWith("CHECK")
                && defineSQL.indexOf("((") >= 0 && defineSQL.indexOf("))") >= 0)
        {
            defineSQL = defineSQL.replace("((", "(").replace("))", ")");
        }
        return defineSQL;
    }

    public void setDefineSQL(String definition)
    {
        this.defineSQL = definition;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getCheckConditonQuoted()
    {
        return checkConditonQuoted;
    }

    public void setCheckConditonQuoted(String checkConditonQuoted)
    {
        this.checkConditonQuoted = checkConditonQuoted;
    }

    public boolean isNoInherit()
    {
        return noInherit;
    }

    public void setNoInherit(boolean noInherit)
    {
        this.noInherit = noInherit;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        if (getType() != null && getType().equals(TreeEnum.TreeNode.FOREIGN_KEY))
        {
            return this.name + "->" + this.refTable;
        } else
        {
            return this.name;
        }
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }
}
