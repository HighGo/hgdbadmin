/* ------------------------------------------------
 *
 * File: ProcedureInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，202002
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ProcedureInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 * @description it's just for diff with function, and isn't really be uesed.
 * @author liuyuanyuan
 */
public class ProcedureInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
    
    private Long oid;
    private String owner;
    private String simpleName;
    //private String returnType;
    //parameter
    private int inArgCount=0;
    private String inArgTypeOids;//get from db
    private String inArgTypes;//input arguments (including INOUT and VARIADIC arguments), for name
    private List<FunctionArgDTO> argList;
    //body code
    private String language;
    private String src;
    private String bin;//when language is C, need to specify bin, others not 
    //option
    //private String volatiles;
    //private Boolean returnSet;
    //private boolean strict;
    private boolean securityDefiner;
    //private boolean window;
    //private String parallel;
    //private int cost;
    //private boolean leafProof;
    
    
    //private boolean system;
    private String acl;
    private String comment;
    
    private String partDefine;
    
    private List<VariableInfoDTO> variableList;
    private List<SecurityLabelInfoDTO> securityLabelList;
    

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.PROCEDURE;
    }
    
    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    @Override
    public String getName()
    {
        String name = SyntaxController.getInstance().getName(this.simpleName);
        if (inArgCount == 0)
        {
            return name + "()";
        } else
        {
            return name + "(" + inArgTypes + ")";
        }
    }
    @Override
    public String toString()
    {
        if (inArgCount == 0)
        {
            return this.simpleName + "()";
        } else
        {
            return this.simpleName + "(" + inArgTypes + ")";
        }
    }

    //name without (argument1,argument..)
    public String getSimpleName()
    {
        return simpleName;
    }
    public void setSimpleName(String simpleName)
    {
        this.simpleName = simpleName;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }
    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }
    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public int getInArgCount()
    {
        return inArgCount;
    }
    public void setInArgCount(int inArgCount)
    {
        this.inArgCount = inArgCount;
    }

    public String getInArgTypeOids()
    {
        return inArgTypeOids;
    }
    public void setInArgTypeOids(String inArgTypeOid)
    {
        this.inArgTypeOids = inArgTypeOid;
    }
    
    public String getInArgTypes()
    {
        return inArgTypes;
    }
    public void setInArgTypes(String inArgTypes)
    {
        this.inArgTypes = inArgTypes;
    }
    
    public List<FunctionArgDTO> getArgList()
    {
        return argList;
    }
    public void setArgList(List<FunctionArgDTO> argList)
    {
        this.argList = argList;
    }
    public String getAllArgDefine()
    {
        StringBuilder str = new StringBuilder();
        if (this.argList!=null && !this.argList.isEmpty())
        {
            for (FunctionArgDTO arg : this.argList)
            {
                if (!arg.getMode().equals("TABLE"))
                {
                    str.append(arg.toString()).append(",");
                }
            }
            str.deleteCharAt(str.length() - 1);
        }
        return str.toString();
    }

    public String getLanguage()
    {
        return language;
    }
    public void setLanguage(String language)
    {
        this.language = language;
    }

    public String getSrc()
    {
        return src;
    }
    public void setSrc(String src)
    {
        this.src = src;
    }

    public String getBin()
    {
        return bin;
    }
    public void setBin(String bin)
    {
        this.bin = bin;
    }

    public boolean isSecurityDefiner()
    {
        return securityDefiner;
    }
    public void setSecurityDefiner(boolean securityDefiner)
    {
        this.securityDefiner = securityDefiner;
    }
    
    public String getAcl()
    {
        return acl;
    }
    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    @Override
    public String getComment()
    {
        return comment;
    }
    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public String getPartDefine()
    {
        return partDefine;
    }
    public void setPartDefine(String partDefine)
    {
        this.partDefine = partDefine;
    }

    public List<SecurityLabelInfoDTO> getSecurityLabelList()
    {
        return securityLabelList;
    }
    public void setSecurityLabelList(List<SecurityLabelInfoDTO> securityLabelList)
    {
        this.securityLabelList = securityLabelList;
    }

    public List<VariableInfoDTO> getVariableList()
    {
        return variableList;
    }
    public void setVariableList(List<VariableInfoDTO> variableList)
    {
        this.variableList = variableList;
    }  
}
