/* ------------------------------------------------
 *
 * File: ViewColumnInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\ViewColumnInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Yuanyuan
 */
public class ViewColumnInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
     
    private String name;
    private int position;
    private String dataType;
    private String defaultValue;    
    private String collation;
    private String acl;
    private String comment;  
    
    
    @Override
    public String getOwner()
    {
       return null;
    }
    
    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.VIEW_COLUMN;
    }
    
    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }

    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
     
    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return Long.valueOf(position);
    }
    
    public int getPosition()
    {
        return position;
    }

    public void setPosition(int position)
    {
        this.position = position;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        this.defaultValue = defaultValue;
    }

    public String getCollation()
    {
        return collation;
    }

    public void setCollation(String collation)
    {
        this.collation = collation;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public boolean isSystem()
    {
        return  helperInfo.getRelationOid() < helperInfo.getDatLastSysOid();
    }

}
