/* ------------------------------------------------
 *
 * File: SequenceInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\model\SequenceInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.model;

import com.highgo.hgdbadmin.util.TreeEnum;
import java.util.List;

/**
 *
 * @author Liu Yuanyuan
 */
public class SequenceInfoDTO extends AbstractObject
{
    private HelperInfoDTO helperInfo;
    private String schema;
    private Long schemaOid;
    
    private String name;
    private long oid;
    private String owner;
    private String acl;
    private String strCurrent;//for alter
    private String strIncrement;    
    private String strMinimum;
    private String strMaximum;
    private String strCache;
    private boolean cycled;
    private boolean called;
    private String comment;

    //for create
    private String strStart;//for create
    private List<SecurityLabelInfoDTO> securityLabelList;

    @Override
    public TreeEnum.TreeNode getType()
    {
        return TreeEnum.TreeNode.SEQUENCE;
    }

    @Override
    public boolean isSystem()
    {
        return this.oid < helperInfo.getDatLastSysOid();
    }

    @Override
    public String toString()
    {
        return this.name;
    }

    @Override
    public HelperInfoDTO getHelperInfo()
    {
        return helperInfo;
    }
    
    public void setHelperInfo(HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
    }
    
    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public long getSchemaOid()
    {
        return schemaOid;
    }

    public void setSchemaOid(long schemaOid)
    {
        this.schemaOid = schemaOid;
    }
    
    @Override
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public Long getOid()
    {
        return oid;
    }

    public void setOid(Long oid)
    {
        this.oid = oid;
    }

    @Override
    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getAcl()
    {
        return acl;
    }

    public void setAcl(String acl)
    {
        this.acl = acl;
    }

    public String getStrCurrent()
    {
        return strCurrent;
    }

    public void setStrCurrent(String current)
    {
        this.strCurrent = current;
    }

    public boolean isCycled()
    {
        return cycled;
    }

    public void setCycled(boolean cycled)
    {
        this.cycled = cycled;
    }

    public boolean isCalled()
    {
        return called;
    }

    public void setCalled(boolean called)
    {
        this.called = called;
    }

    @Override
    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
    
    public String getStrIncrement()
    {
        return strIncrement;
    }

    public void setStrIncrement(String strIncrement)
    {
        this.strIncrement = strIncrement;
    }

    public String getStrStart()
    {
        return strStart;
    }

    public void setStrStart(String strStart)
    {
        this.strStart = strStart;
    }

    public String getStrMinimum()
    {
        return strMinimum;
    }

    public void setStrMinimum(String strMinimum)
    {
        this.strMinimum = strMinimum;
    }

    public String getStrMaximum()
    {
        return strMaximum;
    }

    public void setStrMaximum(String strMaximum)
    {
        this.strMaximum = strMaximum;
    }

    public String getStrCache()
    {
        return strCache;
    }

    public void setStrCache(String strCache)
    {
        this.strCache = strCache;
    }

    public List<SecurityLabelInfoDTO> getSecurityLabelList()
    {
        return securityLabelList;
    }

    public void setSecurityLabelList(List<SecurityLabelInfoDTO> securityLabelList)
    {
        this.securityLabelList = securityLabelList;
    }    
}
