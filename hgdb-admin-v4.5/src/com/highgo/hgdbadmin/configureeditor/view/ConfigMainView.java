/* ------------------------------------------------
 *
 * File: ConfigMainView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\view\ConfigMainView.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.view;

import com.highgo.hgdbadmin.configureeditor.controller.ConfigController;
import com.highgo.hgdbadmin.configureeditor.model.HbaInfoDTO;
import com.highgo.hgdbadmin.configureeditor.model.PgpassInfoDTO;
import com.highgo.hgdbadmin.configureeditor.model.PostgresqlInfoDTO;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import com.highgo.hgdbadmin.util.HtmlPageHelper;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dom4j.DocumentException;
/**
 *
 * @author Liu Yunayuan
 */
public class ConfigMainView extends JFrame
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    private ResourceBundle msgBundle = ResourceBundle.getBundle("messages");

    //for all
    private String confName;            
    private DefaultTableModel tabModel;    
    private HelperInfoDTO helperInfo;
    private String inFile;

    private List<Integer> removeRowList; //for pg_hba.conf
    private List<PostgresqlInfoDTO> changedConfList; //for postgresql.conf
    /**
     * Creates new form OpenConfigureFileView
     * @param helperInfo
     * @param confName
     */
    public ConfigMainView(HelperInfoDTO helperInfo, String confName)
    {
        initComponents();
        this.helperInfo = helperInfo;
        this.confName = confName;
        logger.info(confName);
        switch(confName)
        {
            case "pg_hba.conf":
                this.initHbaView(confName);
                break;
            case "postgresql.conf":
                this.initPostgresqlView(confName);
                break;
            case "catalog.xml":
                this.initPgPasswordView(confName);
                break;        
//            case "pgpass.conf":
//                this.initPgpassView(confName);
//                break;
            default:
                logger.error(confName + " is an exception configuration file,do nothing and return;");
                return;
        }   
        tab.getColumn("").setMaxWidth(40);
        
        miExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                miExitActionPerformed(e);
            }
        });
        ActionListener helpAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                helpAction(e);
            }
        };
        miHelp.addActionListener(helpAction);
        miSuggestion.addActionListener(helpAction);
        btnSuggestion.addActionListener(helpAction);
        miConfigHelp.addActionListener(helpAction);
        btnConfigHelp.addActionListener(helpAction);
        miBugReport.addActionListener(helpAction);
    }
    private void initHbaView(String conf)
    {
        this.setTitle(constBundle.getString("hbaConfigEditor")+" - "+conf);
        String[] header = new String[]
        {
            "", constBundle.getString("type"), constBundle.getString("database"),
            constBundle.getString("user"), constBundle.getString("IPAddress"),
            constBundle.getString("method"), constBundle.getString("options")
        };
        tabModel = new DefaultTableModel(header, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                if (columnIndex == 0)
                {
                    return Boolean.class;
                } else
                {
                    return Object.class;
                }
            }

            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tab.setModel(tabModel);
        tab.getColumn("").setWidth(40);

        //action
        ActionListener openFileAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                openHbaFileAction();
            }
        };
        miOpen.addActionListener(openFileAction);
        btnOpen.addActionListener(openFileAction);

        tab.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tabMouseClickedHbaAction(evt);
            }
        });
        ActionListener deleteAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                deleteHbaRow();
            }
        };
        miDelete.addActionListener(deleteAction);
        btnDelete.addActionListener(deleteAction);

        ActionListener addAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                addHbaRow();
            }
        };
        miAdd.addActionListener(addAction);
        btnAdd.addActionListener(addAction);

        ActionListener revokeAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                refreshHbaTab();
            }
        };
        miRevoke.addActionListener(revokeAction);
        btnRevoke.addActionListener(revokeAction);
        ActionListener saveAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveHbaAction(e);
            }
        };
        miSave.addActionListener(saveAction);
        btnSave.addActionListener(saveAction);
        
        miSaveAs.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveAsHbaAction(e);
            }
        }); 
    }
    private void initPostgresqlView(String conf)
    {
        this.setTitle(constBundle.getString("postgresqlConfigEditor") + " - " + conf);
        btnAdd.setEnabled(false);
        miAdd.setEnabled(false);
        btnRevoke.setEnabled(false);
        btnDelete.setEnabled(false);
        miDelete.setEnabled(false);
        String[] header = new String[]
        {
            "", constBundle.getString("name"), constBundle.getString("value"),
            constBundle.getString("comment")
        };
        tabModel = new DefaultTableModel(header, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                if (columnIndex == 0)
                {
                    return Boolean.class;
                } else
                {
                    return Object.class;
                }
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tab.setModel(tabModel);
        tab.getColumn("").setWidth(40);
        
        //action 
        tab.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tabMouseClickedPostgresqlAction(evt);
            }
        });
        
        ActionListener revokeAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                refreshPostgresqlTab();
            }
        };
        miRevoke.addActionListener(revokeAction);
        btnRevoke.addActionListener(revokeAction);

        btnReloadConfig.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                reloadAction(e);
            }
        });

        ActionListener saveAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                savePostgresqlAction(e);
            }
        };
        miSave.addActionListener(saveAction);
        btnSave.addActionListener(saveAction);

        ActionListener openFileAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                openPostgresqlFileAction();
            }
        };
        miOpen.addActionListener(openFileAction);
        btnOpen.addActionListener(openFileAction);
        
        miSaveAs.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveAsPostgresqlAction(e);
            }
        });
    }
    private void initPgPasswordView(String conf)
    {
        logger.info("password view");
        this.setTitle(constBundle.getString("pgpassConfigureEditor") + " - " + conf);
        btnReloadConfig.setEnabled(false);
        btnAdd.setEnabled(false);
        miAdd.setEnabled(false);
        btnDelete.setEnabled(false);
        miDelete.setEnabled(false);
        String[] header = new String[]
        {
            "", "server", constBundle.getString("host"), constBundle.getString("port"),
            constBundle.getString("db"), constBundle.getString("user"), constBundle.getString("pwd")
        };
        tabModel = new DefaultTableModel(header, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                if (columnIndex == 0)
                {
                    return Boolean.class;
                } else
                {
                    return Object.class;
                }
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tab.setModel(tabModel);
        tab.getColumn("").setWidth(40);        
        tab.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tabMouseClickedPgPasswordAction(evt);
            }
        });
        
        ActionListener openFileAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                openPgPasswordFileAction();
            }
        };
        miOpen.addActionListener(openFileAction);
        btnOpen.addActionListener(openFileAction);
                
        ActionListener revokeAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                refreshPgPasswordTab();
            }
        };
        miRevoke.addActionListener(revokeAction);
        btnRevoke.addActionListener(revokeAction);
        
        ActionListener saveAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                savePgPasswordToFile(e);
            }
        };
        btnSave.addActionListener(saveAction);
        miSave.addActionListener(saveAction);
        miSaveAs.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveAsPgPassword(e);
            }
        });
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        tBar = new javax.swing.JToolBar();
        btnOpen = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        btnReloadConfig = new javax.swing.JButton();
        btnRevoke = new javax.swing.JButton();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnSuggestion = new javax.swing.JButton();
        btnConfigHelp = new javax.swing.JButton();
        jScrPane = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        txfdState = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        menufile = new javax.swing.JMenu();
        miOpen = new javax.swing.JMenuItem();
        miSave = new javax.swing.JMenuItem();
        miSaveAs = new javax.swing.JMenuItem();
        jsepSaveAs = new javax.swing.JPopupMenu.Separator();
        miReload = new javax.swing.JMenuItem();
        jsepReload = new javax.swing.JPopupMenu.Separator();
        miExit = new javax.swing.JMenuItem();
        menuEdit = new javax.swing.JMenu();
        miRevoke = new javax.swing.JMenuItem();
        jsepRevoke = new javax.swing.JPopupMenu.Separator();
        miDelete = new javax.swing.JMenuItem();
        miAdd = new javax.swing.JMenuItem();
        menuHelp = new javax.swing.JMenu();
        miHelp = new javax.swing.JMenuItem();
        miSuggestion = new javax.swing.JMenuItem();
        miConfigHelp = new javax.swing.JMenuItem();
        jsep = new javax.swing.JPopupMenu.Separator();
        miBugReport = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
            "/com/highgo/hgdbadmin/image/hgdb.png")));

tBar.setRollover(true);
tBar.setPreferredSize(new java.awt.Dimension(100, 30));

btnOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/file_open.png"))); // NOI18N
btnOpen.setToolTipText(constBundle.getString("openFile"));
btnOpen.setFocusable(false);
btnOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnOpen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnOpen);

btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/file_save.png"))); // NOI18N
btnSave.setToolTipText(constBundle.getString("saveFile"));
btnSave.setEnabled(false);
btnSave.setFocusable(false);
btnSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnSave);

btnReloadConfig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/gqbOrderAdd.png"))); // NOI18N
btnReloadConfig.setToolTipText(constBundle.getString("reloadConfiguration"));
btnReloadConfig.setFocusable(false);
btnReloadConfig.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnReloadConfig.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnReloadConfig);

btnRevoke.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/edit_undo.png"))); // NOI18N
btnRevoke.setToolTipText(constBundle.getString("undoModification"));
btnRevoke.setEnabled(false);
btnRevoke.setFocusable(false);
btnRevoke.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnRevoke.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnRevoke);

btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/property.png"))); // NOI18N
btnAdd.setFocusable(false);
btnAdd.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnAdd.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnAdd);

btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/delete.png"))); // NOI18N
btnDelete.setToolTipText(constBundle.getString("delRow"));
btnDelete.setEnabled(false);
btnDelete.setFocusable(false);
btnDelete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnDelete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnDelete);

btnSuggestion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/hint2.png"))); // NOI18N
btnSuggestion.setToolTipText(constBundle.getString("showObjSuggestion"));
btnSuggestion.setEnabled(false);
btnSuggestion.setFocusable(false);
btnSuggestion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnSuggestion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnSuggestion);

btnConfigHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/image/help.png"))); // NOI18N
btnConfigHelp.setToolTipText(constBundle.getString("showConfigureHelp"));
btnConfigHelp.setFocusable(false);
btnConfigHelp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
btnConfigHelp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
tBar.add(btnConfigHelp);

getContentPane().add(tBar, java.awt.BorderLayout.NORTH);

jScrPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
jScrPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
jScrPane.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

tab.setModel(new javax.swing.table.DefaultTableModel(
    new Object [][]
    {

    },
    new String []
    {

    }
    ));
    tab.setShowHorizontalLines(false);
    tab.setShowVerticalLines(false);
    //tab.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);  //单选
    jScrPane.setViewportView(tab);

    getContentPane().add(jScrPane, java.awt.BorderLayout.CENTER);

    txfdState.setPreferredSize(new java.awt.Dimension(20, 21));
    getContentPane().add(txfdState, java.awt.BorderLayout.PAGE_END);

    menufile.setText(constBundle.getString("file"));

    miOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
    miOpen.setText(constBundle.getString("open"));
    menufile.add(miOpen);

    miSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
    miSave.setText(constBundle.getString("save"));
    miSave.setEnabled(false);
    menufile.add(miSave);

    miSaveAs.setText(constBundle.getString("saveAs"));
    menufile.add(miSaveAs);
    menufile.add(jsepSaveAs);

    miReload.setText(constBundle.getString("reloadServer"));
    miReload.setEnabled(false);
    menufile.add(miReload);
    menufile.add(jsepReload);

    miExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
    miExit.setText(constBundle.getString("exit"));
    menufile.add(miExit);

    menuBar.add(menufile);

    menuEdit.setText(constBundle.getString("edit"));

    miRevoke.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Z, java.awt.event.InputEvent.CTRL_MASK));
    miRevoke.setText(constBundle.getString("revoke"));
    miRevoke.setEnabled(false);
    menuEdit.add(miRevoke);
    menuEdit.add(jsepRevoke);

    miDelete.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_DELETE, 0));
    miDelete.setText(constBundle.getString("del"));
    miDelete.setEnabled(false);
    menuEdit.add(miDelete);

    miAdd.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_INSERT, 0));
    miAdd.setText(constBundle.getString("add"));
    menuEdit.add(miAdd);

    menuBar.add(menuEdit);

    menuHelp.setText(constBundle.getString("help"));

    miHelp.setText(constBundle.getString("help"));
    menuHelp.add(miHelp);

    miSuggestion.setText(constBundle.getString("suggestion"));
    miSuggestion.setEnabled(false);
    menuHelp.add(miSuggestion);

    miConfigHelp.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
    miConfigHelp.setText(constBundle.getString("configureHelp"));
    menuHelp.add(miConfigHelp);
    menuHelp.add(jsep);

    miBugReport.setText(constBundle.getString("bugReport"));
    miBugReport.setEnabled(false);
    menuHelp.add(miBugReport);

    menuBar.add(menuHelp);

    setJMenuBar(menuBar);

    setSize(new java.awt.Dimension(750, 555));
    setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    private void miExitActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        this.dispose();
    }


   //for all
    private void reloadAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        int response = JOptionPane.showConfirmDialog(this, constBundle.getString("sureToReloadConfiguration"),
                constBundle.getString("reloadConfiguration"), JOptionPane.YES_NO_OPTION);
        if (response == JOptionPane.YES_OPTION)
        {
            TreeController tc = TreeController.getInstance();
            try
            {
                tc.reloadConfiguration(helperInfo);
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"),
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void helpAction(ActionEvent evt)
    {
        Object item = evt.getSource();
        if (item == miHelp)
        {
            HtmlPageHelper.getInstance().showPgHelpPage(this, "");
        } else if (item == miConfigHelp || item == btnConfigHelp)
        {
            switch (confName)
            {
                case "postgresql.conf":
                    HtmlPageHelper.getInstance().showPgHelpPage(this, "runtime-config.html");
                    break;
                case "pg_hba.conf":
                    HtmlPageHelper.getInstance().showPgHelpPage(this, "auth-pg-hba-conf.html");
                    break;
                case "pgpass.conf":
                    HtmlPageHelper.getInstance().showPgHelpPage(this, "libpq-pgpass.html");
                    break;
                default:
                    String error = confName + " is an exception config file, please check!";
                    logger.error(error);
                    JOptionPane.showMessageDialog(this, error,
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                    break;
            }
        } else if (item == miSuggestion || item == btnSuggestion)
        {
            //not defined
//            AbstractObject obj = (AbstractObject)node.getUserObject();   
//            SuggestionDialog suggestionDialog = new SuggestionDialog(this,true,obj.getType());
//            suggestionDialog.setLocationRelativeTo(this);
//            suggestionDialog.setVisible(true);
        } else if (item == miBugReport)
        {
            //not defined
//            uri = "http://www.pgadmin.org/docs/1.18/bugreport.html";
        } else
        {
            String error = item + " is an exception item, do nothing and return!";
            logger.error(error);
            JOptionPane.showMessageDialog(this, error,
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }

    private void emptyTable(DefaultTableModel model)
    {
        int row = model.getRowCount();
        if (row >= 0)
        {
            for (int i = row - 1; i >= 0; i--)
            {
                model.removeRow(i);
            }
        }
    }

    private String getSaveASPath()
    {
        FileChooserDialog fileChooser = new FileChooserDialog();
        int response = fileChooser.showFileChooser(this, constBundle.getString("confFileSaveAs"), "");
        if (response == JFileChooser.APPROVE_OPTION)
        {
            String outFile = fileChooser.getChoosedFileDir();
            logger.info(outFile);
            File f = new File(outFile);
            if (f.exists())
            {
                int resp = JOptionPane.showConfirmDialog(this, MessageFormat.format(constBundle.getString("confirmSave"), outFile),
                        constBundle.getString("confirmSaveTitle"), JOptionPane.OK_CANCEL_OPTION);
                if (resp == JOptionPane.CANCEL_OPTION)
                {
                    return null;
                } else
                {
                    return outFile;
                }
            } else
            {
                return outFile;
            }
        } else
        {
            return null;
        }
    }
    
    //for pg_hba.conf
    public void openHbaFileAction()
    {
        FileChooserDialog fileChooser = new FileChooserDialog();
        int response = fileChooser.showFileChooser(this, constBundle.getString("openConfigFile"), "");
        if (response == JFileChooser.APPROVE_OPTION)
        {
            inFile = fileChooser.getChoosedFileDir();
            this.getHbaContent(inFile);
        }
    }
    public void getHbaContent(String path)
    {
        inFile = path;
        removeRowList = new ArrayList();
        logger.info(path);
        this.emptyTable(tabModel);
        txfdState.setText(MessageFormat.format(msgBundle.getString("readFile"), path));
        ConfigController cec = ConfigController.getInstance();
        try
        {
            cec.readHbaFile(path, tabModel);
        } catch (IOException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void tabMouseClickedHbaAction(MouseEvent evt)
    {
        int row = tab.getSelectedRow();
        logger.info("selectedRow=" + row);
        if(row<0)
        {
            return;
        }
        btnDelete.setEnabled(true);
        miDelete.setEnabled(true);
        if (evt.getClickCount() == 2)
        {
            HbaInfoDTO hba = (HbaInfoDTO) tab.getValueAt(row, 2);
            HbaParameterDialog dialog = new HbaParameterDialog(this, true, helperInfo, hba);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if (dialog.isFinish())
            {
                if (hba.getState()==1)
                {
                    tab.setValueAt(hba.isEnable(), row, 0);
                    tab.setValueAt(hba.getConnectType(), row, 1);
                    tab.setValueAt(hba, row, 2);
                    tab.setValueAt(hba.getUser(), row, 3);
                    tab.setValueAt(hba.getIpAddress(), row, 4);
                    tab.setValueAt(hba.getMethod(), row, 5);
                    tab.setValueAt(hba.getOption(), row, 6);
                    btnSave.setEnabled(true);
                    miSave.setEnabled(true);
                    btnRevoke.setEnabled(true);
                    miRevoke.setEnabled(true);
                }
            }
        }
    }
    private void deleteHbaRow()
    {
        int row = tab.getSelectedRow();
        if(row>=0)
        {
            HbaInfoDTO hba = (HbaInfoDTO) tab.getValueAt(row, 2);
            if(hba.getLineNum()>0)
            {
                removeRowList.add(hba.getLineNum());
            }
            tabModel.removeRow(row);
            
            btnSave.setEnabled(true);
            miSave.setEnabled(true);
            miSaveAs.setEnabled(true);
            btnRevoke.setEnabled(true);
            miRevoke.setEnabled(true);
        }
    }
    private void addHbaRow()
    {       
        HbaParameterDialog dialog = new HbaParameterDialog(this, true, helperInfo, null);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        if (dialog.isFinish())
        {
             HbaInfoDTO hba = dialog.getHbaInfo();
            Object[] rowData = new Object[7];
            rowData[0] = hba.isEnable();
            rowData[1] = hba.getConnectType();
            rowData[2] = hba;
            rowData[3] = hba.getUser();
            rowData[4] = hba.getIpAddress();
            rowData[5] = hba.getMethod();
            rowData[6] = hba.getOption();
            tabModel.addRow(rowData);
            
            btnSave.setEnabled(true);
            miSave.setEnabled(true);
            miSaveAs.setEnabled(true);
            btnRevoke.setEnabled(true);
            miRevoke.setEnabled(true);
        }
    }
    private void refreshHbaTab()
    {
        this.getHbaContent(inFile);
        btnSave.setEnabled(false);
        miSave.setEnabled(false);
        btnRevoke.setEnabled(false);
        miRevoke.setEnabled(false);
    }

    private void saveHbaToFile(String outFie)
    {
        ConfigController cec = ConfigController.getInstance();
        List<HbaInfoDTO> hbaList = new ArrayList();
        int row = tabModel.getRowCount();
        HbaInfoDTO hba;
        for (int i = 0; i < row; i++)
        {
            hba = (HbaInfoDTO) tabModel.getValueAt(i, 2);
            if (hba.getState() != 0)
            {
                hbaList.add(hba);
            }
        }
        try
        {
            cec.saveHbaToFile(inFile, outFie, hbaList, removeRowList);
            txfdState.setText(MessageFormat.format(constBundle.getString("writeConfTo"), inFile));
            removeRowList = new ArrayList();
            btnSave.setEnabled(false);
            miSave.setEnabled(false);
            btnRevoke.setEnabled(false);
            miRevoke.setEnabled(false);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void saveHbaAction(ActionEvent evt)
    {
        this.saveHbaToFile(inFile);
    }
    private void saveAsHbaAction(ActionEvent evt)
    {
        String outFile = this.getSaveASPath();
        if (outFile != null)
        {
            this.saveHbaToFile(outFile);
        }
    }
    
    //for postgresql.conf
    public void openPostgresqlFileAction()
    {
        FileChooserDialog fileChooser = new FileChooserDialog();
        int response = fileChooser.showFileChooser(this, constBundle.getString("openConfigFile"), "");
        if (response == JFileChooser.APPROVE_OPTION)
        {
            inFile = fileChooser.getChoosedFileDir();
            this.getPostgresqlContent(inFile);
        }
    }
    public void getPostgresqlContent(String path)
    {
        inFile = path;
        logger.info(path);
        changedConfList = new ArrayList();        
        this.emptyTable(tabModel);
        txfdState.setText(MessageFormat.format(msgBundle.getString("readFile"), path));
        ConfigController cec = ConfigController.getInstance();
        try
        {
            cec.readPostgresqlFile(path, tabModel);
        } catch (IOException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void tabMouseClickedPostgresqlAction(MouseEvent evt)
    {
        int row = tab.getSelectedRow();
        logger.info("selectedRow=" + row);
        if(row<0)
        {
            return;
        }
        btnDelete.setEnabled(true);
        miDelete.setEnabled(true);
        if (evt.getClickCount() == 2)
        {
            PostgresqlInfoDTO conf = (PostgresqlInfoDTO) tab.getValueAt(row, 1);
            PostgresqlParameterDialog dialog = new PostgresqlParameterDialog(this, true, helperInfo, conf);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if (dialog.isFinish())
            {
                tab.setValueAt(conf.isEnable(), row, 0);
                tab.setValueAt(conf, row, 1);
                tab.setValueAt(conf.getValue(), row, 2);
                tab.setValueAt(conf.getComment(), row, 3);
                btnSave.setEnabled(true);
                miSave.setEnabled(true);
                btnRevoke.setEnabled(true);
                miRevoke.setEnabled(true);
                
                if (!this.isChangedEver(conf))
                {
                    changedConfList.add(conf);
                }
            }
        }
    }  
    private boolean isChangedEver(PostgresqlInfoDTO conf)
    {
        for (PostgresqlInfoDTO oldConf : changedConfList)
        {
            if (oldConf.getLineNum() == conf.getLineNum())
            {
                oldConf = conf;
                logger.info("ever changed");
                return true;
            }
        }
        return false;
    }
    private void refreshPostgresqlTab()
    {
        this.getPostgresqlContent(inFile);
        btnSave.setEnabled(false);
        miSave.setEnabled(false);
        btnRevoke.setEnabled(false);
        miRevoke.setEnabled(false);
    }    
    
    private void savePostgresqlToFile(String outFile)
    {
        try
        {
            ConfigController cec = ConfigController.getInstance();
            cec.savePostgresqlToFile(inFile, outFile, changedConfList);
            txfdState.setText(MessageFormat.format(constBundle.getString("writeConfTo"), inFile));
            changedConfList = new ArrayList();
            btnSave.setEnabled(false);
            miSave.setEnabled(false);
            btnRevoke.setEnabled(false);
            miRevoke.setEnabled(false);
        } catch (IOException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void savePostgresqlAction(ActionEvent evt)
    {
        this.savePostgresqlToFile(inFile);
    }
    private void saveAsPostgresqlAction(ActionEvent evt)
    {
        String outFile = this.getSaveASPath();
        if (outFile != null)
        {
            this.savePostgresqlToFile(outFile);
        }
    }
   
    //for catalog.xml
    private void tabMouseClickedPgPasswordAction(MouseEvent evt)
    {
        int row = tab.getSelectedRow();
        logger.info("selectedRow=" + row);
        if (row < 0)
        {
            return;
        }
        btnDelete.setEnabled(true);
        miDelete.setEnabled(true);
        if (evt.getClickCount() == 2)
        {
            XMLServerInfoDTO pgpass = (XMLServerInfoDTO) tab.getValueAt(row, 1);
            PgPasswordParameterDialog dialog = new PgPasswordParameterDialog(this, true, pgpass);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if (dialog.isFinish())
            {
                tab.setValueAt(pgpass.isSavePwd(), row, 0);
                tab.setValueAt(pgpass, row, 1);
                tab.setValueAt(pgpass.getHost(), row, 2);
                tab.setValueAt(pgpass.getPort(), row, 3);
                tab.setValueAt(pgpass, row, 4);
                tab.setValueAt(pgpass.getUser(), row, 5);
                tab.setValueAt(pgpass.getPwd(), row, 6);
                btnSave.setEnabled(true);
                miSave.setEnabled(true);
                btnRevoke.setEnabled(true);
                miRevoke.setEnabled(true);
            }
        }
    }
    public void openPgPasswordFileAction()
    {
        logger.info("pen passsword file");
        FileChooserDialog fileChooser = new FileChooserDialog();
        int response = fileChooser.showFileChooser(this, constBundle.getString("openConfigFile"), "");
        if (response == JFileChooser.APPROVE_OPTION)
        {
            inFile = fileChooser.getChoosedFileDir();
            this.getPgPasswordContent(inFile);
        }
    }
    public void getPgPasswordContent(String path)
    {
        inFile = path;
        logger.info(path);
        this.emptyTable(tabModel);
        txfdState.setText(MessageFormat.format(msgBundle.getString("readFile"), path));
        ConfigController cec = ConfigController.getInstance();
        try
        {
            cec.readPgPassword(inFile, tabModel);
        } catch (DocumentException ex)
        {
           JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    public void refreshPgPasswordTab()
    {
        this.getPgPasswordContent(inFile);
        btnSave.setEnabled(false);
        miSave.setEnabled(false);
        btnRevoke.setEnabled(false);
        miRevoke.setEnabled(false);
    }
    private void savePgPasswordToFile(ActionEvent evt)
    {
       this.savePgPasswordToFile(inFile);
    }    
    private void savePgPasswordToFile(String outFile)
    {
        ConfigController cec = ConfigController.getInstance();
        List<XMLServerInfoDTO> serverList = new ArrayList();
        int row = tabModel.getRowCount();
        XMLServerInfoDTO server;
        for (int i = 0; i < row; i++)
        {
            server = (XMLServerInfoDTO) tabModel.getValueAt(i, 1);
            serverList.add(server);
        }
        try
        {
            cec.savePgPasswordToFile(inFile, serverList,outFile);
            txfdState.setText(MessageFormat.format(constBundle.getString("writeConfTo"), outFile));
            btnSave.setEnabled(false);
            miSave.setEnabled(false);
            btnRevoke.setEnabled(false);
            miRevoke.setEnabled(false);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void saveAsPgPassword(ActionEvent evt)
    {
        String outFile = this.getSaveASPath();
        if (outFile != null)
        {
            this.savePgPasswordToFile(outFile);
        }
    }
    
    
    
    //the pgpass.conf has been replaced by catalog.xml
    //for pgpass.conf (useless now)
    private void initPgpassView(String conf)
    {
        this.setTitle(constBundle.getString("pgpassConfigureEditor") + " - " + conf);
        btnReloadConfig.setEnabled(false);
        String[] header = new String[]
        {
            "", constBundle.getString("host"), constBundle.getString("port"),
            constBundle.getString("db"), constBundle.getString("user"), constBundle.getString("pwd")
        };
        tabModel = new DefaultTableModel(header, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                if (columnIndex == 0)
                {
                    return Boolean.class;
                } else
                {
                    return Object.class;
                }
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        };
        tab.setModel(tabModel);
        tab.getColumn("").setWidth(40);
        
        tab.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tabMouseClickedPgpassAction(evt);
            }
        });
        
        ActionListener openFileAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                openPgpassFileAction();
            }
        };
        miOpen.addActionListener(openFileAction);
        btnOpen.addActionListener(openFileAction);
        
        ActionListener deleteAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                deletePgpassRow();
            }
        };
        miDelete.addActionListener(deleteAction);
        btnDelete.addActionListener(deleteAction);

        ActionListener addAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                addPgpassRow();
            }
        };
        miAdd.addActionListener(addAction);
        btnAdd.addActionListener(addAction);
        
        ActionListener revokeAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                refreshPgpassTab();
            }
        };
        miRevoke.addActionListener(revokeAction);
        btnRevoke.addActionListener(revokeAction);

        ActionListener saveAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                savePgpassAction(e);
            }
        };
        btnSave.addActionListener(saveAction);
        miSave.addActionListener(saveAction);
        miSaveAs.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                saveAsPgpassAction(e);
            }
        });        
    }
    
    private void tabMouseClickedPgpassAction(MouseEvent evt)
    {
        int row = tab.getSelectedRow();
        logger.info("selectedRow=" + row);
        if (row < 0)
        {
            return;
        }
        btnDelete.setEnabled(true);
        miDelete.setEnabled(true);
        if (evt.getClickCount() == 2)
        {
            PgpassInfoDTO pgpass = (PgpassInfoDTO) tab.getValueAt(row, 3);
            PgpassParameterDialog dialog = new PgpassParameterDialog(this, true, pgpass);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if (dialog.isFinish())
            {
                tab.setValueAt(pgpass.isEnable(), row, 0);
                tab.setValueAt(pgpass.getHost(), row, 1);
                tab.setValueAt(pgpass.getPort(), row, 2);
                tab.setValueAt(pgpass, row, 3);
                tab.setValueAt(pgpass.getUserName(), row, 4);
                tab.setValueAt(pgpass.getPassword(), row, 5);
                btnSave.setEnabled(true);
                miSave.setEnabled(true);
                btnRevoke.setEnabled(true);
                miRevoke.setEnabled(true);
            }
        }
    }
    public void openPgpassFileAction()
    {
        FileChooserDialog fileChooser = new FileChooserDialog();
        int response = fileChooser.showFileChooser(this, constBundle.getString("openConfigFile"), "");
        if (response == JFileChooser.APPROVE_OPTION)
        {
            inFile = fileChooser.getChoosedFileDir();
            this.getPgpassContent(inFile);
        }
    }

    public void getPgpassContent(String path)
    {
        inFile = path;
        logger.info(path);
        this.emptyTable(tabModel);
        txfdState.setText(MessageFormat.format(msgBundle.getString("readFile"), path));
        ConfigController cec = ConfigController.getInstance();
        try
        {
            cec.readPgpassFile(path, tabModel);
        } catch (IOException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void refreshPgpassTab()
    {
        this.getPgpassContent(inFile);
        btnSave.setEnabled(false);
        miSave.setEnabled(false);
    }

    private void addPgpassRow()
    {       
        PgpassParameterDialog dialog = new PgpassParameterDialog(this, true, null);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        if (dialog.isFinish())
        {
            PgpassInfoDTO pass = dialog.getPgpass();
            Object[] rowData = new Object[6];
            rowData[0] = pass.isEnable();
            rowData[1] = pass.getHost();
            rowData[2] = pass.getPort();
            rowData[3] = pass;
            rowData[4] = pass.getUserName();
            rowData[5] = pass.getPassword();
            tabModel.addRow(rowData);
            
            btnSave.setEnabled(true);
            miSave.setEnabled(true);
            miSaveAs.setEnabled(true);
            btnRevoke.setEnabled(true);
            miRevoke.setEnabled(true);
        }
    }
    private void deletePgpassRow()
    {
        int row = tab.getSelectedRow();
        logger.info("delete row:"+row);
        if(row>=0)
        {
            tabModel.removeRow(row);            
            btnSave.setEnabled(true);
            miSave.setEnabled(true);
            miSaveAs.setEnabled(true);
            btnRevoke.setEnabled(true);
            miRevoke.setEnabled(true);
        }
    }
        
    private void savePgpassToFile(String outFile)
    {
        ConfigController cec = ConfigController.getInstance();
        List<PgpassInfoDTO> passList = new ArrayList();
        int row = tabModel.getRowCount();
        PgpassInfoDTO pass;
        for (int i = 0; i < row; i++)
        {
            pass = (PgpassInfoDTO) tabModel.getValueAt(i, 3);
            passList.add(pass);
        }
        try
        {
            cec.savePgpassToFile(outFile, passList);
            txfdState.setText(MessageFormat.format(constBundle.getString("writeConfTo"), outFile));
            btnSave.setEnabled(false);
            miSave.setEnabled(false);
            btnRevoke.setEnabled(false);
            miRevoke.setEnabled(false);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void savePgpassAction(ActionEvent evt)
    {
        this.savePgpassToFile(inFile);
    }
    private void saveAsPgpassAction(ActionEvent evt)
    {
        String outFile = this.getSaveASPath();
        if (outFile != null)
        {
            this.savePgpassToFile(outFile);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnConfigHelp;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnOpen;
    private javax.swing.JButton btnReloadConfig;
    private javax.swing.JButton btnRevoke;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSuggestion;
    private javax.swing.JScrollPane jScrPane;
    private javax.swing.JPopupMenu.Separator jsep;
    private javax.swing.JPopupMenu.Separator jsepReload;
    private javax.swing.JPopupMenu.Separator jsepRevoke;
    private javax.swing.JPopupMenu.Separator jsepSaveAs;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuEdit;
    private javax.swing.JMenu menuHelp;
    private javax.swing.JMenu menufile;
    private javax.swing.JMenuItem miAdd;
    private javax.swing.JMenuItem miBugReport;
    private javax.swing.JMenuItem miConfigHelp;
    private javax.swing.JMenuItem miDelete;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miHelp;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miReload;
    private javax.swing.JMenuItem miRevoke;
    private javax.swing.JMenuItem miSave;
    private javax.swing.JMenuItem miSaveAs;
    private javax.swing.JMenuItem miSuggestion;
    private javax.swing.JToolBar tBar;
    private javax.swing.JTable tab;
    private javax.swing.JTextField txfdState;
    // End of variables declaration//GEN-END:variables
}
