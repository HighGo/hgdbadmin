/* ------------------------------------------------
 *
 * File: PostgresqlParameterDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\view\PostgresqlParameterDialog.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.view;

import com.highgo.hgdbadmin.configureeditor.controller.ConfigController;
import com.highgo.hgdbadmin.configureeditor.model.PostgresqlInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author Yuanyuan
 */
public class PostgresqlParameterDialog extends JDialog
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private JLabel lblEnable;
    private JLabel lblValue;
    private JLabel lblNotes;
    
    private JCheckBox cbEnable;
    private JTextField txfdValue;
    private JTextField txfdComment;
    private JLabel lblName;
    private JLabel lblCategory;
    private JTextArea ttarDescription;

    private JButton btnHelp;
    private JButton btnCancel;
    private JButton btnOk;
    
    private PostgresqlInfoDTO conf;
    private boolean isFinish;   

    public PostgresqlParameterDialog(Frame owner, boolean modal, HelperInfoDTO helperInfo, PostgresqlInfoDTO conf)
    {
        super(owner, modal);
        this.initComponent();
        this.conf = conf;
        isFinish = false;
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/image/property.png")));
        this.setSize(380, 360);
        
        cbEnable.setSelected(conf.isEnable());
        txfdValue.setText(conf.getValue());
        txfdComment.setText(conf.getComment());
        lblName.setText(conf.getName());
        ConfigController cc = ConfigController.getInstance();
        try
        {
            cc.getConfProperty(helperInfo, conf);
            lblCategory.setText(lblCategory.getText() + conf.getCategory());
            ttarDescription.setText(ttarDescription.getText() + conf.getDescription());
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }

        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                helpActionPerformed(e);
            }
        });
        btnOk.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkActionPerformed(e);
            }
        });
        btnCancel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancelActionPerform(e);
            }
        });  
    }

    private void initComponent()
    {
        btnOk = new JButton(constBundle.getString("ok"));
        btnCancel = new JButton(constBundle.getString("cancle"));
        btnHelp = new JButton(constBundle.getString("help"));
        
        lblEnable = new JLabel(constBundle.getString("enable"));
        lblValue = new JLabel(constBundle.getString("value"));
        lblNotes = new JLabel(constBundle.getString("notes"));
        
        cbEnable = new JCheckBox();
        txfdValue = new JTextField();
        txfdComment = new JTextField();
        
        lblName = new JLabel();
        Font font = new Font("bonjort_name", Font.BOLD, 13);        
        lblName.setFont(font);        
        
        lblCategory = new JLabel(constBundle.getString("category")+" ");
 
        //JLabel没有自动换行，将JTextArea伪装成JLabel,实现自动换行
        ttarDescription = new JTextArea(constBundle.getString("description") + " ");
        ttarDescription.setBackground(new Color(240, 240, 240));
        ttarDescription.setEditable(false);
        ttarDescription.setLineWrap(true);
        ttarDescription.setFont(lblCategory.getFont());
        
                
        JPanel paneSouth = new JPanel(new FlowLayout(FlowLayout.LEFT));        
        paneSouth.add(btnHelp);
        paneSouth.add(btnOk);
        paneSouth.add(btnCancel);
        this.add(paneSouth, BorderLayout.SOUTH);
        
        JPanel paneMain = new JPanel();
        this.add(paneMain, BorderLayout.NORTH);
        GridBagLayout layout = new GridBagLayout();
        paneMain.setLayout(layout);
        GridBagConstraints contraint = new GridBagConstraints();
        contraint.fill = GridBagConstraints.BOTH;
        contraint.insets = new Insets(5, 5, 5, 5);

        contraint.gridx = 0;
        contraint.gridy = 0;
        contraint.weightx = 0.0;
        contraint.gridwidth = 4;
        paneMain.add(lblName, contraint);
        
        contraint.gridx = 0;
        contraint.gridy = 1;
        contraint.weightx = 0;
        contraint.gridwidth = 1;
        paneMain.add(lblEnable, contraint);

        contraint.gridx = 1;
        contraint.gridy = 1;
        contraint.weightx = 1.0;
        contraint.gridwidth = 4;
        paneMain.add(cbEnable, contraint);

        contraint.gridx = 0;
        contraint.gridy = 2;
        contraint.weightx = 0;
        contraint.gridwidth = 1;
        paneMain.add(lblValue, contraint);

        contraint.gridx = 1;
        contraint.gridy = 2;
        contraint.weightx = 1.0;
        contraint.gridwidth = 4;
        paneMain.add(txfdValue, contraint);

        contraint.gridx = 0;
        contraint.gridy = 3;
        contraint.weightx = 0;
        contraint.gridwidth = 1;
        paneMain.add(lblNotes, contraint);

        contraint.gridx = 1;
        contraint.gridy = 3;
        contraint.weightx = 1.0;
        contraint.gridwidth = 4;
        paneMain.add(txfdComment, contraint);

        contraint.gridx = 0;
        contraint.gridy = 4;
        contraint.weightx = 0.0;
        contraint.gridwidth = 3;
        paneMain.add(lblCategory, contraint);

        contraint.insets = new Insets(0, 5, 5, 5);
        contraint.gridx = 0;
        contraint.gridy = 5;
        contraint.weightx = 1.0;
        contraint.gridwidth = 3;
        paneMain.add(ttarDescription, contraint);
    }
    private void helpActionPerformed(ActionEvent e)
    {
        try
        {
            Desktop.getDesktop().browse(new URI("http://www.postgresql.org/docs/current/static/runtime-config.html"));
        } catch (IOException | URISyntaxException ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void btnCancelActionPerform(ActionEvent e)
    {
       logger.info(e.getActionCommand());
       this.dispose();
    }
    private void btnOkActionPerformed(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.setProperty();
        isFinish = true;
        this.dispose();
    }
    private void setProperty()
    {
        conf.setEnable(cbEnable.isSelected());
        conf.setValue(txfdValue.getText());
        conf.setComment(txfdComment.getText());
    }
    public boolean isFinish()
    {
        return isFinish;
    }

}
