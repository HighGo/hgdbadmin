/* ------------------------------------------------
 *
 * File: PgPasswordParameterDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\view\PgPasswordParameterDialog.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.view;

import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Liu Yuanyuan
 */
public class PgPasswordParameterDialog extends JDialog
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
        
    private JCheckBox cbEnable;
    private JTextField txfHost;
    private JTextField txfPort;
    private JTextField txfDatabase;
    private JTextField txfUser;
    private JPasswordField psfPwd;
    private JPasswordField psfPwdAgain;
    private JButton btnOk;
    private JButton btnCancel;
    
    private XMLServerInfoDTO pgpass;
    private boolean isFinish;

    public PgPasswordParameterDialog(Frame owner, boolean modal, XMLServerInfoDTO pgpass)
    {
        super(owner, modal);
        this.initComponent();
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/image/property.png")));
        this.setSize(280, 300);
        if(pgpass==null)
        {
            logger.error("Enter null serverInfo, do nothing and return.");
            this.dispose();
            return;
        }
        isFinish = false;
        this.pgpass = pgpass;
        cbEnable.setSelected(pgpass.isSavePwd());
        txfHost.setText(pgpass.getHost());
        txfPort.setText(pgpass.getPort());
        txfDatabase.setText(pgpass.getMaintainDB());
        txfUser.setText(pgpass.getUser());
        psfPwd.setText(pgpass.getPwd());
        psfPwdAgain.setText(pgpass.getPwd());
        
        cbEnable.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkEnableAction();
            }
        });
        KeyAdapter btnOkEnable = new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                btnOkEnableAction();
            }
        };
        txfHost.addKeyListener(btnOkEnable);
        txfPort.addKeyListener(btnOkEnable);
        txfDatabase.addKeyListener(btnOkEnable);
        txfUser.addKeyListener(btnOkEnable);
        psfPwd.addKeyListener(btnOkEnable);
        psfPwdAgain.addKeyListener(btnOkEnable);

        btnOk.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkActionPerformed(e);
            }
        });
        btnCancel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancelActionPerformed(e);
            }
        });
    }
    
    private void initComponent()
    {
        JLabel labEnable = new JLabel(constBundle.getString("enable"));
        JLabel labHost = new JLabel(constBundle.getString("host"));
        JLabel labPort = new JLabel(constBundle.getString("port"));
        JLabel labDatabase = new JLabel(constBundle.getString("db"));
        JLabel labUserName = new JLabel(constBundle.getString("user"));
        JLabel labPassword = new JLabel(constBundle.getString("pwd"));
        JLabel labPasswordAgain = new JLabel(constBundle.getString("pwdAgain"));

        cbEnable = new JCheckBox();
        txfHost = new JTextField();
        txfPort = new JTextField();
        txfDatabase = new JTextField();
        txfUser = new JTextField();
        psfPwd = new JPasswordField();
        psfPwdAgain = new JPasswordField();
        
        txfHost.setEditable(false);
        txfPort.setEditable(false);
        txfDatabase.setEditable(false);
        txfUser.setEditable(false);                

        btnCancel = new JButton("取消(C)");
        btnOk = new JButton("确定(O)");
        btnOk.setEnabled(false);        

        JPanel paneSouth = new JPanel();
        this.add(paneSouth, BorderLayout.SOUTH);
        paneSouth.setLayout(new FlowLayout(FlowLayout.RIGHT));
        paneSouth.add(btnOk);
        paneSouth.add(btnCancel);
               
        JPanel paneMain = new JPanel();
        this.add(paneMain, BorderLayout.NORTH);
        GridBagLayout layout = new GridBagLayout();
        paneMain.setLayout(layout);
        GridBagConstraints constraint = new GridBagConstraints();
        constraint.fill = GridBagConstraints.BOTH;
        constraint.insets = new Insets(5, 5, 5, 5);
        
        constraint.gridx = 0;
        constraint.gridy = 0;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labEnable, constraint);

        constraint.gridx = 1;
        constraint.gridy = 0;
        constraint.weightx = 1;
        constraint.gridwidth = 3;
        paneMain.add(cbEnable, constraint);

        constraint.gridx = 0;
        constraint.gridy = 1;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labHost, constraint);

        constraint.gridx = 1;
        constraint.gridy = 1;
        constraint.weightx = 1.0;
        constraint.gridwidth = 3;
        paneMain.add(txfHost, constraint);

        constraint.gridx = 0;
        constraint.gridy = 2;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labPort, constraint);

        constraint.gridx = 1;
        constraint.gridy = 2;
        constraint.weightx = 1.0;
        constraint.gridwidth = 3;
        paneMain.add(txfPort, constraint);

        constraint.gridx = 0;
        constraint.gridy = 3;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labDatabase, constraint);

        constraint.gridx = 1;
        constraint.gridy = 3;
        constraint.weightx = 1.0;
        constraint.gridwidth = 3;
        paneMain.add(txfDatabase, constraint);

        constraint.gridx = 0;
        constraint.gridy = 4;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labUserName, constraint);

        constraint.gridx = 1;
        constraint.gridy = 4;
        constraint.weightx = 1.0;
        constraint.gridwidth = 3;
        paneMain.add(txfUser, constraint);

        constraint.gridx = 0;
        constraint.gridy = 5;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labPassword, constraint);

        constraint.gridx = 1;
        constraint.gridy = 5;
        constraint.weightx = 1.0;
        constraint.gridwidth = 3;
        paneMain.add(psfPwd, constraint);

        constraint.gridx = 0;
        constraint.gridy = 6;
        constraint.weightx = 0;
        constraint.gridwidth = 1;
        paneMain.add(labPasswordAgain, constraint);

        constraint.gridx = 1;
        constraint.gridy = 6;
        constraint.weightx = 1.0;
        constraint.gridwidth = 3;
        paneMain.add(psfPwdAgain, constraint);
    }
    
    private void btnOkEnableAction()
    {
        String host = txfHost.getText();
        String port = txfPort.getText();
        String database = txfDatabase.getText();
        String user = txfUser.getText();
        char[] pwd = psfPwd.getPassword();
        char[] pwdAgain = psfPwdAgain.getPassword();
        logger.info(cbEnable.isSelected() + "," + host + "," + port + "," + "database"
                + "," + user + "," + String.valueOf(pwd) + "," + String.valueOf(pwdAgain));
        if (host.isEmpty()
                || port.isEmpty()
                || database.isEmpty()
                || user.isEmpty()
                || pwd.length <= 0
                || pwdAgain.length <= 0)
        {
            btnOk.setEnabled(false);
        } else if (!Arrays.equals(pwd, pwdAgain))
        {
            btnOk.setEnabled(false);
        } else if (pgpass.isSavePwd() == true
                && cbEnable.isSelected() == true
//                && pgpass.getHost().equals(host)
//                && pgpass.getPort().equals(port)
//                && pgpass.getMaintainDB().equals(database)
//                && pgpass.getUser().equals(user)
                && pgpass.getPwd().equals(String.valueOf(pwd)))
        {
            btnOk.setEnabled(false);
        } else
        {
            btnOk.setEnabled(true);
        }
    }

    private void btnOkActionPerformed(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        pgpass.setSavePwd(cbEnable.isSelected());
        pgpass.setHost(txfHost.getText());
        pgpass.setPort(txfPort.getText());
        pgpass.setMaintainDB(txfDatabase.getText());
        pgpass.setUser(txfUser.getText());
        pgpass.setPwd(String.valueOf(psfPwd.getPassword()));
        try
        {
            String url = "jdbc:highgo://" + pgpass.getHost() + ":" + pgpass.getPort() + "/" + pgpass.getMaintainDB();
            logger.info(url);
            JdbcHelper.getConnection(url, pgpass);
            isFinish = true;
            this.dispose();
        } catch (ClassNotFoundException | SQLException ex)
        {
            logger.info(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex, constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void btnCancelActionPerformed(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.dispose();
    }

    public boolean isFinish()
    {
        return this.isFinish;
    }
    public XMLServerInfoDTO getPgpass()
    {
        return this.pgpass;
    }
}
