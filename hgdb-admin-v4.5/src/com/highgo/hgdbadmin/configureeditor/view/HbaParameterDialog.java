/* ------------------------------------------------
 *
 * File: HbaParameterDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\view\HbaParameterDialog.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.view;

import com.highgo.hgdbadmin.configureeditor.model.HbaInfoDTO;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.util.TreeEnum;

/**
 *
 * @author Liu Yuanyuan
 */
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/*
* @author yuanyuan
*
*/
public class HbaParameterDialog extends JDialog
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private JButton btnHelp;
    private JButton btnOk;
    private JButton btnCancel;
    
    private JCheckBox cbEnable;
    private JComboBox cbbType;
    private JComboBox cbbDatabase;
    private JComboBox cbbUser;
    private JTextField txfdIp;
    private JComboBox cbbMethod;
    private JTextField txfdOption;
    
    private HbaInfoDTO hbaInfo; 
    private boolean isFinish;

    public HbaParameterDialog(Frame owner, boolean modal, HelperInfoDTO helperInfo, HbaInfoDTO hbaInfo)
    {
        super(owner, modal);
        this.hbaInfo = hbaInfo;
        isFinish = false;
        this.initComponent();
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/property.png")));
        this.setTitle(constBundle.getString("parameterConfig"));
        this.setSize(350, 350);
        btnOk.setEnabled(hbaInfo!=null);
        cbbType.setEditable(false);
        cbbType.setModel(new DefaultComboBoxModel(new String[]
        {
            "local",
            "host", 
            "hostssl",
            "hostnossl"
        }));             
        cbbDatabase.setModel(new DefaultComboBoxModel(new String[]
        {
            "all",
            "sameuser",
            "@<filename>"
        }));
        
        TreeController tc = TreeController.getInstance();
        if (helperInfo != null)
        {
            TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
            String version = helperInfo.getVersionNumber();
            logger.info(dbsys + "," + version);
            if (tc.isVersionHigherThan(dbsys, version, 8, 0))
            {
                cbbDatabase.addItem("samerole");
            } else
            {
                cbbDatabase.addItem("samegroup");
            }
            if (tc.isVersionHigherThan(dbsys, version, 8, 4))
            {
                cbbDatabase.addItem("replication");
            }
            try
            {
                for (String db : tc.getDBNameList(helperInfo))
                {
                    cbbDatabase.addItem(db);
                }
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"),
                        JOptionPane.OK_OPTION);
            }
        } else
        {
             cbbDatabase.addItem("samerole");
             cbbDatabase.addItem("samegroup");
             cbbDatabase.addItem("replication");            
        }
        cbbUser.setModel(new DefaultComboBoxModel(new String[]
        {
            "all"
        }));
        if (helperInfo != null)
        {
            try
            {
                for (String user : tc.getUserNameList(helperInfo))
                {
                    cbbUser.addItem(user);
                }
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"),
                        JOptionPane.ERROR_MESSAGE);
            }
        }

        cbbMethod.setEditable(false);
        cbbMethod.setModel(new DefaultComboBoxModel(new String[]
        {
            "trust",
            "reject",
            "md5",
            "password",
            "krb4",
            "krb5",
            "ident",
            "pam"
        }));
        
        if (helperInfo != null)
        {
            TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
            String version = helperInfo.getVersionNumber();
            logger.info(dbsys + "," + version);
            if (tc.isVersionHigherThan(dbsys, version, 8, 1))
            {
                cbbMethod.addItem("ldap");
            }
            if (tc.isVersionHigherThan(dbsys, version, 8, 2))
            {
                cbbMethod.addItem("gss");
                cbbMethod.addItem("sspi");
            }
            if (tc.isVersionHigherThan(dbsys, version, 8, 3))
            {
                cbbMethod.addItem("cert");
            } else
            {
                cbbMethod.addItem("crypt");
            }
            if (tc.isVersionHigherThan(dbsys, version, 8, 4))
            {
                cbbMethod.addItem("radius");
            }
            if (tc.isVersionHigherThan(dbsys, version, 9, 0))
            {
                cbbMethod.addItem("peer");
            }
        }else
        {
            cbbMethod.addItem("ldap");
            cbbMethod.addItem("gss");
            cbbMethod.addItem("sspi");
            cbbMethod.addItem("cert");
            cbbMethod.addItem("crypt");
            cbbMethod.addItem("radius");
            cbbMethod.addItem("peer");
        }        
        txfdOption.setEnabled(false);

        if (hbaInfo != null)
        {
            cbEnable.setSelected(hbaInfo.isEnable());
            cbbType.setSelectedItem(hbaInfo.getConnectType());
            cbbDatabase.setSelectedItem(hbaInfo.getDatabase());
            cbbUser.setSelectedItem(hbaInfo.getUser());
            txfdIp.setText(hbaInfo.getIpAddress());
            cbbMethod.setSelectedItem(hbaInfo.getMethod());
            txfdOption.setText(hbaInfo.getOption());
        }else
        {
             cbEnable.setSelected(false);
             cbbType.setSelectedIndex(-1);
             cbbDatabase.setSelectedIndex(-1);
             cbbUser.setSelectedIndex(-1);
             txfdIp.setText("");
             cbbMethod.setSelectedIndex(-1);
             txfdOption.setText("");
        }

        //enable btnOk
        ItemListener enableBtnOkAction = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                enableBtnOk();
            }
        };
        cbbType.addItemListener(enableBtnOkAction);
        cbbDatabase.addItemListener(enableBtnOkAction);
        cbbUser.addItemListener(enableBtnOkAction);
        txfdIp.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                enableBtnOk();
            }
        });
        cbbMethod.addItemListener(enableBtnOkAction);      
        
        btnHelp.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                try
                {
                    Desktop.getDesktop().browse(
                            new URI("http://www.postgresql.org/docs/current/static/client-authentication.html"));
                } catch (URISyntaxException | IOException e)
                {
                    logger.error(e.getMessage());
                    e.printStackTrace(System.out);
                }
            }
        });
        btnOk.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnOkActionPerformed(e);
            }
        });        
        btnCancel.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancelActionPerformed(e);
            }
        });
    }
    
    private void initComponent()
    {
        JLabel lblEnable = new JLabel(constBundle.getString("enable"));
        JLabel lblType = new JLabel(constBundle.getString("types"));
        JLabel lblDatabase = new JLabel(constBundle.getString("database"));
        JLabel lblUser = new JLabel(constBundle.getString("user"));
        JLabel lblIpAdd = new JLabel(constBundle.getString("IPAddress"));
        JLabel lblMethod = new JLabel(constBundle.getString("method"));
        JLabel lblOption = new JLabel(constBundle.getString("options"));

        btnHelp = new JButton(constBundle.getString("help"));
        btnOk = new JButton(constBundle.getString("ok"));
        btnCancel = new JButton(constBundle.getString("cancle"));
        
        cbEnable = new JCheckBox();
        cbbType = new JComboBox();
        cbbDatabase = new JComboBox();
        cbbUser = new JComboBox();
        txfdIp = new JTextField();
        cbbMethod = new JComboBox();
        txfdOption = new JTextField();

        JPanel pnlNorth = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblEnable, gbc);

        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(cbEnable, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblType, gbc);

        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(cbbType, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblDatabase, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(cbbDatabase, gbc);

        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblUser, gbc);

        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(cbbUser, gbc);

        gbc.gridx = 0;
        gbc.gridy = 4;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblIpAdd, gbc);

        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(txfdIp, gbc);

        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblMethod, gbc);

        gbc.gridx = 1;
        gbc.gridy = 5;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(cbbMethod, gbc);

        gbc.gridx = 0;
        gbc.gridy = 6;
        gbc.weightx = 0.0;
        gbc.gridwidth = 1;
        pnlNorth.add(lblOption, gbc);

        gbc.gridx = 1;
        gbc.gridy = 6;
        gbc.weightx = 1.0;
        gbc.gridwidth = 3;
        pnlNorth.add(txfdOption, gbc);
        this.getContentPane().add(pnlNorth, BorderLayout.NORTH);

        JPanel paneSouth = new JPanel(new FlowLayout(FlowLayout.LEFT));
        paneSouth.add(btnHelp);
        paneSouth.add(btnOk);
        paneSouth.add(btnCancel);
        this.add(paneSouth, BorderLayout.SOUTH);
    }
    private void btnCancelActionPerformed(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.dispose();
    }
    private void btnOkActionPerformed(ActionEvent e)
    {
        logger.info(e.getActionCommand());
        this.setNewHba();
        this.dispose();
        isFinish = true;
    }
    private void enableBtnOk()
    {
        if(hbaInfo==null)
        {
            if (cbbType.getSelectedItem() != null
                    && cbbDatabase.getSelectedItem() != null
                    && cbbUser.getSelectedItem() != null
                    && !txfdIp.getText().isEmpty()
                    && cbbMethod.getSelectedItem() != null)
            {
                btnOk.setEnabled(true); 
            }else
            {
                btnOk.setEnabled(false);
            }
        } else
        {
            btnOk.setEnabled(this.isChanged());            
        }
    }
    private boolean isChanged()
    {
        if (cbEnable.isSelected() == hbaInfo.isEnable()
                && cbbType.getSelectedItem().toString().equals(hbaInfo.getConnectType())
                && cbbDatabase.getSelectedItem().toString().equals(hbaInfo.getDatabase())
                && cbbUser.getSelectedItem().toString().equals(hbaInfo.getUser())
                && txfdIp.getText().equals(hbaInfo.getIpAddress())
                && cbbMethod.getSelectedItem().toString().equals(hbaInfo.getMethod()))
        {
            return false;
        } else
        {
            return true;
        }
    }
    
    public boolean isFinish()
    {
        return this.isFinish;
    }
    
    public HbaInfoDTO getHbaInfo()
    {
        return hbaInfo;
    }
    
    private void setNewHba()
    {
        if (hbaInfo == null)
        {
            hbaInfo = new HbaInfoDTO();
            hbaInfo.setState(2);
        } else
        {
            if (this.isChanged())
            {
                hbaInfo.setState(1);
            }
        }
        hbaInfo.setEnable(cbEnable.isSelected());
        hbaInfo.setConnectType(cbbType.getSelectedItem().toString());
        hbaInfo.setDatabase(cbbDatabase.getSelectedItem().toString());
        hbaInfo.setUser(cbbUser.getSelectedItem().toString());
        hbaInfo.setIpAddress(txfdIp.getText());
        hbaInfo.setMethod(cbbMethod.getSelectedItem().toString());
    }
}
