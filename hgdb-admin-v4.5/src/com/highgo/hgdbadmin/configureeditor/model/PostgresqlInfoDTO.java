/* ------------------------------------------------
 *
 * File: PostgresqlInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\model\PostgresqlInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.model;

/**
 *
 * @author Liu Yunayuan
 */
public class PostgresqlInfoDTO
{
    private boolean commentLine;
    private boolean enable;
    private String name;
    private String value;
    private String comment;
    private int lineNum;
    private String orgText;
    
    //other property queried from db server
    private String category;
    private String description;
    
    
    //useless until now
    private String showComment, dfValue, dfEnabled, tranComment;   
    private boolean isCatalog, inSection;
    private boolean changed = false;
    private boolean isRestart = false;   
    private boolean g_IsCommEnd = false;
    private boolean isRepeat = false;
    private int lstIndex;
    private String method;

    @Override
    public String toString()
    {
        return this.name;
    }

    public boolean isCommentLine()
    {
        return commentLine;
    }

    public void setCommentLine(boolean commentLine)
    {
        this.commentLine = commentLine;
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean enable)
    {
        this.enable = enable;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public int getLineNum()
    {
        return lineNum;
    }

    public void setLineNum(int lineNum)
    {
        this.lineNum = lineNum;
    }

    public String getOrgText()
    {
        return orgText;
    }

    public void setOrgText(String orgText)
    {
        this.orgText = orgText;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getShowComment()
    {
        return showComment;
    }

    public void setShowComment(String showComment)
    {
        this.showComment = showComment;
    }

    public String getDfValue()
    {
        return dfValue;
    }

    public void setDfValue(String dfValue)
    {
        this.dfValue = dfValue;
    }

    public String getDfEnabled()
    {
        return dfEnabled;
    }

    public void setDfEnabled(String dfEnabled)
    {
        this.dfEnabled = dfEnabled;
    }

    public String getTranComment()
    {
        return tranComment;
    }

    public void setTranComment(String tranComment)
    {
        this.tranComment = tranComment;
    }

    public boolean isIsCatalog()
    {
        return isCatalog;
    }

    public void setIsCatalog(boolean isCatalog)
    {
        this.isCatalog = isCatalog;
    }

    public boolean isInSection()
    {
        return inSection;
    }

    public void setInSection(boolean inSection)
    {
        this.inSection = inSection;
    }

    public boolean isChanged()
    {
        return changed;
    }

    public void setChanged(boolean changed)
    {
        this.changed = changed;
    }

    public boolean isIsRestart()
    {
        return isRestart;
    }

    public void setIsRestart(boolean isRestart)
    {
        this.isRestart = isRestart;
    }

    public boolean isG_IsCommEnd()
    {
        return g_IsCommEnd;
    }

    public void setG_IsCommEnd(boolean g_IsCommEnd)
    {
        this.g_IsCommEnd = g_IsCommEnd;
    }

    public boolean isIsRepeat()
    {
        return isRepeat;
    }

    public void setIsRepeat(boolean isRepeat)
    {
        this.isRepeat = isRepeat;
    }

    public int getLstIndex()
    {
        return lstIndex;
    }

    public void setLstIndex(int lstIndex)
    {
        this.lstIndex = lstIndex;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }
    

    
}
