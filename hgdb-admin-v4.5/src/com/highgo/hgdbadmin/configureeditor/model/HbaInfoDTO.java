/* ------------------------------------------------
 *
 * File: HbaInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\model\HbaInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.model;

/**
 *
 * @author Liu Yunayuan
 */
public class HbaInfoDTO 
{  
    private boolean enable;     
    private String connectType;
    private String database;  
    private String user;
    private String ipAddress;
    private String method;
    private String option = null;
    
    private boolean comment;
    private int lineNum=0;//normal>=1, added=0
    
    private int state = 0; // -1 removed, 0 normal, 1 changed, 2 added.
         
    public boolean isComment()
    {
        return comment;
    }

    public void setComment(boolean isComment)
    {
        this.comment = isComment;
    }

    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean isEnable)
    {
        this.enable = isEnable;
    }

    public String getConnectType()
    {
        return connectType;
    }

    public void setConnectType(String connectType)
    {
        this.connectType = connectType;
    }

    public void setDatabase(String database)
    {
        this.database = database;
    }

    public String getDatabase()
    {
        return database;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getUser()
    {
        return user;
    }

    public void setIpAddress(String ipAddress)
    {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress()
    {
        return ipAddress;
    }

    public String getMethod()
    {
        return method;
    }

    public void setMethod(String method)
    {
        this.method = method;
    }

    public void setOption(String option)
    {
        this.option = option;
    }

    public String getOption()
    {
        return option;
    }
    public int getLineNum()
    {
        return lineNum;
    }

    public void setLineNum(int lineNum)
    {
        this.lineNum = lineNum;
    }

    public int getState()
    {
        return state;
    }

    public void setState(int state)
    {
        this.state = state;
    }

    @Override
    public String toString()
    {
        return this.database;
    }
}
