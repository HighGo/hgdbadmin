/* ------------------------------------------------
 *
 * File: PgpassInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\model\PgpassInfoDTO.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.model;

/**
 *
 * @author Liu Yunayuan
 */
public class PgpassInfoDTO
{
    private boolean enable;
    private String host;
    private String port;
    private String database;
    private String userName;
    private String password;
    private boolean comment;

    @Override
    public String toString()
    {
        return this.database;
    }
    
    public boolean isEnable()
    {
        return enable;
    }

    public void setEnable(boolean isEnable)
    {
        this.enable = isEnable;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getDatabase()
    {
        return database;
    }

    public void setDatabase(String database)
    {
        this.database = database;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean isComment()
    {
        return comment;
    }

    public void setComment(boolean comment)
    {
        this.comment = comment;
    }

}
