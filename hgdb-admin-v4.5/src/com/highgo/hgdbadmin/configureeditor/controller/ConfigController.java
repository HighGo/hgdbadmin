/* ------------------------------------------------
 *
 * File: ConfigController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\configureeditor\controller\ConfigController.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.configureeditor.controller;

import com.highgo.hgdbadmin.configureeditor.model.HbaInfoDTO;
import com.highgo.hgdbadmin.configureeditor.model.PgpassInfoDTO;
import com.highgo.hgdbadmin.configureeditor.model.PostgresqlInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.util.CloseStream;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;


/**
 *
 * @author Liu Yunayuan
 */
public class ConfigController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private static ConfigController cc = null;    
    public static ConfigController getInstance()
    {
        if (cc == null)
        {
            cc = new ConfigController();
        }
        return cc;
    }

    /* for all */
    public String getPath(HelperInfoDTO helperInfo, String filename) throws ClassNotFoundException,SQLException
    {
        String path = null;
        String sql;
        switch (filename)
        {
            case "pg_hba.conf":
                sql = "show hba_file";
                break;
            case "postgresql.conf":
                sql = "show config_file";
                break;
            default:
                logger.error(filename + " is an exception file name , do nothing and return null.");
                return null;
        }
        logger.info(sql);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getMaintainDB(),
                    helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                path = rs.getString(1);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        return path;
    }

    /*
     * for pg_hba.conf
     */
    public void readHbaFile(String filePath, DefaultTableModel model) throws IOException
    {
        //logger.info("Enter =" + filePath);
        FileReader fr = null;
        BufferedReader br = null;
        try
        {
            fr = new FileReader(filePath);
            br = new BufferedReader(fr);
            
            int lineNum = 1;
            String line = br.readLine();
            HbaInfoDTO hba;
            while (line != null)
            {
                hba = new HbaInfoDTO();
                this.setHbaFromLine(line, hba);
                hba.setLineNum(lineNum);
                if (!hba.isComment())
                {
                    model.addRow(this.getRowDataFromHba(hba));
                }
                lineNum++;
                line = br.readLine();
            }
        } catch (IOException ex)
        {
            logger.error("Error :" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        } finally
        {            
            CloseStream.close(br);
            CloseStream.close(fr);
            logger.info("Close Resource");
        }
    }
    private void setHbaFromLine(String line,HbaInfoDTO hba)
    {        
        logger.info("Enter :"+line);
        String[] connectType ={ "host", "hostssl", "hostnossl", "local"  };
        line = line.trim();
        if (line.isEmpty())
        {
            hba.setComment(true);
        }
        else if (line.startsWith("#"))
        {            
            if (line.endsWith("[OPTIONS]"))
            {
                hba.setComment(true);
            } else if (line.substring(1).isEmpty())
            {
                hba.setComment(true);
            } else if (line.indexOf(",") >= 0 || line.indexOf(".") >= 0)
            {
                hba.setComment(true);
            } else
            {
                String[] arr = line.substring(2).split("\\s+");//\\s表示空格,回车,换行等空白符,+号表示一个或多个的意思。
                logger.info("type=" + arr[0]);
                for (String type : connectType)
                {
                    if (arr[0].equals(type))
                    {
                        hba.setEnable(true);
                        hba.setConnectType(arr[0]);
                        hba.setDatabase(arr[1]);
                        hba.setUser(arr[2]);
                        if (arr[0].equals("local"))
                        {
                            hba.setMethod(arr[3]);
                        } else
                        {
                            hba.setIpAddress(arr[3]);
                            hba.setMethod(arr[4]);
                        }
                        return;
                    }
                }
                hba.setComment(true);
            }
        }
        else
        {
            hba.setComment(false); 
            String[] arr = line.split("\\s+");
            logger.info("type=" +arr[0]);
            for (String type : connectType)
            {
                if (arr[0].equals(type))
                {
                    hba.setEnable(true);
                    hba.setConnectType(arr[0]);
                    hba.setDatabase(arr[1]);
                    hba.setUser(arr[2]);
                    if (arr[0].equals("local"))
                    {                        
                        hba.setMethod(arr[3]);
                    }else
                    {
                        hba.setIpAddress(arr[3]);
                         hba.setMethod(arr[4]);
                    }
                    return;
                }
            }
            logger.warn("line " + hba.getLineNum() + " is an exception line,please check.");
            return;
        }
    }
    private Object[] getRowDataFromHba(HbaInfoDTO hba)
    {
        Object[] rowData = new Object[7];
        rowData[0] = hba.isEnable();
        rowData[1] = hba.getConnectType();
        rowData[2] = hba;
        rowData[3] = hba.getUser();
        rowData[4] = hba.getIpAddress();
        rowData[5] = hba.getMethod();
        //rowData[7] = String.format("%d", hba.getLineNum());
        rowData[6] = hba.getOption();
        return rowData;
    }
    
    public void saveHbaToFile(String filePath, String outputPath, List<HbaInfoDTO> hbaList, List<Integer> removeLineList) throws Exception
    {
        logger.info("Enter:" + filePath);
        if (filePath == null)
        {
            logger.error("error：filePath is null,do nothing and return");
            return;
        }
        FileReader fr = null;
        BufferedReader br = null;
        
        FileWriter fw = null;
        BufferedWriter bw = null;
        try
        {
            fr = new FileReader(filePath);
            br = new BufferedReader(fr);
                        
            StringBuilder str = new StringBuilder();
            int lineNum = 1;
            String line = br.readLine();
            HbaInfoDTO hba = null;
            while (line != null)
            {
                if (!this.isRemovedLine(lineNum, removeLineList))
                {
                    hba = isModifiedLine(hbaList, lineNum);
                    if (hba == null)
                    {
                        str.append(line).append(System.getProperty("line.separator"));
                    } else
                    {
                        str.append(this.getLineFromHba(hba)).append(System.getProperty("line.separator"));
                    }
                }else
                {
                    //removed row must be omit;
                }
                line = br.readLine();
                lineNum++;
            }
            fw = new FileWriter(outputPath);
            bw = new BufferedWriter(fw);
            bw.write(str.toString());
            //for new added hba
            for(HbaInfoDTO hbaInfo : hbaList)
            {
                if(hbaInfo.getLineNum()==0)
                {
                    bw.write(this.getLineFromHba(hbaInfo));
                    bw.newLine();
                }                
            }
            bw.flush();
        } catch (IOException ex)
        {
            logger.error("Error :" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(bw);
            CloseStream.close(fw);
            
            CloseStream.close(br);
            CloseStream.close(fr);
            logger.info("Close Resource");
        }
    }    
    private boolean isRemovedLine(int lineNum, List<Integer> removedLineList)
    {
        for (int line : removedLineList)
        {
            if (line == lineNum)
            {
                return true;
            }
        }
        return false;
    }   
    private HbaInfoDTO isModifiedLine(List<HbaInfoDTO> hbaList, int lineNum)
    {
        HbaInfoDTO hbaInfo = null;
        for (HbaInfoDTO hba : hbaList)
        {
            if (hba.getLineNum() == lineNum && hba.getState()==1)
            {
                hbaInfo = hba;
                break;
            }
        }
        return hbaInfo;
    }
    private String getLineFromHba(HbaInfoDTO data)
    {
        StringBuilder str = new StringBuilder();
        logger.info("isEnable =" + data.isEnable());
        if (!data.isEnable())
        {
            str.append("#").append(" ");
        }
        str.append(data.getConnectType()).append("  ")
                .append(data.getDatabase()).append("  ")
                .append(data.getUser()).append("  ")
                .append(data.getIpAddress()).append("  ")
                .append(data.getMethod());
        logger.info("return: " + str.toString());
        return str.toString();
    }
    
    
    /*
     * for postgresql.conf
     */
    public List<PostgresqlInfoDTO> readPostgresqlFile(String filePath, DefaultTableModel model) throws IOException
    {
        logger.info("Enter =" + filePath);
        List<PostgresqlInfoDTO> paraList = new ArrayList();
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try
        {
            fis = new FileInputStream(filePath);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
                      
            String line = br.readLine();
            int lineNum = 1;
            Object[] row =null;
            while (line != null)
            {
                PostgresqlInfoDTO paraInfo = new PostgresqlInfoDTO();
                this.setPostgresqlFromLine(line, paraInfo);
                paraInfo.setLineNum(lineNum);
                //paraList.add(paraInfo);
//                if (!paraInfo.isCommentLine() && lineNumber > 0)
//                {
//                    //比较配置项lines里边的值和读入的数据是否重复
//                    for (int i = lineNumber - 1; i >= 0; i--)
//                    {
//                       // logger.info("isComment =" + lines.get(i).isIsComment());
//                        if (!paraList.get(i).isCommentLine())
//                        {
//                            //logger.info("Name :" + lines.get(i).getName());
//                            //处理重复的值
//                            if (paraInfo.getName().equals(paraList.get(i).getName()))
//                            {
//                                paraInfo.setIsRepeat(true);
//                                //logger.info("isRepeat =" + postgresDataDTO.isIsRepeat());
//                                if (paraList.get(i).getComment().isEmpty() && !paraInfo.getComment().isEmpty())
//                                {
//                                    //logger.info("comment :" + postgresDataDTO.getComment());
//                                    paraList.get(i).setComment(paraInfo.getComment());
//                                }
//                            }
//                        }
//                    }
//
//                }
                if (!paraInfo.isCommentLine())
                {
                    row = new Object[]
                    {
                        paraInfo.isEnable(),
                        paraInfo,
                        paraInfo.getValue(),
                        paraInfo.getComment()
                    };
                    model.addRow(row);
                }   
                line = br.readLine();
                lineNum++;
            }
        }
        catch (IOException e)
        {
            logger.error( e.getMessage());
            e.printStackTrace(System.out);
            throw new IOException(e);
        }
        finally
        {
            CloseStream.close(br);
            CloseStream.close(isr);
            CloseStream.close(fis);
            logger.info("close resource");
        }
        logger.info("Return：size=" + paraList.size());
        return paraList;
    }
    private void setPostgresqlFromLine(String line, PostgresqlInfoDTO paraInfo)
    {
        //logger.info("Enter:" + line);
        paraInfo.setOrgText(line);
        line = line.trim();
        if (line.isEmpty() || line.substring(1).isEmpty())
        {
            paraInfo.setCommentLine(true);
            return;
        }else if (line.startsWith("#"))
        {
            if (line.indexOf("=") == -1 || 
                    line.substring(1, 2).equals(" ")||
                    line.substring(1, 2).equals("-") )
            {
                paraInfo.setCommentLine(true);
                return;
            } else
            {
                paraInfo.setCommentLine(false);
                paraInfo.setEnable(false);
                line = line.replaceFirst("#","").trim();
            }            
        } else
        {
            paraInfo.setCommentLine(false);
            paraInfo.setEnable(true);
        }
        //logger.info(line);
        int pos = line.indexOf("=");
        paraInfo.setName(line.substring(0, pos).trim());
        switch(paraInfo.getName())
        {
            case "name":
                paraInfo.setCommentLine(true);
                return;
        }
        line = line.substring(pos+1).trim();
        pos = line.indexOf("#");
        if (pos == -1)
        {
            paraInfo.setValue(line);
            paraInfo.setComment("");
        } else
        {
            paraInfo.setValue(line.substring(0, pos - 1).trim());
            paraInfo.setComment(line.substring(pos+1).trim());
        }
        logger.info(paraInfo.getName() + "," + paraInfo.getValue() + "," + paraInfo.getComment());
//        if (paraInfo.getValue().startsWith("'") && paraInfo.getValue().endsWith("'"))
//        {
//            paraInfo.setValue(paraInfo.getValue().substring(1, paraInfo.getValue().length() - 1));
//            logger.info("Value :" + paraInfo.getValue());
//        }
    }
    
    public void getConfProperty(HelperInfoDTO helperInfo,PostgresqlInfoDTO conf) throws ClassNotFoundException,SQLException
    {
        String sql = "SELECT name, setting, source, category, short_desc, extra_desc, context, vartype, min_val, max_val"
                + " FROM pg_settings WHERE name='"+ conf.getName() + "'";
        logger.info(sql);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getMaintainDB(),
                    helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                conf.setCategory(rs.getString("category"));
                conf.setDescription(rs.getString("short_desc"));
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
    }
    public void savePostgresqlToFile(String filePath, String outputPath, List<PostgresqlInfoDTO> confList) throws IOException
    {
        logger.info("Enter:" + filePath);
        if (filePath == null)
        {
            logger.error("error：filePath is null,do nothing and return");
            return;
        }
        FileReader fr = null;
        BufferedReader br = null;

        FileWriter fw = null;
        BufferedWriter bw = null;
        try
        {
            fr = new FileReader(filePath);
            br = new BufferedReader(fr);

            StringBuilder str = new StringBuilder();
            int lineNum = 1;
            String line = br.readLine();
            PostgresqlInfoDTO conf = null;
            while (line != null)
            {
                conf = isModifiedPostgresqlLine(confList, lineNum);
                if (conf == null)
                {
                    str.append(line).append(System.getProperty("line.separator"));
                } else
                {
                    str.append(this.getLineFromPostgresql(conf)).append(System.getProperty("line.separator"));
                }
                line = br.readLine();
                lineNum++;
            }
            fw = new FileWriter(outputPath);
            bw = new BufferedWriter(fw);
            bw.write(str.toString());
            bw.flush();
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        } finally
        {
            CloseStream.close(bw);
            CloseStream.close(fw);
            
            CloseStream.close(br);
            CloseStream.close(fr);
            logger.info("Close Resource");
        }
    }
    private PostgresqlInfoDTO isModifiedPostgresqlLine(List<PostgresqlInfoDTO> confList, int lineNum)
    {
        for (PostgresqlInfoDTO conf : confList)
        {
            if (conf.getLineNum() == lineNum)
            {
                return conf;
            }
        }
        return null;
    }   
    private String getLineFromPostgresql(PostgresqlInfoDTO conf)
    {
        StringBuilder str = new StringBuilder();
        if (!conf.isEnable())
        {
            str.append("#");
        }
        str.append(conf.getName()).append("=").append(conf.getValue());
        if(conf.getComment()!=null && !conf.getComment().isEmpty())
        {
            str.append("    # ").append(conf.getComment());
        }
        return str.toString();
    }
    
    
    /*
    * for password saved in catalog.xml
    */    
    public void readPgPassword(String inFile, DefaultTableModel model) throws DocumentException
    {
        logger.info(inFile);
        try
        {
            File file = new File(inFile);
            if (!file.exists())
            {
                logger.info("There's no catalog.xml file, do nothing and return.");
                return;
            }
            SAXReader reader = new SAXReader();
            Document rddocument = reader.read(file);
            Element elmServerRoot = rddocument.getRootElement();
            Object[] row;
            for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
            {
                Element elmServerGroup = (Element) itt.next();
                for (Iterator it = elmServerGroup.elementIterator(); it.hasNext();)
                {                   
                    Element elmServer = (Element) it.next();
                    XMLServerInfoDTO server = new XMLServerInfoDTO();
                    server.setName(elmServer.attributeValue("name"));
                    server.setUser(elmServer.attributeValue("user"));
                    server.setHost(elmServer.attributeValue("host"));
                    server.setPort(elmServer.attributeValue("port"));
                    //logger.info("isSavePwd=" + elmServerInfo.attributeValue("isSavePwd"));
                    if (elmServer.attributeValue("isSavePwd").equals("true"))
                    {
                        server.setSavePwd(true);
                    } else
                    {
                        server.setSavePwd(false);
                    }
                    if (elmServer.attributeValue("pwd") != null)
                    {
                        server.setPwd(elmServer.attributeValue("pwd"));
                    }
                    server.setMaintainDB(elmServer.attributeValue("maintainDB"));
                    server.setVersion(elmServer.attributeValue("dbVersion"));
                    server.setHighgoDB(Boolean.valueOf(elmServer.attributeValue("isHighgo")));
                    if (server.isHighgoDB())
                    {
                        server.setKernelVersion(elmServer.attributeValue("kernelVersion"));
                        server.setHgdbCompatibility(elmServer.attributeValue("hgdbCompatibility"));
                    }
                    row = new Object[]
                    {
                        server.isSavePwd(),
                        server,
                        server.getHost(),
                        server.getPort(),
                        server.getMaintainDB(),
                        server.getUser(),
                        server.getPwd()
                    };
                    model.addRow(row);
                }
            }
        }
        catch (DocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new DocumentException(ex);
        }
    }
    public void savePgPasswordToFile(String inPath, List<XMLServerInfoDTO> serverList, String outPath) throws Exception
    {
        logger.info("inPath="+inPath);
        try
        {
            File file = new File(inPath);
            if (!file.exists())
            {
                logger.error(inPath + " does not exist, do nothing and return.");
                throw new Exception(inPath + " does not exist!");
            }
            SAXReader reader = new SAXReader();
            Document rddocument = reader.read(file);
            Element elmServerRoot = rddocument.getRootElement();
            for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
            {
                Element elmServerGroup = (Element) itt.next();
                for (Iterator it = elmServerGroup.elementIterator(); it.hasNext();)
                {
                    Element elmServer = (Element) it.next();
                    String name = elmServer.attributeValue("name");
                    logger.info(name);
                    for (XMLServerInfoDTO s : serverList)
                    {
                        if (s.getName().equals(name))
                        {
                            elmServer.attribute("isSavePwd").setValue(String.valueOf(s.isSavePwd()));
                            if (s.getPwd() != null && !s.getPwd().isEmpty())
                            {
                                elmServer.addAttribute("pwd", s.getPwd());
                            }
                            break;
                        }
                    }
                }
            }
            logger.info("pouPath="+outPath);
            File outFile = new File(outPath);
            if (!outFile.exists())
            {
                logger.info(outFile + " does not exist, so create it." + outFile.createNewFile());
            }
            XMLWriter output = new XMLWriter(new FileWriter(outFile));
            output.write(rddocument);
            output.close();
        }
        catch (IOException | DocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
    }

        
    /*
     * for pgpass.conf
     */
    public void readPgpassFile(String filePath, DefaultTableModel model) throws IOException
    {
        logger.info("Enter:");
        //List<PgpassInfoDTO> lines = new ArrayList<>();
        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        try
        {
            fis = new FileInputStream(filePath);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);

            PgpassInfoDTO pgpass;
            String line = br.readLine();
            Object[] row;
            //int lineNum = 1;
            while (line != null)
            {
                pgpass = new PgpassInfoDTO();
                this.getPgpassFromLine(line, pgpass);
                if (!pgpass.isComment())
                {
                    row = new Object[]
                    {
                        pgpass.isEnable(),
                        pgpass.getHost(),
                        pgpass.getPort(),
                        pgpass,
                        pgpass.getUserName(),
                        pgpass.getPassword()
                    };
                    model.addRow(row);
                    //lines.add(dataModel);
                }
                line = br.readLine();
                //lineNum++;
            }
        } catch (IOException ex)
        {
            logger.error( ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        } finally
        {
            CloseStream.close(br);
            CloseStream.close(isr);
            CloseStream.close(fis);
            logger.info("close resource");
        }
        //logger.info("return:" + lines.size());
        //return lines;
    }
    private void getPgpassFromLine(String line, PgpassInfoDTO pgpass)
    {
        line = line.trim();
        if (line.isEmpty())
        {
            pgpass.setComment(true);
            return;
        } else
        {
            if (line.startsWith("#"))
            {
                line = line.substring(1);
                if (line.isEmpty())
                {
                    pgpass.setComment(true);
                    return;
                } else
                {
                    pgpass.setComment(false);
                    pgpass.setEnable(false);
                    String[] array = line.split(":");
                    this.setPgpassFromArray(pgpass, array);
                }
            } else
            {
                pgpass.setComment(false);
                pgpass.setEnable(true);
                String[] array = line.split(":");
                this.setPgpassFromArray(pgpass, array);
            }
        }
    }
    private void setPgpassFromArray(PgpassInfoDTO pgpass, String[] array)
    {
        List<String> list = Arrays.asList(array);
        Iterator<String> iterator = list.iterator();
        if (iterator.hasNext())
        {
            pgpass.setHost(iterator.next().toString().trim());
        }
        if (iterator.hasNext())
        {
            pgpass.setPort(iterator.next().toString().trim());
        }
        if (iterator.hasNext())
        {
            pgpass.setDatabase(iterator.next().toString().trim());
        }
        if (iterator.hasNext())
        {
            pgpass.setUserName(iterator.next().toString().trim());
        }
        if (iterator.hasNext())
        {
            pgpass.setPassword(iterator.next().toString().trim());
        }
    }
    public void savePgpassToFile(String outputPath, List<PgpassInfoDTO> passList) throws Exception
    {
        FileWriter fw = null;
        BufferedWriter bw = null;
        try
        {
            fw = new FileWriter(outputPath);
            bw = new BufferedWriter(fw);

            for (PgpassInfoDTO pass : passList)
            {
                bw.write(this.getLineFromPgpass(pass));
            }
            bw.flush();
        } catch (IOException ex)
        {
            logger.error("Error :" + ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(bw);
            CloseStream.close(fw);
            logger.info("Close Resource");
        }
    }

    private String getLineFromPgpass(PgpassInfoDTO data)
    {
        StringBuilder strBuilder = new StringBuilder();
        if (!data.isEnable())
        {
            strBuilder.append("# ");
        }
        strBuilder.append(data.getHost())
                .append(":").append(data.getPort())
                .append(":").append(data.getDatabase())
                .append(":").append(data.getUserName())
                .append(":").append(data.getPassword());
        logger.info("return:" + strBuilder.toString());
        return strBuilder.toString();
    }

}
