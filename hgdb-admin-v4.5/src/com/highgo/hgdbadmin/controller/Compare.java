/* ------------------------------------------------
 *
 * File: Compare.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\controller\Compare.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.controller;

import com.highgo.hgdbadmin.model.AutoVacuumDTO;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.FunctionArgDTO;
import com.highgo.hgdbadmin.model.ObjItemInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.VariableInfoDTO;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class Compare
{
    private Logger logger = LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());

    private static Compare compare = null;
    public static Compare getInstance()
    {
        if (compare == null)
        {
            compare = new Compare();
        }
        return compare;
    }
   
    public boolean equals(String a, String b)
    {
        if (a == null && b == null)
        {
            return true;
        }else if(a == null && b!= null)
        {
            return b.isEmpty();
        }else if(a!=null && b==null)
        {
            return a.isEmpty();
        }else{
            return a.equals(b);
        }
    }    
    public boolean equalStrList(List<String> a, List<String> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else if (a.size() != b.size())
        {
            return false;
        } else
        {
            Collections.sort(a);
            Collections.sort(b);
            return a.equals(b);
        }
    }
    
    public boolean equalVariableList(List<VariableInfoDTO> a, List<VariableInfoDTO> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && a == null)
        {
            return false;
        } else if (a.size() != b.size())
        {
            return false;
        } else
        {
            boolean flag = true;
            for (int i = 0; i < a.size(); i++)
            {
                if (!this.equal(a.get(i), b.get(i)))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }        
    }    
    public  boolean equal(VariableInfoDTO a, VariableInfoDTO b)
    {
        if (a == b && a == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else
        {
            return this.equals(a.getDb(), b.getDb())
                    && this.equals(a.getName(), b.getName())
                    && this.equals(a.getUser(), b.getUser())
                    && this.equals(a.getValue(), b.getValue());
        }
    }


    public boolean equalFuncArgList(List<FunctionArgDTO> a, List<FunctionArgDTO> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else if (a.size() != b.size())
        {
            return false;
        } else
        {
            boolean flag = true;
            for (int i = 0; i < a.size(); i++)
            {
                if (!this.equal(a.get(i), b.get(i)))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }        
    }    
    private boolean equal(FunctionArgDTO a, FunctionArgDTO b)
    {
        if (a == b && a == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else
        {
            return this.equals(a.getType(), b.getType())
                    && this.equals(a.getMode(), b.getMode())
                    && this.equals(a.getName(), b.getName())
                    && this.equals(a.getDefaultValue(), b.getDefaultValue());
        }
    }

    public boolean equal(AutoVacuumDTO a, AutoVacuumDTO b)
    {
        if(a==null && b==null)
        {
            return true;
        }else if(a==null && b!=null)
        {
            return false;
        }else if(a!=null && b==null)
        {
            return false;
        } else
        {
            return a.isCustomAutoVacuum() == b.isCustomAutoVacuum()
                    && a.isEnable() == b.isEnable()
                    && this.equals(a.getVacuumBaseThreshold(), b.getVacuumBaseThreshold())
                    && this.equals(a.getAnalyzeBaseThreshold(), b.getAnalyzeBaseThreshold())
                    && this.equals(a.getVacuumScaleFactor(), b.getVacuumScaleFactor())
                    && this.equals(a.getAnalyzeScaleFactor(), b.getAnalyzeScaleFactor())
                    && this.equals(a.getVacuumCostDelay(), b.getVacuumCostDelay())
                    && this.equals(a.getVacuumCostLimit(), b.getVacuumCostLimit())
                    && this.equals(a.getFreezeMinimumAge(), b.getFreezeMinimumAge())
                    && this.equals(a.getFreezeMaximumAge(), b.getFreezeMaximumAge())
                    && this.equals(a.getFreezeTableAge(), b.getFreezeTableAge())
                    && a.isEnableToast() == b.isEnableToast()
                    && this.equals(a.getVacuumBaseThresholdToast(), b.getVacuumBaseThresholdToast())
                    && this.equals(a.getVacuumScaleFactorToast(), b.getVacuumScaleFactorToast())
                    && this.equals(a.getVacuumCostDelayToast(), b.getVacuumCostDelayToast())
                    && this.equals(a.getVacuumCostLimitToast(), b.getVacuumCostLimitToast())
                    && this.equals(a.getFreezeMinimumAgeToast(), b.getFreezeMinimumAgeToast())
                    && this.equals(a.getFreezeMaximumAgeToast(), b.getFreezeMaximumAgeToast())
                    && this.equals(a.getFreezeTableAgeToast(), b.getFreezeTableAgeToast());
        }
    }
    
    public boolean equalObjItemList(List<ObjItemInfoDTO> a, List<ObjItemInfoDTO> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else if (a.size() != b.size())
        {
            return false;
        } else
        {
            boolean flag = true;
            for (int i = 0; i < a.size(); i++)
            {
                if (!this.equal(a.get(i), b.get(i)))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }

    }
    private boolean equal(ObjItemInfoDTO a, ObjItemInfoDTO b)
    {
         if (a == b && a == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else
        {
            return a.getOid() != 0 && b.getOid() != 0 && a.getOid() == b.getOid();
        }
    }

    public boolean equalColumnList(List<ColumnInfoDTO> a, List<ColumnInfoDTO> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else if (a.size() != b.size())
        {
            return false;
        } else
        {
            boolean flag = true;
            for (int i = 0; i < a.size(); i++)
            {
                if (!this.equal(a.get(i), b.get(i)))
                {
                    flag = false;
                    break;
                }
            }
            return flag;
        }
    }
    public boolean equal(ColumnInfoDTO a, ColumnInfoDTO b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else
        {
            logger.info("name=" + a.getName() + "," + b.getName());
            logger.info("datatype=" + a.getDatatype().getFullname() + b.getDatatype().getFullname());
            logger.info("collation=" + a.getCollation() + "," + b.getCollation());
            logger.info("defaultValue=" + a.getDefaultValue() + "," + b.getDefaultValue());
            logger.info("statistics=" + a.getStatistics() + "," + b.getStatistics());
            logger.info("storage=" + a.getStorage() + "," + b.getStorage());
            return (a.getName() != null && b.getName() != null && a.getName().equals(b.getName()))
                    && this.equals(a.getDatatype().getFullname(), b.getDatatype().getFullname())
                    && this.equals(a.getCollation(), b.getCollation())
                    && this.equals(a.getDefaultValue(), b.getDefaultValue())
                    && (a.isNotNull() == b.isNotNull())
                    && this.equals(a.getStatistics(), b.getStatistics())
                    && this.equals(a.getStorage(), b.getStorage());
        }
    }
    
    public boolean equalConstraintList(List<ConstraintInfoDTO> a, List<ConstraintInfoDTO> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return b.isEmpty();
        } else if (a != null && b == null)
        {
            return a.isEmpty();
        } else
        {
            if (a.size() != b.size())
            {
                return false;
            } else
            {         
                for (int i = 0; i < a.size(); i++)
                {
                    if (!this.equal(a.get(i), b.get(i)))
                    {
                       return false;
                    }
                }
                return true;
            }
        }   
    }
    public boolean equal(ConstraintInfoDTO a, ConstraintInfoDTO b)
    {
        if (a == b && a == null)
        {
            return true;
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else
        {
            return ((a.getName() != null && b.getName() != null && a.getName().equals(b.getName()))
                    && this.equals(a.getComment(), b.getComment()))
                    && this.equals(a.getDefineSQL(), b.getDefineSQL());
        }
    }
    
    
     public boolean equalPartitionList(List<PartitionInfoDTO> a, List<PartitionInfoDTO> b)
    {
        if (a == null && b == null)
        {
            return true;
        } else if (a == null && b != null && b.size() > 0)
        {
            return false;
        } else if (a != null && a.size() > 0 && b == null )
        {
            return false;
        } else
        {
            if (a.size() != b.size())
            {
                return false;
            } else
            {
                for (int i = 0; i < a.size(); i++)
                {
                    if (!this.equal(a.get(i), b.get(i)))
                    {
                        return false;
                    }
                }
                return true;
            }
        }    
    }
     
    public boolean equal(PartitionInfoDTO a, PartitionInfoDTO b)
    {
        if (a == null && b == null)
        {
            return true;//cannot be null
        } else if (a == null && b != null)
        {
            return false;
        } else if (a != null && b == null)
        {
            return false;
        } else
        {
            return a.getName().equals(b.getName());//only care name ,other change ignore
        }
    }
    
}
