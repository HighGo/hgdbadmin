/* ------------------------------------------------
 *
 * File: DataTypeController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\controller\DataTypeController.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.controller;

import com.highgo.jdbc.geometric.*;
import com.highgo.jdbc.util.PGInterval;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class DataTypeController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private static class InstanceHoder {
        private static DataTypeController instance = new DataTypeController();
    }
    public static DataTypeController getInstance() {
        return InstanceHoder.instance;
    }
    private DataTypeController(){
    }
    
    public String  getDisplayTypeFromInternalType(String typeName, String defaultValue)
    {
        switch (typeName)
        {
            case "int4":
                if (defaultValue != null && defaultValue.startsWith("nextval"))
                {
                    return "serial";
                } else
                {
                    return "integer";
                }
            case "int8":
                if (defaultValue != null && defaultValue.startsWith("nextval"))
                {
                    return "bigserial";
                } else
                {
                    return "bigint";
                }

            default:
                return typeName;
        }
    }

    public Class getTypeClassFromInternalType(String typeName)
    {
        if (typeName.startsWith("_"))//array
        {
            return Object.class;//java.sql.Array
        }
        switch (typeName)
        {
            //all the class name form rs.getMetaData().getColumnClassName(i);
            case "xml":
                return Object.class;//java.sql.SQLXML

            case "uuid":
                return String.class;//java.util.UUID cause grid cannot edit

            case "money":
                return String.class;//Double.class;//PGmoney.class only support doller,others will cause error,so use double or string

            case "boolean":
                return Boolean.class;

            //bit and varbit can only be entered 0 or 1 ,or combination
            case "bit":
                return String.class;//Boolean.class cannot display detailed content
            case "varbit":
                return Object.class;

            case "bytea":
                return Object.class;//Byte.class;
            case "oid":
                return Long.class;

            case "int2":
                return Integer.class;//get Integer from resultset but not Short;
            case "int4":
                return Integer.class;
            case "int8":
                return Long.class;
            case "numeric":
                return BigDecimal.class;//Number
            case "float4":
                return Float.class;
            case "float8":
                return Double.class;

            case "bpchar":
                return String.class;//get String from resultset but not Character,and Character make grid not editable
            case "varchar":
            case "text":
                return String.class;

            case "date":
                return String.class;//return Date.class; jtable not support maybe
            case "timestamp":
            case "timestamptz":
                return Timestamp.class;
            case "time":
            case "timetz":
                return Time.class;
            case "interval":
                return PGInterval.class;

            //geometric
            case "point":
                return PGpoint.class;
            case "line":
                return PGline.class;
            case "lseg":
                return PGlseg.class;
            case "box":
                return PGbox.class;
            case "path":
                return PGpath.class;
            case "polygon":
                return PGpolygon.class;
            case "circle":
                return PGcircle.class;

            case "cidr":
            case "inet":
            case "macaddr":
                return Object.class;

            case "tsvector":
            case "tsquery":
                return Object.class;

            case "json":
                return Object.class;

            default:
                logger.info(typeName + " is out of exception");
                return Object.class;
        }
    }

}
