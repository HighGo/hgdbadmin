/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.highgo.hgdbadmin.controller;

import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author highgoer
 */
public class ServerCtlThread extends Thread
{
    private Logger logger = LogManager.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
  
    private Integer exitValue;
    
    private Object lock;
    private javax.swing.text.Document docs;
    private boolean isStart;
    private String os;
    private String host;
    private int sshport;
    private String sysuser;
    private String syspwd;
    private String bin;
    private String data;
    public ServerCtlThread(Object lock, javax.swing.text.Document docs, boolean isStart, String os, String host, int sshport, String sysuser, String syspwd, String bin, String data)
    {
        exitValue = -1;
        this.lock = lock;
        this.docs = docs;
        this.isStart = isStart;
        this.os = os;
        this.host = host;
        this.sshport = sshport;
        this.sysuser = sysuser;
        this.syspwd = syspwd;
        this.bin = bin;
        this.data = data;      
    }
    
    private void execute() //throws Exception
    {
        SimpleAttributeSet attr = new SimpleAttributeSet();
        ch.ethz.ssh2.Connection conn = null;
        Session ssh = null;
        try
        {
            conn = new ch.ethz.ssh2.Connection(host, sshport);
            conn.connect();
            if (!conn.authenticateWithPassword(sysuser, syspwd))
            {
                throw new Exception("Connect failed, user name or password is wrong!");
            } else
            {
                logger.info("server connected");
                
                String lib = data.replace("data", "lib");
                String cmd = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:" + lib + "; " +
                        "cd " + bin + "; pwd; "
                        + "./pg_ctl " + (isStart ? "start" : "stop") + " -D " + data + "; ./pg_ctl status -D " + data + ";";
                //String cmd = bin + "/pg_ctl " + (isStart ? "start" : "stop") + " -D " + data;
                logger.info(cmd.toString());
                docs.insertString(docs.getLength(), cmd + System.lineSeparator()
                        + "----------------------------------------------------------" + System.lineSeparator(), attr);
                ssh = conn.openSession();
                logger.info("session opened");
                ssh.execCommand(cmd.toString());
                InputStream is = new StreamGobbler(ssh.getStdout());
                InputStream erris = new StreamGobbler(ssh.getStderr());
                this.printStdOut(isStart, is, docs, attr);
                this.printStdOut(isStart, erris, docs, attr);
                this.exitValue = ssh.getExitStatus();
                logger.info("exit value = " + exitValue);//the value null or 0 indicates normal termination.     
                docs.insertString(docs.getLength(), "Exit value: " + exitValue + "." + System.lineSeparator(), attr);
                synchronized (lock)
                {
                    lock.notify();
                }
            }
        } catch (Exception e)
        {
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
            //throw e;
             try
            {
                docs.insertString(docs.getLength(),e.getMessage() + System.lineSeparator(), attr);
            } catch (BadLocationException ex)
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
            }
        } finally
        {
            if (ssh != null)
            {
                ssh.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }
    }
    
    private void printStdOut(boolean isStart, InputStream is, javax.swing.text.Document docs,SimpleAttributeSet attr) throws UnsupportedEncodingException, IOException, BadLocationException
    { 
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"UTF-8"));//"GBK"
        String line;
        while ((line = bufferedReader.readLine()) != null)
        {
            logger.info(line);
            docs.insertString(docs.getLength(), line + System.lineSeparator(), attr);
            if (isStart && (line.trim().startsWith("pg_ctl: server is running ") || line.trim().startsWith(constBundle.getString("serverStarted"))))
            {
                break;
            } else if (!isStart && (line.trim().startsWith("pg_ctl: no server running") || line.trim().startsWith(constBundle.getString("serverStopped"))))
            {
                break;
            }
        }
    }
    
    public Integer getExitValue()
    {
        return this.exitValue;
    }
    
    @Override
    public void run(){
        this.execute();
    }    
}
