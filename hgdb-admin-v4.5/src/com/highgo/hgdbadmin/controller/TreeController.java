/* ------------------------------------------------
 *
 * File: TreeController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\controller\TreeController.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.controller;

import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;
import com.highgo.hgdbadmin.datatransfer.model.ColumnDTO;
import com.highgo.hgdbadmin.model.AutoVacuumDTO;
import com.highgo.hgdbadmin.model.CastInfoDTO;
import com.highgo.hgdbadmin.model.CatalogInfoDTO;
import com.highgo.hgdbadmin.model.ColumnInfoDTO;
import com.highgo.hgdbadmin.model.ConnectInfoDTO;
import com.highgo.hgdbadmin.model.ConstraintInfoDTO;
import com.highgo.hgdbadmin.model.BranchDTO;
import com.highgo.hgdbadmin.model.DBInfoDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.IndexInfoDTO;
import com.highgo.hgdbadmin.model.ObjItemInfoDTO;
import com.highgo.hgdbadmin.model.LanguageInfoDTO;
import com.highgo.hgdbadmin.model.ObjGroupDTO;
import com.highgo.hgdbadmin.model.ObjInfoDTO;
import com.highgo.hgdbadmin.model.AbstractObject;
import com.highgo.hgdbadmin.model.ColInfoDTO;
import com.highgo.hgdbadmin.model.ColItemInfoDTO;
import com.highgo.hgdbadmin.model.FunObjInfoDTO;
import com.highgo.hgdbadmin.model.FunctionArgDTO;
import com.highgo.hgdbadmin.model.FunctionInfoDTO;
import com.highgo.hgdbadmin.model.RoleInfoDTO;
import com.highgo.hgdbadmin.model.RuleInfoDTO;
import com.highgo.hgdbadmin.model.SchemaInfoDTO;
import com.highgo.hgdbadmin.model.SecurityLabelInfoDTO;
import com.highgo.hgdbadmin.model.SequenceInfoDTO;
import com.highgo.hgdbadmin.model.SysVariableInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.model.TablespaceInfoDTO;
import com.highgo.hgdbadmin.model.TriggerInfoDTO;
import com.highgo.hgdbadmin.model.DatatypeDTO;
import com.highgo.hgdbadmin.model.EventTriggerInfoDTO;
import com.highgo.hgdbadmin.model.PartitionInfoDTO;
import com.highgo.hgdbadmin.model.VariableInfoDTO;
import com.highgo.hgdbadmin.model.ViewColumnInfoDTO;
import com.highgo.hgdbadmin.model.ViewInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerBranchInfoDTO;
import com.highgo.hgdbadmin.model.XMLServerInfoDTO;
import com.highgo.hgdbadmin.optioneditor.controller.OptionController;
import com.highgo.hgdbadmin.util.JdbcHelper;
import com.highgo.hgdbadmin.util.TreeEnum;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 *
 * @author Liu Yuanyuan
 */
public class TreeController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private TreeController()
    {
    }
    private static TreeController treeController = null;
    public static TreeController getInstance()
    {
        if (treeController == null)
        {
            treeController = new TreeController();
        }
        return treeController;
    }

    
    private boolean hasPrivilege(HelperInfoDTO helperInfo, String objType, String objName, String privilege) throws ClassNotFoundException, SQLException
    {
        boolean flag = false;
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT has_").append(objType).append("_privilege('")
                .append(objName).append("','").append(privilege).append("')");
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getMaintainDB();
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                flag = rs.getBoolean(1);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return flag;
    }
    //DB Version > 
    public boolean isVersionHigherThan(TreeEnum.DBSYS dbsys, String number, int majorComp, int minorComp)
    {
        return true;
        /*
        boolean isHigher = false;
        int major = 0, minor = 0;
        if (dbsys.equals(TreeEnum.DBSYS.HIGHGO))
        {
            if (number.startsWith("2.0"))
            {
                major = 9;
                minor = 2;
            } else if (number.startsWith("1.3") || number.startsWith("1.2"))
            {
                major = 9;
                minor = 0;
            } else if (number.startsWith("1.0"))
            {
                major = 8;
                minor = 4;
            }
        } else if (dbsys.equals(TreeEnum.DBSYS.POSTGRESQL))
        {
            major = Integer.valueOf(String.valueOf(number.charAt(0)));
            minor = Integer.valueOf(String.valueOf(number.charAt(2)));            
        } else
        {
            logger.warn(dbsys.toString() + " is an exception dbsys");
        }
        if (major > majorComp)
        {
            isHigher = true;
        } else if (major == majorComp && minor > minorComp)
        {
            isHigher = true;
        } else
        {
            isHigher = false;
        }
        return isHigher;
        */
    }
    //helper info from server info
    public HelperInfoDTO getHelperInfoFromServerInfo(XMLServerInfoDTO serverInfo)
    {
        HelperInfoDTO helperInfo = new HelperInfoDTO();
        //helperInfo.setPartURL("jdbc:highgo://" + serverInfo.getHost() + ":" + serverInfo.getPort() + "/");
        helperInfo.setHost(serverInfo.getHost());
        helperInfo.setPort(serverInfo.getPort());
        helperInfo.setMaintainDB(serverInfo.getMaintainDB()); //this is the only maintainDB, can never be changed.
        logger.debug("serverInfo.getVersion()=" + serverInfo.getVersion());
        String vinfo = serverInfo.getVersion().split(",")[0];
        if (vinfo.startsWith("HighGo Database"))
        {
            helperInfo.setDbSys(TreeEnum.DBSYS.HIGHGO);
            helperInfo.setVersionNumber(vinfo.split(" ")[2]);
        } else if (vinfo.startsWith("PostgreSQL"))
        {
            helperInfo.setDbSys(TreeEnum.DBSYS.POSTGRESQL);
            helperInfo.setVersionNumber(vinfo.split(" ")[1]);
        }
        helperInfo.setOnSSL(serverInfo.isOnSSL());
        helperInfo.setUser(serverInfo.getUser());
        helperInfo.setPwd(serverInfo.getPwd());
        if (serverInfo.getDatLastSysOid() != null)
        {
            helperInfo.setDatLastSysOid(serverInfo.getDatLastSysOid());
        }
        return helperInfo;
    }
    //change password
    public void changeServerPwd(ConnectInfoDTO helperInfo, String newPwd) throws ClassNotFoundException,SQLException
    {
        logger.info("Enter");
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getMaintainDB();
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            String sql = "ALTER USER " + helperInfo.getUser() + " WITH PASSWORD '" + newPwd + "';";
            //logger.debug("sql=" + sql);
            stmt = conn.createStatement();
            stmt.execute(sql);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
    }    
    /*public String[] getChangeUsers(ConnectInfoDTO helperInfo)throws ClassNotFoundException, SQLException
    {
        logger.debug("Enter");
        List<String> list = new ArrayList();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getMaintainDB();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            String sql = "SELECT rolname FROM pg_roles WHERE rolname NOT IN('sysdba', 'syssao');";
            logger.debug("sql=" + sql);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while(rs.next()){
                list.add(rs.getString(1));
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        String[] s = new String[list.size()];
        list.toArray(s);
        logger.debug("return:" + list.size());
        return s;
    }*/
   
    //Server load, add, delete
    public void saveXMLFile(File file, Object document)throws IOException
    {
        XMLWriter output = null;
        try
        {
            file.createNewFile();
            OutputFormat format = OutputFormat.createPrettyPrint();
            format.setEncoding(System.getProperty("file.encoding"));//format.setEncoding("utf-8"); //in fact, default is UTF-8
            format.setNewlines(true);
            logger.debug("=writer encoding="+ format.getEncoding());
            output = new XMLWriter(new FileWriter(file), format); //当前工程项目的绝对路径
            output.write(document);
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        }finally
        {
            if(output != null)
            {
                output.close();
            }
        }
    }
    private void createCommonCatalogXML()throws Exception
    {
        File file = new File("conf" + File.separator + "catalog.xml");
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement("serverRoot");

        Element server = root.addElement("serversGroup");
        server.setText("Servers");

        Element serverInfo = server.addElement("serverInfo");
        serverInfo.addAttribute("name", "HighGo Databse Server");
        serverInfo.addAttribute("host", "localhost");
        serverInfo.addAttribute("port", "5866");
        serverInfo.addAttribute("user", "sysdba");
        serverInfo.addAttribute("isOnSSL", "false");
        serverInfo.addAttribute("isSavePwd", "false");
        serverInfo.addAttribute("maintainDB", "highgo");
        serverInfo.addAttribute("dbVersion", "HighGo Database 4.0");
        //serverInfo.addAttribute("serviceId", "hgdb-se.4");//hgdb-se.4
        serverInfo.setText(serverInfo.attributeValue("name") + "(" + serverInfo.attributeValue("host")
                + ":" + serverInfo.attributeValue("port") + ")");

        logger.info(file.getAbsolutePath());
        this.saveXMLFile(file, document);
    }
    public List<XMLServerBranchInfoDTO> getServerFromXML()throws DocumentException, Exception
    {
        File f = new File("");
        logger.info("default path: " + f.getAbsolutePath());
        List<XMLServerBranchInfoDTO> servGroupInfoList = new ArrayList();
        try
        {
            String path = "conf" + File.separator + "catalog.xml";
            logger.info(path);
            File file = new File(path);
            if (!file.exists())
            {
                logger.info("There's no catalog.xml file, now create one.");
                this.createCommonCatalogXML();
            }
            logger.info("AbsolutePath: " + file.getAbsolutePath());
            
            logger.debug("=========sys encoding=" + System.getProperty("file.encoding") + "=============");
            SAXReader reader = new SAXReader();
            reader.setEncoding(System.getProperty("file.encoding"));//reader.setEncoding("utf-8");
            logger.debug("reader encoding=" + reader.getEncoding());
            Document rddocument = reader.read(file);
            Element elmServerRoot = rddocument.getRootElement();
            //Element elmtServer = elmtServerGroup.element("Servers");
            for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
            {
                XMLServerBranchInfoDTO servGroupInfo = new XMLServerBranchInfoDTO();
                Element elmServerGroup = (Element) itt.next();
                List<XMLServerInfoDTO> servInfoList = new ArrayList();
                for (Iterator it = elmServerGroup.elementIterator(); it.hasNext();)
                {
                    XMLServerInfoDTO svInfo = new XMLServerInfoDTO();
                    Element elmServerInfo = (Element) it.next();
                    svInfo.setName(elmServerInfo.attributeValue("name"));
                    svInfo.setUser(elmServerInfo.attributeValue("user"));
                    svInfo.setHost(elmServerInfo.attributeValue("host"));
                    svInfo.setPort(elmServerInfo.attributeValue("port"));
                    svInfo.setOnSSL(elmServerInfo.attributeValue("isOnSSL").equals("true"));
                    svInfo.setSavePwd(elmServerInfo.attributeValue("isSavePwd").equals("true"));
                    if (elmServerInfo.attributeValue("isSavePwd").equals("true"))
                    {
                        svInfo.setPwd(elmServerInfo.attributeValue("pwd"));
                    }
                    svInfo.setMaintainDB(elmServerInfo.attributeValue("maintainDB"));
                    //svInfo.setDatLastSysOid(Long.valueOf(elmServerInfo.attributeValue("datLastSysOid")));
                    svInfo.setVersion(elmServerInfo.attributeValue("dbVersion"));
                    svInfo.setHighgoDB(Boolean.valueOf(elmServerInfo.attributeValue("isHighgo")));
                    if (svInfo.isHighgoDB())
                    {
                        svInfo.setKernelVersion(elmServerInfo.attributeValue("kernelVersion"));
                        svInfo.setHgdbCompatibility(elmServerInfo.attributeValue("hgdbCompatibility"));
                    }
                    svInfo.setService(elmServerInfo.attributeValue("serviceId"));
                    svInfo.setGroup(elmServerGroup.getText().trim());
                    svInfo.setConnected(false);
                    servInfoList.add(svInfo);
                }
                servGroupInfo.setGroupName(elmServerGroup.getText());
                servGroupInfo.setXmlServerInfoList(servInfoList);
                servGroupInfoList.add(servGroupInfo);
            }
        }
        catch (DocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new  DocumentException(ex);
        }
        return servGroupInfoList;
    }
    public String addServer(XMLServerInfoDTO serverInfo)throws SQLException, ClassNotFoundException, Exception
    {        
        if (serverInfo == null)
        {
            logger.info("serverInfo is null, do nothing and return.");
            return null; 
        }
        String loginHint = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String url = serverInfo.getPartURL() + serverInfo.getMaintainDB();
        logger.info("url=" + url);
        try
        {
            conn = JdbcHelper.getConnection(url, serverInfo);
            loginHint = conn.getWarnings() == null ? null : conn.getWarnings().getMessage();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT kernel_version()");//for hgdb is kernel_version()
            if(rs.next())
            {
                String versionDetail = rs.getString(1);
                logger.info("version=" + versionDetail);
                serverInfo.setVersion(versionDetail);
                //serverInfo.setService(this.getServiceByVersion(versionDetail));
                serverInfo.setHighgoDB(versionDetail.startsWith("HighGo Database"));
            }            
            if (serverInfo.isHighgoDB())
            {
                //kernel version
                rs = stmt.executeQuery("SELECT version();");
                if(rs.next())
                {
                    serverInfo.setKernelVersion(rs.getString(1));
                }
                
                /*
                //compatibility
                rs = stmt.executeQuery("SELECT setting  FROM pg_settings WHERE name ='hgdb_compatibility'");
                if (rs.next())
                {
                    serverInfo.setHgdbCompatibility(rs.getString(1));
                } else
                {
                    rs = stmt.executeQuery("SELECT setting FROM pg_settings WHERE name ='oracle_compatibility'");
                    logger.info("hasResult：" + rs.next());
                    if (rs.getString(1) != null && rs.getString(1).equals("on"))
                    {
                        serverInfo.setHgdbCompatibility("oracle");
                    } else
                    {
                        serverInfo.setHgdbCompatibility("");
                    }
                }
                */
                
                //last system oid
                rs = stmt.executeQuery("SELECT datlastsysoid FROM pg_database WHERE datName='" + serverInfo.getMaintainDB() + "'");
                if (rs.next())
                {
                    serverInfo.setDatLastSysOid(rs.getLong(1));
                }
            }
            this.addServer2XML(serverInfo);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        }finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.debug("close resource");
        }
        return loginHint;
    }
    private String getServiceByVersion(String versionDetail)
    {
        StringBuilder serviceId = new StringBuilder();
        if (versionDetail.startsWith("HighGo Database"))//HighGo Database 1.3.0 -- hgdb-se1.3   ,HighGo Database 2.0.1 -- hgdb-se2.0.1
        {
            String version = versionDetail.split(" ")[2];
            serviceId.append("hgdb-se").append(version.charAt(0)).append(".").append(version.charAt(2));
            if (!String.valueOf(version.charAt(4)).equals("0"))
            {
                serviceId.append(".").append(version.charAt(4));
            }
        } else if (versionDetail.startsWith("PostgreSQL"))//PostgreSQL 9.4.0, compiled by Visual C++ build 1800, 32-bit
        {
            serviceId.append("postgresql");
            if (versionDetail.endsWith("64-bit"))
            {
                serviceId.append("-x64");
            }
            String version = ((versionDetail.split(",")[0]).split(" ")[1]);
            serviceId.append("-").append(version.charAt(0)).append(".").append(version.charAt(2));
        } else
        {
            logger.warn("this is an exception db system");
        }
        logger.info("serviceId=" + serviceId.toString());
        return serviceId.toString();
    }
    public void changeServerUser(XMLServerInfoDTO oldServerInfo, String newUser, String newPwd) throws Exception
    {
        Connection conn = null;        
        try        
        {
            String url = oldServerInfo.getPartURL() + oldServerInfo.getMaintainDB();
            logger.debug("url=" + url);
            conn = JdbcHelper.getConnection(url, newUser, newPwd, oldServerInfo.isOnSSL());
            logger.debug("connected");
            changeUsernameForServerFromXML(oldServerInfo, newUser);             
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }finally
        {
            JdbcHelper.close(conn);
            logger.debug("close resource");
        }
    }
    
    
    private void addServer2XML(XMLServerInfoDTO serverInfo) throws DocumentException, Exception
    {
        String path = "conf" + File.separator + "catalog.xml";
        logger.info(path);
        File file = new File(path);
        if (!file.exists())
        {
            logger.error("catalog.xml not exist");
            throw new Exception("catalog.xml not exist");
        }
        SAXReader reader = new SAXReader();
        reader.setEncoding(System.getProperty("file.encoding"));//reader.setEncoding("utf-8");
        logger.debug("=reader encoding=" + reader.getEncoding());
        Document rddocument = reader.read(file);
        Element elmServerRoot = rddocument.getRootElement();
        for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
        {
            Element elmServerGroup = (Element) itt.next();
//            if (elmServerGroup.getText().trim().equals(serverInfo.getGroup()))
//            {
            Element elmServerInfo = elmServerGroup.addElement("serverInfo");
            elmServerInfo.addAttribute("name", serverInfo.getName());
            elmServerInfo.addAttribute("host", serverInfo.getHost());
            elmServerInfo.addAttribute("port", serverInfo.getPort());
            elmServerInfo.addAttribute("user", serverInfo.getUser());
            elmServerInfo.addAttribute("isOnSSL", String.valueOf(serverInfo.isOnSSL()));
            elmServerInfo.addAttribute("isSavePwd", String.valueOf(serverInfo.isSavePwd()));
            if (serverInfo.isSavePwd())
            {
                elmServerInfo.addAttribute("pwd", serverInfo.getPwd());
            }
            elmServerInfo.addAttribute("maintainDB", serverInfo.getMaintainDB());
            //elmServerInfo.addAttribute("datLastSysOid", String.valueOf(serverInfo.getDatLastSysOid()));
            elmServerInfo.addAttribute("dbVersion", serverInfo.getVersion());
            elmServerInfo.addAttribute("isHighgo", String.valueOf(serverInfo.isHighgoDB()));
            if(serverInfo.isHighgoDB())
            {
                 elmServerInfo.addAttribute("kernelVersion", serverInfo.getKernelVersion());
                 elmServerInfo.addAttribute("hgdbCompatibility", serverInfo.getHgdbCompatibility());
            }
            elmServerInfo.addAttribute("serviceId", serverInfo.getService());
            elmServerInfo.setText(elmServerInfo.attributeValue("name") + "(" + elmServerInfo.attributeValue("host")
                    + ":" + elmServerInfo.attributeValue("port") + ")");
//            }
        }
        saveXMLFile(file, rddocument);
    }
    public void deleteServerFromXML(XMLServerInfoDTO serverInfo)throws DocumentException,IOException
    {
        logger.info("Enter:serverGroup=" + serverInfo.getGroup() + ",name=" + serverInfo.getName());
        try
        {
            String path = "conf"+File.separator+"catalog.xml";
            logger.info(path);
            File file = new File(path);
            logger.info("file exit:" + file.exists());
            if (!file.exists())
            {
                logger.error("catalog.xml not exist");
                //throw new Exception("catalog.xml not exist");
            }
            SAXReader reader = new SAXReader();
            reader.setEncoding(System.getProperty("file.encoding"));
            logger.debug("=reader encoding=" + reader.getEncoding());
            Document rddocument = reader.read(file);
            Element elmServerRoot = rddocument.getRootElement();
            for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
            {
                Element elmServerGroup = (Element) itt.next();
                //logger.info("GroupName=" + elmServerGroup.getText().trim());
                if (elmServerGroup.getText().trim().equals(serverInfo.getGroup()))
                {
                    for (Iterator it = elmServerGroup.elementIterator(); it.hasNext();)
                    {
                        Element elmServerInfo = (Element) it.next();
                        //logger.info("serverName" + elmServerInfo.attributeValue("name"));
                        if (elmServerInfo.attributeValue("name").equals(serverInfo.getName()))
                        {
                            elmServerGroup.remove(elmServerInfo);
                        }
                    }
                }
            }
            this.saveXMLFile(file, rddocument);
        }
        catch (DocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new DocumentException(ex);
        }
    }
    public void changeUsernameForServerFromXML(XMLServerInfoDTO serverInfo, String newUser) throws DocumentException,IOException
    {
        logger.info("Enter: serverGroup=" + serverInfo.getGroup() + ",name=" + serverInfo.getName());
        try
        {
            String path = "conf" + File.separator + "catalog.xml";
            logger.info(path);
            File file = new File(path);
            logger.info("file exit:" + file.exists());
            if (!file.exists())
            {
                logger.error("catalog.xml not exist");
                //throw new Exception("catalog.xml not exist");
            }
            SAXReader reader = new SAXReader();
            reader.setEncoding(System.getProperty("file.encoding"));//reader.setEncoding("utf-8");
            logger.debug("=reader encoding=" + reader.getEncoding());
            Document rddocument = reader.read(file);
            Element elmServerRoot = rddocument.getRootElement();
            for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
            {
                Element elmServerGroup = (Element) itt.next();
                //logger.info("GroupName=" + elmServerGroup.getText().trim());
                if (elmServerGroup.getText().trim().equals(serverInfo.getGroup()))
                {
                    for (Iterator it = elmServerGroup.elementIterator(); it.hasNext();)
                    {
                        Element elmServerInfo = (Element) it.next();
                        //logger.info("ServerName=" + elmServerInfo.attributeValue("name")+","+serverInfo.getName());
                        if (elmServerInfo.attributeValue("name").trim().equals(serverInfo.getName()))
                        {
                              elmServerInfo.attribute("user").setValue(newUser);
                        }
                    }
                }
            }
            this.saveXMLFile(file, rddocument);
        }catch (DocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new DocumentException(ex);
        }finally
        {
            logger.info("return");
        }
    }
    public void changePwdForServerFromXML(XMLServerInfoDTO serverInfo) throws DocumentException,IOException
    {
        logger.info("Enter: serverGroup=" + serverInfo.getGroup() + ",name=" + serverInfo.getName());
        try
        {
            String path = "conf"+File.separator+"catalog.xml";
            logger.info(path);
            File file = new File(path);
            logger.info("file exit:" + file.exists());
            if (!file.exists())
            {
                logger.error("catalog.xml not exist");
                //throw new Exception("catalog.xml not exist");
            }
            SAXReader reader = new SAXReader();
            reader.setEncoding(System.getProperty("file.encoding"));//reader.setEncoding("utf-8");
            logger.debug("=reader encoding=" + reader.getEncoding());
            Document rddocument = reader.read(file);
            Element elmServerRoot = rddocument.getRootElement();
            for (Iterator itt = elmServerRoot.elementIterator(); itt.hasNext();)
            {
                Element elmServerGroup = (Element) itt.next();
                //logger.info("GroupName=" + elmServerGroup.getText().trim());
                if (elmServerGroup.getText().trim().equals(serverInfo.getGroup()))
                {
                    for (Iterator it = elmServerGroup.elementIterator(); it.hasNext();)
                    {
                        Element elmServerInfo = (Element) it.next();
                        //logger.info("ServerName=" + elmServerInfo.attributeValue("name")+","+serverInfo.getName());
                        if (elmServerInfo.attributeValue("name").trim().equals(serverInfo.getName()))
                        {
                              elmServerInfo.attribute("pwd").setValue(serverInfo.getPwd());
                        }
                    }
                }
            }
            this.saveXMLFile(file, rddocument);
        }catch (DocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new DocumentException(ex);
        }finally
        {
            logger.info("return");
        }
    }
    //more server info -- unfinished
    public void getMoreServerInfo(XMLServerInfoDTO serverInfo) throws ClassNotFoundException,SQLException
    {
        logger.debug("Enter");
        HelperInfoDTO helperInfo = this.getHelperInfoFromServerInfo(serverInfo);
        TreeEnum.DBSYS sys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT current_setting('autovacuum'), usecreatedb, usesuper");
        if (this.isVersionHigherThan(sys, version, 8, 0))
        {
            sql.append(", CASE WHEN usesuper THEN pg_postmaster_start_time() ELSE NULL END as upsince");
        } else
        {
            sql.append(", CASE WHEN usesuper THEN pg_postmaster_starttime() ELSE NULL END as upsince");
        }
        if (this.isVersionHigherThan(sys, version, 8, 3))
        {
            sql.append(", CASE WHEN usesuper THEN pg_conf_load_time() ELSE NULL END as confloadedsince");
        }
        if (this.isVersionHigherThan(sys, version, 8, 4))
        {
            sql.append(", CASE WHEN usesuper THEN pg_is_in_recovery() ELSE NULL END as inrecovery");
                   //.append(", CASE WHEN usesuper THEN NULL ELSE NULL END as receiveloc") //.append(", CASE WHEN usesuper THEN pg_last_xlog_receive_location() ELSE NULL END as receiveloc")
                   // .append(", CASE WHEN usesuper THEN NULL ELSE NULL END as replayloc");//.append(", CASE WHEN usesuper THEN pg_last_xlog_replay_location() ELSE NULL END as replayloc");
        }
        sql.append(" FROM pg_user WHERE usename=current_user");
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs= null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();
            logger.debug(sql.toString());
            rs = stmt.executeQuery(sql.toString());
            if (rs.next())
            {
                 serverInfo.setInRecovery(String.valueOf(rs.getObject("inrecovery")));//notice null value
                 logger.debug("InRecovery=" + serverInfo.isInRecovery());
            }
            rs = stmt.executeQuery("SELECT datlastsysoid FROM pg_database WHERE datName='" + serverInfo.getMaintainDB() + "'");
            if (rs.next())
            {
                serverInfo.setDatLastSysOid(rs.getLong(1));
                logger.debug("DatLastSysOid=" + serverInfo.getDatLastSysOid());
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.debug("close stream");
        }        
    }
        
    
    //Object Intro - for tree node click action
    //OBJECT STATISTICS    
    private String getStatisticsSql(AbstractObject obj)
    {
        StringBuilder sql = new StringBuilder();
        HelperInfoDTO helperInfo = obj.getHelperInfo();
        String version = helperInfo.getVersionNumber();
        TreeEnum.DBSYS sys = helperInfo.getDbSys();
        switch (obj.getType())
        {
            case TABLESPACE:
                TablespaceInfoDTO tablespace = (TablespaceInfoDTO) obj;
                sql.append("SELECT pg_size_pretty(pg_tablespace_size(").append(tablespace.getOid()).append(")) AS ").append("tablespace_size");
                break;
            case DATABASE:
                DBInfoDTO db = (DBInfoDTO) obj;
                sql.append("SELECT numbackends AS ").append("backend")
                        .append(", xact_commit AS ").append("xact_committed")
                        .append(", xact_rollback AS ").append("xact_rolled_back")
                        .append(", blks_read AS ").append("block_read")
                        .append(", blks_hit AS ").append("block_hit");
                if (this.isVersionHigherThan(sys, version, 8, 2))
                {
                    sql.append(", tup_returned AS ").append("tuple_returned")
                            .append(", tup_fetched AS ").append("tuple_fetched")
                            .append(", tup_inserted AS ").append("tuple_inserted")
                            .append(", tup_updated AS ").append("tuple_updated")
                            .append(", tup_deleted AS ").append("tuple_deleted");
                }
                if (this.isVersionHigherThan(sys, version, 9, 0))
                {
                    sql.append(", stats_reset AS ").append("last_statistics_reset")
                            .append(", slave.confl_tablespace AS ").append("tablespace_conflicts")
                            .append(", slave.confl_lock AS ").append("lock_conflicts")
                            .append(", slave.confl_snapshot AS ").append("snapshot_conflicts")
                            .append(", slave.confl_bufferpin AS ").append("bufferpin_conflicts")
                            .append(", slave.confl_deadlock AS ").append("deadlock_conflicts");
                }
                if (this.isVersionHigherThan(sys, version, 9, 1))
                {
                    sql.append(", temp_files AS ").append("temporary_files")
                            .append(", temp_bytes AS ").append("size_of_temporary_files")
                            .append(", deadlocks AS ").append("deadlocks")
                            .append(", blk_read_time AS ").append("block_read_time")
                            .append(", blk_write_time AS ").append("block_write_time");
                }
                sql.append(", pg_size_pretty(pg_database_size(db.datid)) AS ").append("size");
                sql.append("\n  FROM pg_stat_database db");
                if (this.isVersionHigherThan(sys, version, 9, 0))
                {
                    sql.append(" JOIN pg_stat_database_conflicts slave ON db.datid=slave.datid");
                }
                sql.append(" WHERE db.datid=").append(db.getOid());
                break;
            case FUNCTION:
                FunctionInfoDTO fun = (FunctionInfoDTO) obj;
                sql.append("SELECT calls AS ").append("call_count")
                        .append(", total_time AS ").append("total_time")
                        .append(", self_time AS ").append("sel_time")
                        .append(" FROM pg_stat_user_functions ")
                        .append(" WHERE funcid=").append(fun.getOid());
                break;
            case TABLE:
                TableInfoDTO table = (TableInfoDTO) obj;
                sql.append("SELECT seq_scan AS ").append("sequential_scan")
                        .append(", seq_tup_read AS ").append("sequential_tuples_read")
                        .append(", idx_scan AS ").append("index_scan")
                        .append(", idx_tup_fetch AS ").append("index_tuples_fetched")
                        .append(", n_tup_ins AS ").append("tuples_inserted")
                        .append(", n_tup_upd AS ").append("tuples_updated")
                        .append(", n_tup_del AS ").append("tuples_deleted");

                if (this.isVersionHigherThan(sys, version, 8, 2))
                {
                    sql.append(", n_tup_hot_upd AS ").append("tuples_hot_updated")
                            .append(", n_live_tup AS ").append("live_tuples")
                            .append(", n_dead_tup AS ").append("dead_tuples");
                }
                sql.append(", heap_blks_read AS ").append("heap_blocks_read")
                        .append(", heap_blks_hit AS ").append("heap_blocks_hit")
                        .append(", idx_blks_read AS ").append("index_blocks_read")
                        .append(", idx_blks_hit AS ").append("index_blocks_hit")
                        .append(", toast_blks_read AS ").append("toast_blocks_read")
                        .append(", toast_blks_hit AS ").append("toast_blocks_hit")
                        .append(", tidx_blks_read AS ").append("toast_index_blocks_read")
                        .append(", tidx_blks_hit AS ").append("toast_index_blocks_hit");
                if (this.isVersionHigherThan(sys, version, 8, 1))
                {
                    sql.append(", last_vacuum AS ").append("last_vacuum")
                            .append(", last_autovacuum AS ").append("last_autovacuum")
                            .append(", last_analyze AS ").append("last_analyze")
                            .append(", last_autoanalyze AS ").append("last_auto_analyze");
                }

                if (this.isVersionHigherThan(sys, version, 9, 0))
                {
                    sql.append(", vacuum_count AS ").append("vacuum_counter")
                            .append(", autovacuum_count AS ").append("auto_vacuum_counter")
                            .append(", analyze_count AS ").append("analyze_counter")
                            .append(", autoanalyze_count AS ").append("auto_analyze_counter");
                }
//                if (GetConnection()->HasFeature(FEATURE_SIZE))
//                {
                sql.append(", pg_size_pretty(pg_relation_size(stat.relid)) AS ").append("table_size")
                        .append(", CASE WHEN cl.reltoastrelid = 0 THEN '").append("none'")
                        .append(" ELSE pg_size_pretty(pg_relation_size(cl.reltoastrelid)+ COALESCE((SELECT SUM(pg_relation_size(indexrelid)) FROM pg_index WHERE indrelid=cl.reltoastrelid)::int8, 0)) END AS ").append("toast_table_size")
                        .append(", pg_size_pretty(COALESCE((SELECT SUM(pg_relation_size(indexrelid)) FROM pg_index WHERE indrelid=stat.relid)::int8, 0)) AS ").append("index_size");
//                }
//	        if (showExtendedStatistics)
//                {
//                    sql.append("\n")
//                            .append(", tuple_count AS ").append(constBundle.getString("Tuple Count")).append(",\n")
//                            .append("  pg_size_pretty(tuple_len) AS ").append(constBundle.getString("Tuple Length")).append(",\n")
//                            .append("  tuple_percent AS ").append(constBundle.getString("Tuple Percent")).append(",\n")
//                            .append("  dead_tuple_count AS ").append(constBundle.getString("Dead Tuple Count")).append(",\n")
//                            .append("  pg_size_pretty(dead_tuple_len) AS ").append(constBundle.getString("Dead Tuple Length")).append(",\n")
//                            .append("  dead_tuple_percent AS ").append(constBundle.getString("Dead Tuple Percent")).append(",\n")
//                            .append("  pg_size_pretty(free_space) AS ").append(constBundle.getString("Free Space")).append(",\n")
//                            .append("  free_percent AS ").append(constBundle.getString("Free Percent")).append("\n")
//                            .append("  FROM pgstattuple('").append(GetQuotedFullIdentifier()).append("'), pg_stat_all_tables stat");
//                } else
//                {
                sql.append("\n").append("  FROM pg_stat_all_tables stat");
//                }
                sql.append("\n").append("  JOIN pg_statio_all_tables statio ON stat.relid = statio.relid\n")
                        .append("  JOIN pg_class cl ON cl.oid=stat.relid\n")
                        .append(" WHERE stat.relid = ").append(table.getOid());
                break;
            case COLUMN:
            case VIEW_COLUMN:
                sql.append("SELECT null_frac AS ").append("null_fraction")
                        .append(", avg_width AS ").append("average_width")
                        .append(", n_distinct AS ").append("distinct_value")
                        .append(", most_common_vals AS ").append("most_common_value")
                        .append(", most_common_freqs AS ").append("most_common_frequency")
                        .append(", histogram_bounds AS ").append("histogram_bound")
                        .append(", correlation AS ").append("correlation")
                        .append("  FROM pg_stats \n")
                        .append(" WHERE schemaname = '").append(helperInfo.getSchema()).append("' \n")
                        .append(" AND tablename = '").append(helperInfo.getRelation()).append("' \n")
                        .append(" AND attname = '").append(obj.getName()).append("' ");
                break;
            case INDEX:
                IndexInfoDTO index = (IndexInfoDTO) obj;
                sql.append("SELECT idx_scan AS ").append("index_scan")
                        .append(", idx_tup_read AS ").append("index_tuple_read")
                        .append(", idx_tup_fetch AS ").append("index_tuple_fetched")
                        .append(", idx_blks_read AS ").append("index_block_read")
                        .append(", idx_blks_hit AS ").append("index_block_hit");
//	        if (HasFeature(FEATURE_SIZE))
//                {
                sql.append(", pg_size_pretty(pg_relation_size(stat.indexrelid)) AS ").append("index_size");
//                }
//	        if (showExtendedStatistics)
//                {
//                    sql.append(", version AS ").append(constBundle.getString("Version")).append(",\n")
//                            .append("  tree_level AS ").append(constBundle.getString("Tree Level")).append(",\n")
//                            .append("  pg_size_pretty(index_size) AS ").append(_("Index Size")).append(",\n")
//                            .append("  root_block_no AS ").append(_("Root Block No")).append(",\n")
//                            .append("  internal_pages AS ").append(_("Internal Pages")).append(",\n")
//                            .append("  leaf_pages AS ").append(_("Leaf Pages")).append(",\n")
//                            .append("  empty_pages AS ").append(_("Empty Pages")).append(",\n")
//                            .append("  deleted_pages AS ").append(_("Deleted Pages")).append(",\n")
//                            .append("  avg_leaf_density AS ").append(_("Average Leaf Density")).append(",\n")
//                            .append("  leaf_fragmentation AS ").append(_("Leaf Fragmentation")).append("\n")
//                            .append("  FROM pgstatindex('").append(GetQuotedFullIdentifier()).append("'), pg_stat_all_indexes stat");
//                } else
//                {
                sql.append("\n").append("  FROM pg_stat_all_indexes stat");
//                }
                sql.append("\n  JOIN pg_statio_all_indexes statio ON stat.indexrelid = statio.indexrelid \n")
                        .append("  JOIN pg_class cl ON cl.oid=stat.indexrelid \n")
                        .append(" WHERE stat.indexrelid = ").append(index.getOid());
                break;
            default:
                logger.info(obj.getType() + " is an Exception type, do nothing and return an empty string");
                break;
        }
        return sql.toString();
    }
    public void getStatisticsList(AbstractObject obj, DefaultTableModel model) throws SQLException, ClassNotFoundException
    {
        if (obj == null)
        {
            logger.error("obj is null, do nothing and return.");
            return;
        }
        HelperInfoDTO helperInfo = obj.getHelperInfo();
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return;
        }
        String sql = this.getStatisticsSql(obj);
        if (sql == null || sql.isEmpty())
        {
            logger.error("sql is null, do nothing and return.");
            return;
        }
        //logger.debug(sql);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(),
                    helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            ResultSetMetaData metaData = rs.getMetaData();
            int column = metaData.getColumnCount();
            if (rs.next())
            {
                for (int i = 1; i <= column; i++)
                {
                    model.addRow(new Object[]
                    {
                        constBundle.getString(metaData.getColumnName(i)), rs.getObject(i)
                    });
                }
            } else
            {
                for (int i = 1; i <= column; i++)
                {
                    model.addRow(new Object[]
                    {
                        constBundle.getString(metaData.getColumnName(i)), ""
                    });
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
    
    //branch 
    //server branch: databases,tablespace,group role,login role
    public BranchDTO getServerBranchInfo(XMLServerInfoDTO serverInfo) throws SQLException, ClassNotFoundException, Exception
    {
        if (serverInfo == null)
        {
            logger.error("serverInfo is null,do nothing,return null");
            throw new Exception("serverInfo is null,do nothing,return null");
        }
        BranchDTO branch = new BranchDTO(TreeEnum.TreeNode.SERVER);
        Connection conn = null;
        Statement stmt = null;   
        String url = serverInfo.getPartURL() + serverInfo.getMaintainDB();
        logger.debug("url=" + url);
        try
        { 
            conn = JdbcHelper.getConnection(url, serverInfo);
            serverInfo.setConnected(true);
            stmt = conn.createStatement();
            HelperInfoDTO helperInfo = this.getHelperInfoFromServerInfo(serverInfo);
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.DATABASE_GROUP, TreeEnum.TreeNode.DATABASE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.TABLESPACE_GROUP, TreeEnum.TreeNode.TABLESPACE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.GROUPROLE_GROUP, TreeEnum.TreeNode.GROUPROLE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.LOGINROLE_GROUP, TreeEnum.TreeNode.LOGINROLE, helperInfo));
        } catch (SQLException ex)
        {
            logger.error("Failed Connect:url = " + url + ",user= " + serverInfo.getUser()
                    + ",pwd=" + serverInfo.getPwd() + ",isConnected =" + serverInfo.isConnected());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        return branch;
    }
    //database branch: catalog,cast,language,schema
    public BranchDTO getDBBranchInfo(HelperInfoDTO helperInfo) throws SQLException, ClassNotFoundException, Exception
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null,do nothing,return null");
            throw new Exception("helperInfo is null,do nothing and return.");
        } else if (helperInfo.getDbName() == null)
        {
            logger.error("dbName is null,do nothing,return null");
            throw new Exception("dbName is null,do nothing and return.");
        }
        BranchDTO branch = new BranchDTO(TreeEnum.TreeNode.DATABASE);
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            
            branch.add(this.getObjGroupInfo(stmt, TreeEnum.TreeNode.CATALOG_GROUP, TreeEnum.TreeNode.CATALOG, helperInfo));
            //branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.CAST_GROUP, TreeEnum.TreeNode.CAST, helperInfo));
            branch.add(this.getObjGroupInfo(stmt, TreeEnum.TreeNode.LANGUAGE_GROUP, TreeEnum.TreeNode.LANGUAGE, helperInfo));
            branch.add(this.getObjGroupInfo(stmt, TreeEnum.TreeNode.EVENT_TRIGGER_GROUP, TreeEnum.TreeNode.EVENT_TRIGGER, helperInfo));
            branch.add(this.getObjGroupInfo(stmt, TreeEnum.TreeNode.SCHEMA_GROUP, TreeEnum.TreeNode.SCHEMA, helperInfo));
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        return branch;
    }
    //schema branch: table,view,sequence ...
    public BranchDTO getSchemaBranchInfo(HelperInfoDTO helperInfo) throws SQLException, ClassNotFoundException, Exception
    {
        logger.debug("Enter getSchemaGroupInfo");
        if (helperInfo == null)
        {
            logger.error("helperInfo is null,do nothing and return.");
            throw new Exception("serverInfo is null,do nothing and return.");
        } else if (helperInfo.getDbName() == null)
        {
            logger.error("dbName is null,do nothing and return.");
            throw new Exception("dbName is null,do nothing and return.");
        }
        BranchDTO branch = new BranchDTO(TreeEnum.TreeNode.SCHEMA);
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.TABLE_GROUP, TreeEnum.TreeNode.TABLE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.VIEW_GROUP, TreeEnum.TreeNode.VIEW, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.SEQUENCE_GROUP, TreeEnum.TreeNode.SEQUENCE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.FUNCTION_GROUP, TreeEnum.TreeNode.FUNCTION, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.PROCEDURE_GROUP, TreeEnum.TreeNode.PROCEDURE, helperInfo));            
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        logger.debug("Return SchemaGroupInfo");
        return branch;
    }
    //catalog(system) branch: table,view..
    public BranchDTO getCatalogBranchInfo(HelperInfoDTO helperInfo) throws SQLException, ClassNotFoundException, Exception
    {
        //logger.debug("Enter getSchemaGroupInfo");
        if (helperInfo == null)
        {
            logger.error("helperInfo is null,do nothing,return null.");
            throw new Exception("helperInfo is null,do nothing,return null.");
        } else if (helperInfo.getDbName() == null)
        {
            logger.error("dbName is null,do nothing,return null.");
            throw new Exception("dbName is null,do nothing,return null.");
        }
        BranchDTO branch = new BranchDTO(TreeEnum.TreeNode.CATALOG);
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.TABLE_GROUP, TreeEnum.TreeNode.TABLE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.VIEW_GROUP, TreeEnum.TreeNode.VIEW, helperInfo));
//            branch.add(getObjGroupInfo(stmt,helperInfo.getScmName(), TreeEnum.TreeNode.SEQUENCE_GROUP));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.FUNCTION_GROUP, TreeEnum.TreeNode.FUNCTION, helperInfo));
        } catch (SQLException ex)
        {
            logger.error( ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        return branch;
    }
     public BranchDTO getViewBranchInfo(HelperInfoDTO helperInfo)throws SQLException, ClassNotFoundException, Exception
    {
        //logger.debug("Enter");
        if (helperInfo == null)
        {
            logger.error("serverInfo is null,do nothing,return null");
            throw new Exception("serverInfo is null,do nothing,return null");
        }  else if (helperInfo.getDbName() == null)
        {
            logger.error("dbName is null,do nothing,return null.");
            throw new Exception("dbName is null,do nothing,return null.");
        }     
        
        Long viewOid = helperInfo.getRelationOid();
        if (viewOid == null || viewOid==0)
        {
            logger.error("tabOid is null,do nothing,return null");
             throw new Exception("tabOid is null,do nothing,return null");
        }

        BranchDTO branch = new BranchDTO(TreeEnum.TreeNode.VIEW);
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();

            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.COLUMN_GROUP, TreeEnum.TreeNode.VIEW_COLUMN, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.RULE_GROUP, TreeEnum.TreeNode.RULE, helperInfo));
            if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
            {
                branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.TRIGGER_GROUP, TreeEnum.TreeNode.TRIGGER, helperInfo));
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        //logger.debug("Return tabBranchInfoList");
        return branch;
    }
    //table branch: column,constraint,index,rule,trigger
    public BranchDTO getTableBranchInfo(HelperInfoDTO helperInfo)throws SQLException, ClassNotFoundException, Exception
    {
        //logger.debug("Enter");
        if (helperInfo == null)
        {
            logger.error("serverInfo is null,do nothing,return null");
             throw new Exception("helperInfo is null,do nothing,return null");
        }
        Long tabOid = helperInfo.getRelationOid();
        if (tabOid == null)
        {
            logger.error("Error：tabOid is null,do nothing,return null");
             throw new Exception("tabOid is null,do nothing,return null");
        }
        
        BranchDTO branch = new BranchDTO(TreeEnum.TreeNode.TABLE);
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();

            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.COLUMN_GROUP, TreeEnum.TreeNode.COLUMN, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.CONSTRAINT_GROUP, TreeEnum.TreeNode.CONSTRAINT, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.INDEX_GROUP, TreeEnum.TreeNode.INDEX, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.RULE_GROUP, TreeEnum.TreeNode.RULE, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.TRIGGER_GROUP, TreeEnum.TreeNode.TRIGGER, helperInfo));
            branch.add(getObjGroupInfo(stmt, TreeEnum.TreeNode.PARTITION_GROUP, TreeEnum.TreeNode.PARTITION, helperInfo));
        } catch (SQLException ex)
        {
            logger.error( ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        //logger.debug("Return tabBranchInfoList");
        return branch;
    }
    //for all object group
    private ObjGroupDTO getObjGroupInfo(Statement stmt, TreeEnum.TreeNode groupType,
            TreeEnum.TreeNode objType, HelperInfoDTO helperInfo) throws SQLException, ClassNotFoundException, Exception
    {
        logger.info("Enter getObjGroupInfo");
        if (stmt == null)
        {
            logger.error("Error: stmt is null, do nothing,return null");
        }
        if ( groupType == null)
        {
            logger.error("Error: objType is null, do nothing,return null");
        }
        ObjGroupDTO objGroupInfoDTO = new ObjGroupDTO();
        objGroupInfoDTO.setHelperInfo(helperInfo);
        objGroupInfoDTO.setType(groupType);
        ResultSet rs = null;
        try
        {      
            String sql = this.getGroupSQL(groupType, helperInfo);
            List<ObjInfoDTO> objInfoList = new ArrayList();
            switch (groupType)
            {
                case FUNCTION_GROUP:
                case PROCEDURE_GROUP:
                    //the following two lines must in this order
                    Map types = this.getTypeMap(stmt);
                    rs = stmt.executeQuery(sql);
                    while(rs.next())
                    {
                        FunObjInfoDTO fun = new FunObjInfoDTO();
                        fun.setType(objType);
                        fun.setOid(rs.getLong("oid"));
                        fun.setName(rs.getString("proname"));
                        //fun.setFullname(rs.getString("fullname"));
                        fun.setOwner(rs.getString("proowner"));
                        fun.setComment(rs.getString("description"));                        
                        fun.setInArgTypeOids(rs.getString("proargtypes"));
                        //Long[] argTypeOidArray = (Long[]) rs.getArray("proargtypes").getArray();
                        fun.setInArgTypes(this.getFuncInArgStr(fun.getInArgTypeOids(), types));
                        objInfoList.add(fun);
                    }
                    break;
                case VIEW_GROUP:
                    rs = stmt.executeQuery(sql);
                    while (rs.next())
                    {
                        ObjInfoDTO objInfo = new ObjInfoDTO();
                        objInfo.setOid(rs.getLong(1));
                        objInfo.setName(rs.getString(2));
                        objInfo.setOwner(rs.getString(3));
                        objInfo.setComment(rs.getString(4));
                        if (rs.getBoolean("materialized"))
                        {
                            objInfo.setType(TreeEnum.TreeNode.MATERIALIZED_VIEW);
                        } else
                        {
                            objInfo.setType(objType);
                        }
                        objInfoList.add(objInfo);
                    }
                    break;
                case CONSTRAINT_GROUP:
                    rs = stmt.executeQuery(sql);
                    while (rs.next())
                    {
                        ObjInfoDTO objInfo = new ObjInfoDTO();
                        objInfo.setOid(rs.getLong(1));
                        objInfo.setName(rs.getString(2));
                        objInfo.setOwner(rs.getString(3));
                        objInfo.setComment(rs.getString(4));
                        objInfo.setType(this.getConstraintType(rs.getString(6)));
                        objInfoList.add(objInfo);
                    }
                    break;
                default:
                    rs = stmt.executeQuery(sql);
                    while (rs.next())
                    {
                        ObjInfoDTO objInfo = new ObjInfoDTO();
                        objInfo.setType(objType);
                        objInfo.setOid(rs.getLong(1));
                        objInfo.setName(rs.getString(2));
                        objInfo.setOwner(rs.getString(3));
                        objInfo.setComment(rs.getString(4));
                        objInfoList.add(objInfo);
                    }
                    break;
            }
            objGroupInfoDTO.setObjInfoList(objInfoList);
        } catch (SQLException ex)
        {
            logger.error( ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
        }
        return objGroupInfoDTO;
    }
    private String getFuncInArgStr(String inArgTypeOidStr, Map types)
    {
        //logger.debug(inArgTypeOidStr);
        if (inArgTypeOidStr != null
                && !inArgTypeOidStr.isEmpty()) {
            StringBuilder args = new StringBuilder();           
            String[] inArgTypeOidArray = inArgTypeOidStr.split(" ");
            boolean first = true;
            for (String oid : inArgTypeOidArray) {
                //logger.debug("oid = " + oid);
                if (first) {
                    first = false;
                } else {
                    args.append(",");
                }
                args.append(types.get(oid));
            }
            return args.toString();
        }
        return null;
    }
    private Map getTypeMap(Statement stmt)
    {
        Map hashMap = new HashMap();
        String sql = "select oid, format_type(oid, NULL) From pg_type";
        ResultSet rs = null;
        try
        {
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                hashMap.put(String.valueOf(rs.getLong(1)), rs.getString(2));
            }
        } catch (SQLException ex)
        {
            logger.info(ex.getMessage());
            ex.printStackTrace(System.out);
        } finally
        {
            JdbcHelper.close(rs);
        }
        return hashMap;
    }
    private String getGroupSQL(TreeEnum.TreeNode groupType, HelperInfoDTO helperInfo)throws Exception
    {
        if (groupType == null)
        {
            logger.error("groupType is null, do nothing and return null.");
            return null;
        }
        logger.info(groupType.toString());
        StringBuilder sql = new StringBuilder();
        switch (groupType)
        {
            //server
            case DATABASE_GROUP:
                sql.append("SELECT dat.oid, dat.datname, pg_get_userbyid(dat.datdba), des.description")
                        .append(" FROM pg_database dat")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON dat.oid = des.objoid")
                        .append(" WHERE dat.datname NOT IN('template1','template0') ")
                        .append(" ORDER BY dat.datname");
                break;
            case TABLESPACE_GROUP:
                sql.append(" SELECT spc.oid, spc.spcname, pg_get_userbyid(spc.spcowner),des.description")
                        .append(" FROM pg_tablespace spc")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON spc.oid = des.objoid")
                        .append(" ORDER BY spc.spcname");
                break;
            case GROUPROLE_GROUP:
                sql.append("SELECT aut.oid, aut.rolname ,NULL AS owner, des.description ")
                        .append(" FROM pg_roles aut") //pg_authid only can be read by superuser
                        .append(" LEFT OUTER JOIN pg_shdescription des ON aut.oid = des.objoid ")
                        .append(" WHERE aut.rolcanlogin = false")
                        .append(" ORDER BY aut.rolname ");
                break;
            case LOGINROLE_GROUP:
                sql.append("SELECT aut.oid, aut.rolname ,NULL AS owner, des.description ")
                        .append(" FROM pg_roles  aut") //pg_authid only can be read by superuser
                        .append(" LEFT OUTER JOIN pg_shdescription des ON aut.oid = des.objoid ")
                        .append(" WHERE aut.rolcanlogin = true")
                        .append(" ORDER BY aut.rolname ");
                break;
            //db
            case CATALOG_GROUP:
                sql.append("SELECT nsp.oid, nsp.nspname, pg_get_userbyid(nsp.nspowner),des.description")
                        .append(" FROM pg_namespace nsp")
                        .append(" LEFT OUTER JOIN pg_description des ON nsp.oid = des.objoid ")
                        .append(" WHERE nsp.nspname IN(")
                        .append(" 'pg_catalog', 'hgdb_catalog', ")
                        .append(" 'hgdb_oracle_catalog','hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog',")
                        .append(" 'oracle_catalog','mysql_catalog', 'mssql_catalog', 'db2_catalog',")
                        .append(" 'information_schema' )")
                        .append(" ORDER BY nsp.nspname;");
                break;
            case CAST_GROUP:
                sql.append(" SELECT cst.oid,")
                        .append("(format_type(typ1.oid,NULL)||'->'||format_type(typ2.oid,typ2.typtypmod)) AS name,")
                        .append(" null AS owner, dcs.description ")
                        .append(" FROM pg_cast cst ")
                        .append(" LEFT OUTER JOIN pg_type typ1 ON cst.castsource = typ1.oid ")
                        .append(" LEFT OUTER JOIN pg_type typ2 ON cst.casttarget = typ2.oid ")
                        .append(" LEFT OUTER JOIN pg_description dcs ON cst.oid = dcs.objoid ")
                        .append(" WHERE cst.oid >(SELECT DISTINCT datlastsysoid FROM pg_database) ")
                        .append(" ORDER BY (format_type(typ1.oid,NULL)||'->'||format_type(typ2.oid,typ2.typtypmod));");
                break;
            case LANGUAGE_GROUP:
                sql.append("SELECT lan.oid, lan.lanname, pg_get_userbyid(lan.lanowner), des.description")
                        .append(" FROM pg_language lan")
                        .append(" LEFT OUTER JOIN pg_description des ON lan.oid = des.objoid")
                        .append(" WHERE lan.lanispl =  true")
                        .append(" ORDER BY lan.lanname;");
                break;
            case EVENT_TRIGGER_GROUP:
                sql.append(" SELECT e.oid, e.evtname AS name")
                        .append(" , pg_get_userbyid(e.evtowner), des.description")
                        .append(" FROM pg_event_trigger e")
                        .append(" LEFT OUTER JOIN pg_description des ON e.oid = des.objoid")
                        .append(" ORDER BY e.evtname");
                break;
            case SCHEMA_GROUP:
                sql.append("SELECT nsp.oid, nsp.nspname, pg_get_userbyid(nsp.nspowner),des.description")
                        .append(" FROM pg_namespace nsp")
                        .append(" LEFT OUTER JOIN pg_description des ON nsp.oid = des.objoid ")
                        .append(" WHERE nsp.nspname NOT IN(")
                        .append(" 'pg_toast', 'pg_catalog', 'information_schema','hgdb_catalog',")
                        .append("  'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog'")
                        .append("  ,'oracle_catalog', 'mysql_catalog', 'mssql_catalog', 'db2_catalog')")
                        .append(" AND nsp.nspname NOT LIKE 'pg_temp_%' AND nsp.nspname NOT LIKE 'pg_toast_temp%'");
                //filter for sm
                sql.append(" AND (nsp.nspname IN('public') OR pg_get_userbyid(nsp.nspowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in nsp.nspacl::text)>0 )");
                       sql.append(" ORDER BY nsp.nspname;");
                break;
            //schema
            case TABLE_GROUP:
                sql.append("SELECT c.oid, c.relname,pg_get_userbyid(c.relowner) AS owner,")
                        .append("des.description AS comment,c.relnamespace AS schemaOid")
                        .append(" FROM pg_class c ")
                        .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                        .append(" LEFT JOIN pg_description des ON des.objoid = c.oid").append(" AND des.objsubid=0")//to omit column comment of column                       
                        //.append(" WHERE c.relkind = 'r'::\"char\" ")
                        .append(" WHERE c.relkind IN('r','p')")
                        .append(" AND c.relnamespace=").append(helperInfo.getSchemaOid())
                        .append(" AND c.relispartition = false ");
                //filter for sm
                sql.append(" AND (n.nspname IN('pg_catalog') OR pg_get_userbyid(c.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in c.relacl::text)>0 )");
                        sql.append(" ORDER BY c.relname;");
                break;
            case VIEW_GROUP:
                sql.append(" SELECT c.oid, c.relname, pg_get_userbyid(c.relowner) AS owner,")
                        .append(" description AS comment, c.relnamespace AS schemaOid")
                        .append(" , CASE WHEN c.relkind='m' THEN true ELSE false END AS materialized ")
                        .append(" FROM pg_class c ")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objoid=c.oid and des.objsubid=0)")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")
                        .append(" WHERE ((c.relhasrules AND (EXISTS (SELECT r.rulename FROM pg_rewrite r ")
                        .append(" WHERE ((r.ev_class = c.oid) AND (bpchar(r.ev_type) = '1'::bpchar)) ))) ")
                        .append(" OR (c.relkind IN ('v','m')))")
                        .append(" AND c.relnamespace=").append(helperInfo.getSchemaOid());
                //filter for sm
                sql.append(" AND (n.nspname IN('pg_catalog') OR pg_get_userbyid(c.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in c.relacl::text)>0 )");
                        sql.append(" ORDER BY c.relname");
                break;
            case SEQUENCE_GROUP:
                sql.append("SELECT c.oid, c.relname, pg_get_userbyid(relowner) AS owner,")//relfilenode,
                        .append("  description AS comment, c.relnamespace AS schemaOid")
                        .append(" FROM pg_class c")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid=c.oid")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")
                        .append(" WHERE relkind = 'S'")
                        .append(" AND c.oid > (SELECT DISTINCT datlastsysoid FROM pg_database)")
                        .append(" AND c.relnamespace=").append(helperInfo.getSchemaOid());
                //filter for sm
                sql.append(" AND (pg_get_userbyid(c.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in c.relacl::text)>0 )");
                        sql.append(" ORDER BY c.relname;");
                break;
            case FUNCTION_GROUP:
            case PROCEDURE_GROUP:
/* SELECT pr.oid, pr.proname, pr.proargtypes
, pg_get_userbyid(proowner) as funcowner, des. description
, pr.proargtypes
, pr.proname || '(' || COALESCE(pg_catalog.pg_get_function_identity_arguments(pr.oid), '') || ')' as fullname
--  ,pr.pronamespace AS nsp, lanname
FROM pg_proc pr
JOIN pg_type typ ON typ.oid=prorettype
LEFT OUTER JOIN pg_description des ON des.objoid=pr.oid
-- JOIN pg_language lng ON lng.oid=prolang
-- JOIN pg_namespace nsp ON nsp.oid=pr.pronamespace
WHERE pr.prokind = 'p'::char
    AND typname NOT IN ('trigger', 'event_trigger')
    AND pr.pronamespace=2200
ORDER BY pr.proname */
                String type = TreeEnum.TreeNode.PROCEDURE_GROUP == groupType ? "IN ('p'::char)" : "IN ('f'::char)";
                String sqlstr = "SELECT pr.oid, pr.proname \n"
                        + " , pg_get_userbyid(proowner) as proowner, des.description \n"
                        + " , pr.proargtypes \n "
                        + " , pr.proname || '(' || COALESCE(pg_catalog.pg_get_function_identity_arguments(pr.oid), '') || ')' as fullname\n"  
                        + " FROM pg_proc pr\n"
                        + " JOIN pg_type typ ON typ.oid=prorettype\n"
                        + " LEFT OUTER JOIN pg_namespace n ON n.oid = pr.pronamespace\n"
                        + " LEFT OUTER JOIN pg_description des ON des.objoid=pr.oid\n"
                        + " WHERE pr.prokind " + type + " \n"
                        + "   AND typname NOT IN ('trigger', 'event_trigger')\n"
                        + "   AND pr.pronamespace="+ helperInfo.getSchemaOid() + "\n"
                        + " AND (n.nspname IN('pg_catalog') OR pg_get_userbyid(pr.proowner) IN('" + helperInfo.getUser() + "') OR position('" + helperInfo.getUser() + "' in pr.proacl::text)>0 ) \n" //for sm
                        + "ORDER BY pr.proname";
                sql.append(sqlstr);
                break;
            //table or view
            case PARTITION_GROUP:
                sql.append("SELECT c.oid, c.relname, pg_get_userbyid(c.relowner) AS owner")
                        .append(" ,des.description AS comment, c.relnamespace")
                        .append(" FROM pg_catalog.pg_class c ")
                        .append(" LEFT OUTER JOIN pg_catalog.pg_inherits i ON (c.oid=i.inhrelid )")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objoid = c.oid  AND des.objsubid=0) ")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")//for sm 
                        .append(" WHERE i.inhparent= ").append(helperInfo.getRelationOid())
                        .append(" AND c.relispartition=true "); //show partion except inherit
                //filter for sm
                sql.append(" AND (n.nspname IN('pg_catalog') OR pg_get_userbyid(c.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in c.relacl::text)>0 )");
                        sql.append(" ORDER BY c.relname");
                break;
            case COLUMN_GROUP:
                sql.append("SELECT att.attnum AS posiztion, att.attname,NULL  AS owner,")//pg_get_userbyid(cls.relowner)
                        .append(" des.description, cls.relnamespace as schemaoid")
                        .append(" FROM pg_attribute att")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objsubid = att.attnum AND des.objoid = att.attrelid)")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = att.attrelid")
                       // .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = cls.relnamespace")//for sm 
                        .append(" WHERE att.attnum > 0 ")
                        .append(" AND att.attisdropped = false")
                        .append(" AND att.attrelid = ").append(helperInfo.getRelationOid());
                //filter for sm
                sql.append(" AND (  pg_get_userbyid(cls.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in cls.relacl::text)>0 )");
                        sql.append(" ORDER BY att.attnum,att.attname").append(";");
                break;
            case CONSTRAINT_GROUP:
                sql.append("SELECT DISTINCT con.oid, con.conname, NULL AS owner,")//pg_get_userbyid(cls.relowner)
                        .append(" des.description, cls.relnamespace as schemaoid, con.contype ")
                        .append(" FROM pg_constraint con")
                        .append(" LEFT OUTER JOIN pg_roles aut ON con.oid = aut.oid")//pg_authid only can be read by superuser
                        .append(" LEFT OUTER JOIN pg_description des ON con.oid = des.objoid")
                        .append(" LEFT OUTER JOIN pg_namespace np ON con.connamespace = np.oid")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = con.conrelid")
                        .append(" WHERE  np.nspname NOT IN('information_schema')")
                        .append(" AND con.contype NOT IN('t')")
                        .append(" AND con.conrelid = ").append(helperInfo.getRelationOid());
                //filter for sm
                sql.append(" AND (pg_get_userbyid(cls.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in cls.relacl::text)>0 )");
                        sql.append(" ORDER BY con.contype, con.conname;");
                break;
            case INDEX_GROUP:
                sql.append("SELECT idx.indexrelid AS idxoid, cls.relname AS idxname, NULL AS owner,")//pg_get_userbyid(cls.relowner)
                        .append(" des.description, cls.relnamespace as schemaoid")
                        .append(" FROM pg_index idx")
                        .append(" JOIN pg_class cls ON cls.oid=indexrelid")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid=idx.indexrelid")
                        .append(" LEFT JOIN pg_depend dep ON (dep.classid = cls.tableoid AND dep.objid = cls.oid ")
                        .append(" AND dep.refobjsubid = '0' AND dep.refclassid=(SELECT oid FROM pg_class WHERE relname='pg_constraint'))")
                        .append(" LEFT OUTER JOIN pg_constraint con ON (con.tableoid = dep.refclassid AND con.oid = dep.refobjid)")
                        .append(" WHERE con.oid IS NULL ")
                        .append(" AND idx.indrelid = ").append(helperInfo.getRelationOid());
                //filter for sm
                sql.append(" AND (pg_get_userbyid(cls.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in cls.relacl::text)>0 )");
                        sql.append(" ORDER BY cls.relname");
                break;
            case RULE_GROUP:
                sql.append("SELECT rew.oid, rew.rulename, NULL AS owner,")//pg_get_userbyid(cls.relowner)
                        .append(" des.description, cls.relnamespace as schemaoid")
                        .append(" FROM pg_rewrite rew ")
                        .append(" LEFT OUTER JOIN pg_description des ON rew.oid = des.objoid ")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid=rew.ev_class")
                        .append(" WHERE rew.ev_class = ").append(helperInfo.getRelationOid());
                //filter for sm
                sql.append(" AND (pg_get_userbyid(cls.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in cls.relacl::text)>0 )");
                        sql.append(" ORDER BY rew.rulename");
                break;
            case TRIGGER_GROUP:
                sql.append(" SELECT tri.oid, tri.tgname, NULL AS owner,")//pg_get_userbyid(cls.relowner)
                        .append(" des.description, cls.relnamespace as schemaoid")
                        .append(" FROM pg_trigger tri")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid = tri.oid")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = tri.tgrelid")
                        .append(" WHERE tri.tgisinternal = false")
                        .append(" AND tri.tgrelid = ").append(helperInfo.getRelationOid());
                //filter for sm
                sql.append(" AND (pg_get_userbyid(cls.relowner) IN('").append(helperInfo.getUser()).append("') OR position('").append(helperInfo.getUser()).append("' in cls.relacl::text)>0 )");
                        sql.append(" ORDER BY tri.tgname");
                break;
            default:
                String error = "There's no sql for type=" + groupType;
                logger.error(error);
                throw new Exception(error);
        }
        logger.debug("Return: " + sql.toString());
        return sql.toString();
    }

    
       
    //Object Property
    private String getObjPropertySQL(AbstractObject objInfo,HelperInfoDTO helperInfo) throws Exception//ObjInfoDTO
    {
        TreeEnum.DBSYS dbsys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        StringBuilder sql = new StringBuilder();
        TreeEnum.TreeNode type = objInfo.getType();
        logger.info("type="+type);
        String name = objInfo.getName().replaceAll("'", "''");
        switch (type)
        {
            case TABLESPACE:
                if (isVersionHigherThan(dbsys, version, 9, 1))
                {
                    sql.append("SELECT spcname, ts.oid, pg_get_userbyid(spcowner) AS spcowner")
                            .append(" ,pg_catalog.pg_tablespace_location(ts.oid) AS spclocation")
                            .append(" ,spcacl, pg_catalog.shobj_description(oid, 'pg_tablespace') AS comment ")
                            .append(" ,spcoptions")//for varivales
                            .append(" ,(SELECT array_agg(label) FROM pg_shseclabel sl1 WHERE sl1.objoid=ts.oid) AS labels ")
                            .append(" ,(SELECT array_agg(provider) FROM pg_shseclabel sl2 WHERE sl2.objoid=ts.oid) AS providers ")
                            .append(" FROM pg_tablespace ts");
                } else
                {
                    sql.append("SELECT spcname, ts.oid, pg_get_userbyid(spcowner) AS spcowner")
                            .append(" ,spclocation")
                            .append(" ,spcacl, pg_catalog.shobj_description(oid, 'pg_tablespace') AS comment")
                            .append(" ,spcoptions")//for varivales
                            .append(" FROM pg_tablespace ts");
                }
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //TablespaceInfoDTO tsp = (TablespaceInfoDTO) objInfo;
                    sql.append(" WHERE ts.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" WHERE ts.spcname='").append(name).append("'");
                }
                sql.append(" ORDER BY spcname");
                break;
            case GROUPROLE: 
            case LOGINROLE:
                String table;
                if(this.hasPrivilege(helperInfo, "table","pg_authid", "select"))
                {
                    table = "pg_authid";
                }else
                {
                     table = "pg_roles";
                }
                sql.append("SELECT auth.rolname AS name, auth.oid AS oid, auth.rolpassword as pwd, auth.rolvaliduntil AS accountexpires")
                        .append(",auth.rolcanlogin AS isCanLogin, auth.rolsuper AS isSupperuser")
                        .append(",auth.rolcreatedb AS isCreateDatabases, auth.rolcreaterole AS isCreateRoles")
                        //ygq v5 admin update start
                        //.append(",auth.rolcatupdate AS isUpdateCatalog, auth.rolinherit AS isInherits")
                        .append(",false AS isUpdateCatalog, auth.rolinherit AS isInherits")
                        //ygq v5 admin update end
                        .append(",auth.rolconnlimit AS isConnectionLimit")
                        .append(",sdscpt.description AS comment");
                if (isVersionHigherThan(dbsys, version, 9, 0))
                {
                    sql.append(",auth.rolreplication");
                }
                sql.append(" FROM ").append(table).append(" auth ") //pg_authid only can access by superuser
                        .append(" LEFT OUTER JOIN pg_shdescription sdscpt ON auth.oid = sdscpt.objoid")
                        .append(" WHERE rolcanlogin = ").append(type == TreeEnum.TreeNode.LOGINROLE);
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //RoleInfoDTO role = (RoleInfoDTO) objInfo;
                    sql.append(" AND auth.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND auth.rolname = '").append(objInfo.getName()).append("'");
                }
                sql.append(" ORDER BY auth.rolname;");
                break;
            case DATABASE:
                sql.append("select db.datname AS name, db.oid AS oid, pg_get_userbyid(db.datdba) AS owner")//usr.usename AS owner,")
                        .append(",db.datacl AS acl, tbspc.spcname AS tablespace")
                        .append(",current_setting('default_tablespace') AS defaulttablespace")
                        .append(",pg_encoding_to_char(db.encoding) AS serverencoding")
                        .append(",db.datcollate AS collation, db.datctype AS characterType")
                        .append(",current_setting('search_path') AS defaultSchema")
                        .append(",db.datconnlimit AS connectionlimit")
                        .append(",des.description AS comment")
                        .append(",db.datallowconn AS isAllowConnection, has_database_privilege(db.oid, 'CREATE') AS cancreate")
                        .append(",db.datlastsysoid ")
                        .append(" FROM pg_database db")
                        //.append(" LEFT OUTER JOIN pg_user usr ON db.datdba = usr.usesysid")
                        .append(" LEFT OUTER JOIN pg_tablespace tbspc ON db.dattablespace = tbspc.oid")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON db.oid = des.objoid")
                        .append(" WHERE db.datistemplate = false");
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //DBInfoDTO db = (DBInfoDTO) objInfo;
                    sql.append(" AND db.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND db.datname = '").append(name).append("'");
                }
                sql.append(" ORDER BY db.datname;");
                break;
            case CAST:
                sql.append("SELECT (format_type(typ1.oid,NULL)||'->'||format_type(typ2.oid,typ2.typtypmod)) AS name")
                        .append(" ,cst.oid AS oid, format_type(typ1.oid,NULL) AS sourceType")
                        .append(" ,format_type(typ2.oid,typ2.typtypmod) AS targetType")
                        .append(" ,pr.proname AS function, cst.castcontext AS context")
                        .append(" ,dcpt.description AS comment")
                        .append(" FROM pg_cast cst")
                        .append(" LEFT OUTER JOIN pg_type typ1 ON cst.castsource = typ1.oid")
                        .append(" LEFT OUTER JOIN pg_type typ2 ON cst.casttarget = typ2.oid")
                        .append(" LEFT OUTER JOIN pg_description dcpt ON cst.oid = dcpt.objoid")
                        .append(" LEFT JOIN pg_proc pr ON cst.castfunc = pr.oid")
                        .append(" WHERE cst.oid >(SELECT DISTINCT datlastsysoid FROM pg_database)");
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //CastInfoDTO cast = (CastInfoDTO) objInfo;
                    sql.append("AND cst.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND name='").append(name).append("'");
                }
                sql.append(" ORDER BY cst.oid;");
                break;
            case LANGUAGE:
                sql.append("SELECT lan.lanname AS name, lan.oid AS oid, auth.rolname AS owner")
                        .append(",lan.lanacl AS acl, lan.lanpltrusted AS istrusted")
                        .append(",lan.lanplcallfoid AS handler, lan.lanvalidator AS validator")
                        .append(",dscpt.description AS comment")
                        .append(" FROM pg_language  lan")
                        .append(" LEFT OUTER JOIN pg_roles auth ON lan.lanowner = auth.oid")//pg_authid
                        .append(" LEFT OUTER JOIN pg_description dscpt ON lan.oid =dscpt.objoid")
                        .append(" WHERE lan.lanispl =  true");
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //LanguageInfoDTO lan = (LanguageInfoDTO) objInfo;
                    sql.append(" AND lan.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND lan.lanname='").append(name).append("'");
                }
                sql.append(" ORDER BY lan.lanname;");
                break;
            case EVENT_TRIGGER:
                sql.append("SELECT e.oid, e.xmin, e.evtname AS name, upper(e.evtevent) AS eventname,")
                        .append("pg_catalog.pg_get_userbyid(e.evtowner) AS eventowner,")
                        .append("e.evtenabled AS enabled,")
                        .append("e.evtfoid AS eventfuncoid,")
                        .append("CASE WHEN n.nspname = 'public'")
                        .append("THEN quote_ident(n.nspname) || '.' || cast(e.evtfoid::regproc as text)")
                        .append("ELSE cast(e.evtfoid::regproc as text) END AS  eventfunname,")
                        .append("array_to_string(array(select quote_literal(x) from unnest(evttags) as t(x)), ', ') AS when,")
                        .append("pg_catalog.obj_description(e.oid, 'pg_event_trigger') AS comment,")
                        .append("(SELECT array_agg(provider || '=' || label) FROM pg_seclabel sl1 WHERE sl1.objoid=e.oid) AS seclabels,")
                        .append("p.prosrc AS source, p.pronamespace AS schemaoid, l.lanname AS language")
                        .append(" FROM pg_event_trigger e")
                        .append(" LEFT OUTER JOIN pg_proc p ON p.oid=e.evtfoid")
                        .append(" LEFT OUTER JOIN pg_language l ON l.oid=p.prolang,")
                        .append(" pg_namespace n WHERE p.pronamespace = n.oid");
                if (objInfo.getOid() != null && objInfo.getOid() != 0) {
                    sql.append(" AND e.oid = ").append(objInfo.getOid());
                } else {
                    sql.append(" AND e.evtname = '").append(name).append("'");
                }
                sql.append(" ORDER BY e.evtname");
                break;
            case SCHEMA:
                sql.append("select nmspc.nspname AS name, nmspc.oid AS oid, auth.rolname AS owner")
                        .append(" ,nmspc.nspacl AS acl")
                        .append(" ,sdpt.description AS comment")
                        .append(" FROM pg_namespace nmspc ")
                        .append(" LEFT OUTER JOIN pg_description sdpt ON nmspc.oid = sdpt.objoid ")
                        .append(" LEFT OUTER JOIN pg_roles auth ON nmspc.nspowner = auth.oid ")//pg_authid
                        .append(" WHERE nmspc.nspname NOT IN ")
                        .append(" ('pg_toast', 'pg_catalog', 'information_schema', 'hgdb_catalog', ")
                        .append(" 'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog'")//1.3.0
                        .append(" ,'oracle_catalog', 'mysql_catalog', 'mssql_catalog', 'db2_catalog')")//since 1.3.1
                        .append(" AND nmspc.nspname NOT LIKE 'pg_temp_%' AND nmspc.nspname NOT LIKE 'pg_toast_temp%'");
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //SchemaInfoDTO schema = (SchemaInfoDTO) objInfo;
                    sql.append(" AND nmspc.oid = ").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND nmspc.nspname = '").append(name).append("'");
                }
                sql.append(" ORDER BY nmspc.nspname;");
                break;
            case CATALOG:
                sql.append("select nmspc.nspname AS name, nmspc.oid AS oid, auth.rolname AS owner")
                        .append(" ,nmspc.nspacl AS acl")
                        .append(" ,sdpt.description AS comment")
                        .append(" FROM pg_namespace nmspc ")
                        .append(" LEFT OUTER JOIN pg_description sdpt ON nmspc.oid = sdpt.objoid ")
                        .append(" LEFT OUTER JOIN pg_roles auth ON nmspc.nspowner = auth.oid ")//pg_authid
                        .append(" WHERE nmspc.nspname IN ")
                        .append(" ('pg_catalog', 'hgdb_catalog',")
                        .append("   'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog',  'hgdb_db2_catalog', ")//1.3.0
                        .append("   'oracle_catalog', 'mysql_catalog', 'mssql_catalog',  'db2_catalog', ")//since 1.3.1
                        .append("   'information_schema') ");
                if (objInfo.getOid() != null && objInfo.getOid()!=0)
                {
                    sql.append(" AND nmspc.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND nmspc.nspname ='").append(name).append("'");
                }
                sql.append(" ORDER BY nmspc.nspname;");
                break;
            case SEQUENCE:
                sql.append("SELECT c.relname, c.oid AS oid, pg_get_userbyid(relowner) AS owner ")//c.relnamespace AS schemaOid, 
                        .append(" ,relacl")
                        .append(" ,description AS comment ")
                        .append(" FROM pg_class c")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid=c.oid")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")
                        .append(" WHERE relkind = 'S'")
                        .append(" AND c.oid > (SELECT DISTINCT datlastsysoid FROM pg_database)");
                if (objInfo.getOid() != null && objInfo.getOid() != 0)
                {
                    sql.append(" AND c.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND c.relnamespace = ").append(helperInfo.getSchemaOid())
                            .append(" AND c.relname = '").append(name).append("'");
                }
                sql.append(" ORDER BY c.relname;");
                break;
            case PROCEDURE:

            case FUNCTION:
                sql.append("SELECT pr.proname, pr.oid, pg_get_userbyid(pr.proowner) as proowner")
                        .append(",typns.nspname AS typnsp, lng.lanname")
                        .append(",pr.pronargs,pr.proargtypes")
                        .append(",pr.proallargtypes, pr.proargmodes, pr.proargnames")
                        .append(",pg_get_expr(proargdefaults, 'pg_catalog.pg_class'::regclass) AS proargdefaultvals")
                        .append(",format_type(typ.oid, NULL) AS returntype, pr.prosrc, pr.probin, pr.procost")
                        .append(",CASE WHEN pr.provolatile='i' THEN 'IMMUTABLE' WHEN pr.provolatile='s' THEN 'STABLE' WHEN pr.provolatile='v' THEN 'VOLATILE'  END AS volatile")
                        .append(",pr.proretset")
                        //.append("--,pr.proleakproof ")//version>=9.2
                        .append(",pr.prosecdef, pr.proisstrict")
                        .append(isVersionHigherThan(dbsys, version, 10, 11)?", pr.prokind='w' AS proiswindow" : ", pr.proiswindow")
                        .append(", pr.proleakproof, pr.proparallel, pr.proacl, des.description")
                        .append(",pg_get_functiondef(pr.oid)")
                        .append(",proconfig");//for variables
                if (isVersionHigherThan(dbsys, version, 9, 0))
                {
                    sql.append(",\n(SELECT array_agg(label) FROM pg_seclabels sl1 WHERE sl1.objoid=pr.oid) AS labels")
                            .append(",\n(SELECT array_agg(provider) FROM pg_seclabels sl2 WHERE sl2.objoid=pr.oid) AS providers");
                }
                sql.append(" FROM pg_proc pr")
                        .append(" JOIN pg_type typ ON typ.oid= pr.prorettype")
                        .append(" JOIN pg_namespace typns ON typns.oid=typ.typnamespace")
                        .append(" JOIN pg_language lng ON lng.oid=prolang")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid=pr.oid");
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    sql.append(" WHERE  pr.oid=").append(objInfo.getOid());
                } else
                {
                    FunObjInfoDTO fun = (FunObjInfoDTO) objInfo;
                    sql.append(" WHERE pronamespace =").append(helperInfo.getSchemaOid())
                            .append(" AND pr.proname='").append(name).append("'")
                            .append(" AND pr.proargtypes='").append(fun.getInArgTypeOids()).append("'");
                }
                sql.append(" ORDER BY proname");
                break;                
            case TABLE:
                sql.append("SELECT rel.relname, rel.oid, pg_get_userbyid(rel.relowner) AS relowner")//rel.relnamespace AS schemaOid,
                        .append(",ta.spcname AS tablesapce")//CASE WHEN rel.reltablespace=0 THEN NULL ELSE ta.spcname END AS tablesapce
                        .append(",rel.relacl AS acl, CASE WHEN rel.reloftype=0 THEN null ELSE pg_type.typname END AS oftype")
                        .append(",substring(pg_get_constraintdef(c.oid) FROM 'PRIMARY KEY (.*)') AS PKcolumns")
                        .append(",rel.reltuples AS rowEstimate")
                        //.append("CASE WHEN <100 THEN (SELECT COUNT(*) FROM ").append(objInfo.getName()).append(") ELSE 0 END AS rowCount,")
                        .append(",substring(array_to_string(rel.reloptions, ',') FROM 'fillfactor=([0-9]*)') AS fillfactor")
                        .append(",rel.relhassubclass")
                        .append(",rel.relkind")//kind=p bepartitioned  
                        //modify begin by sunqk at 20191225 for bugs v4.3.4.8-TD-12 
                        //if the table's new create, so object.getoid's NULL, repair this bug
                        .append(", (CASE WHEN rel.relkind = 'p' THEN pg_get_partkeydef(rel.oid) ELSE '' END) AS partitionSchema ");
                        //modify   end by sunqk at 20191225 for bugs v4.3.4.8-TD-12 
                //hg1.3 not support .append("CASE WHEN rel.relpersistence='u' THEN true ELSE false END AS isunlogged,")
                if (isVersionHigherThan(dbsys, version, 9, 0))
                {
                    sql.append(",CASE WHEN rel.relpersistence='u' THEN true ELSE false END AS isunlogged");
                } else
                {
                    sql.append(",false AS isunlogged");
                }
                sql.append(isVersionHigherThan(dbsys, version, 11, 9)? ", false AS relhasoids" : ", rel.relhasoids")//rel.relhasoids not support
                        .append(",CASE WHEN rel.reltoastrelid = 0 THEN false ELSE true END AS hastoasttable")                        
                        .append(",description AS comment ")
                        .append(",EXISTS(select 1 FROM pg_trigger JOIN pg_proc pt ON pt.oid=tgfoid AND pt.proname='logtrigger'")
                        .append(" JOIN pg_proc pc ON pc.pronamespace=pt.pronamespace AND pc.proname='slonyversion'")
                        .append(" WHERE tgrelid=rel.oid) AS isreplication")                        
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_enabled=([a-z|0-9]*)') AS autovacuum_enabled")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_threshold=([0-9]*)') AS autovacuum_vacuum_threshold ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_scale_factor=([0-9]*[.][0-9]*)') AS autovacuum_vacuum_scale_factor ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_analyze_threshold=([0-9]*)') AS autovacuum_analyze_threshold ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_analyze_scale_factor=([0-9]*[.][0-9]*)') AS autovacuum_analyze_scale_factor ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_cost_delay=([0-9]*)') AS autovacuum_vacuum_cost_delay ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_cost_limit=([0-9]*)') AS autovacuum_vacuum_cost_limit ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_freeze_min_age=([0-9]*)') AS autovacuum_freeze_min_age ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_freeze_max_age=([0-9]*)') AS autovacuum_freeze_max_age ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_freeze_table_age=([0-9]*)') AS autovacuum_freeze_table_age")
                        //for toast table
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_enabled=([a-z|0-9]*)') AS autovacuum_enabled_toast")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_threshold=([0-9]*)') AS autovacuum_vacuum_threshold_toast ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_scale_factor=([0-9]*[.][0-9]*)') AS autovacuum_vacuum_scale_factor_toast ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_cost_delay=([0-9]*)') AS autovacuum_vacuum_cost_delay_toast ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_vacuum_cost_limit=([0-9]*)') AS autovacuum_vacuum_cost_limit_toast ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_freeze_min_age=([0-9]*)') AS autovacuum_freeze_min_age_toast ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_freeze_max_age=([0-9]*)') AS autovacuum_freeze_max_age_toast ")
                        .append(", substring(array_to_string(rel.reloptions, ',') FROM 'autovacuum_freeze_table_age=([0-9]*)') AS autovacuum_freeze_table_age_toast")
                        .append(" FROM pg_class rel")
                        .append(" LEFT OUTER JOIN pg_tablespace ta on ta.oid=rel.reltablespace")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objoid=rel.oid AND des.objsubid=0)")
                        .append(" LEFT OUTER JOIN pg_constraint c ON c.conrelid=rel.oid AND c.contype='p'")
                        .append(" LEFT JOIN pg_type ON reloftype=pg_type.oid")
                        .append(" LEFT OUTER JOIN pg_class cl ON rel.reltoastrelid = cl.oid")
                        .append(" WHERE rel.relkind IN ('r','p') ")//r , s, t, p
                        .append(" AND NOT rel.relispartition ");
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    sql.append(" AND rel.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND rel.relnamespace = ").append(helperInfo.getSchemaOid())
                            .append(" AND rel.relname = '").append(name).append("'");
                }
                sql.append(" ORDER BY rel.relname;");
                break;  
            case VIEW:
            case MATERIALIZED_VIEW:
                sql.append("SELECT c.relname, c.oid, pg_get_userbyid(c.relowner) AS viewowner")//c.relnamespace AS schemaOid,
                        .append(" ,c.relacl")
                        .append(" ,pg_get_viewdef(c.oid) AS definition, des.description AS comment");
                if (isVersionHigherThan(dbsys, version, 9, 1))//>=9.2
                {
                    sql.append(",\nsubstring(array_to_string(c.reloptions, ',') FROM 'security_barrier=([a-z|0-9]*)') AS security_barrier");
                }
                if (isVersionHigherThan(dbsys, version, 9, 2))//>=9.3
                {
                    sql.append(", substring(array_to_string(c.reloptions, ',') FROM 'fillfactor=([0-9]*)') AS fillfactor \n");
                    sql.append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_enabled=([a-z|0-9]*)') AS autovacuum_enable \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_vacuum_threshold=([0-9]*)') AS autovacuum_vacuum_threshold \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_vacuum_scale_factor=([0-9]*[.][0-9]*)') AS autovacuum_vacuum_scale_factor \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_analyze_threshold=([0-9]*)') AS autovacuum_analyze_threshold \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_analyze_scale_factor=([0-9]*[.][0-9]*)') AS autovacuum_analyze_scale_factor \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_vacuum_cost_delay=([0-9]*)') AS autovacuum_vacuum_cost_delay \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_vacuum_cost_limit=([0-9]*)') AS autovacuum_vacuum_cost_limit \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_freeze_min_age=([0-9]*)') AS autovacuum_freeze_min_age \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_freeze_max_age=([0-9]*)') AS autovacuum_freeze_max_age \n")
                            .append(", substring(array_to_string(c.reloptions, ',') FROM 'autovacuum_freeze_table_age=([0-9]*)') AS autovacuum_freeze_table_age \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_enabled=([a-z|0-9]*)') AS toast_autovacuum_enabled \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_vacuum_threshold=([0-9]*)') AS toast_autovacuum_vacuum_threshold \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_vacuum_scale_factor=([0-9]*[.][0-9]*)') AS toast_autovacuum_vacuum_scale_factor \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_vacuum_cost_delay=([0-9]*)') AS toast_autovacuum_vacuum_cost_delay \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_vacuum_cost_limit=([0-9]*)') AS toast_autovacuum_vacuum_cost_limit \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_freeze_min_age=([0-9]*)') AS toast_autovacuum_freeze_min_age \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_freeze_max_age=([0-9]*)') AS toast_autovacuum_freeze_max_age \n")
                            .append(", substring(array_to_string(tst.reloptions, ',') FROM 'autovacuum_freeze_table_age=([0-9]*)') AS toast_autovacuum_freeze_table_age \n")
                            .append(", c.reloptions AS reloptions, tst.reloptions AS toast_reloptions \n")
                            .append(", CASE WHEN c.reltoastrelid = 0 THEN false ELSE true END AS hastoasttable\n")
                            .append(", CASE WHEN c.relkind='m' THEN true ELSE false END AS materialized\n")
                            .append(", c.relispopulated AS withdata\n")
                            .append(", spc.spcname AS spcname\n");
                }
                sql.append(" FROM pg_class c")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objoid=c.oid and des.objsubid=0)")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")
                        .append(" LEFT OUTER JOIN pg_tablespace spc on spc.oid=c.reltablespace");
                if (isVersionHigherThan(dbsys, version, 9, 3))
                {
                    sql.append("  LEFT OUTER JOIN pg_class tst ON tst.oid = c.reltoastrelid\n");
                }
                sql.append(" WHERE ((c.relhasrules AND (EXISTS (SELECT r.rulename FROM pg_rewrite r ")
                        .append(" WHERE ((r.ev_class = c.oid) AND (bpchar(r.ev_type) = '1'::bpchar)) ))) ")
                        .append(" OR ( c.relkind IN ('v','m')))");//.append(" OR (c.relkind = 'v'::char))")
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //ViewInfoDTO view = (ViewInfoDTO) objInfo;
                    sql.append("AND c.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND c.relnamespace=").append(helperInfo.getSchemaOid())
                        .append(" AND c.relname = '").append(name).append("'");
                }
                sql.append(" ORDER BY relname;");
                break;
            //for table
            case PARTITION:
                /*
                /for inherit sun table
                sql.append("SELECT c.relname, c.oid, pg_get_userbyid(c.relowner) AS owner")
                        .append(" ,des.description AS comment, c.relnamespace, i.inhparent, ta.spcname")
                        .append(" FROM pg_catalog.pg_class c")
                        .append(" LEFT OUTER JOIN pg_catalog.pg_inherits i ON (i.inhrelid=c.oid) ")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objoid=c.oid AND des.objsubid=0) ")
                        .append(" LEFT OUTER JOIN pg_tablespace ta ON (ta.oid=c.reltablespace) ")
                        .append(" WHERE c.oid= ").append(objInfo.getOid());
                */
                /*
                
SELECT rel.oid, rel.relname AS relname, pg_get_userbyid(rel.relowner) AS owner, nsp.nspname AS schema,
    (SELECT count(*) FROM pg_trigger WHERE tgrelid=rel.oid AND tgisinternal = FALSE) AS triggercount,
    (SELECT count(*) FROM pg_trigger WHERE tgrelid=rel.oid AND tgisinternal = FALSE AND tgenabled = 'O') AS has_enable_triggers,
    pg_get_expr(rel.relpartbound, rel.oid) AS partition_value,
    (CASE WHEN rel.relkind = 'p' THEN true ELSE false END) AS is_partitioned,
    (CASE WHEN rel.relkind = 'p' THEN pg_get_partkeydef(rel.oid::oid) ELSE '' END) AS partition_scheme
    ,des.description AS comment
FROM (SELECT * FROM pg_inherits WHERE inhrelid = 143282::oid) inh
    LEFT JOIN pg_class rel ON inh.inhrelid = rel.oid
    LEFT JOIN pg_description des ON (des.objoid = inh.inhrelid  AND des.objsubid=0)
    LEFT JOIN pg_namespace nsp ON rel.relnamespace = nsp.oid
    WHERE rel.relispartition ORDER BY rel.relname;
                */
                sql.append("SELECT rel.oid, rel.relname AS relname, nsp.nspname AS schema,pg_get_userbyid(rel.relowner) AS owner,")
                        .append("pg_get_expr(rel.relpartbound, rel.oid) AS partition_value,")
                        .append("rel.relispartition ,")
                        .append("(CASE WHEN rel.relkind = 'p' THEN pg_get_partkeydef(rel.oid::oid) ELSE '' END) AS partition_scheme")
                        .append(",des.description AS comment")
                        .append(" FROM (SELECT * FROM pg_inherits WHERE inhrelid = ").append(objInfo.getOid()).append(") inh")
                        .append(" LEFT JOIN pg_class rel ON inh.inhrelid = rel.oid")
                        .append(" LEFT JOIN pg_description des ON (des.objoid = inh.inhrelid  AND des.objsubid=0)")
                        .append(" LEFT JOIN pg_namespace nsp ON rel.relnamespace = nsp.oid")
                        .append(" WHERE rel.relispartition ORDER BY rel.relname;");
                break;
            case COLUMN:
                sql.append("SELECT pg_attribute.attname AS name, pg_attribute.attnum AS position,")
                        .append("pg_type.oid AS typeoid, pg_type.typname AS typename, format_type(pg_type.oid,pg_attribute.atttypmod) AS fulltypename, pg_type.typlen, ")
                        .append("pg_get_expr(pg_attrdef.adbin, pg_attrdef.adrelid) AS default,") //.append("pg_attrdef.adsrc AS default,")
                        .append("pg_type.typalign AS sequence,")//not sure
                        .append("pg_attribute.attstorage AS storage,")
                        .append("pg_attribute.attstattarget AS statistics,")
                        .append("pg_attribute.attacl AS acl,")//error in hg1.3(with DISTINCT), correct in pg9.1 and 9.3
                        .append("pg_description.description AS comment,")
                        .append("pg_attribute.attnotnull AS notnull, ")
                        .append("CASE WHEN(pg_constraint.contype = 'p') THEN true ELSE false END AS ispk,")
                        //.append("CASE WHEN(pg_constraint.contype = 'u') THEN true ELSE false END AS isunique,")
                        .append("CASE WHEN(pg_constraint.contype = 'f') THEN true ELSE false END AS isfk,")
                        .append("CASE WHEN(pg_attribute.attislocal=true) THEN false ELSE true END AS isinherited")
                        .append(", attoptions");
                boolean higherthan90 = TreeController.getInstance().isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0);
                if (higherthan90)
                {
                    sql.append(", CASE WHEN (nspc.nspname IS NULL OR coll.collname IS NULL) THEN NULL")
                            .append(" ELSE quote_ident(nspc.nspname)||'.'|| quote_ident(coll.collname)  END AS collation");
                }
                sql.append(" FROM pg_attribute ")
                        .append(" INNER JOIN pg_class ON (pg_class.oid = pg_attribute.attrelid ")
                        //.append(" AND pg_class.relkind = 'r')")
                        .append(" AND pg_class.relkind IN('r','p'))")
                        .append(" INNER JOIN pg_type ON (pg_type.oid = pg_attribute.atttypid ")
                        .append(")") //.append(" AND pg_type.typname NOT IN ('oid', 'tid', 'xid', 'cid'))")                        
                        .append(" LEFT JOIN pg_attrdef ON (pg_attrdef.adrelid = pg_attribute.attrelid ")
                        .append(" AND pg_attrdef.adnum = pg_attribute.attnum) ")
                        .append(" LEFT JOIN pg_constraint ON (  pg_constraint.conrelid = pg_attribute.attrelid")
                        .append(" AND pg_constraint.contype  NOT IN('c','u','t','x')")
                        .append(" AND ( pg_constraint.conkey[1] = pg_attribute.attnum ")
                        .append(" OR pg_constraint.conkey[2] = pg_attribute.attnum ")
                        .append(" OR pg_constraint.conkey[3] = pg_attribute.attnum ")
                        .append(" OR pg_constraint.conkey[4] = pg_attribute.attnum ")
                        .append(" OR pg_constraint.conkey[5] = pg_attribute.attnum ")
                        .append(" OR pg_constraint.conkey[6] = pg_attribute.attnum )")
                        .append(" OR pg_constraint.conkey[7] = pg_attribute.attnum ")
                        .append(" OR pg_constraint.conkey[8] = pg_attribute.attnum  )")
                        .append(" LEFT OUTER JOIN pg_description ON pg_description.objoid = pg_attribute.attrelid")
                        .append(" AND pg_description.objsubid = pg_attribute.attnum");//.append(" AND pg_description.objsubid = pg_attrdef.oid");
                //.append(" LEFT OUTER JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid")
                if (higherthan90)
                {
                    sql.append(" LEFT OUTER JOIN pg_collation coll ON pg_attribute.attcollation=coll.oid")
                            .append(" LEFT OUTER JOIN pg_namespace nspc ON coll.collnamespace=nspc.oid");
                }
                sql.append(" WHERE pg_attribute.attisdropped = false AND pg_attribute.attnum>0 ")
                        .append(" AND pg_attribute.attname NOT IN('tableoid')")
                        .append(" AND pg_attribute.attrelid = ").append(helperInfo.getRelationOid());
                if (objInfo.getOid() != null && objInfo.getOid()!=0)
                {
                    //ColumnInfoDTO column = (ColumnInfoDTO) objInfo;
                    sql.append(" AND pg_attribute.attnum=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND pg_attribute.attname='").append(name).append("'");
                }
                sql.append(" ORDER BY pg_attribute.attnum;");
                break;
            case INDEX://because index branch never includ constraint,so omit primary,exclution constraint
                sql.append("SELECT DISTINCT ON(cls.relname) pg_get_indexdef(indexrelid), indexrelid AS idxoid, cls.relname as idxname")
                        .append(",indnatts AS colcount")
                        //.append(",n.nspname, indrelid AS taboid, tab.relname AS tabname")                        
                        //.append(",indkey AS colpositionarray, indclass AS operatorClassOidArray")
                        //.append(",indcollation AS collationOidArray")//support version>=9.1
                        .append(",ta.spcname AS tablespace")
                        .append(",indisclustered, indisvalid, indisunique, indisprimary,amname AS accessmethod,")                        
                        .append("pg_get_expr(indpred, indrelid) AS indconstraint,")                        
                        .append("substring(array_to_string(cls.reloptions, ',') from 'fillfactor=([0-9]*)') AS fillfactor")
                        //.append(",false AS indisexclusion")//hg1.3 not have:indisexclusion
                        //.append("con.oid AS conoid,contype, condeferrable, condeferred,")
                        //.append(",CASE contype WHEN 'p' THEN desp.description WHEN 'u' THEN desp.description ELSE des.description END AS description")
                        .append(",des.description AS description")
                        .append(" FROM pg_index idx")
                        .append(" JOIN pg_class cls ON cls.oid=indexrelid")
                        .append(" JOIN pg_class tab ON tab.oid=indrelid")
                        .append(" LEFT OUTER JOIN pg_tablespace ta on ta.oid=cls.reltablespace")
                        .append(" JOIN pg_namespace n ON n.oid=tab.relnamespace")
                        .append(" JOIN pg_am am ON am.oid=cls.relam")
                        //.append(" LEFT JOIN pg_depend dep ON (dep.classid = cls.tableoid AND dep.objid = cls.oid ")
                        //.append(" AND dep.refobjsubid = '0' AND dep.refclassid=(SELECT oid FROM pg_class WHERE relname='pg_constraint'))")
                        //.append(" LEFT OUTER JOIN pg_constraint con ON (con.tableoid = dep.refclassid AND con.oid = dep.refobjid)")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid=cls.oid")
                        //.append(" LEFT OUTER JOIN pg_description desp ON (desp.objoid=con.oid AND desp.objsubid = 0)")
                        .append(" WHERE indrelid = ").append(helperInfo.getRelationOid());
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //IndexInfoDTO index = (IndexInfoDTO) objInfo;
                    sql.append(" AND indexrelid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND cls.relname='").append(name).append("'");
                }
                //.append(" AND con.oid IS NULL")
                sql.append(" ORDER BY cls.relname");
                break;
            case PRIMARY_KEY:
            case UNIQUE:
            case FOREIGN_KEY:            
            case CHECK:
            case EXCLUDE:
                sql.append("SELECT DISTINCT con.contype,")
                        .append(" con.conname, con.oid, des.description,")
                        .append(" con.conindid , con.conkey,")
                        .append(" tsp.spcname AS tablespace, ");
                if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
                {
                    sql.append("con.convalidated,");
                }
                sql.append(" am.amname,")
                        //.append(" CASE WHEN array_length(c3.reloptions,1)>0 THEN c3.reloptions[1] ELSE null END AS fillfactor,")
                        .append("substring(array_to_string(c3.reloptions, ',') from 'fillfactor=([0-9]*)') AS fillfactor ,")
                        .append(" quote_ident(c2.relname) AS reftable, c2.oid AS reftableoid, con.confkey ,")
                        .append(" con.confmatchtype AS matchtype, con.confupdtype AS updateaction,con.confdeltype AS deleteaction,")
                        .append(" con.condeferrable, con.condeferred,");
                        //.append(" con.consrc AS checkcondition,");
                if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 11, 9)) 
                {
                    sql.append(" pg_get_constraintdef(con.oid) AS checkcondition,");
                } else 
                {
                    sql.append(" con.consrc AS checkcondition,");
                }
                if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
                {
                    sql.append(" con.connoinherit AS noinherit,");
                }
                sql.append(" con.conislocal, con.coninhcount,")
                        .append(" pg_get_constraintdef(con.oid) AS partdefinition,")
                        .append(" con.conrelid AS tableoid")//just only for order by
                        .append(" , con.conenabled")//, con.conenabled
                        .append(" FROM pg_constraint con ")
                        .append(" LEFT OUTER JOIN pg_description des ON con.oid = des.objoid ")
                        .append(" LEFT OUTER JOIN pg_class c ON con.conrelid = c.oid")
                        .append(" LEFT OUTER JOIN pg_class c2 ON con.confrelid = c2.oid")
                        .append(" LEFT OUTER JOIN pg_namespace np ON con.connamespace = np.oid")
                        .append(" LEFT OUTER JOIN pg_class c3 ON con.conindid = c3.oid")
                        .append(" LEFT OUTER JOIN pg_tablespace  tsp ON c3.reltablespace=tsp.oid")
                        .append(" LEFT OUTER JOIN pg_am am ON c3.relam = am.oid")
                        .append(" WHERE np.nspname NOT IN('information_schema')")
                        .append(" AND con.contype NOT IN('t')")//t stands for trigger
                        .append(" AND con.conrelid = ").append(helperInfo.getRelationOid());
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                   // ConstraintInfoDTO constr = (ConstraintInfoDTO) objInfo;
                    sql.append(" AND con.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND con.conname ='").append(name).append("'");
                }
                sql.append(" ORDER BY con.conrelid, con.oid;");
                break;
            case TRIGGER:
                sql.append("SELECT t.tgname, t.oid,")
                        .append("CASE relkind WHEN 'r' THEN 'TABLE' ELSE 'VIEW' END AS parenttype,")
                        .append("CASE WHEN t.tgconstraint=0 THEN false ELSE true END AS isconstraint,")
                        .append("trim(substring(pg_get_triggerdef(t.oid), t.tgname||'.* ON'), t.tgname||'ON') AS firesevent, ")
                        .append("CASE WHEN position('FOR EACH ROW' in pg_get_triggerdef(t.oid))>0 THEN true ELSE false END AS isforeachrow , ")
                        .append("quote_ident(nsp.nspname) || '.' || quote_ident(p.proname) || trim(substring(pg_get_triggerdef(t.oid), p.proname || '(.*)'), '')  AS function,")
                        .append("trim(substring(pg_get_triggerdef(t.oid), 'WHEN (.*) EXECUTE PROCEDURE'), '()') AS whenclause,")
                        .append("CASE WHEN t.tgenabled='D' THEN false ELSE true END AS isenable,")
                        .append("d.description AS comment,")
                        .append("pg_get_triggerdef(t.oid) AS definition, t.xmin")
                        .append(",t.tgdeferrable, t.tginitdeferred ")
                        .append(" FROM pg_trigger t")
                        .append(" LEFT OUTER JOIN pg_proc p ON p.oid = t.tgfoid")
                        .append(" LEFT OUTER JOIN pg_namespace nsp ON p.pronamespace = nsp.oid ")
                        .append(" LEFT OUTER JOIN pg_description d ON d.objoid  = t.oid")
                        .append(" LEFT OUTER JOIN pg_class cl ON cl.oid=t.tgrelid")
                        .append(" LEFT OUTER JOIN pg_namespace na ON na.oid=relnamespace")
                        .append(" LEFT OUTER JOIN pg_language l ON l.oid=p.prolang")
                        .append(" WHERE NOT tgisinternal")
                        .append(" AND t.tgrelid=").append(helperInfo.getRelationOid());
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //TriggerInfoDTO trigger = (TriggerInfoDTO) objInfo;
                    sql.append(" AND t.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND t.tgname='").append(name).append("'");
                }
                sql.append(" ORDER BY t.tgname");
                break;
            case RULE:
                sql.append("SELECT rew.rulename,rew.oid,")// pg_get_userbyid(cls.relowner) AS owner,
                        .append("CASE WHEN relkind = 'r' THEN 'TABLE' ELSE 'VIEW' END AS parenttype,")
                        .append("CASE rew.ev_type WHEN '1' THEN 'SELECT' WHEN '2' THEN 'UPDATE' ")
                        .append("WHEN '3' THEN 'INSERT' WHEN  '4' THEN 'DELETE' END AS event,")
                        .append("rew.is_instead AS doinstead, substring(pg_get_ruledef(rew.oid)from 'DO (.*);')AS dostatement,")//pg9.3 not so support pg_get_ruledef
                        .append("substring(pg_get_ruledef(rew.oid)from 'WHERE (.*) DO') AS wherestatement,")
                        .append("CASE WHEN rew.ev_enabled='D' THEN false ELSE true END AS isenable,")
                        .append("des.description,")
                        .append("pg_get_ruledef(rew.oid) AS definesql")
                        .append(" FROM pg_rewrite rew ")
                        .append(" LEFT OUTER JOIN pg_description des ON rew.oid = des.objoid ")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid=rew.ev_class")
                        .append(" WHERE rew.ev_class = ").append(helperInfo.getRelationOid());
                if (objInfo.getOid()!=null && objInfo.getOid()!=0)
                {
                    //RuleInfoDTO rule = (RuleInfoDTO) objInfo;
                    sql.append(" AND rew.oid=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND rew.rulename='").append(name).append("'");
                }
                sql.append(" ORDER BY rew.rulename;");
                break;
            case VIEW_COLUMN:
                //pg_get_userbyid(cls.relowner) AS owner,cls.relname AS parenttab,
                sql.append("SELECT att.attname,att.attnum AS position, pg_type.typname AS dataType,")
                        .append("pg_get_expr(pg_attrdef.adbin, pg_attrdef.adrelid) AS default, att.attacl AS acl,")//pg_attrdef.adsrc AS default, 
                        .append("des.description AS comment")
                        .append(" FROM pg_attribute att")
                        .append(" LEFT OUTER JOIN pg_type ON (pg_type.oid = att.atttypid  AND pg_type.typname NOT IN ('oid', 'tid', 'xid', 'cid')) ")
                        .append(" LEFT OUTER JOIN pg_attrdef ON (pg_attrdef.adrelid = att.attrelid  AND pg_attrdef.adnum = att.attnum) ")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objsubid = att.attnum AND des.objoid = att.attrelid) ")
                        //LEFT OUTER JOIN pg_class cls ON cls.oid = att.attrelid 
                        .append(" WHERE att.attnum > 0 ")
                        .append(" AND att.attisdropped = false")
                        .append(" AND att.attrelid=").append(helperInfo.getRelationOid());
                if (objInfo.getOid() != null && objInfo.getOid()!=0)
                {
                    sql.append(" AND att.attnum=").append(objInfo.getOid());
                } else
                {
                    sql.append(" AND att.attname='").append(name).append("'");
                }
                sql.append(" ORDER BY att.attnum,att.attname;");
                break;
        }
        return sql.toString();
    }
    public Object getObjPropertyInfo(HelperInfoDTO helperInfo, AbstractObject objInfo) throws SQLException, ClassNotFoundException, Exception
    {        
        if (helperInfo == null)
        {
            logger.error("Error：helperInfo is null,do nothing,return null");
            return null;
        }
        if(objInfo == null)
        {
            logger.error("Error：objInfo is null,do nothing,return null");
            return null;
        }
        //logger.debug(helperInfo.getDbName() + "." + helperInfo.getSchema() + "." + objInfo.getName()+", oid="+objInfo.getOid());
        String sql = this.getObjPropertySQL(objInfo, helperInfo);
        //logger.debug(sql);
        if (sql != null && sql.isEmpty())
        {
            logger.warn("no sql for " + objInfo.getType() + ": " + objInfo.getName() + ", do nothing and return.");
            return null;
        }
        Object obj = null;        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String url = helperInfo.getPartURL();
        if (TreeEnum.TreeNode.DATABASE == objInfo.getType())
        {
            url = url + helperInfo.getMaintainDB();
        } else
        {
            url = url + helperInfo.getDbName();
        }
        logger.debug(url);
        try
        {            
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            if (!rs.next())
            {
                return null;
            }
            switch (objInfo.getType())
            {
                //server
                case DATABASE:
                    obj = getDBInfoDTO(rs, stmt, helperInfo);
                    break;
                case TABLESPACE:
                    obj = getTablespaceInfoDTO(rs, helperInfo);
                    break;
                case GROUPROLE:
                case LOGINROLE:
                    obj = getRoleInfoDTO(rs, stmt, helperInfo);
                    break;
                //db
                case CATALOG:
                    obj = getCatalogInfoDTO(rs, stmt, helperInfo);
                    break;
                case CAST:
                    obj = getCastInfoDTO(rs, helperInfo);
                    break;
                case LANGUAGE:
                    obj = getLanguageInfoDTO(rs, helperInfo);
                    break;
                case EVENT_TRIGGER:
                    obj = getEventTriggerInfoDTO(rs, stmt, helperInfo);
                    break;
                case SCHEMA:
                    obj = getSchemaInfoDTO(rs, stmt, helperInfo);
                    break;
                //schema
                case FUNCTION:
                case PROCEDURE:
                    obj = this.getFuncProcInfoDTO(objInfo.getType(),  rs, helperInfo, stmt);
                    break;
                case SEQUENCE:
                    obj = getSequenceInfoDTO(rs, stmt, helperInfo);
                    break;
                case VIEW:
                case MATERIALIZED_VIEW:
                    obj = getViewInfoDTO(rs, helperInfo);
                    break;
                case TABLE:
                    obj = getTableInfoDTO(rs, stmt, helperInfo);
                    break;
                //table or view
                case PARTITION:
                    obj = getPartitionInfoDTO(rs, helperInfo);
                    break;
                case COLUMN:
                    obj = getColumnInfoDTO(rs, helperInfo);
                    break;
                case VIEW_COLUMN:
                    obj = getViewColumnInfoDTO(rs, helperInfo);
                    break;
                case INDEX:
                    obj = getIndexInfoDTO(conn, rs, helperInfo);
                    break;
                case PRIMARY_KEY:
                case UNIQUE:
                    obj = getConstraintInfoDTO(conn, rs, helperInfo);
                    break;
                case FOREIGN_KEY:
                case CHECK:
                case EXCLUDE:
                    obj = getConstraintInfoDTO(conn, rs, helperInfo);
                    break;
                case RULE:
                    obj = getRuleInfoDTO(rs, helperInfo);
                    break;
                case TRIGGER:
                    obj = getTriggerInfoDTO(rs,  stmt, helperInfo);
                    break;
                default:
                    obj = null;
                    logger.error(objInfo.getType() + " is an exception type, do nothing and return.");
                    break;
            }
        }
        catch (SQLException ex)
        {
            logger.error("Failed Connect:url = " + url + ",user= " + helperInfo.getUser()+ ",pwd=" + helperInfo.getPwd());
            ex.printStackTrace(System.out);
            throw new SQLException(ex.getMessage());//throw connect error exception to tree View
        }
        finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        logger.debug("Return:" + obj.getClass());
        return obj;
    }
    //server   
    private TablespaceInfoDTO getTablespaceInfoDTO(ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        TablespaceInfoDTO tablespaceInfo = new TablespaceInfoDTO();
        tablespaceInfo.setHelperInfo(helperInfo);
        tablespaceInfo.setName(rs.getString("spcname"));
        tablespaceInfo.setOid(rs.getLong("oid"));
        tablespaceInfo.setOwner(rs.getString("spcowner"));
        tablespaceInfo.setLocation(rs.getString("spclocation"));
        tablespaceInfo.setAcl(rs.getString("spcacl"));
        tablespaceInfo.setComment(rs.getString("comment"));
        tablespaceInfo.setVariableList(this.getVariableListFromArray(rs.getArray("spcoptions")));
        
        if (isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            rs.getArray("labels");
            rs.getArray("providers");
        }
        return tablespaceInfo;
    }
    private List<VariableInfoDTO> getVariableListFromArray(Array varArray)
    {
        List<VariableInfoDTO> list = new ArrayList();
        if (varArray == null)
        {
            return list;
        }
        String str = varArray.toString();
        str = str.substring(1, str.length() - 1);
        logger.info(str);
        String[] array = str.split(",");
        for (String a : array)
        {
            String[] aa = a.split("=");
            VariableInfoDTO v = new VariableInfoDTO();
            v.setName(aa[0]);
            v.setValue(aa[1]);
            list.add(v);
        }
        return list;
    }
    private List<String> getMembersOf(ResultSet rs, Statement stmt, long roleOid) throws SQLException
    {
        List<String> owners = new ArrayList();       
        try
        {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT am.roleid AS ownerid, a2.rolname AS ownername, am.admin_option")
                    .append(" FROM pg_auth_members am")
                    .append(" LEFT OUTER JOIN pg_roles a2 on a2.oid = am.roleid") //pg_authid
                    .append(" WHERE am.member = ").append(roleOid).append(";");
            logger.info("sql=" + sql.toString());
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                if(rs.getBoolean(3))
                {
                    owners.add(rs.getString(2)+"(*)"); 
                }else
                {
                    owners.add(rs.getString(2));
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex.getMessage());//throw connect error exception to tree View
        }
        return owners;
    }   
    private RoleInfoDTO getRoleInfoDTO( ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException, Exception
    {
        RoleInfoDTO groupRoleInfo = new RoleInfoDTO();
        groupRoleInfo.setHelperInfo(helperInfo);
        
        groupRoleInfo.setName(rs.getString(1));
        groupRoleInfo.setOid(rs.getLong(2));
        groupRoleInfo.setEncrypted(true);
        groupRoleInfo.setPwd(rs.getString(3));
        groupRoleInfo.setAccountExpires(rs.getString(4));
        groupRoleInfo.setCanLogin(rs.getBoolean(5));
        groupRoleInfo.setSuperuser(rs.getBoolean(6));
        groupRoleInfo.setCanCreateDB(rs.getBoolean(7));
        groupRoleInfo.setCanCreateRoles(rs.getBoolean(8));
        //ygq v5 admin del start
        //groupRoleInfo.setCanModifyCatalog(rs.getBoolean(9));
        //ygq v5 admin del edn
        groupRoleInfo.setCanInherits(rs.getBoolean(10));
        groupRoleInfo.setConnectionLimit(rs.getString(11));
        groupRoleInfo.setComment(rs.getString(12));
        if (TreeController.getInstance().isVersionHigherThan(helperInfo.getDbSys(),
                helperInfo.getVersionNumber(), 9, 0))
        {
            groupRoleInfo.setCanInitStreamReplicationAndBackup(rs.getBoolean("rolreplication"));
        }
        groupRoleInfo.setMemeberOf(this.getMembersOf(rs, stmt, groupRoleInfo.getOid()));
        groupRoleInfo.setVariableList(this.getRoleValiableList(rs, stmt, groupRoleInfo.getOid()));
        return groupRoleInfo;
    }
    private List<VariableInfoDTO> getRoleValiableList(ResultSet rs, Statement stmt, long oid) throws SQLException
    {
        List<VariableInfoDTO> vlist = new ArrayList();
        try
        {
            StringBuilder sql = new StringBuilder();
            sql.append("WITH configs AS ")
                    .append("(SELECT quote_ident(datname) AS datname, unnest(setconfig) AS config FROM pg_db_role_setting s")
                    .append(" LEFT JOIN pg_database d ON d.oid=s.setdatabase WHERE s.setrole=").append(oid).append(")")
                    .append(" SELECT datname, split_part(config, '=', 1) AS variable, replace(config, split_part(config, '=', 1) || '=', '') AS value")
                    .append(" FROM configs");
            logger.info("sql=" + sql.toString());
            rs = stmt.executeQuery(sql.toString());
            VariableInfoDTO v;
            while (rs.next())
            {
                v= new VariableInfoDTO();
                v.setDb(rs.getString("datname"));
                v.setName(rs.getString("variable"));
                v.setValue(rs.getObject("value").toString());
                vlist.add(v);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex.getMessage());
        }
        return vlist;       
    }
    private DBInfoDTO getDBInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException
    {
        DBInfoDTO dbInfo = new DBInfoDTO();
        dbInfo.setName(rs.getString(1));
        
        HelperInfoDTO newHelper = new HelperInfoDTO();
        newHelper.setMaintainDB(helperInfo.getMaintainDB());
        newHelper.setDbSys(helperInfo.getDbSys());
        newHelper.setDatLastSysOid(helperInfo.getDatLastSysOid());
        newHelper.setVersionNumber(helperInfo.getVersionNumber());
        newHelper.setHost(helperInfo.getHost());
        newHelper.setPort(helperInfo.getPort());
        newHelper.setOnSSL(helperInfo.isOnSSL());
        newHelper.setUser(helperInfo.getUser());
        newHelper.setPwd(helperInfo.getPwd());
        newHelper.setDbName(dbInfo.getName());        
        dbInfo.setHelperInfo(newHelper);

        dbInfo.setDatLastSysOid(rs.getLong("datlastsysoid"));        
        dbInfo.setOid(rs.getLong(2));
        dbInfo.setOwner(rs.getString(3));
        dbInfo.setAcl(rs.getString(4));
        dbInfo.setTablespace(rs.getString(5));
        newHelper.setDbTablespace(dbInfo.getTablespace());
        String defaultTbsp = rs.getString(6);
        if (defaultTbsp == null || defaultTbsp.isEmpty() || defaultTbsp.equals("unset"))
        {
            dbInfo.setDefaultTablespace(dbInfo.getTablespace());
        } else
        {
            dbInfo.setDefaultTablespace(defaultTbsp);
        }
        dbInfo.setEncoding(rs.getString(7));
        dbInfo.setCollation(rs.getString(8));
        dbInfo.setCharacterType(rs.getString(9));
        dbInfo.setDefaultSchema(rs.getString(10));        
        dbInfo.setConnectionLimit(rs.getString(11));
        dbInfo.setConnected(true);
        dbInfo.setComment(rs.getString(12));
        dbInfo.setAllowConnection(rs.getBoolean(13));
        //variables
        dbInfo.setVariableList(this.getDBValiableList(rs, stmt, dbInfo.getOid()));
        //security label for database from version 9.2, for other object from version 9.1
//        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
//        {
//            sql.append(" ,\n(SELECT array_agg(label) FROM pg_shseclabel sl1 WHERE sl1.objoid=db.oid) AS labels")
//                    .append(" ,\n(SELECT array_agg(provider) FROM pg_shseclabel sl2 WHERE sl2.objoid=db.oid) AS providers");
//        }     
        //acl
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 8, 4))
        {
            try
            {
                String defaultSql = "SELECT defaclacl, defaclobjtype FROM pg_catalog.pg_default_acl dacl"
                        +" WHERE dacl.defaclnamespace = 0::OID AND defaclobjtype IN('r', 'S', 'f','T')";
                logger.info(defaultSql);
                rs = stmt.executeQuery(defaultSql);
                while (rs.next())
                {
                    switch (rs.getString("defaclobjtype"))
                    {
                        case "r":
                            dbInfo.setDefaultTableAcl(rs.getString("defaclacl"));
                            break;
                        case "S":
                            dbInfo.setDefaultSequenceAcl(rs.getString("defaclacl"));
                            break;
                        case "f":
                            dbInfo.setDefaultFunctionAcl(rs.getString("defaclacl"));
                            break;
                        case "T"://version>=9.2
                            dbInfo.setDefaultTypeAcl(rs.getString("defaclacl"));
                            break;
                    }
                }
            } catch (SQLException ex)
            {
                logger.error(ex.getMessage());
                ex.printStackTrace();
                throw new SQLException(ex);
            }
        }
        return dbInfo;
    }
    private List<VariableInfoDTO> getDBValiableList(ResultSet rs, Statement stmt, long oid) throws SQLException
    {
        List<VariableInfoDTO> vlist = new ArrayList();
        try
        {
            StringBuilder sql = new StringBuilder();
            sql.append("WITH configs AS(SELECT quote_ident(rolname) AS rolname, unnest(setconfig) AS config FROM pg_db_role_setting s ")
                    .append(" LEFT JOIN pg_roles r ON r.oid=s.setrole WHERE s.setdatabase=").append(oid).append(")")
                    .append(" SELECT rolname, split_part(config, '=', 1) AS variable, ")
                    .append(" replace(config, split_part(config, '=', 1) || '=', '') AS value")
                    .append(" FROM configs");
            logger.info("sql=" + sql.toString());
            //stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            VariableInfoDTO v;
            while (rs.next())
            {
                v= new VariableInfoDTO();
                v.setUser(rs.getString("rolname"));
                v.setName(rs.getString("variable"));
                v.setValue(rs.getObject("value").toString());
                vlist.add(v);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex.getMessage());
        }
        return vlist;       
    }
    //database
    private CastInfoDTO getCastInfoDTO(ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        CastInfoDTO castInfo = new CastInfoDTO();
        castInfo.setHelperInfo(helperInfo);//for issystem
        
        castInfo.setName(rs.getString(1));
        castInfo.setOid(rs.getLong(2));
        castInfo.setSourceType(rs.getString(3));
        castInfo.setTargetType(rs.getString(4));
        castInfo.setFunction(rs.getString(5));
        castInfo.setContext(rs.getString(6));
        castInfo.setComment(rs.getString(7));
        return castInfo;
    }
    private LanguageInfoDTO getLanguageInfoDTO(ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        LanguageInfoDTO languageInfo = new LanguageInfoDTO();
        languageInfo.setHelperInfo(helperInfo);//for issystem
        
        languageInfo.setName(rs.getString(1));
        languageInfo.setOid(rs.getLong(2));
        languageInfo.setOwner(rs.getString(3));
        languageInfo.setAcl(rs.getString(4));
        languageInfo.setTrusted(rs.getBoolean(5));
        languageInfo.setHandler(rs.getString(6));
        languageInfo.setValidator(rs.getString(7));
        languageInfo.setComment(rs.getString(8));
        return languageInfo;
    }
    private CatalogInfoDTO getCatalogInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException
    {
        CatalogInfoDTO catalogInfo = new CatalogInfoDTO();
        catalogInfo.setName(rs.getString(1));
        catalogInfo.setOid(rs.getLong(2));
        
        //catalogInfo.setHelperInfo(helperInfo);   
        HelperInfoDTO newHelper = new HelperInfoDTO();
        newHelper.setMaintainDB(helperInfo.getMaintainDB());
        newHelper.setDatLastSysOid(helperInfo.getDatLastSysOid());
        newHelper.setDbSys(helperInfo.getDbSys());
        newHelper.setVersionNumber(helperInfo.getVersionNumber());
        newHelper.setHost(helperInfo.getHost());
        newHelper.setPort(helperInfo.getPort());
        newHelper.setOnSSL(helperInfo.isOnSSL());
        newHelper.setUser(helperInfo.getUser());
        newHelper.setPwd(helperInfo.getPwd());
        newHelper.setDbName(helperInfo.getDbName());
        newHelper.setSchema(catalogInfo.getName());
        newHelper.setSchemaOid(catalogInfo.getOid()); 
        catalogInfo.setHelperInfo(newHelper);
        
        
        catalogInfo.setOwner(rs.getString(3));
        catalogInfo.setAcl(rs.getString(4));
        catalogInfo.setComment(rs.getString(5));        
        String defaultSql;
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 8, 4))
        {
            defaultSql = "SELECT defaclacl,defaclobjtype  FROM pg_catalog.pg_default_acl dacl WHERE dacl.defaclnamespace = "
                    + catalogInfo.getOid() + " AND defaclobjtype IN('r', 'S', 'f','T')";
            logger.info(defaultSql);
            rs = stmt.executeQuery(defaultSql);
            while (rs.next())
            {
                switch (rs.getString("defaclobjtype"))
                {
                    case "r":
                        catalogInfo.setDefaultTableAcl(rs.getString("defaclacl"));
                        break;
                    case "S":
                        catalogInfo.setDefaultSequenceAcl(rs.getString("defaclacl"));
                        break;
                    case "f":
                        catalogInfo.setDefaultFunctionAcl(rs.getString("defaclacl"));
                        break;
                    case "T"://version >=9.2
                        catalogInfo.setDefaultTypeAcl(rs.getString("defaclacl"));
                        break;
                }
            }
        }
        return catalogInfo;
    }
    private EventTriggerInfoDTO getEventTriggerInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException
    {
        EventTriggerInfoDTO info = new EventTriggerInfoDTO();
        info.setName(rs.getString("name"));
        info.setOid(rs.getLong("oid"));
        info.setOwner(rs.getString("eventowner"));
        info.setComment(rs.getString(5));
        // O = trigger fires in “origin” and “local” modes, 
        // D = trigger is disabled,
        // R = trigger fires in “replica” mode, 
        // A = trigger fires always.
        String enabled = null;
        switch(rs.getString("enabled"))
        {
            case "O":
                enabled = "ENABLE";
                break;
            case "D":
                enabled = "DISABLE";
                break;
            case "R":
                enabled = "REPLICA";
                break;
            case "A":
                enabled = "ALWAYS";
                break;
            
        }
        info.setEnableWhat(enabled);
        info.setFunc(rs.getString("eventfunname"));
        info.setEvent(rs.getString("eventname"));
        info.setWhenTag(rs.getString("when"));
        info.setComment(rs.getString("comment"));
        
        HelperInfoDTO newHelper = new HelperInfoDTO();
        newHelper.setMaintainDB(helperInfo.getMaintainDB());
        newHelper.setDatLastSysOid(helperInfo.getDatLastSysOid());
        newHelper.setDbSys(helperInfo.getDbSys());
        newHelper.setVersionNumber(helperInfo.getVersionNumber());
        newHelper.setHost(helperInfo.getHost());
        newHelper.setPort(helperInfo.getPort());
        newHelper.setOnSSL(helperInfo.isOnSSL());
        newHelper.setUser(helperInfo.getUser());
        newHelper.setPwd(helperInfo.getPwd());
        newHelper.setDbName(helperInfo.getDbName());
        newHelper.setDbTablespace(helperInfo.getDbTablespace());
        newHelper.setSchema(info.getName());
        newHelper.setSchemaOid(info.getOid());
        
        info.setHelperInfo(newHelper);
        
        return info;
    }
    
    private SchemaInfoDTO getSchemaInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException
    {
        SchemaInfoDTO schemaInfo = new SchemaInfoDTO();
        schemaInfo.setName(rs.getString(1));
        schemaInfo.setOid(rs.getLong(2));
        
        HelperInfoDTO newHelper = new HelperInfoDTO();
        newHelper.setMaintainDB(helperInfo.getMaintainDB());
        newHelper.setDatLastSysOid(helperInfo.getDatLastSysOid());
        newHelper.setDbSys(helperInfo.getDbSys());
        newHelper.setVersionNumber(helperInfo.getVersionNumber());
        newHelper.setHost(helperInfo.getHost());
        newHelper.setPort(helperInfo.getPort());
        newHelper.setOnSSL(helperInfo.isOnSSL());
        newHelper.setUser(helperInfo.getUser());
        newHelper.setPwd(helperInfo.getPwd());
        newHelper.setDbName(helperInfo.getDbName());
        newHelper.setDbTablespace(helperInfo.getDbTablespace());
        newHelper.setSchema(schemaInfo.getName());
        newHelper.setSchemaOid(schemaInfo.getOid());
        schemaInfo.setHelperInfo(newHelper);
        
        
        schemaInfo.setOwner(rs.getString(3));
        schemaInfo.setAcl(rs.getString(4));
        schemaInfo.setComment(rs.getString(5));

        String defaultSql;
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 8, 4))
        {
            defaultSql = "SELECT defaclacl,defaclobjtype FROM pg_catalog.pg_default_acl dacl WHERE dacl.defaclnamespace = "
                    + schemaInfo.getOid() + " AND defaclobjtype IN('r', 'S', 'f','T')";
            logger.info(defaultSql);
            rs = stmt.executeQuery(defaultSql);
            while (rs.next())
            {
                switch (rs.getString("defaclobjtype"))
                {
                    case "r":
                        schemaInfo.setDefaultTableAcl(rs.getString("defaclacl"));
                        break;
                    case "S":
                        schemaInfo.setDefaultSequenceAcl(rs.getString("defaclacl"));
                        break;
                    case "f":
                        schemaInfo.setDefaultFunctionAcl(rs.getString("defaclacl"));
                        break;
                    case "T"://version >=9.2
                        schemaInfo.setDefaultTypeAcl(rs.getString("defaclacl"));
                        break;
                }
            }
        }
        return schemaInfo;
    }
    //schema
    private FunctionInfoDTO getFuncProcInfoDTO(TreeEnum.TreeNode type, ResultSet rs, HelperInfoDTO helperInfo, Statement stmt) throws SQLException
    {
        FunctionInfoDTO fun = new FunctionInfoDTO();
        fun.setType(type);
        fun.setHelperInfo(helperInfo);
        
        fun.setSimpleName(rs.getString("proname"));
        fun.setOid(rs.getLong("oid"));
        fun.setOwner(rs.getString("proowner"));
        fun.setLanguage(rs.getString("lanname"));
        
        fun.setInArgCount(rs.getInt("pronargs"));
        fun.setInArgTypeOids(rs.getString("proargtypes"));
        
        fun.setReturnType(rs.getString("returntype"));        
        fun.setSrc(rs.getString("prosrc"));
        fun.setBin(rs.getString("probin"));
        fun.setCost(rs.getInt("procost"));
        fun.setVolatiles(rs.getString("volatile"));
        fun.setReturnSet(rs.getBoolean("proretset"));
        fun.setSecurityDefiner(rs.getBoolean("prosecdef"));
        fun.setStrict(rs.getBoolean("proisstrict"));
        fun.setWindow(rs.getBoolean("proiswindow"));
        fun.setLeafProof(rs.getBoolean("proleakproof"));
        String p = rs.getString("proparallel");
        switch (p)
        {
            case "s":
                fun.setParallel("SAFE");
                break;
            case "r":
                fun.setParallel("RESTRICTED");
                break;
            case "u":
                fun.setParallel("UNSAFE");
                break;
        } 
        fun.setAcl(rs.getString("proacl"));
        fun.setComment(rs.getString("description"));   
        fun.setPartDefine(rs.getString("pg_get_functiondef"));
        //for variable 
        fun.setVariableList(this.getVariableListFromArray(rs.getArray("proconfig")));
        //for type define
        logger.info("inArgCount=" + fun.getInArgCount());
        logger.info("inArgTypeOid = " + fun.getInArgTypeOids());        
        String allArgTypeOid = rs.getString("proallargtypes");
        logger.info("allArgTypeOid = " + allArgTypeOid);       
        String allArgMode = rs.getString("proargmodes");
        logger.info("allArgMode = " + allArgMode);        
        String allArgName = rs.getString("proargnames");
        logger.info("allArgName = " + allArgName);
        String allArgDefault = rs.getString("proargdefaultvals");//like defaultval1, defaultval2,...
        logger.info("allArgDefaults = " + allArgDefault);   
       
        Map types = this.getTypeMap(stmt);//when recreate statment, then the rs will be closed.
        //Long[] argTypeOidArray = (Long[]) rs.getArray("proargtypes").getArray();
        fun.setInArgTypes(this.getFuncInArgStr(fun.getInArgTypeOids(), types));
        
        //when all args is in args, proallargtypes return null
        String[] allTypeOidArray = null;
        if (allArgTypeOid == null || allArgTypeOid.isEmpty()) 
        {
            if (fun.getInArgTypeOids() == null || fun.getInArgTypeOids().isEmpty()) 
            {
                fun.setArgList(new ArrayList());
                logger.debug("return");
                return fun;
            } else
            {     
                String[] inArgTypeOidArray = fun.getInArgTypeOids().split(" ");
                allTypeOidArray = inArgTypeOidArray;
            }
        } else
        {
            allTypeOidArray = allArgTypeOid.substring(1, allArgTypeOid.length() - 1).split(",");
        }        
        //when all arg mode is IN, proargmodes return null.
        String[] modes;
        if (allArgMode == null)
        {
            modes = null;
        } else
        {
            modes = allArgMode.substring(1, allArgMode.length()-1).split(",");
        }
        //when no arg has name, proargnames return null.
        String[] names;
        if (allArgName == null)
        {
            names =null;
        } else
        {
            names = allArgName.substring(1, allArgName.length()-1).split(",");
        }
        //when no arg has default, proargdefaults return null.
        String[] defaults;
        if(allArgDefault ==null)
        {
            defaults =null;
        }else
        {
            defaults = allArgDefault.split(",");
        }      
        
        List<FunctionArgDTO>  argList = new ArrayList();
        FunctionArgDTO arg;
        int inArgCount = 0;
        for (int i = 0; i < allTypeOidArray.length; i++)
        {
            arg = new FunctionArgDTO();
            //arg mode
            if (modes != null && modes.length > i && modes[i] != null)
            {
                String mode = this.getModeByKey(modes[i]);               
                if (!mode.equals("TABLE"))
                {
                     arg.setMode(mode);
                }
            }
            //arg name
            if (names != null && names.length > i && names[i] != null && !names[i].isEmpty() && !names[i].equals("\"\""))
            {
                arg.setName(names[i]);
            }
            //type
            arg.setType(types.get(allTypeOidArray[i]).toString());
            //default - only in arg has default value
            if (fun.getInArgCount() > 0
                    && (modes == null || (i < modes.length && (modes[i] == null || modes[i].equals("i"))))
                    && defaults != null)
            {
                inArgCount++;
                logger.info("=====================in arg num="+inArgCount);
                int defaultNum = inArgCount - fun.getInArgCount() + defaults.length - 1;
                if (inArgCount > (fun.getInArgCount() - defaults.length) && inArgCount <= fun.getInArgCount()
                        && defaults[defaultNum] != null && !defaults[defaultNum].isEmpty() && !defaults[defaultNum].equals("\"\""))
                {
                    arg.setDefaultValue(defaults[defaultNum]);
                }
            }            
            argList.add(arg);
        }
        fun.setArgList(argList);
        return fun;
    }
    private String getModeByKey(String key)
    {
        switch (key)
        {
            case "i":
                return "IN";
            case "o":
                return "OUT";
            case "b":
                return "INOUT";
            case "v":
                return "VARIADIC";
            case "t":
                return "TABLE";
            default:
                logger.error(key+" is an exception mode key, do nothing and return null");
                return null;
        }
    }
    private ViewInfoDTO getViewInfoDTO(ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        ViewInfoDTO viewInfo = new ViewInfoDTO();
        viewInfo.setOid(rs.getLong("oid"));
        viewInfo.setName(rs.getString("relname"));
        
        HelperInfoDTO newHelper = new HelperInfoDTO();
        newHelper.setMaintainDB(helperInfo.getMaintainDB());
        newHelper.setDatLastSysOid(helperInfo.getDatLastSysOid());
        newHelper.setDbSys(helperInfo.getDbSys());
        newHelper.setVersionNumber(helperInfo.getVersionNumber());
        newHelper.setHost(helperInfo.getHost());
        newHelper.setPort(helperInfo.getPort());
        newHelper.setOnSSL(helperInfo.isOnSSL());
        newHelper.setUser(helperInfo.getUser());
        newHelper.setPwd(helperInfo.getPwd());
        newHelper.setDbName(helperInfo.getDbName());
        newHelper.setDbTablespace(helperInfo.getDbTablespace());
        newHelper.setSchema(helperInfo.getSchema());
        newHelper.setSchemaOid(helperInfo.getSchemaOid());
        newHelper.setType(TreeEnum.TreeNode.VIEW);
        newHelper.setRelation(viewInfo.getName());
        newHelper.setRelationOid(viewInfo.getOid());
        viewInfo.setHelperInfo(newHelper);      
              
        viewInfo.setOwner(rs.getString("viewowner"));
        viewInfo.setAcl(rs.getString("relacl"));
        viewInfo.setDefinition(rs.getString("definition"));
        viewInfo.setComment(rs.getString("comment"));        
        TreeEnum.DBSYS dbSys = helperInfo.getDbSys();
        String version = helperInfo.getVersionNumber();
        if (isVersionHigherThan(dbSys, version, 9, 1))
        {
            String securityBarrier = rs.getString("security_barrier");
            logger.info("securityBarrier=" + securityBarrier);
            if (securityBarrier != null && securityBarrier.equals("true"))
            {
                viewInfo.setSecurityBarrier(true);
            } else
            {
                viewInfo.setSecurityBarrier(false);
            }
        }
        if (isVersionHigherThan(dbSys, version, 9, 2))
        {
            viewInfo.setMaterializedView(rs.getBoolean("materialized"));
            if (viewInfo.isMaterializedView())
            {
                viewInfo.setTablespace(rs.getString("spcname"));
                if (viewInfo.getTablespace() == null || viewInfo.getTablespace().isEmpty())
                {
                    viewInfo.setTablespace(newHelper.getDbTablespace());
                }
                viewInfo.setFillFactor(rs.getString("fillfactor"));
                viewInfo.setWithData(rs.getBoolean("withdata"));

                String autovacuumEnable = rs.getString("autovacuum_enable");//true,false,null
                AutoVacuumDTO vacuumInfo = new AutoVacuumDTO();
                if (autovacuumEnable == null || autovacuumEnable.isEmpty())
                {
                    vacuumInfo.setCustomAutoVacuum(false);
                } else
                {
                    vacuumInfo.setCustomAutoVacuum(true);
                    if (autovacuumEnable.toLowerCase().equals("true"))
                    {
                        vacuumInfo.setEnable(true);
                        vacuumInfo.setVacuumBaseThreshold(rs.getString("autovacuum_vacuum_threshold"));
                        vacuumInfo.setVacuumScaleFactor(rs.getString("autovacuum_vacuum_scale_factor"));
                        vacuumInfo.setAnalyzeBaseThreshold(rs.getString("autovacuum_analyze_threshold"));
                        vacuumInfo.setAnalyzeScaleFactor(rs.getString("autovacuum_analyze_scale_factor"));
                        vacuumInfo.setVacuumCostDelay(rs.getString("autovacuum_vacuum_cost_delay"));
                        vacuumInfo.setVacuumCostLimit(rs.getString("autovacuum_vacuum_cost_limit"));
                        vacuumInfo.setFreezeMinimumAge(rs.getString("autovacuum_freeze_min_age"));
                        vacuumInfo.setFreezeMaximumAge(rs.getString("autovacuum_freeze_max_age"));
                    } else
                    {
                        vacuumInfo.setEnable(false);
                    }
                }
                String toastEnable = rs.getString("toast_autovacuum_enabled");
                if (toastEnable == null || toastEnable.isEmpty())
                {
                    vacuumInfo.setCustomAutoVacuumToast(false);
                } else
                {
                    vacuumInfo.setCustomAutoVacuumToast(true);
                    if (toastEnable.toLowerCase().equals("true"))
                    {
                        vacuumInfo.setEnableToast(true);
                        vacuumInfo.setVacuumBaseThresholdToast(rs.getString("toast_autovacuum_vacuum_threshold"));
                        vacuumInfo.setVacuumScaleFactorToast(rs.getString("toast_autovacuum_vacuum_scale_factor"));
                        vacuumInfo.setVacuumCostDelayToast(rs.getString("toast_autovacuum_vacuum_cost_delay"));
                        vacuumInfo.setVacuumCostLimitToast(rs.getString("toast_autovacuum_vacuum_cost_limit"));
                        vacuumInfo.setFreezeMinimumAgeToast(rs.getString("toast_autovacuum_freeze_min_age"));
                        vacuumInfo.setFreezeMaximumAgeToast(rs.getString("toast_autovacuum_freeze_max_age"));
                        vacuumInfo.setFreezeTableAgeToast(rs.getString("toast_autovacuum_freeze_table_age"));
                    } else
                    {
                        vacuumInfo.setEnableToast(false);
                    }
                }
                viewInfo.setAutoVacuumInfo(vacuumInfo);
            }
        }
        return viewInfo;
    }
    private SequenceInfoDTO getSequenceInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException
    {
        SequenceInfoDTO sequenceInfo = new SequenceInfoDTO();
        sequenceInfo.setHelperInfo(helperInfo);
        sequenceInfo.setName(rs.getString(1));
        sequenceInfo.setOid(rs.getLong(2));
        sequenceInfo.setOwner(rs.getString(3));
        sequenceInfo.setAcl(rs.getString(4));
        sequenceInfo.setComment(rs.getString(5));        
        this.getMoreSeqProperty(rs, stmt, helperInfo, sequenceInfo);
        return sequenceInfo;
    }
    private SequenceInfoDTO getMoreSeqProperty(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo, SequenceInfoDTO sequenceInfo) throws SQLException
    {
        logger.info("Enter getSchemaGroupInfo");
        if (helperInfo == null)
        {
            logger.error("helperInfo is null,do nothing,return null");
            return null;
        }
        else if (helperInfo.getDbName() == null)
        {
            logger.error("dbName is null,do nothing,return null");
            return null;
        }
        else if (helperInfo.getSchema() == null)
        {
            logger.info("(schema is null,do nothing,return null");
            return null;
        }

        SyntaxController sc = SyntaxController.getInstance();
        //String scm = sc.getName(helperInfo.getSchema());
        //String seq = sc.getName(sequenceInfo.getName());      
        String scm =  sc.getName(helperInfo.getSchema());
        String seq = sc.getName(sequenceInfo.getName());
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT s.last_value, start_value, min_value, max_value, ")
                .append("increment_by, cache_size, cycle, s.is_called")
                .append(" FROM pg_sequences, ").append(scm).append(".").append(seq).append(" s ")
                .append(" WHERE schemaname='").append(scm).append("'")
                .append(" AND sequencename='").append(seq).append("' ;");
        try
        {
            logger.debug(sql.toString());
            rs = stmt.executeQuery(sql.toString());
            if (rs.next())//only one line
            {
                sequenceInfo.setStrCurrent(rs.getString(1));//current value is the last value
                sequenceInfo.setStrStart(rs.getString(2));
                sequenceInfo.setStrMinimum(rs.getString(3));
                sequenceInfo.setStrMaximum(rs.getString(4));
                sequenceInfo.setStrIncrement(rs.getString(5));
                sequenceInfo.setStrCache(rs.getString(6));
                sequenceInfo.setCycled(rs.getBoolean(7));
                sequenceInfo.setCalled(rs.getBoolean(8));
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        }      
        logger.info("Return");
        return sequenceInfo;
    }
    private TableInfoDTO getTableInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException, Exception
    {
        TableInfoDTO tableInfo = new TableInfoDTO();
        tableInfo.setName(rs.getString(1));
        tableInfo.setOid(rs.getLong(2));
        
        HelperInfoDTO newHelper = new HelperInfoDTO();
        newHelper.setMaintainDB(helperInfo.getMaintainDB());
        newHelper.setDatLastSysOid(helperInfo.getDatLastSysOid());
        newHelper.setDbSys(helperInfo.getDbSys());
        newHelper.setVersionNumber(helperInfo.getVersionNumber());
        newHelper.setHost(helperInfo.getHost());
        newHelper.setPort(helperInfo.getPort());
        newHelper.setOnSSL(helperInfo.isOnSSL());
        newHelper.setUser(helperInfo.getUser());
        newHelper.setPwd(helperInfo.getPwd());
        newHelper.setDbName(helperInfo.getDbName());
        newHelper.setDbTablespace(helperInfo.getDbTablespace());
        newHelper.setSchema(helperInfo.getSchema());
        newHelper.setSchemaOid(helperInfo.getSchemaOid());
        newHelper.setType(TreeEnum.TreeNode.TABLE);
        newHelper.setRelation(tableInfo.getName());
        newHelper.setRelationOid(tableInfo.getOid());
        tableInfo.setHelperInfo(newHelper);

        tableInfo.setOwner(rs.getString(3));
        if (rs.getString(4) == null)
        {
            tableInfo.setTablespace(newHelper.getDbTablespace());
        } else
        {
            tableInfo.setTablespace(rs.getString(4));
        }
        tableInfo.setAcl(rs.getString(5));
        tableInfo.setOfType(rs.getString(6));
        tableInfo.setPrimaryKey(rs.getString(7));
        tableInfo.setRowsEstimated(rs.getInt(8));
        tableInfo.setFillFactor(rs.getString(9));        
        tableInfo.setInheritedByTables(rs.getBoolean("relhassubclass"));
        tableInfo.setKind(rs.getString("relkind").charAt(0));        
        tableInfo.setPartitionSchema(rs.getString("partitionSchema"));   
        logger.debug("-------" + tableInfo.getPartitionSchema() + "-------");
        tableInfo.setUnlogged(rs.getBoolean(13));//unrealized
        tableInfo.setHasOID(rs.getBoolean(14));
        tableInfo.setHasToastTable(rs.getBoolean(15));        
        tableInfo.setComment(rs.getString(16));
        tableInfo.setReplication(rs.getBoolean(17));
        String enable = rs.getString(18);//true,false,null
        if (enable == null || enable.isEmpty())
        {
            tableInfo.setAutoVacuumInfo(null);
        } else
        {
            AutoVacuumDTO vacuumInfo = new AutoVacuumDTO();
            if (enable.toLowerCase().equals("true"))
            {
                vacuumInfo.setEnable(true);
                vacuumInfo.setVacuumBaseThreshold(rs.getString(17));
                vacuumInfo.setVacuumScaleFactor(rs.getString(18));
                vacuumInfo.setAnalyzeBaseThreshold(rs.getString(19));
                vacuumInfo.setAnalyzeScaleFactor(rs.getString(20));
                vacuumInfo.setVacuumCostDelay(rs.getString(21));
                vacuumInfo.setVacuumCostLimit(rs.getString(22));
                vacuumInfo.setFreezeMinimumAge(rs.getString(23));
                vacuumInfo.setFreezeMaximumAge(rs.getString(24));
                String toastEnable = rs.getString(25);
                if (toastEnable != null && !toastEnable.isEmpty())
                {
                    if (toastEnable.toLowerCase().equals("true"))
                    {
                        vacuumInfo.setEnableToast(true);
                        vacuumInfo.setVacuumBaseThresholdToast(rs.getString(26));
                        vacuumInfo.setVacuumScaleFactorToast(rs.getString(27));
                        vacuumInfo.setFreezeMinimumAgeToast(rs.getString(28));
                        vacuumInfo.setFreezeMaximumAgeToast(rs.getString(19));
                        vacuumInfo.setFreezeTableAgeToast(rs.getString(30));
                    } else
                    {
                        vacuumInfo.setEnableToast(false);
                    }
                }
            } else
            {
                vacuumInfo.setEnable(false);
            }
            tableInfo.setAutoVacuumInfo(vacuumInfo);
        }        
        tableInfo.setInheritTableList(this.getInheritTableList(rs, stmt, tableInfo.getOid()));//inherit and partition
        tableInfo.setPartitionList(this.getPartitionList4Table(rs, stmt, tableInfo.getOid(), newHelper));
        tableInfo.setColumnInfoList(this.getColumnList4Table(rs, stmt, tableInfo.getOid(), newHelper));
        tableInfo.setConstraintInfoList(this.getConstraintList4Table(tableInfo.getOid(),newHelper));
        return tableInfo;
    }
    private List<ObjItemInfoDTO> getInheritTableList(ResultSet rs, Statement stmt, Long tableOid) throws SQLException
    {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT inh.inhparent as oid ,quote_ident(cl.relname) as name , quote_ident(nsp.nspname) as schema")
                .append(" FROM pg_inherits inh")
                .append(" LEFT OUTER JOIN pg_class cl on cl.oid = inh.inhparent ")
                .append(" LEFT OUTER JOIN pg_namespace nsp ON nsp.oid=cl.relnamespace")
                .append(" WHERE inh.inhrelid=").append(tableOid)
                .append(" ORDER BY inh.inhseqno");
        logger.debug(sql.toString());
        List<ObjItemInfoDTO> inhTableList = new ArrayList();
        try
        {
            rs = stmt.executeQuery(sql.toString());
            ObjItemInfoDTO inhtable;
            while (rs.next())
            {
                inhtable = new ObjItemInfoDTO();
                inhtable.setOid(rs.getLong("oid"));
                inhtable.setTable(rs.getString("name"));
                inhtable.setSchema(rs.getString("schema"));
                inhTableList.add(inhtable);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } 
        logger.debug("Return: inherit table count:" + inhTableList.size());
        return inhTableList;
    }
    private List<ColumnInfoDTO> getColumnList4Table(ResultSet rs, Statement stmt, Long tableOid, HelperInfoDTO helperInfo) throws SQLException
    {
        logger.info("Enter:tableOid" + tableOid);
        List<ColumnInfoDTO> columnInfoList = new ArrayList();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(pg_attribute.attname) AS name, pg_attribute.attnum AS position,")
                .append("pg_type.oid AS typeoid, pg_type.typname AS typename, format_type(pg_type.oid, pg_attribute.atttypmod) AS fulltypename, pg_type.typlen, ")
                .append("pg_get_expr(pg_attrdef.adbin, pg_attrdef.adrelid) AS default,")//.append("pg_attrdef.adsrc AS default,")
                .append("pg_type.typalign AS sequence,")//not sure
                .append("pg_attribute.attstorage AS storage,")
                .append("pg_attribute.attstattarget AS statistics,")
                .append("pg_attribute.attacl AS acl,")// (with DISTINCT) error in hg1.3, correct in pg9.1 and 9.3
                .append("pg_description.description AS comment,")
                .append("pg_attribute.attnotnull AS notnull, ")
                .append("CASE WHEN(pg_constraint.contype = 'p') THEN true ELSE false END AS ispk,")
                .append("CASE WHEN(pg_constraint.contype = 'f') THEN true ELSE false END AS isfK,")
                .append("CASE WHEN(pg_attribute.attislocal=true) THEN false ELSE true END AS isinherited")
                .append(",attoptions");
        boolean higherthan90 = TreeController.getInstance().isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0);
        if (higherthan90)
        {
            sql.append(", CASE WHEN (nspc.nspname IS NULL OR coll.collname IS NULL) THEN NULL")
                    .append(" ELSE quote_ident(nspc.nspname)||'.'|| quote_ident(coll.collname)  END AS collation");
        }
        sql.append(" FROM pg_attribute ")
                .append(" INNER JOIN pg_class ON (pg_class.oid = pg_attribute.attrelid ")
                //.append(" AND pg_class.relkind = 'r')")
                .append(" AND pg_class.relkind IN('r','p'))")
                .append(" INNER JOIN pg_type ON (pg_type.oid = pg_attribute.atttypid ")
                .append(")")//.append(" AND pg_type.typname NOT IN ( 'oid','tid', 'xid', 'cid'))")
                .append(" LEFT JOIN pg_attrdef ON (pg_attrdef.adrelid = pg_attribute.attrelid ")
                .append(" AND pg_attrdef.adnum = pg_attribute.attnum) ")
                .append(" LEFT JOIN pg_constraint ON ( pg_constraint.conrelid = pg_attribute.attrelid")
                .append(" AND pg_constraint.contype  NOT IN('f','c','u','t','x')")
                .append(" AND ( pg_constraint.conkey[1] = pg_attribute.attnum ")
                .append(" OR pg_constraint.conkey[2] = pg_attribute.attnum ")
                .append(" OR pg_constraint.conkey[3] = pg_attribute.attnum ")
                .append(" OR pg_constraint.conkey[4] = pg_attribute.attnum ")
                .append(" OR pg_constraint.conkey[5] = pg_attribute.attnum ")
                .append(" OR pg_constraint.conkey[6] = pg_attribute.attnum )")
                .append(" OR pg_constraint.conkey[7] = pg_attribute.attnum ")
                .append(" OR pg_constraint.conkey[8] = pg_attribute.attnum  )")
                .append(" LEFT OUTER JOIN pg_description ON pg_description.objoid = pg_attribute.attrelid ")
                .append(" AND pg_description.objsubid = pg_attribute.attnum");//.append(" AND pg_description.objsubid = pg_attrdef.oid");
                //.append(" LEFT OUTER JOIN pg_inherits inh ON inh.inhrelid=pg_attribute.attrelid")//add
             if (higherthan90)
            {
                sql.append(" LEFT OUTER JOIN pg_collation coll ON pg_attribute.attcollation=coll.oid")
                        .append(" LEFT OUTER JOIN pg_namespace nspc ON coll.collnamespace=nspc.oid");
            }
             sql.append(" WHERE pg_attribute.attnum > 0  AND  pg_attribute.attisdropped = false")
                .append(" AND pg_attribute.attname NOT IN('tableoid')")
                .append(" AND pg_attribute.attrelid = ").append(tableOid)
                .append(" ORDER BY pg_attribute.attnum;");

        StringBuilder sql2 = new StringBuilder();
        sql2.append("SELECT inhparent, quote_ident(nsp.nspname)||'.'||quote_ident(c.relname) as inhtable, a.attname")
                .append(" FROM pg_inherits i")
                .append(" LEFT JOIN pg_attribute a ON (attrelid = inhparent AND attnum > 0)")
                .append(" LEFT OUTER JOIN pg_class c ON c.oid=i.inhparent")
                .append(" LEFT OUTER JOIN pg_namespace nsp ON nsp.oid = c.relnamespace")
                .append(" WHERE inhrelid=").append(tableOid)//.append(" AND  a.attname=?")
                .append(" ORDER BY inhseqno DESC");
        try
        {
            logger.info( sql2.toString());
            rs = stmt.executeQuery(sql2.toString());
            LinkedHashMap hm = new LinkedHashMap<>();
            while (rs.next())
            {
                hm.put(rs.getString("attname"), rs.getString("inhtable"));
            }
            logger.info(sql.toString());
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                ColumnInfoDTO col = new ColumnInfoDTO();
                col.setHelperInfo(helperInfo);
                col.setName(rs.getString("name"));
                col.setPosition(rs.getInt("position"));
                
                DatatypeDTO type = new DatatypeDTO();                
                type.setOid(rs.getLong("typeoid"));
                type.setFullname(rs.getString("fulltypename"));
                type.setShortname(rs.getString("typename"));
                //type.setHasArgs(rs.getInt("typlen") < 0);
                col.setDatatype(type);
                
                col.setDefaultValue(rs.getString("default"));
                col.setSequence(rs.getString("sequence"));
                col.setStorage(rs.getString("storage"));
                col.setStatistics(rs.getString("statistics"));
                col.setAcl(rs.getString("acl"));
                col.setComment(rs.getString("comment"));
                col.setNotNull(rs.getBoolean("notnull"));
                col.setPk(rs.getBoolean("ispk"));
                col.setFk(rs.getBoolean("isfk"));
                col.setInherit(rs.getBoolean("isinherited"));
                if (hm.get(col.getName()) != null)
                {
                    col.setInheritedTable(hm.get(col.getName()).toString());
                }
                col.setVariableInfoList(this.getVariableListFromArray(rs.getArray("attoptions")));
                if (higherthan90)
                {
                    col.setCollation(rs.getString("collation"));
                }
                columnInfoList.add(col);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } 
        logger.info("Return:size = " + columnInfoList.size());
        return columnInfoList;
    }
    private List<ConstraintInfoDTO> getConstraintList4Table(Long tableOid, HelperInfoDTO helperInfo) throws Exception
    {
        logger.info("tableOid = " + tableOid);
        List<ConstraintInfoDTO> constrInfoList = new ArrayList();
        if(helperInfo==null || tableOid==null || tableOid==0)
        {
            logger.error("Enter info is null, do nothing and return an empty list.");
            return constrInfoList;
        }        
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT con.contype,")
                .append(" quote_ident(con.conname) AS conname, con.oid, des.description,")
                .append(" con.conindid , con.conkey,")
                .append(" CASE WHEN c3.reltablespace=0  THEN 'pg_default' ELSE  tsp.spcname  END  AS tablespace,");
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
        {
            sql.append("con.convalidated,");
        } 
        sql.append(" am.amname,")
                //.append(" CASE WHEN array_length(c3.reloptions,1)>0 THEN c3.reloptions[1] ELSE null END AS fillfactor,")
                .append("substring(array_to_string(c3.reloptions, ',') from 'fillfactor=([0-9]*)') AS fillfactor ,")
                .append(" c2.relname AS reftable, c2.oid AS reftableoid, con.confkey,")
                .append(" con.confmatchtype AS matchtype, con.confupdtype AS updateaction, con.confdeltype AS deleteaction,")
                .append(" con.condeferrable, con.condeferred,");
                //.append(" con.consrc AS checkcondition,");
         if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 11, 9)) 
         {
            sql.append(" pg_get_constraintdef(con.oid) AS checkcondition,");
         } else 
         {
            sql.append(" con.consrc AS checkcondition,");
         }
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            sql.append(" con.connoinherit AS noinherit,");
        }
        sql.append(" con.conislocal, con.coninhcount,")
                .append(" pg_get_constraintdef(con.oid) AS partdefinition,")
                .append(" con.conrelid AS tableoid")//just only for order by
                .append(" , con.conenabled")//, con.conenabled
                .append(" FROM pg_constraint con ")
                .append(" LEFT OUTER JOIN pg_description des ON con.oid = des.objoid ")
                .append(" LEFT OUTER JOIN pg_class c ON con.conrelid = c.oid")
                .append(" LEFT OUTER JOIN pg_class c2 ON con.confrelid = c2.oid")
                .append(" LEFT OUTER JOIN pg_namespace np ON con.connamespace = np.oid")
                .append(" LEFT OUTER JOIN pg_class c3 ON con.conindid = c3.oid")
                .append(" LEFT OUTER JOIN pg_tablespace tsp ON c3.reltablespace=tsp.oid")
                .append(" LEFT OUTER JOIN pg_am am ON c3.relam = am.oid")
                .append(" WHERE np.nspname NOT IN('information_schema')")
                .append(" AND con.contype NOT IN('t')")//t stands for trigger
                .append(" AND con.conrelid = ").append(tableOid)
                .append(" ORDER BY con.conrelid, con.oid;");
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {          
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            logger.info(sql.toString());
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                constrInfoList.add(this.getConstraintInfoDTO(conn, rs, helperInfo));//this just needn't helperInfo
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        logger.info("Return:Constraint=" + constrInfoList.size());
        return constrInfoList;
    }
    private List<PartitionInfoDTO> getPartitionList4Table(ResultSet rs, Statement stmt, Long tableOid, HelperInfoDTO helperInfo) throws SQLException
    {
        List<PartitionInfoDTO> list = new ArrayList();
        try
        {
            final String sql = "SELECT rel.oid, rel.relname AS relname, nsp.nspname AS schema,pg_get_userbyid(rel.relowner) AS owner"
                + " ,pg_get_expr(rel.relpartbound, rel.oid) AS partition_value,rel.relispartition "
                + " ,(CASE WHEN rel.relkind = 'p' THEN pg_get_partkeydef(rel.oid::oid) ELSE '' END) AS partition_scheme"
                + " ,des.description AS comment "
                + " FROM (SELECT * FROM pg_inherits WHERE inhparent = " + tableOid + ") inh "
                + " LEFT JOIN pg_class rel ON inh.inhrelid = rel.oid "
                + " LEFT JOIN pg_description des ON (des.objoid = inh.inhrelid  AND des.objsubid=0) "
                + " LEFT JOIN pg_namespace nsp ON rel.relnamespace = nsp.oid "
                + " WHERE rel.relispartition ORDER BY rel.relname;\n";
            logger.info(sql);
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                list.add(getPartitionInfoDTO(rs, helperInfo));
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        }
        logger.info("Return:size = " + list.size());
        return list;
    }
    //table or view
    private PartitionInfoDTO getPartitionInfoDTO(ResultSet rs, HelperInfoDTO helperInfo)throws SQLException
    {
        PartitionInfoDTO partition = new PartitionInfoDTO();
        partition.setHelperInfo(helperInfo);        
        partition.setOid(rs.getLong("oid"));
        partition.setName(rs.getString("relname"));
        partition.setSchema(rs.getString("schema"));        
        partition.setOwner(rs.getString("owner"));    
        String value = rs.getString("partition_value");
        partition.setValue(value);
        if("DEFAULT".equalsIgnoreCase(value))
        {
            partition.setDefaultValue(true);
        } else if (value.contains("IN"))
        {
            partition.setPartitionSchema("LIST");
            partition.setInValue(value.substring(value.indexOf("(") + 1, value.indexOf(")")));
            logger.debug("schema=" + partition.getSchema() + "  IN=" + partition.getInValue());
        } else if(value.contains("WITH"))
        {
            partition.setPartitionSchema("HASH");
            int mstart = (value.indexOf("MODULUS") == -1 ? value.indexOf("modulus") 
                    : value.indexOf("MODULUS")) + "MODULUS".length();
            partition.setModulusValue(value.substring(mstart, value.indexOf(",")).trim());
            int rstart = (value.indexOf("REMAINDER")== -1 ? value.indexOf("remainder") 
                    : value.indexOf("REMAINDER"))+ "REMAINDER".length();
            partition.setRemainderValue(value.substring(rstart, value.indexOf(")")).trim());
            logger.debug("schema=" + partition.getSchema() 
                    + "  MODULUS=" + partition.getModulusValue()
                    + "  REMAINDER=" + partition.getRemainderValue());
        } else //FROM TO
        {
            partition.setPartitionSchema("RANGE");
            partition.setFromValue(value.substring(value.indexOf("(") + 1, value.indexOf(")")));
            partition.setToValue(value.substring(value.lastIndexOf("(") + 1, value.lastIndexOf(")")));
            logger.debug("schema=" + partition.getSchema() 
                    + "  FROM=" + partition.getFromValue() 
                    + " TO=" + partition.getToValue());
        }
        
        partition.setPartition(rs.getBoolean("relispartition"));
        partition.setComment(rs.getString("comment"));
        //partion.setTablespace(rs.getString("spcname"));
        //partion.setParentOid(rs.getLong("inhparent"));
        return partition;
    }
    private RuleInfoDTO getRuleInfoDTO(ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        RuleInfoDTO ruleInfo = new RuleInfoDTO();
        ruleInfo.setHelperInfo(helperInfo);
        
        ruleInfo.setName(rs.getString(1));
        ruleInfo.setOid(rs.getLong(2));
        ruleInfo.setParent(rs.getString(3));
        ruleInfo.setEvent(rs.getString(4));
        ruleInfo.setDoInstead(rs.getBoolean(5));
        ruleInfo.setDoStatement(rs.getString(6).replaceFirst("INSTEAD", "").trim());
        ruleInfo.setWhereCondition(rs.getString(7));
        ruleInfo.setEnable(rs.getBoolean(8));
        ruleInfo.setComment(rs.getString(9));
        ruleInfo.setDefineSQL(rs.getString(10));
        return ruleInfo;
    }
    private IndexInfoDTO getIndexInfoDTO(Connection conn, ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        IndexInfoDTO idx = new IndexInfoDTO();
        idx.setHelperInfo(helperInfo);
        
        idx.setDefineSQL(rs.getString(1));
        idx.setOid(rs.getLong("idxoid"));
        idx.setName(rs.getString("idxname"));
        int colCount = rs.getInt("colcount");
        if (rs.getString("tablespace") == null)
        {
            idx.setTablespace(helperInfo.getDbTablespace());
        } else
        {
            idx.setTablespace(rs.getString("tablespace"));
        }
        idx.setClustered(rs.getBoolean("indisclustered"));
        idx.setValid(rs.getBoolean("indisvalid"));
        idx.setUnique(rs.getBoolean("indisunique"));
        idx.setPrimary(rs.getBoolean("indisprimary"));
        idx.setAccessMethod(rs.getString("accessmethod"));
        //idx.setExclude(rs.getBoolean("indisexclusion"));
        idx.setConstraint(rs.getString("indconstraint"));
        idx.setFillFactor(rs.getString("fillfactor"));
        idx.setComment(rs.getString("description"));
        idx.setColInfoList(this.getColumnList(helperInfo, conn, colCount, idx.getOid()));
        return idx;
    }
    private ConstraintInfoDTO getConstraintInfoDTO(Connection conn, ResultSet rtset, HelperInfoDTO helperInfo) throws SQLException, Exception
    {
        ConstraintInfoDTO constrInfo = new ConstraintInfoDTO();
        constrInfo.setHelperInfo(helperInfo);
        
        constrInfo.setType(getConstraintType(rtset.getString("contype")));
        constrInfo.setName(rtset.getString("conname"));
        constrInfo.setOid(rtset.getLong("oid"));
        constrInfo.setComment(rtset.getString("description"));

        constrInfo.setIndexOid(rtset.getLong("conindid"));
        if (rtset.getString("tablespace") == null)
        {
            constrInfo.setTablespace(helperInfo.getDbTablespace());
        } else
        {
            constrInfo.setTablespace(rtset.getString("tablespace"));
        }
        if (constrInfo.getType() != TreeEnum.TreeNode.CHECK)
        {
            constrInfo.setColInfoList(this.getColsFromPosition(rtset.getArray("conkey"), helperInfo, conn, constrInfo.getIndexOid()));
        }

        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0))
        {
            constrInfo.setValidate(rtset.getBoolean("convalidated"));
        }
        constrInfo.setAccessMethod(rtset.getString("amname"));
        constrInfo.setFillFactor(rtset.getString("fillfactor"));//only value of fillfactor

        if (rtset.getString(1).equals("f"))
        {
            constrInfo.setRefTable(rtset.getString("reftable"));
            constrInfo.setRefTabOid(rtset.getLong("reftableoid"));
            constrInfo.setRefColList(this.getRefColsFromPositions(rtset.getArray("confkey"), conn, constrInfo.getRefTabOid()));
            //Foreign key match type: f = full, p = partial, s = simple
            constrInfo.setMatchType(this.getMatchType(rtset.getString("matchtype")));
            constrInfo.setUpdateActions(getAction(rtset.getString("updateaction")));
            constrInfo.setDeleteAction(getAction(rtset.getString("deleteaction")));
            constrInfo.setDeferrable(rtset.getBoolean("condeferrable"));
            constrInfo.setDeferred(rtset.getBoolean("condeferred"));
        }
        constrInfo.setCheckConditonQuoted(rtset.getString("checkcondition"));
        logger.info("****************check constraint quoted = " + constrInfo.getCheckConditonQuoted());
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            constrInfo.setNoInherit(rtset.getBoolean("noinherit"));
        }
        
        constrInfo.setEnable(rtset.getBoolean("conenabled"));//for gc hgdb
        
//        constrInfo.setDefineLocally(rtset.getBoolean("conislocal"));
//        constrInfo.setInheritanceCount(rtset.getInt("coninhcount"));        
        constrInfo.setDefineSQL(rtset.getString("partdefinition"));//this is only a part of sql,like RIMARY KEY(appointment)
        logger.info("****************constraint part define = " + constrInfo.getDefineSQL());
        return constrInfo;
    }
    //pk constraint
    private List<ColInfoDTO> getColsFromPosition(Array colNumArray, HelperInfoDTO helperInfo, Connection conn, Long idxoid) throws SQLException
    {
        if (colNumArray == null || colNumArray.toString().isEmpty())
        {
            return new ArrayList();
        } else
        {            
            logger.info(colNumArray.toString());
            String[] array = colNumArray.toString().trim().replace("{", "").replace("}", "").split(" ");
            int colCount = array.length;
            return this.getColumnList(helperInfo, conn, colCount, idxoid);
        }
    }
    private List<ColInfoDTO> getColumnList(HelperInfoDTO helperInfo, Connection conn, int colCount, Long idxoid) throws SQLException
    {
        List<ColInfoDTO> list = new ArrayList();        
        Statement stmt = null;
        ResultSet rs = null;
       
        try
        {
            StringBuilder sql;
            for(int i=1;i<=colCount;i++)
            {
            sql = new StringBuilder();
            sql.append("select distinct quote_ident(a.attname) AS attname")//pg_get_indexdef(i.indexrelid,").append(i).append(",true) AS coldef,
                    .append(",i.indoption[").append(i - 1).append("] as option");
            boolean higherthan90 = TreeController.getInstance().isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0);
            if (higherthan90)
            {
                sql.append(", CASE WHEN (nspc.nspname IS NULL OR coll.collname IS NULL) THEN NULL")
                        .append(" ELSE quote_ident(nspc.nspname)||'.'|| quote_ident(coll.collname)  END AS collation");
            }
            sql.append(",quote_ident(opcname) AS opcname, o.opcdefault, op.oprname")//operator need not quoted
                    .append(" FROM pg_index i")
                    .append(" JOIN pg_attribute a ON (a.attrelid = i.indexrelid AND attnum=").append(i).append(")")//the attnum of this (constraint) index
                    .append(" LEFT OUTER JOIN pg_opclass o ON (o.oid = i.indclass[").append(i - 1).append("])")
                    .append(" LEFT OUTER JOIN pg_constraint c ON (c.conindid = i.indexrelid)")
                    .append(" LEFT OUTER JOIN pg_operator op ON (op.oid = c.conexclop[").append(i).append("])");
            if (higherthan90)
            {
                sql.append(" LEFT OUTER JOIN pg_collation coll ON a.attcollation=coll.oid")
                        .append(" LEFT OUTER JOIN pg_namespace nspc ON coll.collnamespace=nspc.oid");
            }
            sql.append(" WHERE i.indexrelid =").append(idxoid);
            logger.info(sql.toString());
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                ColInfoDTO c = new ColInfoDTO();
                c.setName(rs.getString("attname"));                
                if (higherthan90)
                {
                    c.setCollation(rs.getString("collation"));
                }                
                c.setOperatorClass(rs.getString("opcname"));
                c.setDefaultOpClass(rs.getBoolean("opcdefault"));//if operator class is default, the it's not defined manully                
                c.setOperator(rs.getString("oprname"));                
                int option = rs.getInt("option");
                switch (option)
                {
                    case 0://this is default
                        //c.setOrder("ASC");
                        //c.setNullsWhen("LAST");
                        c.setOrder(null);
                        c.setNullsWhen(null);
                        break;
                    case 1:
                        c.setOrder("DESC");
                        c.setNullsWhen("LAST");
                        break;
                    case 2:
                        c.setOrder("ASC");
                        c.setNullsWhen("FIRST");
                        break;
                        case 3:
                            c.setOrder("DESC");
                            c.setNullsWhen("FIRST");
                            break;
                        default:
                            logger.error(option + " is an exception option.");
                            break;
                    }
                    list.add(c);
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            logger.info("close resource");
        }
        return list;
    }
    //fk constraint
    private List<ColInfoDTO> getRefColsFromPositions(Array colNumArray, Connection conn, Long tabOid) throws Exception
    {
        if (colNumArray == null)
        {
            return new ArrayList();
        } else
        {
            logger.info(colNumArray.toString());
            String[] array = colNumArray.toString().trim().replace("{", "").replace("}", "").split(" ");
            StringBuilder cols = new StringBuilder();
            cols.append("(");
            for (String a : array)
            {
                cols.append(a).append(",");
            }
            cols.deleteCharAt(cols.length()-1);
            cols.append(")");
            return this.getRefColumnList(conn, cols.toString(), tabOid);
        }
    }
    private List<ColInfoDTO> getRefColumnList(Connection conn, String colPositions, Long tabOid) throws SQLException
    {
        logger.info("Enter:colPositions=" + colPositions);
        List<ColInfoDTO> list = new ArrayList();        
        Statement stmt = null;
        ResultSet rs = null;        
        try
        {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT quote_ident(attname), attnum FROM pg_attribute ")
                    .append(" WHERE attisdropped=false ")
                    .append(" AND attrelid=").append(tabOid)
                    .append(" AND attnum IN").append(colPositions)
                    .append(" ORDER BY attnum");
            logger.info("sql=" + sql);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                ColInfoDTO col = new ColInfoDTO();
                col.setName(rs.getString(1));
                col.setPosition(rs.getInt(2));
                list.add(col);
            }
        }
        catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        }
        finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            logger.info("Close Resource");
        }
        logger.info("Return:columnSize=" + list.size());
        return list;
    }
    //for constraint
    private String getMatchType(String key)
    {
        String matchType = null;
        switch (key)
        {
            case "f":
                matchType = "FULL";
                break;
            case "p":
                matchType = "PARTIAL";
                break;
            case "s":
                matchType = "SIMPLE";
                break;
        }
        return matchType;
    }
    private TreeEnum.TreeNode getConstraintType(String key)
    {
        //c = check constraint, f = foreign key constraint,p = primary key constraint,
        //u = unique constraint,t = constraint trigger, x = exclusion constraint
        switch (key)
        {
            case "c":
               return TreeEnum.TreeNode.CHECK;
            case "f":
                return TreeEnum.TreeNode.FOREIGN_KEY;
            case "p":
                return TreeEnum.TreeNode.PRIMARY_KEY;
            case "u":
                return TreeEnum.TreeNode.UNIQUE;
            case "x":
                return TreeEnum.TreeNode.EXCLUDE;
            case "t":
                logger.info("this is an trigger, not constraint.");
                break;
        }
        return null;
    }
    private String getAction(String key)
    {
        String action = null;
        //Foreign key update/delete action code: a = no action, r = restrict, c = cascade, n = set null, d = set default                  
        switch (key)
        {
            case "a":
                action = "NO ACTION";
                break;
            case "r":
                action = "RESTRICT";
                break;
            case "c":
                action = "CASCADE";
                break;
            case "n":
                action = "SET NULL";
                break;
            case "d":
                action = "SET DEFAULT";
                break;
        }
        return action;
    } 
    private TriggerInfoDTO getTriggerInfoDTO(ResultSet rs, Statement stmt, HelperInfoDTO helperInfo) throws SQLException
    {
        TriggerInfoDTO tgr = new TriggerInfoDTO();
        tgr.setHelperInfo(helperInfo);
        tgr.setName(rs.getString(1));
        tgr.setOid(rs.getLong(2));
        tgr.setParent(rs.getString(3));
        tgr.setConstraintTrigger(rs.getBoolean(4));
        String fireEvent= rs.getString(5).trim();
        if (fireEvent.startsWith("BEFORE"))
        {
            tgr.setFire("BEFORE");
        } else if (fireEvent.startsWith("AFTER"))
        {
            tgr.setFire("AFTER");
        } else if (fireEvent.startsWith("INSTEAD OF"))
        {
            tgr.setFire("INSTEAD OF");
        }
        List<String> eventlist = new ArrayList();
        String[] array = fireEvent.split(" ");
        for (String event : array)
        {
            switch (event)
            {
                case "INSERT":
                case "UPDATE":
                case "DELETE":
                case "TRUNCATE":
                    eventlist.add(event);
                    break;
            }
        }
        tgr.setEventList(eventlist);
        //this is fire ,events and columns
        tgr.setRowTrigger(rs.getBoolean(6));
        tgr.setTriggerFunction(rs.getString(7));
        tgr.setWhen(rs.getString(8));
        tgr.setEnable(rs.getBoolean(9));
        tgr.setComment(rs.getString("comment"));
        tgr.setDefineSQL(rs.getString("definition"));
        tgr.setDeferrable(rs.getBoolean("tgdeferrable"));
        tgr.setDeferred(rs.getBoolean("tginitdeferred"));
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(attname) AS colname")
                .append(" FROM pg_attribute,")
                .append(" (SELECT tgrelid, unnest(tgattr) FROM pg_trigger WHERE oid=").append(tgr.getOid()).append(")AS columns(tgrelid, colnum)")
                .append(" WHERE colnum=attnum AND tgrelid=attrelid");
        logger.info(sql.toString());
        rs = stmt.executeQuery(sql.toString());
        List<String> collist = new ArrayList();
        while (rs.next())
        {
            collist.add(rs.getString("colname"));
        }
        tgr.setColumnList(collist);
        return tgr;
    }
    private ColumnInfoDTO getColumnInfoDTO(ResultSet rs, HelperInfoDTO helperInfo)throws SQLException
    {
        ColumnInfoDTO col = new ColumnInfoDTO();
        col.setHelperInfo(helperInfo);
        col.setName(rs.getString("name"));
        col.setPosition(rs.getInt("position"));
        
        DatatypeDTO type = new DatatypeDTO();
        type.setOid(rs.getLong("typeoid"));
        type.setFullname(rs.getString("fulltypename"));
        type.setShortname(rs.getString("typename"));
        //type.setHasArgs(rs.getInt("typlen") < 0);
        col.setDatatype(type);
        
        col.setDefaultValue(rs.getString("default"));
        col.setSequence(rs.getString("sequence"));
        col.setStorage(rs.getString("storage"));
        col.setStatistics(rs.getString("statistics"));
        col.setAcl(rs.getString("acl"));
        col.setComment(rs.getString("comment"));
        col.setNotNull(rs.getBoolean("notnull"));
        col.setPk(rs.getBoolean("ispk"));
        col.setInherit(rs.getBoolean("isinherited"));
//        if (hm.get(col.getName()) != null)
//        {
//            col.setInheritedTable(hm.get(col.getName()).toString());
//        }
        
        col.setVariableInfoList(this.getVariableListFromArray(rs.getArray("attoptions")));
        boolean higherthan90 = TreeController.getInstance().isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 0);
        if (higherthan90)
        {
            col.setCollation(rs.getString("collation"));
        }
        return col;
    }
    private ViewColumnInfoDTO getViewColumnInfoDTO(ResultSet rs, HelperInfoDTO helperInfo) throws SQLException
    {
        ViewColumnInfoDTO clm = new ViewColumnInfoDTO();
        clm.setHelperInfo(helperInfo);
        clm.setName(rs.getString(1));
        clm.setPosition(rs.getInt(2));
        clm.setDataType(rs.getString(3));
        clm.setDefaultValue(rs.getString(4));
        clm.setAcl(rs.getString(5));
        clm.setComment(rs.getString(6));
        return clm;
    }
    
    
      
    //SQL
    //OBJECT DEFINITION SQL 
    public String getDefinitionSQL(AbstractObject obj)
    {
        StringBuilder sql = new StringBuilder();
        if (obj == null)
        {
            logger.warn("Enter null object, then do nothing and return null.");
            return null;
        }else if(obj instanceof TablespaceInfoDTO)
        {
            sql = getTablespaceDefinition((TablespaceInfoDTO) obj);
        }
        else if(obj instanceof RoleInfoDTO)
        {
            sql = getRoleDefinition((RoleInfoDTO) obj);
        }
        else if (obj instanceof DBInfoDTO)
        {
            sql = getDBDefinition((DBInfoDTO) obj);
        }
        else if (obj instanceof EventTriggerInfoDTO)
        {
            sql = getEventTriggerDefinition((EventTriggerInfoDTO) obj);
        }
        else if (obj instanceof SchemaInfoDTO)
        {
            sql = getSchemaDefinition((SchemaInfoDTO) obj);
        }
        else if (obj instanceof TableInfoDTO)
        {
            sql = getTableDefinition((TableInfoDTO)obj);
        }
        else if(obj instanceof PartitionInfoDTO)
        {
            sql = getPartitionDefinition((PartitionInfoDTO)obj);
        }
        else if (obj instanceof SequenceInfoDTO)
        {
            SequenceInfoDTO sequenceInfo = (SequenceInfoDTO) obj;
            sql = getSequenceDefinition(sequenceInfo);
        }
        else if (obj instanceof ViewInfoDTO)
        {
            ViewInfoDTO viewInfo = (ViewInfoDTO) obj;
            sql = getViewDefinition(viewInfo);
        }
        else if (obj instanceof ColumnInfoDTO)
        {
            ColumnInfoDTO columnInfo = (ColumnInfoDTO) obj;
            sql = getColumnAddDefinition(columnInfo, columnInfo.getHelperInfo());
        }
        else if (obj instanceof ConstraintInfoDTO)
        {
            ConstraintInfoDTO constraintInfo = (ConstraintInfoDTO) obj;
            sql = getConstraintAddDefinition(constraintInfo, constraintInfo.getHelperInfo());
        }
        else if (obj instanceof IndexInfoDTO)
        {
            IndexInfoDTO indexInfo = (IndexInfoDTO) obj;
            sql = getIndexAddDefinition(indexInfo);
        }
        else if (obj instanceof RuleInfoDTO)
        {
            RuleInfoDTO ruleInfo = (RuleInfoDTO) obj;
            sql = getRuleAddDefinition(ruleInfo);
        }
        else if (obj instanceof TriggerInfoDTO)
        {
            TriggerInfoDTO triggerInfo = (TriggerInfoDTO) obj;
            sql = getTriggerAddDefinition(triggerInfo);
        }else if(obj instanceof FunctionInfoDTO)
        {
            FunctionInfoDTO functionInfo = (FunctionInfoDTO) obj;
           sql = getFuncProcAddDefinition(functionInfo);
        }
        //logger.debug("Return:sql = " + sql.toString());
        return sql.toString();
    }    
    private StringBuilder getTablespaceDefinition(TablespaceInfoDTO tablespaceInfo)
    {
        SyntaxController sc = SyntaxController.getInstance();
        String tablespace = sc.getName(tablespaceInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLESPACE ").append(tablespace);
        if (tablespaceInfo.getOwner() != null && !tablespaceInfo.getOwner().isEmpty())
        {
            sql.append("\n OWNER ").append( sc.getName(tablespaceInfo.getOwner()) );
        }
        sql.append("\n LOCATION '").append(tablespaceInfo.getLocation()).append("'; ");
        //privilege
        sql.append(this.getPrivilegeSQLFromACL("TABLESPACE",tablespaceInfo.getAcl(),tablespace));       
        //variable
        sql.append(this.getAllVariableSetSQL(tablespaceInfo.getType(), tablespace, tablespaceInfo.getVariableList()));
        //security label
        sql.append(this.getSecurityLabelSQL("TABLESPACE",tablespaceInfo.getSecurityLabelList(),tablespace));
        //comment
        sql.append(this.getCommentSQL(tablespaceInfo.getType(), tablespaceInfo.getComment(), tablespace, true));
        return sql;
    }
    private StringBuilder getRoleDefinition(RoleInfoDTO roleInfo)
    {
        SyntaxController sc = SyntaxController.getInstance();
        String role = sc.getName(roleInfo.getName());

        StringBuilder sql = new StringBuilder();
        sql.append("CREATE ROLE ").append(role);
        if (roleInfo.isCanLogin())
        {
            sql.append(" LOGIN");
        }
        if (roleInfo.getPwd() != null && !roleInfo.getPwd().isEmpty())
        {
            if (roleInfo.isEncrypted()
                    && !roleInfo.getPwd().equals("********"))//* is got from pg_roles
            {
                sql.append(System.lineSeparator()).append(" ENCRYPTED PASSWORD '").append(roleInfo.getPwd()).append("'");
            } else if (!roleInfo.isEncrypted())
            {
                sql.append(System.lineSeparator()).append(" PASSWORD '").append(roleInfo.isDisplaySQL()? "******":roleInfo.getPwd()).append("'");
            }
        }
        sql.append(System.lineSeparator());
        if (roleInfo.isSuperuser())
        {
            sql.append(" SUPERUSER");
        } else
        {
            sql.append(" NOSUPERUSER");
        }
        if (roleInfo.isCanInherits())
        {
            sql.append(" INHERIT");
        } else
        {
            sql.append(" NOINHERIT");
        }
        if (roleInfo.isCanCreateDB())
        {
            sql.append(" CREATEDB");
        } else
        {
            sql.append(" NOCREATEDB");
        }
        if (roleInfo.isCanCreateRoles())
        {
            sql.append(" CREATEROLE");
        } else
        {
            sql.append(" NOCREATEROLE");
        }
        if (roleInfo.isCanInitStreamReplicationAndBackup())//version >= 9.1
        {
            sql.append(" REPLICATION");
        } else
        {
            sql.append(" NOREPLICATION");
        }
        if (roleInfo.getAccountExpires()!=null && !roleInfo.getAccountExpires().isEmpty())
        {
            sql.append("\n VALID UNTIL '").append(roleInfo.getAccountExpires()).append("'");
        }
        if (roleInfo.getConnectionLimit() != null
                && !roleInfo.getConnectionLimit().isEmpty()
                && !roleInfo.getConnectionLimit().equals("-1"))
        {
            sql.append("\n CONNECTION LIMIT ").append(roleInfo.getConnectionLimit());
        }        
        sql.append(" ; "); 
        
        //member of
        List<String> memberof = roleInfo.getMemeberOf();
        if (memberof!=null && !memberof.isEmpty())
        {
            for (String r : memberof)
            {
                sql.append(System.lineSeparator()).append("GRANT ").append(sc.getName(r.replace("(*)", "")))
                        .append(" TO ").append(role);
                if (r.endsWith("(*)"))
                {
                    sql.append(" WITH ADMIN OPTION");
                }
                sql.append(" ; ");
            }
        }
        //variable
        sql.append(this.getAllVariableSetSQL(roleInfo.getType(), role,  roleInfo.getVariableList()));
        //security label
        sql.append(getSecurityLabelSQL("ROLE",roleInfo.getSecurityLabelList(),role));
        //comment
        sql.append(getCommentSQL(roleInfo.getType(),roleInfo.getComment(),role, true));
        return sql;
    }
    private StringBuilder getDBDefinition(DBInfoDTO dbInfo)
    {
        SyntaxController sc = SyntaxController.getInstance();
        String db = sc.getName(dbInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE DATABASE ").append(db)
                .append("\n  WITH ENCODING = '").append(dbInfo.getEncoding()).append("'");
        if (!dbInfo.getOwner().equals(""))
        {
            sql.append("\n  OWNER = ").append(sc.getName(dbInfo.getOwner()));
        }
        if (dbInfo.getTemplate() != null && !dbInfo.getTemplate().isEmpty())
        {
            sql.append("\n  TEMPLATE =").append(dbInfo.getTemplate());
        }
        if (!dbInfo.getCollation().equals(""))
        {
            sql.append("\n  LC_COLLATE = '").append(dbInfo.getCollation()).append("' ");
        }
        if (!dbInfo.getCharacterType().equals(""))
        {
            sql.append("\n  LC_CTYPE = '").append(dbInfo.getCharacterType()).append("' ");
        }
        if (dbInfo.getTablespace() != null
                && !dbInfo.getTablespace().isEmpty()
                && !(dbInfo.getTablespace().equals("<" + constBundle.getString("defaultTablespace") + ">")))
        {
            sql.append("\n  TABLESPACE = ").append(sc.getName(dbInfo.getTablespace()));
        }
        if (!dbInfo.getConnectionLimit().equals(""))
        {
            sql.append("\n  CONNECTION LIMIT = ").append(dbInfo.getConnectionLimit());
        }
        sql.append(";");
        
        //privilege sql
        sql.append(this.getPrivilegeSQLFromACL("DATABASE", dbInfo.getAcl(), db));
        //variable sql
        sql.append(this.getAllVariableSetSQL(dbInfo.getType(), db, dbInfo.getVariableList()));
        //security label
        sql.append(getSecurityLabelSQL("DATABASE", dbInfo.getSecurityLabelList(), db));
        //comment
        sql.append(getCommentSQL(dbInfo.getType(),dbInfo.getComment(),db, true));     
        return sql;
    }
    private StringBuilder getEventTriggerDefinition(EventTriggerInfoDTO info)
    {
        String name = SyntaxController.getInstance().getName(info.getName());
        StringBuilder sql = new StringBuilder();
        
        sql.append("CREATE EVENT TRIGGER ").append(name)
                .append(" ON ").append(info.getEvent());      
        if(info.getWhenTag()!= null && !info.getWhenTag().trim().isEmpty())
        {
            sql.append("\n  WHEN TAG IN(").append(info.getWhenTag()).append(")");
        }
        sql.append("\n  EXECUTE PROCEDURE ").append(info.getFunc()).append("();\n");
        
        sql.append(getEventTriggerEnableAlterSQL(info, name));
        sql.append(this.getOwnerAlterSQL(info.getType(), info.getOwner(), name));
        sql.append(this.getCommentSQL(info.getType(),info.getComment(), name, true));
        
        return sql;
    }
    private String getEventTriggerEnableAlterSQL(EventTriggerInfoDTO info, String name)
    {
        StringBuilder sql = new StringBuilder();
        sql.append("\nALTER EVENT TRIGGER ").append(name);
        if ("DISABLE".equals(info.getEnableWhat())
                || "ENABLE".equals(info.getEnableWhat())) {
            sql.append(" ").append(info.getEnableWhat());
        } else {
            sql.append(" ENABLE ").append(info.getEnableWhat());
        }
        sql.append(";");
        return sql.toString();
    }

    
    private StringBuilder getSchemaDefinition(SchemaInfoDTO schemaInfo)
    {
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(schemaInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE SCHEMA ").append(schema);
        if (!schemaInfo.getOwner().equals(""))
        {
            sql.append("\n  AUTHORIZATION ").append(sc.getName(schemaInfo.getOwner()));
        }
        sql.append("; ");           
        //privilege
        sql.append(this.getPrivilegeSQLFromACL("SCHEMA",schemaInfo.getAcl(),schema));     
        //security label
        sql.append(this.getSecurityLabelSQL("SCHEMA", schemaInfo.getSecurityLabelList(), schema));
        //comment
        sql.append(this.getCommentSQL(schemaInfo.getType(),schemaInfo.getComment(),schema, true));
        return sql;
    }
    private StringBuilder getSequenceDefinition(SequenceInfoDTO sequenceInfo)
    {
        HelperInfoDTO helperInfo = sequenceInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String seqWithSchema = sc.getName(helperInfo.getSchema())  + "." + sc.getName(sequenceInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE SEQUENCE ").append(seqWithSchema);
        if (sequenceInfo.getStrStart() != null && !sequenceInfo.getStrStart().isEmpty())
        {
            sql.append(" \n ").append("  START ").append(sequenceInfo.getStrCurrent());//sequenceInfo.getStrStart()
        }
        if (sequenceInfo.getStrIncrement() != null && !sequenceInfo.getStrIncrement().isEmpty())
        {
            sql.append(" \n ").append("  INCREMENT ").append(sequenceInfo.getStrIncrement());
        }
        if (sequenceInfo.getStrMinimum() != null && !sequenceInfo.getStrMinimum().isEmpty())
        {
            sql.append(" \n ").append("  MINVALUE ").append(sequenceInfo.getStrMinimum());
        }
        if (sequenceInfo.getStrMaximum() != null && !sequenceInfo.getStrMaximum().isEmpty())
        {
            sql.append(" \n ").append("  MAXVALUE ").append(sequenceInfo.getStrMaximum());
        }        
        if (sequenceInfo.getStrCache() != null && !sequenceInfo.getStrCache().isEmpty())
        {
            sql.append(" \n ").append("  CACHE ").append(sequenceInfo.getStrCache());
        }
        if (sequenceInfo.isCycled())
        {
            sql.append(" \n ").append("  CYCLE");
        }
        sql.append(" ; ");

        //owner
        sql.append(getOwnerAlterSQL(sequenceInfo.getType(), sequenceInfo.getOwner(), seqWithSchema));
        //privilege
        sql.append(getPrivilegeSQLFromACL("SEQUENCE", sequenceInfo.getAcl(), seqWithSchema));
        //security label
        sql.append(getSecurityLabelSQL("SEQUENCE", sequenceInfo.getSecurityLabelList(), seqWithSchema));
        //comment
        sql.append(getCommentSQL(sequenceInfo.getType(), sequenceInfo.getComment(), seqWithSchema, true));
                     
        return sql;
    }
    private StringBuilder getTableDefinition(TableInfoDTO tabInfo)
    {
        HelperInfoDTO helperInfo = tabInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String tableWithSchema = sc.getName(helperInfo.getSchema()) + "." + sc.getName(tabInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        
        sql.append("CREATE");
        if (tabInfo.isUnlogged())
        {
            sql.append(" UNLOGGED");
        }
        sql.append(" TABLE ").append(tableWithSchema);
        
        logger.debug("OfType=" + tabInfo.getOfType());
        if (tabInfo.getOfType() != null && !tabInfo.getOfType().isEmpty())
        {
            sql.append("\n OF ").append(tabInfo.getOfType());
        } else
        {
            sql.append("\n(");
            logger.debug("LikeRelation = " + tabInfo.getLikeRelation());
            if (tabInfo.getLikeRelation() != null && !tabInfo.getLikeRelation().isEmpty())
            {
                sql.append("\n LIKE ").append(tabInfo.getLikeRelation()).append(" ");
            }
            List<ColumnInfoDTO> columnList = tabInfo.getColumnInfoList();
            logger.debug("columnList = " + columnList);
            if (columnList != null)
            {
                int columnSize = columnList.size();
                logger.debug("columnSize = " + columnSize);
                for (int i = 0; i < columnSize; i++)
                {
//                    if ((i == 0 && !(sql.substring(sql.length() - 1, sql.length())).equals("(")) || i > 0)
//                    {
//                        sql.append(", ");
//                    }
                    ColumnInfoDTO col = columnList.get(i);
                    String colName = sc.getName(col.getName());
                    sql.append("\n ");
                    if (col.isInherit())
                    {
                        sql.append("-- Inherits From Table ").append(col.getInheritedTable()).append(": ");
                    }                    
                    sql.append(colName).append(" ").append(col.getDatatype().getFullname());
                    if (col.isNotNull())
                    {
                        sql.append(" NOT NULL");
                    }                    
                    String defalutValue = col.getDefaultValue();
                    if (defalutValue != null && !defalutValue.isEmpty())
                    {
                        sql.append(" DEFAULT ");
                        if (tabInfo.getOid() != null && tabInfo.getOid() > 0)
                        {
                            sql.append(defalutValue);
                        } else
                        {
                            sql.append("'").append(defalutValue).append("'::").append(col.getDatatype());
                        }
                    }
                    String collate = col.getCollation();
                    if (collate != null && !collate.isEmpty())
                    {
                        sql.append(" COLLATE ").append(collate);
                    } 
                    
                    //2016.4.13 add column comments
                    if (i < columnSize - 1)
                    {
                        sql.append(",");
                    }
                    //logger.debug("=============================================");
                    if (col.getComment() != null && !col.getComment().isEmpty())
                    {
                        logger.info(col.getComment());
                        sql.append(" --").append(col.getComment());
                    }                    
                }
            }
        }

        List<ConstraintInfoDTO> constrList = tabInfo.getConstraintInfoList();
        logger.debug("constrList = " + constrList);
        if (constrList != null)
        {
            int constrSize = constrList.size();
            logger.debug("constrSize=" + constrSize);
            if ((tabInfo.getOfType() != null && !tabInfo.getOfType().isEmpty()) && constrSize > 0)
            {
                sql.append("\n(");
            }
            for (int i = 0; i < constrSize; i++)
            {
                logger.debug("last char=" + sql.substring(sql.length() - 1, sql.length()));
                if ((i == 0 && !(sql.substring(sql.length() - 1, sql.length())).equals("(")) || i > 0)
                {
                    sql.append(",");
                }
                sql.append("\n CONSTRAINT ").append(sc.getName(constrList.get(i).getName())).append(" ")
                        .append(constrList.get(i).getDefineSQL());
            }
            if ((tabInfo.getOfType() != null && !tabInfo.getOfType().isEmpty()) && constrSize > 0)
            {
                sql.append("\n)");
            }
        }
        if (tabInfo.getOfType() == null || tabInfo.getOfType().isEmpty())
        {
            sql.append("\n)");
        }

        List<ObjItemInfoDTO> inheritTableList = tabInfo.getInheritTableList();
        if (inheritTableList != null && !inheritTableList.isEmpty())
        {
            sql.append("\n INHERITS ");
            int size = inheritTableList.size();
            for (int i = 0; i < size; i++)
            {
                if (i == 0)
                {
                    sql.append(" ( ");
                } else
                {
                    sql.append(", ");
                }
                sql.append(inheritTableList.get(i).toString());
                if (i == (size - 1))
                {
                    sql.append(" ) ");
                }
            }
        }
        
        if(tabInfo.isPartitioned())
        {
            sql.append("\n PARTITION BY ").append(tabInfo.getPartitionSchema())
                    .append(tabInfo.getPartitionKeyStr4add());
        }
        
        sql.append("\n WITH \n( ");
        if (tabInfo.getFillFactor() != null && !tabInfo.getFillFactor().isEmpty())
        {
            sql.append("\n  FILLFACTOR = ").append(tabInfo.getFillFactor()).append(",");
        }
        sql.append("\n  OIDS = ");
        if (tabInfo.isHasOID())
        {
            sql.append("TRUE");
        } else
        {
            sql.append("FALSE");
        }
        //for storage parameter
        if(getAutoVacuumDefinition(tabInfo.getAutoVacuumInfo()).length()>0)
        {
            sql.append(",");
        }
        sql.append(getAutoVacuumDefinition(tabInfo.getAutoVacuumInfo()));
        sql.append("\n) ");

        if (tabInfo.getTablespace() != null
                && !tabInfo.getTablespace().isEmpty()
                && !(tabInfo.getTablespace().equals("<" + constBundle.getString("defaultTablespace") + ">")))
        {
            sql.append("\n TABLESPACE ").append(tabInfo.getTablespace());
        }
        sql.append("; ");
   
        //owner
        sql.append(getOwnerAlterSQL(tabInfo.getType(), tabInfo.getOwner(), tableWithSchema));
        //privilege
        sql.append(getPrivilegeSQLFromACL("TABLE", tabInfo.getAcl(), tableWithSchema));
        //security label
        sql.append(getSecurityLabelSQL("TABLE", tabInfo.getSecurityInfoList(), tableWithSchema));
        //comment
        sql.append(getCommentSQL(tabInfo.getType(), tabInfo.getComment(), tableWithSchema, true));
        
        //partition 
        if(tabInfo.getPartitionList() != null)
        {
            sql.append("\n\n");
            for(PartitionInfoDTO partInfo : tabInfo.getPartitionList())
            {
                sql.append(getPartitionDefinition(partInfo).toString());
            }
        }
        
        return sql;
    }
     private StringBuilder getPartitionDefinition(PartitionInfoDTO partInfo)
    {
        HelperInfoDTO helperInfo = partInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(helperInfo.getSchema()) + "." +  sc.getName(helperInfo.getRelation());
        //String partition = sc.getName(partInfo.getSchema())+ "." + sc.getName(partInfo.getName());//compatible
        
        StringBuilder sql = new StringBuilder();      
        sql.append("CREATE TABLE ").append(partInfo.getName())
                .append(" PARTITION OF ").append(parent);
        if(partInfo.getOid()!=0l)
        {
           sql.append("\n").append(partInfo.getValue()).append(";\n");
        }else if(partInfo.isDefaultValue())
        {
            sql.append(" DEFAULT;\n");
        }else
        {
           sql.append("\n").append(" FOR VALUES ");
           if("RANGE".equals(partInfo.getPartitionSchema()))
           {
              sql.append(" FROM (").append(partInfo.getFromValue()).append(") TO (" ).append(partInfo.getToValue()).append(");\n"); 
           } else if("LIST".equals(partInfo.getPartitionSchema()))
           {
              sql.append(" IN (").append(partInfo.getInValue()).append(");\n"); 
           } else //HASH
           {
              //FOR VALUES WITH (MODULUS 4, REMAINDER 3);
              sql.append(" WITH (MODULUS ").append(partInfo.getModulusValue())
                   .append(", REMAINDER ").append(partInfo.getRemainderValue()).append(");\n"); 
           }
        }              
        return sql;
    }
    private StringBuilder getViewDefinition(ViewInfoDTO viewInfo)
    {
        HelperInfoDTO helperInfo = viewInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(helperInfo.getSchema());
        String view = sc.getName(viewInfo.getName());
        String viewWithSchema = schema + "." + view;
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE ");
        if (viewInfo.isMaterializedView())
        {
            sql.append("MATERIALIZED ");
        } else
        {
            sql.append("OR REPLACE ");
        }
        sql.append("VIEW ").append(viewWithSchema).append(" \n ");
        if (viewInfo.isSecurityBarrier())
        {
            sql.append(" WITH ( security_barrier = true ) \n");
        }
        if (viewInfo.isMaterializedView())
        {
            String autoVacuum = this.getAutoVacuumDefinition(viewInfo.getAutoVacuumInfo());
            boolean hasFillFactor = (viewInfo.getFillFactor() != null && !viewInfo.getFillFactor().isEmpty());
            if (hasFillFactor || autoVacuum.length() > 0)
            {
                sql.append("WITH ( \n");
            }
            if (hasFillFactor)
            {
                sql.append(" FILLFACTOR = ").append(viewInfo.getFillFactor());
            }
            if (autoVacuum.length() > 0)
            {
                if (hasFillFactor)
                {
                    sql.append(" , \n");
                }
                sql.append(autoVacuum);
            }
            if (hasFillFactor || autoVacuum.length() > 0)
            {
                sql.append(" \n ) \n");
            }
        }
        if (viewInfo.isMaterializedView())
        {
            if (viewInfo.getTablespace() != null 
                    && !(viewInfo.getTablespace().equals("<" + constBundle.getString("defaultTablespace") + ">")))
            {
                sql.append("TABLESPACE ").append(sc.getName(viewInfo.getTablespace())).append(" ");
            }
        }
        sql.append("AS \n  ").append(viewInfo.getDefinition().replaceAll(";", ""));
        if (viewInfo.isMaterializedView())
        {
            if (viewInfo.isWithData())
            {
                sql.append("\nWITH DATA");
            }
            else
            {
                sql.append("\nWITH NO DATA");
            }
        }
        sql.append("; ");
    
        //for alter owner
        //GRANT TABLE is for table,foreign table and view , materialized view
        sql.append(this.getOwnerAlterSQL(viewInfo.getType(), viewInfo.getOwner(), viewWithSchema));
        //for privilege sql
        sql.append(this.getPrivilegeSQLFromACL("TABLE", viewInfo.getAcl(), viewWithSchema));
        //for security label
        if (viewInfo.isMaterializedView())
        {
            sql.append(this.getSecurityLabelSQL("MATERIALIZED VIEW", viewInfo.getSecurityLabelInfoList(), viewWithSchema));
        } else
        {
            sql.append(this.getSecurityLabelSQL("VIEW", viewInfo.getSecurityLabelInfoList(), viewWithSchema));
        }
        //for comment
        sql.append(this.getCommentSQL(viewInfo.getType(), viewInfo.getComment(), viewWithSchema, true));
        return sql;
    }
    private String getAutoVacuumDefinition(AutoVacuumDTO autoVacuumInfo)
    {
        StringBuilder sql = new StringBuilder();
        if(autoVacuumInfo==null)
        {
            return "";
        }        
        if (autoVacuumInfo.isCustomAutoVacuum())
        {
            sql.append(" autovacuum_enabled = ").append(autoVacuumInfo.isEnable());
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumBaseThreshold()))
            {
                sql.append(",\n  autovacuum_vacuum_threshold = ").append(autoVacuumInfo.getVacuumBaseThreshold());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getAnalyzeBaseThreshold()))
            {
                sql.append(",\n autovacuum_analyze_threshold = ").append(autoVacuumInfo.getAnalyzeBaseThreshold());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumScaleFactor()))
            {
                sql.append(",\n autovacuum_vacuum_scale_factor = ").append(autoVacuumInfo.getVacuumScaleFactor());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getAnalyzeScaleFactor()))
            {
                sql.append(",\n autovacuum_analyze_scale_factor = ").append(autoVacuumInfo.getAnalyzeScaleFactor());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumCostDelay()))
            {
                sql.append(",\n autovacuum_vacuum_cost_delay = ").append(autoVacuumInfo.getVacuumCostDelay());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumCostLimit()))
            {
                sql.append(",\n autovacuum_vacuum_cost_limit = ").append(autoVacuumInfo.getVacuumCostLimit());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getFreezeMinimumAge()))
            {
                sql.append(",\n  autovacuum_freeze_min_age = ").append(autoVacuumInfo.getFreezeMinimumAge());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getFreezeMaximumAge()))
            {
                sql.append(",\n autovacuum_freeze_max_age = ").append(autoVacuumInfo.getFreezeMaximumAge());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getFreezeTableAge()))
            {
                sql.append(",\n autovacuum_freeze_table_age = ").append(autoVacuumInfo.getFreezeTableAge());
            }
        }
        if (autoVacuumInfo.isCustomAutoVacuumToast())
        {
            if (sql.toString().length() > 0)
            {
                sql.append(",\n  toast.autovacuum_enabled = ").append(autoVacuumInfo.isEnableToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumBaseThresholdToast()))
            {
                sql.append(",\n toast.autovacuum_vacuum_threshold = ").append(autoVacuumInfo.getVacuumBaseThresholdToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumScaleFactor()))
            {
                sql.append(",\n toast.autovacuum_vacuum_scale_factor = ").append(autoVacuumInfo.getVacuumScaleFactorToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumCostDelay()))
            {
                sql.append(",\n toast.autovacuum_vacuum_cost_delay = ").append(autoVacuumInfo.getVacuumCostDelayToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getVacuumCostLimit()))
            {
                sql.append(",\n toast.autovacuum_vacuum_cost_limit = ").append(autoVacuumInfo.getVacuumCostLimitToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getFreezeMinimumAge()))
            {
                sql.append(",\n toast.autovacuum_freeze_min_age = ").append(autoVacuumInfo.getFreezeMinimumAgeToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getFreezeMaximumAge()))
            {
                sql.append(",\n toast.autovacuum_freeze_max_age = ").append(autoVacuumInfo.getFreezeMaximumAgeToast());
            }
            if (this.notNullOrEmpty(autoVacuumInfo.getFreezeTableAge()))
            {
                sql.append(",\n  toast.autovacuum_freeze_table_age = ").append(autoVacuumInfo.getFreezeTableAgeToast());
            }
        }
        return sql.toString();
    }
    private boolean notNullOrEmpty(String arg)
    {
        return arg != null && !arg.isEmpty();
    }
    private StringBuilder getColumnAddDefinition(ColumnInfoDTO columnInfo, HelperInfoDTO helperInfo)
    {
        //HelperInfoDTO helperInfo = columnInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(helperInfo.getSchema()) + "." + sc.getName(helperInfo.getRelation());
        String column = sc.getName(columnInfo.getName());

        StringBuilder sql = new StringBuilder();
        sql.append("ALTER TABLE ").append(parent).append(System.lineSeparator())
                .append(" ADD COLUMN ")
                .append(column).append(" ").append(columnInfo.getDatatype().getFullname());
        if (columnInfo.isNotNull())
        {
            sql.append(" NOT NULL");
        }
        if ((columnInfo.getDefaultValue() != null) && (!columnInfo.getDefaultValue().isEmpty()))
        {
            sql.append(" DEFAULT ");
            if (columnInfo.getOid() != null && columnInfo.getOid() > 0)
            {
                sql.append(columnInfo.getDefaultValue());
            } else
            {
                sql.append("'").append(columnInfo.getDefaultValue()).append("'::").append(columnInfo.getDatatype());
            }
        }
        if ((columnInfo.getCollation() != null) && (!columnInfo.getCollation().isEmpty()))
        {
            sql.append(" COLLATE ").append(columnInfo.getCollation());
        }
        sql.append("; ");
        //privilege
        sql.append(this.getPrivilegeSQLFromACL4Column("COLUMN", columnInfo.getAcl(), column, parent));
        //variable
        List<VariableInfoDTO> varList = columnInfo.getVariableInfoList();
        if (varList != null)
        {
            for (VariableInfoDTO var : varList)
            {
                sql.append(this.getAlterVariableSQL(TreeEnum.TreeNode.COLUMN, true, 
                         parent, column, var));
            }
        }
        //comment
        sql.append(getCommentSQL(columnInfo.getType(), columnInfo.getComment(),  parent + "." + column, true));
        //security label
        sql.append(getSecurityLabelSQL("COLUMN", columnInfo.getSecurityLabelInfoList(),  parent + "." + column));
        return sql;
    }
    private String getPrivilegeSQLFromACL4Column(String type, String acls, String column, String tabNameWithSchema)
    {
        logger.info("Enter:acl=" + acls);
        StringBuilder sql = new StringBuilder();
        if (acls != null && !acls.isEmpty() && !acls.equals("{}"))
        {
            acls = acls.substring(1, acls.length() - 1);
            String[] aclArray = acls.split(",");
            for (String a : aclArray)
            {
                String[] sp = a.split("=");
                String role = sp[0].replace("group", "GROUP");
                if(role==null || role.isEmpty())
                {
                    role = "public";
                }
                role =  SyntaxController.getInstance().getName(role);
                String privilege = sp[1].split("/")[0];
                if (privilege == null || privilege.isEmpty())
                {
                    sql.append(System.lineSeparator()).append("REVOKE ALL(").append(column).append(") ON ")
                            .append(" ").append(tabNameWithSchema)
                            .append(" FROM ").append(role).append("; ");
                } else
                {
                    String[] privilegeTags = this.getPrivilegeTags(privilege);
                    //this is for without option, column acl withour option
                    if (!privilegeTags[0].isEmpty())
                    {
                        String tags = privilegeTags[0];
                        sql.append(System.lineSeparator()).append("GRANT ");
                        if (type.equals("COLUMN") && tags.equals("arwx"))
                        {
                            sql.append("ALL(").append(column).append(")");
                        } else
                        {
                            for (int i = 0; i < tags.length(); i++)
                            {
                                if (i != 0)
                                {
                                    sql.append(" , ");
                                }
                                sql.append(getPrivilegeKey(tags.charAt(i))).append("(").append(column).append(")");
                            }
                        }
                        sql.append(" ON ").append(tabNameWithSchema)
                                .append(" TO ").append(role).append("; ");
                    }
                }
            }
        }
        return sql.toString();
    }
    private StringBuilder getConstraintAddDefinition(ConstraintInfoDTO constrInfo, HelperInfoDTO helperInfo)
    {
        //HelperInfoDTO helperInfo = constrInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(helperInfo.getSchema()) + "." + sc.getName(helperInfo.getRelation());
        String name = sc.getName(constrInfo.getName());
        StringBuilder sql = new StringBuilder();
        sql.append("ALTER TABLE ")
                .append(parent)
                .append("\n ADD CONSTRAINT ").append(name).append(" ")
                .append(constrInfo.getType().toString().replace("_", " ")).append(" ")
                .append(this.getPartContraintDefine(constrInfo)).append(" ; ");

        sql.append(this.getCommentSQL(TreeEnum.TreeNode.CONSTRAINT, constrInfo.getComment(), name, parent, true));
        return sql;
    }
    private StringBuilder getIndexAddDefinition(IndexInfoDTO indexInfo)
    {
        HelperInfoDTO helperInfo = indexInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(helperInfo.getSchema());
        String table = sc.getName(helperInfo.getRelation());
        String index = sc.getName(indexInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE ");
        if (indexInfo.isUnique())
        {
            sql.append("UNIQUE ");
        }
        sql.append("INDEX ");
        if (indexInfo.isConcurrently())
        {
            sql.append("CONCURRENTLY ");
        }
        if (indexInfo.getName() != null && !indexInfo.getName().isEmpty())
        {
            sql.append(index).append(" ");
        }
        sql.append("\n ON ").append(schema).append(".").append(table).append(" ");
        if (indexInfo.getAccessMethod() != null && !indexInfo.getAccessMethod().isEmpty())
        {
            sql.append("\n USING ").append(indexInfo.getAccessMethod()).append(" ");
        }
        sql.append("(").append(indexInfo.getAllColumnDefine()).append(")");
        if (indexInfo.getFillFactor() != null && !indexInfo.getFillFactor().isEmpty())
        {
            sql.append("\n WITH ( FILLFACTOR = ").append(indexInfo.getFillFactor()).append(")");
        }
        if (indexInfo.getTablespace() != null
                && !indexInfo.getTablespace().isEmpty()
                && !(indexInfo.getTablespace().equals("<" + constBundle.getString("defaultTablespace") + ">")))
        {
            sql.append("\n TABLESPACE ").append(indexInfo.getTablespace()).append(" ");
        }
        if (indexInfo.getConstraint() != null && !indexInfo.getConstraint().isEmpty())
        {
            sql.append(" WHERE ").append(indexInfo.getConstraint());
        }
        sql.append("; ");
        
        if (indexInfo.isClustered()
                && (indexInfo.getName() != null && !indexInfo.getName().isEmpty()))
        {
            sql.append("\nALTER TABLE ").append(schema).append(".").append(table)
                    .append(" CLUSTER ON ")
                    .append(index).append(";");
        }
        

        sql.append(this.getCommentSQL(indexInfo.getType(), indexInfo.getComment(), schema + "." + index, true));        
        return sql;
    }
    private StringBuilder getRuleAddDefinition(RuleInfoDTO ruleInfo)
    {
        HelperInfoDTO helperInfo = ruleInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(helperInfo.getSchema()) + "." + sc.getName(helperInfo.getRelation());
        String rule = sc.getName(ruleInfo.getName());  
        
        StringBuilder sql = new StringBuilder();
        sql.append(this.getRuleCreateSQL(parent, rule, ruleInfo));        
        sql.append(this.getCommentSQL(ruleInfo.getType(), ruleInfo.getComment(), rule, parent, true));
        return sql;
    }
    private String getRuleCreateSQL(String parent, String rule, RuleInfoDTO ruleInfo)
    {
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE OR REPLACE RULE ")
                .append(rule).append(" AS")
                .append("\n ON ").append(ruleInfo.getEvent())
                .append(" TO ").append(parent);
        if (ruleInfo.getWhereCondition() != null && !ruleInfo.getWhereCondition().isEmpty())
        {
            sql.append("\n WHERE ").append(ruleInfo.getWhereCondition());
        }
        sql.append("\n DO ");
        if (ruleInfo.isDoInstead())
        {
            sql.append("INSTEAD ");
        }
        if (ruleInfo.getDoStatement() == null || ruleInfo.getDoStatement().isEmpty())
        {
            sql.append("NOTHING");
        } else
        {
            sql.append(ruleInfo.getDoStatement());
        }
        sql.append("; ");    
        return sql.toString();
    }
    private StringBuilder getTriggerAddDefinition(TriggerInfoDTO triggerInfo)
    {
        HelperInfoDTO helperInfo = triggerInfo.getHelperInfo();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(helperInfo.getSchema()) + "." + sc.getName(helperInfo.getRelation());
        String trigger = sc.getName(triggerInfo.getName());
        
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE ");
        if (triggerInfo.isConstraintTrigger())
        {
            sql.append("CONSTRAINT ");
        }
        sql.append("TRIGGER ").append(trigger).append(" ")
                .append(triggerInfo.getFire()).append(" ");                
        List<String> eventList = triggerInfo.getEventList();
        if (eventList != null)
        {
            int size = eventList.size();
            for (int i = 0; i < size; i++)
            {
                if (i > 0)
                {
                    sql.append("OR ");
                }
                sql.append(eventList.get(i)).append(" ");
                if (eventList.get(i).toUpperCase().equals("UPDATE")
                        && triggerInfo.getColumnList() != null)
                {
                    int colSize = triggerInfo.getColumnList().size();
                    for (int j = 0; j < colSize; j++)
                    {
                        if (j == 0)
                        {
                            sql.append("OF ");
                        }else
                        {
                            sql.append(",");
                        }
                        sql.append(triggerInfo.getColumnList().get(j));
                    }
                    sql.append(" ");
                }
            }
        }
        sql.append("\n ON ").append(parent).append(" ");
        if (triggerInfo.isDeferrable())
        {
            sql.append("DEFERRABLE ");
            if (triggerInfo.isDeferred())
            {
                sql.append("INITIALLY DEFERRED ");
            }
        }
        sql.append("FOR EACH ");
        if (triggerInfo.isRowTrigger())
        {
            sql.append("ROW ");
        }
        else
        {
            sql.append("STATEMENT ");
        }
        if (triggerInfo.getWhen() != null
                && !triggerInfo.getWhen().isEmpty())
        {
            sql.append("\n WHEN (").append(triggerInfo.getWhen()).append(")");
        }
        sql.append("\n EXECUTE PROCEDURE ");
        if (triggerInfo.getTriggerFunction() != null && !triggerInfo.getTriggerFunction().isEmpty())
        {
            sql.append(triggerInfo.getTriggerFunction());
        } else
        {
            sql.append(triggerInfo.getTriggerFunWithoutArg()).append("(");
            if (triggerInfo.getArguments() != null && !triggerInfo.getArguments().isEmpty())
            {
                sql.append(triggerInfo.getArguments());
            }
            sql.append(")");
        }
        sql.append("; ");

        sql.append(this.getCommentSQL(triggerInfo.getType(), triggerInfo.getComment(), trigger, parent, true));
        return sql;
    }
    private StringBuilder getFuncProcAddDefinition(FunctionInfoDTO fun)
    {
        StringBuilder sql = new StringBuilder();        
        //function full name,like funname() or funname(arg1,arg2..)
        SyntaxController sc = SyntaxController.getInstance();
        String funName = sc.getName(fun.getHelperInfo().getSchema()) + "." + fun.getName();//checked full name 
        //create or replace
        sql.append(this.getFuncProcCreateSQL(fun));
        //owner alter
        sql.append(this.getOwnerAlterSQL(fun.getType(), fun.getOwner(), funName));
        //privilege
        sql.append(this.getPrivilegeSQLFromACL(fun.getType().toString(), fun.getAcl(), funName));
        //variable
        sql.append(this.getAllVariableSetSQL(fun.getType(), funName, fun.getVariableList()));
        //security label
        sql.append(this.getSecurityLabelSQL(fun.getType().toString(), fun.getSecurityLabelList(), funName));
        //comment
        sql.append(this.getCommentSQL(fun.getType(), fun.getComment(), funName, true));
        return sql;
    }
    private String getFuncProcCreateSQL(FunctionInfoDTO fun)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String nameWithSchema = sc.getName(fun.getHelperInfo().getSchema()) + "." + sc.getName(fun.getSimpleName());
        if (fun.getOid() == null || fun.getOid()==0)
        {
            sql.append("CREATE ");
        } else
        {
            sql.append("CREATE OR REPLACE ");
        }
        sql.append(fun.getType()).append(" ")
                .append(nameWithSchema).append("(");
        if (fun.getArgList() != null && fun.getArgList().size() > 0)
        {
           sql.append(fun.getAllArgDefine());
        }
        sql.append(")").append(System.lineSeparator());
        
        if (TreeEnum.TreeNode.FUNCTION == fun.getType()) {
            sql.append(" RETURNS");
            if (fun.isReturnSet()) {
                sql.append(" SETOF");
            }
            sql.append(" ").append(fun.getReturnType()==null? "void" : fun.getReturnType());
        }
        
        sql.append(" AS ").append(System.lineSeparator());
        if (fun.getLanguage().equals("c"))
        {
            sql.append(" '").append(fun.getBin()).append("'");
            if (fun.getSrc() != null && !fun.getSrc().isEmpty())
            {
                sql.append(" ,'").append(fun.getSrc()).append("' ");
            }
            sql.append(System.lineSeparator());
        } else
        {
            sql.append(" $BODY$ ").append(System.lineSeparator()).append(fun.getSrc()).append(System.lineSeparator()).append(" $BODY$ ").append(System.lineSeparator());
        }
        sql.append(" LANGUAGE ").append(sc.getName(fun.getLanguage())).append(System.lineSeparator());
        
        if (TreeEnum.TreeNode.FUNCTION == fun.getType()) 
        {
            if (fun.isWindow()) {
                sql.append(" WINDOW");
            }
            if (fun.getVolatiles() != null && !fun.getVolatiles().isEmpty()) {
                sql.append(" ").append(fun.getVolatiles());
            }
            if (fun.isStrict()) {
                sql.append(" STRICT");
            }
            if (fun.isLeafProof()) {
                sql.append(" LEAKPROOF");
            }
        }
        
        if (fun.isSecurityDefiner())
        {
            sql.append(" SECURITY DEFINER");
        }
        
        if (TreeEnum.TreeNode.FUNCTION == fun.getType()) 
        {
            if (fun.getParallel() != null || !fun.getParallel().isEmpty()) {
                sql.append(" PARALLEL ").append(fun.getParallel());
            }
            if (fun.getCost() > 0) {
                sql.append(System.lineSeparator()).append(" COST ").append(fun.getCost())
                        .append(" ");//use blank to quote cost for syntax analysis;
            }
        }

        sql.append(";");
        return sql.toString();
    }
    //script
    public String getSelectScript(AbstractObject obj)throws ClassNotFoundException, SQLException
    {
        SyntaxController sc = SyntaxController.getInstance();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT ");
        if (obj instanceof TableInfoDTO)
        {
            TableInfoDTO tableInfo = (TableInfoDTO) obj;
            List<ColumnInfoDTO> columnList = tableInfo.getColumnInfoList();
            int size = columnList.size();
            for (int i = 0; i < size; i++)
            {
                if (i > 0)
                {
                    sql.append(", ");
                }
                sql.append(sc.getName(columnList.get(i).getName()));
            }
            sql.append("\n FROM ")
                    .append(sc.getName(tableInfo.getHelperInfo().getSchema()))
                    .append(".").append(sc.getName(tableInfo.getName())).append(";");
        } else if (obj instanceof ViewInfoDTO)
        {
            ViewInfoDTO viewInfo = (ViewInfoDTO) obj;
            HelperInfoDTO helperInfo = viewInfo.getHelperInfo();
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            String selectSql = "SELECT attname FROM pg_attribute  WHERE attrelid="
                    + viewInfo.getOid() + " AND attnum>0 ORDER BY attnum;";
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            try
            {
                logger.info(url);
                conn = JdbcHelper.getConnection(url, helperInfo);
                stmt = conn.createStatement();
                logger.info(selectSql);
                rs = stmt.executeQuery(selectSql);
                boolean isFirst = true;
                while (rs.next())
                {
                    if (isFirst)
                    {
                        isFirst = false;
                    } else
                    {
                        sql.append(",");
                    }
                    sql.append(sc.getName(rs.getString(1)));
                }
                sql.append("\n FROM ")
                        .append(sc.getName(viewInfo.getHelperInfo().getSchema()))
                        .append(".").append(sc.getName(viewInfo.getName())).append(";");
            } catch (SQLException ex)
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new SQLException(ex);
            } finally
            {
                JdbcHelper.close(rs);
                JdbcHelper.close(stmt);
                JdbcHelper.close(conn);
                logger.info("Close Resource");
            }
        }
        return sql.toString();
    }
    public String getInsertScript(TableInfoDTO tableInfo)
    {
        SyntaxController sc= SyntaxController.getInstance();         
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO \n ").append(sc.getName(tableInfo.getHelperInfo().getSchema()))
                .append(".").append(sc.getName(tableInfo.getName()))
                .append("(");
        List<ColumnInfoDTO> columnList = tableInfo.getColumnInfoList();
        StringBuilder paras = new StringBuilder();
        int size = columnList.size();
        for (int i = 0; i < size; i++)
        {
            if (i > 0)
            {
                sql.append(", ");
                paras.append(", ");
            }
            sql.append(sc.getName(columnList.get(i).getName()));
            paras.append("?");
        }
        sql.append(")\n VALUES(").append(paras.toString()).append(");");
        return sql.toString();
    }
    public String getUpdateScript(TableInfoDTO tableInfo)
    {
        SyntaxController sc= SyntaxController.getInstance();   
        StringBuilder sql = new StringBuilder();       
        sql.append("UPDATE ").append(sc.getName(tableInfo.getHelperInfo().getSchema()))
                .append(".").append(sc.getName(tableInfo.getName()))
                .append("\n SET ");
        List<ColumnInfoDTO> columnList = tableInfo.getColumnInfoList();
        int size = columnList.size();
        for (int i = 0; i < size; i++)
        {
            if (i > 0)
            {
                sql.append(", ");
            }
            sql.append(sc.getName(columnList.get(i).getName())).append("=?");
        }
        sql.append("\n WHERE <condition>;");
        return sql.toString();
    }
    public String getDeleteScript(TableInfoDTO tableInfo)
    {
        SyntaxController sc= SyntaxController.getInstance();
        StringBuilder sql = new StringBuilder();        
        sql.append("DELETE FROM ").append(sc.getName(tableInfo.getHelperInfo().getSchema()))
                .append(".").append(sc.getName(tableInfo.getName()))
                .append("\n WHERE <condition>;");
        return sql.toString();
    }
     //OBJECT CREATE EXCUTION
    public void createObj(HelperInfoDTO helperInfo, AbstractObject objInfo) throws SQLException, ClassNotFoundException, Exception
    {
        logger.info("Enter");
        if (objInfo == null)
        {
            logger.error("objInfo is null, do nothing, return.");
            throw new Exception("objInfo is null, do nothing, return.");
        }
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return.");
            throw new Exception("connectInfo is null, do nothing, return.");
        }
        String db = helperInfo.getDbName();
        logger.debug(db);
        String sql = this.getDefinitionSQL(objInfo);
        logger.debug(sql);
        this.executeSql(helperInfo, helperInfo.getDbName(), sql);
    }     
    //OBJECT DROP [CASCADE] EXCUTION
    public String getDropSQL(AbstractObject objInfo, HelperInfoDTO helperInfo, boolean isCascade)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc= SyntaxController.getInstance();
        TreeEnum.TreeNode type = objInfo.getType();
        String name;
        if (TreeEnum.TreeNode.FUNCTION == type
                || TreeEnum.TreeNode.PROCEDURE == type) 
        {
            FunctionInfoDTO func = (FunctionInfoDTO) objInfo;
            name = func.getName();
        } else 
        {
            name = sc.getName(objInfo.getName());
        }
        //note:1. system object cannot be droped 2. inherited column cannot be droped
        switch(type)
        {
            case LANGUAGE:
                sql.append("DROP LANGUAGE ").append(name);
                break;
            case LOGINROLE:
            case GROUPROLE:
                sql.append("DROP ROLE ").append(name);
                break;
            case TABLESPACE:
                if (name.equals("pg_global") || name.equals("pg_default"))
                {
                    logger.warn(objInfo.getName() + " is system tablespace, cannot be drop.");
                    return null;
                }
                sql.append("DROP TABLESPACE ").append(name);
                break;
            case DATABASE:
                sql.append("DROP DATABASE ").append(name);
                break;
            case EVENT_TRIGGER:
                sql.append("DROP EVENT TRIGGER IF EXISTS ").append(name);
                break;
            case SCHEMA:
                sql.append("DROP SCHEMA ").append(name);
                break;
            case FUNCTION:
            case PROCEDURE:
                sql.append("DROP ").append(type.toString()).append(" ")
                        .append(sc.getName(helperInfo.getSchema())).append(".").append(name);
                break;
            case SEQUENCE:
                sql.append("DROP SEQUENCE ").append(sc.getName(helperInfo.getSchema())).
                        append(".").append(name);
                break;
            case VIEW:
                sql.append("DROP VIEW ").append(sc.getName(helperInfo.getSchema())).
                        append(".").append(name);
                break;
            case MATERIALIZED_VIEW:
                sql.append("DROP MATERIALIZED VIEW ").append(sc.getName(helperInfo.getSchema())).
                        append(".").append(name);
                break;
            case TABLE:
                sql.append("DROP TABLE ").append(sc.getName(helperInfo.getSchema())).
                        append(".").append(name);
                break;
            case PARTITION:
                //ALTER TABLE public.trange DETACH PARTITION public.trange35;
                PartitionInfoDTO pt = (PartitionInfoDTO)objInfo;
                //DETACH
                sql.append("ALTER TABLE ")
                        .append(sc.getName(helperInfo.getSchema())).append(".").append(sc.getName(helperInfo.getRelation()))
                        .append(" DETACH PARTITION ")
                        .append(sc.getName(pt.getSchema())).append(".").append(pt.getName()).append(";");
                //DROP.here cannnot use line seperator
                sql.append("\nDROP TABLE ").append(sc.getName(pt.getSchema())).append(".").append(pt.getName());
                break;
            case COLUMN:
                ColumnInfoDTO col = (ColumnInfoDTO) objInfo;
                if(col.isInherit())
                {
                    logger.warn(col.getName() + " is inherited column,  cannot be changed");
                    return null;
                } else
                {
                    sql.append("ALTER TABLE ").append(sc.getName(helperInfo.getSchema()))
                            .append(".").append(sc.getName(helperInfo.getRelation()))
                            .append(" DROP COLUMN ").append(name);
                }
                break;
            case PRIMARY_KEY:
            case UNIQUE:
            case FOREIGN_KEY:
            case CHECK:
            case EXCLUDE:
                sql.append("ALTER TABLE ").append(sc.getName(helperInfo.getSchema())) .append(".").append(sc.getName(helperInfo.getRelation()))
                        .append(" DROP CONSTRAINT ").append(name);
                break;
            case INDEX:
                sql.append("DROP INDEX ")
                        .append(sc.getName(helperInfo.getSchema())).append(".").append(name);
                break;
            case TRIGGER:
                sql.append("DROP TRIGGER ").append(name)
                        .append(" ON ").append(sc.getName(helperInfo.getSchema()))
                        .append(".").append(sc.getName(helperInfo.getRelation()));
                break;
            case RULE:
                sql.append("DROP RULE ").append(name)
                        .append(" ON ").append(sc.getName(helperInfo.getSchema()))
                        .append(".").append(sc.getName(helperInfo.getRelation()));
                break;
            default:
                logger.warn("No drop sql for " + name + " of type "+ type.toString());
                return null;
        }
        if(isCascade)
        {
            sql.append(" CASCADE");
        }
        sql.append(";");
        
        logger.info("return:sql=" + sql.toString());
        return sql.toString();
    }
    public void dropObj(HelperInfoDTO helperInfo, AbstractObject objInfo, boolean isCascade) throws SQLException, ClassNotFoundException, Exception
    {
        logger.info("Enter:");
        if (objInfo == null)
        {
            logger.error("objInfo is null, do nothing, return.");
            throw new Exception("objInfo is null, do nothing, return.");
        }
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return.");
            throw new Exception("connectInfo is null, do nothing, return.");
        }
        String db;
        if (objInfo instanceof DBInfoDTO)
        {
            db = helperInfo.getMaintainDB();
        } else
        {
            db = helperInfo.getDbName();
        }
        logger.info(db);
        String sql = this.getDropSQL(objInfo, helperInfo, isCascade);
        logger.info(sql);
        this.executeSql(helperInfo, db, sql);
    } 
   
    
    
    //Define SQL 
    //privilges sql (from acl string, for db,schema,table,sequence,view)
    private String getPrivilegeSQLFromACL(String objType, String acls, String objNameWithSchema)
    {
        logger.info("Enter:acl=" + acls);
        StringBuilder sql = new StringBuilder();
        if (acls != null && !acls.isEmpty() && !acls.equals("{}"))
        {
            acls = acls.substring(1, acls.length() - 1);
            String[] aclArray = acls.split(",");
            for (String a : aclArray)
            {
                String[] sp = a.split("=");
                String role = sp[0];//group role without 'group' is also ok
                if (role==null || role.isEmpty())
                {
                    role = "public";
                }
                //logger.debug("role=" + role);
                role = SyntaxController.getInstance().getName(role);//must check quoted, beacuse this role name cannot be quoted in interface 
                String privilege = sp[1].split("/")[0];
                if (privilege == null || privilege.isEmpty())
                {
                    sql.append(this.getPrivilegeRevokeSQL(objType, objNameWithSchema, role));
                } else
                {
                    String[] privilegeTags = this.getPrivilegeTags(privilege);                    
                    if (!privilegeTags[0].isEmpty())
                    {
                        sql.append(this.getPrivilegeGrantSQL(privilegeTags[0], objType, objNameWithSchema, role, false));
                    }
                    if (!privilegeTags[1].isEmpty())
                    {
                        sql.append(this.getPrivilegeGrantSQL(privilegeTags[1], objType, objNameWithSchema, role, true));
                    }
                }
            }
        }
        return sql.toString();
    }
    private String getPrivilegeRevokeSQL(String type, String name, String usergrouprole)
    {
        StringBuilder sql = new StringBuilder();
        sql.append(" ").append(System.lineSeparator()).append("REVOKE ALL ON ").append(type.equals("VIEW")? "TABLE":type).append(" ")
                .append(name)
                .append(" FROM ").append(usergrouprole).append("; ");
        return sql.toString();
    }
    public String[] getPrivilegeTags(String tagString)
    {
        logger.info("Enter:" + tagString);
        if (tagString == null || tagString.isEmpty())
        {
            return null;
        }
        String[] tagArray = new String[2];
        tagArray[0] = "";//tags without *
        tagArray[1] = "";//tags with *
        for (int i = 0; i < tagString.length(); i++)
        {
            char[] charArray = tagString.toCharArray();
            if (charArray[i] != '*')
            {
                int k = i + 1;
                if (k < tagString.length() && charArray[k] == '*')
                {
                    tagArray[1] = tagArray[1] + charArray[i];
                } else
                {
                    tagArray[0] = tagArray[0] + charArray[i];
                }
            }
        }
        logger.info("tag with *:" + tagArray[0] + ",tag without *:" + tagArray[1]);
        return tagArray;
    }
    private String getPrivilegeGrantSQL(String tags, String type, String name, String usergrouprole, boolean withOption)
    {
        if (tags.isEmpty())
        {
            return null;
        }
        StringBuilder strb = new StringBuilder();
        strb.append(" ").append(System.lineSeparator()).append("GRANT ");
        if ((type.equals("DATABASE") && tags.equals("CTc"))
                || (type.equals("SCHEMA") && tags.equals("UC"))
                || (type.equals("TABLE") && tags.equals("arwdDxt"))
                || (type.equals("SEQUENCE") && tags.equals("arwdDxt"))
                || (type.equals("VIEW") && tags.equals("arwdDxt"))
                || (type.equals("MATERIALIZED_VIEW") && tags.equals("arwdDxt"))
                || (type.equals("SEQUENCE") && tags.equals("rwU"))
                || (type.equals("FUNCTION") && tags.equals("X")))
        {
            strb.append("ALL");
        } else
        {
            for (int i = 0; i < tags.length(); i++)
            {
                if (i != 0)
                {
                    strb.append(" , ");
                }
                strb.append(getPrivilegeKey(tags.charAt(i)));
            }
        }
        strb.append(" ON ").append(type.equals("VIEW")? "TABLE":type).append(" ")
                .append(name)
                .append(" TO ").append(usergrouprole);
        if (withOption)
        {
            strb.append(" WITH GRANT OPTION");
        }
        strb.append("; ");
        return strb.toString();
    }
    private String getPrivilegeKey(char tag)
    {
        String key = null;
        switch (tag)
        {
            case 'r':
                key = "SELECT";
                break;
            case 'w':
                key = "UPDATE";
                break;
            case 'U':
                key = "USAGE";
                break;
            case 'a':
                key = "INSERT";
                break;
            case 'd':
                key = "DELETE";
                break;
            case 'D':
                key = "TRUNCATE";
                break;
            case 'x':
                key = "REFERENCES";
                break;
            case 't':
                key = "TRIGGER";
                break;
            case 'C':
                key = "CREATE";
                break;
            case 'c':
                key = "CONNECT";
                break;
            case 'T':
                key = "TEMP";
                break;
            case 'X':
                key = "EXECUTE";
                break;
        }
        return key;
    }
    //owner sql
    private String getOwnerAlterSQL(TreeEnum.TreeNode objType, String newOwner, String objNameWithSchema)
    {
        StringBuilder sql = new StringBuilder();
        if (newOwner != null && !newOwner.isEmpty())
        {
             SyntaxController sc = SyntaxController.getInstance();
            sql.append(" ").append(System.lineSeparator()).append("ALTER ")
                    .append(this.getTypeFromEnum(objType)).append(" ").append(objNameWithSchema)
                    .append(" OWNER TO ").append(sc.getName(newOwner)).append("; ");
        }
        return sql.toString();
    }
    //comment sql
    private String getCommentSQL(TreeEnum.TreeNode objType, String comment, String objNameWithSchema, boolean isCreate)
    {
        if (isCreate && (comment == null || comment.isEmpty()))
        {
            return "";//empty comment needn't add when creating
        }
        StringBuilder sql = new StringBuilder();
        sql.append("").append(System.lineSeparator()).append("COMMENT ON ")
                .append(this.getTypeFromEnum(objType)).append(" ")
                .append(objNameWithSchema).append(" IS ");
        if (comment == null || comment.isEmpty())
        {
            sql.append("NULL");
        } else
        {
            sql.append("'").append(comment).append("'");
        }
        sql.append("; ");
        return sql.toString();
    }
    //for relation's children-constraint,rule,trigger
    private String getCommentSQL(TreeEnum.TreeNode objType, String comment, String objName, String parentWithSchema, boolean isCreate)
    {
        if (isCreate && (comment == null || comment.isEmpty()))
        {
            return "";//empty comment needn't add when creating
        }        
        StringBuilder sql = new StringBuilder();
        sql.append("").append(System.lineSeparator()).append("COMMENT ON ")
                .append(this.getTypeFromEnum(objType)).append(" ").append(objName)
                .append(" ON ").append(parentWithSchema)
                .append("\n IS ");
        if (comment == null || comment.isEmpty())
        {
            sql.append("NULL");
        } else
        {
            sql.append("'").append(comment).append("'");
        }
        sql.append("; ");        
        return sql.toString();
    }
    private String getAllVariableSetSQL(TreeEnum.TreeNode type, String name, List<VariableInfoDTO> vList)
    {
        StringBuilder sql = new StringBuilder();
        if (vList == null || vList.isEmpty())
        {
            return sql.toString();
        }
        switch (type)
        {
            case TABLESPACE:
            case LOGINROLE:
            case GROUPROLE:
            case DATABASE:
            case FUNCTION:
                for (VariableInfoDTO var : vList)
                {
                    sql.append(this.getAlterVariableSQL(type, true, name, var));
                }
                break;
        }
        return sql.toString();
    }
    private String getTypeFromEnum(TreeEnum.TreeNode objType)
    {
        switch (objType)
        {
            case LOGINROLE:
            case GROUPROLE:
                return "ROLE";
            case EVENT_TRIGGER:
                return "EVENT TRIGGER";
            case MATERIALIZED_VIEW:
                return "MATERIALIZED VIEW";
            case CHECK:
            case FOREIGN_KEY:
            case PRIMARY_KEY:
            case UNIQUE:
            case EXCLUDE:
                return "CONSTRAINT";
            default:
                return objType.toString();
        }
    }
    //security labels sql
    private String getSecurityLabelSQL(String objType, List<SecurityLabelInfoDTO> slist, String objNameWithSchema)
    {
        StringBuilder sql = new StringBuilder();
        if (slist != null && !slist.isEmpty())
        {
            logger.info("size=" + slist.size());
            for (int k = 0; k < slist.size(); k++)
            {
                SecurityLabelInfoDTO security = slist.get(k);
                sql.append(" ").append(System.lineSeparator()).append("SECURITY LABEL FOR ").append(security.getProvider())
                        .append(" ON ").append(objType).append(" ").append(objNameWithSchema).append(" IS '")
                        .append(security.getSecurityLabel()).append("'; ");
            }
        }
        return sql.toString();
    }
    //Change SQL    
    //rename sql
    private String getRenameSQL(TreeEnum.TreeNode type, String oldName, String newName)
    {
        StringBuilder sql = new StringBuilder();
        sql.append("ALTER");
        switch (type)
        {
            case LOGINROLE:
            case GROUPROLE:
                sql.append(" ROLE ").append(oldName).append(" RENAME TO ")
                        .append(newName).append(";");
                break;
            case MATERIALIZED_VIEW:
                sql.append(" MATERIALIZED VIEW ").append(oldName).append(" RENAME TO ")
                        .append(newName).append(";");
                break;
            default:
                sql.append(" ").append(type.toString()).append(" ").append(oldName)
                        .append(" RENAME TO ").append(newName).append(";");
                break;
        }
        return sql.toString();
    }
    private String getRenameSQL(TreeEnum.TreeNode type, String parentRelation, String oldName, String newName)
    {
        StringBuilder sql = new StringBuilder();
        sql.append(" ").append(System.lineSeparator()).append("ALTER");
        switch (type)
        {
            case COLUMN:
                sql.append(" TABLE ").append(parentRelation)
                        .append(" RENAME ").append(oldName).append(" TO ").append(newName)
                        .append(";");
                break;
            case PRIMARY_KEY:
            case UNIQUE:
            case FOREIGN_KEY:
            case CHECK:
            case EXCLUDE:                
                sql.append(" TABLE ").append(parentRelation)
                        .append(" RENAME CONSTRAINT ").append(oldName).append(" TO ").append(newName)
                        .append(";");
                break;
            case TRIGGER:
            case RULE:// V >= 9.3
                sql.append(" ").append(type.toString()).append(" ").append(oldName)
                        .append(" ON ").append(parentRelation)
                        .append(" RENAME TO ").append(newName).append(";");
                break;
        }
        return sql.toString();
    }
    //variable sql
    private int findVariable(TreeEnum.TreeNode type, VariableInfoDTO var, List<VariableInfoDTO> ov)
    {
        int state = 0;//new 
        switch (type)
        {
            case LOGINROLE:
            case GROUPROLE:
                for (VariableInfoDTO v : ov)
                {
                    if (var.getName().equals(v.getName()) && var.getDb().equals(v.getDb()))
                    {
                        if (!var.getValue().equals(v.getValue()))
                        {
                            state = 1;//change 
                        } else
                        {
                            state = 2;//not change
                        }
                        break;
                    }
                }
                break;
            case DATABASE:
                for (VariableInfoDTO v : ov)
                {
                    if (var.getName().equals(v.getName()) && var.getUser().equals(v.getUser()))
                    {
                        if (!var.getValue().equals(v.getValue()))
                        {
                            state = 1;//change 
                        } else
                        {
                            state = 2;//not change
                        }
                        break;
                    }
                }
                break;
            case TABLESPACE:
            case FUNCTION:
            case COLUMN:
                for (VariableInfoDTO v : ov)
                {
                    if (var.getName().equals(v.getName()))
                    {
                        if (!var.getValue().equals(v.getValue()))
                        {
                            state = 1;//change 
                        } else
                        {
                            state = 2;//not change
                        }
                        break;
                    }
                }
                break;
        }
        logger.info("**************" + var.getName() + "," + state);
        return state;
    }
    private String getVariableSQL(TreeEnum.TreeNode type, String name, List<VariableInfoDTO> oldv, List<VariableInfoDTO> newv)
    {
        StringBuilder sql = new StringBuilder();
        Collections.sort(oldv);
        Collections.sort(newv);
        if (newv == null)
        {
            return sql.toString();
        }
        switch (type)
        {
            case TABLESPACE:
            case FUNCTION:
            case LOGINROLE:
            case GROUPROLE:
            case DATABASE:
                for (VariableInfoDTO var : newv)
                {
                    if (this.findVariable(type, var, oldv) <= 1)
                    {
                        sql.append(this.getAlterVariableSQL(type, true, name, var));
                    }
                }
                for (VariableInfoDTO v : oldv)
                {
                    if (this.findVariable(type, v, newv) == 0)
                    {
                        sql.append(this.getAlterVariableSQL(type, false, name, v));
                    }
                }
                break;
        }
        return sql.toString();
    }
    private String getAlterVariableSQL(TreeEnum.TreeNode type, boolean isset, String name, VariableInfoDTO var)
    {
        StringBuilder sql = new StringBuilder();
        switch (type)
        {
            case TABLESPACE:
            case FUNCTION:
                sql.append(" ").append(System.lineSeparator()).append("ALTER ").append(type.toString()).append(" ").append(name);
                if (isset)
                {
                    sql.append(" SET ").append(var.getName()).append(" = ").append(var.getValue()).append("; ");
                } else
                {
                    sql.append(" RESET ").append(var.getName()).append("; ");
                }
                break;
            case LOGINROLE:
            case GROUPROLE:
                sql.append(" ").append(System.lineSeparator()).append("ALTER ROLE ").append(name);
                if (var.getDb() != null && !var.getDb().isEmpty())
                {
                    sql.append(" IN  DATABASE ").append(var.getDb());
                }
                if (isset)
                {
                    sql.append(" SET ").append(var.getName()).append(" = '").append(var.getValue()).append("'; ");
                } else
                {
                    sql.append(" RESET ").append(var.getName()).append("; ");
                }
                break;
            case DATABASE:
                sql.append(" ").append(System.lineSeparator()).append("ALTER ");
                if (var.getUser() != null && !var.getUser().equals(""))
                {
                    sql.append(" ROLE ").append(var.getUser()).append(" IN ");
                }
                sql.append(" DATABASE ").append(name);
                if (isset)
                {
                    sql.append(" SET ").append(var.getName()).append("='").append(var.getValue()).append("'; ");
                } else
                {
                    sql.append(" RESET ").append(var.getName()).append("; ");
                }
                break;
        }
        return sql.toString();
    }
    private String getVariableSQL(TreeEnum.TreeNode type, String parent, String name,List<VariableInfoDTO> oldv, List<VariableInfoDTO> newv)
    {
        StringBuilder sql = new StringBuilder();
        Collections.sort(oldv);
        Collections.sort(newv);
        if (newv == null)
        {
            return sql.toString();
        }
        switch (type)
        {
            case COLUMN:
                for (VariableInfoDTO var : newv)
                {
                    if (this.findVariable(type, var, oldv) <= 1)
                    {
                        sql.append(this.getAlterVariableSQL(type, true, parent, name, var));
                    }
                }
                for (VariableInfoDTO v : oldv)
                {
                    if (this.findVariable(type, v, newv) == 0)
                    {
                        sql.append(this.getAlterVariableSQL(type, false, parent,  name, v));
                    }
                }
                break;
        }
        logger.info( sql.toString());
        return sql.toString();
    }
    private String getAlterVariableSQL(TreeEnum.TreeNode type, boolean isset, String parent, String name, VariableInfoDTO var)
    {
        StringBuilder sql = new StringBuilder();
        switch (type)
        {
            case COLUMN:
                sql.append(" ").append(System.lineSeparator())
                        .append("ALTER TABLE ").append(parent)
                        .append(" ALTER ").append(type.toString()).append(" ").append(name);
                if (isset)
                {
                    sql.append(" SET (").append(var.getName()).append(" = ").append(var.getValue()).append("); ");
                } else
                {
                    sql.append(" RESET (").append(var.getName()).append("); ");
                }
                break;
        }
        logger.info(sql.toString());
        return sql.toString();
    }

    
    
    
    //OBJECT CHANGE SQL
    public String getTablespaceChangeSQL(TablespaceInfoDTO oldTablespace, TablespaceInfoDTO newTablespace)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String newName = sc.getName(newTablespace.getName());
        String oldName = sc.getName(oldTablespace.getName());
        
        if (!newTablespace.getName().equals(oldTablespace.getName()))
        {
            sql.append(this.getRenameSQL(newTablespace.getType(), oldName, newName));
        }
        if (!newTablespace.getOwner().equals(oldTablespace.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newTablespace.getType(), newTablespace.getOwner(), newName));
        }
        if (!Compare.getInstance().equalVariableList(newTablespace.getVariableList(), oldTablespace.getVariableList()))
        {
            sql.append(this.getVariableSQL(newTablespace.getType(), newName, oldTablespace.getVariableList(), newTablespace.getVariableList()));
        }
        if (!newTablespace.getAcl().equals(oldTablespace.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL(newTablespace.getType().toString(), newTablespace.getAcl(), newName));
        }
        if (!newTablespace.getComment().equals(oldTablespace.getComment())
                && !(newTablespace.getComment().isEmpty() && oldTablespace.getComment() == null))
        {
            sql.append(this.getCommentSQL(newTablespace.getType(), newTablespace.getComment(), newName, false));
        }
        return sql.toString();
    }
    public String getRoleChangeSQL(RoleInfoDTO oldRole, RoleInfoDTO newRole)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String newName = sc.getName(newRole.getName());
        
        if (!newRole.getName().equals(oldRole.getName()))
        {
            sql.append(this.getRenameSQL(newRole.getType(), sc.getName(oldRole.getName()), newName));
        }        
        if(newRole.getPwd()!=null && !newRole.getPwd().isEmpty())
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER ROLE ").append(newName).append(" PASSWORD '").append(newRole.isDisplaySQL()? "******":newRole.getPwd()).append("';");
        }
        if (newRole.getConnectionLimit() != null && !newRole.getConnectionLimit().isEmpty()
                && !newRole.getConnectionLimit().equals(oldRole.getConnectionLimit()))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER ROLE ").append(newName).append(" CONNECTION LIMIT ").append(newRole.getConnectionLimit()).append(";");
        }
        if (newRole.getAccountExpires() != null && !newRole.getAccountExpires().isEmpty()
                && (oldRole.getAccountExpires() == null || !oldRole.getAccountExpires().contains(newRole.getAccountExpires())))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER ROLE ").append(newName).append(" VALID UNTIL '").append(newRole.getAccountExpires()).append("'").append(";");
        } else if ((newRole.getAccountExpires() == null || newRole.getAccountExpires().isEmpty())
                && (oldRole.getAccountExpires() != null && !oldRole.getAccountExpires().isEmpty()))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER ROLE ").append(newName).append(" VALID UNTIL 'infinity'").append(";");
        }
        
        if (newRole.isCanLogin() != oldRole.isCanLogin()
                || newRole.isCanInherits() != oldRole.isCanInherits()
                || newRole.isSuperuser() != oldRole.isSuperuser()
                || newRole.isCanCreateDB() != oldRole.isCanCreateDB()
                || newRole.isCanCreateRoles() != oldRole.isCanCreateRoles()
                //ygq v5 admin del start
                //|| newRole.isCanModifyCatalog() != oldRole.isCanModifyCatalog()
                //ygq v5 admin del end
                || newRole.isCanInitStreamReplicationAndBackup() != oldRole.isCanInitStreamReplicationAndBackup())
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER ROLE ").append(newName);
            if (newRole.isCanLogin() != oldRole.isCanLogin())
            {
                if (newRole.isCanLogin())
                {
                    sql.append(" LOGIN");
                } else
                {
                    sql.append(" NOLOGIN");
                }
            }
            if (newRole.isCanInherits() != oldRole.isCanInherits())
            {
                if (newRole.isCanInherits())
                {
                    sql.append(" INHERIT");
                } else
                {
                    sql.append(" NOINHERIT");
                }
            }
            if (newRole.isSuperuser() != oldRole.isSuperuser())
            {
                if (newRole.isSuperuser())
                {
                    sql.append(" SUPERUSER");
                } else
                {
                    sql.append(" NOSUPER");
                }
            }
            if (newRole.isCanCreateDB() != oldRole.isCanCreateDB())
            {
                if (newRole.isCanCreateDB())
                {
                    sql.append(" CREATEDB");
                } else
                {
                    sql.append(" NOCREATEDB");
                }
            }
            if (newRole.isCanCreateRoles() != oldRole.isCanCreateRoles())
            {
                if (newRole.isCanCreateRoles())
                {
                    sql.append(" CREATEROLE");
                } else
                {
                    sql.append(" NOCREATEROLE");
                }
            }
            if (newRole.isCanInitStreamReplicationAndBackup() != oldRole.isCanInitStreamReplicationAndBackup())
            {
                if (newRole.isCanInitStreamReplicationAndBackup())
                {
                    sql.append(" REPLICATION");
                } else
                {
                    sql.append(" NOREPLICATION");
                }
            }
            sql.append(";");
        }
        logger.debug(newRole.getMemeberOf().size() + "," + oldRole.getMemeberOf().size());
        if (!Compare.getInstance().equalStrList(newRole.getMemeberOf(),oldRole.getMemeberOf()))
        {
            List<String> newList = newRole.getMemeberOf();
            List<String> oldList = oldRole.getMemeberOf();
            Collections.sort(newList);
            Collections.sort(oldList);
            for (int i = 0; i < newList.size(); i++)
            {
                String member = newList.get(i);
                boolean isExist = false;
                for (int j = 0; j < oldList.size(); j++)
                {
                    if (oldList.get(j).equals(member))//all the same
                    {
                        //oldList.remove(j);//this will effect the original member of list,so never remove
                        isExist = true;
                        break;
                    } else if (member.endsWith("(*)") && oldList.get(j).equals(member.replace("(*)", "")))//with admin option
                    {
                        sql.append(System.lineSeparator()).append("GRANT ").append(sc.getName(member.replace("(*)", "")))
                                .append(" TO ").append(newName).append(" WITH ADMIN OPTION").append(" ;");
                        isExist = true;
                        break;
                    } else if (oldList.get(j).equals(member + "(*)"))
                    {
                        sql.append(System.lineSeparator()).append("REVOKE ADMIN OPTION FOR ").append(sc.getName(member))
                                .append(" FROM ").append(newName).append(";");
                        isExist = true;
                        break;
                    }
                }
                if (!isExist)
                {
                    if (member.endsWith("(*)"))
                    {
                        sql.append(System.lineSeparator()).append("GRANT ").append(sc.getName(member.replace("(*)", "")))
                                .append(" TO ").append(newName).append(" WITH ADMIN OPTION").append(" ;");
                    } else
                    {
                        sql.append(System.lineSeparator()).append("GRANT ").append(sc.getName(member))
                                .append(" TO ").append(newName).append(" ;");
                    }
                }
            }
            for (int k = 0; k < oldList.size(); k++)
            {
                String oldmember = oldList.get(k);
                boolean isExist = false;
                for (String x : newList)
                {
                    if (x.equals(oldmember)
                            || x.equals(oldmember + "(*)")
                            || (oldmember.endsWith("(*)") && x.equals(oldmember.replace("(*)", ""))))
                    {
                        isExist = true;
                        break;
                    }
                }
                if (!isExist)
                {
                    sql.append(System.lineSeparator()).append("REVOKE ").append(sc.getName(oldmember.replace("(*)", "")))
                            .append(" FROM ").append(newName).append(";");
                }
            }
        }
        if (!Compare.getInstance().equalVariableList(newRole.getVariableList(),oldRole.getVariableList()))
        {
           sql.append(this.getVariableSQL(newRole.getType(), newName, oldRole.getVariableList(), newRole.getVariableList()));
        }
        if (!newRole.getComment().equals(oldRole.getComment())
                && !(newRole.getComment().isEmpty() && oldRole.getComment() == null))
        {
            sql.append(this.getCommentSQL(newRole.getType(), newRole.getComment(), newName, false));
        }
        logger.info(sql.toString());
        return sql.toString();
    }
    public String getDBChangeSQL(DBInfoDTO olddb, DBInfoDTO newdb)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String newName = sc.getName(newdb.getName());
        String oldName = sc.getName(olddb.getName());
        
        if (!newdb.getName().equals(olddb.getName()))
        {
            sql.append(this.getRenameSQL(newdb.getType(), oldName, newName));
        }
        if (!newdb.getOwner().equals(olddb.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newdb.getType(), newdb.getOwner(), newName));
        }
        if (!newdb.getTablespace().equals(olddb.getTablespace()))
        {
            sql.append(System.lineSeparator()).append("ALTER DATABASE ").append(newName)
                    .append(" SET TABLESPACE ").append(sc.getName(newdb.getTablespace())).append(";");
        }
        if(!newdb.getConnectionLimit().equals(olddb.getConnectionLimit()))
        {
            sql.append(System.lineSeparator()).append("ALTER DATABASE ").append(newName)
                    .append(" WITH CONNECTION LIMIT = ").append(newdb.getConnectionLimit()).append(";");
        }
        if (!Compare.getInstance().equalVariableList(newdb.getVariableList(),olddb.getVariableList()))
        {
           sql.append(this.getVariableSQL(newdb.getType(), newName, olddb.getVariableList(), newdb.getVariableList()));
        }
        logger.info("****************** ACL Compare:" + newdb.getAcl()+ "," + olddb.getAcl());
        if (!newdb.getAcl().equals(olddb.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL(newdb.getType().toString(), newdb.getAcl(), newName));
        }                
        if (!newdb.getComment().equals(olddb.getComment())
                && !(newdb.getComment().isEmpty() && olddb.getComment() == null))
        {
            sql.append(this.getCommentSQL(newdb.getType(), newdb.getComment(), newName, false));
        }
        
        return sql.toString();
    }
    public String getEventTriggerChangeSQL(EventTriggerInfoDTO oldOne, EventTriggerInfoDTO newOne)
    {
        StringBuilder sql = new StringBuilder();
        
        SyntaxController sc = SyntaxController.getInstance();
        String newName = sc.getName(newOne.getName());
        String oldName = sc.getName(oldOne.getName());
        
        if(!newOne.getFunc().equals(oldOne.getFunc())
                || !newOne.getEvent().equals(oldOne.getEvent())
                || !newOne.getWhenTag().equals(oldOne.getWhenTag()))
        {
            //create a new one(has new oid)
            sql.append(this.getDropSQL(oldOne, oldOne.getHelperInfo(), true) + "\n")
                    .append(getEventTriggerDefinition(newOne).toString());
        } else {
            if (!newOne.getName().equals(oldOne.getName())) {
                sql.append(this.getRenameSQL(newOne.getType(), oldName, newName));
            }
            if (!newOne.getEnableWhat().equals(oldOne.getEnableWhat())) {
                sql.append(getEventTriggerEnableAlterSQL(newOne, newName));
            }
            if (!newOne.getOwner().equals(oldOne.getOwner())) {
                sql.append(this.getOwnerAlterSQL(newOne.getType(), newOne.getOwner(), newName));
            }
            if (!newOne.getComment().equals(oldOne.getComment())
                    && !(newOne.getComment().isEmpty() && oldOne.getComment() == null)) {
                sql.append(this.getCommentSQL(newOne.getType(), newOne.getComment(), newName, false));
            }
        }
        return sql.toString();
    }
    public String getSchemaChangeSQL(SchemaInfoDTO oldSchema, SchemaInfoDTO newSchema)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String newName = sc.getName(newSchema.getName());
        String oldName = sc.getName(oldSchema.getName());
        
        if (!newSchema.getName().equals(oldSchema.getName()))
        {
            sql.append(this.getRenameSQL(newSchema.getType(), oldName, newName));
        }
        if (!newSchema.getOwner().equals(oldSchema.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newSchema.getType(), newSchema.getOwner(), newName));
        }                
        if (!newSchema.getAcl().equals(oldSchema.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL(newSchema.getType().toString(), newSchema.getAcl(), newName));
        }        
        if (!newSchema.getComment().equals(oldSchema.getComment())
                && !(newSchema.getComment().isEmpty() && oldSchema.getComment() == null))
        {
            sql.append(this.getCommentSQL(newSchema.getType(), newSchema.getComment(), newName, false));
        }
        return sql.toString();
    }
    public String getSequenceChangeSQL(SequenceInfoDTO oldseq, SequenceInfoDTO newseq)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newseq.getHelperInfo().getSchema());
        String newName = sc.getName(newseq.getName());
        String oldName =  sc.getName(oldseq.getName());
        
        if (!newseq.getName().equals(oldseq.getName()))
        {
            sql.append(this.getRenameSQL(newseq.getType(), schema + "." + oldName, newName));
        }
       if (!newseq.getOwner().equals(oldseq.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newseq.getType(), newseq.getOwner(), schema + "." + newName));
        }
        if (!newseq.getStrCurrent().equals(oldseq.getStrCurrent()))
        {
            sql.append(System.lineSeparator()).append(" SELECT setval('").append(schema + "." + newName)
                    .append("',").append(newseq.getStrCurrent()).append(", true);");
        }
        if (!newseq.getStrIncrement().equals(oldseq.getStrIncrement())
                || !newseq.getStrMinimum().equals(oldseq.getStrMinimum())
                || !newseq.getStrMaximum().equals(oldseq.getStrMaximum())
                || !newseq.getStrCache().equals(oldseq.getStrCache())
                || newseq.isCycled() != oldseq.isCycled())
        {
            sql.append(System.lineSeparator()).append("ALTER SEQUENCE ").append(schema + "." + newName);
            if (!newseq.getStrIncrement().equals(oldseq.getStrIncrement()))
            {
                sql.append("\n").append(" INCREMENT ").append(newseq.getStrIncrement());
            }
            if (!newseq.getStrMinimum().equals(oldseq.getStrMinimum()))
            {
                sql.append("\n").append(" MINVALUE ").append(newseq.getStrMinimum());
            }
            if (!newseq.getStrMaximum().equals(oldseq.getStrMaximum()))
            {
                sql.append("\n").append(" MAXVALUE ").append(newseq.getStrMaximum());
            }
            if (!newseq.getStrCache().equals(oldseq.getStrCache()))
            {
                sql.append("\n").append(" CACHE ").append(newseq.getStrCache());
            }
            if (newseq.isCycled() != oldseq.isCycled())
            {
                sql.append("\n");
                if (!newseq.isCycled())
                {
                    sql.append(" NO ");
                }
                sql.append("CYCLE");
            }
            sql.append(";");
        }
        if (!newseq.getAcl().equals(oldseq.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL(newseq.getType().toString(), newseq.getAcl(), schema + "." + newName));
        }
        if (!newseq.getComment().equals(oldseq.getComment())
                && !(newseq.getComment().isEmpty() && oldseq.getComment() == null))
        {
            sql.append(this.getCommentSQL(newseq.getType(), newseq.getComment(), schema + "." + newName, false));
        }
        return sql.toString();
    }
    public String getViewChangeSQL(ViewInfoDTO oldview, ViewInfoDTO newview)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newview.getHelperInfo().getSchema());
        String newName = sc.getName(newview.getName());
        String oldName = sc.getName(oldview.getName());
        String type=this.getTypeFromEnum(newview.getType());
        
        if (!newview.getName().equals(oldview.getName()))
        {
            sql.append(this.getRenameSQL(newview.getType(), schema + "." + oldName, newName));
        }        
        if (!newview.getOwner().equals(oldview.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newview.getType(), newview.getOwner(), schema + "." + newName));
        }
        if (!newview.getDefinition().equals(oldview.getDefinition()))
        {
            sql.append(System.lineSeparator()).append("CREATE OR REPLACE ").append(type).append(" ")
                    .append(schema + "." + newName).append(" AS \n")
                    .append(newview.getDefinition()).append(";");
        }
        if (newview.isSecurityBarrier() != oldview.isSecurityBarrier())
        {
            sql.append(System.lineSeparator()).append("ALTER ").append(type).append(" ").append(schema + "." + newName)
                    .append("\n SET (security_barrier=").append(newview.isSecurityBarrier()).append(");");
        }
        if (newview.isMaterializedView())
        {
            if (!newview.getTablespace().equals(oldview.getTablespace()))
            {
                sql.append(" ").append(System.lineSeparator())
                        .append("ALTER MATERIALIZED VIEW ").append(schema + "." + newName)
                        .append("  SET TABLESPACE ").append(sc.getName(newview.getTablespace())).append(";");
            }
            if (!newview.getFillFactor().isEmpty() && !newview.getFillFactor().equals(oldview.getFillFactor()))
            {
                sql.append(" ").append(System.lineSeparator())
                        .append("ALTER MATERIALIZED VIEW ").append(schema + "." + newName)
                        .append(" SET (fillfactor=").append(oldview.getFillFactor()).append(");");
            }
            if (newview.isWithData() != oldview.isWithData())
            {
                sql.append(" ").append(System.lineSeparator())
                        .append("REFRESH MATERIALIZED VIEW ").append(schema + "." + newName)
                        .append(" WITH");
                if (!newview.isWithData())
                {
                    sql.append(" NO");
                }
                sql.append(" DATA;");
            }
            //IF
            AutoVacuumDTO newVInfo = newview.getAutoVacuumInfo();
            AutoVacuumDTO oldVInfo = oldview.getAutoVacuumInfo();
            //must in this oreder, because all arguments in newCinfo never null.
            if (!Compare.getInstance().equal(newVInfo, oldVInfo))
            {
                sql.append(" ").append(System.lineSeparator())
                        .append("ALTER MATERIALIZED VIEW ").append(schema + "." + newName);
                sql.append(this.getAutovacuumSQL(newVInfo, oldVInfo));
            }
        }
        if (!newview.getAcl().equals(oldview.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL(newview.getType().toString(), newview.getAcl(), schema + "." + newName));
        }
        if (!newview.getComment().equals(oldview.getComment())
                && !(newview.getComment().isEmpty() && oldview.getComment() == null))
        {
            sql.append(this.getCommentSQL(newview.getType(), newview.getComment(), schema + "." + newName, false));
        }
        return sql.toString();
    }
    private String getAutovacuumSQL(AutoVacuumDTO newa, AutoVacuumDTO olda)
    {
        StringBuilder sql = new StringBuilder();
        if (newa.isCustomAutoVacuum() != olda.isCustomAutoVacuum() && newa.isCustomAutoVacuum() == false)
        {
            sql.append(" RESET(")
                    .append(System.lineSeparator()).append("  autovacuum_enabled,")
                    .append(System.lineSeparator()).append("  autovacuum_vacuum_threshold,")
                    .append(System.lineSeparator()).append("  autovacuum_analyze_threshold,")
                    .append(System.lineSeparator()).append("  autovacuum_vacuum_scale_factor,")
                    .append(System.lineSeparator()).append("  autovacuum_analyze_scale_factor,")
                    .append(System.lineSeparator()).append("  autovacuum_vacuum_cost_delay,")
                    .append(System.lineSeparator()).append("  autovacuum_vacuum_cost_limit,")
                    .append(System.lineSeparator()).append("  autovacuum_freeze_min_age,")
                    .append(System.lineSeparator()).append("  autovacuum_freeze_max_age,")
                    .append(System.lineSeparator()).append("  autovacuum_freeze_table_age")
                    .append(System.lineSeparator()).append(");");
        } else if (newa.isCustomAutoVacuum() == true)
        {
            sql.append(" SET(");
            if (newa.isEnable() != olda.isEnable())
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_enabled=").append(newa.isEnable()).append(",");
            }
            if(!newa.getVacuumBaseThreshold().equals(olda.getVacuumBaseThreshold())
                    &&!(newa.getVacuumBaseThreshold().isEmpty() && olda.getVacuumBaseThreshold()==null))
            {
                 sql.append(System.lineSeparator())
                        .append("  autovacuum_vacuum_threshold=").append(newa.getVacuumBaseThreshold()).append(",");
            }
            if(!newa.getAnalyzeBaseThreshold().equals(olda.getAnalyzeBaseThreshold())
                    && !(newa.getAnalyzeBaseThreshold().isEmpty() && olda.getAnalyzeBaseThreshold()==null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_analyze_threshold=").append(newa.getAnalyzeBaseThreshold()).append(",");
            }
            if (!newa.getVacuumScaleFactor().equals(olda.getVacuumScaleFactor())
                    && !(newa.getVacuumScaleFactor().isEmpty() && olda.getVacuumScaleFactor() == null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_vacuum_scale_factor=").append(newa.getVacuumScaleFactor()).append(",");
            }
            if (!newa.getAnalyzeScaleFactor().equals(olda.getAnalyzeScaleFactor())
                    && !(newa.getAnalyzeScaleFactor().isEmpty() && olda.getAnalyzeScaleFactor()==null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_analyze_scale_factor=").append(newa.getAnalyzeScaleFactor()).append(",");
            }
            if (!newa.getVacuumCostDelay().equals(olda.getVacuumCostDelay())
                    && !(newa.getVacuumCostDelay().isEmpty() && olda.getVacuumCostDelay() == null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_vacuum_cost_delay=").append(newa.getVacuumCostDelay()).append(",");
            }
            if (!newa.getVacuumCostLimit().equals(olda.getVacuumCostLimit())
                    && !(newa.getVacuumCostLimit().isEmpty() && olda.getVacuumCostLimit()==null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_vacuum_cost_limit=").append(newa.getVacuumCostLimit()).append(",");
            }
            if (!newa.getFreezeMinimumAge().equals(olda.getFreezeMinimumAge())
                    && !(newa.getFreezeMinimumAge().isEmpty() && olda.getFreezeMinimumAge() == null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_freeze_min_age=").append(newa.getFreezeMinimumAge()).append(",");
            }
            if (!newa.getFreezeMaximumAge().equals(olda.getFreezeMaximumAge())
                    && !(newa.getFreezeMaximumAge().isEmpty() && olda.getFreezeMaximumAge() == null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_freeze_max_age=").append(newa.getFreezeMaximumAge()).append(",");
            }
            if (!newa.getFreezeTableAge().equals(olda.getFreezeTableAge())
                    && !(newa.getFreezeTableAge().isEmpty() && olda.getFreezeTableAge() == null))
            {
                sql.append(System.lineSeparator())
                        .append("  autovacuum_freeze_table_age=").append(newa.getFreezeTableAge()).append(",");
            }
            if (sql.toString().endsWith(","))
            {
                sql.deleteCharAt(sql.length() - 1);
            }
            sql.append(System.lineSeparator()).append(");");
        }
        return sql.toString();
    }
    public String getFuncProcChangeSQL(FunctionInfoDTO oldfun, FunctionInfoDTO newfun)
    {
        logger.debug(newfun.getType());
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newfun.getHelperInfo().getSchema());
        String newName = sc.getName(newfun.getSimpleName());
        String oldName =oldfun.getName();//checked full name
        
        if (!newfun.getSimpleName().equals(oldfun.getSimpleName()))
        {
            sql.append(this.getRenameSQL(newfun.getType(), schema + "." + oldName, newName));
        }
        
        String newfullname =  schema + "." + newfun.getName();
        if (!newfun.getOwner().equals(oldfun.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newfun.getType(), newfun.getOwner(),newfullname));
        }
        
        if (TreeEnum.TreeNode.FUNCTION == newfun.getType())
        {
            if (!Compare.getInstance().equals(newfun.getLanguage(), oldfun.getLanguage())
                    || (newfun.getLanguage().equals("c") && oldfun.getLanguage().equals(newfun.getLanguage()) && !Compare.getInstance().equals(newfun.getBin(), oldfun.getBin()))
                    || (!newfun.getLanguage().equals("c") && oldfun.getLanguage().equals(newfun.getLanguage()) && !Compare.getInstance().equals(newfun.getSrc(), oldfun.getSrc()))
                    || !Compare.getInstance().equals(newfun.getReturnType(), oldfun.getReturnType())
                    || !Compare.getInstance().equals(newfun.getVolatiles(), oldfun.getVolatiles())
                    || !Compare.getInstance().equals(newfun.getParallel(), oldfun.getParallel())
                    || newfun.isStrict() != oldfun.isStrict()
                    || newfun.isSecurityDefiner() != oldfun.isSecurityDefiner()
                    || newfun.isLeafProof() != oldfun.isLeafProof()
                    || newfun.getCost() != oldfun.getCost()
                    || !Compare.getInstance().equalFuncArgList(newfun.getArgList(), oldfun.getArgList()))
            {
                logger.debug(newfun.printDebug());
                logger.debug(oldfun.printDebug());
                sql.append(" ").append(System.lineSeparator())
                        .append(this.getFuncProcCreateSQL(newfun));
            }
        }else
        {
            if (!Compare.getInstance().equals(newfun.getLanguage(), oldfun.getLanguage())
                    || (newfun.getLanguage().equals("c") && oldfun.getLanguage().equals(newfun.getLanguage()) && !Compare.getInstance().equals(newfun.getBin(), oldfun.getBin()))
                    || (!newfun.getLanguage().equals("c") && oldfun.getLanguage().equals(newfun.getLanguage()) && !Compare.getInstance().equals(newfun.getSrc(), oldfun.getSrc()))
                    //|| !Compare.getInstance().equals(newfun.getVolatiles(), oldfun.getVolatiles())
                    //|| !Compare.getInstance().equals(newfun.getParallel(), oldfun.getParallel())
                    //|| newfun.isStrict() != oldfun.isStrict()
                    || newfun.isSecurityDefiner() != oldfun.isSecurityDefiner()
                    //|| newfun.isLeafProof() != oldfun.isLeafProof()
                    //|| newfun.getCost() != oldfun.getCost()
                    || !Compare.getInstance().equalFuncArgList(newfun.getArgList(), oldfun.getArgList()))
            {
                logger.debug(newfun.printDebug());
                logger.debug(oldfun.printDebug());
                
                sql.append(" ").append(System.lineSeparator())
                        .append(this.getFuncProcCreateSQL(newfun));
            }
        }
        
        if (!Compare.getInstance().equalVariableList(newfun.getVariableList(), oldfun.getVariableList()))
        {
            sql.append(this.getVariableSQL(newfun.getType(),
                    newfullname,
                    oldfun.getVariableList(), newfun.getVariableList()));
        }
        
        if (!Compare.getInstance().equals(newfun.getAcl(), oldfun.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL(newfun.getType().toString(), newfun.getAcl(), newfullname));
        }
        
        if (!Compare.getInstance().equals(newfun.getComment(), oldfun.getComment()))
        {
            sql.append(this.getCommentSQL(newfun.getType(), newfun.getComment(), newfullname,false));
        }
        return sql.toString();
    }
    public String getColumnChangeSQL(ColumnInfoDTO oldc, ColumnInfoDTO newc)
    {
        if (Compare.getInstance().equal(newc, oldc))
        {
            return "";
        }
        
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(newc.getHelperInfo().getSchema()) + "." + sc.getName(newc.getHelperInfo().getRelation());
        String newName = sc.getName(newc.getName());
        String oldName = sc.getName(oldc.getName());
        
        if (!newc.getName().equals(oldc.getName()))
        {
            sql.append(this.getRenameSQL(newc.getType(), parent, oldName, newName));
        }
        logger.info(newc.getDatatype().getFullname() + "********" + oldc.getDatatype().getFullname());
        if (!newc.getDatatype().getFullname().equals(oldc.getDatatype().getFullname())
                || !newc.getCollation().equals(oldc.getCollation()))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(parent).append(System.lineSeparator())
                    .append(" ALTER COLUMN ").append(newName)
                    .append(" TYPE ").append(newc.getDatatype().getFullname());
            if (!newc.getCollation().equals(oldc.getCollation()))
            {
                sql.append(" COLLATE ").append(newc.getCollation());
            }
            sql.append(";");
        }
        if (!newc.getDefaultValue().equals(oldc.getDefaultValue()) && !(newc.getDefaultValue().isEmpty() && oldc.getDefaultValue()==null))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(parent).append(System.lineSeparator())
                    .append(" ALTER COLUMN ").append(newName);
            if (newc.getDefaultValue().isEmpty())
            {
                sql.append(" DROP DEFAULT");
            } else
            {
                sql.append(" SET DEFAULT '").append(newc.getDefaultValue()).append("'::").append(newc.getDatatype());
            }
            sql.append(" ;");
        }
        if (newc.isNotNull() != oldc.isNotNull())
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(parent).append(System.lineSeparator())
                    .append(" ALTER COLUMN ").append(newName);
            if (newc.isNotNull())
            {
                sql.append(" SET NOT NULL");
            } else
            {
                sql.append(" DROP NOT NULL");
            }
            sql.append(";");
        }
        if (!newc.getStatistics().equals(oldc.getStatistics())
                && !newc.getStatistics().isEmpty())
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(parent).append(System.lineSeparator())
                    .append(" ALTER COLUMN ").append(newName)
                    .append(" SET STATISTICS ").append(newc.getStatistics()).append(";");
        }
        if (!newc.getStorage().equals(oldc.getStorage()) && !newc.getStorage().isEmpty() )
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(parent).append(System.lineSeparator())
                    .append(" ALTER COLUMN ").append(newName)
                    .append(" SET STORAGE ").append(newc.getStorage()).append(";");
        }
        //variable
        logger.debug("**************" + Compare.getInstance().equalVariableList(newc.getVariableInfoList(), oldc.getVariableInfoList()));
        if (!Compare.getInstance().equalVariableList(newc.getVariableInfoList(), oldc.getVariableInfoList()))
        {
            sql.append(this.getVariableSQL(TreeEnum.TreeNode.COLUMN, parent, newName,
                    oldc.getVariableInfoList(), newc.getVariableInfoList()));
        }        
        //privilege
        if (!newc.getAcl().equals(oldc.getAcl()))
        {
            sql.append(this.getPrivilegeSQLFromACL4Column("COLUMN", newc.getAcl(), newName,parent));
        }        
        //comment 
        if (!newc.getComment().equals(oldc.getComment())
                && !(newc.getComment().isEmpty() && oldc.getComment() == null))
        {
            sql.append(this.getCommentSQL(newc.getType(), newc.getComment(), newName, parent, false));
        }
        return sql.toString();
    }
    public String getConstraintChangeSQL(ConstraintInfoDTO oldc, ConstraintInfoDTO newc)
    {
        if (Compare.getInstance().equal(newc, oldc))
        {
            return "";
        }
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newc.getHelperInfo().getSchema());
        String parent = schema + "." + sc.getName(newc.getHelperInfo().getRelation());
        String newName = sc.getName(newc.getName());
        String oldName = sc.getName(oldc.getName());
        StringBuilder sql = new StringBuilder();        
        if (!newc.getName().equals(oldc.getName()))
        {
            sql.append(this.getRenameSQL(newc.getType(), parent, oldName, newName));
        }
        switch(newc.getType())
        {
            case PRIMARY_KEY:
            case UNIQUE:
                //tablespace
                //fillfactor
                //index
            case EXCLUDE:
                //tablespace
                //fillfactor
                //constraint
                if (newc.getTablespace() != null && oldc.getTablespace() != null
                        && !newc.getTablespace().equals(oldc.getTablespace()))
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append("ALTER INDEX ").append(schema).append(".").append(newName)
                            .append(" SET TABLESPACE ").append(newc.getTablespace()).append(";");
                }                
                if (!newc.getFillFactor().isEmpty() 
                        && !newc.getFillFactor().equals(oldc.getFillFactor()))
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append("ALTER INDEX ").append(schema).append(".").append(newName)
                            .append(" SET (FILLFACTOR=").append(newc.getFillFactor()).append(");");
                }
               break;
            case CHECK:
            case FOREIGN_KEY:
                //validate
                if (newc.isValidate() != oldc.isValidate()
                        && newc.isValidate() == true)
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append("ALTER TABLE ").append(parent)
                            .append(" VALIDATE CONSTRAINT ").append(newName).append(";");
                }
                break;
        }

        if (!newc.getComment().equals(oldc.getComment())
                && !(newc.getComment().isEmpty() && oldc.getComment() == null))
        {
            sql.append(this.getCommentSQL(newc.getType(), newc.getComment(), newName, parent, false));
        }
        return sql.toString();
    }
    public String getIndexChangeSQL(IndexInfoDTO oldi, IndexInfoDTO newi)//only for table
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newi.getHelperInfo().getSchema());
        String newName = sc.getName(newi.getName());
        String oldName = sc.getName(oldi.getName());
        
        if (!newi.getName().equals(oldi.getName()))
        {
            sql.append(this.getRenameSQL(newi.getType(), schema + "." + oldName,  newName));
        }
        if (!newi.getTablespace().equals(oldi.getTablespace()))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER INDEX ").append(schema).append(".").append(newName)
                    .append(" SET TABLESPACE ").append(newi.getTablespace()).append(";");
        }
        if (!newi.getFillFactor().isEmpty() && !newi.getFillFactor().equals(oldi.getFillFactor()))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER INDEX ").append(schema).append(".").append(newName)
                    .append(" SET(fillfactor=").append(newi.getFillFactor()).append(");");
        }
        if (newi.isClustered() != oldi.isClustered())
        {
            String table = sc.getName(newi.getHelperInfo().getRelation());
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(schema).append(".").append(table);
            if (newi.isClustered())
            {
                sql.append(" CLUSTER ON ").append(newName).append(";");
            } else
            {
                sql.append(" SET WITHOUT CLUSTER;");
            }
        }
        if (!newi.getComment().equals(oldi.getComment())
                && !(newi.getComment().isEmpty() && oldi.getComment() == null))
        {
            sql.append(this.getCommentSQL(newi.getType(), newi.getComment(), newName, false));
        }
        return sql.toString();
    }
    public String getRuleChangeSQL(RuleInfoDTO oldr, RuleInfoDTO newr)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String parent = sc.getName(newr.getHelperInfo().getSchema())
                + "." + sc.getName(newr.getHelperInfo().getRelation());
        String newName = sc.getName(newr.getName());
        
        if (!newr.getEvent().equals(oldr.getEvent())
                || newr.isDoInstead() != oldr.isDoInstead()
                || (!newr.getWhereCondition().equals(oldr.getWhereCondition()) && !(newr.getWhereCondition().isEmpty() && oldr.getWhereCondition() == null))
                || (!newr.getDoStatement().equals(oldr.getDoStatement()) && !(newr.getDoStatement().isEmpty() && oldr.getDoStatement() == null)))
        {
            sql.append(this.getRuleCreateSQL(parent, newName, newr));
        }
        if (!newr.getComment().equals(oldr.getComment())
                && !(newr.getComment().isEmpty() && oldr.getComment() == null))
        {
            sql.append(this.getCommentSQL(newr.getType(), newr.getComment(), newName,  parent, false));
        }
        return sql.toString();
    }
    public String getTriggerChangeSQL(TriggerInfoDTO oldt, TriggerInfoDTO newt)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newt.getHelperInfo().getSchema());
        String parent = sc.getName(newt.getHelperInfo().getRelation());
        String newName = sc.getName(newt.getName());
        String oldName = sc.getName(oldt.getName());
        
        if (!newt.getName().equals(oldt.getName()))
        {
            sql.append(this.getRenameSQL(newt.getType(), schema + "." + parent, oldName, newName));
        }
        if (!newt.getComment().equals(oldt.getComment())
                && !(newt.getComment().isEmpty() && oldt.getComment() == null))
        {
            sql.append(this.getCommentSQL(newt.getType(), newt.getComment(), schema + "." + newName, false));
        }
        return sql.toString();
    }
    //for table
    private boolean findObjItem(ObjItemInfoDTO obj, List<ObjItemInfoDTO> objlist)
    {
        boolean state = false;//new 
        for (ObjItemInfoDTO o : objlist)
        {
            if (o.getOid() == obj.getOid())
            {
                state = true;
                break;
            }
        }
        return state;
    }
    private ColumnInfoDTO findColumn(ColumnInfoDTO col, List<ColumnInfoDTO> list)
    {
        for (ColumnInfoDTO o : list)
        {
            if (o.getOid() == col.getOid())
            {
                return o;
            }
        }
        return null;
    }
    private ConstraintInfoDTO findConstraint(ConstraintInfoDTO constr, List<ConstraintInfoDTO> list)
    {
        for (ConstraintInfoDTO c : list)
        {
            if (c.getOid() == constr.getOid())
            {
                return c;
            }
        }
        return null;
    }
    public String getTableChangeSQL(TableInfoDTO oldt ,TableInfoDTO newt)
    {
        StringBuilder sql = new StringBuilder();
        SyntaxController sc = SyntaxController.getInstance();
        String schema = sc.getName(newt.getHelperInfo().getSchema());
        String newName = sc.getName(newt.getName());
        String oldName = sc.getName(oldt.getName());
        
        //rename
        if (!newt.getName().equals(oldt.getName()))
        {
            sql.append(this.getRenameSQL(newt.getType(), schema + "." + oldName, newName));
        }
        //owner
        if (!newt.getOwner().equals(oldt.getOwner()))
        {
            sql.append(this.getOwnerAlterSQL(newt.getType(), newt.getOwner(), schema + "." + newName));
        }
        if (!newt.getTablespace().equals(oldt.getTablespace()))
        {
            sql.append(System.lineSeparator()).append("ALTER TABLE ").append(schema + "." + newName)
                    .append(" SET TABLESPACE ").append(sc.getName(newt.getTablespace())).append(";");
        }
        //fillfactor
        if (!newt.getFillFactor().isEmpty() && !newt.getFillFactor().equals(oldt.getFillFactor()))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(schema).append(".").append(newName)
                    .append(" SET(fillfactor=").append(newt.getFillFactor()).append(");");
        }
        //with oid
        if (newt.isHasOID() != oldt.isHasOID())
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(schema).append(".").append(newName)
                    .append(" SET ");
            if (newt.isHasOID())
            {
                sql.append("WITH");
            } else
            {
                sql.append("WITHOUT");
            }
            sql.append(" OIDs;");
        }        
        //inherit table
        if (!Compare.getInstance().equalObjItemList(newt.getInheritTableList(), oldt.getInheritTableList()))
        {
            for (ObjItemInfoDTO obj : newt.getInheritTableList())
            {
                if (!this.findObjItem(obj, oldt.getInheritTableList()))
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append("ALTER TABLE ").append(schema).append(".").append(newName)
                            .append(" INHERIT ").append(obj.toString()).append(";");
                }
            }
            for (ObjItemInfoDTO obj : oldt.getInheritTableList())
            {
                if (!this.findObjItem(obj, newt.getInheritTableList()))
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append("ALTER TABLE ").append(schema).append(".").append(newName)
                            .append(" NO INHERIT ").append(obj.toString()).append(";");
                }
            }
        }        
        //like table, cannot be changed 
        //column
        List<ColumnInfoDTO> newColumns = newt.getColumnInfoList();
        List<ColumnInfoDTO> oldColumns = oldt.getColumnInfoList();
        if (!Compare.getInstance().equalColumnList(newColumns, oldColumns))
        {
            for (ColumnInfoDTO column : newColumns)
            {
                ColumnInfoDTO theOldCol = this.findColumn(column, oldColumns);
                if (theOldCol == null)
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append(this.getColumnAddDefinition(column,newt.getHelperInfo()));
                } else
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append(this.getColumnChangeSQL( theOldCol, column));
                }
            }
            for (ColumnInfoDTO column : oldColumns)
            {
                ColumnInfoDTO theNewCol = this.findColumn(column, newColumns);
                if (theNewCol == null)
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append(this.getDropSQL(column, newt.getHelperInfo(), false));
                }
            }
        }
        //constraint
        List<ConstraintInfoDTO> newConstrList = newt.getConstraintInfoList();
        List<ConstraintInfoDTO> oldConstrList = oldt.getConstraintInfoList();
        if (!Compare.getInstance().equalConstraintList(newConstrList, oldConstrList))
        {
            for (ConstraintInfoDTO constr : newConstrList)
            {
                if (this.findConstraint(constr, oldConstrList) == null)
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append(this.getConstraintAddDefinition(constr, newt.getHelperInfo()));
                } 
            }
            for (ConstraintInfoDTO constr : oldConstrList)
            {
                if (this.findConstraint(constr, newConstrList) == null)
                {
                    sql.append(" ").append(System.lineSeparator())
                            .append(this.getDropSQL(constr, newt.getHelperInfo(), false));
                }
            }
        }
        //autovacuum
        AutoVacuumDTO newVInfo = newt.getAutoVacuumInfo();
        AutoVacuumDTO oldVInfo = oldt.getAutoVacuumInfo();
        //must in this order, because all arguments in newCinfo never null.
        if (!Compare.getInstance().equal(newVInfo, oldVInfo))
        {
            sql.append(" ").append(System.lineSeparator())
                    .append("ALTER TABLE ").append(schema).append(".").append(newName);
            sql.append(this.getAutovacuumSQL(newVInfo, oldVInfo));
        }
        //privilege
        if (!newt.getAcl().equals(oldt.getAcl())
                && !(newt.getAcl().isEmpty() && oldt.getAcl() == null))
        {
            sql.append(this.getPrivilegeSQLFromACL(newt.getType().toString(), newt.getAcl(), schema + "." + newName));
        }        
        //comment
        if (!newt.getComment().equals(oldt.getComment())
                && !(newt.getComment().isEmpty() && oldt.getComment() == null))
        {
            sql.append(this.getCommentSQL(newt.getType(), newt.getComment(), schema + "." + newName, false));
        }
        //partition 
        if(newt.getPartitionList() != null)
        {
            //drop old
            for (PartitionInfoDTO oldp : oldt.getPartitionList())
            {
                if(!findPartition(oldp, newt.getPartitionList()))
                {
                    sql.append(" ").append(System.lineSeparator())
                           .append(this.getDropSQL(oldp, oldp.getHelperInfo(), false));
                }   
            }
            //add new 
            for (PartitionInfoDTO newp : newt.getPartitionList())
            {
                if(!findPartition(newp,oldt.getPartitionList()))
                {
                    sql.append(" ").append(System.lineSeparator())
                           .append(getPartitionDefinition(newp));
                }   
            }
            
        }
        
        return sql.toString();
    }
    
    private boolean findPartition(PartitionInfoDTO newp, List<PartitionInfoDTO> list)
    {
        if(list==null || list.isEmpty())
        {
            return false;
        }
        for(PartitionInfoDTO oldp : list)
        {
            if(newp.getName().equals(oldp.getName()))
            {
                return true;
            }
        }
        return false;
    }
    
    //for new a column for table creation
    public String getPartColumnDefine(ColumnInfoDTO columnInfo)
    {
        StringBuilder define = new StringBuilder();
        define.append(columnInfo.getDatatype().getFullname());
        if (columnInfo.getCollation() != null && !columnInfo.getCollation().isEmpty())
        {
            define.append(" COLLATE ").append(columnInfo.getCollation());
        }
        if (columnInfo.isNotNull())
        {
            define.append(" NOT NULL");
        }
        if (columnInfo.getDefaultValue() != null && !columnInfo.getDefaultValue().isEmpty())
        {
            define.append(" DEFAULT ").append(columnInfo.getDefaultValue()).append(" ");
        }
        logger.info("Column Part Define:" + define.toString());
        return define.toString();
    }
    //for new a constraint for table creation
    public String getPartContraintDefine(ConstraintInfoDTO constrInfo)
    {
        logger.info("Enter:" + constrInfo.getType());
        StringBuilder define = new StringBuilder();
        TreeEnum.TreeNode type = constrInfo.getType();
        switch (type)
        {
            case PRIMARY_KEY:
            case UNIQUE:
                if(constrInfo.getUsingIndex()!=null && !constrInfo.getUsingIndex().isEmpty())
                {
                    define.append(" USING INDEX ").append(constrInfo.getUsingIndex());
                } else
                {
                    define.append("(").append(constrInfo.getAllColumnDefine()).append(")");
                    if (constrInfo.getFillFactor() != null && !constrInfo.getFillFactor().isEmpty())
                    {
                        define.append("\n WITH(FILLFACTOR=").append(constrInfo.getFillFactor()).append(")");
                    }
                    if (constrInfo.getTablespace() != null
                            && !constrInfo.getTablespace().isEmpty()
                            && !(constrInfo.getTablespace().equals("<" + constBundle.getString("defaultTablespace") + ">")))
                    {
                        define.append("\n USING INDEX TABLESPACE ").append(constrInfo.getTablespace());
                    }
                    if (constrInfo.isDeferrable())
                    {
                        define.append("\n DEFERRABLE");
                        if (constrInfo.isDeferred())
                        {
                            define.append(" INITIALLY DEFERRED");
                        }
                    }
                }
                break;
            case FOREIGN_KEY:
                define.append("(").append(constrInfo.getAllColumnDefine()).append(")");         
                define.append("\n REFERENCES ").append(constrInfo.getAllRelColumnDefine());
                if (constrInfo.getMatchType() != null && !constrInfo.getMatchType().isEmpty())
                {
                    define.append("\n MATCH ").append(constrInfo.getMatchType());
                }
                define.append("\n ON UPDATE ").append(constrInfo.getUpdateActions())
                        .append(" ON DELETE ").append(constrInfo.getDeleteAction());
                if (constrInfo.isDeferrable())
                {
                    define.append(" DEFERRABLE");
                    if (constrInfo.isDeferred())
                    {
                        define.append(" INITIALLY DEFERRED");
                    }
                }
                if (!constrInfo.isValidate())
                {
                    define.append(System.lineSeparator()).append(" NOT VALID");
                }
                break;
            case EXCLUDE:
                if (constrInfo.getAccessMethod() != null && !constrInfo.getAccessMethod().isEmpty())
                {
                    define.append("\n USING ").append(constrInfo.getAccessMethod());
                }
                define.append("(").append(constrInfo.getAllColumnDefine()).append(")");
                if (constrInfo.getTablespace() != null
                        && !constrInfo.getTablespace().isEmpty()
                        && !(constrInfo.getTablespace().equals("<" + constBundle.getString("defaultTablespace") + ">")))
                {
                    define.append("\n USING INDEX TABLESPACE ").append(constrInfo.getTablespace());
                }
                if (constrInfo.isDeferrable())
                {
                    define.append("\n DEFERRABLE");
                    if (constrInfo.isDeferred())
                    {
                        define.append(" INITIALLY DEFERRED");
                    }
                }
                if (constrInfo.getConstraint() != null && !constrInfo.getConstraint().isEmpty())
                {
                    define.append("\n WITH（").append(constrInfo.getConstraint()).append(")");
                }
                break;
            case CHECK:
                define.append(constrInfo.getCheckConditonQuoted());
                if (constrInfo.isValidate())
                {
                    define.append(" NOT VALID");
                }
                if (constrInfo.isNoInherit())
                {
                    define.append(" NO INHERIT");
                }
                break;
        }
        logger.info("Return:Part Define = " + define.toString());
        return define.toString();
    }

    //get items for Object create
    public String[] getItemsArrary(HelperInfoDTO helperInfo, String type) throws SQLException, ClassNotFoundException
    {
        String[] array = new String[]{};
        if (helperInfo == null)
        {
            logger.error("connectInfo is null, do nothing, return.");
            return array;
        }
        if (type == null || type.isEmpty())
        {
            logger.error("type is null, do nothing, return.");
            return array;
        }
        
        StringBuilder sql = new StringBuilder();
        logger.info(type);
        switch (type)
        {
            case "tablespace":
                sql.append("SELECT t.spcname")
                        .append(" FROM pg_tablespace t")
                        .append(" WHERE t.spcname!='pg_global'")
                        .append(" ORDER BY t.spcname");
                break;
            case "owner":
                sql.append("SELECT rolname FROM pg_roles;");
                break;
            case "template":
                sql.append("SELECT datname FROM pg_database;");
                break;
            case "schema":
                sql.append("SELECT nspname FROM pg_namespace ")
                        .append(" WHERE  ")
                        .append(" nspname NOT IN('pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema'")
                        .append(" , 'dbms_alert', 'dbms_assert', 'dbms_output', 'dbms_pipe', 'dbms_random', 'dbms_utility'")
                        .append(" , 'oracle', 'plunit', 'plvchr', 'plvdate', 'plvlex', 'plvstr', 'plvsubst', 'utl_file' ) ")
                        .append(" AND nspname NOT LIKE '%_catalog'");
                       // .append(" order by oid asc");
                break;
            case "event_trigger_function"://function for event trigger
                sql.append("SELECT quote_ident(nspname) || '.' || quote_ident(proname) AS tfname")
                        .append(" FROM pg_proc p, pg_namespace n, pg_language l")
                        .append(" WHERE p.pronamespace = n.oid AND p.prolang = l.oid AND p.pronargs = 0 ")
                        .append(" AND l.lanname != 'sql' AND prorettype::regtype::text = 'event_trigger'\n")
                        .append(" ORDER BY nspname ASC, proname ASC");
                break;
        }
        logger.info(sql.toString());
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;        
        try
        {
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();            
            rs = stmt.executeQuery(sql.toString());
            List<String> list = new ArrayList();
            switch (type)
            {
                case "tablespace":
                    list.add("<" + constBundle.getString("defaultTablespace") + ">");
                    while (rs.next())
                    {
                        list.add(rs.getString(1));
                    }
                    break;
                case "owner":
                case "template":
                    list.add("");
                    while (rs.next())
                    {
                        list.add(rs.getString(1));
                    }
                    break;
                case "event_trigger_function":
                    while (rs.next())
                    {
                        list.add(rs.getString(1));
                    }
                    break;
                case "schema":
                    while (rs.next())
                    {
                        list.add(rs.getString(1));
                    }
                    break;
            }
            array = new String[list.size()];
            array = list.toArray(array);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
        return array;
    }    
    //get roles for object privilege
    public Object[] getRoles(HelperInfoDTO helperInfo) throws Exception
    {
        logger.info("enter");
        StringBuilder sql = new StringBuilder();
        sql.append("select 'public' union all")
                .append(" select r.rolname from pg_roles r")
                .append(" WHERE r.rolname NOT IN('syssao','syssso', 'sysdba')");// filter for sm privilege limit
                //.append(" where NOT r.rolcanlogin");
        List<String> list = this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString());
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    //get roles for role's member     
    public String[] getMemberRoles(HelperInfoDTO helperInfo, Long roleOid) throws Exception
    {
        logger.debug("Enter");
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT r.rolname ")
                .append(" FROM pg_roles r");
        if (roleOid == null || roleOid == 0)
        {
           //sql.append(" where NOT r.rolcanlogin");
            sql.append(" WHERE r.rolname NOT IN('sysdba', 'syssao', 'syssso');");//for gc
        } else
        {
            //sql.append(" LEFT OUTER JOIN pg_auth_members ON r.oid=roleid AND member=").append(roleOid)
                    //.append(" AND roleid IS NULL").append(" AND NOT r.rolcanlogin");
            sql.append(" WHERE r.oid <> ").append(roleOid).append(" AND r.rolname NOT IN('sysdba', 'syssao', 'syssso');");       
        }
        logger.info(sql.toString());
        List<String> list = this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString());
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    //for variable - tablesapce, database, role
    public SysVariableInfoDTO[] getVariables(HelperInfoDTO helperInfo, String objType) throws SQLException, ClassNotFoundException
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return null.");
            return null;
        }        
        String varCondition = null;
        switch (objType)
        {
            case "tablespace":
                varCondition = "WHERE name IN('random_page_cost','seq_page_cost')";
                break;
            case "role":
            case "database":
                varCondition = "WHERE context IN ('user', 'superuser')";
                break;
            default:
                logger.error("Never support for Variable of " + objType);
                return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT name, vartype, min_val, max_val, context, setting AS current_value, source AS parameterSource")
                .append(" FROM pg_settings ")
                .append(varCondition)
                .append(" ORDER BY lower(name);");
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());        
        SysVariableInfoDTO[] array = null;        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            //stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql.toString());
            List<SysVariableInfoDTO> list = new ArrayList();
            SysVariableInfoDTO var;
            if (objType.equals("role"))
            {
                var = new SysVariableInfoDTO();
                var.setName("role");
                var.setValueType("string");
                list.add(var);
            }
            while (rs.next())
            {
                var = new SysVariableInfoDTO();
                var.setName(rs.getString("name"));
                var.setValueType(rs.getString("vartype"));
                var.setMinValue(rs.getString("min_val"));
                var.setMaxValue(rs.getString("max_val"));
                var.setContext(rs.getString("context"));                
                list.add(var);
            }
            array = new SysVariableInfoDTO[list.size()];
            array = list.toArray(array);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return array;        
    }
    //for function - language
    public List<String> getLanguageList(HelperInfoDTO helperInfo) throws Exception
    {
        String sql = "SELECT DISTINCT lanname FROM pg_language";
        return getListFromQuery(helperInfo, helperInfo.getMaintainDB(), sql);
    }

    //For Table
    //the return tables is for table inherit, foreign key reference
    //this can be quoted, because needn't beselected
    public ObjItemInfoDTO[] getParentTables(HelperInfoDTO helperInfo) throws ClassNotFoundException,SQLException
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(c.relname) AS relname, quote_ident(n.nspname) AS nspname, c.oid ")
                .append(",(SELECT count(*) FROM pg_attribute WHERE attrelid=c.oid AND attnum>0) AS colscount")
                .append(" FROM pg_class c")
                .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                .append(" WHERE c.relkind = 'r'::\"char\" AND n.nspname NOT IN")
                .append(" ('pg_toast', 'pg_temp_1', 'pg_toast_temp_1','pg_catalog', ")
                .append(" 'hgdb_catalog', 'hgdb_oracle_catalog','hgdb_mysql_catalog', ")
                .append(" 'hgdb_mssql_catalog', 'hgdb_db2_catalog', 'information_schema')")
                .append(" ORDER BY n.nspname,c.relname");        
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());        
        ObjItemInfoDTO[] array = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;       
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            //stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql.toString());
            List<ObjItemInfoDTO> list = new ArrayList();
            ObjItemInfoDTO item;
            while (rs.next())
            {
                item = new ObjItemInfoDTO();
                item.setTable(rs.getString("relname"));
                item.setSchema(rs.getString("nspname"));
                item.setOid(rs.getLong("oid"));
                list.add(item);
            }
            array = new ObjItemInfoDTO[list.size()];
            array = list.toArray(array);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return array;
    }  
    //the return columns from parent table(inherit and foreign key reference table)
    public String[][] getInheritTableColumns(HelperInfoDTO helperInfo, Long oid) throws ClassNotFoundException,SQLException  
    {
         if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(pg_attribute.attname)")
                .append(",format_type(pg_type.oid,NULL) AS typname")//.append(",quote_ident(pg_type.typname)")
                .append(",quote_ident(pg_namespace.nspname)")
                .append(",quote_ident(pg_class.relname)")
                //.append(",pg_attrdef.adsrc AS defaultValue")
                //.append(",pg_attribute.attnotnull AS isNotNull ")
                //.append(",CASE WHEN(pg_attribute.attnum < 0) THEN true ELSE false END isSystemColumn")
                .append(" FROM pg_attribute ")
                .append(" INNER JOIN pg_class ON (pg_class.oid = pg_attribute.attrelid ")
                .append(" AND pg_class.relkind = 'r')")
                .append(" INNER JOIN pg_namespace on (pg_namespace.oid  = pg_class.relnamespace)")
                .append(" INNER JOIN pg_type ON (pg_type.oid = pg_attribute.atttypid")
                .append(" AND pg_type.typname NOT IN ('oid', 'tid', 'xid', 'cid'))")
                .append(" LEFT JOIN pg_attrdef ON (pg_attrdef.adrelid = pg_attribute.attrelid")
                .append(" AND pg_attrdef.adnum = pg_attribute.attnum)")
                .append(" WHERE pg_attribute.attisdropped = false")
                .append(" AND pg_class.oid ='").append(oid).append("'");
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());
        String[][] inheritColumn = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;      
        try
        {
            conn = JdbcHelper.getConnection(url,helperInfo);
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            //stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            rs.last();
            int i;
            int row = rs.getRow();
            inheritColumn = new String[row][10];//currently only deal first 3 columns 
            if (row > 0)
            {
                rs.first();
                inheritColumn[0][0] = rs.getString(1);
                inheritColumn[0][1] = rs.getString(2);
                inheritColumn[0][2] = rs.getString(3) + "." + rs.getString(4);
                i = 1;
                while (rs.next())
                {
                    inheritColumn[i][0] = rs.getString(1);
                    inheritColumn[i][1] = rs.getString(2);
                    inheritColumn[i][2] = rs.getString(3) + "." + rs.getString(4);
                    i++;
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return inheritColumn;
    }
    //the return tables is for table like
    public String[] getLikeTables(HelperInfoDTO helperInfo) throws Exception
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return null.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT '' UNION ALL ")
                .append(" (SELECT quote_ident(n.nspname) || '.' || quote_ident(c.relname) AS relname ")
                .append(" FROM pg_class c")
                .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                .append(" WHERE c.relkind IN ");
        if (this.isVersionHigherThan(helperInfo.getDbSys(), helperInfo.getVersionNumber(), 9, 1))
        {
            sql.append("('r', 'v', 'f')");
        } else
        {
            sql.append("('r')");
        }
        sql.append(" AND n.nspname NOT IN('pg_toast', 'pg_temp_1', 'pg_toast_temp_1','pg_catalog', ")
                .append(" 'hgdb_catalog', 'hgdb_oracle_catalog','hgdb_mysql_catalog', ")
                .append(" 'hgdb_mssql_catalog', 'hgdb_db2_catalog', 'information_schema') ")
                .append(" ORDER BY n.nspname,c.relname)");
        logger.info(sql.toString());
        List<String> list = this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString());
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    //the return tables is for table of type
    public ObjItemInfoDTO[] getOfTypeTabless(HelperInfoDTO helperInfo) throws ClassNotFoundException,SQLException
    {
        if (helperInfo == null)
        {
            logger.error("Error：connectInfo is null, do nothing, return null.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(n.nspname) AS nspname, quote_ident(t.typname) AS typname, t.oid ")
                .append(" FROM pg_type t, pg_namespace n ")
                .append(" WHERE t.typtype='c' AND t.typnamespace=n.oid ")
                .append(" AND NOT (n.nspname like 'pg_%' OR n.nspname Like '%_catalog' OR n.nspname='information_schema') ")
                .append(" ORDER BY n.nspname, t.typname");
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());
        
        ObjItemInfoDTO[] array = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;        
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            List<ObjItemInfoDTO> list = new ArrayList();
            list.add(null);
            ObjItemInfoDTO item;
            while(rs.next())
            {
                item = new ObjItemInfoDTO();
                item.setOid(rs.getLong("oid"));
                item.setTable(rs.getString("typname"));
                item.setSchema(rs.getString("nspname"));
                list.add(item);
            }
            array = new ObjItemInfoDTO[list.size()];
            array = list.toArray(array);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return array;
    } 
    public String[][] getOfTypeTableColumn(HelperInfoDTO helperInfo, Long oid) throws ClassNotFoundException, SQLException
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return null.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(a.attname) AS colname, format_type(atttypid, NULL) AS typname ")
                .append(" FROM pg_attribute a, pg_class c")
                .append(" WHERE NOT a.attisdropped AND a.attnum>0 ")
                .append(" AND a.attrelid=c.oid AND c.reltype=").append(oid);
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());
        
        String[][] ofTypeColumn = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null; 
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql.toString());
            rs.last();
            int row = rs.getRow();
            ofTypeColumn = new String[row][10];//currently only deal first 2 columns 
            if (row > 0)
            {
                rs.first();
                ofTypeColumn[0][0] = rs.getString(1);
                ofTypeColumn[0][1] = rs.getString(2);
                int i = 1;
                while (rs.next())
                {
                    ofTypeColumn[i][0] = rs.getString(1);
                    ofTypeColumn[i][1] = rs.getString(2);
                    i++;
                }
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return ofTypeColumn;
    }
    //datatype
    //for function return datatype type and datatype
    public DatatypeDTO[] getDataType4Function(HelperInfoDTO helperInfo) throws ClassNotFoundException,SQLException
    {
        //for function arg type
        String condition = "(typtype IN ('b', 'c', 'd', 'e', 'p', 'r')  AND typname NOT IN ('any', 'language_handler') )";// AND typname NOT IN ('any', 'trigger', 'language_handler', 'event_trigger'))
        //not show system object
        condition = condition + " AND nspname NOT LIKE E'pg\\\\_toast%' AND nspname NOT LIKE E'pg\\\\_temp%'";
        return this.getDataTypes(helperInfo, condition, true);
    }
    //column datatype addSerial=true,withDomain=true
    public DatatypeDTO[] getDataType(HelperInfoDTO helperInfo, boolean withDomain, boolean addSerials) throws ClassNotFoundException,SQLException
    {
        String condition = " typisdefined  AND typtype ";
        // We don't get pseudotypes here
        if (withDomain)
        {
            condition += "IN ('b', 'c', 'd', 'e', 'r')";
        } else
        {
            condition += "IN ('b', 'c', 'e', 'r')";
        }
        condition += "AND NOT EXISTS (select 1 from pg_class where relnamespace=typnamespace and relname = typname and relkind != 'c') "
                + " AND (typname not like '_%' OR NOT EXISTS (select 1 from pg_class where relnamespace=typnamespace and relname = substring(typname from 2)::name and relkind != 'c')) ";
        //not show system object
        condition += " AND nsp.nspname != 'information_schema'";
        return this.getDataTypes(helperInfo, condition, addSerials);
    }
    private DatatypeDTO[] getDataTypes(HelperInfoDTO helperInfo, String condition, boolean addSerials) throws ClassNotFoundException,SQLException
    {
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing, return.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ")
                .append(" (SELECT format_type(t.oid,NULL) AS typname, CASE WHEN typelem > 0 THEN typelem ELSE t.oid END as elemoid, typlen, typtype, t.oid, nspname,\n")
                .append(" (SELECT COUNT(1) FROM pg_type t2 WHERE t2.typname = t.typname) > 1 AS isdup, \n")
                .append(" CASE WHEN t.typcollation != 0 THEN true ELSE false END AS is_collatable ")
                .append(" FROM pg_type t\n")
                .append(" JOIN pg_namespace nsp ON typnamespace=nsp.oid\n")
                .append(" WHERE (NOT (typname = 'unknown' AND nspname IN('pg_catalog', 'oracle')) )\n")
                .append("  AND ").append(condition).append("\n");
        if (addSerials)
        {
            sql.append(" UNION SELECT 'smallserial', 0, 2, 'b', 0, 'pg_catalog', false, false\n");
            sql.append(" UNION SELECT 'bigserial', 0, 8, 'b', 0, 'pg_catalog', false, false\n");
            sql.append(" UNION SELECT 'serial', 0, 4, 'b', 0, 'pg_catalog', false, false\n");
            sql.append(" UNION SELECT 'float', 0, 4, 'b', 0, 'pg_catalog', false, false\n");//for gc
        }
        //sql.append("  ) AS dummy ORDER BY nspname <> 'pg_catalog', nspname <> 'public', nspname, 1");
        sql.append(" ) AS dummy ")
                .append(" ORDER BY nspname <> 'pg_catalog', nspname <> 'public', nspname, typname");
                //.append(" WHERE nspname NOT IN( 'pg_catalog', 'public')")
                //.append(" ORDER BY typname");
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());
        DatatypeDTO[] array = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = stmt.executeQuery(sql.toString());
            List<DatatypeDTO> list = new ArrayList();
            boolean withOracle = false;
            try {
                withOracle = Boolean.valueOf(OptionController.getInstance().getPropertyValue(
                        OptionController.getInstance().getOptionProperties(), "with_oracle_prefix"));
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
            }
            logger.debug("withOracle=" + withOracle);
            DatatypeDTO t;
            while (rs.next())
            {
                t = new DatatypeDTO();
                t.setOid(rs.getLong("elemoid"));
                t.setName(withOracle? rs.getString("typname") : rs.getString("typname").replace("oracle.", ""));
                
                //t.setHasArgs(rs.getInt("typlen")<0);//notwork
                //logger.debug(t.getName() + "|" + rs.getInt("typlen") + "|" + t.isHasArgs() );
                list.add(t);
            }   
            array = new DatatypeDTO[list.size()];
            array = list.toArray(array);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return array;
    }
    //for change column datatype
    public DatatypeDTO[] getAlternativeDatatypes(HelperInfoDTO helperInfo, DatatypeDTO originalType) throws SQLException, ClassNotFoundException
    {
        DatatypeDTO[] array = null;        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT tt.oid AS typeoid, format_type(tt.oid,NULL) AS typename, tt.typlen\n")
                .append("  FROM pg_cast\n")
                .append("  JOIN pg_type tt ON tt.oid=casttarget\n")
                .append("  WHERE castsource=").append(originalType.getOid()).append("\n")
                .append("  AND castcontext IN ('i', 'a')");
        logger.info(sql.toString());
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            List<DatatypeDTO> list = new ArrayList();
            list.add(originalType);
            DatatypeDTO t;
            while (rs.next())
            {
                t = new DatatypeDTO();
                t.setOid(rs.getLong("typeoid"));
                t.setName(rs.getString("typename"));
                //t.setHasArgs(rs.getInt("typlen")<0);
                list.add(t);
            }
            array = new DatatypeDTO[list.size()];
            array = list.toArray(array);
        } catch ( SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return array;
    }
    //column for table or view 
    public ColItemInfoDTO[] getColumnOfRelation(HelperInfoDTO helperInfo, Long reloid) throws ClassNotFoundException, SQLException
    {
        logger.info("Enter");
        if (helperInfo == null)
        {
            logger.error("Error：connectInfo is null, do nothing, return.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(att.attname) AS attname, att.atttypid, att.attnum")
                .append(" FROM pg_attribute att ")
                .append(" WHERE att.attnum > 0  AND att.attisdropped=false AND att.attrelid=")
                .append(reloid)
                .append(" ORDER BY att.attnum;");
        String url = helperInfo.getPartURL() + helperInfo.getDbName();
        logger.info(url);
        logger.info(sql.toString());   
        
        ColItemInfoDTO[] array = null;        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;       
        try
        {
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            List<ColItemInfoDTO> list = new ArrayList();
            ColItemInfoDTO item;
            while (rs.next())
            {
                item = new ColItemInfoDTO();
                item.setName(rs.getString("attname"));
                item.setTypeOid(rs.getLong("atttypid"));
                list.add(item);
            }
            array = new ColItemInfoDTO[list.size()];
            array = list.toArray(array);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return array;
    }
    //for trigger
    public String[] getTriggerFunctions(HelperInfoDTO helperInfo)throws ClassNotFoundException, SQLException
    {
        logger.info("Enter");
        if (helperInfo == null)
        {
            logger.error("Error：connectInfo is null, do nothing, return.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(nspname) ||'.'||quote_ident(pro.proname)")
                .append(" FROM pg_catalog.pg_proc pro")
                .append(" LEFT OUTER JOIN pg_namespace nsp ON(pro.pronamespace=nsp.oid) ")
                .append(" WHERE pro.prorettype=2279 ")
                .append(" AND nspname NOT LIKE '%_catalog'")
                .append(" ORDER BY pro.proname");
        List<String> list = this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString());
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    //for pkey and unique constarint
    public String[] getIndexOfTable(HelperInfoDTO helperInfo, Long taboid) throws ClassNotFoundException, SQLException
    {
        StringBuilder sql = new StringBuilder();
        sql.append("select quote_ident(relname)")
                .append(" from pg_index i")
                .append(" left outer join pg_class c on c.oid=indexrelid")
                .append(" where indrelid=").append(taboid);
        List<String> list = new ArrayList();
        list.add("");
        list.addAll(1, this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString()));
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    //not used until now
    private String[] getAccessMethod(HelperInfoDTO helperInfo) throws Exception
    {
        String sql = "select amname,oid from pg_am";
        List<String> list = new ArrayList();
        list.add("");
        list.addAll(1, this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString()));
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    public String[] getOperatorClassOfAccessMethod(HelperInfoDTO helperInfo, String accessMethod) throws  ClassNotFoundException, SQLException
    {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT opcname, opcmethod")
                //.append(",opcmethod, amname")
                .append(" FROM pg_opclass o")
                .append(" LEFT OUTER JOIN pg_am a on opcmethod=a.oid")
                .append(" WHERE amname='").append(accessMethod).append("'")
                .append(" AND NOT opcdefault")
                .append(" ORDER BY opcmethod");
        List<String> list = new ArrayList();
        list.add("");
        list.addAll(1, this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString()));
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    public String[] getOperatorOfType(HelperInfoDTO helperInfo, Long typeoid) throws  ClassNotFoundException, SQLException
    {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT DISTINCT oprname")
                //.append(",oprnamespace ")
                .append(" FROM pg_operator ")
                .append(" WHERE (oprleft=").append(typeoid).append(" OR oprright=").append(typeoid)
                .append(")  AND oprcom > 0")
                .append("ORDER BY oprname");
        logger.debug(sql.toString());
        List<String> list = new ArrayList();
        list.add("");
        list.addAll(1, this.getListFromQuery(helperInfo, helperInfo.getDbName(), sql.toString()));
        String[] array = new String[list.size()];
        array = list.toArray(array);
        return array;
    }
    //for other tool
    //backup - encoding list
    public String[] getEncodingList(HelperInfoDTO helperInfo)throws  ClassNotFoundException, SQLException
    {
        String[] encodings = null;
        List<String> encodingList = new ArrayList();
        encodingList.add("");
        Connection conn = null;
        PreparedStatement ppst = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL()+helperInfo.getDbName(), helperInfo);
            String sql = "SELECT pg_encoding_to_char(?) ";
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            int encodingNo = 0;
            String encoding = "";
            do
            {
                ppst.setInt(1, encodingNo);
                rs = ppst.executeQuery();
                while (rs.next())
                {
                    encoding = rs.getString(1);
                    if (!encoding.isEmpty())
                    {
                        encodingList.add(encoding);
                    }
                }
                encodingNo++;
                ppst.clearParameters();
            } while (!encoding.isEmpty());
            encodings = new String[encodingList.size()];
            encodings = encodingList.toArray(encodings);
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        }finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(ppst);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return encodings;
    }
    
    
    public void executeSql(ConnectInfoDTO helperInfo, String db, String sql) throws ClassNotFoundException, SQLException
    {
        Connection conn = null;
        Statement stmt = null;
        try
        {
            String url = helperInfo.getPartURL() + db;
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);
            stmt = conn.createStatement();
            logger.info(sql);
            stmt.execute(sql);
            /*
            SQLWarning warn = stmt.getWarnings();
            if (warn != null
                    && (warn.getSQLState().equals("01006") || warn.getSQLState().equals("01007")))
            {
                //ref: https://www.postgresql.org/docs/10/errcodes-appendix.html
                //logger.warn("SQLState=" + warn.getSQLState());
                //logger.warn("errorCode=" + warn.getErrorCode());//0
                throw warn;
            }
            */
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw ex;
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.debug("close stream");
        }
    }
        
    //the following are server level, use maintain db to create connection.
    //reload configuration
    public void reloadConfiguration(ConnectInfoDTO connectInfo) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT pg_reload_conf()";
        logger.info(sql);
        this.executeSql(connectInfo, connectInfo.getMaintainDB(), sql);
    }
    public void addNameRestorePoint(XMLServerInfoDTO serverInfo, String label) throws ClassNotFoundException, SQLException
    {
        String sql = "select pg_create_restore_point(\'" + label + "\')";
        logger.info(sql);
        this.executeSql(serverInfo, serverInfo.getMaintainDB(), sql);
    }
    public void pauseWALReplay(XMLServerInfoDTO serverInfo) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT pg_xlog_replay_pause()";
        logger.info(sql);
        this.executeSql(serverInfo, serverInfo.getMaintainDB(), sql);
    }
    public void resumeWALReplay(XMLServerInfoDTO serverInfo) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT pg_xlog_replay_resume()";
        logger.info(sql);
        this.executeSql(serverInfo, serverInfo.getMaintainDB(), sql);
    }    
    //server start/stop
    private void printStdOut(boolean isStart, InputStream is, javax.swing.text.Document docs,SimpleAttributeSet attr) throws UnsupportedEncodingException, IOException, BadLocationException
    { 
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is,"UTF-8"));//"GBK"
        String line;
        while ((line = bufferedReader.readLine()) != null)
        {
            logger.info(line);
            docs.insertString(docs.getLength(), line + System.lineSeparator(), attr);
            if (isStart && (line.trim().startsWith("pg_ctl: server is running ") || line.trim().startsWith(constBundle.getString("serverStarted"))))
            {
                break;
            } else if (!isStart && (line.trim().startsWith("pg_ctl: no server running") || line.trim().startsWith(constBundle.getString("serverStopped"))))
            {
                break;
            }
        }
    }
    @Deprecated
    public void startLocalServer(boolean isStart, XMLServerInfoDTO serverInfo, javax.swing.text.Document docs,
            String sysuser, String syspwd) throws IOException, InterruptedException, Exception
    {        
        Properties prop = OptionController.getInstance().getOptionProperties();
        String bin = prop.getProperty("bin_path");
        String data = prop.getProperty("data_path");
        if (bin == null || bin.isEmpty())
        {
            throw new Exception("Bin directory has not set,please set it in option before this operation.");
        }
        if (data == null || data.isEmpty())
        {
            throw new Exception("Data directory has not set,please set it in option before this operation.");
        }

        SimpleAttributeSet attr = new SimpleAttributeSet();
        Process proc = null;   
        try
        {
            String osName = System.getProperty("os.name").toLowerCase();
            logger.info(osName);
            if (osName.indexOf("windows") == -1)
            {
                StringBuilder cmd = new StringBuilder();
                cmd.append(bin).append(File.separator).append("pg_ctl");
                if (isStart)
                {
                    cmd.append(" start ");
                } else
                {
                    cmd.append(" stop ");
                }
                cmd.append(" -D ").append(data).append(" -w");
                docs.insertString(docs.getLength(), cmd + System.lineSeparator(), attr);
                logger.info(cmd.toString());
                proc = Runtime.getRuntime().exec(cmd.toString());
            } else
            {
                List<String> cmds = new ArrayList<>();
                StringBuilder cmdStr = new StringBuilder();
                cmds.add( bin + File.separator +"pg_ctl");//
                cmdStr.append("\"").append(bin).append(File.separator).append("pg_ctl.exe\" ");
                if (isStart)
                {
                    cmds.add("start");
                    cmdStr.append("start ");
                } else
                {
                    cmds.add("stop");
                    cmdStr.append("stop ");
                }
                cmds.add("-t");
                cmds.add("5");
                cmds.add("-D");
                cmds.add(data);
                cmds.add("-w");               
                cmdStr.append("-D \"").append(data).append("\" -w");//.append(" -m fast");
                docs.insertString(docs.getLength(), cmdStr.toString() + System.lineSeparator(), attr);
                logger.info(cmdStr.toString());
                ProcessBuilder pb = new ProcessBuilder(cmds);
                //Map<String,String> map=pb.environment();
                //map.put("PGPASSWORD", helperInfo.getPwd());
                //File log = new File("loglyy");
                pb.redirectErrorStream(true);//将错误流与标准输出流合并到一起
                //pb.redirectOutput(ProcessBuilder.Redirect.to(log));
                proc = pb.start();
            }
            InputStream is = proc.getInputStream();
            this.printStdOut(isStart, is, docs, attr);
            logger.info("wait for:" + proc.waitFor());
            Integer exitValue = proc.exitValue();
            logger.info("exit value=" + exitValue);//the value null or 0 indicates normal termination.     
            docs.insertString(docs.getLength(), "Exit value:" + exitValue + System.lineSeparator(), attr);
        } catch (BadLocationException | IOException | InterruptedException ex)
        {
            if (proc != null)
            {
                proc.destroy();
            }
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        }
    }
    
    @Deprecated
    //need sh script put into pg bin path
    public void controlServerBySSH(boolean isStart, javax.swing.text.Document docs,
            String os, String host, int sshport, String sysuser, String syspwd, String sysservice) throws Exception
    {
        logger.info(os + "," + host + "," + sshport + "," + sysuser + "," + sysservice);
        SimpleAttributeSet attr = new SimpleAttributeSet();
        ch.ethz.ssh2.Connection conn = null;
        Session ssh = null;
        try
        {
            conn = new ch.ethz.ssh2.Connection(host, sshport);
            conn.connect();
            if (!conn.authenticateWithPassword(sysuser, syspwd))
            {
                throw new Exception("Connect failed, user name or password is wrong!");
            } else
            {
                logger.info("connected to the server");
                String cmd = "";
                switch (os)
                {
                    case "Linux": 
                        Properties prop = OptionController.getInstance().getOptionProperties();
                        String bin = prop.getProperty("bin_path");
                        logger.info("BinPath="+bin);
                        if (bin == null || bin.isEmpty())
                        {
                            throw new Exception("Bin directory has not set,please set it in option before this operation.");
                        }
                        
                         //move .sh file to the db server bin path
//                        SCPClient clt = conn.createSCPClient();                       
//                        if (isStart)
//                        {
//                            clt.put("conf/server_start.sh", bin);
//                        } else
//                        {
//                            clt.put("conf/server_stop.sh", bin);
//                        }
                        //command
                        //cmd = "cd " + bin + ";pwd;";
                        cmd = "service " + sysservice;
                        if (isStart)
                        {
                            //cmd = cmd + "./pg_ctl start -D " + data + ";./pg_ctl status -D " + data + ";";
                            //cmd = cmd + "chmod 777 -R server_start.sh;./server_start.sh";//service hgdb-2.0 start
                            cmd = cmd + " start";
                        } else
                        {
                            //cmd = cmd + "./pg_ctl start -D " + data + " -m fast;./pg_ctl status -D " + data + ";";
                            // cmd = cmd + "chmod 777 -R server_stop.sh;./server_stop.sh";//service hgdb-2.0 stop
                            cmd = cmd + " stop";
                        }
                        break;
                    case "Windows":
                        if (isStart)
                        {
                            cmd = "net start ";
                        } else
                        {
                            cmd = "net stop ";
                        }
                        cmd = cmd + sysservice;
                        break;
                }                
                docs.insertString(docs.getLength(), cmd + System.lineSeparator()
                        + "----------------------------------------------------------" + System.lineSeparator(), attr);
                logger.info(cmd.toString());
                ssh = conn.openSession();
                ssh.execCommand(cmd.toString());
                InputStream is = new StreamGobbler(ssh.getStdout());
                this.printStdOut(isStart, is, docs, attr);
                Integer exitValue = ssh.getExitStatus();
                logger.info("exit value = " + exitValue);//the value null or 0 indicates normal termination.     
                docs.insertString(docs.getLength(), "Exit value: " + exitValue +"."+ System.lineSeparator(), attr);
            }
        } catch (Exception e)
        {
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
            throw new Exception(e);
        } finally
        {
            if (ssh != null)
            {
                ssh.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }
    }
    
    public void controlServerByCtl(boolean isStart, javax.swing.text.Document docs,
            String os, String host, int sshport, String sysuser, String syspwd, String bin, String data) throws Exception
    {
        logger.info(os + "," + host + "," + sshport + "," + sysuser + "," + bin + ", " + data);
        SimpleAttributeSet attr = new SimpleAttributeSet();
        //docs.insertString(docs.getLength(), (isStart? "sttaring..." : "stopping...") + System.lineSeparator(), attr);
        ch.ethz.ssh2.Connection conn = null;
        Session ssh = null;
        try
        {
            conn = new ch.ethz.ssh2.Connection(host, sshport);
            conn.connect();
            if (!conn.authenticateWithPassword(sysuser, syspwd))
            {
                throw new Exception("Connect failed, user name or password is wrong!");
            } else
            {
                logger.info("server connected");
                
                String lib = data.replace("data", "lib");
                String cmd = "export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:" + lib + "; " +
                        "cd " + bin + "; pwd; "
                        + "./pg_ctl " + (isStart ? "start" : "stop") + " -D " + data + "; ./pg_ctl status -D " + data + ";";
                //String cmd = bin + "/pg_ctl " + (isStart ? "start" : "stop") + " -D " + data;
                logger.info(cmd.toString());
                docs.insertString(docs.getLength(), cmd + System.lineSeparator()
                        + "----------------------------------------------------------" + System.lineSeparator(), attr);
                ssh = conn.openSession();
                logger.info("session opened");
                ssh.execCommand(cmd.toString());
                InputStream is = new StreamGobbler(ssh.getStdout());
                InputStream erris = new StreamGobbler(ssh.getStderr());
                this.printStdOut(isStart, is, docs, attr);
                this.printStdOut(isStart, erris, docs, attr);
                Integer exitValue = ssh.getExitStatus();
                logger.info("exit value = " + exitValue);//the value null or 0 indicates normal termination.     
                docs.insertString(docs.getLength(), "Exit value: " + exitValue + "." + System.lineSeparator(), attr);
            }
        } catch (Exception e)
        {
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
            throw e;
        } finally
        {
            if (ssh != null)
            {
                ssh.close();
            }
            if (conn != null)
            {
                conn.close();
            }
        }
    }
    
    //hba config
    public List<String> getDBNameList(HelperInfoDTO helperInfo) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT DISTINCT quote_ident(datname), datname  FROM pg_database ORDER BY datname";
        return this.getListFromQuery(helperInfo,helperInfo.getMaintainDB(), sql);
    }
    public List<String> getUserNameList(HelperInfoDTO helperInfo) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT DISTINCT quote_ident(usename) FROM pg_user UNION  SELECT 'group ' || groname FROM pg_group";
       return this.getListFromQuery(helperInfo, helperInfo.getMaintainDB(), sql);
    }
    //reassign and drop owned
    public String[] getAllRolesExcept(HelperInfoDTO helperInfo, String currentUser) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT DISTINCT quote_ident(rolname),rolname FROM pg_roles WHERE rolname<>'" + currentUser + "' ORDER BY rolname";
        List<String> list = this.getListFromQuery(helperInfo, helperInfo.getMaintainDB(), sql);
        String[] array = new String[list.size()];
        array= list.toArray(array);
        return array;
        
    }
    public String[] getOwnedDBs(HelperInfoDTO helperInfo) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT DISTINCT quote_ident(datname),datname FROM pg_database WHERE datallowconn ORDER BY datname";
         List<String> list = this.getListFromQuery(helperInfo, helperInfo.getMaintainDB(), sql);
         String[] array = new String[list.size()];
         array = list.toArray(array);
         return array;
    }
    //move tablespace
    public String[] getTablespacesExcept(HelperInfoDTO helperInfo, String currentSpcname) throws ClassNotFoundException, SQLException
    {
        String sql = "SELECT DISTINCT quote_ident(spcname), spcname  FROM pg_tablespace WHERE spcname<>'" + currentSpcname + "' ORDER BY spcname";
         List<String> list =  this.getListFromQuery(helperInfo, helperInfo.getMaintainDB(), sql);
         String[] array = new String[list.size()];
         array = list.toArray(array);
         return array;
    }    
    private List<String> getListFromQuery(HelperInfoDTO helperInfo, String db, String sql) throws ClassNotFoundException, SQLException
    {
        List<String> nameList = new ArrayList();
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return an empty list.");
            return nameList;
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            logger.info(helperInfo.getPartURL() + db);
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + db, helperInfo);
            stmt = conn.createStatement();
            logger.info(sql);
            rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                nameList.add(rs.getString(1));
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        logger.info("Return: size=" + nameList.size());
        return nameList;
    }  
    
    
    /*the following are db level, use their db to create connection*/
    //enable or disable all Trigger of a table
    public void enableAllTrigger(TableInfoDTO table, boolean isEnable) throws ClassNotFoundException, SQLException
    {
        if (table == null)
        {
            logger.error("table info is null, do nothing and return.");
            return;
        }
        SyntaxController sc= SyntaxController.getInstance();
        StringBuilder sql = new StringBuilder();
        sql.append("ALTER TABLE ")
                .append(sc.getName(table.getHelperInfo().getSchema())).append(".").append(sc.getName(table.getName()));
        if (isEnable)
        {
            sql.append(" ENABLE");
        } else
        {
            sql.append(" DISABLE");
        }
        sql.append(" TRIGGER ALL;");
        logger.info(sql.toString());
        HelperInfoDTO helperInfo = table.getHelperInfo();
        this.executeSql(helperInfo, helperInfo.getDbName(), sql.toString());
    }
    //enable/disable one trigger
    public void enableTheTrigger(TriggerInfoDTO trigger, boolean isEnable) throws ClassNotFoundException, SQLException
    {
        if (trigger == null)
        {
            logger.error("trigger info is null, do nothing and return.");
            return;
        }
        
        HelperInfoDTO helperInfo = trigger.getHelperInfo();
        SyntaxController sc= SyntaxController.getInstance();
        StringBuilder sql = new StringBuilder();
        sql.append("ALTER TABLE ").append(sc.getName(helperInfo.getSchema())).append(".").append(sc.getName(helperInfo.getRelation()));
        if (isEnable)
        {
            sql.append(" ENABLE");
        } else
        {
            sql.append(" DISABLE");
        }
        sql.append(" TRIGGER ").append(sc.getName(trigger.getName())).append(";");
        logger.info(sql.toString());
        this.executeSql(helperInfo, helperInfo.getDbName(), sql.toString());
        trigger.setEnable(isEnable); 
    }
    //enable/disable the rule
    public void enableTheRule(RuleInfoDTO rule, boolean isEnable) throws ClassNotFoundException, SQLException
    {
        if (rule == null)
        {
            logger.error("rule info is null, do nothing and return.");
            return;
        }        
        HelperInfoDTO helperInfo = rule.getHelperInfo();
        StringBuilder sql = new StringBuilder();
        SyntaxController sc= SyntaxController.getInstance();
        sql.append("ALTER TABLE ")
                .append(sc.getName(helperInfo.getSchema())).append(".").append(sc.getName(helperInfo.getRelation()));
        if (isEnable)
        {
            sql.append(" ENABLE");
        } else
        {
            sql.append(" DISABLE");
        }
        sql.append(" RULE ").append(sc.getName(rule.getName())).append(";");
        logger.info(sql.toString());
        this.executeSql(helperInfo, helperInfo.getDbName(), sql.toString());
        rule.setEnable(isEnable);
    }
    //enable/disable the constraint for gc
    public void enableTheContraint(ConstraintInfoDTO constr, boolean isEnable) throws ClassNotFoundException, SQLException
    {
        if (constr == null)
        {
            logger.error("constraint info is null, do nothing and return.");
            return;
        }        
        HelperInfoDTO helperInfo = constr.getHelperInfo();
        StringBuilder sql = new StringBuilder();
        SyntaxController sc= SyntaxController.getInstance();
        sql.append("ALTER TABLE ")
                .append(sc.getName(helperInfo.getSchema())).append(".").append(sc.getName(helperInfo.getRelation()));
        if (isEnable)
        {
            sql.append(" ENABLE");
        } else
        {
            sql.append(" DISABLE");
        }
        sql.append(" CONSTRAINT ").append(sc.getName(constr.getName())).append(";");
        logger.info(sql.toString());
        this.executeSql(helperInfo, helperInfo.getDbName(), sql.toString());
        constr.setEnable(isEnable);
    }
    //count row of table
    public void countTableRow(TableInfoDTO table) throws ClassNotFoundException, SQLException
    {
        if (table == null)
        {
            logger.error("table info is null, do nothing and return.");
            return;
        }
        HelperInfoDTO helperInfo = table.getHelperInfo();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {         
            String url = helperInfo.getPartURL() + helperInfo.getDbName();
            logger.debug(url);
            SyntaxController sc = SyntaxController.getInstance();
            String sql = "SELECT COUNT(*) FROM " + sc.getName(helperInfo.getSchema()) + "." + sc.getName(table.getName());
            logger.debug(sql);
            conn = JdbcHelper.getConnection(url, helperInfo );
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next())
            {
                int rowCount = rs.getInt(1);
                logger.debug("rowCount=" + rowCount);
                table.setRowsCounted(rowCount);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.debug("close stream");
        }
    }
    //truncate [cascade] table
    public void truncateTable(TableInfoDTO table, boolean isCascade) throws ClassNotFoundException, SQLException
    {
        if (table == null)
        {
            logger.error("table info is null, do nothing and return.");
            return;
        }
        HelperInfoDTO helperInfo = table.getHelperInfo();
        StringBuilder sql = new StringBuilder();
        SyntaxController sc= SyntaxController.getInstance();        
        sql.append("TRUNCATE ");
        sql.append(sc.getName(helperInfo.getSchema())).append(".").append(sc.getName(table.getName()));
        if (isCascade)
        {
            sql.append(" CASCADE");
        }
        logger.debug(sql.toString());
        this.executeSql(helperInfo, helperInfo.getDbName(), sql.toString());
        table.setRowsEstimated(0);
        table.setRowsCounted(0);
    }

    //export table data
    public List<ColumnDTO> getColumnsOfTable(TableInfoDTO table) throws ClassNotFoundException, SQLException
    {
        List<ColumnDTO> nameList = new ArrayList();
        HelperInfoDTO helperInfo = table.getHelperInfo();
        if(helperInfo ==null)
        {
            logger.error("helperInfo is null, do nothing and return an empty list.");
            return nameList;
        }        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT quote_ident(att.attname) AS colName,att.attnum AS colLocation,")
                .append(" typname AS dataType,format_type(pg_type.oid,att.atttypmod) AS displaytypname ")
                .append(" FROM pg_attribute att")
                .append(" INNER JOIN pg_type ON (pg_type.oid=att.atttypid)")
                .append(" WHERE  att.attnum>0")
                .append(" AND att.attrelid=").append(table.getOid())
                .append(" ORDER BY att.attnum ASC");
        logger.info(sql.toString());        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(), helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            ColumnDTO column;
            while(rs.next())
            {
                column = new ColumnDTO();
                column.setName(rs.getString(1));
                column.setType(rs.getString(3));
                nameList.add(column);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new SQLException(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        logger.info("Return: size="+nameList.size());
        return nameList;
    }

}
