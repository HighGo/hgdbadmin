/* ------------------------------------------------
 *
 * File: SyntaxController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\controller\SyntaxController.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 * based on pg9.3.5 document
 */
public class SyntaxController
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    
    private static SyntaxController syntaxController = null;
    public static SyntaxController getInstance()
    {
        if (syntaxController == null)
        {
            syntaxController = new SyntaxController();
        }
        return syntaxController;
    }  
    
    public static void main(String[] args){
    
        System.out.println(SyntaxController.getInstance().getName("\"\"\""));
    }
    
    public String getName(String unquotedName)
    {
        //logger.info(unquotedName);
        if(unquotedName == null || unquotedName.isEmpty())
        {
            logger.error("enterd value is (null or empty)" + unquotedName);
        }
        
        //" rule is for all
        if(unquotedName.contains("\""))
        {
            unquotedName = unquotedName.replaceAll("\"", "\"\"");
        }
        //' rule is only when for condition string
        /*if(unquotedName.contains("'"))
        {
            unquotedName = unquotedName.replaceAll("'", "''");
        }*/
        
        /*if (unquotedName.startsWith("\"")
                && unquotedName.endsWith("\""))
        {
            return unquotedName;
        }*/
        
        //Pattern pattern = Pattern.compile("[A-Z]+");
        //Matcher matcher = pattern.matcher(unquotedName);
        //matcher.find() || isReservedKeyWord(unquotedName)
        if (!unquotedName.matches("^[a-z_][a-z0-9_$]*$") || this.isReservedKeyWord(unquotedName) )
        {
            return "\"" + unquotedName + "\"";
        } else
        {
            return unquotedName;
        }
    }
    
    //keyword
    public boolean isReservedKeyWord(String word)
    {
        //logger.info("Enter:word="+word);
        boolean flag = false;
        List<String> keyWords = getReservedKeyWord();
        for (String keyWord : keyWords)
        {
            //System.out.println("keyWord="+keyWord);
            if (keyWord.equals(word.toUpperCase()))
            {
                flag = true;
                break;
            }
        }
        //logger.info("return;"+flag);
        return flag;
    }
    private boolean isReservedAllowFunctionType(String word)
    {
        boolean flag = false;
        List<String> keyWords = getReservedAllowFunctionType();
        for (String keyWord : keyWords)
        {
            if (keyWord.equals(word.toUpperCase()))
            {
                flag = true;
                break;
            }
        }
        return flag;
    }    
    
    public boolean isNonReservedKeyWord(String word)
    {
        boolean flag = false;
        List<String> keyWords = getNonReservedKeyWord();
        for (String keyWord : keyWords)
        {
            if (keyWord.equals(word.toUpperCase()))
            {
                flag = true;
                break;
            }
        }
        return flag;
    }
    private boolean isNonReservedForbidFunctionType(String word)
    {
        boolean flag = false;
        List<String> keyWords = getNonReservedForbidFunctionType();
        for (String keyWord : keyWords)
        {
            if (keyWord.equals(word.toUpperCase()))
            {
                flag = true;
                break;
            }
        }
        return flag;
    }    
    
    
    private List<String> getNonReservedForbidFunctionType()
    {
        List<String> keywords = new ArrayList();
        keywords.add("BETWEEN");
        keywords.add("BIGINT");
        keywords.add("BIT");
        keywords.add("BOOLEAN");
        keywords.add("CHAR");
        keywords.add("CHARACTER");
        keywords.add("COALESCE");
        keywords.add("DEC");
        keywords.add("DECIMAL");
        keywords.add("EXISTS");
        keywords.add("EXTRACT");
        keywords.add("FLOAT");
        keywords.add("GREATEST");
        keywords.add("INOUT");
        keywords.add("INT");
        keywords.add("INTEGER");
        keywords.add("INTERVAL");
        keywords.add("LEAST");
        keywords.add("NATIONAL");
        keywords.add("NCHAR");
        keywords.add("NONE");
        keywords.add("NULLIF");
        keywords.add("NUMERIC");
        keywords.add("OUT");
        keywords.add("OVERLAY");
        keywords.add("POSITION");
        keywords.add("PRECISION");
        keywords.add("REAL");
        keywords.add("ROW");
        keywords.add("SETOF");
        keywords.add("SMALLINT");
        keywords.add("SUBSTRING");
        keywords.add("TIME");
        keywords.add("TIMESTAMP");
        keywords.add("TREAT");
        keywords.add("TRIM");
        keywords.add("VALUES");
        keywords.add("VARCHAR");
        keywords.add("XMLATTRIBUTES");
        keywords.add("XMLCONCAT");
        keywords.add("XMLELEMENT");
        keywords.add("XMLEXISTS");
        keywords.add("XMLFOREST");
        keywords.add("XMLPARSE");
        keywords.add("XMLPI");
        keywords.add("XMLROOT");
        keywords.add("XMLSERIALIZE");
        return keywords;
    }
    private List<String> getNonReservedKeyWord()
    {
        List<String> keywords = new ArrayList();
        keywords.add("ABORT");
        keywords.add("ABSOLUTE");
        keywords.add("ACCESS");
        keywords.add("ACTION");
        keywords.add("ADD");
        keywords.add("ADMIN");
        keywords.add("AFTER");
        keywords.add("AGGREGATE");
        keywords.add("ALSO");
        keywords.add("ALTER");
        keywords.add("ALWAYS");
        keywords.add("ASSERTION");
        keywords.add("ASSIGNMENT");
        keywords.add("AT");
        keywords.add("ATTRIBUTE	");
        keywords.add("BACKWARD	");
        keywords.add("BEFORE");
        keywords.add("BEGIN");
        keywords.add("BY");
        keywords.add("CACHE");
        keywords.add("CALLED");
        keywords.add("CASCADE");
        keywords.add("CASCADED");
        keywords.add("CATALOG");
        keywords.add("CHAIN");
        keywords.add("CHARACTERISTICS");
        keywords.add("CHECKPOINT");
        keywords.add("CLASS");
        keywords.add("CLOSE");
        keywords.add("CLUSTER");
        keywords.add("COMMENT");
        keywords.add("COMMENTS");
        keywords.add("COMMIT");
        keywords.add("COMMITTED");
        keywords.add("CONFIGURATION");
        keywords.add("CONNECTION");
        keywords.add("CONSTRAINTS");
        keywords.add("CONTENT");
        keywords.add("CONTINUE");
        keywords.add("CONVERSION");
        keywords.add("COPY");
        keywords.add("COST");
        keywords.add("CSV");
        keywords.add("CURRENT");
        keywords.add("CURSOR");
        keywords.add("CYCLE");
        keywords.add("DATA");
        keywords.add("DATABASE");
        keywords.add("DAY");
        keywords.add("DEALLOCATE");
        keywords.add("DECLARE");
        keywords.add("DEFAULTS");
        keywords.add("DEFERRED");
        keywords.add("DEFINER");
        keywords.add("DELETE");
        keywords.add("DELIMITER");
        keywords.add("DELIMITERS");
        keywords.add("DICTIONARY");
        keywords.add("DISABLE");
        keywords.add("DISCARD");
        keywords.add("DOCUMENT");
        keywords.add("DOMAIN");
        keywords.add("DOUBLE");
        keywords.add("DROP");
        keywords.add("EACH");
        keywords.add("ENABLE");
        keywords.add("ENCODING");
        keywords.add("ENCRYPTED");
        keywords.add("ENUM");
        keywords.add("ESCAPE");
        keywords.add("EVENT");
        keywords.add("EXCLUDE");
        keywords.add("EXCLUDING");
        keywords.add("EXCLUSIVE");
        keywords.add("EXECUTE");
        keywords.add("EXPLAIN");
        keywords.add("EXTENSION");
        keywords.add("EXTERNAL");
        keywords.add("FAMILY");
        keywords.add("FIRST");
        keywords.add("FOLLOWING");
        keywords.add("FORCE");
        keywords.add("FORWARD");
        keywords.add("FUNCTION");
        keywords.add("FUNCTIONS");
        keywords.add("GLOBAL");
        keywords.add("GRANTED");
        keywords.add("HANDLER");
        keywords.add("HEADER");
        keywords.add("HOLD");
        keywords.add("HOUR");
        keywords.add("IDENTITY");
        keywords.add("IF");
        keywords.add("IMMEDIATE");
        keywords.add("IMMUTABLE");
        keywords.add("IMPLICIT");
        keywords.add("INCLUDING");
        keywords.add("INCREMENT");
        keywords.add("INDEX");
        keywords.add("INDEXES");
        keywords.add("INHERIT");
        keywords.add("INHERITS");
        keywords.add("INLINE");
        keywords.add("INPUT");
        keywords.add("INSENSITIVE");
        keywords.add("INSERT");
        keywords.add("INSTEAD");
        keywords.add("INVOKER");
        keywords.add("ISOLATION");
        keywords.add("KEY");
        keywords.add("LABEL");
        keywords.add("LANGUAGE");
        keywords.add("LARGE");
        keywords.add("LAST");
        keywords.add("LC_COLLATE");
        keywords.add("LC_CTYPE");
        keywords.add("LEAKPROOF");
        keywords.add("LEVEL");
        keywords.add("LISTEN");
        keywords.add("LOAD");
        keywords.add("LOCAL");
        keywords.add("LOCATION");
        keywords.add("LOCK");
        keywords.add("MAPPING");
        keywords.add("MATCH");
        keywords.add("MATERIALIZED");
        keywords.add("MAXVALUE");
        keywords.add("MINUTE");
        keywords.add("MINVALUE");
        keywords.add("MODE");
        keywords.add("MONTH");
        keywords.add("MOVE");
        keywords.add("NAME");
        keywords.add("NAMES");
        keywords.add("NEXT");
        keywords.add("NO");
        keywords.add("NOTHING");
        keywords.add("NOTIFY");
        keywords.add("NOWAIT");
        keywords.add("NULLS");
        keywords.add("OBJECT");
        keywords.add("OF");
        keywords.add("OFF");
        keywords.add("OIDS");
        keywords.add("OPERATOR");
        keywords.add("OPTION");
        keywords.add("OPTIONS");
        keywords.add("OWNED");
        keywords.add("OWNER");
        keywords.add("PARSER");
        keywords.add("PARTIAL");
        keywords.add("PARTITION");
        keywords.add("PASSING");
        keywords.add("PASSWORD");
        keywords.add("PLANS");
        keywords.add("PRECEDING");
        keywords.add("PREPARE");
        keywords.add("PREPARED");
        keywords.add("PRESERVE");
        keywords.add("PRIOR");
        keywords.add("PRIVILEGES");
        keywords.add("PROCEDURAL");
        keywords.add("PROCEDURE");
        keywords.add("PROGRAM");
        keywords.add("QUOTE");
        keywords.add("RANGE");
        keywords.add("READ");
        keywords.add("REASSIGN");
        keywords.add("RECHECK");
        keywords.add("RECURSIVE");
        keywords.add("REF");
        keywords.add("REFRESH");
        keywords.add("REINDEX");
        keywords.add("RELATIVE");
        keywords.add("RELEASE");
        keywords.add("RENAME");
        keywords.add("REPEATABLE");
        keywords.add("REPLACE");
        keywords.add("REPLICA");
        keywords.add("RESET");
        keywords.add("RESTART");
        keywords.add("RESTRICT");
        keywords.add("RETURNS");
        keywords.add("REVOKE");
        keywords.add("ROLE");
        keywords.add("ROLLBACK");
        keywords.add("ROWS");
        keywords.add("RULE");
        keywords.add("SAVEPOINT");
        keywords.add("SCHEMA");
        keywords.add("SCROLL");
        keywords.add("SEARCH");
        keywords.add("SECOND");
        keywords.add("SECURITY");
        keywords.add("SEQUENCE");
        keywords.add("SEQUENCES");
        keywords.add("SERIALIZABLE");
        keywords.add("SERVER");
        keywords.add("SESSION");
        keywords.add("SET");
        keywords.add("SHARE");
        keywords.add("SHOW");
        keywords.add("SIMPLE");
        keywords.add("SNAPSHOT");
        keywords.add("STABLE");
        keywords.add("STANDALONE");
        keywords.add("START");
        keywords.add("STATEMENT");
        keywords.add("STATISTICS");
        keywords.add("STDIN");
        keywords.add("STDOUT");
        keywords.add("STORAGE");
        keywords.add("STRICT");
        keywords.add("STRIP");
        keywords.add("SYSID");
        keywords.add("SYSTEM");
        keywords.add("TABLES");
        keywords.add("TABLESPACE");
        keywords.add("TEMP");
        keywords.add("TEMPLATE");
        keywords.add("TEMPORARY");
        keywords.add("TEXT");
        keywords.add("TRANSACTION");
        keywords.add("TRIGGER");
        keywords.add("TRUNCATE");
        keywords.add("TRUSTED");
        keywords.add("TYPE");
        keywords.add("TYPES");
        keywords.add("UNBOUNDED");
        keywords.add("UNCOMMITTED");
        keywords.add("UNENCRYPTED");
        keywords.add("UNKNOWN");
        keywords.add("UNLISTEN");
        keywords.add("UNLOGGED");
        keywords.add("UNTIL");
        keywords.add("UPDATE");
        keywords.add("VACUUM");
        keywords.add("VALID");
        keywords.add("VALIDATE");
        keywords.add("VALIDATOR");
        keywords.add("VALUE");
        keywords.add("VARYING");
        keywords.add("VERSION");
        keywords.add("VIEW");
        keywords.add("VOLATILE");
        keywords.add("WHITESPACE");
        keywords.add("WITHOUT");
        keywords.add("WORK");
        keywords.add("WRAPPER");
        keywords.add("WRITE");
        keywords.add("XML");
        keywords.add("YEAR");
        keywords.add("YES");
        keywords.add("ZONE");        
        keywords.add("BETWEEN");
        keywords.add("BIGINT");
        keywords.add("BIT");
        keywords.add("BOOLEAN");
        keywords.add("CHAR");
        keywords.add("CHARACTER");
        keywords.add("COALESCE");
        keywords.add("DEC");
        keywords.add("DECIMAL");
        keywords.add("EXISTS");
        keywords.add("EXTRACT");
        keywords.add("FLOAT");
        keywords.add("GREATEST");
        keywords.add("INOUT");
        keywords.add("INT");
        keywords.add("INTEGER");
        keywords.add("INTERVAL");
        keywords.add("LEAST");
        keywords.add("NATIONAL");
        keywords.add("NCHAR");
        keywords.add("NONE");
        keywords.add("NULLIF");
        keywords.add("NUMERIC");
        keywords.add("OUT");
        keywords.add("OVERLAY");
        keywords.add("POSITION");
        keywords.add("PRECISION");
        keywords.add("REAL");
        keywords.add("ROW");
        keywords.add("SETOF");
        keywords.add("SMALLINT");
        keywords.add("SUBSTRING");
        keywords.add("TIME");
        keywords.add("TIMESTAMP");
        keywords.add("TREAT");
        keywords.add("TRIM");
        keywords.add("VALUES");
        keywords.add("VARCHAR");
        keywords.add("XMLATTRIBUTES");
        keywords.add("XMLCONCAT");
        keywords.add("XMLELEMENT");
        keywords.add("XMLEXISTS");
        keywords.add("XMLFOREST");
        keywords.add("XMLPARSE");
        keywords.add("XMLPI");
        keywords.add("XMLROOT");
        keywords.add("XMLSERIALIZE");
        return keywords;
    }
    
    private List<String> getReservedAllowFunctionType()
    {
        List<String> keywords = new ArrayList();
        keywords.add("AUTHORIZATION");
        keywords.add("BINARY");
        keywords.add("COLLATION");
        keywords.add("CONCURRENTLY");
        keywords.add("CROSS");
        keywords.add("CURRENT_SCHEMA");
        keywords.add("FREEZE");
        keywords.add("FULL");
        keywords.add("ILIKE");
        keywords.add("INNER");
        keywords.add("IS");
        keywords.add("ISNULL");
        keywords.add("JOIN");
        keywords.add("LEFT");
        keywords.add("LIKE");
        keywords.add("NATURAL");
        keywords.add("NOTNULL");
        keywords.add("OUTER");
        keywords.add("OVER");
        keywords.add("OVERLAPS");
        keywords.add("RIGHT");
        keywords.add("SIMILAR");
        keywords.add("VERBOSE");
        return keywords;
    }
    private List<String> getReservedKeyWord()
    {
        List<String> keywords = new ArrayList();
        keywords.add("ALL");
        keywords.add("ANALYSE");
        keywords.add("ANALYZE");
        keywords.add("AND");
        keywords.add("ANY");
        keywords.add("ARRAY");
        keywords.add("AS");
        keywords.add("ASC");
        keywords.add("ASYMMETRIC");
        keywords.add("BOTH");
        keywords.add("CASE");
        keywords.add("CAST");
        keywords.add("CHECK");
        keywords.add("COLLATE");
        keywords.add("COLUMN");
        keywords.add("CONSTRAINT");
        keywords.add("CREATE");
        keywords.add("CURRENT_CATALOG");
        keywords.add("CURRENT_DATE");
        keywords.add("CURRENT_ROLE");
        keywords.add("CURRENT_TIME");
        keywords.add("CURRENT_TIMESTAMP");
        keywords.add("CURRENT_USER");
        keywords.add("DEFAULT");
        keywords.add("DEFERRABLE");
        keywords.add("DESC");
        keywords.add("DISTINCT");
        keywords.add("DO");
        keywords.add("ELSE");
        keywords.add("END");
        keywords.add("EXCEPT");
        keywords.add("FETCH");
        keywords.add("FOR");
        keywords.add("FOREIGN");
        keywords.add("FROM");
        keywords.add("GRANT");
        keywords.add("GROUP");
        keywords.add("HAVING");
        keywords.add("IN");
        keywords.add("INITIALLY");
        keywords.add("INTERSECT");
        keywords.add("INTO");
        keywords.add("LATERAL");
        keywords.add("LEADING");
        keywords.add("LIMIT");
        keywords.add("LOCALTIME");
        keywords.add("LOCALTIMESTAMP");
        keywords.add("NOT");
        keywords.add("NULL");
        keywords.add("OFFSET");
        keywords.add("ON");
        keywords.add("ONLY");
        keywords.add("OR");
        keywords.add("ORDER");
        keywords.add("PLACING");
        keywords.add("PRIMARY");
        keywords.add("REFERENCES");
        keywords.add("RETURNING");
        keywords.add("SELECT");
        keywords.add("SESSION_USER");
        keywords.add("SOME");
        keywords.add("SYMMETRIC");
        keywords.add("TABLE");
        keywords.add("THEN");
        keywords.add("TO");
        keywords.add("TRAILING");
        keywords.add("UNION");
        keywords.add("UNIQUE");
        keywords.add("USER");
        keywords.add("USING");
        keywords.add("VARIADIC");
        keywords.add("WHEN");
        keywords.add("WHERE");
        keywords.add("WINDOW");
        keywords.add("WITH");
        keywords.add("FALSE");
        keywords.add("TRUE");
        keywords.add("AUTHORIZATION");
        keywords.add("BINARY");
        keywords.add("COLLATION");
        keywords.add("CONCURRENTLY");
        keywords.add("CROSS");
        keywords.add("CURRENT_SCHEMA");
        keywords.add("FREEZE");
        keywords.add("FULL");
        keywords.add("ILIKE");
        keywords.add("INNER");
        keywords.add("IS");
        keywords.add("ISNULL");
        keywords.add("JOIN");
        keywords.add("LEFT");
        keywords.add("LIKE");
        keywords.add("NATURAL");
        keywords.add("NOTNULL");
        keywords.add("OUTER");
        keywords.add("OVER");
        keywords.add("OVERLAPS");
        keywords.add("RIGHT");
        keywords.add("SIMILAR");
        keywords.add("VERBOSE");
        return keywords;
    }

    
    
    
    
    
    //sql syntax Highlighting for SQL View
    //operator    
    private boolean isOperator(String word)
    {
        //logger.info("Enter:word="+word);
        boolean flag = false;
        List<String> operators = getOperator();
        for (String operator : operators)
        {
            if (operator.equals(word))
            {
                flag = true;
                break;
            }
        }
        //logger.info("return;"+flag);
        return flag;
    }
    private List<String> getOperator()
    {
        List<String> operators = new ArrayList();
        //Logical Operators
        operators.add("AND");
        operators.add("OR");
        operators.add("NOT");
        //Comparison Operators
        operators.add(">");
        operators.add(">=");
        operators.add("<");
        operators.add("<=");
        operators.add("=");
        operators.add("<>");
        operators.add("!=");
        
        return operators;
    }
    //digit
    private boolean isDigit(String word)
    {
        //logger.info("Enter:" + word);
        boolean flag = false;
        try
        {
            Double.parseDouble(word);
            flag = true;
        } catch (NumberFormatException ex)
        {
            flag = false;
        }
        //logger.info("Return:" + flag);
        return flag;
    }
    //quoted string
     private boolean isQuoted(String word)
    {
        //logger.info("Enter:word=" + word);
        boolean flag = false;
        if (word.startsWith("\"") && word.endsWith("\""))
        {
            flag = true;
        } else if (word.startsWith("'") && word.endsWith("'"))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        //logger.info("Return:" + flag);
        return flag;
    }
    
    //set sql
    private Color keyWordColor = new Color(0, 1, 125);
    private Color quotedStringColor = new Color(126, 1, 126);
    private Color digitColor = new Color(0, 128, 126);
    private Color operatorColor = new Color(125, 125, 125);
    private Color identiferColor = Color.BLACK;
    //private Color commentColor = new Color(3, 131, 3);
    public void insert(Document docs, String str, AttributeSet attrset)
    {
        try
        {
            docs.insertString(docs.getLength(), str, attrset);
        } catch (BadLocationException ble)
        {
            logger.error(ble.getMessage());
            ble.printStackTrace(System.out);
        }
    }
    public void setComment(Document docs, String str)
    {
        SimpleAttributeSet attrset = new SimpleAttributeSet();
        StyleConstants.setForeground(attrset, new Color(3, 131, 3));
        insert(docs, str+"\n", attrset);
    }
    private void setSQL(Document docs, String str, Color color)
    {
        SimpleAttributeSet attrset = new SimpleAttributeSet();
        StyleConstants.setForeground(attrset, color);
        insert(docs, str, attrset);
    }

    public void setDefine(Document docs, String define)
    {
        if (define == null)
        {
            logger.warn("Define is  null, do nothing and return.");
            return;
        }
        String[] array = define.split(" ");
        for (String word : array)
        {
            String temp = word.replaceAll("\n", "").trim();
            if (temp.equals(" "))
            {
                //do nothing
            } else if (isReservedKeyWord(temp) || isNonReservedKeyWord(temp))
            {
                setSQL(docs, word + " ", keyWordColor);
            } else if (isQuoted(temp))
            {
                setSQL(docs, word + " ", quotedStringColor);
            } else if (isDigit(temp))
            {
                setSQL(docs, word + " ", digitColor);
            } else if (isOperator(temp))
            {
                setSQL(docs, word + " ", operatorColor);
            } else
            {
                setSQL(docs, word + " ", identiferColor);
            }
        }
    }
    
}
