/* ------------------------------------------------ 
* 
* File: ServerConfigView.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverconfig\view\ServerConfigView.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverconfig.view;


import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.serverconfig.controller.Controller;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 *
 * @author Yuanyuan
 */
public class ServerConfigView extends JFrame
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");
    
    private HelperInfoDTO helper;
    
    /**
     * Creates new form MainView
     * @param helper
     */
    public ServerConfigView(HelperInfoDTO helper)
    {
        this.helper = helper;
        initComponents();
        this.setTitle(constBundle.getString("serverConfig") + " - " + helper.toString());
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/image/hgdb.png")));

        String[] headers = new String[]
        {
           "", constBundle.getString("name"), constBundle.getString("value"), constBundle.getString("unit"),
            constBundle.getString("minValue"), constBundle.getString("maxValue"), constBundle.getString("description")
        };
        DefaultTableModel tbModel = new DefaultTableModel(headers, 0)
        {
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                if (columnIndex == 0  )
                {
                    return Boolean.class;
                } else
                {
                    return Object.class;
                }
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return column==0 || column == 2;
            }
        };
        tblConfig.setModel(tbModel);
        tblConfig.getColumn("").setMaxWidth(30);
        tblConfig.getColumn(constBundle.getString("name")).setPreferredWidth(170);
        tblConfig.getColumn(constBundle.getString("value")).setWidth(100);
        tblConfig.getColumn(constBundle.getString("value")).setPreferredWidth(100);
        tblConfig.getColumn(constBundle.getString("unit")).setMaxWidth(50);
        tblConfig.getColumn(constBundle.getString("minValue")).setMaxWidth(100);
        tblConfig.getColumn(constBundle.getString("maxValue")).setMaxWidth(150);

        DefaultComboBoxModel model = new DefaultComboBoxModel();
        cbbCategory.setModel(model);
        try
        {
            Controller.getInstance().getCatalog(this.helper, model);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        cbbCategory.setSelectedIndex(-1);        
        cbbCategory.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbbCategoryStateChanged(e);
            }
        });
        tblConfig.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent e)
            {
                logger.info("row=" + tblConfig.getSelectedRow());
                //Object newValue = tblConfig.getValueAt(tblConfig.getSelectedRow(), 2);
                //logger.info("newValue=" + newValue);
                tblConfig.setValueAt(true, tblConfig.getSelectedRow(), 0);
            }
        });
        
        ActionListener set = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnSetAction(e);
            }
        };
        btnSet.addActionListener(set);
        itemSet.addActionListener(set);

        ActionListener reset = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnResetAction(e);
            }
        };
        btnReset.addActionListener(reset);
        itemReset.addActionListener(reset);

        ActionListener resetAll = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnResetAllAction(e);
            }
        };
        btnResetAll.addActionListener(resetAll);
        itemResetAll.addActionListener(resetAll);
        
        itemExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemExitActionPerformed(e);
            }
        });
    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        pnlCustomer = new javax.swing.JPanel();
        pnlMain = new javax.swing.JPanel();
        cbbCategory = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblConfig = new javax.swing.JTable();
        btnSet = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        btnResetAll = new javax.swing.JButton();
        menubar = new javax.swing.JMenuBar();
        menuSystem = new javax.swing.JMenu();
        itemExit = new javax.swing.JMenuItem();
        menuOperation = new javax.swing.JMenu();
        itemSet = new javax.swing.JMenuItem();
        itemReset = new javax.swing.JMenuItem();
        itemResetAll = new javax.swing.JMenuItem();

        javax.swing.GroupLayout pnlCustomerLayout = new javax.swing.GroupLayout(pnlCustomer);
        pnlCustomer.setLayout(pnlCustomerLayout);
        pnlCustomerLayout.setHorizontalGroup(
            pnlCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        pnlCustomerLayout.setVerticalGroup(
            pnlCustomerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        cbbCategory.setMaximumRowCount(15);

        tblConfig.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblConfig);

        btnSet.setText(constBundle.getString("set"));

        btnReset.setText(constBundle.getString("reset")
        );

        btnResetAll.setText(constBundle.getString("resetAll"));

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 521, Short.MAX_VALUE)
                    .addGroup(pnlMainLayout.createSequentialGroup()
                        .addComponent(cbbCategory, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(btnSet)
                        .addGap(10, 10, 10)
                        .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btnResetAll, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMainLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbbCategory, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSet)
                    .addComponent(btnReset)
                    .addComponent(btnResetAll))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 381, Short.MAX_VALUE)
                .addContainerGap())
        );

        menuSystem.setText(constBundle.getString("system"));

        itemExit.setText(constBundle.getString("exit"));
        menuSystem.add(itemExit);

        menubar.add(menuSystem);

        menuOperation.setText(constBundle.getString("operation")
        );

        itemSet.setText(constBundle.getString("set")
        );
        menuOperation.add(itemSet);

        itemReset.setText(constBundle.getString("reset")
        );
        menuOperation.add(itemReset);

        itemResetAll.setText(constBundle.getString("resetAll")
        );
        menuOperation.add(itemResetAll);

        menubar.add(menuOperation);

        setJMenuBar(menubar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void itemExitActionPerformed(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        this.dispose();
    }
    private void cbbCategoryStateChanged(ItemEvent evt)
    {
        String category = cbbCategory.getSelectedItem().toString();
        DefaultTableModel m = (DefaultTableModel) tblConfig.getModel();
        int rowCount = m.getRowCount();
        if (rowCount > 0)
        {
            for (int i = rowCount - 1; i >= 0; i--)
            {
                m.removeRow(i);
            }
        }
        try
        {
            Controller.getInstance().getArgsByCategory(helper, category, m);
            tblConfig.validate();
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    private void btnSetAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        int row = tblConfig.getRowCount();
        if (row <= 0)
        {
            return;
        }
        for (int i = 0; i < row; i++)
        {
            if (tblConfig.getValueAt(i, 0).equals(true))
            {
                try
                {
                    Controller.getInstance().setArg(helper, tblConfig.getValueAt(i, 1).toString(), tblConfig.getValueAt(i, 2));
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("warning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    private void btnResetAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        int row = tblConfig.getRowCount();
        if (row <= 0)
        {
            return;
        }
        for (int i = 0; i < row; i++)
        {
            if (tblConfig.getValueAt(i, 0).equals(true))
            {
                try
                {
                    Object defaultValue = Controller.getInstance().resetArg(helper, tblConfig.getValueAt(i, 1).toString());
                    tblConfig.setValueAt(defaultValue, i, 1);
                } catch (Exception ex)
                {
                    JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("warning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }
    private void btnResetAllAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        try
        {
            Controller.getInstance().resetAllArg(helper);
        } catch (Exception ex)
        {
            JOptionPane.showMessageDialog(this, ex.getMessage(), constBundle.getString("warning"), JOptionPane.ERROR_MESSAGE);
        }
    }
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[])
//    {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try
//        {
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex)
//        {
//            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        EventQueue.invokeLater(new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                LoginDialog login = new LoginDialog(null, true);
//                login.setLocationRelativeTo(null);
//                login.setVisible(true);
//                if (login.isFinish())
//                {
//                    MainView main = new MainView(login.getHelperInfo());
//                    main.setLocationRelativeTo(null);
//                    main.setVisible(true);
//                }
//            }
//        });
    
//    }

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnResetAll;
    private javax.swing.JButton btnSet;
    private javax.swing.JComboBox cbbCategory;
    private javax.swing.JMenuItem itemExit;
    private javax.swing.JMenuItem itemReset;
    private javax.swing.JMenuItem itemResetAll;
    private javax.swing.JMenuItem itemSet;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenu menuOperation;
    private javax.swing.JMenu menuSystem;
    private javax.swing.JMenuBar menubar;
    private javax.swing.JPanel pnlCustomer;
    private javax.swing.JPanel pnlMain;
    private javax.swing.JTable tblConfig;
    // End of variables declaration//GEN-END:variables
}
