/* ------------------------------------------------ 
* 
* File: Controller.java
*
* Abstract: 
*       .
*
* Authored by liuyuanyuan@highgo.com，20180713.
*
* Copyright:
* Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
* All rights reserved .   
*
* Identification:
*       src\com\highgo\hgdbadmin\serverconfig\controller\Controller.java
*
*-------------------------------------------------
*/

package com.highgo.hgdbadmin.serverconfig.controller;

import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class Controller
{
    private Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private static Controller c = null;
    public static Controller getInstance()
    {
        if (c == null)
        {
            c = new Controller();
        }
        return c;
    }
    
    public boolean getCatalog(HelperInfoDTO helper, DefaultComboBoxModel model) throws Exception
    {
        boolean flag = false;
        Connection conn = null;
        PreparedStatement ppst = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helper.getPartURL()+helper.getDbName(), helper);
            String sql = "SELECT DISTINCT category FROM pg_settings ORDER BY category;";
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            rs = ppst.executeQuery();
            while (rs.next())
            {
                model.addElement(rs.getString(1));
            }
            flag = true;
        } catch (Exception ex)
        {
            flag = false;
            logger.info(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(ppst);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        return flag;
    }

    public boolean getArgsByCategory(HelperInfoDTO helper, String category, DefaultTableModel model) throws Exception
    {
        logger.info("Enter: " + category);
        boolean flag = false;
        Connection conn = null;
        PreparedStatement ppst = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helper.getPartURL()+helper.getDbName(), helper);
            String sql = "SELECT name, setting, unit, vartype, min_val, max_val, source, context, short_desc"
                    + " FROM pg_settings"
                    + " WHERE category=?"
                    + " ORDER BY name";
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            ppst.setString(1, category);
            rs = ppst.executeQuery();
            Object[] row;
            while (rs.next())
            {
                row = new Object[7];
                row[0] = false;
                row[1] = rs.getString(1);
                row[2] = rs.getString(2);
                row[3] = rs.getString(3);                
                row[4] = rs.getString(5);
                row[5] = rs.getString(6);
                row[6] = rs.getString(9);
                model.addRow(row);
            }
            flag = true;
        } catch (Exception ex)
        {
            flag = false;
            logger.info(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(ppst);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        return flag;
    }

    public boolean setArg(HelperInfoDTO helper, String argName, Object argSetting) throws Exception
    {
        logger.info("argument name:" + argName + ", setting:" + argSetting);
        boolean flag = false;
        Connection conn = null;
        PreparedStatement ppst = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helper.getPartURL()+helper.getDbName(), helper);
            String sql = "ALTER SYSTEM SET " + argName + "=" + argSetting;
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            flag = ppst.execute();
        } catch (Exception ex)
        {
            flag = false;
            logger.info(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(ppst);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        return flag;
    }
    
    public Object resetArg(HelperInfoDTO helper, String arg) throws Exception
    {
        Object defaultValue = null;
        Connection conn = null;
        PreparedStatement ppst = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helper.getPartURL()+helper.getDbName(), helper);
            String sql = "ALTER SYSTEM RESET " + arg;
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            ppst.execute();
            sql = "SELECT DISTINCT setting FROM pg_settings WHERE name='" + arg + "'";
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            rs = ppst.executeQuery();
            while (rs.next())
            {
                defaultValue = rs.getObject(1);
            }
        } catch (Exception ex)
        {
            logger.info(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(ppst);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        return defaultValue;
    }

    public boolean resetAllArg(HelperInfoDTO helper) throws Exception
    {
        boolean flag = false;
        Connection conn = null;
        PreparedStatement ppst = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helper.getPartURL()+helper.getDbName(), helper);
            String sql = "ALTER SYSTEM RESET ALL";
            logger.info(sql);
            ppst = conn.prepareStatement(sql);
            flag = ppst.execute();
            ppst.clearParameters();
        } catch (Exception ex)
        {
            flag = false;
            logger.info(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(ppst);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        return flag;
    }
    
    
}
