/* ------------------------------------------------
 *
 * File: IllegalEventDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\view\IllegalEventDialog.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.view;

import com.highgo.hgdbadmin.audit.controller.MonitorController;
import com.highgo.hgdbadmin.audit.model.IllegalEventInfoDTO;
import com.highgo.hgdbadmin.audit.model.HelperInfoDTO;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Yuanyuan
 */
public class IllegalEventDialog extends JDialog
{
    
    private Logger logger = LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants_audit");

    private boolean isSuccess;
    private IllegalEventInfoDTO eventInfo;
    private HelperInfoDTO helperInfo;
    
    /**
     * Creates new form DetailDialog
     *
     * @param parent
     * @param modal
     * @param helperInfo
     * @param eventInfo
     */
    public IllegalEventDialog(Frame parent, boolean modal, HelperInfoDTO helperInfo, IllegalEventInfoDTO eventInfo)
    {
        super(parent, modal);        
        initComponents();
        isSuccess = false;
        this.helperInfo = helperInfo;
        this.eventInfo = eventInfo;
//        String[] objectTypes = new String[]
//        {
//            "", "user", "tablespace", "database", "schema",
//            "sequence", "table", "view", "rule", "index", "function", "trigger"
//        };
//        cbObjType.setModel(new DefaultComboBoxModel(objectTypes));
        /*
        if (eventInfo == null)
        {
            setTitle(constBundle.getString("customIllegalEvent"));
            setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/hgdbmonitor/images/create.png")));
            cbEvent.setEnabled(true);
            cbObjType.setEnabled(true);
            cbEvent.setSelectedIndex(-1);
            cbObjType.setSelectedIndex(-1);
            cbAction.setSelectedIndex(-1);
            cbEvent.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent evt)
                {                    
                    cbEventItemStateChanged(evt);
                    enableBtnOkWhenAddEvent();
                }
            });
            cbObjType.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent evt)
                {
                    cbObjTypeItemStateChanged(evt);
                    enableBtnOkWhenAddEvent();
                }
            });
            cbObjName.addItemListener(new ItemListener()
            {

                @Override
                public void itemStateChanged(ItemEvent evt)
                {
                    cbObjNameItemStateChanged(evt);
                    enableBtnOkWhenAddEvent();
                }
            }); 
            cbOperation.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    enableBtnOkWhenAddEvent();
                }                
            });
            txfdEventCount.addKeyListener(new KeyAdapter()
            {
                @Override
                public void keyReleased(KeyEvent evt)
                {
                    enableBtnOkWhenAddEvent();
                }
            });
            cbAction.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent evt)
                {
                    cbActionItemStateChangedWhenAdd(evt);
                }
            });
         }else
         */
        setTitle(constBundle.getString("editIllegalEvent"));
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/audit/property.png")));
        cbEvent.addItem(eventInfo.getEventName());
        cbEvent.setSelectedIndex(0);
        cbObjType.addItem(eventInfo.getObjectType());
        cbObjType.setSelectedIndex(0);
        txfdObjOid.setText(eventInfo.getObjectOid());
        cbObjName.addItem(eventInfo.getObjectName());
        cbObjName.setSelectedIndex(0);
        txfdEventCount.setText(eventInfo.getCount());
        cbOperation.addItem(eventInfo.getOperation());
        cbOperation.setSelectedIndex(0);
        ttarDescription.setText(eventInfo.getDescription());
        MonitorController mc = MonitorController.getInstance();
        cbAction.setModel(new DefaultComboBoxModel(mc.getActionByEventId(eventInfo.getEventId())));
        cbAction.setSelectedItem(eventInfo.getAction());
        ttarDescription.addKeyListener(new KeyAdapter()
        {
            @Override
            public void keyReleased(KeyEvent evt)
            {
                ttarDescriptionKeyReleased(evt);
            }
        });
        cbAction.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                cbActionItemStateChangedWhenEdit(evt);
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        lblEventName = new javax.swing.JLabel();
        lblObjOid = new javax.swing.JLabel();
        lblEventCount = new javax.swing.JLabel();
        lblOperation = new javax.swing.JLabel();
        txfdObjOid = new javax.swing.JTextField();
        lblObjType = new javax.swing.JLabel();
        lblObjName = new javax.swing.JLabel();
        txfdEventCount = new javax.swing.JTextField();
        lblAction = new javax.swing.JLabel();
        lblDescription = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        ttarDescription = new javax.swing.JTextArea();
        btnOk = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        cbAction = new javax.swing.JComboBox();
        cbEvent = new javax.swing.JComboBox();
        cbObjType = new javax.swing.JComboBox();
        cbObjName = new javax.swing.JComboBox();
        cbOperation = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(constBundle.getString("editIllegalEvent"));

        lblEventName.setText(constBundle.getString("illegalEvent"));

        lblObjOid.setText(constBundle.getString("objectOid"));

        lblEventCount.setText(constBundle.getString("failedLoginCount"));

        lblOperation.setText(constBundle.getString("operation")
        );

        txfdObjOid.setEnabled(false);
        txfdObjOid.setPreferredSize(new java.awt.Dimension(60, 21));

        lblObjType.setText(constBundle.getString("objectType"));

        lblObjName.setText(constBundle.getString("objectName"));

        txfdEventCount.setEnabled(false);

        lblAction.setText(constBundle.getString("action"));

        lblDescription.setText(constBundle.getString("description"));

        ttarDescription.setColumns(20);
        ttarDescription.setRows(5);
        jScrollPane1.setViewportView(ttarDescription);

        btnOk.setText(constBundle.getString("ok"));
        btnOk.setEnabled(false);
        btnOk.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnOkActionPerformed(evt);
            }
        });

        btnCancle.setText(constBundle.getString("cancle"));
        btnCancle.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnCancleActionPerformed(evt);
            }
        });

        cbEvent.setEnabled(false);

        cbObjType.setEnabled(false);
        cbObjType.setPreferredSize(new java.awt.Dimension(100, 21));

        cbObjName.setEnabled(false);
        cbObjName.setPreferredSize(new java.awt.Dimension(150, 21));

        cbOperation.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblEventCount, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblOperation, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(10, 10, 10)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txfdEventCount)
                                    .addComponent(cbOperation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lblEventName, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbEvent, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblAction, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbAction, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblDescription, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane1))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblObjType, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(cbObjType, 0, 74, Short.MAX_VALUE)
                                .addGap(10, 10, 10)
                                .addComponent(lblObjOid)
                                .addGap(10, 10, 10)
                                .addComponent(txfdObjOid, javax.swing.GroupLayout.DEFAULT_SIZE, 74, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblObjName)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbObjName, 0, 87, Short.MAX_VALUE)))
                        .addGap(10, 10, 10))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnOk)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancle)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEventName)
                    .addComponent(cbEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblObjOid)
                    .addComponent(txfdObjOid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblObjType)
                    .addComponent(lblObjName)
                    .addComponent(cbObjType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbObjName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEventCount)
                    .addComponent(txfdEventCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOperation)
                    .addComponent(cbOperation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAction)
                    .addComponent(cbAction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDescription)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOk)
                    .addComponent(btnCancle))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancleActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCancleActionPerformed
    {//GEN-HEADEREND:event_btnCancleActionPerformed
        // TODO add your handling code here:
        logger.info("cancle");
        this.dispose();
    }//GEN-LAST:event_btnCancleActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOkActionPerformed
    {//GEN-HEADEREND:event_btnOkActionPerformed
        // TODO add your handling code here:
        logger.info("ok");
        isSuccess = true;
        this.dispose();
    }//GEN-LAST:event_btnOkActionPerformed
    //for edit
    private void ttarDescriptionKeyReleased(java.awt.event.KeyEvent evt)
    {
        String description = ttarDescription.getText();
        logger.info("description:" + description + ",isNull:" + (description == null));
        if (eventInfo.getDescription() != null && !description.equals(eventInfo.getDescription()))
        {
            btnOk.setEnabled(true);
        } else if (eventInfo.getDescription() == null && !description.isEmpty())
        {
            btnOk.setEnabled(true);
        } else
        {
            btnOk.setEnabled(false);
        }
    }
    
    private void cbActionItemStateChangedWhenEdit(ItemEvent evt)
    {
        int actionIndex = cbAction.getSelectedIndex();
        logger.info("actionIndex=" + actionIndex);
        if (actionIndex < 0)
        {
            return;
        }
        logger.info("action=" + actionIndex);
        if (actionIndex >= 0 && !cbAction.getSelectedItem().toString().equals(eventInfo.getAction()))
        {
            btnOk.setEnabled(true);
        } else
        {
            btnOk.setEnabled(false);
        }
    }
    
    public boolean isSuccess()
    {
        return this.isSuccess;
    }

    public IllegalEventInfoDTO getIllegalEventInfo()
    {
        IllegalEventInfoDTO newEvent = new IllegalEventInfoDTO();
        newEvent.setAction(cbAction.getSelectedItem().toString());
        if (ttarDescription.getText().isEmpty())
        {
            newEvent.setDescription(null);
        } else
        {
            newEvent.setDescription(ttarDescription.getText());
        }
        return newEvent;
    }
    
    /*
    private void btnOkActionPerformed(java.awt.event.ActionEvent evt)                                      
    {                                          
        // TODO add your handling code here:
        logger.info("ok");
        isSuccess = true;
        this.dispose();
//        MonitorController mc = MonitorController.getInstance();
//        try
//        {
//            if (eventInfo == null)
//            {
//                IllegalEventInfoDTO newEvent = this.getIllegalEventInfo();
//                if (newEvent.getObjectType() != null && newEvent.getObjectType().equals("user"))
//                {
//                    if (mc.isUserExistNow(helperInfo, newEvent.getObjectOid()))
//                    {
//                        mc.insertIllegalEvent(helperInfo, this.getIllegalEventInfo());
//                        isSuccess = true;
//                        this.dispose();
//                    } else
//                    {
//                        JOptionPane.showMessageDialog(this, "Warning: user '" + newEvent.getObjectName() + "' just been dropd, the audit rule cannot be insert,",
//                                constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
//                        cbObjName.removeItem(cbObjName.getSelectedItem());
//                    }
//                } else
//                {
//                    mc.insertIllegalEvent(helperInfo, this.getIllegalEventInfo());
//                    isSuccess = true;
//                    this.dispose();
//                }
//            } else
//            {
//                mc.updateIllegalEvent(helperInfo, this.getIllegalEventInfo());
//                isSuccess = true;
//                this.dispose();
//            }            
//        } catch (Exception ex)
//        {
//            isSuccess = false;
//            logger.error("Error " + ex.getMessage());
//            ex.printStackTrace(System.out);
//            JOptionPane.showMessageDialog(this, ex.getMessage(),
//                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
//        }
    }                           
    //for add
    private void cbActionItemStateChangedWhenAdd(ItemEvent evt)
    {
        // TODO add your handling code here:
        if (cbAction.getSelectedIndex() < 0)
        {
            return;
        }
        enableBtnOkWhenAddEvent();
    }
    
    private void cbEventItemStateChanged(ItemEvent evt)       
    {
        if (cbEvent.getSelectedIndex() < 0)
        {
            return;
        }
        String selectedEvent = cbEvent.getSelectedItem().toString();
        logger.info("selectedEvent = " + selectedEvent);
        //for item  
        MonitorController mc = MonitorController.getInstance();
        String[] objTypes = mc.getObjTypeArrayByEvent(selectedEvent);
        cbObjType.setModel(new DefaultComboBoxModel(objTypes));
        cbObjType.setSelectedIndex(-1);
        //for enable
        if (selectedEvent.equals("wrong_pwd_login_failure"))
        {
            txfdEventCount.setEnabled(true);
            cbOperation.setEnabled(false);
            cbObjType.setEnabled(true);
            cbObjName.setEnabled(true);
        } else if (selectedEvent.equals("object_not_exist"))
        {
            txfdEventCount.setEnabled(false);
            cbOperation.setEnabled(true);
            cbObjType.setEnabled(false);
            cbObjName.setEnabled(false);
        } else
        {
            txfdEventCount.setEnabled(false);
            cbOperation.setEnabled(true);
            cbObjType.setEnabled(true);
            cbObjName.setEnabled(true);
        }
        switch (selectedEvent)
        {
            case "illegal_data_access":
            case "illegal_data_modify":
            case "illegal_defination_modify":
                cbAction.setModel(new DefaultComboBoxModel(new String[]{"log"}));
                break;
            default:
                String[] actions = new String[]
                {
                    "log", "warn", "disconnect", "lock"
                };
                cbAction.setModel(new DefaultComboBoxModel(actions));
                break;

        }       
    }

    private void cbObjTypeItemStateChanged(ItemEvent evt)
    {
        if (cbObjType.getSelectedIndex() < 0)
        {
            return;
        }
        String selectedEvent = cbEvent.getSelectedItem().toString();
        String objType = cbObjType.getSelectedItem().toString();
        MonitorController mc = MonitorController.getInstance();
        cbOperation.setModel(new DefaultComboBoxModel(mc.getOperationArrayByEvent(selectedEvent)));
        cbOperation.setSelectedIndex(-1);
        try
        {
            List<ObjectInfoDTO> objList = mc.getObjectInfoList(helperInfo, objType);
            cbObjName.removeAllItems();
            for (ObjectInfoDTO objInfo : objList)
            {
                cbObjName.addItem(objInfo);
            }
            cbObjName.setSelectedIndex(-1);
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
      
    private void cbObjNameItemStateChanged(ItemEvent evt)
    {
        logger.info("ObjName Selected Index ="+cbObjName.getSelectedIndex() );
        if (cbObjName.getSelectedIndex() < 0)
        {
            txfdObjOid.setText(null);
            return;
        }
        ObjectInfoDTO obj = (ObjectInfoDTO) cbObjName.getSelectedItem();
        txfdObjOid.setText(obj.getOid());
    }
        
    private void enableBtnOkWhenAddEvent()
    {
        if (cbEvent.getSelectedIndex() < 0 || cbAction.getSelectedIndex() < 0)
        {
            btnOk.setEnabled(false);
        } else
        {
            String event = cbEvent.getSelectedItem().toString();
            switch (event)
            {
                case "wrong_pwd_login_failure":
                    if (cbObjType.getSelectedIndex() >= 0
                            && cbObjName.getSelectedIndex() >= 0
                            && txfdEventCount.getText() != null
                            && !txfdEventCount.getText().isEmpty())
                    {
                        btnOk.setEnabled(true);
                    } else
                    {
                        btnOk.setEnabled(false);
                    }
                    break;
                case "object_not_exist":
                    if (cbOperation.getSelectedIndex() >= 0)
                    {
                        btnOk.setEnabled(true);
                    } else
                    {
                        btnOk.setEnabled(false);
                    }
                    break;
                default:
                    if (cbObjType.getSelectedIndex() >= 0
                            && cbObjName.getSelectedIndex() >= 0
                            && cbOperation.getSelectedIndex() >= 0)
                    {
                        btnOk.setEnabled(true);
                    } else
                    {
                        btnOk.setEnabled(false);
                    }
            }
        }
    }
    
    //for both 
    public IllegalEventInfoDTO getIllegalEventInfo()
    {
        if (this.eventInfo == null)
        {
            IllegalEventInfoDTO newEventInfo = new IllegalEventInfoDTO();
            String event = cbEvent.getSelectedItem().toString();
            newEventInfo.setName(event);
            newEventInfo.setAction(cbAction.getSelectedItem().toString());
            switch (event)
            {
                case "wrong_pwd_login_failure":
                    newEventInfo.setObjectType(cbObjType.getSelectedItem().toString());
                    ObjectInfoDTO obj  = (ObjectInfoDTO) cbObjName.getSelectedItem();
                    newEventInfo.setObjectOid(obj.getOid());
                    newEventInfo.setObjectName(obj.getName());
                    newEventInfo.setCount(txfdEventCount.getText());
                    newEventInfo.setDescription(ttarDescription.getText());
                    return newEventInfo;
                case  "object_not_exist":
                     newEventInfo.setOperation(cbOperation.getSelectedItem().toString());
                     newEventInfo.setDescription(ttarDescription.getText());
                    return newEventInfo;
                default:
                    newEventInfo.setObjectType(cbObjType.getSelectedItem().toString());
                    ObjectInfoDTO obj2  = (ObjectInfoDTO) cbObjName.getSelectedItem();
                    newEventInfo.setObjectOid(obj2.getOid());
                    newEventInfo.setObjectName(obj2.getName());
                    newEventInfo.setOperation(cbOperation.getSelectedItem().toString());
                    newEventInfo.setDescription(ttarDescription.getText());
                    return newEventInfo;
            }
        } else
        {
            this.eventInfo.setAction(cbAction.getSelectedItem().toString());
            this.eventInfo.setDescription(ttarDescription.getText());
            return this.eventInfo;
        }
    }
*/
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnOk;
    private javax.swing.JComboBox cbAction;
    private javax.swing.JComboBox cbEvent;
    private javax.swing.JComboBox cbObjName;
    private javax.swing.JComboBox cbObjType;
    private javax.swing.JComboBox cbOperation;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblAction;
    private javax.swing.JLabel lblDescription;
    private javax.swing.JLabel lblEventCount;
    private javax.swing.JLabel lblEventName;
    private javax.swing.JLabel lblObjName;
    private javax.swing.JLabel lblObjOid;
    private javax.swing.JLabel lblObjType;
    private javax.swing.JLabel lblOperation;
    private javax.swing.JTextArea ttarDescription;
    private javax.swing.JTextField txfdEventCount;
    private javax.swing.JTextField txfdObjOid;
    // End of variables declaration//GEN-END:variables
}
