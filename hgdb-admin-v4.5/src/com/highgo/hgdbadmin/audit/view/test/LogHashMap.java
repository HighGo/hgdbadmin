/* ------------------------------------------------
 *
 * File: LogHashMap.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20190214.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\view\test\LogHashMap.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.view.test;

import java.util.HashMap;

/**
 *
 * @author Liu Yuanyuan
 */
public class LogHashMap<K,T> extends HashMap<K,T>
{
    @Override
    public String toString()
    {
        if (this.size() == 0)
        {
            return null;
        } else
        {
            return this.get("logtime").toString();
        }
    }
    
    
//    private Timestamp logTime;
//    private String user;
//    private String db;
//    private int procNum;
//    private String hostName;
//    private String sessionId;
//    private int lineNum;
//    private String operation;
//    private Timestamp startTime;
//    private String virtualTrasId;
//    private String trasId;
//    private String errServerity;
//    private String sqlStatCode;
//    private int userOid;
//    private String operSql;
//    private String falg;
    
    
    
}
