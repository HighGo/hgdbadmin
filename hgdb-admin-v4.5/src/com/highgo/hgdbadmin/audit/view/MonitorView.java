/* ------------------------------------------------
 *
 * File: MonitorView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\view\MonitorView.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.view;

import com.highgo.hgdbadmin.audit.controller.MonitorController;
import com.highgo.hgdbadmin.audit.model.AuditRuleInfoDTO;
import com.highgo.hgdbadmin.audit.model.IllegalEventInfoDTO;
import com.highgo.hgdbadmin.audit.model.HelperInfoDTO;
import com.highgo.hgdbadmin.audit.model.LabeledRecordInfoDTO;
import com.highgo.hgdbadmin.audit.model.LogInfoDTO;
import com.highgo.hgdbadmin.audit.model.ObjectDTO;
import com.highgo.hgdbadmin.audit.model.ObjectInfoDTO;
import com.highgo.hgdbadmin.audit.paging.SmartPagingModel;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.URI;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Liu Yuanyuan
 */
public class MonitorView extends JFrame
{
    private Logger logger = LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants_audit");
    private HelperInfoDTO helperInfo;
    
    private DefaultTableModel eventModel;
    private DefaultTableModel ruleModel;
    private Timestamp lastTime; 
    
    private DefaultTableModel obj4LabelModel;
    private DefaultTableModel recordModel;    
    
    
    private Timestamp logValidSince = new Timestamp(System.currentTimeMillis());;
    
    /**
     * Creates new form MonitorView
     * @param helperInfo connect information
     */
    public MonitorView(final HelperInfoDTO helperInfo)
    {
        this.helperInfo = helperInfo;
        initComponents();
        lastTime = new Timestamp(System.currentTimeMillis());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(("/com/highgo/hgdbadmin/audit/images/audit.png"))));
        this.setTitle(MessageFormat.format(constBundle.getString("hgdbMonitor"),
                helperInfo.getUser(), helperInfo.getHost(), helperInfo.getPort(), helperInfo.getDb()));
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                itemExitActionPerformed(null); 
            }
        });
        itemExit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                itemExitActionPerformed(evt);
            }
        });
        itemAboutUs.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemAboutUsActionPerformed(e);
            }
        });
        ActionListener helpAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                itemHelpActionPerformed(e);
            }
        };
        itemHelp.addActionListener(helpAction);
        btnHelp.addActionListener(helpAction);
        if (helperInfo.isAuditUser())
        {
            this.initAuditRuleView();
            this.initAuditLogView();      
            this.initLogStateMonitor();
        } else
        {
            this.initAuditLogView(); //add 
            this.initLogStateMonitor(); //add
            this.initIllegalEventView();
            this.initLabelView();
            this.initRecordLabelView();
            btnRefresh.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    int index = tabPane.getSelectedIndex();
                    if (index == 3)
                    {
                        filterIllegalEventActionPerformed(null);
                    } else if (index == 4)
                    {
                        refreshLabelData();
                    }else if(index ==5)
                    {
                        refreshLabelTable();
                    }
                }
            });
            tabPane.addChangeListener(new ChangeListener()
            {
                @Override
                public void stateChanged(ChangeEvent e)
                {
                    tbPanel4ssoStateChanged(e);
                }
            });            
        }
        
        /*
        btnConfig.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
           {
                btnConfigActionPerformed(e);
           }
        });
        */
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        btnHelp = new javax.swing.JButton();
        menuHelp = new javax.swing.JMenu();
        itemHelp = new javax.swing.JMenuItem();
        sepHelp = new javax.swing.JPopupMenu.Separator();
        itemAboutUs = new javax.swing.JMenuItem();
        cbTable4Label = new javax.swing.JComboBox();
        tabPane = new javax.swing.JTabbedPane();
        pnlState = new javax.swing.JPanel();
        spTableState = new javax.swing.JScrollPane();
        tblLogState = new javax.swing.JTable();
        pnlAuditLog = new javax.swing.JPanel();
        lblStartTime = new javax.swing.JLabel();
        lblEndTime = new javax.swing.JLabel();
        btnQueryLog = new javax.swing.JButton();
        spLog = new javax.swing.JScrollPane();
        tblAuditLog = new javax.swing.JTable();
        spinStart = new javax.swing.JSpinner();
        spinEnd = new javax.swing.JSpinner();
        lblUser = new javax.swing.JLabel();
        txfUser = new javax.swing.JTextField();
        spAuditRule = new javax.swing.JScrollPane();
        pnlAuditRule = new javax.swing.JPanel();
        cbObjTypeSort = new javax.swing.JComboBox();
        cbOperationSort = new javax.swing.JComboBox();
        cbUserSort = new javax.swing.JComboBox();
        spTableRule = new javax.swing.JScrollPane();
        tblRule = new javax.swing.JTable();
        cbResultSort = new javax.swing.JComboBox();
        lblQuery = new javax.swing.JLabel();
        cbObjTypeAdd = new javax.swing.JComboBox();
        cbObjNameAdd = new javax.swing.JComboBox();
        cbUserAdd = new javax.swing.JComboBox();
        cbOperationAdd = new javax.swing.JComboBox();
        cbResultAdd = new javax.swing.JComboBox();
        btnAddRule = new javax.swing.JButton();
        btnBatchCommit = new javax.swing.JButton();
        lblCustomRule = new javax.swing.JLabel();
        spIllegalEvent = new javax.swing.JScrollPane();
        pnlIllegalEvent = new javax.swing.JPanel();
        cbEventSort = new javax.swing.JComboBox();
        cbObjTypeForEventSort = new javax.swing.JComboBox();
        cbActionSort = new javax.swing.JComboBox();
        spTableEvent = new javax.swing.JScrollPane();
        tblEvent = new javax.swing.JTable();
        lblEvent = new javax.swing.JLabel();
        cbEvent = new javax.swing.JComboBox();
        cbObjTypeForEvent = new javax.swing.JComboBox();
        cbObjNameForEvent = new javax.swing.JComboBox();
        cbOperationForEvent = new javax.swing.JComboBox();
        txfdFailedTime = new javax.swing.JTextField();
        cbAction = new javax.swing.JComboBox();
        lblCustomEvent = new javax.swing.JLabel();
        btnAddEvent = new javax.swing.JButton();
        btnBatchCommitForEvent = new javax.swing.JButton();
        lblEventCount = new javax.swing.JLabel();
        pnlObjLabel = new javax.swing.JPanel();
        cbObjType4Label = new javax.swing.JComboBox();
        spLabel = new javax.swing.JScrollPane();
        tblLabel = new javax.swing.JTable();
        spnSensitivity = new javax.swing.JSpinner();
        btnUpdate = new javax.swing.JButton();
        lblSensitivity = new javax.swing.JLabel();
        cbRowLimit = new javax.swing.JComboBox();
        pnlRecordLabel = new javax.swing.JPanel();
        cbTables = new javax.swing.JComboBox();
        spRecord = new javax.swing.JScrollPane();
        tblRecord = new javax.swing.JTable();
        spinRecordSensitivity = new javax.swing.JSpinner();
        btnUpdateRecordSensitivity = new javax.swing.JButton();
        lblTableName = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        toolBar = new javax.swing.JToolBar();
        btnEdit = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnConfig = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        menuFile = new javax.swing.JMenu();
        itemExit = new javax.swing.JMenuItem();
        menuAudit = new javax.swing.JMenu();
        itemEditAuditRule = new javax.swing.JMenuItem();
        itemDeleteAuditRule = new javax.swing.JMenuItem();
        menuSecurity = new javax.swing.JMenu();
        itemEditIllegalEvent = new javax.swing.JMenuItem();
        itemDeleteIllegalEvent = new javax.swing.JMenuItem();

        btnHelp.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/audit/images/help2.png"))); // NOI18N
        btnHelp.setToolTipText(constBundle.getString("help"));
        btnHelp.setFocusable(false);
        btnHelp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnHelp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        menuHelp.setText(constBundle.getString("help"));

        itemHelp.setText(constBundle.getString("help"));
        menuHelp.add(itemHelp);
        menuHelp.add(sepHelp);

        itemAboutUs.setText(constBundle.getString("aboutUs"));
        menuHelp.add(itemAboutUs);

        cbTable4Label.setMaximumRowCount(16);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabPane.setBackground(new java.awt.Color(255, 255, 255));
        tabPane.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));

        spTableState.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        spTableState.setViewportView(tblLogState);
        if (tblLogState.getColumnModel().getColumnCount() > 0)
        {
            tblLogState.getColumnModel().getColumn(0).setPreferredWidth(30);
            tblLogState.getColumnModel().getColumn(1).setPreferredWidth(30);
        }

        javax.swing.GroupLayout pnlStateLayout = new javax.swing.GroupLayout(pnlState);
        pnlState.setLayout(pnlStateLayout);
        pnlStateLayout.setHorizontalGroup(
            pnlStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 722, Short.MAX_VALUE)
            .addGroup(pnlStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spTableState, javax.swing.GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE))
        );
        pnlStateLayout.setVerticalGroup(
            pnlStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 406, Short.MAX_VALUE)
            .addGroup(pnlStateLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(spTableState, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 406, Short.MAX_VALUE))
        );

        tabPane.addTab(constBundle.getString("auditLogMonitor"), pnlState);

        pnlAuditLog.setBackground(new java.awt.Color(255, 255, 255));

        lblStartTime.setText(constBundle.getString("startTime"));

        lblEndTime.setText(constBundle.getString("endTime"));

        btnQueryLog.setText(constBundle.getString("queryLog"));

        spLog.setViewportView(tblAuditLog);

        spinStart.setModel(new javax.swing.SpinnerDateModel());

        spinEnd.setModel(new javax.swing.SpinnerDateModel());

        lblUser.setText(constBundle.getString("user"));

        javax.swing.GroupLayout pnlAuditLogLayout = new javax.swing.GroupLayout(pnlAuditLog);
        pnlAuditLog.setLayout(pnlAuditLogLayout);
        pnlAuditLogLayout.setHorizontalGroup(
            pnlAuditLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAuditLogLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAuditLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spLog)
                    .addGroup(pnlAuditLogLayout.createSequentialGroup()
                        .addComponent(lblStartTime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(lblEndTime)
                        .addGap(5, 5, 5)
                        .addComponent(spinEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblUser)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txfUser, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnQueryLog, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pnlAuditLogLayout.setVerticalGroup(
            pnlAuditLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAuditLogLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlAuditLogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblStartTime)
                    .addComponent(btnQueryLog)
                    .addComponent(spinStart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinEnd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEndTime)
                    .addComponent(lblUser)
                    .addComponent(txfUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(spLog, javax.swing.GroupLayout.DEFAULT_SIZE, 353, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabPane.addTab(constBundle.getString("auditLog"), pnlAuditLog);

        spAuditRule.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        pnlAuditRule.setBackground(new java.awt.Color(255, 255, 255));

        cbObjTypeSort.setMaximumRowCount(13);

        cbOperationSort.setMaximumRowCount(15);
        cbOperationSort.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));

        spTableRule.setBackground(new java.awt.Color(255, 255, 255));

        tblRule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblRule.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        spTableRule.setViewportView(tblRule);

        lblQuery.setBackground(new java.awt.Color(255, 255, 255));
        lblQuery.setText(constBundle.getString("conditionQuery")
        );

        cbObjTypeAdd.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));
        cbObjTypeAdd.setMaximumRowCount(13);

        cbObjNameAdd.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));

        cbUserAdd.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));

        cbOperationAdd.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));
        cbOperationAdd.setMinimumSize(new java.awt.Dimension(90, 21));

        cbResultAdd.setBackground(javax.swing.UIManager.getDefaults().getColor("Button.light"));

        btnAddRule.setForeground(javax.swing.UIManager.getDefaults().getColor("Button.light"));
        btnAddRule.setText(constBundle.getString("addRule"));
        btnAddRule.setEnabled(false);

        btnBatchCommit.setText(constBundle.getString("batchEnable"));

        lblCustomRule.setBackground(new java.awt.Color(255, 255, 255));
        lblCustomRule.setText(constBundle.getString("customRule"));

        javax.swing.GroupLayout pnlAuditRuleLayout = new javax.swing.GroupLayout(pnlAuditRule);
        pnlAuditRule.setLayout(pnlAuditRuleLayout);
        pnlAuditRuleLayout.setHorizontalGroup(
            pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAuditRuleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(spTableRule)
                    .addGroup(pnlAuditRuleLayout.createSequentialGroup()
                        .addGroup(pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlAuditRuleLayout.createSequentialGroup()
                                .addComponent(lblQuery, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(cbObjTypeSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbUserSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbOperationSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbResultSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlAuditRuleLayout.createSequentialGroup()
                                .addComponent(lblCustomRule, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(cbObjTypeAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbObjNameAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbUserAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbOperationAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbResultAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAddRule, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnBatchCommit, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addGap(10, 10, 10))
        );
        pnlAuditRuleLayout.setVerticalGroup(
            pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlAuditRuleLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbObjTypeAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbObjNameAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddRule)
                    .addComponent(lblCustomRule)
                    .addComponent(cbUserAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbOperationAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbResultAdd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(pnlAuditRuleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbObjTypeSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbOperationSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbUserSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbResultSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblQuery)
                    .addComponent(btnBatchCommit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(spTableRule, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                .addContainerGap())
        );

        spAuditRule.setViewportView(pnlAuditRule);

        tabPane.addTab(constBundle.getString("auditRule"), spAuditRule);

        pnlIllegalEvent.setBackground(new java.awt.Color(255, 255, 255));

        cbEventSort.setMaximumRowCount(9);

        cbObjTypeForEventSort.setMaximumRowCount(13);
        cbObjTypeForEventSort.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " " }));

        tblEvent.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblEvent.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        spTableEvent.setViewportView(tblEvent);

        lblEvent.setText(constBundle.getString("conditionQuery"));

        cbEvent.setMaximumRowCount(9);

        cbObjTypeForEvent.setMaximumRowCount(13);

        txfdFailedTime.setEnabled(false);

        lblCustomEvent.setText(constBundle.getString("customEvent"));

        btnAddEvent.setText(constBundle.getString("addEvent"));
        btnAddEvent.setEnabled(false);

        btnBatchCommitForEvent.setText(constBundle.getString("batchEnable"));

        lblEventCount.setText(constBundle.getString("failedLoginCountInput"));
        lblEventCount.setEnabled(false);

        javax.swing.GroupLayout pnlIllegalEventLayout = new javax.swing.GroupLayout(pnlIllegalEvent);
        pnlIllegalEvent.setLayout(pnlIllegalEventLayout);
        pnlIllegalEventLayout.setHorizontalGroup(
            pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIllegalEventLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spTableEvent, javax.swing.GroupLayout.DEFAULT_SIZE, 816, Short.MAX_VALUE)
                    .addGroup(pnlIllegalEventLayout.createSequentialGroup()
                        .addComponent(lblEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(cbEventSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(cbObjTypeForEventSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbActionSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBatchCommitForEvent))
                    .addGroup(pnlIllegalEventLayout.createSequentialGroup()
                        .addComponent(lblCustomEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addGroup(pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnlIllegalEventLayout.createSequentialGroup()
                                .addComponent(lblEventCount)
                                .addGap(2, 2, 2)
                                .addComponent(txfdFailedTime, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbOperationForEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbAction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pnlIllegalEventLayout.createSequentialGroup()
                                .addComponent(cbEvent, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbObjTypeForEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(cbObjNameForEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnAddEvent)))))
                .addGap(10, 10, 10))
        );
        pnlIllegalEventLayout.setVerticalGroup(
            pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlIllegalEventLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbObjTypeForEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbObjNameForEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCustomEvent)
                    .addComponent(btnAddEvent))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbOperationForEvent, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txfdFailedTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbAction, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEventCount))
                .addGap(10, 10, 10)
                .addGroup(pnlIllegalEventLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbObjTypeForEventSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbActionSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbEventSort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEvent)
                    .addComponent(btnBatchCommitForEvent))
                .addGap(9, 9, 9)
                .addComponent(spTableEvent, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                .addGap(18, 18, 18))
        );

        spIllegalEvent.setViewportView(pnlIllegalEvent);

        tabPane.addTab(constBundle.getString("illegalEvent"), spIllegalEvent);

        pnlObjLabel.setBackground(new java.awt.Color(255, 255, 255));

        cbObjType4Label.setMaximumRowCount(16);

        tblLabel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblLabel.setAutoscrolls(false);
        spLabel.setViewportView(tblLabel);

        btnUpdate.setText(constBundle.getString("updateSentiviy"));

        lblSensitivity.setText(constBundle.getString("sensitivityRange"));

        javax.swing.GroupLayout pnlObjLabelLayout = new javax.swing.GroupLayout(pnlObjLabel);
        pnlObjLabel.setLayout(pnlObjLabelLayout);
        pnlObjLabelLayout.setHorizontalGroup(
            pnlObjLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlObjLabelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlObjLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlObjLabelLayout.createSequentialGroup()
                        .addComponent(cbObjType4Label, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbRowLimit, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSensitivity)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spnSensitivity, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUpdate))
                    .addComponent(spLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlObjLabelLayout.setVerticalGroup(
            pnlObjLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlObjLabelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlObjLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbObjType4Label, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdate)
                    .addComponent(spnSensitivity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSensitivity, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbRowLimit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(spLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabPane.addTab(constBundle.getString("objSensitivity"), pnlObjLabel);

        pnlRecordLabel.setBackground(new java.awt.Color(255, 255, 255));

        cbTables.setModel(new DefaultComboBoxModel(new String[]{constBundle.getString("tableNameSort")}));

        tblRecord.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {

            }
        ));
        tblRecord.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        spRecord.setViewportView(tblRecord);

        btnUpdateRecordSensitivity.setText(constBundle.getString("updateSentiviy"));

        lblTableName.setText(constBundle.getString("tableName"));

        jLabel1.setText(constBundle.getString("sensitivityRange"));

        javax.swing.GroupLayout pnlRecordLabelLayout = new javax.swing.GroupLayout(pnlRecordLabel);
        pnlRecordLabel.setLayout(pnlRecordLabelLayout);
        pnlRecordLabelLayout.setHorizontalGroup(
            pnlRecordLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRecordLabelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRecordLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlRecordLabelLayout.createSequentialGroup()
                        .addComponent(spRecord, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(pnlRecordLabelLayout.createSequentialGroup()
                        .addComponent(lblTableName)
                        .addGap(5, 5, 5)
                        .addComponent(cbTables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(5, 5, 5)
                        .addComponent(spinRecordSensitivity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btnUpdateRecordSensitivity)
                        .addGap(8, 8, 8))))
        );
        pnlRecordLabelLayout.setVerticalGroup(
            pnlRecordLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlRecordLabelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlRecordLabelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbTables, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(spinRecordSensitivity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnUpdateRecordSensitivity)
                    .addComponent(lblTableName)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(spRecord, javax.swing.GroupLayout.DEFAULT_SIZE, 357, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabPane.addTab(constBundle.getString("recordSensitivity"), pnlRecordLabel);

        toolBar.setFloatable(false);
        toolBar.setRollover(true);

        btnEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/audit/images/property.png"))); // NOI18N
        btnEdit.setToolTipText(constBundle.getString("viewOrEditProperty"));
        btnEdit.setEnabled(false);
        btnEdit.setFocusable(false);
        btnEdit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEdit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnEdit);

        btnRefresh.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/audit/images/refresh.png"))); // NOI18N
        btnRefresh.setToolTipText(constBundle.getString("refresh"));
        btnRefresh.setEnabled(false);
        btnRefresh.setFocusable(false);
        btnRefresh.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnRefresh.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnRefresh);

        btnDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/audit/images/drop.png"))); // NOI18N
        btnDelete.setToolTipText(constBundle.getString("delete"));
        btnDelete.setFocusable(false);
        btnDelete.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDelete.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnDelete);

        btnConfig.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/highgo/hgdbadmin/audit/images/config.png"))); // NOI18N
        btnConfig.setEnabled(false);
        btnConfig.setFocusable(false);
        btnConfig.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnConfig.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        toolBar.add(btnConfig);

        menuFile.setText(constBundle.getString("file"));

        itemExit.setText(constBundle.getString("exit"));
        menuFile.add(itemExit);

        menuBar.add(menuFile);

        menuAudit.setText(constBundle.getString("audit"));

        itemEditAuditRule.setText(constBundle.getString("editAuditRule"));
        itemEditAuditRule.setEnabled(false);
        menuAudit.add(itemEditAuditRule);

        itemDeleteAuditRule.setText(constBundle.getString("deleteAuditRule"));
        menuAudit.add(itemDeleteAuditRule);

        menuBar.add(menuAudit);

        menuSecurity.setText(constBundle.getString("security"));

        itemEditIllegalEvent.setText(constBundle.getString("editIllegalEvent"));
        itemEditIllegalEvent.setEnabled(false);
        menuSecurity.add(itemEditIllegalEvent);

        itemDeleteIllegalEvent.setText(constBundle.getString("deleteIllegalEvent"));
        menuSecurity.add(itemDeleteIllegalEvent);

        menuBar.add(menuSecurity);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 729, Short.MAX_VALUE)
            .addComponent(tabPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tabPane, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE))
        );

        tabPane.getAccessibleContext().setAccessibleName(constBundle.getString("auditLogMonitor")
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void itemExitActionPerformed(ActionEvent evt)
    {
        logger.info("exit");
        this.dispose();
        System.exit(0);
    }
    private void itemHelpActionPerformed(ActionEvent evt)
    {
        Desktop desktop = Desktop.getDesktop();
        try
        {
            desktop.browse(new URI("http://www.highgo.com/contact-us.html"));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
    private void itemAboutUsActionPerformed(ActionEvent evt)
    {
        Desktop desktop = Desktop.getDesktop();
        try
        {
            desktop.browse(new URI("http://www.highgo.com/enterprise-profile.html"));
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            try
            {
                Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "
                        + "http://www.highgo.com/enterprise-profile.html");
            } catch (IOException e)
            {
                logger.error(e.getMessage());
                e.printStackTrace(System.out);
            }
        }
    }
    private void emptyTableData(DefaultTableModel model)
    {
        int row = model.getRowCount();
        logger.info("current rowCount=" + row);
        if (row > 0)
        {
            for (int i = row - 1; i >= 0; i--)
            {
                model.removeRow(i);
            }
        }
    }

    //for sso
    private void tbPanel4ssoStateChanged(ChangeEvent e)
    {
        int index = tabPane.getSelectedIndex();
        //logger.info("SelectedTabIndex:" + index);
        if (index == 5)
        {
            btnEdit.setEnabled(false);
            btnDelete.setEnabled(false);
            menuSecurity.setEnabled(false);
        } else if (index == 4)
        {
            btnEdit.setEnabled(false);
            btnDelete.setEnabled(false);
            menuSecurity.setEnabled(false);
        } else if (index == 3)
        {
            btnDelete.setEnabled(true);
            menuSecurity.setEnabled(true);
        }
    }

    //for record label
    private final int PageSize = 100;
    private void initRecordLabelView()
    {       
        spinRecordSensitivity.setModel(new SpinnerNumberModel(0, 0, 32767, 1));
        //forbid  illegally character
        JFormattedTextField tf = ((JSpinner.NumberEditor) spinRecordSensitivity.getEditor()).getTextField();
        DefaultFormatterFactory factory = (DefaultFormatterFactory) tf.getFormatterFactory();
        NumberFormatter formatter = (NumberFormatter) factory.getDefaultFormatter();
        formatter.setAllowsInvalid(false);
        
        btnUpdateRecordSensitivity.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnUpdateRecordSensitivityAction(e);
            }
        });
          
        cbTables.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbTableStateChanged(e);
            }
        });

        //tblRecord.setModel(recordModel);
        tblRecord.getTableHeader().setReorderingAllowed(false);
        //tblRecord.getColumn(constBundle.getString("sensitivity")).setMaxWidth(80);
        //tblRecord.getColumn("").setMaxWidth(50);

        refreshLabelTable();
    }
    private void cbTableStateChanged(ItemEvent e)
    {
        int index = cbTables.getSelectedIndex();
        logger.info("index=" + index);
        if (index < 0)
        {
            return;
        }else if(index==0)
        {
            //tblRecord.setModel(new DefaultTableModel());
            //spRecord.setRowHeaderView(null);
            this.emptyTableData((DefaultTableModel) tblRecord.getModel());
            spRecord.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, null);
            spRecord.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, null);
            tblRecord.repaint();
            return;
        }

        String tableWithSchema = cbTables.getSelectedItem().toString();
        logger.info("tableWithSchema: " + tableWithSchema);  
        Object[] header4LabelRecord = new Object[]
        {
            constBundle.getString("record"),
            constBundle.getString("sensitivity"),
            ""
        };
        Class[] types = new Class[]
        {
            LabeledRecordInfoDTO.class,
            Object.class,
            Boolean.class
        };
        StringBuilder sql = new StringBuilder();        
        sql.append("select t.ctid,")
                .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('")
                .append(tableWithSchema).append("', ")
                .append(" btrim(split_part(t.ctid::varchar,',',1),'(')::int )) p")
                .append(" where p.t_ctid=t.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                .append(" ,t.* from ")
                .append(tableWithSchema).append(" t");
        MonitorController mc = MonitorController.getInstance();
        try
        {            
            List<Object[]> objList = mc.getLabeledRecordList(helperInfo, tableWithSchema, PageSize);
            SmartPagingModel spm = new SmartPagingModel(helperInfo, sql.toString(), PageSize, types, header4LabelRecord, objList);
            tblRecord.setModel(spm);
            tblRecord.getColumn(constBundle.getString("record")).setPreferredWidth(470);
            tblRecord.getColumn(constBundle.getString("sensitivity")).setMaxWidth(80);
            tblRecord.getColumn("").setMaxWidth(50);
            spm.addTableModelListener(new TableModelListener()
            {
                @Override
                public void tableChanged(TableModelEvent e)
                {
                    SmartPagingModel pagingModel = (SmartPagingModel) e.getSource();
                    logger.info("currentPage=" + (pagingModel.getPageOffset() + 1));
                }
            });
            spRecord = SmartPagingModel.createPagingScrollPaneForTable(spRecord, tblRecord);
            tblRecord.repaint();
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void btnUpdateRecordSensitivityAction(ActionEvent e)
    {
        logger.info("sensitivity=" + spinRecordSensitivity.getValue().toString());
        DefaultTableModel smodel = (DefaultTableModel) tblRecord.getModel();
        int row = smodel.getRowCount();
        logger.info("rowCount="+row);
        MonitorController mc = MonitorController.getInstance();
        try
        {
            List<Object> ctidList = new ArrayList();
            for (int i = 0; i < row; i++)
            {
                if (smodel.getValueAt(i, 2).equals(true))
                {
                    LabeledRecordInfoDTO record = (LabeledRecordInfoDTO) smodel.getValueAt(i, 0);
                    ctidList.add(record.getCtid());
                }
            }
            if (ctidList.isEmpty())
            {
                return;
            }
            LabeledRecordInfoDTO recordInfo = (LabeledRecordInfoDTO) smodel.getValueAt(0, 0);
            logger.info("acolumn=" + recordInfo.getaColumn());
            mc.batchUpdateRecordSensitivity(helperInfo, cbTables.getSelectedItem().toString(),
                    ctidList, Integer.valueOf(spinRecordSensitivity.getValue().toString()), recordInfo.getaColumn());
            
            //refresh data
            cbTableStateChanged(null);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void refreshLabelTable()
    {
//        tblRecord.setModel(new DefaultTableModel());
//        spRecord.setRowHeaderView(null);
        this.emptyTableData((DefaultTableModel) tblRecord.getModel());
        spRecord.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, null);
        spRecord.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, null);

        cbTables.removeAllItems();
        cbTables.addItem(constBundle.getString("tableNameSort"));
        cbTables.setSelectedIndex(0);
        MonitorController mc = MonitorController.getInstance();
        try
        {
            List<ObjectInfoDTO> ol = mc.getObjectInfoList(helperInfo, "table4label");
            for (ObjectInfoDTO obj : ol)
            {
                cbTables.addItem(obj);
            }
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    } 
    //for  object sensitivity label
    private void initLabelView()
    {
        cbRowLimit.setModel(new DefaultComboBoxModel(new String[]
        {
            MessageFormat.format(constBundle.getString("firstRows"), 100),
            MessageFormat.format(constBundle.getString("firstRows"), 500),
            constBundle.getString("allRows")
        }));
        cbRowLimit.setSelectedIndex(0);
        Object[] header4LabelObj = new Object[]
        {
            constBundle.getString("objectType"),
            "Oid",
            constBundle.getString("objectName"),
            constBundle.getString("sensitivity"),
            ""
        };
        obj4LabelModel = new DefaultTableModel(header4LabelObj, 0)
        {
            public Class[] types = new Class[]
            {
                Object.class, Object.class, ObjectInfoDTO.class,
                Object.class, Boolean.class
            };
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return column == 4;
            }
        }; 

//        Object[] header4LabelRecord = new Object[]
//        {
//            constBundle.getString("record"),
//            constBundle.getString("sensitivity"),
//            ""
//        };
//        recordModel = new DefaultTableModel(header4LabelRecord, 0)
//        {
//            public Class[] types = new Class[]
//            {
//                LabeledRecordInfoDTO.class,                
//                Object.class,
//                Boolean.class
//            };
//            @Override
//            public Class<?> getColumnClass(int columnIndex)
//            {
//                return types[columnIndex];
//            }
//            @Override
//            public boolean isCellEditable(int row, int column)
//            {
//                return column == 2;
//            }
//        };
        tblLabel.setModel(obj4LabelModel);
        tblLabel.getTableHeader().setReorderingAllowed(false);
        tblLabel.getColumn(constBundle.getString("objectType")).setMaxWidth(70);
        tblLabel.getColumn("Oid").setMaxWidth(70);
        tblLabel.getColumn(constBundle.getString("sensitivity")).setMaxWidth(70);
        tblLabel.getColumn("").setMaxWidth(50);
        
        String[] objectTypes = new String[]
        {
            constBundle.getString("objTypeSort"),
            "user", "tablespace", "database", "schema",
            "sequence", "view", "table",
            "index", "rule", "trigger", "function",
            "constraint"
             //, "data"
        };
        cbObjType4Label.setModel(new DefaultComboBoxModel(objectTypes));
        cbObjType4Label.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                objType4LabelChanged(e);
            }
        });
        cbTable4Label.setModel(new DefaultComboBoxModel(new String[]
        {
            constBundle.getString("tableNameSort")
        }));
        cbTable4Label.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbObj4LabelChanged(e);
            }
        });
     
        spnSensitivity.setModel(new SpinnerNumberModel(0, 0, 32767, 1)); 
        //forbid  illegally character
        JFormattedTextField tf = ((JSpinner.NumberEditor)spnSensitivity.getEditor()).getTextField();
        DefaultFormatterFactory factory = (DefaultFormatterFactory)tf.getFormatterFactory();
        NumberFormatter formatter = (NumberFormatter)factory.getDefaultFormatter();
        formatter.setAllowsInvalid(false);
        
        btnUpdate.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnUpdateSensitivityAction(e);
            }
        });
    } 
    private void objType4LabelChanged(ItemEvent e)
    {
        if (cbObjType4Label.getSelectedIndex() < 0)
        {
            return;
        }
//        if (cbObjType4Label.getSelectedIndex() == 0)
//        {
//            cbTable4Label.setEnabled(false);
//            cbTable4Label.removeAllItems();
//            cbTable4Label.addItem(constBundle.getString("tableNameSort"));
//            return;
//        }
//        MonitorController mc = MonitorController.getInstance();
        try
        {
            String objType = cbObjType4Label.getSelectedItem().toString();
            logger.info("selectedObjType=" + objType);
//            if (objType.equals("data"))
//            {
//                cbTable4Label.setEnabled(true);
//                cbTable4Label.removeAllItems();
//                cbTable4Label.addItem(constBundle.getString("tableNameSort"));
//                List<ObjectInfoDTO> ol = mc.getObjectInfoList(helperInfo, "table4label");
//                for (ObjectInfoDTO obj : ol)
//                {
//                    cbTable4Label.addItem(obj);
//                }
//                tblLabel.setModel(recordModel);
//                tblLabel.getTableHeader().setReorderingAllowed(false);
//                tblLabel.getColumn(constBundle.getString("sensitivity")).setMaxWidth(80);
//                tblLabel.getColumn("").setMaxWidth(50);
//            } else
//            {
//                cbTable4Label.setEnabled(false);
//                cbTable4Label.removeAllItems();
//                cbTable4Label.addItem(constBundle.getString("tableNameSort"));
//                
//                tblLabel.setModel(obj4LabelModel);
//                tblLabel.getColumn(constBundle.getString("objectType")).setMaxWidth(80);
//                tblLabel.getColumn("Oid").setMaxWidth(80);
//                tblLabel.getColumn(constBundle.getString("sensitivity")).setMaxWidth(80);
//                tblLabel.getColumn("").setMaxWidth(50);
//             }
            this.refreshLabelData();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void cbObj4LabelChanged(ItemEvent e)
    {
        logger.info("selectedTable="+cbTable4Label.getSelectedItem());
        refreshLabelData();
    }    
    private void btnUpdateSensitivityAction(ActionEvent e)
    {
        logger.info("sensitivity=" + spnSensitivity.getValue().toString());
        DefaultTableModel smodel = (DefaultTableModel) tblLabel.getModel();
        int row = smodel.getRowCount();
        logger.info("rowCount="+row);
        String objType = cbObjType4Label.getSelectedItem().toString();
//        boolean isData = cbObjType4Label.getSelectedItem().equals("data");
        MonitorController mc = MonitorController.getInstance();
        try
        {
//            if (isData)
//            {
//            List<Object> ctidList = new ArrayList();
//                for (int i = 0; i < row; i++)
//                {
//                    if (smodel.getValueAt(i, 2).equals(true))
//                    {
//                        LabeledRecordInfoDTO record = (LabeledRecordInfoDTO) smodel.getValueAt(i, 0);
//                        ctidList.add(record.getCtid());
//                    }
//                }
//                if (ctidList.isEmpty())
//                {
//                    return;
//                }
//                LabeledRecordInfoDTO recordInfo = (LabeledRecordInfoDTO) smodel.getValueAt(0, 0);
//                logger.info("acolumn=" + recordInfo.getaColumn());
//                mc.batchUpdateRecordSensitivity(helperInfo, cbTable4Label.getSelectedItem().toString(),
//                        ctidList, Integer.valueOf(spnSensitivity.getValue().toString()), recordInfo.getaColumn());
//            } else 
            if (objType.equals("user"))
            {
                List<String> userList = new ArrayList();
                for (int i = 0; i < row; i++)
                {
                    if (smodel.getValueAt(i, 4).equals(true))
                    {
                        userList.add(smodel.getValueAt(i, 2).toString());
                    }
                }
                if (userList.isEmpty())
                {
                    return;
                }
                mc.batchUpdateUserSensitivity(helperInfo, objType, userList, Integer.valueOf(spnSensitivity.getValue().toString()));
            } else
            {
                List<Long> oidList = new ArrayList();
                for (int i = 0; i < row; i++)
                {
                    if (smodel.getValueAt(i, 4).equals(true))
                    {
                        oidList.add(Long.valueOf(smodel.getValueAt(i, 1).toString()));
                    }
                }
                if (oidList.isEmpty())
                {
                    return;
                }
                mc.batchUpdateObjSensitivity(helperInfo, objType, oidList, Integer.valueOf(spnSensitivity.getValue().toString()));
            }
            this.refreshLabelData();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        } 
    }
    private void refreshLabelData()
    {
        MonitorController mc = MonitorController.getInstance();
        DefaultTableModel labelModel = (DefaultTableModel) tblLabel.getModel();
        this.emptyTableData(labelModel);
        if (cbObjType4Label.getSelectedIndex() <= 0)
        {
            return;
        }
        logger.info("objType = " + cbObjType4Label.getSelectedItem().toString());
        String objType = cbObjType4Label.getSelectedItem().toString();
        logger.info("RowLimit: " + cbRowLimit.getSelectedItem());
        int rowLimit = -1;//all rows
        int index = cbRowLimit.getSelectedIndex();
        if (index == 0)
        {
            rowLimit = 100;
        } else if (index == 1)
        {
            rowLimit = 500;
        }
        try
        {
//            if (objType.equals("data"))
//            {
//                if(cbTable4Label.getSelectedIndex()<=0)
//                {
//                    return;
//                }
//                String tableWithSchema= cbTable4Label.getSelectedItem().toString();
//                mc.getLabeledRecordList(helperInfo, tableWithSchema,rowLimit,labelModel);
//            } else
//            {
                mc.getLabeledObjectList(helperInfo, objType,rowLimit,labelModel);
//                List<LabeledObjInfoDTO> objList = mc.getLabeledObjectList(helperInfo, objType,rowLimit);
//                Object[] rowData;
//                for (LabeledObjInfoDTO objInfo : objList)
//                {
//                    rowData = new Object[5];
//                    rowData[0] = objInfo.getType();
//                    rowData[1] = objInfo.getOid();
//                    rowData[2] = objInfo;
//                    rowData[3] = objInfo.getSentivity();
//                    rowData[4] = false;
//                    labelModel.addRow(rowData);
//                }
//            }
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    //for illegal event 
    private void initIllegalEventView()
    {
        tabPane.setSelectedIndex(3);
        tabPane.setEnabledAt(0, true);//changed
        tabPane.setEnabledAt(1, true);//changed
        tabPane.setEnabledAt(2, false);
        tabPane.setEnabledAt(3, true);
        tabPane.setEnabledAt(4, true);
        tabPane.setEnabledAt(5, true);
        menuAudit.setEnabled(false);
        menuSecurity.setEnabled(true);
        btnRefresh.setEnabled(true);
        String[] events = new String[]
        {
            constBundle.getString("illegalEventSort"),
            constBundle.getString("illegalDataAccess"), 
            constBundle.getString("illegalDataModify"),
            constBundle.getString("illegalDefinationModify"),
            constBundle.getString("dataDelete"), 
            constBundle.getString("dataModify"),
            constBundle.getString("objectDrop"),
            constBundle.getString("objectNotExist"),
            constBundle.getString("wrongPwdLoginFailure")
        };
        cbEventSort.setModel(new DefaultComboBoxModel(events));
        String[] objectTypes = new String[]
        {
            constBundle.getString("objTypeSort"),
            "user", "tablespace", "database", "schema",
            "sequence", "view", "table",
            "index", "rule", "trigger", "function",
            "constraint"
        };
        cbObjTypeForEventSort.setModel(new DefaultComboBoxModel(objectTypes));
        String[] actions = new String[]
        {
            constBundle.getString("actionSort"),
            constBundle.getString("log"), constBundle.getString("warn"),
            constBundle.getString("disconnect"), constBundle.getString("lock")
        };
        cbActionSort.setModel(new DefaultComboBoxModel(actions));
        String[] headerArray = new String[]
        {
            "Oid",
            constBundle.getString("illegalEvent"),
            constBundle.getString("objectType"),
            constBundle.getString("objectOid"),
            constBundle.getString("objectName"),
            constBundle.getString("operation"),
            constBundle.getString("failedLoginCount"),
            constBundle.getString("action"),
            constBundle.getString("description"),
            ""
        };
        tblEvent.setModel(new DefaultTableModel(headerArray, 0)
        {
            public Class[] types = new Class[]
            {
                IllegalEventInfoDTO.class, Object.class, Object.class, Object.class,
                Object.class, Object.class, Object.class, Object.class, Object.class,
                Boolean.class
            };
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column)
            {
                return column==9 || column==7;
            }            
        });
        tblEvent.getColumn("Oid").setMaxWidth(50);
        tblEvent.getColumn(constBundle.getString("illegalEvent")).setMaxWidth(120);
        tblEvent.getColumn(constBundle.getString("objectType")).setMaxWidth(60);
        tblEvent.getColumn(constBundle.getString("objectOid")).setMaxWidth(55);
        tblEvent.getColumn(constBundle.getString("operation")).setMaxWidth(70);
        tblEvent.getColumn(constBundle.getString("failedLoginCount")).setMaxWidth(100);
        tblEvent.getColumn(constBundle.getString("action")).setMaxWidth(70);
        tblEvent.getColumn("").setMaxWidth(30);
        
        tblEvent.getTableHeader().setReorderingAllowed(false);
        tblEvent.setRowHeight(20);        
        final JComboBox cbActionSelect = new JComboBox();
        tblEvent.getColumnModel().getColumn(7).setCellEditor(new DefaultCellEditor(cbActionSelect));
        tblEvent.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent evt)
            {
                JTable t = (JTable) evt.getSource();
                int c = t.getSelectedColumn();
                logger.info("column=" + c);
                if (c == 7)
                {
                    IllegalEventInfoDTO event = (IllegalEventInfoDTO) t.getValueAt(t.getSelectedRow(), 0);
                    logger.info("eventId=" + event.getEventId());
                    switch (event.getEventId())
                    {
                        case 1:
                        case 2:
                        case 3:
                            String[] action1 = new String[]
                            {constBundle.getString("log")};
                            cbActionSelect.setModel(new DefaultComboBoxModel(action1));
                            break;
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            String[] action2 = new String[]
                            {constBundle.getString("log"), constBundle.getString("warn"), constBundle.getString("disconnect")};
                            cbActionSelect.setModel(new DefaultComboBoxModel(action2));
                            break;
                        case 8:
                            String[] action3 = new String[]
                            {constBundle.getString("log"), constBundle.getString("warn"), constBundle.getString("lock")};
                            cbActionSelect.setModel( new DefaultComboBoxModel(action3));
                            break;
                        default:
                            logger.error(event.getActionId() +" is an exception action id.");
                            break;
                    }
                }                
            }
        });
        
        eventModel = (DefaultTableModel) tblEvent.getModel();
        eventModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                int column = e.getColumn();
                int row = e.getLastRow();
                if (column == 7)
                {
                    System.out.println("column=" + column + ",row=" + row);
                    eventModel.setValueAt(true, row, 9);
                }
            }            
        });
        
        //for custom illegal event
        cbEvent.setModel(new DefaultComboBoxModel(events));
        cbObjTypeForEvent.addItem(constBundle.getString("objTypeSort"));
        cbObjNameForEvent.addItem(constBundle.getString("objNameSort"));
        cbOperationForEvent.addItem(constBundle.getString("operationSort"));
        cbAction.addItem(constBundle.getString("actionSort"));
        cbEvent.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                cbEventItemStateChanged(evt);
                enableBtnAddEvent();
            }
        });
        cbObjTypeForEvent.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                cbObjTypeForEventItemStateChanged(evt);
                enableBtnAddEvent();
            }
        });
        cbObjNameForEvent.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                enableBtnAddEvent();
            }
        });
        cbOperationForEvent.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                enableBtnAddEvent();
            }
        });
        txfdFailedTime.addKeyListener(new KeyAdapter()
        {
             @Override
            public void keyTyped(KeyEvent e)
            {
                txfdFailedTimeValueKeyTyped(e);
            }
            @Override
            public void keyReleased(KeyEvent evt)
            {
                enableBtnAddEvent();
            }
        });
        cbAction.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                enableBtnAddEvent();
            }
        });
        btnAddEvent.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
               btnAddEventAction(e);
            }            
        });
                      
        
        //filter
        ItemListener filterEventItemListener = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                filterIllegalEventActionPerformed(evt);
            }
        };
        cbEventSort.addItemListener(filterEventItemListener);
        cbActionSort.addItemListener(filterEventItemListener);
        cbObjTypeForEventSort.addItemListener(filterEventItemListener);
    
        
        //edit and detail
        tblEvent.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tblEventMouseClicked(evt);
            }
        });
        ActionListener editAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                editIllegalEventActionPerformed();
            }
        };
        itemEditIllegalEvent.addActionListener(editAction);
        btnEdit.addActionListener(editAction);

        //delete
        ActionListener batchDeleteAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                deleteIllegalEventAction(evt);
            }
        };
        itemDeleteIllegalEvent.addActionListener(batchDeleteAction);
        btnDelete.addActionListener(batchDeleteAction);

        //batch insert and update
        btnBatchCommitForEvent.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnBatchCommitForEventAction(e);
                filterIllegalEventActionPerformed(null);
            }
        });

    }   
    //custom event
    private void txfdFailedTimeValueKeyTyped(KeyEvent evt)
    {
        char keyCh = evt.getKeyChar();
        logger.info("keych:" + keyCh + "," + Character.isDigit(keyCh));
        //<=5
        if (Character.isDigit(keyCh) && (keyCh > '0') && (keyCh < '6'))
        {
            txfdFailedTime.setText("");
        } else
        {
            evt.setKeyChar('\0');
        }
        //just digital
//        if ((keyCh < '0') || (keyCh > '9'))
//        {
//            if (keyCh != '') //回车字符
//            {
//                evt.setKeyChar('\0');
//            }
//        }
    }    
    private void cbEventItemStateChanged(ItemEvent evt)
    {
        int selectedEvent = cbEvent.getSelectedIndex();
        logger.info("selectedEvent = " + selectedEvent);
        if (selectedEvent < 0)
        {
            return;
        } else if (selectedEvent == 0)
        {
            cbObjTypeForEvent.removeAllItems();
            cbObjTypeForEvent.addItem(constBundle.getString("objTypeSort"));
            cbObjNameForEvent.removeAllItems();
            cbObjNameForEvent.addItem(constBundle.getString("objNameSort"));
            cbOperationForEvent.removeAllItems();
            cbOperationForEvent.addItem(constBundle.getString("operationSort"));
            cbAction.removeAllItems();
            cbAction.addItem(constBundle.getString("actionSort"));
            return;
        }
        MonitorController mc = MonitorController.getInstance();
        cbObjTypeForEvent.removeAllItems();
        cbObjTypeForEvent.addItem(constBundle.getString("objTypeSort"));
        List<ObjectDTO> objTypes = mc.getObjTypeArrayByEventId(selectedEvent);
        for (ObjectDTO type : objTypes)
        {
            cbObjTypeForEvent.addItem(type);
        }
        cbObjNameForEvent.removeAllItems();
        cbObjNameForEvent.addItem(constBundle.getString("objNameSort"));
        cbOperationForEvent.removeAllItems();
        cbOperationForEvent.addItem(constBundle.getString("operationSort"));
        List<ObjectDTO> operations = mc.getOperationArrayByEventId(selectedEvent);
        for (ObjectDTO op : operations)
        {
            cbOperationForEvent.addItem(op);
        }       
        cbAction.removeAllItems();
        cbAction.addItem(constBundle.getString("actionSort"));
        String[] actions = mc.getActionByEventId(selectedEvent);
        for(String action : actions)
        {
            cbAction.addItem(action);
        }
        cbAction.setSelectedIndex(0);
        //for enable
        if (selectedEvent == 8)
        {
            txfdFailedTime.setEnabled(true);
            cbOperationForEvent.setEnabled(false);
            cbObjTypeForEvent.setEnabled(true);
            cbObjNameForEvent.setEnabled(true);
            txfdFailedTime.setEnabled(true);
            lblEventCount.setEnabled(true);
        } else if (selectedEvent == 7)
        {
            txfdFailedTime.setEnabled(false);
            cbOperationForEvent.setEnabled(true);
            cbObjTypeForEvent.setEnabled(false);
            cbObjNameForEvent.setEnabled(false);
            txfdFailedTime.setEnabled(false);
            lblEventCount.setEnabled(false);
        } else
        {
            txfdFailedTime.setEnabled(false);
            cbOperationForEvent.setEnabled(true);
            cbObjTypeForEvent.setEnabled(true);
            cbObjNameForEvent.setEnabled(true);
            txfdFailedTime.setEnabled(false);
            lblEventCount.setEnabled(false);
        }
    }
    private void cbObjTypeForEventItemStateChanged(ItemEvent evt)
    {
        if (cbObjTypeForEvent.getSelectedIndex() <= 0)
        {
            return;
        }
        logger.info("objType=" + cbObjTypeForEvent.getSelectedItem().toString());
        MonitorController mc = MonitorController.getInstance();
        try
        {
            cbObjNameForEvent.removeAllItems();
            cbObjNameForEvent.addItem(constBundle.getString("objNameSort"));
            String objType = cbObjTypeForEvent.getSelectedItem().toString();
            List<ObjectInfoDTO> objList = mc.getObjectInfoList(helperInfo, objType);
            for (ObjectInfoDTO objInfo : objList)
            {
                if (cbEvent.getSelectedIndex() == 8 && objInfo.getName().equals("syssso"))
                {
                    //do nothing
                } else
                {
                    cbObjNameForEvent.addItem(objInfo);
                }
            }
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void enableBtnAddEvent()
    {
        int event = cbEvent.getSelectedIndex();
        logger.info("event=" + event + ",objType=" + cbObjTypeForEvent.getSelectedIndex()
                + ",objName=" + cbObjNameForEvent.getSelectedIndex() + ",operation=" + cbOperationForEvent.getSelectedIndex()
                + ",eventCount=" + txfdFailedTime.getText() + ",action=" + cbAction.getSelectedIndex());
        if (event <= 0 || cbAction.getSelectedIndex() <= 0)
        {
            btnAddEvent.setEnabled(false);
        } else
        {
            switch (event)
            {
                case 8://"wrong_pwd_login_failure"
                    if (cbObjTypeForEvent.getSelectedIndex() > 0
                            && cbObjNameForEvent.getSelectedIndex() > 0
                            && txfdFailedTime.getText() != null
                            && !txfdFailedTime.getText().isEmpty())
                    {
                        btnAddEvent.setEnabled(true);
                    } else
                    {
                        btnAddEvent.setEnabled(false);
                    }
                    break;
                case 7://"object_not_exist"
                    if (cbOperationForEvent.getSelectedIndex() > 0)
                    {
                        btnAddEvent.setEnabled(true);
                    } else
                    {
                        btnAddEvent.setEnabled(false);
                    }
                    break;
                default:
                    if (cbObjTypeForEvent.getSelectedIndex() > 0
                            && cbObjNameForEvent.getSelectedIndex() > 0
                            && cbOperationForEvent.getSelectedIndex() > 0)
                    {
                        btnAddEvent.setEnabled(true);
                    } else
                    {
                        btnAddEvent.setEnabled(false);
                    }
            }
        }
    }
    private void addRowForEvent(IllegalEventInfoDTO eventInfo)
    {
        Object[] rowData = new Object[10];
        rowData[0] = eventInfo;
        rowData[1] = eventInfo.getEventName();
        rowData[2] = eventInfo.getObjectType();
        rowData[3] = eventInfo.getObjectOid();
        rowData[4] = eventInfo.getObjectName();
        rowData[5] = eventInfo.getOperation();
        rowData[6] = eventInfo.getCount();
        rowData[7] = eventInfo.getAction();
        rowData[8] = eventInfo.getDescription();
        rowData[9] = eventInfo.getOid() == null;
        eventModel.addRow(rowData);
    }
    //filter event and refresh
    private void filterIllegalEventActionPerformed(ItemEvent evt)
    {
        int eventIndex = cbEventSort.getSelectedIndex();
        int actionIndex = cbActionSort.getSelectedIndex();
        int objTypeIndex = cbObjTypeForEventSort.getSelectedIndex();
        logger.info("index;event=" + eventIndex + ",action=" + actionIndex + ",objType=" + objTypeIndex);
        if (eventIndex >0 || actionIndex >= 0 || objTypeIndex >= 0)
        {
            this.emptyTableData(eventModel);
            MonitorController mc = MonitorController.getInstance();
            try
            {
                mc.queryIllegalEvents(helperInfo, eventIndex, actionIndex, objTypeIndex,eventModel);
//                List<IllegalEventInfoDTO> eventList = mc.queryIllegalEvents(helperInfo, eventIndex, actionIndex, objTypeIndex);
//                for (IllegalEventInfoDTO eventInfo : eventList)
//                {
//                    Object[] rowData = new Object[10];
//                    rowData[0] = eventInfo;
//                    rowData[1] = eventInfo.getEventName();
//                    rowData[2] = eventInfo.getObjectType();
//                    rowData[3] = eventInfo.getObjectOid();
//                    rowData[4] = eventInfo.getObjectName();
//                    rowData[5] = eventInfo.getOperation();
//                    rowData[6] = eventInfo.getCount();
//                    rowData[7] = eventInfo.getAction();
//                    rowData[8] = eventInfo.getDescription();
//                    rowData[9] = false;
//                    eventModel.addRow(rowData);
//                }
                tblEvent.repaint();
            } catch (Exception ex)
            {
                logger.error("Error " + ex.getMessage());
                ex.printStackTrace(System.out);
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    //edit event
    private void tblEventMouseClicked(MouseEvent evt)
    {
        int selectedRow = tblEvent.getSelectedRow();
        logger.info("event selected row =" + selectedRow);
        if (selectedRow >= 0)
        {
            btnEdit.setEnabled(true);
            itemEditIllegalEvent.setEnabled(true);
        } else
        {
            btnEdit.setEnabled(false);
            itemEditIllegalEvent.setEnabled(false);
        }
        int count = evt.getClickCount();
        logger.info("mouseClick=" + count);
        if (count == 2)
        {
            editIllegalEventActionPerformed();
        }
    }
    private void editIllegalEventActionPerformed()
    {
        int currentRow = tblEvent.getSelectedRow();
        logger.info("currentRow=" + currentRow);
        if (currentRow < 0)
        {
            return;
        }
        IllegalEventInfoDTO eventInfo = (IllegalEventInfoDTO) eventModel.getValueAt(currentRow, 0);
        IllegalEventDialog eventEditDialog = new IllegalEventDialog(this, true, helperInfo, eventInfo);
        eventEditDialog.setLocationRelativeTo(this);
        eventEditDialog.setVisible(true);
        if (eventEditDialog.isSuccess())
        {
            IllegalEventInfoDTO newEventInfo = eventEditDialog.getIllegalEventInfo();
            eventModel.setValueAt(newEventInfo.getAction(), currentRow, 7);
            eventModel.setValueAt(newEventInfo.getDescription(), currentRow, 8);
            eventModel.setValueAt(true, currentRow, 9);
        }
    }
    //enable all event
    private void btnBatchCommitForEventAction(ActionEvent evt)
    {
        int row = eventModel.getRowCount();
        if (row < 0)
        {
            return;
        }
        List<IllegalEventInfoDTO> updateEventList = new ArrayList();
        List<IllegalEventInfoDTO> insertEventList = new ArrayList();
        MonitorController mc = MonitorController.getInstance();
        try
        {
            for (int i = 0; i < row; i++)
            {
                logger.info("isSelected=" + eventModel.getValueAt(i, 9) + ",oid = " + eventModel.getValueAt(i, 0).toString());
                if ((Boolean) eventModel.getValueAt(i, 9))
                {
                    IllegalEventInfoDTO event = (IllegalEventInfoDTO) eventModel.getValueAt(i, 0);
                    if (event.getOid() != null)
                    {
                        String newAction = eventModel.getValueAt(i, 7).toString();
                        String newDescription = null;
                        if( eventModel.getValueAt(i, 8) !=null)
                        {
                            newDescription = eventModel.getValueAt(i, 8).toString();
                        }
                        logger.info("newDescription=" + newDescription + ",isNull=" + (newDescription == null));
                        logger.info("description=" + event.getDescription() + ",isNull=" + (event.getDescription() == null));
                        if ((!event.getAction().equals(newAction))
                                || (event.getDescription() != null && !event.getDescription().equals(newDescription))
                                || (newDescription != null && !newDescription.equals(event.getDescription())))
                        {
                            if (!event.getAction().equals(newAction))
                            {
                                event.setAction(newAction);
                                event.setActionId(mc.getIdByAction(newAction));
                            }
                            if ((event.getDescription() != null && !event.getDescription().equals(newDescription))
                                || (newDescription != null && !newDescription.equals(event.getDescription())))
                            {
                                event.setDescription(newDescription);
                            }
                            updateEventList.add(event);
                        }
                    } else
                    {
                        if (event.getObjectOid() != null && event.getObjectType().equals("user")
                                && !mc.isUserExistNow(helperInfo, event.getObjectOid()))
                        {
                            logger.warn("user " + event.getObjectName() + "(oid=" + event.getObjectOid() 
                                    + ") not exist,the audit rule cannot be enabled.");
                            JOptionPane.showMessageDialog(this, "Warning: user '" + event.getObjectName()
                                    + "' just been dropd, the audit rule cannot be enable.",
                                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                        } else
                        {
                            String newAction = eventModel.getValueAt(i, 7).toString();
                            String newDescription = null;
                            if (eventModel.getValueAt(i, 8) != null)
                            {
                                newDescription = eventModel.getValueAt(i, 8).toString();
                            }
                            if (!event.getAction().equals(newAction))
                            {
                                event.setAction(newAction);
                            }
                            if ((event.getDescription() != null && !event.getDescription().equals(newDescription))
                                    || (newDescription != null && !newDescription.equals(event.getDescription())))
                            {
                                event.setDescription(newDescription.toString());
                            }
                            insertEventList.add(event);
                        }
                    }
                }
            }
            if (insertEventList.size() > 0)
            {
                mc.batchInsertIllegalEvent(helperInfo, insertEventList);
            }
            if (updateEventList.size() > 0)
            {
                mc.bacthUpdateIllegalEvent(helperInfo, updateEventList);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    //add event
    private void btnAddEventAction(ActionEvent evt)
    {
        IllegalEventInfoDTO newEvent = new IllegalEventInfoDTO();
        MonitorController mc = MonitorController.getInstance();
        int event = cbEvent.getSelectedIndex();
        newEvent.setEventId(event);
        newEvent.setEventName(cbEvent.getSelectedItem().toString());
        newEvent.setActionId(mc.getIdByAction(cbAction.getSelectedItem().toString()));
        newEvent.setAction(cbAction.getSelectedItem().toString());
        newEvent.setDescription(null);
        switch (event)
        {
            case 8://"wrong_pwd_login_failure"
                ObjectDTO type = (ObjectDTO) cbObjTypeForEvent.getSelectedItem();
                newEvent.setObjectTypeId(type.getId());
                newEvent.setObjectType(type.getValue());
                ObjectInfoDTO obj = (ObjectInfoDTO) cbObjNameForEvent.getSelectedItem();
                newEvent.setObjectOid(obj.getOid());
                newEvent.setObjectName(obj.getName());
                newEvent.setCount(txfdFailedTime.getText());
                break;
            case 7://"object_not_exist"
                ObjectDTO operation = (ObjectDTO) cbOperationForEvent.getSelectedItem();
                newEvent.setOperationId(operation.getId());
                newEvent.setOperation(operation.getValue());
                break;
            default:
                ObjectDTO type2 = (ObjectDTO) cbObjTypeForEvent.getSelectedItem();
                newEvent.setObjectTypeId(type2.getId());
                newEvent.setObjectType(type2.getValue());
                ObjectInfoDTO obj2 = (ObjectInfoDTO) cbObjNameForEvent.getSelectedItem();
                newEvent.setObjectOid(obj2.getOid());
                newEvent.setObjectName(obj2.getName());
                ObjectDTO operation2 = (ObjectDTO) cbOperationForEvent.getSelectedItem();
                newEvent.setOperationId(operation2.getId());
                newEvent.setOperation(operation2.getValue());
                break;
        }
        this.addRowForEvent(newEvent);
    }
    //delete event
    private void deleteIllegalEventAction(ActionEvent evt)
    {
        int row = eventModel.getRowCount();
        if (row < 0)
        {
            return;
        }
        MonitorController mc = MonitorController.getInstance();
        try
        {
            List<Long> oidList = new ArrayList();
            for (int i = row - 1; i >= 0; i--)
            {
                logger.info("isSelected=" + eventModel.getValueAt(i, 9) + ",oid = " + eventModel.getValueAt(i, 0));
                if ((Boolean) eventModel.getValueAt(i, 9))
                {
                    if (eventModel.getValueAt(i, 0).toString() != null)
                    {
                        oidList.add(Long.valueOf(eventModel.getValueAt(i, 0).toString()));
                    }
                    eventModel.removeRow(i);
                }
            }
            if (oidList.size() > 0)
            {
                mc.batchDeleteIllegalEvent(helperInfo, oidList);
            }
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    
    //for sao 
    private void tbPanel4saoStateChanged(ChangeEvent evt)
    {
        int index = tabPane.getSelectedIndex();
        if (index < 0)
        {
            return;
        }
        logger.info("SelectedTab:" + tabPane.getTitleAt(index));
        if (index == 0 || index == 1)
        {
            menuAudit.setEnabled(false);
            btnDelete.setEnabled(false);
            btnEdit.setEnabled(false);
            btnRefresh.setEnabled(false);
        } else if (index == 2)
        {
            btnRefresh.setEnabled(true);
            btnDelete.setEnabled(true);
            menuAudit.setEnabled(true);
            itemDeleteAuditRule.setEnabled(false);
            if (tblRule.getSelectedRow() >= 0)
            {
                btnEdit.setEnabled(true);               
                itemEditAuditRule.setEnabled(true);
            } else
            {
                btnEdit.setEnabled(false);
                itemEditAuditRule.setEnabled(false);                
            }
        }
    }
    //for audit rule
    private void initAuditRuleView()
    {
        tabPane.setSelectedIndex(0);
        tabPane.setEnabledAt(0, true);
        tabPane.setEnabledAt(1, true);
        tabPane.setEnabledAt(2, true);
        tabPane.setEnabledAt(3, false);
        tabPane.setEnabledAt(4, false);
        tabPane.setEnabledAt(5, false);
        menuAudit.setEnabled(false);
        menuSecurity.setEnabled(false);
        btnEdit.setEnabled(false);
        btnDelete.setEnabled(false);    
        btnRefresh.setEnabled(false);
        tabPane.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                tbPanel4saoStateChanged(e);
            }           
        });
        Object[] ruleHeaderArray = new Object[]
        {
            "Oid",
            constBundle.getString("objectType"),
            constBundle.getString("objectName"),
            constBundle.getString("user"),
            constBundle.getString("operation"),
            constBundle.getString("result"),
            constBundle.getString("enableTime"),
            ""
        };
        tblRule.getTableHeader().setReorderingAllowed(false);
        tblRule.setModel(new DefaultTableModel(ruleHeaderArray, 0)
        {
            public Class[] types = new Class[]
            {
                AuditRuleInfoDTO.class, Object.class, Object.class, 
                Object.class, Object.class, Object.class, Object.class, Boolean.class                    
            };
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return  column == 7 || column==5;
            }
        });
        
        String[] result = new String[]
        {
            "success", "failed", "all"
        };
        JComboBox cbResult = new JComboBox(result);
        
        tblRule.getColumn("Oid").setMaxWidth(50);
        tblRule.getColumn(constBundle.getString("objectType")).setMaxWidth(60);
        tblRule.getColumn(constBundle.getString("operation")).setMaxWidth(110);
        tblRule.getColumn(constBundle.getString("operation")).setMinWidth(110);
        tblRule.getColumn(constBundle.getString("result")).setMaxWidth(70);
        tblRule.getColumn(constBundle.getString("enableTime")).setMaxWidth(180);
        tblRule.getColumn(constBundle.getString("enableTime")).setMinWidth(180);
        tblRule.getColumn("").setMaxWidth(30);
        
        tblRule.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(cbResult));       
        ruleModel = (DefaultTableModel) tblRule.getModel();
        ruleModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                int column = e.getColumn();
                int row = e.getLastRow();
                if (column == 5)
                {
                    logger.info("column=" + column + ",row=" + row);
                    ruleModel.setValueAt(true, row, 7);
                }
            }
        });

        String[] objectTypes = new String[]
        {
            constBundle.getString("objTypeSort"),
            "none", "user", "tablespace", "database", "schema",
            "sequence", "view", "table",
            "index", "rule", "trigger", "function", "constraint"
        };
        cbObjTypeSort.setModel(new DefaultComboBoxModel(objectTypes));
        String[] operations = new String[]
        {
            constBundle.getString("operationSort"),
            "alter", "drop", "revoke", "grant",
            "select", "insert", "update", "delete", "truncate",
            "lock", "cluster", "copy", "reindex",
            "create role", "create database", "create tablespace", "create schema",
            "create sequence", "create view", "create table", "create index",
            "create rule", "create trigger", "create function",
            "set constraints"
        };
        cbOperationSort.setModel(new DefaultComboBoxModel(operations));
        String[] results = new String[]
        {
            constBundle.getString("resultSort"),
             "failed","success", "all"
        };
        cbResultSort.setModel(new DefaultComboBoxModel(results));
        MonitorController mc = MonitorController.getInstance();
        try
        {
            cbUserSort.removeAllItems();
            cbUserSort.addItem(constBundle.getString("userSort"));
            List<ObjectInfoDTO> objList = mc.getObjectInfoList(helperInfo, "user");           
            for (ObjectInfoDTO obj : objList)
            {
                cbUserSort.addItem(obj);
            }
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
        
        //rule filter
        ItemListener filterRuleItemListener = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent evt)
            {
                filterAuditRuleActionPerformed(evt);
            }
        };
        cbObjTypeSort.addItemListener(filterRuleItemListener);
        cbUserSort.addItemListener(filterRuleItemListener);
        cbOperationSort.addItemListener(filterRuleItemListener);
        cbResultSort.addItemListener(filterRuleItemListener);
        btnRefresh.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                filterAuditRuleActionPerformed(null);
            }
        });

        //rule detail
        tblRule.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tblRuleMouseClicked(evt);
            }
        });
        ActionListener editAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                editAuditRuleAction(evt);
            }
        };
        itemEditAuditRule.addActionListener(editAction);
        btnEdit.addActionListener(editAction);

        //rule delete
        ActionListener deleteAction = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                deleteAuditRuleAction(evt);
            }
        };
        itemDeleteAuditRule.addActionListener(deleteAction);
        btnDelete.addActionListener(deleteAction);

        //rule batch add
        cbObjTypeAdd.setModel(new DefaultComboBoxModel(objectTypes));
        cbObjNameAdd.addItem(constBundle.getString("objNameSort"));
        cbUserAdd.addItem(constBundle.getString("userSort"));
        cbOperationAdd.addItem(constBundle.getString("operationSort"));
        cbResultAdd.setModel(new DefaultComboBoxModel(results));     
        cbObjTypeAdd.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                enableBtnAddRule();
                cbObjTypeAddStateChanged(e);
            }
        });
        ItemListener enableBtnAddRuleListener = new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                enableBtnAddRule();
            }
        };
        cbObjNameAdd.addItemListener(enableBtnAddRuleListener);
        cbUserAdd.addItemListener(enableBtnAddRuleListener);
        cbOperationAdd.addItemListener(enableBtnAddRuleListener);
        cbResultAdd.addItemListener(enableBtnAddRuleListener);
        btnAddRule.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnAddRuleAction(e);
            }
        });
        btnBatchCommit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnBatchCommitAction(e);
                filterAuditRuleActionPerformed(null);//refresh
            }
        });     
    }
    private void addRowForRule(AuditRuleInfoDTO rule)
    {
        Object[] rowData = new Object[8];
        rowData[0] = rule;
        rowData[1] = rule.getObjectType();
        rowData[2] = rule.getObjectName();
        rowData[3] = rule.getUser();
        rowData[4] = rule.getOperation();
        rowData[5] = rule.getResult();
        rowData[6] = rule.getEnableTime();
        rowData[7] = rule.getOid()==null;        
        ruleModel.addRow(rowData);
    } 
    //filter
    private void filterAuditRuleActionPerformed(ItemEvent evt)
    {
        int typeIndex = cbObjTypeSort.getSelectedIndex();
        int userIndex  = cbUserSort.getSelectedIndex();
        int operationIndex = cbOperationSort.getSelectedIndex();
        int resultIndex = cbResultSort.getSelectedIndex();
        logger.info("index;type=" + typeIndex + ",user=" + userIndex + ",operation=" + operationIndex +",result="+resultIndex);
        if (typeIndex>=0 && userIndex>=0 && operationIndex>=0 && resultIndex>=0)
        {
            logger.info("name:type=" + cbObjTypeSort.getSelectedItem().toString()
                    + ", user=" + cbUserSort.getSelectedItem().toString()
                    + ", operation=" + cbOperationSort.getSelectedItem().toString()
                    + ", result=" + cbResultSort.getSelectedItem().toString());
            this.emptyTableData(ruleModel);
            MonitorController mc = MonitorController.getInstance();
            try
            {
                String userId = "";
                if (userIndex > 0)
                {
                    ObjectInfoDTO userInfo = (ObjectInfoDTO) cbUserSort.getSelectedItem();
                    userId = userInfo.getOid();
                }
                mc.queryAuditRules(helperInfo,typeIndex-1, userId, operationIndex, resultIndex-1, ruleModel);
//                List<AuditRuleInfoDTO> ruleList = mc.queryAuditRules(helperInfo,typeIndex-1, userId, operationIndex, resultIndex-1);
//                for (AuditRuleInfoDTO ruleInfo : ruleList)
//                {
//                    this.addRowForRule(ruleInfo);
//                }
                tblRule.repaint();
            } catch (Exception ex)
            {
                logger.error("Error " + ex.getMessage());
                ex.printStackTrace(System.out);
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    //rule detail and modify result
    private void tblRuleMouseClicked(MouseEvent evt)
    {
        int selectedRow = tblRule.getSelectedRow();
        logger.info(" rule selected row=" + selectedRow);
        if (selectedRow >= 0 && tabPane.getSelectedIndex() == 2)
        {
            btnEdit.setEnabled(true);
            itemEditAuditRule.setEnabled(true);
            btnDelete.setEnabled(true);
            itemDeleteAuditRule.setEnabled(true);
        } else
        {
            btnEdit.setEnabled(false);
            itemEditAuditRule.setEnabled(false);
            btnDelete.setEnabled(false);
            itemDeleteAuditRule.setEnabled(false);
        }
        int count = evt.getClickCount();
        logger.info("mouseClick=" + count);
        if (count == 2)
        {
            editAuditRuleAction(null);            
        }
    }    
    private void editAuditRuleAction(ActionEvent evt)
    {
        int selectedRow = tblRule.getSelectedRow();
        AuditRuleInfoDTO ruleInfo = (AuditRuleInfoDTO) ruleModel.getValueAt(selectedRow, 0);
        AuditRuleDetailDialog auditRuleDialog = new AuditRuleDetailDialog(this, true, helperInfo, ruleInfo);
        auditRuleDialog.setLocationRelativeTo(this);
        auditRuleDialog.setVisible(true);
        if (auditRuleDialog.isSuccess())
        {
            ruleModel.setValueAt(auditRuleDialog.getNewResult(), selectedRow, 5);
            ruleModel.setValueAt(true, selectedRow, 7);
        }
    }
    //add rule to table
    private void enableBtnAddRule()
    {
        if (cbObjTypeAdd.getSelectedIndex() < 1)
        {
            btnAddRule.setEnabled(false);
            return;
        }
        String type = cbObjTypeAdd.getSelectedItem().toString();
        int objName = cbObjNameAdd.getSelectedIndex();
        int owner = cbUserAdd.getSelectedIndex();
        int operation = cbOperationAdd.getSelectedIndex();
        int result = cbResultAdd.getSelectedIndex();
        if (type.equals("none") && owner >= 1 && operation >= 1 && result >= 1)
        {
            btnAddRule.setEnabled(true);
        } else if (objName >= 1 && owner >= 1 && operation >= 1 && result >= 1)
        {
            btnAddRule.setEnabled(true);
        } else
        {
            btnAddRule.setEnabled(false);
        }
    }
    private void cbObjTypeAddStateChanged(ItemEvent evt)
    {
        if (cbObjTypeAdd.getSelectedIndex() == 0)
        {
            cbResultAdd.setSelectedIndex(0);
            cbObjNameAdd.removeAllItems();
            cbObjNameAdd.addItem(constBundle.getString("objNameSort"));
            cbUserAdd.removeAllItems();
            cbUserAdd.addItem(constBundle.getString("userSort"));
            cbOperationAdd.removeAllItems();
            cbOperationAdd.addItem(constBundle.getString("operationSort"));
        } else if (cbObjTypeAdd.getSelectedIndex() >= 1)
        {
            cbResultAdd.setSelectedIndex(0);
            String type = cbObjTypeAdd.getSelectedItem().toString();
            MonitorController mc = MonitorController.getInstance();
            cbOperationAdd.removeAllItems();
            cbOperationAdd.addItem(constBundle.getString("operationSort"));
            List<ObjectDTO> operations =  mc.getOperationArrayByObjectType(type);
            for (ObjectDTO op : operations)
            {
                cbOperationAdd.addItem(op);
            }
            try
            {                
                cbObjNameAdd.removeAllItems();
                cbObjNameAdd.addItem(constBundle.getString("objNameSort"));
                if (cbObjTypeAdd.getSelectedIndex() == 1)
                {
                    cbObjNameAdd.setEnabled(false);
                } else
                {
                    cbObjNameAdd.setEnabled(true);
                    List<ObjectInfoDTO> objList = mc.getObjectInfoList(helperInfo, type);
                    for (ObjectInfoDTO obj : objList)
                    {
                        cbObjNameAdd.addItem(obj);
                    }
                }

                cbUserAdd.removeAllItems();
                cbUserAdd.addItem(constBundle.getString("userSort"));
                List<ObjectInfoDTO> userList = mc.getObjectInfoList(helperInfo, "user");
                for (ObjectInfoDTO user : userList)
                {
                    cbUserAdd.addItem(user);
                }
                cbUserAdd.setEnabled(true);
//                }
            } catch (Exception ex)
            {
                logger.error("Error " + ex.getMessage());
                ex.printStackTrace(System.out);
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void btnAddRuleAction(ActionEvent evt)
    {
        AuditRuleInfoDTO rule = new AuditRuleInfoDTO();
        rule.setObjectType(cbObjTypeAdd.getSelectedItem().toString());
        rule.setObjectTypeId(cbObjTypeAdd.getSelectedIndex()-1);
        if (cbObjTypeAdd.getSelectedItem().toString().equals("none"))
        {
            rule.setObjectId("0");
            rule.setObjectName(null);
        } else
        {
            ObjectInfoDTO obj = (ObjectInfoDTO) cbObjNameAdd.getSelectedItem();
            rule.setObjectId(obj.getOid());
            rule.setObjectName(obj.getName());
        }
        ObjectInfoDTO user = (ObjectInfoDTO) cbUserAdd.getSelectedItem();
        rule.setUser(user.getName());
        rule.setUserId(user.getOid());
        
        ObjectDTO operation = (ObjectDTO) cbOperationAdd.getSelectedItem();
        rule.setOperation(operation.getValue());
        rule.setOperationId(operation.getId());
        rule.setResult(cbResultAdd.getSelectedItem().toString());
        MonitorController mc = MonitorController.getInstance();
        rule.setResultId(mc.getIdByResult(cbResultAdd.getSelectedItem().toString()));
        this.addRowForRule(rule);
    }
    //batch insert and update 
    private void btnBatchCommitAction(ActionEvent evt)
    {
        int row = ruleModel.getRowCount();
        if (row < 0)
        {
            return;
        }
        List<AuditRuleInfoDTO> updateRuleList = new ArrayList();
        List<AuditRuleInfoDTO> insertRuleList = new ArrayList();
        MonitorController mc = MonitorController.getInstance();
        try
        {
            for (int i = 0; i < row; i++)
            {
                //logger.info("isSelected=" + ruleModel.getValueAt(i, 7) + ",oid = " + ruleModel.getValueAt(i, 0).toString());
                if ((Boolean) ruleModel.getValueAt(i, 7))
                {
                    AuditRuleInfoDTO rule = (AuditRuleInfoDTO) ruleModel.getValueAt(i, 0);
                    if (rule.getOid() != null)
                    {
                        String newResult = ruleModel.getValueAt(i, 5).toString();
                        if (!rule.getResult().equals(newResult))
                        {
                            rule.setResult(newResult);
                            rule.setResultId(mc.getIdByResult(newResult));
                            updateRuleList.add(rule);
                        }
                    } else
                    {
                        if (rule.getUserId() != null && !mc.isUserExistNow(helperInfo, rule.getUserId()))
                        {
                            JOptionPane.showMessageDialog(this, "Warning: user '" + rule.getUser() + "' just been dropd, the audit rule cannot be enable,",
                                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                        } else
                        {
                            String newResult = ruleModel.getValueAt(i, 5).toString();
                            if (!rule.getResult().equals(newResult))
                            {
                                rule.setResult(newResult);
                                rule.setResultId(mc.getIdByResult(newResult));
                            }
                            insertRuleList.add(rule);
                        }
                    }
                }
            }
            if (insertRuleList.size() > 0)
            {
                mc.batchInsertAuditRule(helperInfo, insertRuleList);
            }
            if (updateRuleList.size() > 0)
            {
                mc.batchUpdateAuditRule(helperInfo, updateRuleList);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    //batch delete
    private void deleteAuditRuleAction(ActionEvent evt)
    {
        int row = ruleModel.getRowCount();
        if (row < 0)
        {
            return;
        }       
        try
        {
            List<Long> oidList = new ArrayList();
            for (int i = row - 1; i >= 0; i--)
            {
                logger.info("isSelected=" + ruleModel.getValueAt(i, 7) + ",oid = " + ruleModel.getValueAt(i, 0));
                if ((Boolean) ruleModel.getValueAt(i, 7))
                {
                    if (ruleModel.getValueAt(i, 0).toString() != null)
                    {
                        oidList.add(Long.valueOf(ruleModel.getValueAt(i, 0).toString()));
                    }
                    ruleModel.removeRow(i);
                }
            }
            if (oidList.size() > 0)
            {
                MonitorController mc = MonitorController.getInstance();
                mc.batchDeleteAuditRule(helperInfo, oidList);
            }            
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    
    //for log
    private void btnConfigActionPerformed(ActionEvent evt)                                          
    {                                              
        ConfigLogValidityDialog cdialog = new ConfigLogValidityDialog(this, true, logValidSince);
        cdialog.setVisible(true);
    }
   //for log historyquery
    private void initAuditLogView()
    {
        String[] logHeaderArray = new String[]
        {
            constBundle.getString("logTimestamp"),
            constBundle.getString("user"),
            constBundle.getString("database"),
            constBundle.getString("connectionFrom"),//add
            constBundle.getString("operationType"),//add
            constBundle.getString("operatingRecord"),//("query"),
            constBundle.getString("operatingResult"),//add
            constBundle.getString("msg"),
            constBundle.getString("illegalEventOid")
        };
        tblAuditLog.setModel(new DefaultTableModel(logHeaderArray, 0)
        {
            public Class[] types = new Class[]
            {
                LogInfoDTO.class,
                Object.class,
                Object.class,
                Object.class,//add
                Object.class,//add
                Object.class,//add
                Object.class,
                Object.class,
                Object.class
            };
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
        });
        tblAuditLog.getTableHeader().setReorderingAllowed(false);
        tblAuditLog.getColumn(constBundle.getString("logTimestamp")).setMaxWidth(165);
        tblAuditLog.getColumn(constBundle.getString("logTimestamp")).setMinWidth(165);
        tblAuditLog.getColumn(constBundle.getString("user")).setWidth(50);
        tblAuditLog.getColumn(constBundle.getString("database")).setWidth(50);
        tblAuditLog.getColumn(constBundle.getString("connectionFrom")).setMaxWidth(100);//add
        tblAuditLog.getColumn(constBundle.getString("connectionFrom")).setMinWidth(100);//add
        tblAuditLog.getColumn(constBundle.getString("operationType")).setMaxWidth(65);//add
        tblAuditLog.getColumn(constBundle.getString("operationType")).setMinWidth(65);//add
        tblAuditLog.getColumn(constBundle.getString("illegalEventOid")).setMaxWidth(80);
        tblAuditLog.getColumn(constBundle.getString("illegalEventOid")).setMinWidth(70);
        
        JSpinner.DateEditor startEditor = new JSpinner.DateEditor(spinStart, "yyyy-MM-dd HH:mm:ss");
        spinStart.setEditor(startEditor);
        JSpinner.DateEditor endEditor = new JSpinner.DateEditor(spinEnd, "yyyy-MM-dd HH:mm:ss");
        spinEnd.setEditor(endEditor);

        JFormattedTextField tf = ((JSpinner.DateEditor) spinStart.getEditor()).getTextField();
        DefaultFormatterFactory factory = (DefaultFormatterFactory) tf.getFormatterFactory();
        DateFormatter formatter = (DateFormatter) factory.getDefaultFormatter();
        formatter.setAllowsInvalid(false);

        JFormattedTextField tfEnd = ((JSpinner.DateEditor) spinEnd.getEditor()).getTextField();
        DefaultFormatterFactory factoryEnd = (DefaultFormatterFactory) tfEnd.getFormatterFactory();
        DateFormatter formatterEnd = (DateFormatter) factoryEnd.getDefaultFormatter();
        formatterEnd.setAllowsInvalid(false);

        btnQueryLog.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnQueryLogActionPerformed(e);
            }
        });
        tblAuditLog.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tblAuditLogMouseClicked(evt);
            }
        });
    }
    private SimpleDateFormat dateFormate =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String getStartTime()
    {
        String start = dateFormate.format(spinStart.getValue());
        logger.info(start);
        return start;
    }
    private String getEndTime()
    {
        String end = dateFormate.format(spinEnd.getValue());
        logger.info(end);
        return end;
    }
    private void btnQueryLogActionPerformed(ActionEvent evt)
    {
        MonitorController mc = MonitorController.getInstance();
        DefaultTableModel logModel = (DefaultTableModel) tblAuditLog.getModel();
        this.emptyTableData(logModel);
        try
        {
            mc.queryLogByCondition(helperInfo, this.getStartTime(), this.getEndTime(),txfUser.getText(),logModel);
//            List<LogInfoDTO> list = mc.queryLogByCondition(helperInfo, this.getStartTime(), this.getEndTime());
//            for (LogInfoDTO log : list)
//            {
//                //logger.info("logTime=" + log.getLogTime());
//                Object[] row = new Object[6];
//                row[0] = log;
//                row[1] = log.getUser();
//                row[2] = log.getDatabase();
//                row[3] = log.getQuery();
//                row[4] = log.getMsg();
//                row[5] = log.getIllegalEventId();
//                logModel.addRow(row);
//            }
        } catch (Exception ex)
        {
            logger.error("Error " + ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
        }
    }
    private void tblAuditLogMouseClicked(MouseEvent evt)
    {
        int clickCount = evt.getClickCount();
        int selectRow = tblAuditLog.getSelectedRow();
        if (clickCount == 2 && selectRow >= 0)
        {
            logger.info("selectRow=" + selectRow);
            DefaultTableModel logModel = (DefaultTableModel) tblAuditLog.getModel();
            LogInfoDTO log = (LogInfoDTO) logModel.getValueAt(selectRow, 0);            
            try
            {
                //MonitorController mc = MonitorController.getInstance();
                //IllegalEventInfoDTO event = mc.getIllegalEventByOid(helperInfo, log.getIllegalEventId());
                LogDetailDialog dd = new LogDetailDialog(this, true, log);//, event
                dd.setLocationRelativeTo(this);
                dd.setVisible(true);
            } catch (Exception ex)
            {
                logger.error("Error " + ex.getMessage());
                ex.printStackTrace(System.out);
                JOptionPane.showMessageDialog(null, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    
    //for log monitor
    private final Toolkit kit = this.getToolkit();// new Frame()
    private void initLogStateMonitor()
    {
        String[] taskHeaderArray = new String[]
        {
            constBundle.getString("logTimestamp"),
            constBundle.getString("user"),
            constBundle.getString("database"),
            constBundle.getString("connectionFrom"),//add
            constBundle.getString("operationType"),//add
            constBundle.getString("operatingRecord"),//("query"),
            constBundle.getString("operatingResult"),//add
            constBundle.getString("msg"),
            constBundle.getString("illegalEventOid"),
            constBundle.getString("securityLevel")//add
        };
        tblLogState = new LogMonitorTable();        
        spTableState.setViewportView(tblLogState);
        tblLogState.setModel(new DefaultTableModel(taskHeaderArray, 0)
        {
            public Class[] types = new Class[]
            {
                LogInfoDTO.class,
                Object.class,
                Object.class,
                Object.class,//add
                Object.class,//add
                Object.class,//add
                Object.class,
                Object.class,
                IllegalEventInfoDTO.class,
                Object.class//add
            };
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return types[columnIndex];
            }
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }          
        });
        tblLogState.getTableHeader().setReorderingAllowed(false);
        tblLogState.getColumn(constBundle.getString("logTimestamp")).setMaxWidth(165);
        tblLogState.getColumn(constBundle.getString("logTimestamp")).setMinWidth(165);
        tblLogState.getColumn(constBundle.getString("user")).setMaxWidth(60);
        tblLogState.getColumn(constBundle.getString("database")).setMaxWidth(60);
        tblLogState.getColumn(constBundle.getString("connectionFrom")).setMaxWidth(100);//add
        tblLogState.getColumn(constBundle.getString("connectionFrom")).setMinWidth(10);
        tblLogState.getColumn(constBundle.getString("operationType")).setMaxWidth(65);//add
        tblLogState.getColumn(constBundle.getString("operationType")).setMinWidth(65);//add
        tblLogState.getColumn(constBundle.getString("illegalEventOid")).setMaxWidth(80);
        tblLogState.getColumn(constBundle.getString("illegalEventOid")).setMinWidth(70);
        tblLogState.getColumn(constBundle.getString("securityLevel")).setMaxWidth(70);//add
        
        logger.info("start timer task");
        final DefaultTableModel model = (DefaultTableModel) tblLogState.getModel();
        final MonitorController mc = MonitorController.getInstance();
        TimerTask task = new TimerTask()
        {            
            @Override
            public void run()
            {               
                try
                {
                    List<LogInfoDTO> list = mc.getNewLogList(helperInfo, lastTime);
                     //logger.info("run******log size="+list.size());
                    for (LogInfoDTO log : list)
                    {
                        //logger.info("logTime=" + log.getLogTime());
                        Object[] row = new Object[10];
                        row[0] = log;
                        row[1] = log.getUser();
                        row[2] = log.getDatabase();
                        row[3] = log.getConnectionFrom();//add 
                        row[4] = log.getOperationType();//add
                        row[5] = log.getQuery();
                        row[6] = log.getOperationResult();
                        row[7] = log.getMsg();
                        row[8] = log.getIllegalEventId();
                        row[9] = log.getEventSeverity();//security level
                        model.addRow(row); 
                        if (log.getEventSeverity() > 1)
                        {
                            logger.warn("Before beep EventSeverity=" + log.getEventSeverity());
                            kit.beep();//voice , add 2018-3-20 Li Peng, BeiJing Military test
                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e)
                            {}
                            logger.warn("After beep EventSeverity=" + log.getEventSeverity());
                        }
                        lastTime = log.getLogTime();
//                        if (log.getEventSeverity() > 1)// if (event != null && !event.getAction().equals("log"))
//                        {
//                            JOptionPane.showMessageDialog(tblLogState,
//                                    constBundle.getString("illegalEventOccur") + "!" + constBundle.getString("illegalEventOid") + ":" + log.getIllegalEventId(),//event.getEventName() + "!"
//                                    constBundle.getString("eventWarning"), JOptionPane.WARNING_MESSAGE);                      
//                        }                        
                        
                        //row limit
                        if(model.getRowCount()==301)
                        {
                            model.removeRow(0);                        
                        }
                        
                        //set scroll bar to the last row
                        int rowCount = model.getRowCount();
                        if (rowCount > 0)//to avoid null pointer exception
                        {
                            tblLogState.getSelectionModel().setSelectionInterval(rowCount - 1, rowCount - 1);
                            Rectangle rect = tblLogState.getCellRect(rowCount - 1, 0, true);
                            tblLogState.updateUI();
                            tblLogState.scrollRectToVisible(rect);
                        }
                        //another method, effect is worse
                        //JScrollBar scrollBar = spTableState.getVerticalScrollBar();
                        //scrollBar.setValue(scrollBar.getMaximum());
                    }
                } catch (Exception ex)
                {
                    logger.error(ex.getMessage());
                    ex.printStackTrace(System.out);
                    JOptionPane.showMessageDialog(tblLogState, ex.getMessage(),
                            constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        Timer timer = new Timer();
        //long delay = 3;
        long intevalPeriod = 1000;//1000ms = 1s
        //schedules the task to be run in an interval
        timer.scheduleAtFixedRate(task, lastTime, intevalPeriod);
        tblLogState.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent evt)
            {
                tblStateMouseClicked(evt);
            }
        });
    }  
    private void tblStateMouseClicked(MouseEvent evt)
    {
        int clickCount = evt.getClickCount(); 
        int selectRow = tblLogState.getSelectedRow();       
        if (clickCount == 2 && selectRow >= 0)
        {
            logger.info("selectRow=" + selectRow);
            DefaultTableModel model = (DefaultTableModel) tblLogState.getModel();
            LogDetailDialog dd = new LogDetailDialog(this, true , (LogInfoDTO) model.getValueAt(selectRow, 0));//,(IllegalEventInfoDTO) model.getValueAt(selectRow, 5)
            dd.setLocationRelativeTo(this);
            dd.setVisible(true);
        }
    }
       
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex)
        {
            ex.printStackTrace(System.out);
        }
        //</editor-fold>

        /* Create and display the form */
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
//                Enumeration e = org.apache.log4j.Logger.getRootLogger().getAllAppenders();
//                while (e.hasMoreElements())
//                {
//                    Appender app = (Appender) e.nextElement();
//                    if (app instanceof FileAppender)
//                    {
//                        System.out.println("File: " + ((FileAppender) app).getFile());
//                    }
//                }              
                RegisterDialog registerDialog = new RegisterDialog(null, true);
                registerDialog.setLocationRelativeTo(null);
                registerDialog.setAlwaysOnTop(true);
                registerDialog.setVisible(true);
                if (registerDialog.isSuccess())
                {
                    HelperInfoDTO helperInfo = registerDialog.getHelperInfo();
                    MonitorView mv = new MonitorView(helperInfo);
                    mv.setLocationRelativeTo(null);
                    mv.setVisible(true);                    
                }
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddEvent;
    private javax.swing.JButton btnAddRule;
    private javax.swing.JButton btnBatchCommit;
    private javax.swing.JButton btnBatchCommitForEvent;
    private javax.swing.JButton btnConfig;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnHelp;
    private javax.swing.JButton btnQueryLog;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JButton btnUpdateRecordSensitivity;
    private javax.swing.JComboBox cbAction;
    private javax.swing.JComboBox cbActionSort;
    private javax.swing.JComboBox cbEvent;
    private javax.swing.JComboBox cbEventSort;
    private javax.swing.JComboBox cbObjNameAdd;
    private javax.swing.JComboBox cbObjNameForEvent;
    private javax.swing.JComboBox cbObjType4Label;
    private javax.swing.JComboBox cbObjTypeAdd;
    private javax.swing.JComboBox cbObjTypeForEvent;
    private javax.swing.JComboBox cbObjTypeForEventSort;
    private javax.swing.JComboBox cbObjTypeSort;
    private javax.swing.JComboBox cbOperationAdd;
    private javax.swing.JComboBox cbOperationForEvent;
    private javax.swing.JComboBox cbOperationSort;
    private javax.swing.JComboBox cbResultAdd;
    private javax.swing.JComboBox cbResultSort;
    private javax.swing.JComboBox cbRowLimit;
    private javax.swing.JComboBox cbTable4Label;
    private javax.swing.JComboBox cbTables;
    private javax.swing.JComboBox cbUserAdd;
    private javax.swing.JComboBox cbUserSort;
    private javax.swing.JMenuItem itemAboutUs;
    private javax.swing.JMenuItem itemDeleteAuditRule;
    private javax.swing.JMenuItem itemDeleteIllegalEvent;
    private javax.swing.JMenuItem itemEditAuditRule;
    private javax.swing.JMenuItem itemEditIllegalEvent;
    private javax.swing.JMenuItem itemExit;
    private javax.swing.JMenuItem itemHelp;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblCustomEvent;
    private javax.swing.JLabel lblCustomRule;
    private javax.swing.JLabel lblEndTime;
    private javax.swing.JLabel lblEvent;
    private javax.swing.JLabel lblEventCount;
    private javax.swing.JLabel lblQuery;
    private javax.swing.JLabel lblSensitivity;
    private javax.swing.JLabel lblStartTime;
    private javax.swing.JLabel lblTableName;
    private javax.swing.JLabel lblUser;
    private javax.swing.JMenu menuAudit;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenu menuFile;
    private javax.swing.JMenu menuHelp;
    private javax.swing.JMenu menuSecurity;
    private javax.swing.JPanel pnlAuditLog;
    private javax.swing.JPanel pnlAuditRule;
    private javax.swing.JPanel pnlIllegalEvent;
    private javax.swing.JPanel pnlObjLabel;
    private javax.swing.JPanel pnlRecordLabel;
    private javax.swing.JPanel pnlState;
    private javax.swing.JPopupMenu.Separator sepHelp;
    private javax.swing.JScrollPane spAuditRule;
    private javax.swing.JScrollPane spIllegalEvent;
    private javax.swing.JScrollPane spLabel;
    private javax.swing.JScrollPane spLog;
    private javax.swing.JScrollPane spRecord;
    private javax.swing.JScrollPane spTableEvent;
    private javax.swing.JScrollPane spTableRule;
    private javax.swing.JScrollPane spTableState;
    private javax.swing.JSpinner spinEnd;
    private javax.swing.JSpinner spinRecordSensitivity;
    private javax.swing.JSpinner spinStart;
    private javax.swing.JSpinner spnSensitivity;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JTable tblAuditLog;
    private javax.swing.JTable tblEvent;
    private javax.swing.JTable tblLabel;
    private javax.swing.JTable tblLogState;
    private javax.swing.JTable tblRecord;
    private javax.swing.JTable tblRule;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JTextField txfUser;
    private javax.swing.JTextField txfdFailedTime;
    // End of variables declaration//GEN-END:variables
}
