/* ------------------------------------------------
 *
 * File: LogMonitorTable.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\view\LogMonitorTable.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.view;

import com.highgo.hgdbadmin.audit.model.LogInfoDTO;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Liu Yuanyuan
 */
public class LogMonitorTable extends JTable
{
    private LogTableCellRenderer render = null;        
    @Override
    public TableCellRenderer getCellRenderer(int row, int column)
    {
        if (render == null)
        {
            render = new LogTableCellRenderer();
        }
        return render;
    }
    
    private Color blue = new Color(51, 154, 254);
    class LogTableCellRenderer extends DefaultTableCellRenderer
    {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            this.setColor(cell, table, isSelected, hasFocus, row, column);
            return cell;
        }

        private void setColor(Component component, JTable table, boolean isSelected, boolean hasFocus, int row, int column)
        {
            LogInfoDTO log = (LogInfoDTO) table.getValueAt(row, 0);
            if (log.getEventSeverity() > 1)
            {                
                component.setBackground(Color.red);
            } else
            {
                if (isSelected)
                {
                    component.setBackground(blue);
                } else
                {
                    component.setBackground(Color.white);
                }
            }
        }
    }
}
