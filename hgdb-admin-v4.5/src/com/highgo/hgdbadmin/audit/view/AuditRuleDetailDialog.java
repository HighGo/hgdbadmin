/* ------------------------------------------------
 *
 * File: AuditRuleDetailDialog.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\view\AuditRuleDetailDialog.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.view;

import com.highgo.hgdbadmin.audit.model.AuditRuleInfoDTO;
import com.highgo.hgdbadmin.audit.model.HelperInfoDTO;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Liu Yuanyuan
 */
public class AuditRuleDetailDialog extends JDialog
{
    
    private Logger logger = LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants_audit");

    private boolean isSuccess;
    private HelperInfoDTO helperInfo;
    private AuditRuleInfoDTO ruleInfo;
    /**
     * Creates new form DetailDialog
     *
     * @param parent
     * @param modal
     * @param helperInfo
     * @param ruleInfo
     */
    public AuditRuleDetailDialog(Frame parent, boolean modal,HelperInfoDTO helperInfo, AuditRuleInfoDTO ruleInfo)
    {
        super(parent, modal);
        this.helperInfo = helperInfo;
        this.ruleInfo = ruleInfo;
        isSuccess = false;
        initComponents();
              
        /*
        if (ruleInfo == null)
        {
            this.setTitle(constBundle.getString("customAuditRule"));
            this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/hgdbmonitor/images/create.png")));
            String[] objTypes = new String[]
            {
                "user", "tablespace", "database", "schema",
                "sequence", "view", "table", "index", "rule", "function", "trigger", "constraint"
            };
            cbObjType.setModel(new DefaultComboBoxModel(objTypes));
            cbObjType.setSelectedIndex(-1);
            cbObjType.setEnabled(true);
            cbObjName.setSelectedIndex(-1);
            cbObjName.setEnabled(true);
            cbUser.setSelectedIndex(-1);
            cbUser.setEnabled(true);
            cbOperation.setSelectedIndex(-1);
            cbOperation.setEnabled(true);
            cbResult.setSelectedIndex(-1);
            cbObjType.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    enableBtnOkWhenAddRule();
                    cbObjTypeStateChanged(e);
                }                
            });
            cbObjName.addItemListener(new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    enableBtnOkWhenAddRule();
                    cbObjNameStateChanged(e);
                }
            });
            ItemListener enableBtnOkListener = new ItemListener()
            {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    enableBtnOkWhenAddRule();
                }
            };
            cbUser.addItemListener(enableBtnOkListener);
            cbOperation.addItemListener(enableBtnOkListener);
            cbResult.addItemListener(enableBtnOkListener);
        } else
        */
        this.setTitle(constBundle.getString("auditRuleDetail"));
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/com/highgo/hgdbadmin/audit/images/property.png")));
        cbObjType.addItem(ruleInfo.getObjectType());
        cbObjType.setSelectedIndex(0);
        txfdObjOid.setText(ruleInfo.getObjectId());
        cbObjName.addItem(ruleInfo.getObjectName());
        cbObjName.setSelectedIndex(0);
        cbUser.addItem(ruleInfo.getUser());
        cbUser.setSelectedIndex(0);
        cbOperation.addItem(ruleInfo.getOperation());
        cbOperation.setSelectedIndex(0);
        String[] results = new String[]
        {
            "success", "failed", "all"
        };
        cbResult.setModel(new DefaultComboBoxModel(results));
        cbResult.setSelectedItem(ruleInfo.getResult());
        cbResult.addItemListener(new ItemListener()
        {
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                cbResult4EditStateChanged(e);
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        lblObjType = new javax.swing.JLabel();
        cbObjType = new javax.swing.JComboBox();
        lblObjOid = new javax.swing.JLabel();
        txfdObjOid = new javax.swing.JTextField();
        lblObjName = new javax.swing.JLabel();
        cbObjName = new javax.swing.JComboBox();
        cbOperation = new javax.swing.JComboBox();
        lblOperation = new javax.swing.JLabel();
        lblUser = new javax.swing.JLabel();
        cbUser = new javax.swing.JComboBox();
        lblResult = new javax.swing.JLabel();
        cbResult = new javax.swing.JComboBox();
        btnClose = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblObjType.setText(constBundle.getString("objectType"));

        cbObjType.setEnabled(false);
        cbObjType.setPreferredSize(new java.awt.Dimension(100, 21));

        lblObjOid.setText(constBundle.getString("objectOid"));

        txfdObjOid.setEnabled(false);
        txfdObjOid.setPreferredSize(new java.awt.Dimension(60, 21));

        lblObjName.setText(constBundle.getString("objectName"));

        cbObjName.setEnabled(false);
        cbObjName.setPreferredSize(new java.awt.Dimension(150, 21));

        cbOperation.setEnabled(false);

        lblOperation.setText(constBundle.getString("operation")
        );

        lblUser.setText(constBundle.getString("user"));

        cbUser.setEnabled(false);

        lblResult.setText(constBundle.getString("result"));

        btnClose.setText(constBundle.getString("close"));
        btnClose.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnCloseActionPerformed(evt);
            }
        });

        btnOk.setText(constBundle.getString("ok"));
        btnOk.setEnabled(false);
        btnOk.addActionListener(new java.awt.event.ActionListener()
        {
            public void actionPerformed(java.awt.event.ActionEvent evt)
            {
                btnOkActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblUser, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblObjOid, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblObjType, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblOperation, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblResult, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbResult, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbOperation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txfdObjOid, javax.swing.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblObjName, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(cbObjName, 0, 191, Short.MAX_VALUE))
                            .addComponent(cbUser, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbObjType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnOk)
                        .addGap(10, 10, 10)
                        .addComponent(btnClose)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblObjType)
                    .addComponent(cbObjType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblObjOid)
                    .addComponent(txfdObjOid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbObjName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblObjName))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblUser))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOperation)
                    .addComponent(cbOperation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblResult)
                    .addComponent(cbResult, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClose)
                    .addComponent(btnOk))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCloseActionPerformed
    {//GEN-HEADEREND:event_btnCloseActionPerformed
        // TODO add your handling code here:
        logger.info("close");
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed
    private void btnOkActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOkActionPerformed
    {//GEN-HEADEREND:event_btnOkActionPerformed
        // TODO add your handling code here:
        logger.info("btn ok");
        isSuccess = true;
        this.dispose();
    }//GEN-LAST:event_btnOkActionPerformed

    //for Edit
    private void cbResult4EditStateChanged(ItemEvent evt)
    {
        logger.info("select result = " + cbResult.getSelectedItem() );
        if (cbResult.getSelectedIndex() >= 0
                && !cbResult.getSelectedItem().toString().equals(ruleInfo.getResult()))
        {
            btnOk.setEnabled(true);
        } else
        {
            btnOk.setEnabled(false);
        }
    }

    public boolean isSuccess()
    {
        return this.isSuccess;
    }

    public String getNewResult()
    {
        return cbResult.getSelectedItem().toString();
    }
    
    /*    
    private void btnOkActionPerformed(java.awt.event.ActionEvent evt)                                      
    {                                          
        // TODO add your handling code here:
        logger.info("btn ok");
        isSuccess = true;
        this.dispose();
//        MonitorController mc = MonitorController.getInstance();
//        try
//        {
//            if (ruleInfo == null)
//            {
//                AuditRuleInfoDTO newRule = this.getAuditRuleInfo();
//                if (newRule.getObjectType() != null && newRule.getObjectType().equals("user"))
//                {
//                    mc.insertAuditRule(helperInfo, newRule);
//                    isSuccess = true;
//                    this.dispose();
//                } else
//                {
//                    if (mc.isUserExistNow(helperInfo, newRule.getUserId()))
//                    {
//                        mc.insertAuditRule(helperInfo, newRule);
//                        isSuccess = true;
//                        this.dispose();
//                    } else
//                    {
//                        JOptionPane.showMessageDialog(this, "Warning: user '" + newRule.getUser() + "' just been dropd, the audit rule cannot be insert,",
//                                constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
//                        cbUser.removeItem(cbUser.getSelectedItem());
//                    }
//                }             
//            } else
//            {
//                mc.updateAuditRule(helperInfo, this.getAuditRuleInfo());
//                isSuccess = true;
//                this.dispose();
//            }            
//        } catch (Exception ex)
//        {
//            isSuccess = false;
//            logger.error("Error " + ex.getMessage());
//            ex.printStackTrace(System.out);
//            JOptionPane.showMessageDialog(this, ex.getMessage(),
//                    constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
//        }
    } 
    //for add
    private void cbObjTypeStateChanged(ItemEvent evt)
    {
        if (cbObjType.getSelectedIndex() >= 0)
        {
            String type = cbObjType.getSelectedItem().toString();
            MonitorController mc = MonitorController.getInstance();
            cbOperation.setModel(new DefaultComboBoxModel(mc.getOperationArrayByObjectType(type)));
            try
            {
                List<ObjectInfoDTO> objList = mc.getObjectInfoList(helperInfo, type);
                cbObjName.removeAllItems();
                for (ObjectInfoDTO obj : objList)
                {
                    cbObjName.addItem(obj);
                }
                if (type.equals("user"))
                {
                    cbUser.removeAllItems();
                    cbUser.setEnabled(false);
                } else
                {
                    cbUser.removeAllItems();
                    List<ObjectInfoDTO> userList = mc.getObjectInfoList(helperInfo, "user");
                    for (ObjectInfoDTO user : userList)
                    {
                        cbUser.addItem(user);
                    }
                    cbUser.setEnabled(true);
                }
            } catch (Exception ex)
            {
                logger.error("Error " + ex.getMessage());
                ex.printStackTrace(System.out);
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void cbObjNameStateChanged(ItemEvent evt)
    {
        if (cbObjName.getSelectedIndex() >= 0)
        {
            ObjectInfoDTO obj = (ObjectInfoDTO) cbObjName.getSelectedItem();
            txfdObjOid.setText(obj.getOid());
        }
    }
    private void enableBtnOkWhenAddRule()
    {
        if (cbObjType.getSelectedIndex() < 0)
        {
            btnOk.setEnabled(false);
            return;
        }
        String type = cbObjType.getSelectedItem().toString();
        int objName = cbObjName.getSelectedIndex();
        int owner = cbUser.getSelectedIndex();
        int operation = cbOperation.getSelectedIndex();
        int result = cbResult.getSelectedIndex();
        if (type.equals("user") && objName >= 0
                && operation >= 0 && result >= 0)
        {
            btnOk.setEnabled(true);
        } else if (objName >= 0 && owner >= 0
                && operation >= 0 && result >= 0)
        {
            btnOk.setEnabled(true);
        } else
        {
            btnOk.setEnabled(false);
        }
    }
   
    public AuditRuleInfoDTO getAuditRuleInfo()
    {
        if (ruleInfo == null)
        {
            AuditRuleInfoDTO newRuleInfo = new AuditRuleInfoDTO();
            newRuleInfo.setObjectType(cbObjType.getSelectedItem().toString());
            ObjectInfoDTO obj = (ObjectInfoDTO) cbObjName.getSelectedItem();
            newRuleInfo.setObjectOid(obj.getOid());
            newRuleInfo.setObjectName(obj.getName());
            if (cbUser.isEnabled())
            {
                ObjectInfoDTO user = (ObjectInfoDTO) cbUser.getSelectedItem();
                newRuleInfo.setUserId(user.getOid());
                newRuleInfo.setUser(user.getName());                
            } else
            {
                newRuleInfo.setUser(null);
                newRuleInfo.setUserId(null);
            }
            newRuleInfo.setOperation(cbOperation.getSelectedItem().toString());
            newRuleInfo.setResult(cbResult.getSelectedItem().toString());
            return newRuleInfo;
        } else
        {
            ruleInfo.setResult(cbResult.getSelectedItem().toString());
            return this.ruleInfo;
        }
    }
    */
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnOk;
    private javax.swing.JComboBox cbObjName;
    private javax.swing.JComboBox cbObjType;
    private javax.swing.JComboBox cbOperation;
    private javax.swing.JComboBox cbResult;
    private javax.swing.JComboBox cbUser;
    private javax.swing.JLabel lblObjName;
    private javax.swing.JLabel lblObjOid;
    private javax.swing.JLabel lblObjType;
    private javax.swing.JLabel lblOperation;
    private javax.swing.JLabel lblResult;
    private javax.swing.JLabel lblUser;
    private javax.swing.JTextField txfdObjOid;
    // End of variables declaration//GEN-END:variables
}
