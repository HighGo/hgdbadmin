/* ------------------------------------------------
 *
 * File: MonitorController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\controller\MonitorController.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.controller;

import com.highgo.hgdbadmin.audit.model.AuditRuleInfoDTO;
import com.highgo.hgdbadmin.audit.model.IllegalEventInfoDTO;
import com.highgo.hgdbadmin.audit.model.HelperInfoDTO;
import com.highgo.hgdbadmin.audit.model.LabeledObjInfoDTO;
import com.highgo.hgdbadmin.audit.model.LabeledRecordInfoDTO;
import com.highgo.hgdbadmin.audit.model.LogInfoDTO;
import com.highgo.hgdbadmin.audit.model.ObjectDTO;
import com.highgo.hgdbadmin.audit.model.ObjectInfoDTO;
import com.highgo.hgdbadmin.audit.util.JdbcHelper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.Key;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.crypto.Cipher;
import javax.swing.table.DefaultTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Liu Yuanyuan
 */
public class MonitorController
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants_audit");

    private static MonitorController mc = null;
    public static MonitorController getInstance()
    {
        if (mc == null)
        {
            mc = new MonitorController();
        }
        return mc;
    }
    
    //for label
    private String getObjLabelSQL(String type)
    {
        StringBuilder sql = new StringBuilder();
        /*
        switch (type)
        {
            case "user":
//                sql.append("select c.oid, NULL AS schema, c.rolname, c.ctid, ")
//                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_authid', ")
//                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
//                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
//                        .append(" from pg_authid c WHERE c.rolname NOT IN('syssao','syssso','sysdba')");
                sql.append("SELECT c.oid, null AS schema, c.rolname, c.ctid, c.label")
                        .append(" FROM pg_authid c")
                        .append(" WHERE c.rolname NOT IN('syssao','syssso','sysdba')");
                break;
            case "tablespace":
                sql.append("SELECT c.oid, NULL AS schema,c.spcname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_tablespace', ")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_tablespace c")
                        .append(" WHERE c.spcname NOT IN('pg_default','pg_global')");
                break;    
            case "database":
                sql.append("SELECT c.oid, NULL AS schema, c.datname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_database',")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_database c")
                        .append(" WHERE c.datname NOT IN('template1', 'template0')");
                break;
            case "schema":
                sql.append("SELECT c.oid, NULL AS schema, c.nspname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_namespace', ")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")                        
                        .append(" FROM pg_namespace c")
                        .append(" WHERE c.nspname NOT IN('pg_catalog', 'hgdb_catalog', 'oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            case "table":
                sql.append("SELECT c.oid, n.nspname AS schema,c.relname, c.ctid, ")
                        .append("(select p.t_infomask3 from heap_page_items(get_raw_page('pg_class', ")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_class c")
                        .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")                       
                        .append(" WHERE c.relkind = 'r'::\"char\"")
                        .append(" AND n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");                        
                break;
            case "view":
                sql.append("select c.oid, n.nspname AS schema,c.relname, c.ctid,  ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_class',")
                        .append("  btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" from pg_class c")
                        .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")                       
                        .append(" WHERE ((c.relhasrules AND (EXISTS ")
                        .append(" (SELECT r.rulename FROM pg_rewrite r  WHERE ((r.ev_class = c.oid) AND (bpchar(r.ev_type) = '1'::bpchar)) ) ) )")
                        .append(" OR (c.relkind = 'v'::char))")
                        .append(" AND n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            case "sequence":
                sql.append("SELECT c.oid, n.nspname AS schema,c.relname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_class', ")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_class c")
                        .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace")
                        .append(" WHERE c.relkind = 'S'")
                        .append(" AND n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;       
            case "index":
                sql.append("SELECT c.indexrelid, n.nspname as schema, cls.relname AS name, c.ctid,  ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_index',")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_index c")
                        .append(" JOIN pg_class cls ON cls.oid = c.indexrelid")
                        .append(" LEFT OUTER JOIN  pg_namespace n ON n.oid = cls.relnamespace")
                        .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            case "function":
                sql.append("SELECT c.oid, n.nspname AS schema, c.proname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_proc',")
                        .append("  btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_proc c")
                        .append(" LEFT JOIN pg_namespace n  ON n.oid=c.pronamespace")
                        .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            case "rule":
                sql.append("SELECT c.oid, n.nspname as schema, c.rulename, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_rewrite', ")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" FROM pg_rewrite c")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid=c.ev_class")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid=cls.relnamespace")
                        .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            case "trigger":
                sql.append("SELECT c.oid, n.nspname as schema, c.tgname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_trigger', ")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" from pg_trigger c")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = c.tgrelid")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid =  cls.relnamespace")
                        .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            case "constraint":
                sql.append("SELECT c.oid, n.nspname as schema, c.conname, c.ctid, ")
                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_constraint',")
                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                        .append(" where p.t_ctid=c.ctid  AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                        .append(" from pg_constraint c")
                        .append(" LEFT OUTER JOIN pg_namespace n ON c.connamespace = n.oid")
                        .append(" AND c.contype NOT IN('t')")
                        .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
                break;
            default:
                logger.error(type + " is an Exception type, return null.");
                return null;
        }
        */
        if (type.equals("user"))
        {
//                sql.append("select c.oid, NULL AS schema, c.rolname, c.ctid, ")
//                        .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_authid', ")
//                        .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
//                        .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
//                        .append(" from pg_authid c WHERE c.rolname NOT IN('syssao','syssso','sysdba')");
                sql.append("SELECT c.oid, null AS schema, c.rolname, c.ctid, c.label")
                        .append(" FROM pg_authid c")
                        //.append(" WHERE c.rolname NOT IN('syssao','syssso','sysdba')");
                        //modify the where condition to remove syssso and its members from the result.
                        .append(" left outer join pg_auth_members m on m.member = c.oid ")
                        .append(" where c.oid <>13 ")
                        .append(" and (m.roleid is null or m.roleid<>13) ");
                        
        } else if (type.equals("tablespace"))
        {
            sql.append("SELECT c.oid, NULL AS schema,c.spcname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_tablespace', ")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_tablespace c")
                    .append(" WHERE c.spcname NOT IN('pg_default','pg_global')");
        } else if (type.equals("database"))
        {
            sql.append("SELECT c.oid, NULL AS schema, c.datname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_database',")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_database c")
                    .append(" WHERE c.datname NOT IN('template1', 'template0')");
        } else if (type.equals("schema"))
        {
            sql.append("SELECT c.oid, NULL AS schema, c.nspname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_namespace', ")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_namespace c")
                    .append(" WHERE c.nspname NOT IN('pg_catalog', 'hgdb_catalog', 'oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("table"))
        {
            sql.append("SELECT c.oid, n.nspname AS schema,c.relname, c.ctid, ")
                    .append("(select p.t_infomask3 from heap_page_items(get_raw_page('pg_class', ")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_class c")
                    .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                    .append(" WHERE c.relkind = 'r'::\"char\"")
                    .append(" AND n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("view"))
        {
            sql.append("select c.oid, n.nspname AS schema,c.relname, c.ctid,  ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_class',")
                    .append("  btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" from pg_class c")
                    .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                    .append(" WHERE ((c.relhasrules AND (EXISTS ")
                    .append(" (SELECT r.rulename FROM pg_rewrite r  WHERE ((r.ev_class = c.oid) AND (bpchar(r.ev_type) = '1'::bpchar)) ) ) )")
                    .append(" OR (c.relkind = 'v'::char))")
                    .append(" AND n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("sequence"))
        {
            sql.append("SELECT c.oid, n.nspname AS schema,c.relname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_class', ")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_class c")
                    .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace")
                    .append(" WHERE c.relkind = 'S'")
                    .append(" AND n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("index"))
        {
            sql.append("SELECT c.indexrelid, n.nspname as schema, cls.relname AS name, c.ctid,  ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_index',")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_index c")
                    .append(" JOIN pg_class cls ON cls.oid = c.indexrelid")
                    .append(" LEFT OUTER JOIN  pg_namespace n ON n.oid = cls.relnamespace")
                    .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("function"))
        {
            sql.append("SELECT c.oid, n.nspname AS schema, c.proname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_proc',")
                    .append("  btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_proc c")
                    .append(" LEFT JOIN pg_namespace n  ON n.oid=c.pronamespace")
                    .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("rule"))
        {
            sql.append("SELECT c.oid, n.nspname as schema, c.rulename, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_rewrite', ")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" FROM pg_rewrite c")
                    .append(" LEFT OUTER JOIN pg_class cls ON cls.oid=c.ev_class")
                    .append(" LEFT OUTER JOIN pg_namespace n ON n.oid=cls.relnamespace")
                    .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("trigger"))
        {
            sql.append("SELECT c.oid, n.nspname as schema, c.tgname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_trigger', ")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" from pg_trigger c")
                    .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = c.tgrelid")
                    .append(" LEFT OUTER JOIN pg_namespace n ON n.oid =  cls.relnamespace")
                    .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else if (type.equals("constraint"))
        {
            sql.append("SELECT c.oid, n.nspname as schema, c.conname, c.ctid, ")
                    .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('pg_constraint',")
                    .append(" btrim(split_part(c.ctid::varchar,',',1),'(')::int )) p ")
                    .append(" where p.t_ctid=c.ctid  AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                    .append(" from pg_constraint c")
                    .append(" LEFT OUTER JOIN pg_namespace n ON c.connamespace = n.oid")
                    .append(" AND c.contype NOT IN('t')")
                    .append(" WHERE n.nspname NOT IN('pg_catalog','hgdb_catalog','oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')");
        } else
        {
            logger.error(type + " is an Exception type, return null.");
            return null;
        }
        logger.info("Return: sql = " + sql.toString());
        return sql.toString();        
    }
    public void getLabeledObjectList(HelperInfoDTO helperInfo, String type,int rowLimit, DefaultTableModel labelModel) throws Exception
    {
        logger.info("Enter:type=" + type);
        //List<LabeledObjInfoDTO> objList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return;//objList;
        }
        
        String sql = this.getObjLabelSQL(type);
        if (sql == null)
        {
            logger.warn("sql is null ,do nothing and return an empty list.");
            return;//objList;
        }
        if (rowLimit != -1)
        {
            sql = sql + " limit " + rowLimit + " offset 0 ";
        }
        logger.info(sql);
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            stmt.setFetchSize(100);
            logger.info("fetchSize=" + stmt.getFetchSize());
            rs = stmt.executeQuery(sql);
            LabeledObjInfoDTO obj;
            Object[] rowData;
            while (rs.next())
            {
                obj = new LabeledObjInfoDTO();
                obj.setType(type);
                obj.setOid(rs.getString(1));
                obj.setSchema(rs.getString(2));
                obj.setName(rs.getString(3));
                obj.setCtid(rs.getObject(4).toString());
                obj.setSentivity(rs.getInt(5));
                //objList.add(obj);

                rowData = new Object[5];
                rowData[0] = obj.getType();
                rowData[1] = obj.getOid();
                rowData[2] = obj;
                rowData[3] = obj.getSentivity();
                rowData[4] = false;
                labelModel.addRow(rowData);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        //return objList;
    }
    
    private String getUpdateSensitivitySQL(String type,int newStt)
    {
        StringBuilder sql = new StringBuilder();
        /*
        switch (type)
        {
            //server
            case "tablespace":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_tablespace SET spcname = spcname WHERE  oid = ?");
                break;
//            case "user"://login
//                sql.append("LABEL ").append(newStt).append(" UPDATE pg_authid SET rolname = rolname WHERE oid = ?");
//                sql.append("ALTER USER ? LABEL ").append(newStt);
//                break;
            case "database":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_database SET datname = datname WHERE oid = ?");
                break;
            case "schema":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_namespace SET nspname = nspname WHERE oid = ?");
                break;
            case "table":
            case "view":
            case "sequence":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_class SET relname = relname WHERE oid = ?");
                break;
            case "function":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_proc SET proname = proname WHERE oid = ?");
                break;
            case "index":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_index SET indexrelid = indexrelid WHERE indexrelid = ?");
                break;
            case "rule":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_rewrite SET rulename=rulename WHERE oid = ?");
                break;
            case "trigger":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_trigger SET tgname=tgname WHERE oid = ?");
                break;
            case "constraint":
                sql.append("LABEL ").append(newStt).append(" UPDATE pg_constraint SET conname=conname WHERE oid = ?");
                break;
            default:
                logger.error(type + " is an ecexption type, return null.");
                break;
        }
         */
        if (type.equals("tablespace"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_tablespace SET spcname = spcname WHERE  oid = ?");
        } 
        else if (type.equals("database"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_database SET datname = datname WHERE oid = ?");
        } 
        else if (type.equals("schema"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_namespace SET nspname = nspname WHERE oid = ?");
        } 
        else if (type.equals("table")
                || type.equals("view")
                || type.equals("sequence"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_class SET relname = relname WHERE oid = ?");
        } 
        else if (type.equals("function"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_proc SET proname = proname WHERE oid = ?");
        } 
        else if (type.equals("index"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_index SET indexrelid = indexrelid WHERE indexrelid = ?");
        } 
        else if (type.equals("rule"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_rewrite SET rulename=rulename WHERE oid = ?");
        } 
        else if (type.equals("trigger"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_trigger SET tgname=tgname WHERE oid = ?");
        } 
        else if (type.equals("constraint"))
        {
            sql.append("LABEL ").append(newStt).append(" UPDATE pg_constraint SET conname=conname WHERE oid = ?");
        } 
        else
        {
            logger.error(type + " is an ecexption type, return null.");
        }
        logger.info("Return: sql = " + sql.toString());
        return sql.toString();
    }
    public void batchUpdateObjSensitivity(HelperInfoDTO helperInfo, String type, List<Long> lol, int newStt) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return;
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        String sql = this.getUpdateSensitivitySQL(type,newStt);
        logger.info(sql);
        if (sql == null)
        {
            logger.warn("sql is null ,do nothing and return.");
            return;
        }
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(sql);
            for (Long oid : lol)
            {
                stmt.setLong(1, oid);
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }
    public void batchUpdateUserSensitivity(HelperInfoDTO helperInfo, String type, List<String> userList, int newStt) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return;
        }
        Connection conn = null;
        Statement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.createStatement();            
            for (String user : userList)
            {
                String sql = "ALTER USER " + user + " LABEL " + newStt;
                logger.info(sql);
                stmt.executeUpdate(sql);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }
    
    public void getLabeledRecordList(HelperInfoDTO helperInfo, String tableWithSchema, int rowLimit,DefaultTableModel labelModel) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return;
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql.append("select t.ctid,")
                .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('")
                .append(tableWithSchema).append("', ")
                .append(" btrim(split_part(t.ctid::varchar,',',1),'(')::int )) p")
                .append(" where p.t_ctid=t.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                .append(" ,t.* from ")
                .append(tableWithSchema).append(" t");
        if(rowLimit!=-1)
        {
            sql.append(" limit ").append(rowLimit).append(" offset 0");
        }
        logger.info(sql.toString());
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            stmt.setFetchSize(100);
            logger.info("fetchSize="+stmt.getFetchSize());
            rs = stmt.executeQuery(sql.toString());
            int column = rs.getMetaData().getColumnCount();      
            LabeledRecordInfoDTO obj;
            Object[] rowData;
            StringBuilder record = new StringBuilder();
            int row=0;
            while (rs.next())
            {
                obj = new LabeledRecordInfoDTO();
                obj.setCtid(rs.getObject(1));
                obj.setSentivity(rs.getInt(2));
                obj.setaColumn(rs.getMetaData().getColumnName(3));                
                record.delete(0, record.length());
                record.append("(");
                for (int i = 3; i <= column; i++)
                {
                    if (i > 3)
                    {
                        record.append(",");
                    }
                    String name = rs.getMetaData().getColumnClassName(i);                   
                    //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
                    //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
                    /*
                    switch (name)
                    {
                        case "com.highgo.jdbc.util.PGmoney":
                        case "java.lang.Boolean":
                        case "java.sql.SQLXML"://xml
                         case "[B"://bytea
                            record.append("\"").append(rs.getString(i)).append("\"");
                            break;
                        default:
                            //logger.info("type=" + name);
                            //"java.lang.Integer"
                            record.append("\"").append(rs.getObject(i)).append("\"");
                            break;
                    }    
                     */
                    if (name.equals("com.highgo.jdbc.util.PGmoney")
                            || name.equals("java.lang.Boolean")
                            || name.equals("java.sql.SQLXML")//xml
                            || name.equals("[B"))//bytea
                    {
                        record.append("\"").append(rs.getString(i)).append("\"");
                    } else
                    {
                        record.append("\"").append(rs.getObject(i)).append("\"");
                    }
                }
                record.append(")");
                obj.setRowData(record.toString());
                
                rowData = new Object[3];
                rowData[0] = obj;
                rowData[1] = obj.getSentivity();
                rowData[2] = false;
                labelModel.addRow(rowData);
                row++;
                logger.info("row=" + row);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }
    public void batchUpdateRecordSensitivity(HelperInfoDTO helperInfo, String tableWithSchema, List<Object> ctildList, int newStt, String aColumn) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return;
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        StringBuilder sql = new StringBuilder();
        sql.append("LABEL ").append(newStt)
                .append(" UPDATE ").append(tableWithSchema)
                .append(" SET ").append(aColumn).append("=").append(aColumn)
                .append(" WHERE ctid = ?");
        logger.info(sql.toString());
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(sql.toString());
            for (Object ctid : ctildList)
            {
                stmt.setObject(1, ctid);
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            logger.error(ex.getMessage());
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }
  
    //for record
    public List<Object[]> getLabeledRecordList(HelperInfoDTO helperInfo, String tableWithSchema, int pageSize) throws Exception
    {
        List<Object[]> objList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return objList;
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        StringBuilder sql = new StringBuilder();
        sql.append("select t.ctid,")
                .append(" (select p.t_infomask3 from heap_page_items(get_raw_page('")
                .append(tableWithSchema).append("', ")
                .append(" btrim(split_part(t.ctid::varchar,',',1),'(')::int )) p")
                .append(" where p.t_ctid=t.ctid AND p.t_xmin::text::int > p.t_xmax::text::int) as sensitivity")
                .append(" ,t.* from ")
                .append(tableWithSchema).append(" t");
        if(pageSize!=-1)
        {
            sql.append(" limit ").append(pageSize).append(" offset 0");
        }
        logger.info(sql.toString());
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            stmt.setFetchSize(100);
            logger.info("fetchSize="+stmt.getFetchSize());
            rs = stmt.executeQuery(sql.toString());
            int column = rs.getMetaData().getColumnCount();      
            LabeledRecordInfoDTO obj;
            Object[] rowData;
            int row=0;
            while (rs.next())
            {
                obj = new LabeledRecordInfoDTO();
                obj.setCtid(rs.getObject(1));
                obj.setSentivity(rs.getInt(2));
                obj.setaColumn(rs.getMetaData().getColumnName(3));
                
                StringBuilder rowDatas = new StringBuilder();
                rowDatas.append("(");
                for (int i = 3; i <= column; i++)
                {
                    if (i > 3)
                    {
                        rowDatas.append(",");
                    }
                    String name = rs.getMetaData().getColumnClassName(i);                   
                    //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
                    //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
                    /*
                    switch (name)
                    {
                        case "com.highgo.jdbc.util.PGmoney":
                        case "java.lang.Boolean":
                        case "java.sql.SQLXML"://xml
                         case "[B"://bytea
                            rowDatas.append("\"").append(rs.getString(i)).append("\"");
                            break;
                        default:
                            //logger.info("type=" + name);
                            //"java.lang.Integer"
                            rowDatas.append("\"").append(rs.getObject(i)).append("\"");
                            break;
                    }
                    */
                    if (name.equals("com.highgo.jdbc.util.PGmoney")
                            || name.equals("java.lang.Boolean")
                            || name.equals("java.sql.SQLXML")//xml
                            || name.equals("[B"))//bytea
                    {
                        rowDatas.append("\"").append(rs.getString(i)).append("\"");
                    } else
                    {
                        rowDatas.append("\"").append(rs.getObject(i)).append("\"");
                    }
                }
                rowDatas.append(")");
                obj.setRowData(rowDatas.toString());               
                
                rowData = new Object[3];
                rowData[0] = obj;
                rowData[1] = obj.getSentivity();
                rowData[2] = false;
                objList.add(rowData);
                row++;
                //logger.info("row=" + row);
            }
            conn.setAutoCommit(true);
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        return objList;
    }
    
    
    
    
    
    //get object information
    private String getObjectSQL(String type)
    {
        logger.info("type = " + type);
        if (type == null)
        {
            logger.error("Error:objType is null, do nothing, return null.");
            return null;
        }
        StringBuilder sql = new StringBuilder();
        /*
        switch (type)
        {
            //server
            case "tablespace":
                sql.append(" SELECT spc.oid, spc.spcname, NULL AS schema, 'tablespace' AS type")
                        .append(" FROM pg_tablespace spc")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON spc.oid = des.objoid")
                        .append(" ORDER BY spc.spcname");
                break;
            case "user"://login
                sql.append("SELECT aut.oid, aut.rolname, NULL AS schema, 'user' AS type")
                        .append(" FROM pg_authid  aut")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON aut.oid = des.objoid ")
                        .append(" WHERE aut.rolcanlogin = true")
                        //.append(" AND aut.rolname NOT IN('syssso')")
                        .append(" ORDER BY aut.rolname ");
                break;
            case "database":
                sql.append("SELECT dat.oid, dat.datname, NULL AS schema, 'database' AS type")
                        .append(" FROM pg_database dat")
                        .append(" LEFT OUTER JOIN pg_shdescription des ON dat.oid = des.objoid")
                        .append(" WHERE dat.datname NOT IN('template1','template0') ")
                        .append(" ORDER BY dat.datname");
                break;
            case "schema":
                sql.append("SELECT nsp.oid, nsp.nspname, NULL AS schema , 'schema' AS type")
                        .append(" FROM pg_namespace nsp")
                        .append(" LEFT OUTER JOIN pg_description des ON nsp.oid = des.objoid ")
                        //.append(" WHERE nsp.nspname NOT IN( ")
                        //.append(" 'pg_toast', 'pg_catalog', 'information_schema','hgdb_catalog', 'oracle_catalog',")
                        //.append(" 'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog')")
                        //.append(" AND nsp.nspname NOT LIKE 'pg_temp_%' AND nsp.nspname NOT LIKE 'pg_toast_temp%'")
                        .append(" ORDER BY nsp.nspname;");
                break;
            //schema
            case "table":
                sql.append("SELECT c.oid, c.relname, n.nspname AS schema, 'table' AS type")
                        .append(" FROM pg_class c ")
                        .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                        .append(" LEFT JOIN pg_description des ON des.objoid = c.oid")
                        .append(" WHERE c.relkind = 'r'::\"char\"")
                        .append(" AND c.relname NOT IN('pg_audit_event', 'pg_audit_rule', 'pg_audit_trail_logs')")
                        //.append(" AND n.nspname NOT IN( 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                        .append(" ORDER BY n.nspname,c.relname;");
                break;
            case "table4label":
                sql.append("SELECT c.oid, c.relname, n.nspname AS schema, 'table' AS type")
                        .append(" FROM pg_class c ")
                        .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                        .append(" LEFT JOIN pg_description des ON des.objoid = c.oid")
                        .append(" WHERE c.relkind = 'r'::\"char\"")
                        .append(" AND c.relname NOT IN('pg_audit_event', 'pg_audit_rule', 'pg_audit_trail_logs')")
                        .append(" AND n.nspname NOT IN( 'pg_catalog',  'hgdb_catalog', 'oracle_catalog'")
                        .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')")
                        .append(" ORDER BY n.nspname,c.relname;");
                break;
            case "view":
                sql.append(" SELECT c.oid, c.relname, n.nspname  as schema, 'view' AS type")
                        .append(" FROM pg_class c ")
                        .append(" LEFT OUTER JOIN pg_description des ON (des.objoid=c.oid and des.objsubid=0)")
                        .append(" LEFT OUTER JOIN pg_namespace n ON (n.oid = c.relnamespace)")
                        .append(" WHERE ((c.relhasrules AND (EXISTS (SELECT r.rulename FROM pg_rewrite r ")
                        .append(" WHERE ((r.ev_class = c.oid) AND (bpchar(r.ev_type) = '1'::bpchar)) ))) ")
                        .append(" OR (c.relkind = 'v'::char))")
                        //.append(" AND n.nspname NOT IN ('pg_toast', 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog',")
                        //.append(" 'hgdb_oracle_catalog','hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog')")
                        //.append(" AND n.nspname NOT LIKE 'pg_temp_%' AND n.nspname NOT LIKE 'pg_toast_temp%'")
                        .append(" ORDER BY n.nspname, c.relname");
                break;
            case "sequence":
                sql.append("SELECT c.oid, c.relname, n.nspname AS schema, 'sequence' AS type")
                        .append(" FROM pg_class c")
                        .append(" LEFT OUTER JOIN pg_description des ON des.objoid=c.oid")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")
                        .append(" WHERE relkind = 'S'")
                        .append(" AND c.oid > (SELECT DISTINCT datlastsysoid FROM pg_database)")
                        //.append(" AND n.nspname NOT IN('pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                        .append(" ORDER BY n.nspname, c.relname;");
                break;
            case "function":
                sql.append("SELECT pr.oid,  pr.proname,  np.nspname AS schema, 'function' AS type")
                        .append(" FROM pg_proc pr")
                        .append(" JOIN pg_namespace np  ON np.oid=pr.pronamespace")
                        //.append(" WHERE np.nspname NOT IN( 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog'")
                        //.append(" 'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog')")
                        .append(" ORDER BY proname");
                break;
            //table
            case "index":
                sql.append("SELECT idx.indexrelid AS oid, cls.relname AS name, n.nspname as schema, 'index'  AS type")
                        .append(" FROM pg_index idx")
                        .append(" JOIN pg_class cls ON cls.oid=indexrelid")
                        .append(" LEFT OUTER JOIN  pg_namespace n ON n. oid = cls.relnamespace")
                        //.append(" LEFT OUTER JOIN pg_description des ON des.objoid=idx.indexrelid")
                        .append(" LEFT JOIN pg_depend dep ON (dep.classid = cls.tableoid AND dep.objid = cls.oid ")
                        .append(" AND dep.refobjsubid = '0' AND dep.refclassid=(SELECT oid FROM pg_class WHERE relname='pg_constraint'))")
                        .append(" LEFT OUTER JOIN pg_constraint con ON (con.tableoid = dep.refclassid AND con.oid = dep.refobjid)")
                        .append(" WHERE con.oid IS NULL ")
                        //.append(" AND n.nspname NOT IN('pg_toast', 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                        .append(" ORDER BY cls.relname;");
                break;
            case "rule":
                sql.append("SELECT rew.oid, rew.rulename, n.nspname AS schema, 'rule' AS type")
                        .append(" FROM pg_rewrite rew ")
                        //.append(" LEFT OUTER JOIN pg_description des ON rew.oid = des.objoid ")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid=rew.ev_class")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid =  cls.relnamespace")
                        //.append(" WHERE n.nspname NOT IN('pg_toast','pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog'")
                        //.append(" 'hgdb_oracle_catalog', 'hgdb_mssql_catalog')")
                        .append(" ORDER BY n.nspname, rew.rulename;");
                break;
            case "trigger":
                sql.append(" SELECT tri.oid, tri.tgname,  n.nspname as schema, 'trigger' as type")
                        .append(" FROM pg_trigger tri")
                        //.append(" LEFT OUTER JOIN pg_description des ON des.objoid = tri.oid")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = tri.tgrelid")
                        .append(" LEFT OUTER JOIN pg_namespace n ON n.oid =  cls.relnamespace")
                        .append(" WHERE tri.tgisinternal = false")
                        //.append(" AND n.nspname NOT IN('pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                        .append(" ORDER BY n.nspname, tri.tgname;");
                break;
            case "constraint":
                sql.append("SELECT DISTINCT con.oid,  con.conname,  np.nspname,  'constraint'  AS type, con.contype")
                        .append(" FROM pg_constraint con ")
                        .append(" LEFT OUTER JOIN pg_authid aut ON con.oid = aut.oid")
                        //.append(" LEFT OUTER JOIN pg_description des ON con.oid = des.objoid")
                        .append(" LEFT OUTER JOIN pg_namespace np ON con.connamespace = np.oid")
                        .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = con.conrelid")
                        //.append(" WHERE  np.nspname NOT IN('pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                        .append(" AND con.contype NOT IN('t')")
                        .append(" ORDER BY con.conname;");
                break;
            default:
                logger.error("There's no sql for this type.");
                break;
        }
        */
        if (type.equals("tablespace"))
        {
            sql.append(" SELECT spc.oid, spc.spcname, NULL AS schema, 'tablespace' AS type")
                    .append(" FROM pg_tablespace spc")
                    .append(" LEFT OUTER JOIN pg_shdescription des ON spc.oid = des.objoid")
                    .append(" ORDER BY spc.spcname");
        } else if (type.equals("user"))//login
        {
            sql.append("SELECT aut.oid, aut.rolname, NULL AS schema, 'user' AS type")
                    .append(" FROM pg_authid  aut")
                    .append(" LEFT OUTER JOIN pg_shdescription des ON aut.oid = des.objoid ")
                    .append(" WHERE aut.rolcanlogin = true")
                    //.append(" AND aut.rolname NOT IN('syssso')")
                    .append(" ORDER BY aut.rolname ");
        } else if (type.equals("database"))
        {
            sql.append("SELECT dat.oid, dat.datname, NULL AS schema, 'database' AS type")
                    .append(" FROM pg_database dat")
                    .append(" LEFT OUTER JOIN pg_shdescription des ON dat.oid = des.objoid")
                    .append(" WHERE dat.datname NOT IN('template1','template0') ")
                    .append(" ORDER BY dat.datname");
        } else if (type.equals("schema"))
        {
            sql.append("SELECT nsp.oid, nsp.nspname, NULL AS schema , 'schema' AS type")
                    .append(" FROM pg_namespace nsp")
                    .append(" LEFT OUTER JOIN pg_description des ON nsp.oid = des.objoid ")
                    //.append(" WHERE nsp.nspname NOT IN( ")
                    //.append(" 'pg_toast', 'pg_catalog', 'information_schema','hgdb_catalog', 'oracle_catalog',")
                    //.append(" 'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog')")
                    //.append(" AND nsp.nspname NOT LIKE 'pg_temp_%' AND nsp.nspname NOT LIKE 'pg_toast_temp%'")
                    .append(" ORDER BY nsp.nspname;");
        } else if (type.equals("table"))
        {
            sql.append("SELECT c.oid, c.relname, n.nspname AS schema, 'table' AS type")
                    .append(" FROM pg_class c ")
                    .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                    .append(" LEFT JOIN pg_description des ON des.objoid = c.oid")
                    .append(" WHERE c.relkind = 'r'::\"char\"")
                    .append(" AND c.relname NOT IN('pg_audit_event', 'pg_audit_rule', 'pg_audit_trail_logs')")
                    //.append(" AND n.nspname NOT IN( 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                    .append(" ORDER BY n.nspname,c.relname;");
        } else if (type.equals("table4label"))
        {
            sql.append("SELECT c.oid, c.relname, n.nspname AS schema, 'table' AS type")
                    .append(" FROM pg_class c ")
                    .append(" LEFT JOIN pg_namespace n ON n.oid = c.relnamespace ")
                    .append(" LEFT JOIN pg_description des ON des.objoid = c.oid")
                    .append(" WHERE c.relkind = 'r'::\"char\"")
                    .append(" AND c.relname NOT IN('pg_audit_event', 'pg_audit_rule', 'pg_audit_trail_logs')")
                    .append(" AND n.nspname NOT IN( 'pg_catalog',  'hgdb_catalog', 'oracle_catalog'")
                    .append(" , 'pg_toast', 'pg_temp_1', 'pg_toast_temp_1', 'information_schema', 'Trash')")
                    .append(" ORDER BY n.nspname,c.relname;");
        } else if (type.equals("view"))
        {
            sql.append(" SELECT c.oid, c.relname, n.nspname  as schema, 'view' AS type")
                    .append(" FROM pg_class c ")
                    .append(" LEFT OUTER JOIN pg_description des ON (des.objoid=c.oid and des.objsubid=0)")
                    .append(" LEFT OUTER JOIN pg_namespace n ON (n.oid = c.relnamespace)")
                    .append(" WHERE ((c.relhasrules AND (EXISTS (SELECT r.rulename FROM pg_rewrite r ")
                    .append(" WHERE ((r.ev_class = c.oid) AND (bpchar(r.ev_type) = '1'::bpchar)) ))) ")
                    .append(" OR (c.relkind = 'v'::char))")
                    //.append(" AND n.nspname NOT IN ('pg_toast', 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog',")
                    //.append(" 'hgdb_oracle_catalog','hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog')")
                    //.append(" AND n.nspname NOT LIKE 'pg_temp_%' AND n.nspname NOT LIKE 'pg_toast_temp%'")
                    .append(" ORDER BY n.nspname, c.relname");
        } else if (type.equals("sequence"))
        {
            sql.append("SELECT c.oid, c.relname, n.nspname AS schema, 'sequence' AS type")
                    .append(" FROM pg_class c")
                    .append(" LEFT OUTER JOIN pg_description des ON des.objoid=c.oid")
                    .append(" LEFT OUTER JOIN pg_namespace n ON n.oid = c.relnamespace")
                    .append(" WHERE relkind = 'S'")
                    .append(" AND c.oid > (SELECT DISTINCT datlastsysoid FROM pg_database)")
                    //.append(" AND n.nspname NOT IN('pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                    .append(" ORDER BY n.nspname, c.relname;");
        } else if (type.equals("function"))
        {
            sql.append("SELECT pr.oid,  pr.proname,  np.nspname AS schema, 'function' AS type")
                    .append(" FROM pg_proc pr")
                    .append(" JOIN pg_namespace np  ON np.oid=pr.pronamespace")
                    //.append(" WHERE np.nspname NOT IN( 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog'")
                    //.append(" 'hgdb_oracle_catalog', 'hgdb_mysql_catalog', 'hgdb_mssql_catalog', 'hgdb_db2_catalog')")
                    .append(" ORDER BY proname");
        } else if (type.equals("index"))
        {
            sql.append("SELECT idx.indexrelid AS oid, cls.relname AS name, n.nspname as schema, 'index'  AS type")
                    .append(" FROM pg_index idx")
                    .append(" JOIN pg_class cls ON cls.oid=indexrelid")
                    .append(" LEFT OUTER JOIN  pg_namespace n ON n. oid = cls.relnamespace")
                    //.append(" LEFT OUTER JOIN pg_description des ON des.objoid=idx.indexrelid")
                    .append(" LEFT JOIN pg_depend dep ON (dep.classid = cls.tableoid AND dep.objid = cls.oid ")
                    .append(" AND dep.refobjsubid = '0' AND dep.refclassid=(SELECT oid FROM pg_class WHERE relname='pg_constraint'))")
                    .append(" LEFT OUTER JOIN pg_constraint con ON (con.tableoid = dep.refclassid AND con.oid = dep.refobjid)")
                    .append(" WHERE con.oid IS NULL ")
                    //.append(" AND n.nspname NOT IN('pg_toast', 'pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                    .append(" ORDER BY cls.relname;");
        } else if (type.equals("rule"))
        {
            sql.append("SELECT rew.oid, rew.rulename, n.nspname AS schema, 'rule' AS type")
                    .append(" FROM pg_rewrite rew ")
                    //.append(" LEFT OUTER JOIN pg_description des ON rew.oid = des.objoid ")
                    .append(" LEFT OUTER JOIN pg_class cls ON cls.oid=rew.ev_class")
                    .append(" LEFT OUTER JOIN pg_namespace n ON n.oid =  cls.relnamespace")
                    //.append(" WHERE n.nspname NOT IN('pg_toast','pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog'")
                    //.append(" 'hgdb_oracle_catalog', 'hgdb_mssql_catalog')")
                    .append(" ORDER BY n.nspname, rew.rulename;");
        } else if (type.equals("trigger"))
        {
            sql.append(" SELECT tri.oid, tri.tgname,  n.nspname as schema, 'trigger' as type")
                    .append(" FROM pg_trigger tri")
                    //.append(" LEFT OUTER JOIN pg_description des ON des.objoid = tri.oid")
                    .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = tri.tgrelid")
                    .append(" LEFT OUTER JOIN pg_namespace n ON n.oid =  cls.relnamespace")
                    .append(" WHERE tri.tgisinternal = false")
                    //.append(" AND n.nspname NOT IN('pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                    .append(" ORDER BY n.nspname, tri.tgname;");
        } else if (type.equals("constraint"))
        {
            sql.append("SELECT DISTINCT con.oid,  con.conname,  np.nspname,  'constraint'  AS type, con.contype")
                    .append(" FROM pg_constraint con ")
                    .append(" LEFT OUTER JOIN pg_authid aut ON con.oid = aut.oid")
                    //.append(" LEFT OUTER JOIN pg_description des ON con.oid = des.objoid")
                    .append(" LEFT OUTER JOIN pg_namespace np ON con.connamespace = np.oid")
                    .append(" LEFT OUTER JOIN pg_class cls ON cls.oid = con.conrelid")
                    //.append(" WHERE  np.nspname NOT IN('pg_catalog', 'information_schema', 'hgdb_catalog', 'oracle_catalog')")
                    .append(" AND con.contype NOT IN('t')")
                    .append(" ORDER BY con.conname;");
        } else
        {
            logger.error("There's no sql for this type.");
        }
        logger.info("Return: sql=" + sql.toString());
        return sql.toString();
    }
    public List<ObjectInfoDTO> getObjectInfoList(HelperInfoDTO helperInfo, String type) throws Exception
    {
        logger.info("Enter:type=" + type);
        List<ObjectInfoDTO> objList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return an empty array.");
            return objList;
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = this.getObjectSQL(type);
        logger.info(sql);
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while(rs.next())
            {
                ObjectInfoDTO obj = new ObjectInfoDTO();
                obj.setOid(rs.getString(1));
                obj.setName(rs.getString(2));
                obj.setSchema(rs.getString(3));
                obj.setType(rs.getString(4));
                objList.add(obj);
            }
        } catch (SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        return objList;
    }
    public String getTypeById(int id)
    {
        switch (id)
        {
            case 0:
                return "none";
            case 1:
                return "user";
            case 2:
                return "tablespace";
            case 3:
                return "database";
            case 4:
                return "schema";
            case 5:
                return "sequence";
            case 6:
                return "view";
            case 7:
                return "table";
            case 8:
                return "index";
            case 9:
                return "rule";
            case 10:
                return "trigger";
            case 11:
                return "function";
            case 12:
                return "constraint";
            default:
                logger.error(id + " is an exception type id, return null.");
                return null;
        }
    }
    public String getOperationById(int id)
    {
        switch (id)
        {
            case 1:
                return "alter";
            case 2:
                return "drop";
            case 3:
                return "revoke";
            case 4:
                return "grant";
            case 5:
                return "select";
            case 6:
                return "insert";
            case 7:
                return "update";
            case 8:
                return "delete";
            case 9:
                return "truncate";
            case 10:
                return "lock";
            case 11:
                return "cluster";
            case 12:
                return "copy";
            case 13:
                return "reindex";
            case 14:
                return "create role";
            case 15:
                return "create database";
            case 16:
                return "create tablesapce";
            case 17:
                return "create schema";
            case 18:
                return "create sequence";
            case 19:
                return "create view";
            case 20:
                return "create table";
            case 21:
                return "create index";
            case 22:
                return "create rule";
            case 23:
                return "create trigger";
            case 24:
                return "create function";
            case 25:
                return "set constraints";
            default:
                logger.error(id + " is an exception id, return null.");
                return null;
        }
    }    
     public String getCNOperationById(int id)
    {
        switch (id)
        {
            case 0:
                return constBundle.getString("sysOperation");
            case 1:
                return constBundle.getString("alter");
            case 2:
                return constBundle.getString("drop");
            case 3:
                return constBundle.getString("revoke");
            case 4:
                return constBundle.getString("grant");
            case 5:
                return constBundle.getString("select");
            case 6:
                return constBundle.getString("insert");
            case 7:
                return constBundle.getString("update");
            case 8:
                return constBundle.getString("delete");
            case 9:
                return constBundle.getString("truncate");
            case 10:
                return constBundle.getString("lock");
            case 11:
                return constBundle.getString("cluster");
            case 12:
                return constBundle.getString("copy");
            case 13:
                return constBundle.getString("reindex");
            case 14:
                return constBundle.getString("createRole");
            case 15:
                return constBundle.getString("createDatabase");
            case 16:
                return constBundle.getString("createTablesapce");
            case 17:
                return constBundle.getString("createSchema");
            case 18:
                return constBundle.getString("createSequence");
            case 19:
                return constBundle.getString("createView");
            case 20:
                return constBundle.getString("createTable");
            case 21:
                return constBundle.getString("createIndex");
            case 22:
                return constBundle.getString("createRule");
            case 23:
                return constBundle.getString("createTrigger");
            case 24:
                return constBundle.getString("createFunction");
            case 25:
                return constBundle.getString("setConstraints");
            case 26:
                return constBundle.getString("login");
            default:
                logger.error(id + " is an exception id, return null.");
                return null;
        }
    }    
    
    
    //for illegal event 
    public String getActionById(int id)
    {
        logger.debug("*********id=" + id);
        switch (id)
        {
            case 1:
                return constBundle.getString("log");
            case 2:
                return constBundle.getString("warn");
            case 3:
                return constBundle.getString("disconnect");
            case 4:
                return constBundle.getString("lock");
            default:
                logger.error(id + " is an exception action id, return null.");
                return null;
        }
    }
    public int getIdByAction(String action)
    {
        if (action.equals(constBundle.getString("log")))
        {
            return 1;
        } else if (action.equals(constBundle.getString("warn")))
        {
            return 2;
        } else if (action.equals(constBundle.getString("disconnect")))
        {
            return 3;
        } else if (action.equals(constBundle.getString("lock")))
        {
            return 4;
        }else
        {
            logger.error(action+" is an exception action name, return 0.");
            return 0;
        }
    }
    private String getEventById(int id)
    {
        switch (id)
        {
            case 1:
                return constBundle.getString("illegalDataAccess");
            case 2:
                return constBundle.getString("illegalDataModify");
            case 3:
                return constBundle.getString("illegalDefinationModify");
            case 4:
                return constBundle.getString("dataDelete");
            case 5:
                return constBundle.getString("dataModify");
            case 6:
                return constBundle.getString("objectDrop");
            case 7:
                return constBundle.getString("objectNotExist");
            case 8:
                return constBundle.getString("wrongPwdLoginFailure");
            default:
                logger.error(id + " is an exception action id, return null.");
                return null;
        }
    }   
    
    public String[] getActionByEventId(int selectEvent)
    {
        String[] actions;
        switch (selectEvent)
        {
            case 1://"illegal_data_access"
            case 2://"illegal_data_modify"
            case 3://"illegal_defination_modify"
                actions = new String[]
                {
                    constBundle.getString("log")
                };
                break;
            case 4:
            case 5:
            case 6:
            case 7:
                actions = new String[]
                {
                    constBundle.getString("log"),
                    constBundle.getString("warn"),
                    constBundle.getString("disconnect")
                };
                break;
            case 8://wrong_pwd_login_failure
                actions = new String[]
                {
                    constBundle.getString("log"),
                    constBundle.getString("warn"),
                    constBundle.getString("lock")
                };
                break;
            default:
                actions = new String[0];
                logger.error(selectEvent + " is an exception illegal event id");
                break;
        }
        return actions;                 
    }
    public List<ObjectDTO> getObjTypeArrayByEventId(int selectEvent)
    {
        List<ObjectDTO> types = new ArrayList();
        logger.info("event=" + selectEvent);
        switch (selectEvent)
        {
            case 1://"illegal_data_access"
                types.add(this.getTypeObject(6));//table
                types.add(this.getTypeObject(7));//view
                break;
            case 2://"illegal_data_modify"
            case 4://"data_delete"
            case 5://"data_modify"
                types.add(this.getTypeObject(7));//table
                break;
            case 8://"wrong_pwd_login_failure"
                types.add(this.getTypeObject(1));//user
                break;
            case 6://"object_drop"
            case 3://"illegal_defination_modify"
                types.add(this.getTypeObject(1));
                types.add(this.getTypeObject(2));
                types.add(this.getTypeObject(3));
                types.add(this.getTypeObject(4));
                types.add(this.getTypeObject(5));
                types.add(this.getTypeObject(7));
                types.add(this.getTypeObject(8));
                types.add(this.getTypeObject(11));
                break;
            case 7://"object_not_exist"
                break;
            default:
                logger.error(selectEvent + " is an exception illegal event id");
                break;
         }
         return types;
    }
    private ObjectDTO getTypeObject(int id)
    {
        ObjectDTO obj = new ObjectDTO();
        obj.setId(id);
        obj.setValue(this.getTypeById(id));
        return obj;
    }
    public List<ObjectDTO> getOperationArrayByEventId(int selectedEvent)
    {
        List<ObjectDTO> operations = new ArrayList();
        logger.info("event=" + selectedEvent);
        switch (selectedEvent)
        {
            case 1://"illegal_data_access"
                operations.add(this.getOperationObject(5));//select
                break;
            case 2://"illegal_data_modify"
                operations.add(this.getOperationObject(6));//insert
                operations.add(this.getOperationObject(7));//update
                operations.add(this.getOperationObject(8));//delete
                operations.add(this.getOperationObject(9));//truncate
                break;
            case 3://"illegal_defination_modify"
                operations.add(this.getOperationObject(1));//alter
                break;
            case 4://"data_delete"
                operations.add(this.getOperationObject(8));//delete
                operations.add(this.getOperationObject(9));//truncate
                break;
            case 5://"data_modify"
                operations.add(this.getOperationObject(6));//insert
                operations.add(this.getOperationObject(7));//update
                break;
            case 6://"object_drop"
                operations.add(this.getOperationObject(2));//drop
                 break;
             case 7://"object_not_exist"                 
                 operations.add(this.getOperationObject(1));
                 operations.add(this.getOperationObject(2));
                 operations.add(this.getOperationObject(3));
                 operations.add(this.getOperationObject(4));
                 operations.add(this.getOperationObject(5));
                 operations.add(this.getOperationObject(6));
                 operations.add(this.getOperationObject(7));
                 operations.add(this.getOperationObject(8));
                 operations.add(this.getOperationObject(9));
                 operations.add(this.getOperationObject(10));
                 operations.add(this.getOperationObject(11));
                 operations.add(this.getOperationObject(12));
                 operations.add(this.getOperationObject(13));                 
                 break;
             case 8://"wrong_pwd_login_failure"
                 break;
             default:
                logger.error(selectedEvent+" is an exception illegal event id");
                break;
        }
        return operations;
    }
    private ObjectDTO getOperationObject(int id)
    {
        ObjectDTO obj = new ObjectDTO();
        obj.setId(id);
        obj.setValue(this.getOperationById(id));
        return obj;
    }
    
    public void queryIllegalEvents(HelperInfoDTO helperInfo, int event, int action, int objType, DefaultTableModel eventModel) throws Exception
    {
        //List<IllegalEventInfoDTO> eventList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;// null;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_id, event_count, object_type_id, object_id,")
                .append(" object_name, operation_id, action_id, description")
                .append(" FROM pg_catalog.pg_audit_event ");
        if (event > 0 || action > 0 || objType > 0)
        {
            sql.append(" WHERE ");
            if (event > 0)
            {
                sql.append(" event_id = ").append(event).append("");
            }
            if (action > 0)
            {
                if (event > 0)
                {
                    sql.append(" AND ");
                }
                sql.append(" action_id = ").append(action).append("");
            }
            if (objType > 0)
            {
                if (event > 0 || action > 0)
                {
                    sql.append(" AND ");
                }
                sql.append(" object_type_id = ").append(objType).append("");
            }
        }
        logger.info(sql.toString());
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);                   
            stmt = conn.createStatement();
            stmt.setFetchSize(100);
            logger.info("fetchSize="+stmt.getFetchSize());
            rs = stmt.executeQuery(sql.toString());
            IllegalEventInfoDTO eventInfo;
            Object[] rowData;
            while (rs.next())
            {
                eventInfo = new IllegalEventInfoDTO();
                eventInfo.setOid(rs.getString(1));
                eventInfo.setEventId(rs.getInt(2));
                eventInfo.setEventName(this.getEventById(eventInfo.getEventId()));
                eventInfo.setCount(rs.getString(3));
                eventInfo.setObjectTypeId(rs.getInt(4));
                eventInfo.setObjectType(this.getTypeById(eventInfo.getObjectTypeId()));
                eventInfo.setObjectOid(rs.getString(5));
                eventInfo.setObjectName(rs.getString(6));
                eventInfo.setOperationId(rs.getInt(7));
                eventInfo.setOperation(this.getOperationById(eventInfo.getOperationId()));
                eventInfo.setActionId(rs.getInt(8));
                eventInfo.setAction(this.getActionById(eventInfo.getActionId()));
                eventInfo.setDescription(rs.getString(9));
                //eventList.add(eventInfo);

                rowData = new Object[10];
                rowData[0] = eventInfo;
                rowData[1] = eventInfo.getEventName();
                rowData[2] = eventInfo.getObjectType();
                rowData[3] = eventInfo.getObjectOid();
                rowData[4] = eventInfo.getObjectName();
                rowData[5] = eventInfo.getOperation();
                rowData[6] = eventInfo.getCount();
                rowData[7] = eventInfo.getAction();
                rowData[8] = eventInfo.getDescription();
                rowData[9] = false;
                eventModel.addRow(rowData);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close stream");
        }
        //logger.info("Return:row="+eventList.size());
        //return eventList;
    }
    public void batchDeleteIllegalEvent(HelperInfoDTO helperInfo, List<Long> oidList) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        StringBuilder sql = new StringBuilder("");
        sql.append("DELETE FROM  pg_catalog.pg_audit_event")
                .append(" WHERE oid=?");
        logger.info("sql="+sql.toString());
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            //stmt = conn.prepareStatement("LOCK TABLE pg_catalog.pg_audit_event in EXCLUSIVE mode;");
            //stmt.execute();             
            stmt = conn.prepareStatement(sql.toString());
            for (Long oid : oidList)
            {
                logger.info("oid=" + oid);
                stmt.setLong(1, oid);
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);            
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }
    public void bacthUpdateIllegalEvent(HelperInfoDTO helperInfo, List<IllegalEventInfoDTO> illegalEventList) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        StringBuilder sql = new StringBuilder("");
        sql.append("UPDATE pg_catalog.pg_audit_event")
                .append(" SET action_id=?, description=?")
                .append(" WHERE oid=?");
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            //stmt = conn.prepareStatement("LOCK TABLE pg_catalog.pg_audit_event in EXCLUSIVE mode;");
            //stmt.execute();
            stmt = conn.prepareStatement(sql.toString());
            for (IllegalEventInfoDTO eventInfo : illegalEventList)
            {
                logger.info("oid=" + eventInfo.getOid() + ",actionId=" + eventInfo.getActionId()
                        + ",description=" + eventInfo.getDescription());
                stmt.setInt(1, eventInfo.getActionId());
                stmt.setString(2, eventInfo.getDescription());
                stmt.setLong(3, Long.valueOf(eventInfo.getOid()));
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }  
    public void batchInsertIllegalEvent(HelperInfoDTO helperInfo, List<IllegalEventInfoDTO> illegalEventList) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO pg_catalog.pg_audit_event( event_id, event_count, object_type_id,")
                .append("object_id, object_name, operation_id, action_id, description)")
                .append(" VALUES ( ?, ?, ?, ?, ?,  ?, ?, ?);");
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            //stmt = conn.prepareStatement("LOCK TABLE pg_catalog.pg_audit_event in EXCLUSIVE mode;");
            //stmt.execute();
            stmt = conn.prepareStatement(sql.toString());
            for (IllegalEventInfoDTO event : illegalEventList)
            {
                stmt.setInt(1, event.getEventId());
                if (event.getCount() == null)
                {
                    stmt.setInt(2,0 );//Types.INTEGER
                } else
                {
                    stmt.setInt(2, Integer.valueOf(event.getCount()));
                }
                stmt.setInt(3, event.getObjectTypeId());
                if (event.getObjectOid() == null || event.getObjectOid().isEmpty())
                {
                    stmt.setLong(4, 0);
                } else
                {
                    stmt.setLong(4, Long.valueOf(event.getObjectOid()));                   
                }
                stmt.setString(5, event.getObjectName());
                stmt.setInt(6, event.getOperationId());
                stmt.setInt(7, event.getActionId());
                stmt.setString(8, event.getDescription());
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }

    
    //for audit rule 
    public String getResultById(int id)
    {
        switch (id)
        {
            case 0:
                return "failed";
            case 1:
                return "success";
            case 2:
                return "all";
            default:
                logger.error(id + " is an exception result id, return null.");
                return null;
        }
    }
    public int getIdByResult(String result)
    {
        /*
        switch (result)
        {
            case "failed":
                return 0;
            case "success":
                return 1;
            case "all":
                return 2;
            default:
                logger.error(result + " is an exception result.");
                return -1;
        }
       */
        if (result.equals("failed"))
        {
            return 0;
        } else if (result.equals("success"))
        {
            return 1;
        } else if (result.equals("all"))
        {
            return 2;
        } else
        {
            logger.error(result + " is an exception result.");
            return -1;
        }
    }
      
    public List<ObjectDTO> getOperationArrayByObjectType(String type)
    {
        List<ObjectDTO> operations = new ArrayList();
        /*
        switch (type)
        {
            case "user":
//                    "alter", "drop", "revoke", "grant",
//                    "create role", "create database", "create tablespace", "create schema",
//                    "create sequence", "create view", "create table", "create index",
//                    "create rule", "create trigger", "create function"
                operations.add(this.getOperationObject(1));
                operations.add(this.getOperationObject(2));
                operations.add(this.getOperationObject(3));
                operations.add(this.getOperationObject(4));
//                operations.add(this.getOperationObject(14));
//                operations.add(this.getOperationObject(15));
//                operations.add(this.getOperationObject(16));
//                operations.add(this.getOperationObject(17));
//                operations.add(this.getOperationObject(18));
//                operations.add(this.getOperationObject(19));
//                operations.add(this.getOperationObject(20));
//                operations.add(this.getOperationObject(21));
//                operations.add(this.getOperationObject(22));
//                operations.add(this.getOperationObject(23));
//                operations.add(this.getOperationObject(24));
                break;
            case "tablespace":
            case "database":
            case "schema":
            case "sequence":
            case "function":
            case "trigger":
                operations.add(this.getOperationObject(1));//alter
                operations.add(this.getOperationObject(2));//drop
                break;
            case "view":
                operations.add(this.getOperationObject(1));//alter
                operations.add(this.getOperationObject(2));//drop
                operations.add(this.getOperationObject(5));//select          
                break;
            case "table":
//                    "alter", "drop", "select", "insert", "delete", "truncate",
//                    "update", "lock", "cluster", "copy"
                operations.add(this.getOperationObject(1));
                operations.add(this.getOperationObject(2));
                operations.add(this.getOperationObject(5));
                operations.add(this.getOperationObject(6));
                operations.add(this.getOperationObject(7));
                operations.add(this.getOperationObject(8));
                operations.add(this.getOperationObject(9));
                operations.add(this.getOperationObject(10));
                operations.add(this.getOperationObject(11));
                operations.add(this.getOperationObject(12));
                break;
            case "index":
                operations.add(this.getOperationObject(1));//alter
                operations.add(this.getOperationObject(2));//drop
                operations.add(this.getOperationObject(13));//reindex
                break;
            case "rule":
                operations.add(this.getOperationObject(2));//drop
                break;
            case "constraint":
                operations.add(this.getOperationObject(25));//set constraints
                break;
            case "none":
                operations.add(this.getOperationObject(14));
                operations.add(this.getOperationObject(15));
                operations.add(this.getOperationObject(16));
                operations.add(this.getOperationObject(17));
                operations.add(this.getOperationObject(18));
                operations.add(this.getOperationObject(19));
                operations.add(this.getOperationObject(20));
                operations.add(this.getOperationObject(21));
                operations.add(this.getOperationObject(22));
                operations.add(this.getOperationObject(23));
                operations.add(this.getOperationObject(24));
                break;
            default:
                logger.error(type + " is an exception object type.");
                break;
        }
        */
            if (type.equals("user"))
        {
//          "alter", "drop", "revoke", "grant",
//          "create role", "create database", "create tablespace", "create schema",
//          "create sequence", "create view", "create table", "create index",
//          "create rule", "create trigger", "create function"
            operations.add(this.getOperationObject(1));
            operations.add(this.getOperationObject(2));
            operations.add(this.getOperationObject(3));
            operations.add(this.getOperationObject(4));
//          operations.add(this.getOperationObject(14));
//          operations.add(this.getOperationObject(15));
//          operations.add(this.getOperationObject(16));
//          operations.add(this.getOperationObject(17));
//          operations.add(this.getOperationObject(18));
//          operations.add(this.getOperationObject(19));
//          operations.add(this.getOperationObject(20));
//          operations.add(this.getOperationObject(21));
//          operations.add(this.getOperationObject(22));
//          operations.add(this.getOperationObject(23));
//          operations.add(this.getOperationObject(24));
        } else if (type.equals("tablespace")
                || type.equals("database")
                || type.equals("schema")
                || type.equals("sequence")
                || type.equals("function")
                || type.equals("trigger"))
        {
            operations.add(this.getOperationObject(1));//alter
            operations.add(this.getOperationObject(2));//drop
        } else if (type.equals("view"))
        {
            operations.add(this.getOperationObject(1));//alter
            operations.add(this.getOperationObject(2));//drop
            operations.add(this.getOperationObject(5));//select          
        } else if (type.equals("table"))
        {
//                    "alter", "drop", "select", "insert", "delete", "truncate",
//                    "update", "lock", "cluster", "copy"
            operations.add(this.getOperationObject(1));
            operations.add(this.getOperationObject(2));
            operations.add(this.getOperationObject(5));
            operations.add(this.getOperationObject(6));
            operations.add(this.getOperationObject(7));
            operations.add(this.getOperationObject(8));
            operations.add(this.getOperationObject(9));
            operations.add(this.getOperationObject(10));
            operations.add(this.getOperationObject(11));
            operations.add(this.getOperationObject(12));
        } else if (type.equals("index"))
        {
            operations.add(this.getOperationObject(1));//alter
            operations.add(this.getOperationObject(2));//drop
            operations.add(this.getOperationObject(13));//reindex
        } else if (type.equals("rule"))
        {
            operations.add(this.getOperationObject(2));//drop
        } else if (type.equals("constraint"))
        {
            operations.add(this.getOperationObject(25));//set constraints
        } else if (type.equals("none"))
        {
            operations.add(this.getOperationObject(14));
            operations.add(this.getOperationObject(15));
            operations.add(this.getOperationObject(16));
            operations.add(this.getOperationObject(17));
            operations.add(this.getOperationObject(18));
            operations.add(this.getOperationObject(19));
            operations.add(this.getOperationObject(20));
            operations.add(this.getOperationObject(21));
            operations.add(this.getOperationObject(22));
            operations.add(this.getOperationObject(23));
            operations.add(this.getOperationObject(24));
        } else
        {
            logger.error(type + " is an exception object type.");
        }
        return operations;
    }

    public void queryAuditRules(HelperInfoDTO helperInfo, int typeId, String userId, int operationId, int resultId, DefaultTableModel ruleModel) throws Exception
    {
        //List<AuditRuleInfoDTO> ruleList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return; // ruleList;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT rul.oid, rul.object_type_id, rul.object_id, rul.object_name, rul.user_id, aut.rolname, ")
                .append(" rul.operation_id, rul.result_id, rul.rule_enable_timestamp ")
                .append(" FROM  pg_audit_rule rul")
                .append(" LEFT OUTER JOIN pg_authid aut ON aut.oid = rul.user_id");
        if (typeId >= 0 || !userId.isEmpty() || operationId > 0 || resultId > 0)
        {
            sql.append(" WHERE ");
            if (typeId >= 0)
            {
                sql.append(" rul.object_type_id = ").append(typeId).append("");
            }
            if (!userId.isEmpty())
            {
                if (typeId >= 0)
                {
                    sql.append(" AND ");
                }
                sql.append(" rul.user_id = ").append(userId).append("");
            }
            if (operationId > 0)
            {
                if (typeId >= 0 || !userId.isEmpty())
                {
                    sql.append(" AND ");
                }
                sql.append(" rul.operation_id = ").append(operationId).append("");
            }
            if (resultId > 0)
            {
                if (typeId >= 0 || !userId.isEmpty() || operationId > 0)
                {
                    sql.append(" AND ");
                }
                sql.append(" rul.result_id = '").append(resultId).append("'");
            }
        }
        sql.append(" ORDER BY rul.oid;");
        logger.info(sql.toString());
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            stmt.setFetchSize(100);
            logger.info("fetchSize=" + stmt.getFetchSize());
            rs = stmt.executeQuery(sql.toString());
            AuditRuleInfoDTO rule;
            Object[] rowData;
            while (rs.next())
            {
                rule = new AuditRuleInfoDTO();
                rule.setOid(rs.getString(1));
                rule.setObjectTypeId(rs.getInt(2));
                rule.setObjectType(this.getTypeById(rule.getObjectTypeId()));
                rule.setObjectId(rs.getString(3));
                rule.setObjectName(rs.getString(4));
                rule.setUserId(rs.getString(5));
                rule.setUser(rs.getString(6));
                rule.setOperationId(rs.getInt(7));
                rule.setOperation(this.getOperationById(rule.getOperationId()));
                rule.setResultId(rs.getInt(8));
                rule.setResult(this.getResultById(rule.getResultId()));
                rule.setEnableTime(rs.getString(9));
                //ruleList.add(rule);
                
                rowData = new Object[8];
                rowData[0] = rule;
                rowData[1] = rule.getObjectType();
                rowData[2] = rule.getObjectName();
                rowData[3] = rule.getUser();
                rowData[4] = rule.getOperation();
                rowData[5] = rule.getResult();
                rowData[6] = rule.getEnableTime();
                rowData[7] = (rule.getOid() == null);
                ruleModel.addRow(rowData);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        //logger.info("Return:row="+ruleList.size());
        //return ruleList;
    }
    
    public boolean isUserExistNow(HelperInfoDTO helperInfo, String userId) throws Exception
    {
        logger.info("Enter:userId=" + userId);
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return false.");
            return false;
        }
        boolean isExist = false;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            String checkSQL = "SELECT aut.oid FROM pg_authid aut WHERE aut.rolcanlogin = true  AND aut.oid=" + userId;
            stmt = conn.prepareStatement(checkSQL);
            rs = stmt.executeQuery();
            if (rs.next())
            {
                isExist = true;
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        return isExist;
    }
    
    public void batchInsertAuditRule(HelperInfoDTO helperInfo, List<AuditRuleInfoDTO> ruleList) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO pg_catalog.pg_audit_rule(object_type_id, object_id, object_name, ")
                .append(" user_id, operation_id, result_id, rule_enable_timestamp)")
                .append(" VALUES ( ?, ?, ?, ?, ?, ?, now());");
        logger.info(sql.toString());
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            //stmt = conn.prepareStatement("LOCK TABLE pg_catalog.pg_audit_rule in EXCLUSIVE mode;");
            //stmt.execute();     
            stmt = conn.prepareStatement(sql.toString());
            for (AuditRuleInfoDTO rule : ruleList)
            {
                stmt.setInt(1, rule.getObjectTypeId());
                stmt.setLong(2, Long.valueOf(rule.getObjectId()));
                stmt.setString(3, rule.getObjectName());
                if (rule.getUserId() == null)
                {
                    stmt.setInt(4, 0);//java.sql.Types.BIGINT
                } else
                {
                    stmt.setLong(4, Long.valueOf(rule.getUserId()));
                }
                stmt.setInt(5, rule.getOperationId());
                stmt.setInt(6, rule.getResultId());
                stmt.addBatch();
            }            
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }

    public void batchUpdateAuditRule(HelperInfoDTO helperInfo, List<AuditRuleInfoDTO> ruleList) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE pg_catalog.pg_audit_rule")
                .append(" SET result_id =?")
                .append(" ,rule_enable_timestamp = now()")
                .append(" WHERE oid=?");
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            //stmt = conn.prepareStatement("LOCK TABLE pg_catalog.pg_audit_rule in EXCLUSIVE mode;");
            //stmt.execute();           
            stmt = conn.prepareStatement(sql.toString());
            for (AuditRuleInfoDTO ruleInfo : ruleList)
            {
                logger.info("oid=" + ruleInfo.getOid() + ",resultId=" + ruleInfo.getResultId());
                stmt.setInt(1, ruleInfo.getResultId());
                stmt.setLong(2, Long.valueOf(ruleInfo.getOid()));
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);            
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }

    public void batchDeleteAuditRule(HelperInfoDTO helperInfo, List<Long> oidList) throws Exception
    {
        logger.info("Enter: oid="+oidList.size());
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        StringBuilder sql = new StringBuilder("");
        sql.append("DELETE FROM  pg_catalog.pg_audit_rule")
                .append(" WHERE oid=?");
        logger.info("sql=" + sql.toString());
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            //stmt = conn.prepareStatement("LOCK TABLE pg_catalog.pg_audit_rule in EXCLUSIVE mode;");
            //stmt.execute();            
            stmt = conn.prepareStatement(sql.toString());
            for (Long oid : oidList)
            {
                stmt.setLong(1, oid);
                stmt.addBatch();
            }
            stmt.executeBatch();
            conn.commit();
        } catch (Exception ex)
        {
            JdbcHelper.rollBack(conn);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bex = (BatchUpdateException) ex;
                logger.error(bex.getMessage());
                bex.printStackTrace(System.out);
                logger.error(bex.getNextException().getMessage());
                bex.getNextException().printStackTrace(System.out);
                throw new Exception(bex.getNextException().getMessage());
            } else
            {
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                throw new Exception(ex);
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.autoCommit(conn);            
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
    }
     
    
    //for log 
    /*
    第一次修改
       审计员：能看到所日志
       安全员：只能看到审计日志     
    */
    
    /*
    审计用户syssao本身oid或者角色的oid为12, 安全用户syssso本身或者角色oid为13, 系统管理员sysdba(usually highgo)的oid为10，
    其他为普通用户。
        
    第二次修改2016.3.25
        审计员：生成用户身份标识符、配置审计策略、可查看 审计员 和 安全员 的操作行为日志；
           .append(" WHERE (r.oid IN(12,13) OR m.roleid IN (12,13))")//add
    
        安全员：生成用户身份标识符、用户权限设定、配置安全策略、可查看 普通用户（不包括系统管理员） 和 审计员 的操作行为日志
           .append(" WHERE (r.oid NOT IN(10,13) AND (m.roleid IS NULL OR m.roleid!=13))")//add
    */
    
    /*
    第三次修改2016.3.30
         审计员：管理员，安全员的操作
            .append(" WHERE ( r.oid IN(10,13) OR m.roleid=13 )")
         安全员：普通用户，审计员的操作
            .append(" WHERE (r.oid NOT IN(10,13) AND (m.roleid IS NULL OR m.roleid!=13))")
    */
    
    /*
    第四次修改2016.12.22
    将操作类型改为中文
    */
    
    /*
    第五次修改2017.7.7 日志查看和刷新
    审计员：管理员，安全员的操作，普通用户，
           .append(" WHERE ( r.oid!=12 )")
    安全员：审计员的操作
           .append(" WHERE (r.oid IN(12) )")
     */
    public void queryLogByCondition(HelperInfoDTO helperInfo, String startTime, String endTime,String user,  DefaultTableModel logModel) throws Exception
    {
        ///List<LogInfoDTO> logList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;// logList;
        }
        logger.info("startTime:" + startTime + ",endTime:" + endTime);
        StringBuilder sql = new StringBuilder();
        logger.info("isAuditUser="+helperInfo.isAuditUser());
        if(helperInfo.isAuditUser()) //能看到安全员，管理员，普通用户的操作
        {
            /*
            sql.append("SELECT log_time, user_name , database_name, error_severity, sql_state_code,")//command_tag,
                    .append("  message, query, application_name, connection_from, session_start_time, process_id, xid, subxid, audit_oid, event_oid,event_severity")//1,2,3,4
                    .append(" FROM pg_catalog.pg_audit_trail_logs ")
                    .append(" LEFT OUTER JOIN pg_roles r ON r.rolname = user_name")//add
                    .append(" LEFT OUTER JOIN pg_auth_members m ON m.member = r.oid")//add
                    .append(" WHERE ( r.oid IN(10,13) OR m.roleid=13 )")//add
                    .append(" AND log_time > '").append(startTime).append("'::Timestamptz")
                    .append(" AND log_time < '").append(endTime).append("'::Timestamptz")
                    .append(" ORDER BY log_time asc");
            */
            sql.append("SELECT log_time, user_name , database_name, error_severity, sql_state_code,")//command_tag,
                    .append("  message, query, application_name, connection_from, session_start_time, process_id, xid, subxid,audit_oid, event_oid, operationid, event_severity")//1,2,3,4
                    .append(" FROM pg_catalog.pg_audit_trail_logs ")
                    .append(" LEFT OUTER JOIN pg_roles r ON r.rolname = user_name")//add
                    .append(" LEFT OUTER JOIN pg_auth_members m ON m.member = r.oid")//add
                    //.append(" WHERE ( r.oid IN(10,13) OR m.roleid=13  )")//add
                    .append(" WHERE ( r.oid!=12 )");
            if (user != null && !user.isEmpty())
            {
                sql.append(" AND user_name='").append(user).append("'");
            }
            sql.append(" AND log_time > '").append(startTime).append("'::Timestamptz")
                    .append(" AND log_time < '").append(endTime).append("'::Timestamptz")
                    .append(" ORDER BY log_time asc");
        } else//能看到审计员
        {
            /*
            sql.append("SELECT log_time, user_name , database_name, error_severity, sql_state_code,")//command_tag,
                    .append("  message, query, application_name, connection_from, session_start_time,process_id, xid, subxid, audit_oid, event_oid,event_severity")//1,2,3,4
                    .append(" FROM pg_catalog.pg_audit_trail_logs ")
                    .append(" LEFT OUTER JOIN pg_roles r ON r.rolname = user_name")//add
                    .append(" LEFT OUTER JOIN pg_auth_members m ON m.member = r.oid")//add
                    .append(" WHERE (r.oid NOT IN(10,13) AND (m.roleid IS NULL OR m.roleid!=13))")//add
                    .append(" AND log_time > '").append(startTime).append("'::Timestamptz")
                    .append(" AND log_time < '").append(endTime).append("'::Timestamptz")
                    .append(" ORDER BY log_time asc");
            */
             sql.append("SELECT log_time, user_name , database_name, error_severity, sql_state_code,")//command_tag,
                    .append("  message, query, application_name, connection_from, session_start_time,process_id, xid, subxid, audit_oid, event_oid,operationid, event_severity")//1,2,3,4
                    .append(" FROM pg_catalog.pg_audit_trail_logs ")
                    .append(" LEFT OUTER JOIN pg_roles r ON r.rolname = user_name")//add
                    .append(" LEFT OUTER JOIN pg_auth_members m ON m.member = r.oid")//add
                    //.append(" WHERE (r.oid NOT IN(10,13) AND (m.roleid IS NULL OR m.roleid!=13))")//add
                    .append(" WHERE (r.oid IN(12) )");
            if (user != null && !user.isEmpty())
            {
                sql.append(" AND user_name='").append(user).append("'");
            }
            sql.append(" AND log_time > '").append(startTime).append("'::Timestamptz")
                    .append(" AND log_time < '").append(endTime).append("'::Timestamptz")
                    .append(" ORDER BY log_time asc");
        }
        logger.info(sql.toString());
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            conn.setAutoCommit(false);
            stmt = conn.prepareStatement(sql.toString());
            stmt.setFetchSize(100);
            logger.info("fetchSize="+stmt.getFetchSize());
            rs = stmt.executeQuery();
            LogInfoDTO log;
            Object[] row;
            while(rs.next())
            {
                log = new LogInfoDTO();
                log.setLogTime(rs.getTimestamp(1));
                log.setUser(rs.getString(2));
                log.setDatabase(rs.getString(3));
                log.setErrorSeverity(rs.getString(4));
                log.setSqlStateCode(rs.getString(5));  
                log.setOperationResult(this.getStatus(log.getSqlStateCode()));//add
                //log.setCommandTag(rs.getString(6));
                log.setMsg(rs.getString(6));
                log.setQuery(rs.getString(7));
                log.setApplicationName(rs.getString(8));
                log.setConnectionFrom(rs.getString(9));
                log.setSessionStartTimestamp(rs.getTimestamp(10));
                log.setProcessId(rs.getInt(11));
                log.setTransactionId(rs.getInt(12));
                log.setSubTransactionId(rs.getInt(13));
                log.setAuditId(rs.getLong(14));
                log.setIllegalEventId(rs.getLong(15));
                log.setOperationId(rs.getInt(16));//add                
                log.setEventSeverity(rs.getInt(17));
                log.setOperationType(this.getCNOperationById(log.getOperationId())); //this.getLogOperationType(log.getAuditId(), log.getIllegalEventId(), conn, stmt, rs));//add
                //logList.add(log);
                
                row = new Object[9];
                row[0] = log;
                row[1] = log.getUser();
                row[2] = log.getDatabase();
                row[3] = log.getConnectionFrom();//add
                row[4] = log.getOperationType();//add
                row[5] = log.getQuery();
                row[6] = log.getOperationResult();//add
                row[7] = log.getMsg();
                row[8] = log.getIllegalEventId();
                logModel.addRow(row);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.autoCommit(conn);
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        //logger.info("Return;size=" + logList.size());
        //return logList;
    }
    
    private String getStatus(String sqlStateCode)
    {
       return  (sqlStateCode!=null && "00000".equals(sqlStateCode))?constBundle.getString("success") : constBundle.getString("failed");
    }
    
    public List<LogInfoDTO> getNewLogList(HelperInfoDTO helperInfo, Timestamp lastTime) throws Exception
    {
        //logger.info("lastTime:"+lastTime);
        List<LogInfoDTO> logList = new ArrayList();
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return logList;
        }
        StringBuilder sql = new StringBuilder();
        if (helperInfo.isAuditUser())
        {
            sql.append("SELECT log_time, user_name, database_name, error_severity, sql_state_code,")//command_tag,
                    .append(" message, query, application_name, connection_from,")
                    .append(" session_start_time, process_id, xid, subxid, audit_oid, event_oid, operationid, event_severity")
                    .append(" FROM pg_catalog.pg_audit_trail_logs ")
                    .append(" LEFT OUTER JOIN pg_roles r ON r.rolname = user_name")//add
                    .append(" LEFT OUTER JOIN pg_auth_members m ON m.member = r.oid")//add
                    //.append(" WHERE ( r.oid IN(10,13) OR m.roleid=13 )")//add
                    .append(" WHERE ( r.oid!=12 )")
                    .append(" AND log_time > ?")
                    .append(" ORDER BY log_time asc");
        }else//add
        {
            sql.append("SELECT log_time, user_name, database_name, error_severity, sql_state_code,")//command_tag,
                    .append(" message, query, application_name, connection_from,")
                    .append(" session_start_time, process_id, xid, subxid, audit_oid, event_oid, operationid, event_severity")
                    .append(" FROM pg_catalog.pg_audit_trail_logs ")
                    .append(" LEFT OUTER JOIN pg_roles r ON r.rolname = user_name")//add
                    .append(" LEFT OUTER JOIN pg_auth_members m ON m.member = r.oid")//add
                    //.append(" WHERE (r.oid NOT IN(10,13) AND (m.roleid IS NULL OR m.roleid!=13))")//add
                    .append(" WHERE (r.oid IN(12) )")
                    .append(" AND log_time > ?")
                    .append(" ORDER BY log_time asc");
        }
        logger.info("sql=" + sql.toString());
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.prepareStatement(sql.toString());            
            stmt.setTimestamp(1, lastTime);
            rs = stmt.executeQuery();
            while(rs.next())
            {
                LogInfoDTO log = new LogInfoDTO();
                log.setLogTime(rs.getTimestamp(1));
                log.setUser(rs.getString(2));
                log.setDatabase(rs.getString(3));
                log.setErrorSeverity(rs.getString(4));
                log.setSqlStateCode(rs.getString(5));
                log.setOperationResult(this.getStatus(log.getSqlStateCode()));
                //log.setCommandTag(rs.getString(6));
                log.setMsg(rs.getString(6));
                log.setQuery(rs.getString(7));
                log.setApplicationName(rs.getString(8));
                log.setConnectionFrom(rs.getString(9));
                log.setSessionStartTimestamp(rs.getTimestamp(10));
                log.setProcessId(rs.getInt(11));
                log.setTransactionId(rs.getInt(12));
                log.setSubTransactionId(rs.getInt(13));
                log.setAuditId(rs.getLong(14));
                log.setIllegalEventId(rs.getLong(15));
                log.setOperationId(rs.getInt("operationid"));
                log.setEventSeverity(rs.getInt(17));
                log.setOperationType(this.getCNOperationById(log.getOperationId()));//(this.getLogOperationType(log.getAuditId(), log.getIllegalEventId(), conn, stmt, rs));//add
                logList.add(log);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            //logger.info("close resource");
        }
        //logger.info("Return;new log size=" + logList.size());
        return logList;
    }

    private String getLogOperationType(long auditId, long illegalEventId, Connection conn, PreparedStatement stmt, ResultSet rs) throws SQLException
    {
        String type = null;
       if (auditId != 0)
        {
            stmt = conn.prepareStatement("SELECT operation_id from  pg_catalog.pg_audit_rule WHERE oid=?");
            stmt.setLong(1, auditId);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                type = this.getCNOperationById(rs.getInt(1));
            }
        } else if (illegalEventId != 0)
        {
            stmt = conn.prepareStatement("SELECT operation_id from  pg_catalog.pg_audit_event WHERE oid=?");
            stmt.setLong(1, illegalEventId);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                type =  this.getCNOperationById(rs.getInt(1));
            }
            //type="illegalEvent";
        }else
        {
             type = constBundle.getString("sysOperation");
        }
        return type;
    }
    
    
    
    
           
    //---------------------------------------------------------------------------------------------------------- 
    //useless now
    public void disconnect(HelperInfoDTO helperInfo, int pid) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return.");
            return;
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            String sql = "select pg_terminate_backend(?)";
            logger.info(sql);
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, pid);
            stmt.execute();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            //logger.info("close resource");
        }
    }
    public IllegalEventInfoDTO getIllegalEventByOid(HelperInfoDTO helperInfo, Long oid) throws Exception
     {
     if (helperInfo == null)
     {
     logger.warn("helperInfo is null, do nothing and return null.");
     return null;
     }
     if(oid == 0)
     {
     return null;
     }
     StringBuilder sql = new StringBuilder();
     sql.append("SELECT oid, event_id, event_count, object_type_id, object_id,")
     .append(" object_name, operation_id, action_id, description")
                .append(" FROM pg_catalog.pg_audit_event ")
                .append(" WHERE oid = ").append(oid).append(";");
        logger.info(sql.toString());
        IllegalEventInfoDTO eventInfo = null;
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql.toString());
            while (rs.next())
            {
                eventInfo = new IllegalEventInfoDTO();
                eventInfo.setOid(rs.getString(1));
                eventInfo.setEventId(rs.getInt(2));
                eventInfo.setEventName(this.getEventById(eventInfo.getEventId()));
                eventInfo.setCount(rs.getString(3));
                eventInfo.setObjectTypeId(rs.getInt(4));
                eventInfo.setObjectType(this.getTypeById(eventInfo.getObjectTypeId()));
                eventInfo.setObjectOid(rs.getString(5));
                eventInfo.setObjectName(rs.getString(6));
                eventInfo.setOperationId(rs.getInt(7));
                eventInfo.setOperation(this.getOperationById(eventInfo.getOperationId()));
                eventInfo.setActionId(rs.getInt(8));
                eventInfo.setAction(this.getActionById(eventInfo.getActionId()));
                eventInfo.setDescription(rs.getString(9));
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Stream");
        }
        return eventInfo;
    }
 
    public IllegalEventInfoDTO  analyzeLogInfo( HelperInfoDTO helperInfo, LogInfoDTO log) throws Exception
    {
        if (helperInfo == null)
        {
            logger.warn("helperInfo is null, do nothing and return null.");
            return null;
        }
        if(log == null)
        {
            logger.warn("logInfo is null, do nothing and return null");
            return null;
        }
        IllegalEventInfoDTO event = null;
        String state = log.getSqlStateCode();
        logger.info("sql state code = "+state);
        try
        {
            /*
            switch (state)
            {
                case "00000":
                    String msg = log.getMsg().toLowerCase();
                    if (msg.indexOf("delete") >= 0)
                    {
                        event = getDataDelete(helperInfo, log, "delete");
                    } else if (msg.indexOf("truncate") >= 0)
                    {
                        event = this.getDataDelete(helperInfo, log, "truncate");
                    } else if (msg.indexOf("update") >= 0)
                    {
                        event = this.getDataModify(helperInfo, log, "update");
                    } else if (msg.indexOf("insert") >= 0)
                    {
                        event = this.getDataModify(helperInfo, log, "insert");
                    } else if (msg.indexOf("drop") >= 0)
                    {
                        event = this.getObjectDrop(helperInfo, log);
                    }
                    break;
                case "42500":
                    String commandTag = log.getCommandTag();
                    switch (commandTag)
                    {
                        case "SELECT":
                            //objType = "table" or "view"
                            event = this.getIllegalDataAccess(helperInfo, log);
                            break;
                        case "DELETE":
                        case "TRUNCATE":
                        case "UPDATE":
                        case "INSERT":
                            //objType = "table";
                            event = this.getIllegalDataModifyEvent(helperInfo, log);
                            break;
                        case "ALTER TABLESPACE":
                        case "ALTER ROLE":
                        case "ALTER DATABASE":
                        case "ALTER SCHEMA":
                        case "ALTER TABLE":
                        case "ALTER SEQUENCE":
                        case "ALTER INDEX":
                        case "ALTER FUNCTION":
                            String type = commandTag.replace("ALTER ", "").toLowerCase().trim();
                            event = this.getIllegalDefinitionModify(helperInfo, log, type);
                            break;
                        default:
                            logger.info(state + " is an inconsiderable operation, do nothing and return null.");
                            return null;
                    }
                    break;
                case "42p00":
                    event = this.getObjectNotExist(helperInfo, log);
                    break;
                case "28p00":
                    String commandTag2 = log.getCommandTag();
                    if (commandTag2.equals("authentication"))
                    {
                        event = this.getWrongPwdLoginFailureEvent(helperInfo, log);
                    }
                    break;
                default:
                    logger.info(state + " is an inconsiderable  sql state code, do nothing and return null.");
                    return null;
            }
            */
            if (state.equals("00000"))
            {
                String msg = log.getMsg().toLowerCase();
                if (msg.indexOf("delete") >= 0)
                {
                    event = this.getDataDelete(helperInfo, log, "delete");
                } else if (msg.indexOf("truncate") >= 0)
                {
                    event = this.getDataDelete(helperInfo, log, "truncate");
                } else if (msg.indexOf("update") >= 0)
                {
                    event = this.getDataModify(helperInfo, log, "update");
                } else if (msg.indexOf("insert") >= 0)
                {
                    event = this.getDataModify(helperInfo, log, "insert");
                } else if (msg.indexOf("drop") >= 0)
                {
                    event = this.getObjectDrop(helperInfo, log);
                }
            } else if (state.equals("42500"))
            {
                String commandTag = log.getCommandTag();

                if (commandTag.equals("SELECT"))
                {
                    //objType = "table" or "view"
                    event = this.getIllegalDataAccess(helperInfo, log);
                } else if (commandTag.equals("DELETE")
                        || commandTag.equals("TRUNCATE")
                        || commandTag.equals("UPDATE")
                        || commandTag.equals("INSERT"))
                {
                    //objType = "table";
                    event = this.getIllegalDataModifyEvent(helperInfo, log);
                } else if (commandTag.equals("ALTER TABLESPACE")
                        || commandTag.equals("ALTER ROLE")
                        || commandTag.equals("ALTER DATABASE")
                        || commandTag.equals("ALTER SCHEMA")
                        || commandTag.equals("ALTER TABLE")
                        || commandTag.equals("ALTER SEQUENCE")
                        || commandTag.equals("ALTER INDEX")
                        || commandTag.equals("ALTER FUNCTION"))
                {
                    String type = commandTag.replace("ALTER ", "").toLowerCase().trim();
                    event = this.getIllegalDefinitionModify(helperInfo, log, type);
                } else
                {
                    logger.info(state + " is an inconsiderable operation, do nothing and return null.");
                    return null;
                }

            } else if (state.equals("42p00"))
            {
                event = this.getObjectNotExist(helperInfo, log);
            } else if (state.equals("28p00"))
            {

                String commandTag2 = log.getCommandTag();
                if (commandTag2.equals("authentication"))
                {
                    event = this.getWrongPwdLoginFailureEvent(helperInfo, log);
                }
            } else
            {
                logger.info(state + " is an inconsiderable  sql state code, do nothing and return null.");
                return null;
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
        return event;
    }
    private IllegalEventInfoDTO getIllegalDataAccess(HelperInfoDTO helperInfo, LogInfoDTO log) throws Exception
    {
        String logMsg = log.getMsg();
        logger.info("log message=" + logMsg);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='illegal_data_access' AND object_type IN ('table','view')")//magbe happened that table and view has same name
                .append(" AND operation_name='").append("select").append("';");
        logger.info(sql.toString());
        
        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);           
    }
    private IllegalEventInfoDTO getIllegalDataModifyEvent(HelperInfoDTO helperInfo, LogInfoDTO log) throws Exception
    {
        String logMsg = log.getMsg();
        String logCommand = log.getCommandTag().toLowerCase();
        logger.info("log message = "+logMsg+",operation = "+ logCommand);  
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='illegal_data_modify' AND object_type='table'")
                .append(" AND operation_name='").append(logCommand).append("';");
        logger.info(sql.toString());        
        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);      
    }      
    private IllegalEventInfoDTO getIllegalDefinitionModify(HelperInfoDTO helperInfo, LogInfoDTO log, String objType) throws Exception
    {
        String logMsg = log.getMsg();
        logger.info("log message=" + logMsg +", object type = " + objType);      
        
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='illegal_Definition_modify'")
                .append(" AND object_type = '").append(objType).append("'")
                .append(" AND operation_name = '").append("alter").append("';");
        logger.info(sql.toString());
        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);
    }
    //for illegal data access, illegal data modify, illegal definition modify, object not exist
    private IllegalEventInfoDTO getSatisfiedEvent(HelperInfoDTO  helperInfo, String sql, String logMsg ) throws Exception
    {
        IllegalEventInfoDTO eventInfo = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                String objName = rs.getString(6);
                logger.info("object name = " + objName);
                if (logMsg.indexOf(objName) >= 0)
                {
                    eventInfo = new IllegalEventInfoDTO();
                    eventInfo.setOid(rs.getString(1));
                    eventInfo.setEventName(rs.getString(2));
                    eventInfo.setCount(rs.getString(3));
                    eventInfo.setObjectType(rs.getString(4));
                    eventInfo.setObjectOid(rs.getString(5));
                    eventInfo.setObjectName(rs.getString(6));
                    eventInfo.setOperation(rs.getString(7));
                    eventInfo.setAction(rs.getString(8));
                    eventInfo.setDescription(rs.getString(9));
                    break;
                }
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        return eventInfo;
    }  
    private IllegalEventInfoDTO  getDataDelete(HelperInfoDTO helperInfo, LogInfoDTO log, String operation) throws Exception
    {
        String logMsg = log.getMsg();
        logger.info("log message=" + logMsg);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='data_delete'")
                .append(" AND object_type='table'")
                .append(" AND operation_name='").append(operation).append("';");
        logger.info(sql.toString());
        
        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);    
    }
    private IllegalEventInfoDTO  getDataModify(HelperInfoDTO helperInfo, LogInfoDTO log, String operation) throws Exception
    {
        String logMsg = log.getMsg();
        logger.info("log message=" + logMsg);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='data_modify'")
                .append(" AND object_type='table'")
                .append(" AND operation_name='").append(operation).append("';");
        logger.info(sql.toString());
        
        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);    
    }
    
    private IllegalEventInfoDTO getObjectDrop(HelperInfoDTO helperInfo, LogInfoDTO log) throws Exception
    {
        String logMsg = log.getMsg();
        logger.info("log message=" + logMsg);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='object_drop'")
                .append(" AND operation_name='drop';");
        logger.info(sql.toString());

        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);
    }
          
    private IllegalEventInfoDTO  getObjectNotExist(HelperInfoDTO helperInfo, LogInfoDTO log) throws Exception
    {
        String logMsg = log.getMsg();
        logger.info("log message=" + logMsg);

        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='object_not_exist'")
                .append(" AND operation_name='").append(log.getCommandTag().toLowerCase()).append("';");
        logger.info(sql.toString());
        
        return this.getSatisfiedEvent(helperInfo, sql.toString(), logMsg);    
    }
    
    private IllegalEventInfoDTO getWrongPwdLoginFailureEvent(HelperInfoDTO helperInfo, LogInfoDTO log) throws Exception
    {
        String logMsg = log.getMsg();
        String logCommand = log.getCommandTag().toLowerCase();
        logger.info("log message = "+logMsg+",operation = "+ logCommand);
        IllegalEventInfoDTO eventInfo = null;
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT oid, event_name, event_count, object_type, object_id,")
                .append(" object_name, operation_name, action_name, event_description")
                .append(" FROM syssso.audit_event ")
                .append(" WHERE event_name='wrong_pwd_login_failure' AND object_type='user';");
        logger.info(sql.toString());
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.prepareStatement(sql.toString());
            rs = stmt.executeQuery();
            while (rs.next())
            {
                String objName = rs.getString(6);
                logger.info("object name = " + objName);
                if (logMsg.indexOf(objName) >= 0)
                {
                    int currentCount = getLoginCountOfUser(helperInfo, objName);
                    int countLimit = Integer.valueOf(rs.getString(3));
                    logger.info("countLimit=" + countLimit);
                    if (currentCount >= countLimit)
                    {
                        eventInfo = new IllegalEventInfoDTO();
                        eventInfo.setOid(rs.getString(1));
                        eventInfo.setEventName(rs.getString(2));
                        eventInfo.setCount(rs.getString(3));
                        eventInfo.setObjectType(rs.getString(4));
                        eventInfo.setObjectOid(rs.getString(5));
                        eventInfo.setObjectName(rs.getString(6));
                        eventInfo.setOperation(rs.getString(7));
                        eventInfo.setAction(rs.getString(8));
                        eventInfo.setDescription(rs.getString(9));
                         break;
                    }                   
                }
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        return eventInfo;        
    }
    //for Wrong Pwd Login Failure        
    private int getLoginCountOfUser(HelperInfoDTO helperInfo, String user) throws Exception
    {
        logger.info("Enter:user="+user);
        int count = 0;
        String sql = "select lcount from syssso.pg_authid where rolname =?";
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, user);
            rs = stmt.executeQuery();
            while (rs.next())
            {
                count = rs.getInt(1);
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("close resource");
        }
        logger.info("Return: login count=" + count);
        return count;
    }

    
//---------------------------------------------------------------------------------------
    private void writeLog(PrintWriter pw, String content)
    {
        pw.write(content);
        pw.println();
        pw.flush();
    }
    //加密,String明文输入,String密文输出
    private String getEncString(Key key, String strMing) throws Exception
    {
        String strMi = "";
        try
        {
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] byteFina = cipher.doFinal(strMing.getBytes());

            String hs = "";
            String stmp = "";
            for (int n = 0; n < byteFina.length; n++)
            {
                //整数转成十六进制表示
                stmp = (Integer.toHexString(byteFina[n] & 0XFF));
                if (stmp.length() == 1)
                {
                    hs = hs + "0" + stmp;
                } else
                {
                    hs = hs + stmp;
                }
            }
            strMi = hs.toUpperCase();
            //strMi =  byte2hex(getEncCode(strMing.getBytes()));
        } catch (Exception e)
        {
            logger.error(e.getMessage());
            throw new Exception(e);
        }
        return strMi;
    }
    //解密,以String密文输入,String明文输出
    private String getDesString(Key key, String strMi) throws Exception
    {
        String strMing = "";
        try
        {
            byte[] b = strMi.getBytes();
            if ((b.length % 2) != 0)
            {
                throw new IllegalArgumentException("长度不是偶数");
            }
            byte[] b2 = new byte[b.length / 2];
            for (int n = 0; n < b.length; n += 2)
            {
                String item = new String(b, n, 2);
                // 两位一组，表示一个字节,把这样表示的16进制字符串，还原成一个进制字节
                b2[n / 2] = (byte) Integer.parseInt(item, 16);
            }

            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] byteFina = cipher.doFinal(b2);
            strMing = new String(byteFina);
            //strMing = new String(getDesCode(hex2byte(strMi.getBytes())));
        } catch (Exception e)
        {
            logger.error(e.getMessage());
            throw new Exception(e);
        }
        return strMing;
    }
    //将明文string，加密后存储到文件中。
    public void saveStringToFile(Key key, PrintWriter pw, String content) throws Exception
    {
//        File logFile = new File(filePath);
//        if (!logFile.exists())
//        {
//            System.out.println("log.txt file no exist, create one.");
//            logFile.createNewFile();
//        }
//        PrintWriter pw = new PrintWriter(logFile);
        String strEnc = getEncString(key, content);//加密字符串,返回String的密文 
        writeLog(pw, strEnc);
        System.out.println("密文 = " + strEnc);
    }
    //读取并解析文件中的密文为明文
    public void readStringFromEncryptedFile(Key key, String filePath) throws Exception
    {
        File logFile = new File(filePath);
        if (!logFile.exists())
        {
            System.out.println("log.txt file no exist, read nothing.");
            return;
        }
        FileInputStream fis = new FileInputStream(logFile);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);
        //解析文件              
        String line = "";
        while ((line = br.readLine()) != null)
        {
            String strDes = getDesString(key, line);//把String类型的密文解密   
            System.out.println("明文 = " + strDes);
        }
    }

}
