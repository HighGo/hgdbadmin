/* ------------------------------------------------
 *
 * File: RowIdHeaderModel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\paging\RowIdHeaderModel.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.paging;

import javax.swing.table.DefaultTableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Liu Yuanyuan
 */
public class RowIdHeaderModel extends DefaultTableModel
{
    private Logger logger = LoggerFactory.getLogger(getClass());

    private DefaultTableModel pagingModel;
    private int pageSize;
    public RowIdHeaderModel(DefaultTableModel pagingModel, int pageSize)
    {
        this.pagingModel = pagingModel;
        this.pageSize = pageSize;
    }
    @Override
    public Object getValueAt(int row, int col)
    {
        if (row < 0 || col < 0)
        {
            return null;
        } else
        {
            Object value = 0;
            //logger.info("row="+row+",rowCount="+this.getRowCount()+",isEditable="+pagingModel.isCellEditable(row, pagingModel.getColumnCount()-1));
//            if ((row == this.getRowCount() - 1) && pagingModel.isCellEditable(row, pagingModel.getColumnCount()-1))
//            {
//                value = "*";
//            } else
            if (pagingModel instanceof SmartPagingModel)//for query tool
            {
                SmartPagingModel pm = (SmartPagingModel) pagingModel;
                value = pm.getPageOffset() * pm.getPageSize() + row + 1;
            }       
            return value;
        }
    }


    @Override
    public boolean isCellEditable(int row, int column)
    {
        return false;
    }
    @Override
    public int getColumnCount()
    {
        return 1;
    }
    @Override
    public int getRowCount()
    {
        if (pagingModel == null)
        {
            return pageSize;
        } else
        {
            return pagingModel.getRowCount();
        }
    }
    
    @Override
    public String getColumnName(int column)
    {
        return "ID";
    }
}
