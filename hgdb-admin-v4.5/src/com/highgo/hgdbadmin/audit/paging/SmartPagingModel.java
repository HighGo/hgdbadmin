/* ------------------------------------------------
 *
 * File: SmartPagingModel.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\paging\SmartPagingModel.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.audit.paging;

import com.highgo.hgdbadmin.audit.model.HelperInfoDTO;
import com.highgo.hgdbadmin.audit.model.LabeledRecordInfoDTO;
import com.highgo.hgdbadmin.audit.util.JdbcHelper;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Liu Yuanyuan
 *
 * A larger table model that performs "paging" of its data. This model reports a
 * small number of rows (like 1000 or so) as a "page" of data. You can switch
 * pages to view all of the rows as needed using the pageDown() and pageUp()
 * methods. Presumably, access to the other pages of data is dictated by other
 * GUI elements such as up/down buttons, or maybe a Spinner that allows you
 * to enter the page number you want to display.
 * 
 */
public class SmartPagingModel extends DefaultTableModel
{
    private Logger logger = LoggerFactory.getLogger(getClass());
    
    protected static int pageSize;
    
    protected Object[] header;
    protected Class[] typeArray;  
    private HelperInfoDTO helperInfo;
    private String sql;
    
    protected int pageOffset;//pageId = pageOffSet+1;
    protected List<Object[]> thisPageData;
    protected int thisPageRow;

    public SmartPagingModel(HelperInfoDTO helperInfo, String sql,
            int pageSize, Class[] typeArray, Object[] headerArray, List<Object[]> dataOfFirstPage)
    {
        this.helperInfo = helperInfo;
        this.sql = sql;
        this.typeArray = typeArray;
        this.header = headerArray;
        this.pageSize = pageSize;

        this.pageOffset = 0;        
        this.thisPageData = dataOfFirstPage;
        thisPageRow = thisPageData.size();
//        refreshDataOfThePage();
    }

    @Override
    public boolean isCellEditable(int row, int column)
    {
        //logger.info("column=" + column + ", row=" + row);
        return column == 2;
    }
    @Override
    public int getRowCount()// Return values appropriate for the visible table part.
    {
        if (thisPageData == null)
        {
            return Math.min(pageSize, thisPageRow);
        } else
        {
            return thisPageData.size();
        }
    }
    @Override
    public int getColumnCount()
    {
        return header.length;
    }
    @Override
    public String getColumnName(int col)
    {
        if (col < 0)
        {
            return null;
        } else
        {
            return header[col].toString();
        }
    }
    @Override
    public Class<?> getColumnClass(int columnIndex)
    {
        if(columnIndex<0)
        {
            return null;
        }
        else if(typeArray==null)//this is for 
        {
            return Object.class;
        } else
        {
            return typeArray[columnIndex];
        }
    }
    // Work only on the visible part of the table.  
    @Override
    public Object getValueAt(int row, int col)
    {
        if (row < 0 || col < 0)
        {
            return null;
        } else
        {
            return thisPageData.get(row)[col];
        }
    }
    
        @Override
    public void setValueAt(Object aValue, int row, int column)
    {
        if (row >= 0 && column >= 0)
        {
            logger.info("row=" + row + ",column=" + column + ",aValue=" + aValue);
            if (column == 2)
            {
                thisPageData.get(row)[column] = aValue;
            }
        }
    }
    
    @Override
    public void removeRow(int row)
    {
        thisPageData.remove(row);
        this.fireTableDataChanged();
    }
    
    // Use this method to figure out which page you are on(pageId = pageOffset+1). It starts from 0.
    public int getPageOffset()
    {
        return pageOffset;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    private void refreshDataOfThePage()
    {
        thisPageData = this.getDataList(helperInfo, sql, pageOffset, pageSize);
        thisPageRow = thisPageData.size();
    }
    private List<Object[]> getDataList(HelperInfoDTO helperInfo, String sql, int pageOffset, int pageSize)
    {
        logger.info("sqlSequecne=" + sql);
        List<Object[]> dataList = new ArrayList();
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, return a empty list.");
            return dataList;
        }
        if (sql.isEmpty())
        {
            logger.error("sql is empty, return a empty list.");
            return dataList;
        }
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try
        {
            String url = helperInfo.getUrl();
            logger.info("url=" + url);
//            long startTime = System.currentTimeMillis();
            conn = JdbcHelper.getConnection(helperInfo);
            stmt = conn.prepareStatement(sql + " limit ? offset ?");
            stmt.setInt(1, pageSize);
            stmt.setInt(2, pageOffset * pageSize);
            rs = stmt.executeQuery();
            int column = rs.getMetaData().getColumnCount();
            int row = pageOffset * pageSize;
            LabeledRecordInfoDTO obj;
            Object[] rowData;
            while (rs.next())
            {
                obj = new LabeledRecordInfoDTO();
                obj.setCtid(rs.getObject(1));
                obj.setSentivity(rs.getInt(2));
                obj.setaColumn(rs.getMetaData().getColumnName(3));
                
                StringBuilder rowDatas = new StringBuilder();
                rowDatas.append("(");
                for (int i = 3; i <= column; i++)
                {
                    if (i > 3)
                    {
                        rowDatas.append(",");
                    }
                    String name = rs.getMetaData().getColumnClassName(i);                   
                    //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
                    //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
                    /*
                    switch (name)
                    {
                        case "com.highgo.jdbc.util.PGmoney":
                        case "java.lang.Boolean":
                        case "java.sql.SQLXML"://xml
                         case "[B"://bytea
                            rowDatas.append("\"").append(rs.getString(i)).append("\"");
                            break;
                        default:
                            //logger.info("type=" + name);
                            //"java.lang.Integer"
                            rowDatas.append("\"").append(rs.getObject(i)).append("\"");
                            break;
                    }
                    */
                     if (name.equals("com.highgo.jdbc.util.PGmoney")
                            || name.equals("java.lang.Boolean")
                            || name.equals("java.sql.SQLXML")
                            || name.equals("[B"))
                    {
                        rowDatas.append("\"").append(rs.getString(i)).append("\"");
                    } else
                    {
                         //logger.info("type=" + name);
                        //"java.lang.Integer"
                        rowDatas.append("\"").append(rs.getObject(i)).append("\"");
                    }                    
                }
                rowDatas.append(")");
                obj.setRowData(rowDatas.toString());
                //objList.add(obj);
                
                rowData = new Object[3];
                rowData[0] = obj;
                rowData[1] = obj.getSentivity();
                rowData[2] = false;
                dataList.add(rowData);
                row++;
                //logger.info("row=" + row);
            }
        } catch (Exception e)
        {
            logger.error(e.getMessage());
            e.printStackTrace(System.out);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
            logger.info("Close Resource");
        }
        return dataList;
    }

    //Update the page offset and fire a data changed (all rows).  
    public boolean pageDown()
    {
////        if (pageOffset < getPageCount() - 1)  
//        pageOffset++;
//        logger.info("pageOffset=" + pageOffset);
//        refreshDataOfThePage();
//        if (thisPageData.size() > 0)
//        {
//            this.fireTableDataChanged();
//        }
//        logger.info("thisPageRow=" + thisPageRow);
//        return thisPageRow == pageSize;

        pageOffset++;
        List<Object[]> newData = this.getDataList(helperInfo, sql, pageOffset, pageSize);
        logger.info("dataSize="+newData.size());
        if (newData.size() > 0)
        {
            thisPageData = newData;
            thisPageRow = thisPageData.size();
            this.fireTableDataChanged();
            logger.info("pageOffset=" + pageOffset);
            return thisPageRow == pageSize;
        } else
        {
            pageOffset--;
            logger.info("pageOffset=" + pageOffset);
            return false;           
        }
    }

    //Update the page offset and fire a data changed (all rows).  
    public void pageUp()
    {
        logger.info("pageOffset:" + pageOffset);
        if (pageOffset > 0)
        {
            pageOffset--;
            logger.info("pageOffset=" + pageOffset + ",pageSize =" + pageSize);
            refreshDataOfThePage();;
            this.fireTableDataChanged();
        }
    }
    
    /* 
    * We provide our own version of a scrollpane that includes  
    * the page up and page down buttons by default.  
    */
    public static JScrollPane createPagingScrollPaneForTable(JScrollPane jsp,JTable tbl)
    {
        //JScrollPane jsp = new JScrollPane(tbl);
        jsp.setViewportView(tbl);
        TableModel tmodel = tbl.getModel();
        // Don't choke if this is called on a regular table . 
        if (!(tmodel instanceof SmartPagingModel))
        {
            return jsp;
        }
        //build the real scrollpane  
        final SmartPagingModel pagingModel = (SmartPagingModel) tmodel;
        final JButton upButton = new JButton(new PageTurnIcon(PageTurnIcon.UP));       
        final JButton downButton = new JButton(new PageTurnIcon(PageTurnIcon.DOWN));
        upButton.setEnabled(false); // starts off at 0, so can't go up
        System.out.println("first page row:" + pagingModel.getRowCount());
        downButton.setEnabled(pagingModel.getRowCount() == pageSize);

        upButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                pagingModel.pageUp();
                JButton upButton = (JButton) ae.getSource();
                // If we hit the top of the data, disable the up button.  
                if (pagingModel.getPageOffset() == 0)
                {
                    upButton.setEnabled(false);
                }
                downButton.setEnabled(true);
            }
        });
        
        downButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                boolean isDownable=  pagingModel.pageDown();
                JButton downButton = (JButton) ae.getSource();
                downButton.setEnabled(isDownable);
                //If we hit the bottom of the data, disable the down button.  
//                System.out.println("pageOffSet="+model.getPageOffset()+",pageCount="+model.getPageCount());
//                if (model.getPageOffset() == (model.getPageCount() - 1))
//                {
//                    downButton.setEnabled(false);
//                }
                upButton.setEnabled(true);
            }
        });
        //Turn on the scrollbars; otherwise we won't get our corners.
        jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jsp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        //Add in the corners (page up/down).
        jsp.setCorner(ScrollPaneConstants.UPPER_RIGHT_CORNER, upButton);
        jsp.setCorner(ScrollPaneConstants.LOWER_RIGHT_CORNER, downButton);

        //row header to display row id
        final RowIdHeaderModel rowHeader = new RowIdHeaderModel(pagingModel, pageSize);
        JTable headerTable = new JTable(rowHeader);
        //LookAndFeel.installColorsAndFont(headerTable, "TableHeader.background", "TableHeader.foreground", "TableHeader.font");        
        //headerTable.getTableHeader().setBackground(Color.white);
        headerTable.setEnabled(false);//disable selection
        headerTable.setBackground(new Color(240, 240, 240));
        //headerTable.setFont(new Font("SansSerif", Font.BOLD, 12));
        headerTable.setRowHeight(tbl.getRowHeight());      
        
        headerTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        headerTable.getTableHeader().setReorderingAllowed(false);
        jsp.setCorner(JScrollPane.UPPER_LEFT_CORNER,headerTable.getTableHeader());
        
        int columnWidth = 30;
        headerTable.getColumnModel().getColumn(0).setPreferredWidth(columnWidth);
        headerTable.getColumnModel().getColumn(0).setMaxWidth(100);
        headerTable.setPreferredScrollableViewportSize(new Dimension(columnWidth, 0));
//        headerTable.setIntercellSpacing(new Dimension(0, 0));
//        Dimension d = headerTable.getPreferredScrollableViewportSize();
//        d.width = headerTable.getPreferredSize().width;
//        headerTable.setPreferredScrollableViewportSize(d); 
        
        jsp.setRowHeaderView(headerTable);        
        headerTable.addComponentListener(new RowHeaderResizeListener(jsp,jsp.getRowHeader(),headerTable, tbl));
        
        pagingModel.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                rowHeader.fireTableDataChanged();//refresh row id
            }
        });
        return jsp;
    }
}
