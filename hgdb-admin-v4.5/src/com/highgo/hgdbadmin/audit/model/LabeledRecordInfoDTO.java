/* ------------------------------------------------
 *
 * File: LabeledRecordInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\LabeledRecordInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class LabeledRecordInfoDTO
{
    private Object ctid;
    private int sentivity;
    private String aColumn;
    private String rowData;

    public Object getCtid()
    {
        return ctid;
    }

    public void setCtid(Object ctid)
    {
        this.ctid = ctid;
    }

    public int getSentivity()
    {
        return sentivity;
    }

    public void setSentivity(int sentivity)
    {
        this.sentivity = sentivity;
    }


    public String getaColumn()
    {
        return aColumn;
    }

    public void setaColumn(String aColumn)
    {
        this.aColumn = aColumn;
    }

    public String getRowData()
    {
        return rowData;
    }

    public void setRowData(String rowData)
    {
        this.rowData = rowData;
    }

    @Override
    public String toString()
    {
        return this.rowData.toString();
    }
}
