/* ------------------------------------------------
 *
 * File: LogInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\LogInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

import java.sql.Timestamp;

/**
 *
 * @author Liu Yuanyuan
 */
public class LogInfoDTO
{
    private Timestamp logTime;
    private String user;
    private String database;
    private String commandTag;
    private String errorSeverity;
    private String sqlStateCode;  
    private String operationResult;//failed or success
    private String msg;
    private String query;
    private String applicationName;
    private String connectionFrom;
    private Timestamp sessionStartTimestamp;
    private int processId;
    private int transactionId;
    private int subTransactionId;
    private Long auditId;
    private Long illegalEventId;
    private int operationId;
    private int eventSeverity;//1,2,3,4
    private String operationType;//add

    public String getOperationResult()
    {
        return operationResult;
    }

    public void setOperationResult(String operationResult)
    {
        this.operationResult = operationResult;
    }

    
    
    public int getOperationId()
    {
        return operationId;
    }

    public void setOperationId(int operationId)
    {
        this.operationId = operationId;
    }
    
    public Timestamp getLogTime()
    {
        return logTime;
    }

    public void setLogTime(Timestamp logTime)
    {
        this.logTime = logTime;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getDatabase()
    {
        return database;
    }

    public void setDatabase(String database)
    {
        this.database = database;
    }

    public String getCommandTag()
    {
        return commandTag;
    }

    public void setCommandTag(String commandTag)
    {
        this.commandTag = commandTag;
    }

    public String getErrorSeverity()
    {
        return errorSeverity;
    }

    public void setErrorSeverity(String errorSeverity)
    {
        this.errorSeverity = errorSeverity;
    }

    public String getSqlStateCode()
    {
        return sqlStateCode;
    }

    public void setSqlStateCode(String sqlStateCode)
    {
        this.sqlStateCode = sqlStateCode;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public void setApplicationName(String applicationName)
    {
        this.applicationName = applicationName;
    }

    public String getConnectionFrom()
    {
        return connectionFrom;
    }

    public void setConnectionFrom(String connectionFrom)
    {
        this.connectionFrom = connectionFrom;
    }

    public Timestamp getSessionStartTimestamp()
    {
        return sessionStartTimestamp;
    }

    public void setSessionStartTimestamp(Timestamp sessionStartTimestamp)
    {
        this.sessionStartTimestamp = sessionStartTimestamp;
    }

    public int getProcessId()
    {
        return processId
;    }

    public void setProcessId(int processId)
    {
        this.processId = processId;
    }

    public int getTransactionId()
    {
        return transactionId;
    }

    public void setTransactionId(int transactionId)
    {
        this.transactionId = transactionId;
    }

    public int getSubTransactionId()
    {
        return subTransactionId;
    }

    public void setSubTransactionId(int subTransactionId)
    {
        this.subTransactionId = subTransactionId;
    }

    public Long getAuditId()
    {
        return auditId;
    }

    public void setAuditId(Long auditId)
    {
        this.auditId = auditId;
    }

    
    public Long getIllegalEventId()
    {
        return illegalEventId;
    }

    public void setIllegalEventId(Long illegalEventId)
    {
        this.illegalEventId = illegalEventId;
    }

    public int getEventSeverity()
    {
        return eventSeverity;
    }

    public void setEventSeverity(int eventSeverity)
    {
        this.eventSeverity = eventSeverity;
    }

    public String getOperationType()
    {
        return operationType;
    }

    public void setOperationType(String operationType)
    {
        this.operationType = operationType;
    }
    
    

    @Override
    public String toString()
    {
        return this.logTime.toString();
    }
}
