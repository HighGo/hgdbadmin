/* ------------------------------------------------
 *
 * File: LabeledObjInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\LabeledObjInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class LabeledObjInfoDTO extends ObjectInfoDTO
{
    private String ctid;
    private int sentivity;

    public String getCtid()
    {
        return ctid;
    }

    public void setCtid(String ctid)
    {
        this.ctid = ctid;
    }

    public int getSentivity()
    {
        return sentivity;
    }

    public void setSentivity(int sentivity)
    {
        this.sentivity = sentivity;
    }
    
}
