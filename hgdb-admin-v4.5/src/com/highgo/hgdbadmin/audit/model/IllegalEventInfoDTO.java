/* ------------------------------------------------
 *
 * File: IllegalEventInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\IllegalEventInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class IllegalEventInfoDTO
{
    private String oid;
    private String eventName=null;//
    private int eventId=0;
    private String count = null;
    private String objectType = null;//
    private int objectTypeId = 0;
    private String objectOid = null;
    private String objectName = null;
    private String operation = null;
    private int operationId = 0;
    private String action;//
    private int actionId = 0;
    private String description=null;

    public String getOid()
    {
        return oid;
    }

    public void setOid(String oid)
    {
        this.oid = oid;
    }

    public int getEventId()
    {
        return eventId;
    }

    public void setEventId(int eventId)
    {
        this.eventId = eventId;
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String name)
    {
        this.eventName = name;
    }

    public String getCount()
    {
        return count;
    }

    public void setCount(String count)
    {
        this.count = count;
    }

    public int getObjectTypeId()
    {
        return objectTypeId;
    }

    public void setObjectTypeId(int objectTypeId)
    {
        this.objectTypeId = objectTypeId;
    }

    public String getObjectType()
    {
        return objectType;
    }

    public void setObjectType(String objectType)
    {
        this.objectType = objectType;
    }

    public String getObjectOid()
    {
        return objectOid;
    }

    public void setObjectOid(String objectOid)
    {
        this.objectOid = objectOid;
    }

    public String getObjectName()
    {
        return objectName;
    }

    public void setObjectName(String objectName)
    {
        this.objectName = objectName;
    }

    public int getOperationId()
    {
        return operationId;
    }

    public void setOperationId(int operationId)
    {
        this.operationId = operationId;
    }

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public int getActionId()
    {
        return actionId;
    }

    public void setActionId(int actionId)
    {
        this.actionId = actionId;
    }

    public String getAction()
    {
        return action;
    }

    public void setAction(String action)
    {
        this.action = action;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
    @Override
    public String toString()
    {
        return this.oid;        
    }
}
