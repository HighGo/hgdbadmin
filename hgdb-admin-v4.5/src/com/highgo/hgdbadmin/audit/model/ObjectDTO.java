/* ------------------------------------------------
 *
 * File: ObjectDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\ObjectDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class ObjectDTO
{
    private int id;
    private String value;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }
    
    @Override
    public String toString()
    {
        return this.value;
    }
}
