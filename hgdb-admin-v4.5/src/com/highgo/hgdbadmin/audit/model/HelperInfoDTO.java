/* ------------------------------------------------
 *
 * File: HelperInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\HelperInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class HelperInfoDTO
{
    private String host;
    private String port;
    private String db;
    private boolean auditUser;
    private String user;
    private String pwd;
    private boolean onSSL;

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getDb()
    {
        return db;
    }

    public void setDb(String auditDB)
    {
        this.db = auditDB;
    }

    public boolean isAuditUser()
    {
        return auditUser;
    }

    public void setAuditUser(boolean auditUser)
    {
        this.auditUser = auditUser;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String auditUser)
    {
        this.user = auditUser;
    }

    public String getPwd()
    {
        return pwd;
    }

    public void setPwd(String pwd)
    {
        this.pwd = pwd;
    }

    public boolean isOnSSL()
    {
        return onSSL;
    }

    public void setOnSSL(boolean onSSL)
    {
        this.onSSL = onSSL;
    }  

    public String getUrl()
    {
        return "jdbc:highgo://" + host + ":" + port + "/" + db;
    }
}
