/* ------------------------------------------------
 *
 * File: AuditRuleInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\AuditRuleInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class AuditRuleInfoDTO
{
    private String oid;
    private String objectType;//
    private int objectTypeId;
    private String objectId;
    private String ObjectName;
    private ObjectInfoDTO object;
    private String userId;
    private String user;
    private String operation;//
    private int operationId;
    private String result;//
    private int resultId;
    
    private String enableTime;

    public String getOid()
    {
        return oid;
    }

    public void setOid(String oid)
    {
        this.oid = oid;
    }

    public String getObjectType()
    {
        return objectType;
    }

    public void setObjectType(String objectType)
    {
        this.objectType = objectType;
    }

    public ObjectInfoDTO getObject()
    {
        return object;
    }

    public int getObjectTypeId()
    {
        return objectTypeId;
    }

    public void setObjectTypeId(int objectTypeId)
    {
        this.objectTypeId = objectTypeId;
    }

    public void setObject(ObjectInfoDTO object)
    {
        this.object = object;
    }
    
    public String getObjectId()
    {
        return objectId;
    }

    public void setObjectId(String objectOid)
    {
        this.objectId = objectOid;
    }

    public String getObjectName()
    {
        return ObjectName;
    }

    public void setObjectName(String ObjectName)
    {
        this.ObjectName = ObjectName;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    public String getOperation()
    {
        return operation;
    }

    public void setOperation(String operation)
    {
        this.operation = operation;
    }

    public int getOperationId()
    {
        return operationId;
    }

    public void setOperationId(int operationId)
    {
        this.operationId = operationId;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public int getResultId()
    {
        return resultId;
    }

    public void setResultId(int resultId)
    {
        this.resultId = resultId;
    }

    public String getEnableTime()
    {
        return enableTime;
    }

    public void setEnableTime(String enableTime)
    {
        this.enableTime = enableTime;
    }
   
    @Override
    public String toString()
    {
        return this.oid;
    }
    
}
