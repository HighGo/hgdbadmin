/* ------------------------------------------------
 *
 * File: ObjectInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\audit\model\ObjectInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.audit.model;

/**
 *
 * @author Liu Yuanyuan
 */
public class ObjectInfoDTO
{
    private String oid;
    private String name;
    private String schema;
    private String type;

    public String getOid()
    {
        return oid;
    }

    public void setOid(String oid)
    {
        this.oid = oid;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    
    @Override
    public String toString()
    {
        /*
        switch (this.type)
        {
            case "user":
            case "tablespace":
            case "database":
            case "schema":
            case "column":
            case "constraint":
            case "rule":
            case "trigger":
                return this.name;
            default:
                return this.schema + "." + this.name;
        }
        */
         if (this.type.equals("user")
                || this.type.equals("tablespace")
                || this.type.equals("database")
                || this.type.equals("schema")
                || this.type.equals("column")
                || this.type.equals("constraint")
                || this.type.equals("rule")
                || this.type.equals("trigger"))
        {
            return this.name;
        } else
        {
            return this.schema + "." + this.name;
        }
    }  
}
