/* ------------------------------------------------
 *
 * File: JdbcHelper.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20181116.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/audit/util/JdbcHelper.java
 *
 *-------------------------------------------------
 */
package com.highgo.hgdbadmin.audit.util;

import com.highgo.hgdbadmin.audit.model.HelperInfoDTO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fanyi
 */
public class JdbcHelper
{
    public static void main(String[] args)
    {
        HelperInfoDTO helperInfo = new HelperInfoDTO();        
        helperInfo.setHost("192.168.100.199");
        helperInfo.setDb("highgo");
        helperInfo.setPort("5866");
        helperInfo.setUser("pt2");
        helperInfo.setPwd("qwer12342");
        try
        {
            Connection conn = JdbcHelper.getConnection(helperInfo);
        } catch (Exception ex)
        {
            System.out.println(ex.getMessage());
            Logger.getLogger(JdbcHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    
    //user for hgdbadmin
    public static Connection getConnection(HelperInfoDTO helperInfo) throws Exception
    {
        String driver = "com.highgo.jdbc.Driver";
        Class.forName(driver);
        //Connection conn = DriverManager.getConnection(helperInfo.getUrl(), helperInfo.getUser(), helperInfo.getPwd());
        Properties props = new Properties();
        props.setProperty("user", helperInfo.getUser());
        props.setProperty("password", helperInfo.getPwd());
        //2016.7 add  the follwing two paramenters to conn hgdb on ssl
        if (helperInfo.isOnSSL())
        {
            props.setProperty("ssl", "true");
            props.setProperty("sslfactory", "com.highgo.jdbc.ssl.NonValidatingFactory");
        }
        Connection conn = DriverManager.getConnection(helperInfo.getUrl(), props);
        return conn;
    }

    public static void autoCommit(Connection conn)
    {
        try
        {
            if (conn != null && !conn.isClosed())
            {
                conn.setAutoCommit(true);
            }
        } catch (SQLException ex)
        {
            ex.printStackTrace(System.out);
        }
    }
    
    public static void rollBack(Connection conn)
    {
        try
        {
            if (conn != null && !conn.isClosed())
            {
                conn.rollback();
            }
        } catch (SQLException ex)
        {
            ex.printStackTrace(System.out);
        }
    }
    
    public static void close(Connection conn)
    {
        try
        {
            if (conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(Statement stmt)
    {
        try
        {
            if (stmt != null)
            {
                stmt.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.out);
        }
    }

    public static void close(ResultSet rs)
    {
        try
        {
            if (rs != null)
            {
                rs.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace(System.out);
        }
    }    
    
}
