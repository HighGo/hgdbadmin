/* ------------------------------------------------
 *
 * File: DataImportView.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\view\DataImportView.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datatransfer.view;

import com.highgo.hgdbadmin.controller.SyntaxController;
import com.highgo.hgdbadmin.controller.TreeController;
import com.highgo.hgdbadmin.datatransfer.controller.TransferController;
import com.highgo.hgdbadmin.datatransfer.model.ColumnDTO;
import com.highgo.hgdbadmin.datatransfer.util.Format;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.model.TableInfoDTO;
import com.highgo.hgdbadmin.datatransfer.model.FormatInfoDTO;
import com.highgo.hgdbadmin.util.FileChooserDialog;
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 *
 * @author Yuanyuan
 */
public class DataImportView extends JFrame
{
    private Logger logger = LogManager.getLogger(getClass()); //LoggerFactory.getLogger(getClass());
    private ResourceBundle constBundle = ResourceBundle.getBundle("constants");

    private CardLayout card;
        
    private TableInfoDTO tableInfo;
    private FormatInfoDTO format;
    public DataImportView(TableInfoDTO tableInfo)
    {
        this.initComponents();
        this.customView();
        this.tableInfo = tableInfo;
        this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(
                "/com/highgo/hgdbadmin/image/import.png")));
        
        format = new FormatInfoDTO();
        format.setFormat(Format.TXT);
        
        SyntaxController sc = SyntaxController.getInstance();
        String relationName = sc.getName(tableInfo.getHelperInfo().getSchema()) + "." + sc.getName(tableInfo.getName());   
        DefaultTableModel chooseDirModel = (DefaultTableModel) tblChooseDir.getModel();
        chooseDirModel.addRow(new Object[]
        {
            true, relationName, ""
        });

        cbbTable.addItem(relationName);
        cbbTable.setSelectedItem(relationName);
        
        
        this.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                windowsClosing();
            }
        });
        btnCancle.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnCancleAction(e);
            }
        });
        btnNext.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                btnNextAction(e);
            }
        });
        btnBack.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnBackAction(e);
            }
        });
        
        //FOMRATE
        rbTXT.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFormat(e);
            }
        });
        rbCSV.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFormat(e);
            }
        });
        rbSQL.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFormat(e);
            }
        });
        rbXLS.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFormat(e);
            }
        });
        rbXLSX.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFormat(e);
            }
        });
        rbHTML.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                chooseFormat(e);
            }
        });
        //CHOOSE DIR 
        tblChooseDir.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                tblChooseFileMouseDoubleClicked(e);
            }
        });                

        
        btnStart.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                btnStartImportAction(e);
            }
        });
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {

        bgFormat = new javax.swing.ButtonGroup();
        pnlChooseFormat = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        pblFormat = new javax.swing.JPanel();
        rbTXT = new javax.swing.JRadioButton();
        rbCSV = new javax.swing.JRadioButton();
        rbHTML = new javax.swing.JRadioButton();
        rbSQL = new javax.swing.JRadioButton();
        rbXLS = new javax.swing.JRadioButton();
        rbXLSX = new javax.swing.JRadioButton();
        pnlChooseDir = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblChooseDir = new javax.swing.JTable();
        pnlSpecifyOption = new javax.swing.JPanel();
        cbIncludeColumnName = new javax.swing.JCheckBox();
        jScrollPane7 = new javax.swing.JScrollPane();
        jTextArea4 = new javax.swing.JTextArea();
        cbContinueOnError = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        cbbRowSeperator = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cbbFieldSeperator = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        cbbFieldQuoter = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        cbbDateStyle = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        cbbEncoding = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cbbReplaceByteaValue = new javax.swing.JComboBox();
        pnlMapColumn = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea3 = new javax.swing.JTextArea();
        cbbTable = new javax.swing.JComboBox();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblChooseColumn = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        cbDefaultMap = new javax.swing.JCheckBox();
        pnlExecute = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        jTextArea6 = new javax.swing.JTextArea();
        jScrollPane10 = new javax.swing.JScrollPane();
        tpMsg = new javax.swing.JTextPane();
        pnlMain = new javax.swing.JPanel();
        pnlButton = new javax.swing.JPanel();
        btnBack = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        btnCancle = new javax.swing.JButton();
        btnStart = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        treeNavigation = new javax.swing.JTree();

        jScrollPane3.setBorder(null);

        jTextArea2.setEditable(false);
        jTextArea2.setBackground(javax.swing.UIManager.getDefaults().getColor("Panel.background"));
        jTextArea2.setColumns(20);
        jTextArea2.setLineWrap(true);
        jTextArea2.setRows(2);
        jTextArea2.setText(constBundle.getString("chooseImportFormatHint"));
        jTextArea2.setBorder(null);
        jScrollPane3.setViewportView(jTextArea2);

        bgFormat.add(rbTXT);
        rbTXT.setSelected(true);
        rbTXT.setText("Text File(.txt)");

        bgFormat.add(rbCSV);
        rbCSV.setText("CSV File(.csv)");

        bgFormat.add(rbHTML);
        rbHTML.setText("HTML File(.html)");

        bgFormat.add(rbSQL);
        rbSQL.setText("SQL Insert script(.sql)");

        bgFormat.add(rbXLS);
        rbXLS.setText("Excel spreadsheet(.xls)");

        bgFormat.add(rbXLSX);
        rbXLSX.setText("Excel File(2007 or later)(.xlsx)");

        javax.swing.GroupLayout pblFormatLayout = new javax.swing.GroupLayout(pblFormat);
        pblFormat.setLayout(pblFormatLayout);
        pblFormatLayout.setHorizontalGroup(
            pblFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pblFormatLayout.createSequentialGroup()
                .addGroup(pblFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pblFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(rbTXT, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(rbCSV, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(rbSQL)
                    .addComponent(rbHTML)
                    .addComponent(rbXLS)
                    .addComponent(rbXLSX))
                .addGap(0, 147, Short.MAX_VALUE))
        );
        pblFormatLayout.setVerticalGroup(
            pblFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pblFormatLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(rbTXT)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbCSV)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbSQL)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbXLS)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbXLSX)
                .addGap(0, 0, 0)
                .addComponent(rbHTML)
                .addContainerGap(79, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pnlChooseFormatLayout = new javax.swing.GroupLayout(pnlChooseFormat);
        pnlChooseFormat.setLayout(pnlChooseFormatLayout);
        pnlChooseFormatLayout.setHorizontalGroup(
            pnlChooseFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseFormatLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlChooseFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pblFormat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlChooseFormatLayout.setVerticalGroup(
            pnlChooseFormatLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseFormatLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(pblFormat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane1.setBorder(null);

        jTextArea1.setEditable(false);
        jTextArea1.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        jTextArea1.setColumns(20);
        jTextArea1.setRows(2);
        jTextArea1.setText(constBundle.getString("chooseImportDirHint"));
        jScrollPane1.setViewportView(jTextArea1);

        tblChooseDir.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", "", ""
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean []
            {
                true, false, false
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        tblChooseDir.setColumnSelectionAllowed(true);
        tblChooseDir.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tblChooseDir);
        tblChooseDir.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tblChooseDir.getColumnModel().getColumnCount() > 0)
        {
            tblChooseDir.getColumnModel().getColumn(0).setMinWidth(30);
            tblChooseDir.getColumnModel().getColumn(0).setPreferredWidth(30);
            tblChooseDir.getColumnModel().getColumn(0).setMaxWidth(30);
            tblChooseDir.getColumnModel().getColumn(1).setHeaderValue(constBundle.getString("table")
            );
            tblChooseDir.getColumnModel().getColumn(2).setHeaderValue(constBundle.getString("fileLocation")
            );
        }

        javax.swing.GroupLayout pnlChooseDirLayout = new javax.swing.GroupLayout(pnlChooseDir);
        pnlChooseDir.setLayout(pnlChooseDirLayout);
        pnlChooseDirLayout.setHorizontalGroup(
            pnlChooseDirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseDirLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlChooseDirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlChooseDirLayout.setVerticalGroup(
            pnlChooseDirLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlChooseDirLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                .addContainerGap())
        );

        cbIncludeColumnName.setSelected(true);
        cbIncludeColumnName.setText(constBundle.getString("includeColumnName")
        );

        jScrollPane7.setBorder(null);

        jTextArea4.setEditable(false);
        jTextArea4.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        jTextArea4.setColumns(20);
        jTextArea4.setRows(2);
        jTextArea4.setText(constBundle.getString("specifyOptionHint")
        );
        jScrollPane7.setViewportView(jTextArea4);

        cbContinueOnError.setText(constBundle.getString("continueOnError")
        );

        jLabel2.setText(constBundle.getString("recordDelimiter"));

        cbbRowSeperator.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "local", "CRLF", "CR", "LF" }));

        jLabel3.setText(constBundle.getString("fieldDelimiter"));

        cbbFieldSeperator.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tab", ",", ";" }));
        cbbFieldSeperator.setSelectedIndex(1);

        jLabel4.setText(constBundle.getString("fieldQualifier"));

        cbbFieldQuoter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "none", "\"", "'" }));
        cbbFieldQuoter.setSelectedIndex(1);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("format2")));

        jLabel8.setText(constBundle.getString("dateStyle")
        );

        cbbDateStyle.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "YMD", "DMY" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbDateStyle, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(cbbDateStyle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(77, Short.MAX_VALUE))
        );

        jLabel6.setText(constBundle.getString("encoding")
        );

        cbbEncoding.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "default", "UTF-8" }));

        jLabel7.setText(constBundle.getString("binaryValue"));

        cbbReplaceByteaValue.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "String", "NULL" }));

        javax.swing.GroupLayout pnlSpecifyOptionLayout = new javax.swing.GroupLayout(pnlSpecifyOption);
        pnlSpecifyOption.setLayout(pnlSpecifyOptionLayout);
        pnlSpecifyOptionLayout.setHorizontalGroup(
            pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecifyOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                    .addGroup(pnlSpecifyOptionLayout.createSequentialGroup()
                        .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbIncludeColumnName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbContinueOnError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbbEncoding, 0, 76, Short.MAX_VALUE)
                            .addComponent(cbbFieldSeperator, 0, 76, Short.MAX_VALUE)
                            .addComponent(cbbRowSeperator, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbbReplaceByteaValue, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbbFieldQuoter, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlSpecifyOptionLayout.setVerticalGroup(
            pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlSpecifyOptionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(cbIncludeColumnName)
                .addGap(5, 5, 5)
                .addComponent(cbContinueOnError)
                .addGap(5, 5, 5)
                .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cbbReplaceByteaValue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cbbEncoding, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cbbRowSeperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbbFieldSeperator, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(pnlSpecifyOptionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbbFieldQuoter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jScrollPane5.setBorder(null);

        jTextArea3.setEditable(false);
        jTextArea3.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        jTextArea3.setColumns(20);
        jTextArea3.setRows(2);
        jTextArea3.setText(constBundle.getString("mapColumnHint")
        );
        jScrollPane5.setViewportView(jTextArea3);

        tblChooseColumn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][]
            {

            },
            new String []
            {
                "", "file", "table"
            }
        )
        {
            Class[] types = new Class []
            {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean []
            {
                false, false, true
            };

            public Class getColumnClass(int columnIndex)
            {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex)
            {
                return canEdit [columnIndex];
            }
        });
        tblChooseColumn.setColumnSelectionAllowed(true);
        jScrollPane6.setViewportView(tblChooseColumn);
        tblChooseColumn.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tblChooseColumn.getColumnModel().getColumnCount() > 0)
        {
            tblChooseColumn.getColumnModel().getColumn(0).setMinWidth(30);
            tblChooseColumn.getColumnModel().getColumn(0).setPreferredWidth(30);
            tblChooseColumn.getColumnModel().getColumn(0).setMaxWidth(30);
        }

        jLabel1.setText(constBundle.getString("table"));

        cbDefaultMap.setSelected(true);
        cbDefaultMap.setText(constBundle.getString("default"));
        cbDefaultMap.setEnabled(false);

        javax.swing.GroupLayout pnlMapColumnLayout = new javax.swing.GroupLayout(pnlMapColumn);
        pnlMapColumn.setLayout(pnlMapColumnLayout);
        pnlMapColumnLayout.setHorizontalGroup(
            pnlMapColumnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMapColumnLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlMapColumnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnlMapColumnLayout.createSequentialGroup()
                        .addComponent(cbDefaultMap)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane5)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlMapColumnLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(10, 10, 10)
                        .addComponent(cbbTable, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnlMapColumnLayout.setVerticalGroup(
            pnlMapColumnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlMapColumnLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(pnlMapColumnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbbTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(5, 5, 5)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbDefaultMap)
                .addGap(9, 9, 9))
        );

        jScrollPane9.setBorder(null);

        jTextArea6.setEditable(false);
        jTextArea6.setBackground(javax.swing.UIManager.getDefaults().getColor("Label.background"));
        jTextArea6.setColumns(20);
        jTextArea6.setRows(2);
        jTextArea6.setText(constBundle.getString("startImportHint")
        );
        jScrollPane9.setViewportView(jTextArea6);

        jScrollPane10.setViewportView(tpMsg);

        javax.swing.GroupLayout pnlExecuteLayout = new javax.swing.GroupLayout(pnlExecute);
        pnlExecute.setLayout(pnlExecuteLayout);
        pnlExecuteLayout.setHorizontalGroup(
            pnlExecuteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlExecuteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnlExecuteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane10)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE))
                .addContainerGap())
        );
        pnlExecuteLayout.setVerticalGroup(
            pnlExecuteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlExecuteLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle(constBundle.getString("importData"));

        javax.swing.GroupLayout pnlMainLayout = new javax.swing.GroupLayout(pnlMain);
        pnlMain.setLayout(pnlMainLayout);
        pnlMainLayout.setHorizontalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 437, Short.MAX_VALUE)
        );
        pnlMainLayout.setVerticalGroup(
            pnlMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 385, Short.MAX_VALUE)
        );

        getContentPane().add(pnlMain, java.awt.BorderLayout.CENTER);

        btnBack.setText(constBundle.getString("back"));
        btnBack.setEnabled(false);

        btnNext.setText(constBundle.getString("next")
        );

        btnCancle.setText(constBundle.getString("cancle"));

        btnStart.setText(constBundle.getString("start"));
        btnStart.setEnabled(false);

        javax.swing.GroupLayout pnlButtonLayout = new javax.swing.GroupLayout(pnlButton);
        pnlButton.setLayout(pnlButtonLayout);
        pnlButtonLayout.setHorizontalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlButtonLayout.createSequentialGroup()
                .addContainerGap(149, Short.MAX_VALUE)
                .addComponent(btnBack)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnNext)
                .addGap(10, 10, 10)
                .addComponent(btnStart)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancle)
                .addContainerGap())
        );
        pnlButtonLayout.setVerticalGroup(
            pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnlButtonLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(pnlButtonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBack)
                    .addComponent(btnNext)
                    .addComponent(btnCancle)
                    .addComponent(btnStart))
                .addContainerGap())
        );

        getContentPane().add(pnlButton, java.awt.BorderLayout.PAGE_END);

        jScrollPane2.setPreferredSize(new java.awt.Dimension(120, 322));

        treeNavigation.setBorder(javax.swing.BorderFactory.createTitledBorder(constBundle.getString("navigation")));
        treeNavigation.setModel(null);
        treeNavigation.setRootVisible(false);
        jScrollPane2.setViewportView(treeNavigation);

        getContentPane().add(jScrollPane2, java.awt.BorderLayout.WEST);

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void customView()
    {
        treeNavigation.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) treeNavigation.getCellRenderer();
        renderer.setLeafIcon(null);
        renderer.setClosedIcon(null);
        renderer.setOpenIcon(null);
        treeNavigation.setCellRenderer(renderer);
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("");
        DefaultMutableTreeNode branchNode;
        branchNode = new DefaultMutableTreeNode(constBundle.getString("importFileFormat"));
        rootNode.add(branchNode);
        branchNode = new DefaultMutableTreeNode(constBundle.getString("importDirectory"));
        rootNode.add(branchNode);
        branchNode = new DefaultMutableTreeNode(constBundle.getString("specifyOption"));
        rootNode.add(branchNode);
        branchNode = new DefaultMutableTreeNode(constBundle.getString("mapColumn"));
        rootNode.add(branchNode);
        branchNode = new DefaultMutableTreeNode(constBundle.getString("startImport"));
        rootNode.add(branchNode);
        treeNavigation.setModel(new DefaultTreeModel(rootNode));
        treeNavigation.setSelectionRow(0);

        card = new CardLayout();
        pnlMain.setLayout(card);
        pnlMain.add(pnlChooseFormat, constBundle.getString("importFileFormat"));
        pnlMain.add(pnlChooseDir, constBundle.getString("exportDirectory"));
        pnlMain.add(pnlSpecifyOption, constBundle.getString("specifyOption"));
        pnlMain.add(pnlMapColumn, constBundle.getString("mapColumn"));
        pnlMain.add(pnlExecute, constBundle.getString("startImport"));
    }
    private void windowsClosing()
    {
        if (tc != null)
        {
            try
            {
                tc.stop();
                btnCancle.setText(constBundle.getString("quit"));
            } catch (BadLocationException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
        this.dispose();
    }
    private void btnCancleAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (btnCancle.getText().equals(constBundle.getString("cancle"))
                || btnCancle.getText().equals(constBundle.getString("quit")))
        {
            this.dispose();
        } else if (btnCancle.getText().equals(constBundle.getString("stop")))
        {
            try
            {
                tc.stop();
                btnCancle.setText(constBundle.getString("quit"));
            } catch (BadLocationException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }
    }   
    private void emptyModel(DefaultTableModel model)
    {
        int rowCount = model.getRowCount();
        if (rowCount > 0)
        {
            for (int i = rowCount - 1; i >= 0; i--)
            {
                model.removeRow(i);
            }
        }
    }
    int sequence = 1;
    private void btnNextAction(ActionEvent evt)
    {
        logger.info("sequence=" + sequence);
        if (sequence == 2)
        {
            if (tblChooseDir.getValueAt(0, 2) == null || tblChooseDir.getValueAt(0, 2).toString().isEmpty())
            {
                JOptionPane.showMessageDialog(this, constBundle.getString("noExportFileDirMsg"));
                return;
            }
        }
        card.next(pnlMain);
        sequence++;
        treeNavigation.setSelectionRow(sequence - 1);
        if (sequence == 2)
        {
            btnBack.setEnabled(true);
        }else if(sequence==4)
        {
            this.setFormat();
            this.emptyModel((DefaultTableModel) tblChooseColumn.getModel());            
            switch(format.getFormat())
            {
                case SQL:
                    break;
                default:
                    this.mapColumns();
                break;
            }           
        }
        else if (sequence == 5)
        {
            btnNext.setEnabled(false);
            btnStart.setEnabled(true);
        }
    }
    private void mapColumns()
    {               
        try
        {
            List<ColumnDTO> columnList = TreeController.getInstance().getColumnsOfTable(tableInfo);
            JComboBox cbb = new JComboBox();
            for (ColumnDTO column : columnList)
            {
                cbb.addItem(column);
            }
            tblChooseColumn.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(cbb));
            TransferController tc = new TransferController();
            Object[] columns = tc.getHeaderArrayFromFile(format);
            DefaultTableModel model = (DefaultTableModel) tblChooseColumn.getModel();
            if (columns.length != columnList.size())
            {
                throw new Exception("Number of column is difference.");
            }
            for (int i = 0; i < columns.length; i++)
            {
                model.addRow(new Object[]
                {
                    true, columns[i], columnList.get(i)
                });
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            JOptionPane.showMessageDialog(this, ex.toString(), constBundle.getString("errorWarning"),
                    JOptionPane.ERROR_MESSAGE);
        }
    }
    private void btnBackAction(ActionEvent evt)
    {
        card.previous(pnlMain);        
        sequence--;
        logger.info("sequence=" + sequence);
        treeNavigation.setSelectionRow(sequence-1);
        if (sequence == 1)
        {
            btnBack.setEnabled(false);
        } else if (sequence == 4)
        {
            btnNext.setEnabled(true);
            btnStart.setEnabled(false);
        }
    }
    //choose format
    private void chooseFormat(ActionEvent e)
    {
        JRadioButton rb = (JRadioButton) e.getSource();
        if (rb.isSelected())
        {
            if (rb == rbTXT)
            {
                format.setFormat(Format.TXT);
                cbbRowSeperator.setEnabled(true);
                cbbFieldSeperator.setEnabled(true);
                cbbFieldQuoter.setEnabled(true);
            } else
            {
                if (rb == rbCSV)
                {
                    format.setFormat(Format.CSV);
                    cbbRowSeperator.setEnabled(true);
                } else if (rb == rbSQL)
                {
                    format.setFormat(Format.SQL);                    
                    cbbRowSeperator.setEnabled(true);
                } else
                {
                    if (rb == rbXLS)
                    {
                        format.setFormat(Format.XLS);
                    }else if (rb == rbXLSX)
                    {
                        format.setFormat(Format.XLSX);
                    }
                    else if (rb == rbHTML)
                    {
                        format.setFormat(Format.HTML);
                    } 
                    cbbRowSeperator.setEnabled(false);
                }
                cbbFieldSeperator.setEnabled(false);
                cbbFieldQuoter.setEnabled(false);
            }
           tblChooseColumn.setEnabled(rb != rbSQL);
        }
    }
    //choose directory
    private void tblChooseFileMouseDoubleClicked(MouseEvent evt)
    {
        int column = tblChooseDir.getSelectedColumn();
        int row = tblChooseDir.getSelectedRow();
        if (column != 2)
        {
            return;
        }
        FileChooserDialog fileChooser = new FileChooserDialog();
        String initFile = "";
        if (tblChooseDir.getValueAt(row, column) != null)
        {
            initFile = tblChooseDir.getValueAt(row, column).toString();
        }
        FileNameExtensionFilter filter = new FileNameExtensionFilter("data file", format.getFormat().toString());
        int response = fileChooser.showFileChooser(this, "", initFile, filter);
        //logger.info("response = " + response);
        if (response == 0)
        {
            String choosedFile = fileChooser.getChoosedFileDir();
            logger.info("choosedFile=" + choosedFile);
            if (choosedFile.endsWith("." + format.getFormat().toString().toLowerCase()))
            {
                format.setFilePath(choosedFile);
                tblChooseDir.setValueAt(format.getFilePath(), row, column);
            } else
            {
                JOptionPane.showMessageDialog(this, MessageFormat.format(constBundle.getString("choosedWrongDirMsg"), choosedFile, format.getFormat()),
                        constBundle.getString("hint"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    private void btnStartImportAction(ActionEvent evt)
    {
        logger.info(evt.getActionCommand());
        if (btnStart.getText().equals(constBundle.getString("start")))
        {            
            logger.info("dir=" + format.getFilePath());
            List<ColumnDTO> columnList = new ArrayList();
            int row = tblChooseColumn.getRowCount();
            for (int i = 0; i < row; i++)
            {
                //if (tblChooseColumn.getValueAt(i, 0).equals(true))
                //{
                columnList.add((ColumnDTO) tblChooseColumn.getValueAt(i, 2));
                //}
            }
            ImportRunnable er = new ImportRunnable(tableInfo.getHelperInfo(), 
                    cbbTable.getSelectedItem().toString(), columnList, format, tpMsg.getDocument());
            Thread thread = new Thread(er);
            thread.start();
            btnStart.setText(constBundle.getString("pause"));
            btnCancle.setText(constBundle.getString("stop"));
        } else if (btnStart.getText().equals(constBundle.getString("pause")))
        {
            try
            {
                tc.pause();
                btnStart.setText(constBundle.getString("continue"));
            } catch (BadLocationException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (btnStart.getText().equals(constBundle.getString("continue")))
        {
            try
            {
                tc.continues();
                btnStart.setText(constBundle.getString("pause"));
            } catch (BadLocationException ex)
            {
                JOptionPane.showMessageDialog(this, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        } else if (btnStart.getText().equals(constBundle.getString("finish")))
        {
            this.dispose();
        }
    }
    private void setFormat()
    {
        format.setWithHeader(cbIncludeColumnName.isSelected());
        format.setContinueOnError(cbContinueOnError.isSelected());
        format.setReplaceBinaryValue(cbbReplaceByteaValue.getSelectedItem().toString());
        format.setEncoding(cbbEncoding.getSelectedItem().toString());
        switch (format.getFormat())
        {
            case CSV:
            case TXT:
                switch (cbbRowSeperator.getSelectedItem().toString())
                {
                    case "local":
                        format.setRowSeparator(System.lineSeparator());
                        break;
                    case "CRLF":
                        format.setRowSeparator("\r\n");
                        break;
                    case "CR":
                        format.setRowSeparator("\r");
                        break;
                    case "LF":
                        format.setRowSeparator("\n");
                        break;
                }
                if (format.getFormat() == Format.CSV)
                {
                    format.setColumnSeparator(",");
                    format.setColumnQuoter("\"");
                } else
                {
                    switch (cbbFieldSeperator.getSelectedItem().toString())
                    {
                        case "Tab":
                            format.setColumnSeparator("\t");
                            break;
                        default:
                            format.setColumnSeparator(cbbFieldSeperator.getSelectedItem().toString());
                            break;
                    }
                    switch (cbbFieldQuoter.getSelectedItem().toString())
                    {
                        case "none":
                            format.setColumnQuoter("");
                            break;
                        default:
                            format.setColumnQuoter(cbbFieldQuoter.getSelectedItem().toString());
                            break;
                    }
                }               
                break;
            default:
                break;
        }
    }
    
    private TransferController tc = null;
    private class ImportRunnable implements Runnable
    {
        private HelperInfoDTO helperInfo;
        private String relationName;
        private List<ColumnDTO> columnList;
        private FormatInfoDTO format;
        private Document docs;
        public ImportRunnable(HelperInfoDTO helperInfo, String relationName, List<ColumnDTO> columnList,
                FormatInfoDTO format, Document docs)
        {
            this.helperInfo = helperInfo;
            this.relationName = relationName;
            this.columnList = columnList;
            this.format = format;
            this.docs = docs;
        }
        @Override
        public void run()
        {
            try
            {
                tc = new TransferController();
                tc.importDataFromFile(helperInfo, relationName,
                        columnList, format,  docs);
                btnStart.setText(constBundle.getString("finish"));
                btnCancle.setText(constBundle.getString("quit"));
            } catch (Exception ex)
            {
                JOptionPane.showMessageDialog(null, ex.getMessage(),
                        constBundle.getString("errorWarning"), JOptionPane.ERROR_MESSAGE);
            }
        }               
    }
    
    
    /**
     * 
     * @param args the command line arguments
     */
    public static void main(String args[]) 
    {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | javax.swing.UnsupportedLookAndFeelException ex)
        {
            java.util.logging.Logger.getLogger(DataImportView.class.getName()).log(
                    java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                TableInfoDTO table = new TableInfoDTO();
                HelperInfoDTO helperInfo = new HelperInfoDTO();
                helperInfo.setSchema("public");
                helperInfo.setHost("127.0.0.1");
                helperInfo.setPort("5433");
                helperInfo.setMaintainDB("postgres");
                helperInfo.setUser("postgres");
                helperInfo.setPwd("postgres");
                table.setHelperInfo(helperInfo);
                table.setOid(16393);
                table.setName("table1");
                table.setSchema("public");
                DataImportView dev = new DataImportView(table);
                dev.setLocationRelativeTo(null);
                dev.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgFormat;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCancle;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnStart;
    private javax.swing.JCheckBox cbContinueOnError;
    private javax.swing.JCheckBox cbDefaultMap;
    private javax.swing.JCheckBox cbIncludeColumnName;
    private javax.swing.JComboBox cbbDateStyle;
    private javax.swing.JComboBox cbbEncoding;
    private javax.swing.JComboBox cbbFieldQuoter;
    private javax.swing.JComboBox cbbFieldSeperator;
    private javax.swing.JComboBox cbbReplaceByteaValue;
    private javax.swing.JComboBox cbbRowSeperator;
    private javax.swing.JComboBox cbbTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    private javax.swing.JTextArea jTextArea3;
    private javax.swing.JTextArea jTextArea4;
    private javax.swing.JTextArea jTextArea6;
    private javax.swing.JPanel pblFormat;
    private javax.swing.JPanel pnlButton;
    private javax.swing.JPanel pnlChooseDir;
    private javax.swing.JPanel pnlChooseFormat;
    private javax.swing.JPanel pnlExecute;
    private javax.swing.JPanel pnlMain;
    private javax.swing.JPanel pnlMapColumn;
    private javax.swing.JPanel pnlSpecifyOption;
    private javax.swing.JRadioButton rbCSV;
    private javax.swing.JRadioButton rbHTML;
    private javax.swing.JRadioButton rbSQL;
    private javax.swing.JRadioButton rbTXT;
    private javax.swing.JRadioButton rbXLS;
    private javax.swing.JRadioButton rbXLSX;
    private javax.swing.JTable tblChooseColumn;
    private javax.swing.JTable tblChooseDir;
    private javax.swing.JTextPane tpMsg;
    private javax.swing.JTree treeNavigation;
    // End of variables declaration//GEN-END:variables
}
