/* ------------------------------------------------
 *
 * File: FormatInfoDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\model\FormatInfoDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datatransfer.model;

import com.highgo.hgdbadmin.datatransfer.util.Format;

/**
 *
 * @author Yuanyuan
 */
public class FormatInfoDTO 
{
    //this is for all 
    private String replaceBinaryValue;
    private boolean continueOnError;
    private Format format;    
    private String filePath; 
    private int rowLimitPerFile;//-1--all row of the table 
    private boolean withHeader;
    //for sql
    private boolean withSchema;
    //those for txt
    private String rowSeparator;  
    private String columnQuoter;    
    private String columnSeparator;
    //this is unsure
    private String encoding;
    //this is for query and output
    private int quotWhat;//0-no Quoting,1-only String, 2-all columns

    public String getReplaceBinaryValue()
    {
        return replaceBinaryValue;
    }
    public void setReplaceBinaryValue(String replaceBinaryValue)
    {
        this.replaceBinaryValue = replaceBinaryValue;
    }

    public boolean isContinueOnError()
    {
        return continueOnError;
    }
    public void setContinueOnError(boolean continueOnError)
    {
        this.continueOnError = continueOnError;
    }

    public Format getFormat()
    {
        return this.format;
    }
    public void setFormat(Format format)
    {
        this.format = format;
    }
    
    public String getFilePath()
    {
        return filePath;
    }
    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public int getRowLimitPerFile()
    {
        return rowLimitPerFile;
    }

    public void setRowLimitPerFile(int rowLimitPerFile)
    {
        this.rowLimitPerFile = rowLimitPerFile;
    }

    public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public boolean isWithHeader()
    {
        return withHeader;
    }

    public void setWithHeader(boolean withHeader)
    {
        this.withHeader = withHeader;
    }

    public boolean isWithSchema()
    {
        return withSchema;
    }

    public void setWithSchema(boolean withSchema)
    {
        this.withSchema = withSchema;
    }

    public String getRowSeparator()
    {
        return rowSeparator;
    }

    public void setRowSeparator(String rowSeparator)
    {
        this.rowSeparator = rowSeparator;
    }

    public String getColumnSeparator()
    {
        return columnSeparator;
    }

    public void setColumnSeparator(String columnSeparator)
    {
        this.columnSeparator = columnSeparator;
    }

    public String getColumnQuoter()
    {
        return columnQuoter;
    }

    public void setColumnQuoter(String columnQuoter)
    {
        this.columnQuoter = columnQuoter;
    }

    public int getQuotWhat()
    {
        return quotWhat;
    }

    public void setQuotWhat(int quotWhat)
    {
        this.quotWhat = quotWhat;
    }
}
