/* ------------------------------------------------
 *
 * File: ColumnDTO.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\model\ColumnDTO.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datatransfer.model;

/**
 *
 * @author Yuanyuan
 */
public class ColumnDTO
{
    private String name;
    private String type;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
    
    @Override
    public String toString()
    {
        return this.name;
    }
    
}
