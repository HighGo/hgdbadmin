/* ------------------------------------------------
 *
 * File: Format.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src/com/highgo/hgdbadmin/datatransfer/util/Format.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datatransfer.util;

/**
 *
 * @author Yuanyuan
 */
public enum Format
{
    CSV,
	TXT,
	SQL,
	XLS,
	XLSX,
	HTML
}
