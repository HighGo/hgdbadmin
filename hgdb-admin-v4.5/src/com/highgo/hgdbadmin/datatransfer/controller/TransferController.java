/* ------------------------------------------------
 *
 * File: TransferController.java
 *
 * Abstract:
 *       .
 *
 * Authored by liuyuanyuan@highgo.com，20180713.
 *
 * Copyright:
 * Copyright (c) 2009-2020, HighGo Software Co.,Ltd.
 * All rights reserved .
 *
 * Identification:
 *       src\com\highgo\hgdbadmin\datatransfer\controller\TransferController.java
 *
 *-------------------------------------------------
 */

package com.highgo.hgdbadmin.datatransfer.controller;

import com.highgo.hgdbadmin.datatransfer.model.ColumnDTO;
import com.highgo.hgdbadmin.model.HelperInfoDTO;
import com.highgo.hgdbadmin.datatransfer.model.FormatInfoDTO;
import com.highgo.hgdbadmin.util.CloseStream;
import com.highgo.hgdbadmin.util.JdbcHelper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.NPOIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * lib: poi-3.12-20150511.jar
 *       poi-ooxml-3.12-20150511.jar, xmlbeans-2.6.0.jar
 * 
 * @author Yuanyuan
 */
public class TransferController 
{
    private  Logger logger =  LogManager.getLogger(getClass());//LoggerFactory.getLogger(getClass());  
    public TransferController()
    {
        logger.info("new ExportController");
    }
    
    public int getState()
    {
        return this.state;
    }
    public void pause() throws BadLocationException
    {
        isPause = true;
        docs.insertString(docs.getLength(), "Current state: Paused mannually."+System.lineSeparator(), attr);
        logger.info("execute pause");
    }
    public void continues() throws BadLocationException
    {
        isPause = false;
        docs.insertString(docs.getLength(), "Current state: Continue mannually."+System.lineSeparator(), attr);
        logger.info("execute continue");
    }
    public void stop() throws BadLocationException
    {
        isPause = false;
        isStop = true;
        logger.info(""+docs.getLength());
        docs.insertString(docs.getLength(), "Final state: Stopped mannually."+System.lineSeparator(), attr);
        logger.info("execute stop");
    }   
    private long startTime;
    private int state = 0 ;//0-unfinish, 1-success finish, 2 -error
    private boolean isPause;
    private boolean isStop;
    private SimpleAttributeSet attr;
    private Document docs;
    private SimpleDateFormat dtformat= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    
    /*
     * Export Data
    */
    public void exportDataToFile(HelperInfoDTO helperInfo, String relationName,
            List<ColumnDTO> columnList, FormatInfoDTO format, Document docs)
    {
        logger.info("relation name:　" + relationName);
        logger.info("row limit:　" + format.getRowLimitPerFile());
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return;
        }        
        if(relationName ==null || relationName.isEmpty())
        {
            logger.error("relation name is null, do nothing and return.");
            return;
        }
        if(format.getFilePath()==null || format.getFilePath().isEmpty())
        {
            logger.error("Output file path is null or empty, do nothing and return.");
            return;
        }    
        
        this.docs = docs;
        attr = new SimpleAttributeSet(); 
        state = 0;
        isPause = false;
        isStop = false;       
        
        StringBuilder columns = new StringBuilder();
        for (ColumnDTO column : columnList)
        {
            columns.append(column.getName()).append(",");
        }
        columns.deleteCharAt(columns.length() - 1);
        
        Connection conn = null;
        Statement stmt = null; 
        ResultSet rs = null;
        try
        {
            docs.insertString(0, "Source table: " + relationName + System.lineSeparator(), attr);
            docs.insertString(docs.getLength(), "Export to: " + format.getFilePath() + System.lineSeparator(), attr);
            startTime = System.currentTimeMillis();
            docs.insertString(docs.getLength(), "Start time: " + dtformat.format(new Date()) + System.lineSeparator(), attr);
            
            String url = helperInfo.getPartURL() + helperInfo.getDbName() + "?preferQueryMode=simple";
            logger.info(url);
            conn = JdbcHelper.getConnection(url, helperInfo);           
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "SELECT " + columns + " FROM " + relationName;
            logger.info(sql.toString());
            stmt.setFetchSize(1000);
            rs = stmt.executeQuery(sql);
            switch (format.getFormat())
            {
                case SQL:
                    this.ExportToSQLScript(rs, columns.toString(), relationName, columnList, format);
                    break;
                case TXT:
                case CSV:
                    this.ExportToCSVorTXT(rs, columnList, format);
                    break;
                case HTML:
                    this.ExportToHTML(rs, columnList, format);
                    break;
                case XLS:
                case XLSX:
                    this.ExportToExcel(rs, columnList, format);
                    break;
                default:
                    logger.error(format.getFormat()+" is an exception foramt, do nothing and return.");
                    return;
            }            
            conn.setAutoCommit(true);     
            state = 1;
            
            docs.insertString(docs.getLength(), "End time: " + dtformat.format(new Date())+ System.lineSeparator(), attr);
            docs.insertString(docs.getLength(), "Total time: " + (System.currentTimeMillis() - startTime) + "ms" + System.lineSeparator(), attr);
            docs.insertString(docs.getLength(), "Exit status: Finished successfully" + System.lineSeparator(), attr);            
        } catch (Exception ex)
        {
            state = 2;
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            docInsertString(docs, docs.getLength(), "Total time: " + (System.currentTimeMillis() - startTime) + "ms" + System.lineSeparator(), attr);
            docInsertString(docs, docs.getLength(), "Exit status: Failed in error " + System.lineSeparator() + ex.getMessage(), attr);
        } finally
        {
            JdbcHelper.close(rs);
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);           
            logger.info("close resource");
        }
    }   
    //while using rs.getObject(),we can only use index but not columnname
    //because rs cannot get object by columnname that quioted (like:"name").
    private void ExportToSQLScript(ResultSet rs, String columns,String relationName, 
            List<ColumnDTO> columnList, FormatInfoDTO format) throws Exception
    {  
        PrintWriter pw = null;
        try
        {
            if (format.getEncoding().equals("default"))
            {
                pw = new PrintWriter(format.getFilePath());
            } else if (format.getEncoding().equals("UTF-8"))
            {
                pw = new PrintWriter(new OutputStreamWriter(
                        new FileOutputStream(format.getFilePath()), format.getEncoding()));
            }
            StringBuilder insertSQL = new StringBuilder();
            String name = relationName;
            if(!format.isWithSchema())
            {
                name = name.substring(name.indexOf(".")+1, name.length());                
            }
            logger.info(name);
            while (rs.next())
            {
                while (isPause)
                {
                    logger.info("wait........");
                }
                if (isStop)
                {
                    logger.info("stop");
                    break;
                }                
                insertSQL.append("INSERT INTO ").append(name)
                        .append("(").append(columns).append(") VALUES(");
                for (int j = 1; j <= columnList.size(); j++)
                {
                    if (this.getValue(format, rs, j) == null)
                    {
                        insertSQL.append("NULL");
                    } else
                    {
                        insertSQL.append("'");
                        insertSQL.append(this.getValue(format, rs, j));
                        insertSQL.append("'::").append(columnList.get(j - 1).getType());
                    }
                    insertSQL.append(",");
                }
                insertSQL.deleteCharAt(insertSQL.length() - 1);
                insertSQL.append(");");
                pw.write(insertSQL.toString());
                pw.println();
                pw.flush();
                insertSQL.delete(0, insertSQL.length());
            }
        } catch (FileNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(pw);
            logger.info("close stream");
        }
    }
    // note: column delimiter for .csv is  ',' and column quoter is  "\"", so they cannot be defined.
    private void ExportToCSVorTXT(ResultSet rs, 
            List<ColumnDTO> columnList, FormatInfoDTO format) throws Exception
    {
        FormatInfoDTO csvFormat = (FormatInfoDTO) format;
        PrintWriter pw = null;
        try
        {
            if (format.getEncoding().equals("default"))
            {
                pw = new PrintWriter(format.getFilePath());
            } else if (format.getEncoding().equals("UTF-8"))
            {
                pw = new PrintWriter(new OutputStreamWriter(
                        new FileOutputStream(format.getFilePath()), format.getEncoding()));
            }
            StringBuilder rowData = new StringBuilder();
            if (format.isWithHeader())
            {                
                for (ColumnDTO column : columnList)
                {
                    rowData.append(csvFormat.getColumnQuoter()).append(column).append(csvFormat.getColumnQuoter())
                            .append(csvFormat.getColumnSeparator());
                }
                rowData.deleteCharAt(rowData.length() - 1);
                rowData.append(csvFormat.getRowSeparator());
                logger.info("header=" + rowData.toString());
                pw.write(rowData.toString());
                pw.flush();
                rowData.delete(0, rowData.length());
            }
            while (rs.next())
            {
                while (isPause)
                {
                    logger.info("wait...........");
                }
                if (isStop)
                {
                    logger.info("stop");
                    break;
                }
                for (int j = 1; j <= columnList.size(); j++)
                {
                    rowData.append(csvFormat.getColumnQuoter());
                    //logger.info(rs.getMetaData().getColumnTypeName(j));
                    rowData.append(this.getValue(format, rs, j));
                    rowData.append(csvFormat.getColumnQuoter()).append(csvFormat.getColumnSeparator());
                }
                rowData.deleteCharAt(rowData.length() - 1);
                rowData.append(csvFormat.getRowSeparator());
                //logger.info(rowData.toString());
                pw.write(rowData.toString());
                pw.flush();
                rowData.delete(0, rowData.length());
            }
        } catch (FileNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(pw);
            logger.info("close stream");
        }
    }
    private void ExportToHTML(ResultSet rs, List<ColumnDTO> columnList,
            FormatInfoDTO format) throws Exception
    {
        PrintWriter pw = null;
        try
        {
            pw = new PrintWriter(format.getFilePath());
            pw.write("<html>" + System.lineSeparator()
                    + "<head>" + System.lineSeparator()
                    + "<meta http-equiv=\"Content - Type\" content =\"text/html;charset=UTF-8\">" + System.lineSeparator()
                    + "</head>" + System.lineSeparator()
                    + "<body bgcolor=\"#FFFFFF\">" + System.lineSeparator()
                    + "<table border=\"1\" cellspacing=\"0\" style=\"border-collapse:collapse\">" + System.lineSeparator());
            pw.flush();
            if (format.isWithHeader())
            {
                pw.write("<tr>" + System.lineSeparator());
                for (ColumnDTO column : columnList)
                {
                    pw.write("<td>" + column.getName() + "</td>" + System.lineSeparator());
                }
                pw.write("</tr>" + System.lineSeparator());
                pw.flush();
            }
            StringBuilder rowData = new StringBuilder();
            while (rs.next())
            {
                while (isPause)
                {
                    logger.info("wait...........");
                }
                if (isStop)
                {
                    logger.info("stop");
                    break;
                }
                pw.write("<tr>" + System.lineSeparator());
                for (int j = 1; j <= columnList.size(); j++)
                {
                    if (this.getValue(format, rs, j) == null)
                    {
                        pw.write("<td>null</td>" + System.lineSeparator());
                    } else
                    {
                        if (rs.getMetaData().getColumnClassName(j).equals("com.highgo.jdbc.util.PGmoney"))
                        {
                            pw.write("<td>" + this.getValue(format, rs, j).toString().replaceFirst("￥", "&yen;") 
                                    + "</td>" + System.lineSeparator());
                        } else
                        {
                            pw.write("<td>" + this.getValue(format, rs, j) + "</td>" + System.lineSeparator());
                        }
                    }
                }
                pw.write("</tr>" + System.lineSeparator());
                pw.flush();
                rowData.delete(0, rowData.length());
            }
            pw.write("</table>" + System.lineSeparator()
                    + "</body>" + System.lineSeparator()
                    + "</html>" + System.lineSeparator());
        } catch (FileNotFoundException | SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(pw);
            logger.info("close stream");
        }
    }
    private void ExportToExcel(ResultSet rs, List<ColumnDTO> columnList,
            FormatInfoDTO format) throws Exception
    {
        Workbook wb = null;
        Sheet sheet;
        Row row;
        switch (format.getFormat())
        {
            case XLS:
                wb = new HSSFWorkbook();
                break;
            case XLSX:
                wb = new XSSFWorkbook();
                break;
        }
        sheet = wb.createSheet("lyy");
        row = sheet.createRow((int) 0);
//        设置表头居中  
//        HSSFCellStyle style = wb.createCellStyle();
//        style.setAlignment(HSSFCellStyle.ALIGN_CENTER); 
        short columnId = 0;
        for (ColumnDTO column : columnList)
        {
            Cell cell = row.createCell(columnId);
            cell.setCellValue(column.getName());
//            cell.setCellStyle(style);
            columnId++;
        }        
        try
        {
            int fileId=1;
            int rowId = 0;
            while (rs.next())
            {
                rowId++;
                logger.info("row=" + rowId);
                while (isPause)
                {
                    logger.info("wait...........");
                }
                if (isStop)
                {
                    logger.info("stop");
                    break;
                }
                row = sheet.createRow(rowId);
                for (int i = 0; i < columnList.size(); i++)
                {
                    if (this.getValue(format, rs, i + 1) == null)
                    {
                        row.createCell((short) i).setCellValue("null");
                    } else
                    {
                        row.createCell((short) i).setCellValue(this.getValue(format, rs, i + 1).toString());
                    }
                }                
                if (format.getRowLimitPerFile() > 0 && rowId == format.getRowLimitPerFile())
                {
                    FileOutputStream fout = new FileOutputStream(format.getFilePath() + "." + fileId);
                    wb.write(fout);
                    fout.close();
                    fileId++;                    
                    switch (format.getFormat())
                    {
                        case XLS:
                            wb = new HSSFWorkbook();
                            break;
                        case XLSX:
                            wb = new XSSFWorkbook();
                            break;
                    }
                    sheet = wb.createSheet("lyy");
                    row = sheet.createRow((int) 0);
                    columnId = 0;
                    for (ColumnDTO column : columnList)
                    {
                        Cell cell = row.createCell(columnId);
                        cell.setCellValue(column.getName());
                        columnId++;
                    }
                    rowId = 0;
                }                
            }
            
            FileOutputStream fout = null;
            if (format.getRowLimitPerFile() == -1)
            {
                fout = new FileOutputStream(format.getFilePath());
            } else
            {
                fout = new FileOutputStream(format.getFilePath() + "." + fileId);
            }
            wb.write(fout);
            fout.close();
            
        } catch (SQLException | IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        }
    }
    private Object getValue(FormatInfoDTO format,ResultSet rs, int columnIndex) throws SQLException
    {
        String name = rs.getMetaData().getColumnClassName(columnIndex);
        //pgjdbc将money映射为java的double，并假设货币为美元对服务端返回的money字符串进行解析。
        //这导致对服务端的lc_monetary参数值为美元以外的货币时，pgjdbc将会解析失败(*1)。
        switch (name)
        {
            case "com.highgo.jdbc.util.PGmoney":
            case "java.lang.Boolean":
            case "java.sql.SQLXML":
                return rs.getString(columnIndex);
            case "[B":
                if (format.getReplaceBinaryValue().equals("NULL"))
                {
                    return "null";
                } else
                {
                    return rs.getString(columnIndex);
                }
            default:
                return rs.getObject(columnIndex);
        }
    }
    
    /*
     * get file column header
    */
    public Object[] getHeaderArrayFromFile(FormatInfoDTO format) throws Exception
    {
        logger.info(format.getFilePath());
        Object[] headers = new String[]{};
        switch (format.getFormat())
        {
            case TXT:
            case CSV:
                headers = this.getHeaderArrayFromTxtorCSV(format);
                break;
            case XLS:
            case XLSX:
                headers = this.getHeaderArrayFromExcel(format);
                break;
            case HTML:
                headers = this.getHeaderArrayFromHTML(format);
                break;
        }
        logger.info("Return: size="+headers.length);
        return headers;
    }
    private Object[] getHeaderArrayFromTxtorCSV(FormatInfoDTO format) throws Exception
    {
        Object[] headers = new Object[]{};
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new FileReader(format.getFilePath()));
            String line = br.readLine();
            if (line != null)
            {
                if(!line.startsWith(format.getColumnQuoter())
                        || !line.endsWith(format.getColumnQuoter()))
                {
                    throw new Exception("Line:not in proper quote format.");
                }
                if (format.isWithHeader())
                {
                    line = line.substring(0 + format.getColumnQuoter().length(),
                            line.length() - format.getColumnQuoter().length());
                    headers = line.split(format.getColumnQuoter() + format.getColumnSeparator() + format.getColumnQuoter());
                } else
                {
                    headers = new String[line.split(format.getColumnSeparator()).length];
                    for (int i = 0; i < headers.length; i++)
                    {
                        headers[i] = "field" + (i + 1);
                    }
                }
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(br);
        }
        return headers;
    }
    private Object[] getHeaderArrayFromExcel(FormatInfoDTO format) throws IOException
    {
        Object[] headers = new Object[]{};
        try
        {
            Workbook wb = WorkbookFactory.create(new File(format.getFilePath()));
            if (wb == null || wb.getNumberOfSheets() <= 0)
            {
                logger.warn("There's nothing in the file, return empty array.");
                return headers;
            }
            Sheet sheet = wb.getSheetAt(0);
            Row row = sheet.getRow(0);
                    //getLastCellNum is not absolutly correct, 
            //so we cannot use it as column count
            //int columnCount = row.getLastCellNum();
            //headers = new String[columnCount];
            List<String> headerList = new ArrayList();
            if (format.isWithHeader())
            {
                for (Cell c : row)
                {
                    headerList.add(c.getStringCellValue());
                }
            } else
            {
                int i = 0;
                for (Cell c : row)
                {
                    i++;
                    headerList.add("field" + (i + 1));
                }
            }
            headers = headerList.toArray();
        } catch (IOException | InvalidFormatException | EncryptedDocumentException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace();
            throw new IOException(ex);
        }
        return headers;
    }
    private Object[] getHeaderArrayFromHTML(FormatInfoDTO format) throws IOException
    {
        Object[] headers = new Object[]{};
        BufferedReader br2 = null;
        try
        {
            br2 = new BufferedReader(new FileReader(format.getFilePath()));
            List<String> headerList = new ArrayList();
            String line;
            while ((line = br2.readLine()) != null)
            {
                line = line.trim();
                if (line.equals("<tr>"))
                {
                    line = br2.readLine();
                    while (line != null && !line.trim().equals("</tr>"))
                    {
                        line = line.trim();
                        if (line.startsWith("<td>") && line.endsWith("</td>"))
                        {
                            headerList.add(line.substring(4, line.length() - 5));
                            //logger.info(line);
                        }
                        line = br2.readLine();
                    }
                    //one row ended
                    break;
                }
            }
            headers = headerList.toArray();
        } catch (IOException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        } finally
        {
            CloseStream.close(br2);
        }
        return headers;
    }
    
    
    /*
     * Import Data
    */    
    public void importDataFromFile(HelperInfoDTO helperInfo, String relationName,
             List<ColumnDTO> columnList,FormatInfoDTO format, Document docs)
    {
        logger.info("Enter:column=" + columnList.size());
        if (helperInfo == null)
        {
            logger.error("helperInfo is null, do nothing and return.");
            return;
        }
        if (relationName == null || relationName.isEmpty())
        {
            logger.error("relation name is null, do nothing and return.");
            return;
        }
        if (format.getFilePath() == null || format.getFilePath().isEmpty())
        {
            logger.error("Output file path is null or empty, do nothing and return.");
            return;
        }        
        this.docs = docs;
        attr = new SimpleAttributeSet();
        state = 0;
        isPause = false;
        isStop = false;
        
        logger.info("format=" + format.getFormat().toString());
        BufferedReader br = null;
        Connection conn = null;
        Statement stmt = null;
        try
        {
            docs.insertString(0, "Target table: " + relationName + System.lineSeparator(), attr);
            docs.insertString(docs.getLength(), "Import from: " + format.getFilePath() + System.lineSeparator(), attr);
            startTime = System.currentTimeMillis();
            docs.insertString(docs.getLength(), "Start time: " + dtformat.format(new Date()) + System.lineSeparator(), attr);
            
            conn = JdbcHelper.getConnection(helperInfo.getPartURL() + helperInfo.getDbName(), helperInfo);
            stmt = conn.createStatement();    
            boolean hasError = false;
            switch (format.getFormat())
            {
                case CSV:
                case TXT:
                    hasError = this.importFromTXTorCSV(relationName, columnList, format, stmt);
                    break;
                case SQL:
                    hasError = this.importFromSQL(format, stmt);
                    break;
                case XLS:
                    hasError = this.importFromXLS(relationName, columnList, format, stmt);
                    break;
                case XLSX:
                    hasError = this.importFromXLSX(relationName, columnList, format, stmt);
                    break;
                case HTML:
                    hasError = this.importFromHtml(relationName, columnList, format, stmt);
                    break;
            }
            state = 1;
            docs.insertString(docs.getLength(), "End time: " + dtformat.format(new Date()) + System.lineSeparator(), attr);
            logger.info("Total time: " + (System.currentTimeMillis() - startTime) + "ms");
            docs.insertString(docs.getLength(), "Total time: " + (System.currentTimeMillis() - startTime) + "ms" + System.lineSeparator(), attr);
            docs.insertString(docs.getLength(), "Exit status: Finished " + (hasError ? " with error" : "successfully") + System.lineSeparator(), attr);
        } catch (Exception ex)
        {
            state = 2;
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            this.docInsertString(docs, docs.getLength(), "Total time: " + (System.currentTimeMillis() - startTime) + "ms" + System.lineSeparator(), attr);
            this.docInsertString(docs, docs.getLength(), "Exit status: Failed in error " + System.lineSeparator() + ex.getMessage() + System.lineSeparator(), attr);
            if (ex instanceof BatchUpdateException)
            {
                BatchUpdateException bue = (BatchUpdateException) ex;
                logger.error(bue.getMessage());
                bue.printStackTrace(System.out);
                this.docInsertString(docs, docs.getLength(), bue.getMessage() + System.lineSeparator(), attr);
                logger.error(bue.getNextException().getMessage());
            }
        } finally
        {
            JdbcHelper.close(stmt);
            JdbcHelper.close(conn);
        }
    }
    private boolean importFromSQL( FormatInfoDTO format,  Statement stmt) throws IOException, SQLException, Exception
    {    
        boolean hasError = false;
        BufferedReader br = null;
        try
        {
            File file = new File(format.getFilePath());
            br = new BufferedReader(new FileReader(file));
            String line;
            while ((line = br.readLine()) != null)
            {
                try
                {
                    while (isPause)
                    {
                        logger.info("wait........");
                    }
                    if (isStop)
                    {
                        logger.info("stop");
                        break;
                    }
                    logger.info(line);
                    stmt.execute(line);
                } catch (SQLException ex)
                {
                    hasError = true;
                    if (format.isContinueOnError())
                    {
                        logger.error(ex.getMessage());
                        ex.printStackTrace(System.out);
                        this.docInsertString(docs, docs.getLength(),
                                "Error insert:" + line + System.lineSeparator(), attr);
                    } else
                    {
                        throw new SQLException(ex);
                    }
                }
            }
        } catch (IOException | SQLException ex)
        {
            hasError = true;
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(br);
        }
        return hasError;
    }   
    private boolean importFromTXTorCSV(String relationName, List<ColumnDTO> columnList,
            FormatInfoDTO format, Statement stmt) throws IOException, SQLException, Exception
    {
        boolean hasError = false;
        BufferedReader br = null;
        try
        {   
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO ").append(relationName).append("(");
            for (ColumnDTO column : columnList)
            {
                sql.append(column.getName()).append(",");
            }
            sql.deleteCharAt(sql.length() - 1);
            sql.append(") VALUES ");
            logger.info(sql.toString());

            File file = new File(format.getFilePath());
            br = new BufferedReader(new FileReader(file));
            if (format.isWithHeader())
            {
                br.readLine();
            }
            StringBuilder values;
            String line;
            //int row = 0;
            while ((line = br.readLine()) != null)
            {
                //row++;
                while (isPause)
                {
                    logger.info("wait........");
                }
                if (isStop)
                {
                    logger.info("stop");
                    break;
                }
                //parser like below is more accurate than use only column seperator to split.
                logger.debug("startsWith=" + line.startsWith(format.getColumnQuoter())
                        + " ,endsWith=" + line.endsWith(format.getColumnQuoter()));
                if (!line.startsWith(format.getColumnQuoter())
                        || !line.endsWith(format.getColumnQuoter()))
                {
                    throw new Exception("Line not has proper column quoter." + " \nLine: " + line);
                }
                line = line.substring(0 + format.getColumnQuoter().length(), line.length() - format.getColumnQuoter().length());
                String[] columns = line.split(format.getColumnQuoter() + format.getColumnSeparator() + format.getColumnQuoter());
                if (columns.length != columnList.size())
                {
                    hasError = true;
                    logger.warn("Cannot parser line: " + line + System.lineSeparator() + " , do nothing and continue! ");
                    this.docInsertString(docs, docs.getLength(),
                            "Cannot parser line: " + line + System.lineSeparator(), attr);
                    continue;
                }                
                values = new StringBuilder();
                values.append("(");
                for (int i = 0; i < columns.length; i++)
                {
                    if (columns[i].equals("null"))
                    {
                        values.append("null,");
                    } else
                    {
                        values.append("'").append(columns[i]).append("'::").append(columnList.get(i).getType()).append(",");
                    }
                }
                values.deleteCharAt(values.length() - 1);
                values.append(")");
                logger.info(sql.toString() + values.toString());
                //stmt.addBatch(sql.toString() + values.toString());
                try
                {
                    stmt.execute(sql.toString() + values.toString());
                } catch (SQLException ex)
                {
                    hasError = true;
                    if (format.isContinueOnError())
                    {
                        logger.error(ex.getMessage());
                        ex.printStackTrace(System.out);
                        this.docInsertString(docs, docs.getLength(),
                                "Error insert:" + sql.toString() + values.toString() + System.lineSeparator(), attr);
                    } else
                    {
                        throw new SQLException(ex);
                    }
                }
//            if (row % 10 == 0)
//            {
//                try
//                {
//                    stmt.executeBatch();
//                    conn.commit();
//                    stmt.clearBatch();
//                    stmt.clearWarnings();
//                } catch (SQLException ex)
//                {
//                    this.docInsertString(docs, docs.getLength(), "Batch Error: batch[" + (row - 10) + "-" + row + "] batch insert failed." + System.lineSeparator()
//                            + ex.getMessage() + System.lineSeparator(), attr);
//                }
//            }
            }
//        if (row % 10 != 0)
//        {
//            stmt.executeBatch();
//            conn.commit();
//        }
        } catch (IOException | SQLException ex)
        {
            hasError =  true;
            throw new Exception(ex);
        } finally
        {
            CloseStream.close(br);
        }
        return hasError;
    }
    private boolean importFromXLS(String relationName, List<ColumnDTO> columnList,
            FormatInfoDTO format, Statement stmt) throws Exception
    {
        boolean hasError = false;
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(relationName).append("(");
        for (ColumnDTO column : columnList)
        {
            sql.append(column.getName()).append(",");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(") VALUES ");
        NPOIFSFileSystem fs = null;
        try
        {
            fs = new NPOIFSFileSystem(new File(format.getFilePath()));
            Workbook wb = new HSSFWorkbook(fs.getRoot(), true);
            if (wb.getNumberOfSheets() > 0)
            {
                Sheet sheet = wb.getSheetAt(0);
                this.dealSheetContent(format, columnList, sheet, sql.toString(), stmt);
            } else
            {
                logger.warn("There's no content in the Excel file, do nothing and return.");
            }
        } catch (IOException | SQLException ex)
        {
            hasError = true;
            throw new Exception(ex);
        }finally
        {
            if (fs != null)
            {
                fs.close();
            }
        }
        return hasError;
    }
    private boolean importFromXLSX(String relationName, List<ColumnDTO> columnList,
            FormatInfoDTO format, Statement stmt) throws Exception
    {
        boolean hasError = false;
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(relationName).append("(");
        for (ColumnDTO column : columnList)
        {
            sql.append(column.getName()).append(",");
        }
        sql.deleteCharAt(sql.length()-1);
        sql.append(") VALUES ");
        OPCPackage pkg = null;
        try
        {            
            pkg = OPCPackage.open(new File(format.getFilePath()));
            Workbook wb = new XSSFWorkbook(pkg);
            if (wb.getNumberOfSheets() > 0)
            {
                Sheet sheet = wb.getSheetAt(0);
                this.dealSheetContent(format, columnList, sheet, sql.toString(), stmt);
            } else
            {
                logger.warn("There's no content in the Excel file, do nothing and return.");
            }            
        } catch (IOException | InvalidFormatException | SQLException ex)
        {
            hasError = true;
            throw new Exception(ex);
        } finally
        {
            if (pkg != null)
            {
                pkg.close();
            }
        }
        return hasError;
    }
    private boolean dealSheetContent(FormatInfoDTO format, List<ColumnDTO> columnList,
            Sheet sheet, String sql, Statement stmt) throws SQLException
    {
        boolean hasError = false;
        StringBuilder values = new StringBuilder();
        boolean isFirst = true;
        for (Row row : sheet)
        {
            while (isPause)
            {
                logger.info("wait........");
            }
            if (isStop)
            {
                logger.info("stop");
                break;
            }

            if (isFirst && format.isWithHeader())
            {
                isFirst = false;
                continue;
            }
            values.append("(");
            int i = 0;
            for (Cell cell : row)
            {
                i++;
                if (cell.getStringCellValue() == null
                        || cell.getStringCellValue().isEmpty())
                {
                    values.append("null");
                } else
                {
                    values.append("'").append(cell.getStringCellValue()).append("'")
                            .append("::").append(columnList.get(i - 1).getType());
                }
                values.append(",");
            }
            values.deleteCharAt(values.length() - 1);
            values.append(");");
            try
            {
                //logger.info(sql + values.toString());
                stmt.execute(sql + values.toString());
                values.delete(0, values.length());
            } catch (SQLException ex)
            {
                hasError = true;
                values.delete(0, values.length());
                logger.error(ex.getMessage());
                ex.printStackTrace(System.out);
                if (format.isContinueOnError())
                {
                    this.docInsertString(docs, docs.getLength(),
                            "Error insert:" + sql.toString() + values.toString() + System.lineSeparator(), attr);
                } else
                {
                    throw new SQLException(ex);
                }
            }
        }
        return hasError;
    }
    private boolean importFromHtml(String relationName, List<ColumnDTO> columnList,
            FormatInfoDTO format, Statement stmt) throws SQLException, IOException
    {
        boolean hasError = false;
        StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ").append(relationName).append("(");
        for (ColumnDTO column : columnList)
        {
            sql.append(column.getName()).append(",");
        }
        sql.deleteCharAt(sql.length() - 1);
        sql.append(") VALUES ");
        logger.info(sql.toString());
        FileReader fr = null;
        BufferedReader br = null;
        try
        {
            fr = new FileReader(format.getFilePath());
            br = new BufferedReader(fr);
            
            StringBuilder values = new StringBuilder();
            int row = 0;
            String line;            
            while ((line = br.readLine()) != null)
            {
                line = line.trim();
                if (line.equals("<tr>"))
                {
                    row++;
                    logger.info("rowNum=" + row);
                    if (row == 1 && format.isWithHeader())
                    {
                        continue;
                    }   
                    values.append("(");
                    line = br.readLine();
                    int column = 0;
                    while (line != null && !line.trim().equals("</tr>"))
                    {
                        line = line.trim();
                        if (line.startsWith("<td>") && line.endsWith("</td>"))
                        {
                            column++;
                            line = line.substring(4, line.length() - 5);
                            //logger.info(line);
                            if (line.equals("null"))
                            {
                                values.append("null,");
                            } else
                            {
                                values.append("'");
                                if (columnList.get(column - 1).getType().equals("money"))
                                {
                                    values.append(line.replaceFirst("&yen;", "￥"));
                                } else
                                {
                                    values.append(line);
                                }
                                values.append("'::").append(columnList.get(column - 1).getType()).append(",");
                            }
                        }
                        line = br.readLine();
                    }
                    values.deleteCharAt(values.length() - 1);
                    values.append(");");
                    //logger.info(values.toString());
                    try
                    {
                        stmt.execute(sql.toString() + values.toString());
                        values.delete(0, values.length());
                    } catch (SQLException ex)
                    {
                        hasError = true;
                        values.delete(0, values.length());
                        if (format.isContinueOnError())
                        {
                            logger.error(ex.getMessage());
                            ex.printStackTrace(System.out);
                            this.docInsertString(docs, docs.getLength(),
                                    "Error insert:" + sql.toString() + values.toString() + System.lineSeparator(), attr);
                        } else
                        {
                            throw new SQLException(ex);
                        }
                    }
                }
            }
        } catch (IOException ex)
        {
            hasError = true;
            logger.error(ex.getMessage());
            ex.printStackTrace(System.out);
            throw new IOException(ex);
        } finally
        {
            CloseStream.close(br);
            CloseStream.close(fr);

        }
        return hasError;
    }


    //util
    private void docInsertString(Document docs, int startPoasition, String content,AttributeSet attr)
    {
        try
        {
            docs.insertString(startPoasition, content, attr);
        } catch (BadLocationException ble)
        {
            logger.error(ble.getMessage());
            ble.printStackTrace(System.out);
        }
    }


}

